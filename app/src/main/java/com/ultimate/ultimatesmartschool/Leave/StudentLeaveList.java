package com.ultimate.ultimatesmartschool.Leave;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.Request;
import com.ultimate.ultimatesmartschool.BeanModule.User;
import com.ultimate.ultimatesmartschool.Login.LoginActivity;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.StuGatePass.ViewStudentGatePass;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.CommonProgress;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;
import com.ultimate.ultimatesmartschool.Utility.PaginationListener;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class StudentLeaveList extends AppCompatActivity implements Studentleaveadapter.Mycallback {
    @BindView(R.id.recyclerViewmsg)RecyclerView recyclerView;
    @BindView(R.id.parent)
    RelativeLayout parent;
    LinearLayoutManager layoutManager;
    Studentleaveadapter adapter;
    ArrayList<Studentlistbean> leavelist=new ArrayList<>();
    int loaded = 0;
    String tag="";
    @BindView(R.id.totalRecord)
    TextView totalRecord;
    private LinearLayoutManager layoutManager_new;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.textNorecord)
    TextView txtNorecord;
    ArrayList<OptionBean> vehicleList = new ArrayList<>();
    @BindView(R.id.spinerVehicletype)
    Spinner vehicleType;
    CommonProgress commonProgress;

    @BindView(R.id.main_progress)
    ProgressBar main_progress;
    int  limit=10,page_limit=0,total_pages=0;
    private static final int PAGE_START = 1;
    private int currentPage = PAGE_START;
    private boolean isLastPage = false;
    private boolean isLoading = false;

    @BindView(R.id.scrollView)
    NestedScrollView scrollView;
    @BindView(R.id.pagination)
    LinearLayout pagination;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_staff_leave_list);
        ButterKnife.bind(this);
        commonProgress=new CommonProgress(this);
        layoutManager=new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        adapter=new Studentleaveadapter(leavelist,this,this);
        recyclerView.setAdapter(adapter);
        pagination.setVisibility(View.VISIBLE);
        scrollView.setVisibility(View.GONE);
        txtTitle.setText("Students Leave Details");

        vehicleList.add(new OptionBean("Pending"));
        vehicleList.add(new OptionBean("Approved"));
        vehicleList.add(new OptionBean("Decline"));

        OptionAdapter dataAdapter = new OptionAdapter(StudentLeaveList.this, vehicleList);
        //set the ArrayAdapter to the spinner
        vehicleType.setAdapter(dataAdapter);
        //attach the listener to the spinner
        vehicleType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i != 0) {
                    tag = vehicleList.get(i - 1).getName();
                    if (tag.equalsIgnoreCase("Pending")){
                        tag="unapproved";
                    }
                }else {
                    tag="";
                }
                limit=10;
                page_limit=0;
                total_pages=0;
                currentPage = PAGE_START;
                isLastPage = false;
                isLoading = false;

                fetchleavelist(tag, limit, "yes");

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        recyclerView.addOnScrollListener(new PaginationListener(layoutManager) {
            @Override
            protected void loadMoreItems() {
                // isLoading = true;
                //  Utils.showSnackBar("No more messages found in message list.", parent);
                //  currentPage++;
                // doApiCall();

                if (total_pages>=page_limit) {
                    main_progress.setVisibility(View.VISIBLE);
                    currentPage += 10;
                    page_limit = currentPage + limit;

                    fetchleavelist(tag, page_limit, "no");

                }else{
                    // main_progress.setVisibility(View.GONE);
                    //  Utils.showSnackBar("No more messages found in message list.", parent);
                }


            }
            @Override
            public boolean isLastPage() {
                return isLastPage;
            }
            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });


        //  fetchleavelist(tag);
    }


    @OnClick(R.id.imgBack)
    public void backCall(){
        finish();
    }
    private void fetchleavelist(String tag, int page_limit, String progress) {
        if (progress.equalsIgnoreCase("yes")){
            if (loaded == 0) {
                commonProgress.show();
            }
        }
        loaded++;
        //    ErpProgress.showProgressBar(this, "Please wait...");
        HashMap<String,String> params=new HashMap<>();
        ApiHandler.apiHit(Request.Method.GET, Constants.getBaseURL() + Constants.StudentLEAVELIST+
                Constants.LEAVELISTID+ tag+ "&page_limit=" +page_limit,apicallback,this,params);
    }
    ApiHandler.ApiCallback apicallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            commonProgress.dismiss();
            if (error == null) {
                try {
                    if (leavelist != null) {
                        leavelist.clear();
                    }
                    JSONArray jsonArray = jsonObject.getJSONArray("leave_data");
                    leavelist = Studentlistbean.parseleavelistArray(jsonArray);
                    total_pages= Integer.parseInt(leavelist.get(0).getRowcount());
                    if (leavelist.size() > 0) {
                        adapter.setleavelstList(leavelist);
                        totalRecord.setText("Total Entries:- "+String.valueOf(total_pages));
                        adapter.notifyDataSetChanged();
                        txtNorecord.setVisibility(View.GONE);

                    } else {
                        totalRecord.setText("Total Entries:- 0");
                        adapter.setleavelstList(leavelist);
                        adapter.notifyDataSetChanged();
                        txtNorecord.setVisibility(View.VISIBLE);
                    }
                    main_progress.setVisibility(View.GONE);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                totalRecord.setText("Total Entries:- 0");
                txtNorecord.setVisibility(View.VISIBLE);
                leavelist.clear();
                adapter.setleavelstList(leavelist);
                adapter.notifyDataSetChanged();
                Utils.showSnackBar(error.getMessage(), parent);
            }
        }
    };


    @Override
    public void onApproveCallback(Studentlistbean studentlistbean) {

        if (loaded == 0) {
            commonProgress.show();
        }
        loaded++;
        HashMap<String, String> params = new HashMap<>();
        params.put("leave_id",studentlistbean.getId());
        Log.e("whayismyleaveid",studentlistbean.getId());
        String url="";
        if (User.getCurrentUser().getSchoolData().getFi_school_id().equalsIgnoreCase("XYZGGSMT")
        ){
            url=Constants.APPROVESTULEAVEURL_SUB;
        }else {
            url=Constants.APPROVESTULEAVEURL;
        }

        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + url,approveapicallback, this, params);

    }

    @Override
    public void onUNApproveCallback(Studentlistbean studentlistbean) {
        if (loaded == 0) {
            commonProgress.show();
        }
        loaded++;
        HashMap<String, String> params = new HashMap<>();
        params.put("leave_id",studentlistbean.getId());
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.DECLINESTDNTLEAVEURL,approveapicallback, this, params);
    }



    ApiHandler.ApiCallback approveapicallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            commonProgress.dismiss();
            if (error == null) {
                try {
                    Utils.showSnackBar(jsonObject.getString("msg"), parent);
                    loaded=0;
                    fetchleavelist(tag, limit, "yes");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("error", error.getMessage() + "");
                Utils.showSnackBar(error.getMessage(), parent);

                if (error.getStatusCode() == 405) {
                    User.logout();
                    startActivity(new Intent(StudentLeaveList.this, LoginActivity.class));
                    finish();
                }
            }
        }
    };

}
