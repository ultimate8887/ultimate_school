package com.ultimate.ultimatesmartschool.Leave;

import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.DebugLog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Stafflistbean {

    private static String ID="id";
    private static String STAFFID="staff_id";
    private static String MESSAGE="msg";
    private static String FROMDATE="from_date";
    private static String TODATE="to_date";
    private static String NAME="to_name";
    private static String APPROVE_STATUS="approve";
    /**
     * id : 2
     * staff_id : 1
     * msg : sdgdfhgfdhgfhf
     * from_date : 2018-02-02
     * to_date : 2018-02-02
     * status : active
     * approve : unapproved
     * to_name : Adminn Gupta
     */

    private String id;
    private String staff_id;
    private String msg;
    private String from_date;
    private String to_date;
    private String status;
    private String approve;
    private String to_name;
    /**
     * depart : Developement
     */


    public String getRowcount() {
        return rowcount;
    }

    public void setRowcount(String rowcount) {
        this.rowcount = rowcount;
    }

    private String rowcount;

    private String profile;

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    private String gender;

    private String depart="";

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStaff_id() {
        return staff_id;
    }

    public void setStaff_id(String staff_id) {
        this.staff_id = staff_id;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getFrom_date() {
        return from_date;
    }

    public void setFrom_date(String from_date) {
        this.from_date = from_date;
    }

    public String getTo_date() {
        return to_date;
    }

    public void setTo_date(String to_date) {
        this.to_date = to_date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getApprove() {
        return approve;
    }

    public void setApprove(String approve) {
        this.approve = approve;
    }

    public String getTo_name() {
        return to_name;
    }

    public void setTo_name(String to_name) {
        this.to_name = to_name;
    }

    public String getL_type() {
        return l_type;
    }

    public void setL_type(String l_type) {
        this.l_type = l_type;
    }

    private String l_type;




    public static ArrayList<Stafflistbean> parseleavelistArray(JSONArray jsonArray) {
        ArrayList<Stafflistbean> list = new ArrayList<Stafflistbean>();
        try {

            for (int i = 0; i < jsonArray.length(); i++) {
                Stafflistbean p = parseleavelistObject(jsonArray.getJSONObject(i));
                if (p != null) {
                    list.add(p);
                }
            }
        } catch (Exception e) {
            DebugLog.printLog("exp",e.getMessage());
            e.printStackTrace();
        }
        return list;

    }

    private static Stafflistbean parseleavelistObject(JSONObject jsonObject) {
        Stafflistbean msgObj = new Stafflistbean();
        try {
            if (jsonObject.has(ID)) {
                msgObj.setId(jsonObject.getString(ID));
            }
            if (jsonObject.has(STAFFID) && !jsonObject.getString(STAFFID).isEmpty() && !jsonObject.getString(STAFFID).equalsIgnoreCase("null")) {
                msgObj.setStaff_id(jsonObject.getString(STAFFID));
            }

            if (jsonObject.has(FROMDATE) && !jsonObject.getString(FROMDATE).isEmpty() && !jsonObject.getString(FROMDATE).equalsIgnoreCase("null")) {
                msgObj.setFrom_date(jsonObject.getString(FROMDATE));
            }
            if (jsonObject.has(MESSAGE) && !jsonObject.getString(MESSAGE).isEmpty() && !jsonObject.getString(MESSAGE).equalsIgnoreCase("null")) {
                msgObj.setMsg(jsonObject.getString(MESSAGE));
            }
            if (jsonObject.has(TODATE) && !jsonObject.getString(TODATE).isEmpty() && !jsonObject.getString(TODATE).equalsIgnoreCase("null")) {
                msgObj.setTo_date(jsonObject.getString(TODATE));
            }

            if (jsonObject.has("rowcount") && !jsonObject.getString("rowcount").isEmpty() && !jsonObject.getString("rowcount").equalsIgnoreCase("null")) {
                msgObj.setRowcount(jsonObject.getString("rowcount"));
            }


            if (jsonObject.has(NAME) && !jsonObject.getString(NAME).isEmpty() && !jsonObject.getString(NAME).equalsIgnoreCase("null")) {
                msgObj.setTo_name(jsonObject.getString(NAME));
            }
            if (jsonObject.has(APPROVE_STATUS) && !jsonObject.getString(APPROVE_STATUS).isEmpty() && !jsonObject.getString(APPROVE_STATUS).equalsIgnoreCase("null")) {
                msgObj.setApprove(jsonObject.getString(APPROVE_STATUS));
            }
            if (jsonObject.has("depart") && !jsonObject.getString("depart").isEmpty() && !jsonObject.getString("depart").equalsIgnoreCase("null")) {
                msgObj.setDepart(jsonObject.getString("depart"));
            }
            if (jsonObject.has("profile") && !jsonObject.getString("profile").isEmpty() && !jsonObject.getString("profile").equalsIgnoreCase("null")) {
                msgObj.setProfile(Constants.getImageBaseURL()+jsonObject.getString("profile"));
            }
            if (jsonObject.has("gender") && !jsonObject.getString("gender").isEmpty() && !jsonObject.getString("gender").equalsIgnoreCase("null")) {
                msgObj.setGender(jsonObject.getString("gender"));
            }

            if (jsonObject.has("l_type") && !jsonObject.getString("l_type").isEmpty() && !jsonObject.getString("l_type").equalsIgnoreCase("null")) {
                msgObj.setL_type(jsonObject.getString("l_type"));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return msgObj;
    }


    public String getDepart() {
        return depart;
    }

    public void setDepart(String depart) {
        this.depart = depart;
    }

}
