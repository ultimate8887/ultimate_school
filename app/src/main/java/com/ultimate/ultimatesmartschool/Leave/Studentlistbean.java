package com.ultimate.ultimatesmartschool.Leave;

import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.DebugLog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Studentlistbean {

    private static String ID="id";
    private static String STUDENTID="s_id";
    private static String MESSAGE="msg";
    private static String FROMDATE="from_date";
    private static String TODATE="to_date";
    private static String NAME="to_name";
    private static String APPROVESTATUS="approve_status";
    private static String CLASSNAME="class_name";


    public String getRowcount() {
        return rowcount;
    }

    public void setRowcount(String rowcount) {
        this.rowcount = rowcount;
    }

    private String rowcount;

    private String id;
    private String s_id;
    private String msg;
    private String from_date;
    private String to_date;
    private String status;
    private String approve_status;
    private String to_name;
    private String section_name;

    private String profile;

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    private String gender;

    public String getSection_name() {
        return section_name;
    }

    public void setSection_name(String section_name) {
        this.section_name = section_name;
    }

    public String getRollno() {
        return rollno;
    }

    public void setRollno(String rollno) {
        this.rollno = rollno;
    }

    private String   rollno;
    /**
     * class_name : NURSERY1
     */
    public String getCreate_on() {
        return create_on;
    }

    public void setCreate_on(String create_on) {
        this.create_on = create_on;
    }

    private String create_on;
    private String class_name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getS_id() {
        return s_id;
    }

    public void setS_id(String s_id) {
        this.s_id = s_id;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getFrom_date() {
        return from_date;
    }

    public void setFrom_date(String from_date) {
        this.from_date = from_date;
    }

    public String getTo_date() {
        return to_date;
    }

    public void setTo_date(String to_date) {
        this.to_date = to_date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getApprove_status() {
        return approve_status;
    }

    public void setApprove_status(String approve_status) {
        this.approve_status = approve_status;
    }

    public String getTo_name() {
        return to_name;
    }

    public void setTo_name(String to_name) {
        this.to_name = to_name;
    }

    public String getL_type() {
        return l_type;
    }

    public void setL_type(String l_type) {
        this.l_type = l_type;
    }

    private String l_type;

    public static ArrayList<Studentlistbean> parseleavelistArray(JSONArray jsonArray) {
        ArrayList<Studentlistbean> list = new ArrayList<Studentlistbean>();
        try {

            for (int i = 0; i < jsonArray.length(); i++) {
                Studentlistbean p = parseleavelistObject(jsonArray.getJSONObject(i));
                if (p != null) {
                    list.add(p);
                }
            }
        } catch (Exception e) {
            DebugLog.printLog("exp",e.getMessage());
            e.printStackTrace();
        }
        return list;

    }

    private static Studentlistbean parseleavelistObject(JSONObject jsonObject) {
        Studentlistbean msgObj = new Studentlistbean();
        try {
            if (jsonObject.has(ID)) {
                msgObj.setId(jsonObject.getString(ID));
            }
            if (jsonObject.has(STUDENTID)) {
                msgObj.setS_id(jsonObject.getString(STUDENTID));
            }
            if (jsonObject.has(FROMDATE) && !jsonObject.getString(FROMDATE).isEmpty() && !jsonObject.getString(FROMDATE).equalsIgnoreCase("null")) {
                msgObj.setFrom_date(jsonObject.getString(FROMDATE));
            }
            if (jsonObject.has(MESSAGE) && !jsonObject.getString(MESSAGE).isEmpty() && !jsonObject.getString(MESSAGE).equalsIgnoreCase("null")) {
                msgObj.setMsg(jsonObject.getString(MESSAGE));
            }
            if (jsonObject.has(TODATE) && !jsonObject.getString(TODATE).isEmpty() && !jsonObject.getString(TODATE).equalsIgnoreCase("null")) {
                msgObj.setTo_date(jsonObject.getString(TODATE));
            }

            if (jsonObject.has(NAME) && !jsonObject.getString(NAME).isEmpty() && !jsonObject.getString(NAME).equalsIgnoreCase("null")) {
                msgObj.setTo_name(jsonObject.getString(NAME));
            }

            if (jsonObject.has("l_type") && !jsonObject.getString("l_type").isEmpty() && !jsonObject.getString("l_type").equalsIgnoreCase("null")) {
                msgObj.setL_type(jsonObject.getString("l_type"));
            }

            if (jsonObject.has("rowcount") && !jsonObject.getString("rowcount").isEmpty() && !jsonObject.getString("rowcount").equalsIgnoreCase("null")) {
                msgObj.setRowcount(jsonObject.getString("rowcount"));
            }



            if (jsonObject.has(APPROVESTATUS) && !jsonObject.getString(APPROVESTATUS).isEmpty() && !jsonObject.getString(APPROVESTATUS).equalsIgnoreCase("null")) {
                msgObj.setApprove_status(jsonObject.getString(APPROVESTATUS));
            }
            if (jsonObject.has(CLASSNAME) && !jsonObject.getString(CLASSNAME).isEmpty() && !jsonObject.getString(CLASSNAME).equalsIgnoreCase("null")) {
                msgObj.setClass_name(jsonObject.getString(CLASSNAME));
            }
            if (jsonObject.has("rollno") && !jsonObject.getString("rollno").isEmpty() && !jsonObject.getString("rollno").equalsIgnoreCase("null")) {
                msgObj.setRollno(jsonObject.getString("rollno"));
            }
            if (jsonObject.has("section_name") && !jsonObject.getString("section_name").isEmpty() && !jsonObject.getString("section_name").equalsIgnoreCase("null")) {
                msgObj.setSection_name(jsonObject.getString("section_name"));
            }

            if (jsonObject.has("create_on") && !jsonObject.getString("create_on").isEmpty() && !jsonObject.getString("create_on").equalsIgnoreCase("null")) {
                msgObj.setCreate_on(jsonObject.getString("create_on"));
            }

            if (jsonObject.has("profile") && !jsonObject.getString("profile").isEmpty() && !jsonObject.getString("profile").equalsIgnoreCase("null")) {
                msgObj.setProfile(Constants.getImageBaseURL()+jsonObject.getString("profile"));
            }
            if (jsonObject.has("gender") && !jsonObject.getString("gender").isEmpty() && !jsonObject.getString("gender").equalsIgnoreCase("null")) {
                msgObj.setGender(jsonObject.getString("gender"));
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }
        return msgObj;
    }


    public String getClass_name() {
        return class_name;
    }

    public void setClass_name(String class_name) {
        this.class_name = class_name;
    }
}
