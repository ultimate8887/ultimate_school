package com.ultimate.ultimatesmartschool.Leave;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ultimate.ultimatesmartschool.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Leave_list_Adapter extends RecyclerView.Adapter<Leave_list_Adapter.Viewholder> {
    ArrayList<Leavelistbean> leavelist;
    Context context;

    public Leave_list_Adapter(ArrayList<Leavelistbean> leavelist, Context context) {
        this.context=context;
        this.leavelist=leavelist;
    }

    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.leavelst_adapt_lay,parent,false);
        Leave_list_Adapter.Viewholder viewholder=new Leave_list_Adapter.Viewholder(view);
        return viewholder;
    }

    @Override
    public void onBindViewHolder(@NonNull Viewholder holder, int position) {
        holder.name.setText("Name:"+leavelist.get(position).getTo_name());
        holder.reason.setText("Reason:"+leavelist.get(position).getMsg());
        if(leavelist.get(position).getTo_date().equalsIgnoreCase("0000-00-00")){
            holder.date.setText("LeaveDate:"+leavelist.get(position).getFrom_date());

        }else {
            holder.date.setText("LeaveDate:" + leavelist.get(position).getFrom_date() + " " + "to" + " " + leavelist.get(position).getTo_date());
        }
    }

    @Override
    public int getItemCount() {
        return leavelist.size() ;
    }

    public void setleavelstList(ArrayList<Leavelistbean> leavelist) {
        this.leavelist = leavelist;
    }

    public class Viewholder extends RecyclerView.ViewHolder {
        @BindView(R.id.txtName)
        TextView name;
        @BindView(R.id.txtDate)TextView date;
        @BindView(R.id.txtReason)TextView reason;
        public Viewholder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }


}
