package com.ultimate.ultimatesmartschool.Leave;

import com.ultimate.ultimatesmartschool.Utility.DebugLog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Leavelistbean {

    private static String ID="id";
    private static String STUDENTID="s_id";
    private static String MESSAGE="msg";
    private static String FROMDATE="from_date";
    private static String TODATE="to_date";
    private static String NAME="to_name";
    private static String APPROVESTATUS="approve_status";
    /**
     * id : 1
     * s_id : 1
     * msg : sdgdfhgfdhgfhf
     * from_date : 2018-02-02
     * to_date : 2018-02-02
     * status : active
     * approve_status : unapproved
     * to_name : Yadhvi Rajput
     */

    private String id;
    private String s_id;
    private String msg;
    private String from_date;
    private String to_date;
    private String status;
    private String approve_status;
    private String to_name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getS_id() {
        return s_id;
    }

    public void setS_id(String s_id) {
        this.s_id = s_id;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getFrom_date() {
        return from_date;
    }

    public void setFrom_date(String from_date) {
        this.from_date = from_date;
    }

    public String getTo_date() {
        return to_date;
    }

    public void setTo_date(String to_date) {
        this.to_date = to_date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getApprove_status() {
        return approve_status;
    }

    public void setApprove_status(String approve_status) {
        this.approve_status = approve_status;
    }

    public String getTo_name() {
        return to_name;
    }

    public void setTo_name(String to_name) {
        this.to_name = to_name;
    }



    public static ArrayList<Leavelistbean> parseleavelistArray(JSONArray jsonArray) {
        ArrayList<Leavelistbean> list = new ArrayList<Leavelistbean>();
        try {

            for (int i = 0; i < jsonArray.length(); i++) {
                Leavelistbean p = parseleavelistObject(jsonArray.getJSONObject(i));
                if (p != null) {
                    list.add(p);
                }
            }
        } catch (Exception e) {
            DebugLog.printLog("exp",e.getMessage());
            e.printStackTrace();
        }
        return list;

    }

    private static Leavelistbean parseleavelistObject(JSONObject jsonObject) {
        Leavelistbean msgObj = new Leavelistbean();
        try {

            if (jsonObject.has(FROMDATE) && !jsonObject.getString(FROMDATE).isEmpty() && !jsonObject.getString(FROMDATE).equalsIgnoreCase("null")) {
                msgObj.setFrom_date(jsonObject.getString(FROMDATE));
            }
            if (jsonObject.has(MESSAGE) && !jsonObject.getString(MESSAGE).isEmpty() && !jsonObject.getString(MESSAGE).equalsIgnoreCase("null")) {
                msgObj.setMsg(jsonObject.getString(MESSAGE));
            }
            if (jsonObject.has(TODATE) && !jsonObject.getString(TODATE).isEmpty() && !jsonObject.getString(TODATE).equalsIgnoreCase("null")) {
                msgObj.setTo_date(jsonObject.getString(TODATE));
            }

            if (jsonObject.has(NAME) && !jsonObject.getString(NAME).isEmpty() && !jsonObject.getString(NAME).equalsIgnoreCase("null")) {
                msgObj.setTo_name(jsonObject.getString(NAME));
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }
        return msgObj;
    }


}
