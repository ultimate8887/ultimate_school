package com.ultimate.ultimatesmartschool.Leave;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.github.siyamed.shapeimageview.CircularImageView;
import com.squareup.picasso.Picasso;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Studentleaveadapter extends RecyclerView.Adapter<Studentleaveadapter.Viewholder> {
    ArrayList<Studentlistbean> leavelist;
    Context context;
    Mycallback mAdaptercall;

    public Studentleaveadapter(ArrayList<Studentlistbean> leavelist, Context context,Mycallback mAdaptercall) {
        this.context=context;
        this.leavelist=leavelist;
        this.mAdaptercall= mAdaptercall;
    }

    @NonNull
    @Override
    public Studentleaveadapter.Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.new_leavelst_adapt_lay,parent,false);
        Studentleaveadapter.Viewholder viewholder=new Studentleaveadapter.Viewholder(view);
        return viewholder;
    }

    @Override
    public void onBindViewHolder(@NonNull Studentleaveadapter.Viewholder holder, @SuppressLint("RecyclerView") final int position) {
        if(leavelist.get(position).getS_id()!=null) {
            holder.name.setText(leavelist.get(position).getTo_name() + "(" + leavelist.get(position).getS_id() + ")");
        }else{
            holder.name.setText(leavelist.get(position).getTo_name());
        }
        holder.reason.setText(leavelist.get(position).getMsg());

        holder.reason.setEnabled(false);
        if (leavelist.get(position).getId() != null) {
            String title = getColoredSpanned("Leave ID: ", "#000000");
            String Name = getColoredSpanned(leavelist.get(position).getId(), "#5A5C59");
            holder.txtRollNo.setText(Html.fromHtml(title + " " + Name));
        }


        if (leavelist.get(position).getGender().equalsIgnoreCase("Male")) {
            if (leavelist.get(position).getProfile()!=null) {
                Picasso.get().load(leavelist.get(position).getProfile()).placeholder(R.drawable.stud).into(holder.visitimage);
            } else {
                Picasso.get().load(R.drawable.stud).into(holder.visitimage);
            }
        }else{
            if (leavelist.get(position).getProfile()!=null) {
                Picasso.get().load(leavelist.get(position).getProfile()).placeholder(R.drawable.f_student).into(holder.visitimage);

                //  Log.i("USERDATA",User.getCurrentUser().getProfile());
                Log.i("USERDATA-1",leavelist.get(position).getProfile());

            } else {
                Picasso.get().load(R.drawable.f_student).into(holder.visitimage);
            }
        }
      //  holder.txtRollNo.setText("Reg no. :"+leavelist.get(position).getS_id());
        if (!leavelist.get(position).getCreate_on().equalsIgnoreCase("empty")){
            holder.applyT.setVisibility(View.VISIBLE);
            holder.app1.setVisibility(View.VISIBLE);
            holder.applyT.setText(Utils.getDateTimeFormatedWithAMPM(leavelist.get(position).getCreate_on()));
        }else {
            holder.applyT.setVisibility(View.GONE);
            holder.app1.setVisibility(View.GONE);
        }

        if(leavelist.get(position).getSection_name()!=null) {
            holder.classname.setText(leavelist.get(position).getClass_name()+"("+leavelist.get(position).getSection_name()+")");
        }else{
            holder.classname.setText(leavelist.get(position).getClass_name());
        }

        if(leavelist.get(position).getApprove_status().equalsIgnoreCase("approved")){
            holder.aproveimage.setVisibility(View.VISIBLE);
            holder.reason.setTextColor(ContextCompat.getColor(context, R.color.present));
        }else{
            holder.aproveimage.setVisibility(View.GONE);
        }
        if(leavelist.get(position).getApprove_status().equalsIgnoreCase("decline")){
            holder.rejecteed.setVisibility(View.VISIBLE);
            holder.reason.setTextColor(ContextCompat.getColor(context, R.color.orange));
        }else {
            holder.rejecteed.setVisibility(View.GONE);
        }

        if(leavelist.get(position).getApprove_status().equalsIgnoreCase("unapproved")){
            holder.approve.setVisibility(View.VISIBLE);
            holder.unapprove.setVisibility(View.VISIBLE);
        }else{
            holder.approve.setVisibility(View.INVISIBLE);
            holder.unapprove.setVisibility(View.INVISIBLE);
        }

        holder.approve.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
                builder1.setMessage("Do you want to approve? ");
                builder1.setCancelable(false);
                builder1.setPositiveButton(
                        "Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                                if (mAdaptercall != null) {
                                    mAdaptercall.onApproveCallback(leavelist.get(position));
                                }
                            }
                        });
                builder1.setNegativeButton(
                        "No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        });
                AlertDialog alert11 = builder1.create();
                alert11.show();
            }
        });



        holder.unapprove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
                builder1.setMessage("Do you want to decline? ");
                builder1.setCancelable(false);
                builder1.setPositiveButton(
                        "Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                                if (mAdaptercall != null) {
                                    mAdaptercall.onUNApproveCallback(leavelist.get(position));
                                    Toast.makeText(context,"declined",Toast.LENGTH_SHORT).show();
                                }
                                // mAdapterCallback.deleteNotification(notificationList.get(postion));
                            }
                        });
                builder1.setNegativeButton(
                        "No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        });
                AlertDialog alert11 = builder1.create();
                alert11.show();

            }
        });

        if(leavelist.get(position).getTo_date().equalsIgnoreCase("0000-00-00")){
            String title = getColoredSpanned("", "#ff0099cc");
            String Name="";
            if (leavelist.get(position).getApprove_status().equalsIgnoreCase("approved")){
                Name = getColoredSpanned(Utils.getDateFormated(leavelist.get(position).getFrom_date()), "#1C8B3B");

            }
            if (leavelist.get(position).getApprove_status().equalsIgnoreCase("decline")){
                Name = getColoredSpanned(Utils.getDateFormated(leavelist.get(position).getFrom_date()), "#F4212C");

            }
            if (leavelist.get(position).getApprove_status().equalsIgnoreCase("unapproved")){
                Name = getColoredSpanned(Utils.getDateFormated(leavelist.get(position).getFrom_date()), "#000000");
            }

            holder.date.setText(Html.fromHtml(title + " " + Name));

            if (leavelist.get(position).getL_type()!=null) {
                holder.apply.setText("Half day");
            }else {
                holder.apply.setText("Full day");
            }

        }else {
            String title = getColoredSpanned("", "#ff0099cc");
            String Name="";
            if (leavelist.get(position).getApprove_status().equalsIgnoreCase("approved")){
                Name = getColoredSpanned(Utils.getDateFormated(leavelist.get(position).getFrom_date())+ " " + "to" + " " + Utils.getDateFormated(leavelist.get(position).getTo_date()), "#1C8B3B");
            }
            if (leavelist.get(position).getApprove_status().equalsIgnoreCase("decline")){
                Name = getColoredSpanned(Utils.getDateFormated(leavelist.get(position).getFrom_date())+ " " + "to" + " " + Utils.getDateFormated(leavelist.get(position).getTo_date()), "#F4212C");
            }
            if (leavelist.get(position).getApprove_status().equalsIgnoreCase("unapproved")){
                Name = getColoredSpanned(Utils.getDateFormated(leavelist.get(position).getFrom_date())+ " " + "to" + " " + Utils.getDateFormated(leavelist.get(position).getTo_date()), "#000000");
            }

            holder.date.setText(Html.fromHtml(title + " " + Name));

            holder.apply.setText("Multiple day");
        }

    }
    public interface Mycallback {

        public void onApproveCallback(Studentlistbean studentlistbean);
        public void onUNApproveCallback(Studentlistbean studentlistbean);
    }


    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }

    @Override
    public int getItemCount() {
        return leavelist.size() ;
    }

    public void setleavelstList(ArrayList<Studentlistbean> leavelist) {
        this.leavelist = leavelist;
    }

    public class Viewholder extends RecyclerView.ViewHolder {

        @BindView(R.id.applyT)
        TextView applyT;
        @BindView(R.id.app1)
        TextView app1;
        @BindView(R.id.txtName)
        TextView name;
        @BindView(R.id.txtDate)TextView date;
        @BindView(R.id.txtReason)
        EditText reason;
        @BindView(R.id.apply)
        TextView apply;
        @BindView(R.id.approve)
        ImageView approve;
        @BindView(R.id.visitimage)
        CircularImageView visitimage;
        @BindView(R.id.unaprove)ImageView unapprove;
        @BindView(R.id.txtRollNo)TextView txtRollNo;
        @BindView(R.id.aprvimg)ImageView aproveimage;
        @BindView(R.id.rejetedimg)ImageView rejecteed;
        @BindView(R.id.txtClass)TextView classname;
        public Viewholder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}
