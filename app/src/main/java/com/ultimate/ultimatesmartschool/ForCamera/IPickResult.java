package com.ultimate.ultimatesmartschool.ForCamera;



/**
 * Created by jrvansuita build 02/12/16.
 */

public interface IPickResult {
    void onPickResult(PickResult r);
}
