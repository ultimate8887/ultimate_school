package com.ultimate.ultimatesmartschool.Student.STD_Fragment;

import static android.content.Context.MODE_PRIVATE;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Environment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetSequence;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;
import com.ultimate.ultimatesmartschool.BeanModule.ClassBean;
import com.ultimate.ultimatesmartschool.BeanModule.Studentbean;
import com.ultimate.ultimatesmartschool.BeanModule.User;
import com.ultimate.ultimatesmartschool.Login.LoginActivity;
import com.ultimate.ultimatesmartschool.MSG_NEW.ClassAdapter_New;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Student.StudentListByroll;
import com.ultimate.ultimatesmartschool.Student.StudentProfile;
import com.ultimate.ultimatesmartschool.Student.StudentProfile_Enquiry;
import com.ultimate.ultimatesmartschool.Student.Studentadapter_Enquiry;
import com.ultimate.ultimatesmartschool.Student.UpdateStudentDetails;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.CommonProgress;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class EnquiryFragment extends Fragment implements Studentadapter_Enquiry.StudentPro {


    RecyclerView NewsRecyclerview;
    //NewsAdapter newsAdapter;
    //List<NewsItem> mData;
    SharedPreferences sharedPreferences;
    FloatingActionButton fabSwitcher;
    boolean isDark = false;
    RelativeLayout rootLayout;
    EditText searchInput ;
    CharSequence search="";
    ArrayList<Studentbean> mData = new ArrayList<>();
    @BindView(R.id.textNorecord)
    TextView txtNorecord;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.root_layout)
    RelativeLayout parent;
    @BindView(R.id.imgBack)
    ImageView back;
    @BindView(R.id.totalRecord)
    TextView totalRecord;
    RecyclerView.LayoutManager layoutManager;
    private Studentadapter_Enquiry newsAdapter;
    public String classname="";
    CommonProgress commonProgress;
    @BindView(R.id.export)
    FloatingActionButton export;
    String from_date = "",classid="";
    @BindView(R.id.adddate)
    TextView cal_text;
    Context context;
    String value="";
    int assign_id=0;

    int loaded = 0;
    public ArrayList<ClassBean> classList = new ArrayList<>();
    ClassAdapter_New classAdapter_new;
    @BindView(R.id.spinner)
    Spinner spinnerClass;

    public EnquiryFragment(String value, int assign_id, Context context) {
        this.context=context;
        this.value=value;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view=  inflater.inflate(R.layout.fragment_enquiry, container, false);
        // Inflate the layout for this fragment
        ButterKnife.bind(this,view);
        commonProgress=new CommonProgress(getActivity());
        fabSwitcher = view.findViewById(R.id.fab_switcher);
        rootLayout = view.findViewById(R.id.root_layout);
        searchInput = view.findViewById(R.id.search_input);
        NewsRecyclerview = view.findViewById(R.id.news_rv);
        txtTitle.setVisibility(View.GONE);
        back.setVisibility(View.GONE);
        //  searchInput.setBackgroundResource(R.drawable.search_input_dark_style);
        rootLayout.setBackgroundColor(getResources().getColor(R.color.white));

        layoutManager = new LinearLayoutManager(getActivity());
        NewsRecyclerview.setLayoutManager(layoutManager);
        // newsAdapter = new NewsAdapter(this,mData);
        newsAdapter = new Studentadapter_Enquiry(mData,getActivity(),this);
        NewsRecyclerview.setAdapter(newsAdapter);
        fetchClass(0);
        //fetchStudent();

        if (!search.toString().isEmpty()){

            newsAdapter.getFilter().filter(search);

        }

        searchInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if(s.length()==0)
                {
                    fetchStudent();
                }else {

                    newsAdapter.getFilter().filter(s);
                    search = s;
                    totalRecord.setText("Search Record:- " +String.valueOf(newsAdapter.stulist.size()));
                    if (newsAdapter.stulist.size()==0){
                        export.setVisibility(View.GONE);
                    }

                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        return view;
    }

    @OnClick(R.id.adddate)
    public void cal_lyttttt() {

        java.util.Calendar cal = java.util.Calendar.getInstance(TimeZone.getDefault()); // Get current date
        DatePickerDialog datePicker = new DatePickerDialog(getActivity(),
                R.style.MyDatePickerDialogTheme, ondate,
                cal.get(java.util.Calendar.YEAR),
                cal.get(java.util.Calendar.MONTH),
                cal.get(java.util.Calendar.DAY_OF_MONTH));
        datePicker.getDatePicker();
        datePicker.setCancelable(false);
        datePicker.show();

    }

    DatePickerDialog.OnDateSetListener ondate = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            java.util.Calendar calendar = java.util.Calendar.getInstance(TimeZone.getDefault());
            calendar.set(year, monthOfYear, dayOfMonth);
            calendar.set(Calendar.HOUR_OF_DAY, 00);
            calendar.set(Calendar.MINUTE, 00);
            calendar.set(Calendar.SECOND, 00);
            Date setdate = calendar.getTime();
            SimpleDateFormat fmtOut = new SimpleDateFormat("dd MMM, yyyy");
            String dateString = fmtOut.format(setdate);
            cal_text.setText(dateString);
            SimpleDateFormat dateFrmOut = new SimpleDateFormat("yyyy-MM-dd");
            from_date = dateFrmOut.format(setdate);
            // Toast.makeText(HomeWorkByDate.this,from_date,Toast.LENGTH_SHORT).show();
            fetchStudent();
        }
    };


    private void fetchClass(int limit) {
        HashMap<String, String> params = new HashMap<String, String>();
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.CLASSLIST_URL, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                if (error == null) {
                    try {
                        classList = ClassBean.parseClassArray(jsonObject.getJSONArray(Constants.CLASSDATA));

                        if (getActivity()!=null){
                            classAdapter_new = new ClassAdapter_New(getActivity(), classList);
                            spinnerClass.setAdapter(classAdapter_new);
                            spinnerClass.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                        if (i > 0) {
                                            classid = classList.get(i - 1).getId();
                                        }else {
                                            classid="";
                                        }
                                        fetchStudent();
                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> adapterView) {

                                }
                            });
                        }  else{
                            // Toast.makeText(getActivity(),"Std_nullllll",Toast.LENGTH_LONG).show();
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
//                Utils.showSnackBar(error.getMessage(), parent);
                }
            }
        }, getActivity(), params);
    }

    public void fetchStudent() {
        loaded++;
        // ErpProgress.showProgressBar(this,"please wait...");
        commonProgress.show();
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("class_id", classid);
        params.put("all", "6");
        if (value.equalsIgnoreCase("p")){
            params.put("tag", "pending");
        }else if (value.equalsIgnoreCase("a")){
            params.put("tag", "done");
        }else {
            params.put("tag", "decline");
        }

        params.put("mdate", from_date);
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.studentlistclass, apiCallback, getActivity(), params);

    }

    ApiHandler.ApiCallback apiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {

            commonProgress.dismiss();
            if (error == null) {

                try {
                    if (mData != null) {
                        mData.clear();
                    }
                    if (mData != null){
                        mData.clear();
                        mData=Studentbean.parseHWArray(jsonObject.getJSONArray("student_list"));
                        classname=mData.get(0).getClass_name();
                        newsAdapter.setHQList(mData);
                        newsAdapter.notifyDataSetChanged();
                        totalRecord.setVisibility(View.VISIBLE);
                        totalRecord.setText("Total Students:- "+String.valueOf(mData.size()));
                        txtNorecord.setVisibility(View.GONE);
                        export.setVisibility(View.VISIBLE);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                mData.clear();
                totalRecord.setText("Total Students:- "+String.valueOf(mData.size()));
                export.setVisibility(View.GONE);
                txtNorecord.setVisibility(View.VISIBLE);
                if (loaded==0) {
                    Toast.makeText(getActivity(), "No record found", Toast.LENGTH_LONG).show();
                }
                newsAdapter.setHQList(mData);
                newsAdapter.notifyDataSetChanged();


            }
        }
    };

    private void setShowcaseViewP() {
        new TapTargetSequence(getActivity())
                .targets(TapTarget.forView(export,"Export Excel!","Tap the Export button to export Students list.")
                        .outerCircleColor(R.color.light)
                        .outerCircleAlpha(0.96f)
                        .targetCircleColor(R.color.white)
                        .titleTextSize(20)
                        .titleTextColor(R.color.white)
                        .descriptionTextSize(14)
                        .descriptionTextColor(R.color.black)
                        .textColor(R.color.black)
                        .textTypeface(Typeface.SANS_SERIF)
                        .dimColor(R.color.black)
                        .drawShadow(true)
                        .cancelable(false)
                        .tintTarget(true)
                        .transparentTarget(true)
                        .targetRadius(40)).listener(new TapTargetSequence.Listener() {
                    @RequiresApi(api = Build.VERSION_CODES.N)
                    @Override
                    public void onSequenceFinish() {
                        // Toast.makeText(getApplicationContext(),"Sequence Finished",Toast.LENGTH_SHORT).show();
                        savePrefDataP();

                    }

                    @Override
                    public void onSequenceStep(TapTarget lastTarget, boolean targetClicked) {
                        // Toast.makeText(getActivity(),"GREAT!",Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onSequenceCanceled(TapTarget lastTarget) {

                    }
                }).start();
    }

    private void savePrefDataP(){
        sharedPreferences=getActivity().getSharedPreferences("boarding_pref_view_export",MODE_PRIVATE);
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putBoolean("IsFirstTimeVisit_view_export",true);
        editor.apply();
    }

    private boolean restorePrefDataP(){
        sharedPreferences = getActivity().getSharedPreferences("boarding_pref_view_export",MODE_PRIVATE);
        return sharedPreferences.getBoolean("IsFirstTimeVisit_view_export",false);
    }

    @SuppressLint("RestrictedApi")
    @OnClick(R.id.export)
    public void submit() {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.S) {
            importData();
        } else {
            if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {

                if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    Toast.makeText(getActivity(), "Please provide storage permission from app settings", Toast.LENGTH_LONG).show();
                } else {

                    ActivityCompat.requestPermissions(getActivity(),
                            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            1);
                }
            } else {
                importData();

            }
        }
    }

    @SuppressLint("RestrictedApi")
    private void importData() {
        Workbook wb = new HSSFWorkbook();

        Cell cell = null;

        String ndate="",sub_staff="",sub_date="";

        sub_staff="Admission";

        Sheet sheet = null;
        sheet = wb.createSheet(sub_staff+" Enquiry List");
        //Now column and row
        Row row = sheet.createRow(0);

        cell = row.createCell(0);
        cell.setCellValue("S No");

        cell = row.createCell(1);
        cell.setCellValue("Enquiry No");

        cell = row.createCell(2);
        cell.setCellValue("Preferred Class");

        cell = row.createCell(3);
        cell.setCellValue("Student Name");

        cell = row.createCell(4);
        cell.setCellValue("Father Name");

        cell = row.createCell(5);
        cell.setCellValue("Phone");

        cell = row.createCell(6);
        cell.setCellValue("Enquiry Date");

        cell = row.createCell(7);
        cell.setCellValue("Previous Class");

        cell = row.createCell(8);
        cell.setCellValue("Previous School");

        cell = row.createCell(9);
        cell.setCellValue("City/Village");

        cell = row.createCell(10);
        cell.setCellValue("Admission Status");


        //column width
        sheet.setColumnWidth(0, (20 * 100));
        sheet.setColumnWidth(1, (20 * 150));
        sheet.setColumnWidth(2, (30 * 150));
        sheet.setColumnWidth(3, (30 * 250));
        sheet.setColumnWidth(4, (30 * 200));
        sheet.setColumnWidth(5, (20 * 100));
        sheet.setColumnWidth(6, (20 * 100));
        sheet.setColumnWidth(7, (20 * 100));
        sheet.setColumnWidth(8, (20 * 200));
        sheet.setColumnWidth(9, (20 * 200));
        sheet.setColumnWidth(10, (20 * 200));

        for (int i = 0; i < mData.size(); i++) {

            Row row1 = sheet.createRow(i + 1);

            cell = row1.createCell(0);
            cell.setCellValue(i+1);

            cell = row1.createCell(1);
            cell.setCellValue(mData.get(i).getId());

            cell = row1.createCell(2);
            cell.setCellValue(mData.get(i).getClass_name());

            cell = row1.createCell(3);
            cell.setCellValue((mData.get(i).getName()));
            //  cell.setCellStyle(cellStyle);

            cell = row1.createCell(4);
            cell.setCellValue(mData.get(i).getFather_name());

            cell = row1.createCell(5);
            cell.setCellValue(mData.get(i).getPhoneno());

            cell = row1.createCell(6);
            cell.setCellValue(mData.get(i).getAdmissiondate());

            cell = row1.createCell(7);
            cell.setCellValue(mData.get(i).getPre_prev_class());

            cell = row1.createCell(8);
            cell.setCellValue(mData.get(i).getPre_prev_school());

            cell = row1.createCell(9);
            cell.setCellValue(mData.get(i).getPr_city());

            cell = row1.createCell(10);
            if (value.equalsIgnoreCase("p")){
              //  params.put("tag", "pending");
                cell.setCellValue("Pending");
            }else if (value.equalsIgnoreCase("a")){
              //  params.put("tag", "done");
                cell.setCellValue("done");
            }else {
               // params.put("tag", "decline");
                cell.setCellValue("decline");
            }




            sheet.setColumnWidth(0, (20 * 100));
            sheet.setColumnWidth(1, (20 * 150));
            sheet.setColumnWidth(2, (30 * 150));
            sheet.setColumnWidth(3, (30 * 250));
            sheet.setColumnWidth(4, (30 * 200));
            sheet.setColumnWidth(5, (20 * 100));
            sheet.setColumnWidth(6, (20 * 100));
            sheet.setColumnWidth(7, (20 * 100));
            sheet.setColumnWidth(8, (20 * 200));
            sheet.setColumnWidth(9, (20 * 200));
            sheet.setColumnWidth(10, (20 * 200));

        }
        String fileName;

        fileName = sub_staff+"_students_list_"+ System.currentTimeMillis() + ".xls";

        // File file = new File(getActivity().getExternalFilesDir(null), fileName);

        File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS), fileName);

        FileOutputStream fileOutputStream = null;

        try {
            fileOutputStream = new FileOutputStream(file);
            wb.write(fileOutputStream);
            Utils.openSuccessDialog("Success!","Export file in "+file,"",getActivity());
        } catch (IOException e) {
            Utils.openErrorDialog("Error writing Exception: "+ e,getActivity());
        } catch (Exception e) {
            Utils.openErrorDialog("Failed to save file due to Exception: "+ e,getActivity());
        } finally {
            try {
                if (null != fileOutputStream) {
                    fileOutputStream.close();
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

    }

    @SuppressLint("RestrictedApi")
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == 1 && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            importData();
        } else {
            Toast.makeText(getActivity(), "Permission Denied", Toast.LENGTH_SHORT).show();
        }
    }



    @Override
    public void StudentProfile(Studentbean std) {
        Gson gson  = new Gson();
        String student = gson.toJson(std,Studentbean.class);
        Intent i = new Intent(getActivity(), StudentProfile_Enquiry.class);
        i.putExtra("student",student);
        startActivity(i);

    }

    @Override
    public void onUpdateCallback(Studentbean std) {
        //   Toast.makeText(getApplicationContext(),"Enter!",Toast.LENGTH_SHORT).show();
        Gson gson  = new Gson();
        String student = gson.toJson(std,Studentbean.class);
        Intent i = new Intent(getActivity(), StudentProfile_Enquiry.class);
        i.putExtra("student",student);
        startActivity(i);

    }

    @Override
    public void onResume() {
        super.onResume();
      //  fetchStudent();
    }

    @Override
    public void onBlockCallback(Studentbean std) {

    }


    @Override
    public void onDeleteCallback(Studentbean std) {

    }

    @Override
    public void stdCallBack(Studentbean std) {
        String phoneNo = std.getPhoneno();
        Log.i("email",phoneNo);
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:"+ phoneNo));
        startActivity(intent);
    }

    @Override
    public void stdEmailCallBack(Studentbean std) {

//        String email = std.getEmail();
//        Log.i("email",email);
//        Intent intent = new Intent(Intent.ACTION_SENDTO); // it's not ACTION_SEND
//        intent.setType("text/plain");
//        intent.putExtra(Intent.EXTRA_SUBJECT, "Subject of email");
//        intent.putExtra(Intent.EXTRA_TEXT, "Body of email");
//        intent.setData(Uri.parse("mailto:"+email)); // or just "mailto:" for blank
//        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK); // this will make such that when user returns to your app, your app is displayed, instead of the email app.
//        startActivity(intent);

//        String phoneNo = std.getPhoneno();
//        Uri uri = Uri.parse("smsto:" + phoneNo);
//        Intent i = new Intent(Intent.ACTION_SENDTO, uri);
//        i.setPackage("com.whatsapp");
//        startActivity(Intent.createChooser(i, "Chat Now!"));
        String phoneNo = std.getPhoneno();
        Uri uri = Uri.parse("smsto:" + phoneNo);
        Intent intent = new Intent(Intent.ACTION_SENDTO, uri);
        intent.setPackage("com.android.mms"); // Use the package name for the default messaging app
        startActivity(intent);

    }
}