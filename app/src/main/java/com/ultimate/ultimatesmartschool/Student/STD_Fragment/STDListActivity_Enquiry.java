package com.ultimate.ultimatesmartschool.Student.STD_Fragment;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.tabs.TabLayout;
import com.ultimate.ultimatesmartschool.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class STDListActivity_Enquiry extends AppCompatActivity {
    @BindView(R.id.imgBack)
    ImageView imgBackks;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    private Animation animation;

    @BindView(R.id.viewpager)
    ViewPager viewpager;
    @BindView(R.id.tab)
    TabLayout tablayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_s_t_d_list);
        ButterKnife.bind(this);
        txtTitle.setText("Admission Enquiry List");
        animation = AnimationUtils.loadAnimation(STDListActivity_Enquiry.this, R.anim.btn_blink_animation);
        setupTabPager();
    }
    private void setupTabPager() {

        viewpager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tablayout));

        tablayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(viewpager) {

            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                super.onTabSelected(tab);
                String value=String.valueOf(tab.getText());

                if (value.equalsIgnoreCase("Pending")){
                    tablayout.setTabTextColors(

                            // Unselected Tab Text Color
                            ContextCompat.getColor(STDListActivity_Enquiry.this, R.color.d_light),
                            // Selected Tab Text Color
                            ContextCompat.getColor(STDListActivity_Enquiry.this, R.color.white)
                    );
                    // Selected Tab Indicator Color
                    //   tablayout.setSelectedTabIndicatorColor(getResources().getColor(R.color.leave));
                    //  tablayout.setSelectedTabIndicatorHeight(5);
                }else if (value.equalsIgnoreCase("Done")) {
                    tablayout.setTabTextColors(

                            // Unselected Tab Text Color
                            ContextCompat.getColor(STDListActivity_Enquiry.this, R.color.d_light),
                            // Selected Tab Text Color
                            ContextCompat.getColor(STDListActivity_Enquiry.this, R.color.green)
                    );
                    // Selected Tab Indicator Color
                    //  tablayout.setSelectedTabIndicatorColor(getResources().getColor(R.color.present));
                    //tablayout.setSelectedTabIndicatorHeight(5);
                }else {
                    tablayout.setTabTextColors(

                            // Unselected Tab Text Color
                            ContextCompat.getColor(STDListActivity_Enquiry.this, R.color.d_light),
                            // Selected Tab Text Color
                            ContextCompat.getColor(STDListActivity_Enquiry.this, R.color.light_red)
                    );
                    // Selected Tab Indicator Color
                    //  tablayout.setSelectedTabIndicatorColor(getResources().getColor(R.color.absent));
                    //tablayout.setSelectedTabIndicatorHeight(5);
                }

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                super.onTabUnselected(tab);
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                super.onTabReselected(tab);
            }
        });



        StdTabPager_Enquiry adapter;
        adapter = new StdTabPager_Enquiry(getSupportFragmentManager(),0,this);
        viewpager.setAdapter(adapter);
        tablayout.setupWithViewPager(viewpager);
    }

    @OnClick(R.id.imgBack)
    public void imgBackks()  {
        imgBackks.startAnimation(animation);
        finish();
    }
}