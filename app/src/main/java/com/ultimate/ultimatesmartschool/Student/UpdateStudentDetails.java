package com.ultimate.ultimatesmartschool.Student;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import com.ultimate.ultimatesmartschool.AddmissionForm.HouseSpinnerAdapter;
import com.ultimate.ultimatesmartschool.AddmissionForm.StudentAdmissionForm;
import com.ultimate.ultimatesmartschool.BeanModule.CasteBean;
import com.ultimate.ultimatesmartschool.BeanModule.ClassBean;
import com.ultimate.ultimatesmartschool.BeanModule.CommonBean;
import com.ultimate.ultimatesmartschool.BeanModule.SectionBean;
import com.ultimate.ultimatesmartschool.BeanModule.Studentbean;
import com.ultimate.ultimatesmartschool.BeanModule.User;
import com.ultimate.ultimatesmartschool.Examination.ClassAdapter;
import com.ultimate.ultimatesmartschool.ForCamera.IPickResult;
import com.ultimate.ultimatesmartschool.ForCamera.PickImageDialog;
import com.ultimate.ultimatesmartschool.ForCamera.PickResult;
import com.ultimate.ultimatesmartschool.ForCamera.PickSetup;
import com.ultimate.ultimatesmartschool.Homework.SectionnewAdapter;
import com.ultimate.ultimatesmartschool.Login.LoginActivity;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Staff.CategoryAdapter;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;
import com.ultimate.ultimatesmartschool.Utility.ErpProgressVert;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class UpdateStudentDetails extends AppCompatActivity implements IPickResult {


    @BindView(R.id.spinnerCategory)
    Spinner spinnerCategory;
    @BindView(R.id.spinnerClass)
    Spinner spinnerClass;

    @BindView(R.id.edtFirstName)
    EditText edtFirstName;
    @BindView(R.id.edtlastName)
    EditText edtlastName;
    @BindView(R.id.edtDob)
    EditText edtDob;

    @BindView(R.id.edtAddress)
    EditText edtAddress;
    @BindView(R.id.edtArea)
    EditText edtArea;
    @BindView(R.id.edtCity)
    EditText edtCity;
    @BindView(R.id.edtState)
    EditText edtState;
    @BindView(R.id.edtpinCode)
    EditText edtpinCode;
    @BindView(R.id.edtEmail)
    EditText edtEmail;
    @BindView(R.id.edtPhone)
    EditText edtPhone;
    @BindView(R.id.edtFatherName)
    EditText edtFatherName;
    @BindView(R.id.edtFatherPhone)
    EditText edtFatherPhone;
    @BindView(R.id.edtMotherName)
    EditText edtMotherName;
    @BindView(R.id.edtMotherPhone)
    EditText edtMotherPhone;
    @BindView(R.id.parent)
    RelativeLayout parent;

    @BindView(R.id.edtRoll)
    EditText edtRoll;
    @BindView(R.id.txtRegNo)
    TextView txtRegNo;


    @BindView(R.id.imgBack)
    ImageView imgBack;

    @BindView(R.id.spinnerSection)
    Spinner spinnersection;
    ArrayList<SectionBean> sectionList = new ArrayList<>();
    String sectionid="";
    String sectionname="";
    @BindView(R.id.imgUser)
    CircularImageView imgUser;
    @BindView(R.id.buylayoutgfggf)
    CardView buylayoutgfggf;

    private Bitmap userBitmap;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    private int type;
    private Bitmap hwbitmap,hwbitmap10,hwbitmap12,hwbitmapschlcert,hwbitmapschltrans,hwbitmapgrad;

    private ArrayList<CommonBean> groupList = new ArrayList<>();
    @BindView(R.id.spinnerHouse)
    Spinner spinnerHouse;
    public String feeid = "",houseid="";

    ArrayList<CasteBean> categorylist = new ArrayList<>();
    ArrayList<ClassBean> classList = new ArrayList<>();
    CasteBean casteBean;
    ClassBean classBean;
    String classid = "";
    public String birthdate = "";
    String gender = "";
    String title = "";
    String casteid = "";

    Studentbean user;
    Animation animation;
    @BindView(R.id.submit)
    TextView submit;


    @BindView(R.id.edit_imageaadhar)
    ImageView edit_imageaadhar;
    @BindView(R.id.imageaadhar)
    ImageView imageaadhar;

    @BindView(R.id.edit_image10th)
    ImageView edit_image10th;
    @BindView(R.id.image10th)
    ImageView image10th;

    @BindView(R.id.edit_image12th)
    ImageView edit_image12th;
    @BindView(R.id.image12th)
    ImageView image12th;

    @BindView(R.id.edit_imageschcert)
    ImageView edit_imageschcert;
    @BindView(R.id.imageschcert)
    ImageView imageschcert;

    @BindView(R.id.edit_imageschtrans)
    ImageView edit_imageschtrans;
    @BindView(R.id.imageschtrans)
    ImageView imageschtrans;

    @BindView(R.id.edit_imagegrad)
    ImageView edit_imagegrad;
    @BindView(R.id.imagegrad)
    ImageView imagegrad;


    @BindView(R.id.edit_imageschincome)
    ImageView edit_imageschincome;
    @BindView(R.id.imageschincome)
    ImageView imageschincome;

    @BindView(R.id.f_edit_imageaadhar)
    ImageView f_edit_imageaadhar;
    @BindView(R.id.f_imageaadhar)
    ImageView f_imageaadhar;


    @BindView(R.id.m_edit_imageaadhar)
    ImageView m_edit_imageaadhar;
    @BindView(R.id.m_imageaadhar)
    ImageView m_imageaadhar;

    @BindView(R.id.edtHeight)
    EditText edtHeight;
    @BindView(R.id.edtWeight)
    EditText edtWeight;

    @BindView(R.id.doc_lyt)CardView doc_lyt;
    @BindView(R.id.edit_imageschresidence)
    ImageView edit_imageschresidence;
    @BindView(R.id.imageschresidence)
    ImageView imageschresidence;
    String tag="",sec="",cla="",check="";
    private int selection;
    private Bitmap imageaadharBitmap=null, image10thBitmapProf=null, image12thBitmapProf=null, imageschcertBitmapProf=null, imageschtransBitmapProf=null,
            imagegradBitmapProf=null,imageincomeBitmapProf=null,imageresidenceBitmapProf=null,f_imageaadharBitmap=null,m_imageaadharBitmap=null,profileBitmap=null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_student_details);
        ButterKnife.bind(this);
        txtTitle.setText("Update Student Details");

        if(getIntent()!= null && getIntent().hasExtra("student")){
            Gson gson = new Gson();
            user = gson.fromJson(getIntent().getStringExtra("student"),Studentbean.class);
            tag = getIntent().getExtras().getString("tag");
            sec = getIntent().getExtras().getString("sec");
            cla = getIntent().getExtras().getString("class");
            check = getIntent().getExtras().getString("check");
        }else {
            return;
        }

        if (User.getCurrentUser().getSchoolData().getFi_school_id().equalsIgnoreCase("XYZGGSMT")){
            doc_lyt.setVisibility(View.VISIBLE);
        }else{
            doc_lyt.setVisibility(View.GONE);
        }


        animation = AnimationUtils.loadAnimation(UpdateStudentDetails.this, R.anim.btn_blink_animation);
        fetchCategory();
        fetchClass();
        setuserData();
        checkdata();
        fetchSection();
        fetchHouse();
    }

    private void fetchSection() {
        HashMap<String, String> params = new HashMap<String, String>();
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.SECTIONLIST_URL, sectionapiCallback, this, params);
    }

    ApiHandler.ApiCallback sectionapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            if (error == null) {
                try {
                    sectionList = SectionBean.parseCommonArray(jsonObject.getJSONArray("hostelroom_data"));
                    SectionnewAdapter adapter = new SectionnewAdapter(UpdateStudentDetails.this, sectionList);
                    spinnersection.setAdapter(adapter);
                    spinnersection.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if (i > 0) {
                                sectionid = sectionList.get(i - 1).getSection_id();
                                sectionname = sectionList.get(i - 1).getSection_name();
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }


                int i = 0;
                String id=user.getSection_id();
                for ( SectionBean obj : sectionList) {
                    if (obj.getSection_id().equalsIgnoreCase(id)) {
                        spinnersection.setSelection(i + 1);
                        break;
                    }
                    i++;
                }


            } else {
                Utils.showSnackBar(error.getMessage(), parent);
                if (error.getStatusCode() == 405) {
                    User.logout();
                    startActivity(new Intent(UpdateStudentDetails.this, LoginActivity.class));
                    finish();
                }
            }
        }
    };

    private void fetchHouse() {
        ErpProgress.showProgressBar(this, "Please wait...");
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("check","house");
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.GROUP_LIST_URL, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                ErpProgress.cancelProgressBar();
                if (error == null) {

                    try {
                        groupList.clear();
                        groupList = CommonBean.parseCommonArray(jsonObject.getJSONArray(Constants.GROUP_DATA));
                        final HouseSpinnerAdapter adapter = new HouseSpinnerAdapter(UpdateStudentDetails.this, groupList,0);
                        spinnerHouse.setAdapter(adapter);
                        spinnerHouse.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                if (position > 0) {
                                    houseid = groupList.get(position - 1).getId();
                                } else {
                                    houseid="";
                                }
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                    int i = 0;
                    String id=user.getEs_housesid();
                    for ( CommonBean obj : groupList) {
                        if (obj.getId().equalsIgnoreCase(id)) {
                            spinnerHouse.setSelection(i + 1);
                            break;
                        }
                        i++;
                    }

                } else {
                    //Utils.showSnackBar(error.getMessage(), parent);
                }
            }
        }, this, params);
    }


    @OnClick(R.id.imgCal)
    public void fetchDateOfBirth() {
        java.util.Calendar cal = java.util.Calendar.getInstance(TimeZone.getDefault()); // Get current date
        DatePickerDialog datePicker = new DatePickerDialog(this,
                R.style.MyDatePickerDialogTheme, ondate,
                cal.get(java.util.Calendar.YEAR),
                cal.get(java.util.Calendar.MONTH),
                cal.get(java.util.Calendar.DAY_OF_MONTH));
        datePicker.getDatePicker().setMaxDate(new Date().getTime());
        datePicker.setCancelable(false);
        datePicker.show();
    }

    DatePickerDialog.OnDateSetListener ondate = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

            java.util.Calendar calendar = java.util.Calendar.getInstance(TimeZone.getDefault());
            calendar.set(year, monthOfYear, dayOfMonth);
            Date dbirthdate = calendar.getTime();
            Calendar value_datePicker = calendar;
            SimpleDateFormat fmtOut = new SimpleDateFormat("yyyy-MM-dd");
            String dateString = fmtOut.format(dbirthdate);
            birthdate=dateString;
            edtDob.setText(dateString);
        }
    };

    private void checkdata() {

        if (user.getGender().equalsIgnoreCase("Male")) {
            if (user.getProfile() != null) {
                Picasso.get().load(user.getProfile()).placeholder(R.drawable.stud).into(imgUser);
            } else {
                Picasso.get().load(R.drawable.stud).into(imgUser);
            }
        }else{
            if (user.getProfile() != null) {
                Picasso.get().load(user.getProfile()).placeholder(R.drawable.f_student).into(imgUser);
            } else {
                Picasso.get().load(R.drawable.f_student).into(imgUser);
            }
        }

        if (user.getAadhar_image() != null){
            imageaadhar.setVisibility(View.VISIBLE);
            //Picasso.with(getApplicationContext()).load(Constants.getImageBaseURL()+Constants.DOC_URL+jsonObject.getJSONObject("doc_data").getString("aadhar_image")).placeholder(R.color.divider).into(imageaadhar);
            edit_imageaadhar.setImageResource(R.drawable.ic_baseline_edit);

            Utils.setImageUri(getApplicationContext(), Constants.getImageBaseURL()+Constants.DOC_URL+user.getAadhar_image(), imageaadhar);
        }



        if (user.getTenmarksheet_image()!= null){
            image10th.setVisibility(View.VISIBLE);
            // Picasso.with(getApplicationContext()).load(Constants.getImageBaseURL()+Constants.DOC_URL+jsonObject.getJSONObject("doc_data").getString("tenmarksheet_image")).placeholder(R.color.divider).into(image10th);
            edit_image10th.setImageResource(R.drawable.ic_baseline_edit);
            Utils.setImageUri(getApplicationContext(), Constants.getImageBaseURL()+Constants.DOC_URL+user.getTenmarksheet_image(), image10th);

        }

        if (user.getTwelvemarksheet_image()!= null){
            image12th.setVisibility(View.VISIBLE);
            //  Picasso.with(getApplicationContext()).load(Constants.getImageBaseURL()+Constants.DOC_URL+jsonObject.getJSONObject("doc_data").getString("twelvemarksheet_image")).placeholder(R.color.divider).into(image12th);
            edit_image12th.setImageResource(R.drawable.ic_baseline_edit);
            Utils.setImageUri(getApplicationContext(), Constants.getImageBaseURL()+Constants.DOC_URL+user.getTwelvemarksheet_image(), image12th);

        }

        if (user.getSchol_certif_image()!= null){
            imageschcert.setVisibility(View.VISIBLE);
            //    Picasso.with(getApplicationContext()).load(Constants.getImageBaseURL()+Constants.DOC_URL+jsonObject.getJSONObject("doc_data").getString("schol_certif_image")).placeholder(R.color.divider).into(imageschcert);
            edit_imageschcert.setImageResource(R.drawable.ic_baseline_edit);
            Utils.setImageUri(getApplicationContext(), Constants.getImageBaseURL()+Constants.DOC_URL+user.getSchol_certif_image(), imageschcert);

        }

        if (user.getSchol_trans_certif_image()!= null){
            imageschtrans.setVisibility(View.VISIBLE);
            // Picasso.with(getApplicationContext()).load(Constants.getImageBaseURL()+Constants.DOC_URL+jsonObject.getJSONObject("doc_data").getString("schol_trans_certif_image")).placeholder(R.color.divider).into(imageschtrans);
            edit_imageschtrans.setImageResource(R.drawable.ic_baseline_edit);
            Utils.setImageUri(getApplicationContext(), Constants.getImageBaseURL()+Constants.DOC_URL+user.getSchol_trans_certif_image(), imageschtrans);

        }

        if (user.getGraduation_image()!= null){
            imagegrad.setVisibility(View.VISIBLE);
            //  Picasso.with(getApplicationContext()).load(Constants.getImageBaseURL()+Constants.DOC_URL+jsonObject.getJSONObject("doc_data").getString("graduation_image")).placeholder(R.color.divider).into(imagegrad);
            edit_imagegrad.setImageResource(R.drawable.ic_baseline_edit);
            Utils.setImageUri(getApplicationContext(), Constants.getImageBaseURL()+Constants.DOC_URL+user.getGraduation_image(), imagegrad);

        }


    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @OnClick(R.id.buylayoutgfggf)
    public void buylayoutgfggfff() {
        animation = AnimationUtils.loadAnimation(this, R.anim.btn_blink_animation);
        buylayoutgfggf.startAnimation(animation);
      //  selection=11;

    pick_img();
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @SuppressLint("ResourceType")
    private void pick_img() {


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.UPSIDE_DOWN_CAKE) { // Android 14+ (API 34)
           onImageViewClick();

        } else if (Build.VERSION.SDK_INT > Build.VERSION_CODES.S) {
                if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_MEDIA_IMAGES)
                        != PackageManager.PERMISSION_GRANTED) {

                    if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_MEDIA_IMAGES)) {
                        Toast.makeText(this, "Please provide storage permission from app settings", Toast.LENGTH_LONG).show();
                    } else {

                        ActivityCompat.requestPermissions(this,
                                new String[]{Manifest.permission.READ_MEDIA_IMAGES},
                                1);
                    }
                } else {

                    onImageViewClick();
                }

            } else {
                if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {

                    if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                        Toast.makeText(this, "Please provide storage permission from app settings", Toast.LENGTH_LONG).show();
                    } else {

                        ActivityCompat.requestPermissions(this,
                                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                                1);
                    }
                } else {
                    onImageViewClick();

                }
        }
    }

    private void onImageViewClick() {
        type = 1;
        PickSetup setup = new PickSetup();
        // super.customize(setup);
        PickImageDialog.build(setup)
                //.setOnClick(this)
                .show(this);
    }

    @Override
    public void onPickResult(PickResult r) {
        if (r.getError() == null) {
            //If you want the Bitmap.
                imgUser.setImageBitmap(r.getBitmap());
                profileBitmap = r.getBitmap();
                imgUser.setVisibility(View.VISIBLE);
                //  encoded = Utility.encodeToBase64(profileBitmap, Bitmap.CompressFormat.JPEG, 50);
            //r.getPath();
        } else {
            //Handle possible errors
            //TODO: do what you have to do with r.getError();
            Toast.makeText(this, r.getError().getMessage(), Toast.LENGTH_LONG).show();
        }


    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    @OnClick(R.id.edit_imageaadhar)
    public void edit_imageaadhar() {
        animation = AnimationUtils.loadAnimation(this, R.anim.btn_blink_animation);
        edit_imageaadhar.startAnimation(animation);

        selection=1;

    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @OnClick(R.id.edit_image10th)
    public void edit_image10th() {
        animation = AnimationUtils.loadAnimation(this, R.anim.btn_blink_animation);
        edit_image10th.startAnimation(animation);

        selection=2;



    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @OnClick(R.id.edit_image12th)
    public void edit_image12th() {
        animation = AnimationUtils.loadAnimation(this, R.anim.btn_blink_animation);
        edit_image12th.startAnimation(animation);

        selection=3;



    }
    @RequiresApi(api = Build.VERSION_CODES.M)
    @OnClick(R.id.edit_imageschcert)
    public void edit_imageschcert() {
        animation = AnimationUtils.loadAnimation(this, R.anim.btn_blink_animation);
        edit_imageschcert.startAnimation(animation);

        selection=4;



    }
    @RequiresApi(api = Build.VERSION_CODES.M)
    @OnClick(R.id.edit_imageschtrans)
    public void edit_imageschtrans() {
        animation = AnimationUtils.loadAnimation(this, R.anim.btn_blink_animation);
        edit_imageschtrans.startAnimation(animation);

        selection=5;


    }
    @RequiresApi(api = Build.VERSION_CODES.M)
    @OnClick(R.id.edit_imagegrad)
    public void edit_imagegrad() {
        animation = AnimationUtils.loadAnimation(this, R.anim.btn_blink_animation);
        edit_imagegrad.startAnimation(animation);

        selection=6;



    }



    @RequiresApi(api = Build.VERSION_CODES.M)
    @OnClick(R.id.edit_imageschresidence)
    public void edit_imageschresidenceee() {
        animation = AnimationUtils.loadAnimation(this, R.anim.btn_blink_animation);
        edit_imageschresidence.startAnimation(animation);

        selection=7;



    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @OnClick(R.id.edit_imageschincome)
    public void edit_imageschincomeee() {
        animation = AnimationUtils.loadAnimation(this, R.anim.btn_blink_animation);
        edit_imageschincome.startAnimation(animation);

        selection=8;


    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    @OnClick(R.id.f_edit_imageaadhar)
    public void f_edit_imageaadharrr() {
        animation = AnimationUtils.loadAnimation(this, R.anim.btn_blink_animation);
        f_edit_imageaadhar.startAnimation(animation);

        selection=9;



    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @OnClick(R.id.m_edit_imageaadhar)
    public void m_edit_imageaadharrr() {
        animation = AnimationUtils.loadAnimation(this, R.anim.btn_blink_animation);
        m_edit_imageaadhar.startAnimation(animation);

        selection=10;


    }

    @OnClick(R.id.submit)
    public void submitt() {
        submit.startAnimation(animation);
        ErpProgressVert.showProgressBar(UpdateStudentDetails.this,"Please Wait........!");
        HashMap<String, String> params = new HashMap<>();
        params.put("s_id", user.getId());
        String msg="";
        params.put("f_name", edtFirstName.getText().toString());
        params.put("l_name", edtlastName.getText().toString());
        params.put("s_no", edtRoll.getText().toString());
        params.put("house", houseid);
        params.put("city", edtCity.getText().toString());
        params.put("state", edtState.getText().toString());
        params.put("pin", edtpinCode.getText().toString());
        params.put("father", edtFatherName.getText().toString());
        params.put("f_no", edtFatherPhone.getText().toString());
        params.put("phone", edtPhone.getText().toString());
        params.put("mother", edtMotherName.getText().toString());
        params.put("m_no", edtMotherPhone.getText().toString());
        params.put("dob", edtDob.getText().toString());
        params.put("address", edtAddress.getText().toString());
        params.put("aadhar", edtEmail.getText().toString());

        params.put("height", edtHeight.getText().toString());
        params.put("weight", edtWeight.getText().toString());

        if (casteid != null && !casteid.isEmpty()) {
            params.put("caste_id", casteid);
        }else{
            params.put("caste_id", user.getCastid());
        }
        if (classid != null && !casteid.isEmpty()) {
            params.put("class_id", classid);
        }else {
            params.put("class_id", user.getClass_id());
        }

        if (sectionid != null && !sectionid.isEmpty()) {
            params.put("section", sectionid);
        }else {
            params.put("section", user.getSection_id());
        }



        if (profileBitmap != null) {
            String encoded = Utils.encodeToBase64(profileBitmap, Bitmap.CompressFormat.JPEG, 90);
            encoded = String.format("data:image/jpeg;base64,%s", encoded);
            params.put("profile", encoded);
        }else{
            params.put("profile", "");
        }

        if (imageaadharBitmap != null) {
            String encoded = Utils.encodeToBase64(imageaadharBitmap, Bitmap.CompressFormat.JPEG, 90);
            encoded = String.format("data:image/jpeg;base64,%s", encoded);
            params.put("imageaadhar", encoded);
        }else{
            params.put("imageaadhar", "");
        }
        if (f_imageaadharBitmap != null) {
            String encoded = Utils.encodeToBase64(f_imageaadharBitmap, Bitmap.CompressFormat.JPEG, 90);
            encoded = String.format("data:image/jpeg;base64,%s", encoded);
            params.put("f_imageaadhar", encoded);
        }else{
            params.put("f_imageaadhar", "");
        }


        if (m_imageaadharBitmap != null) {
            String encoded = Utils.encodeToBase64(m_imageaadharBitmap, Bitmap.CompressFormat.JPEG, 90);
            encoded = String.format("data:image/jpeg;base64,%s", encoded);
            params.put("m_imageaadhar", encoded);
        }else{
            params.put("m_imageaadhar", "");
        }


        if (image10thBitmapProf != null) {
            String encoded = Utils.encodeToBase64(image10thBitmapProf, Bitmap.CompressFormat.JPEG, 90);
            encoded = String.format("data:image/jpeg;base64,%s", encoded);
            params.put("image10th", encoded);
        }else{
            params.put("image10th", "");
        }

        if (image12thBitmapProf != null) {
            String encoded = Utils.encodeToBase64(image12thBitmapProf, Bitmap.CompressFormat.JPEG, 90);
            encoded = String.format("data:image/jpeg;base64,%s", encoded);
            params.put("image12th", encoded);
        }else{
            params.put("image12th", "");
        }

        if (imageschcertBitmapProf != null) {
            String encoded = Utils.encodeToBase64(imageschcertBitmapProf, Bitmap.CompressFormat.JPEG, 90);
            encoded = String.format("data:image/jpeg;base64,%s", encoded);
            params.put("imageschcert", encoded);
        }else{
            params.put("imageschcert", "");
        }

        if (imageschtransBitmapProf != null) {
            String encoded = Utils.encodeToBase64(imageschtransBitmapProf, Bitmap.CompressFormat.JPEG, 90);
            encoded = String.format("data:image/jpeg;base64,%s", encoded);
            params.put("imageschtrans", encoded);
        }else{
            params.put("imageschtrans", "");
        }

        if (imagegradBitmapProf != null) {
            String encoded = Utils.encodeToBase64(imagegradBitmapProf, Bitmap.CompressFormat.JPEG, 90);
            encoded = String.format("data:image/jpeg;base64,%s", encoded);
            params.put("imagegrad", encoded);
        }else{
            params.put("imagegrad", "");
        }

        if (imageresidenceBitmapProf != null) {
            String encoded = Utils.encodeToBase64(imageresidenceBitmapProf, Bitmap.CompressFormat.JPEG, 90);
            encoded = String.format("data:image/jpeg;base64,%s", encoded);
            params.put("residence", encoded);
        }else{
            params.put("residence", "");
        }

        if (imageincomeBitmapProf != null) {
            String encoded = Utils.encodeToBase64(imageincomeBitmapProf, Bitmap.CompressFormat.JPEG, 90);
            encoded = String.format("data:image/jpeg;base64,%s", encoded);
            params.put("income", encoded);
        }else{
            params.put("income", "");
        }


        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.ADD_DOC, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error ) {
                ErpProgressVert.cancelProgressBar();
                if (error == null) {
                    //   User.setaddCurrentUser(jsonObject.getJSONObject(Constants.USERDATA));
                    Toast.makeText(UpdateStudentDetails.this, "Student Updated Successfully", Toast.LENGTH_SHORT).show();
                    //   Log.i("USERDATA", User.getCurrentUser().getAadhar_image());
//                    Intent i = new Intent(UpdateStudentDetails.this, STDListActivity.class);
//                    startActivity(i);

                    if (tag.equalsIgnoreCase("class")){
                        Intent intent = new Intent(UpdateStudentDetails.this, StudentList.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.putExtra("classid", cla);
                        intent.putExtra("check", check);
                        startActivity(intent);
                        finish();
                    }else  if (tag.equalsIgnoreCase("sec")){
                        Intent intent = new Intent(UpdateStudentDetails.this, StudentListBySection.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.putExtra("classid", cla);
                        intent.putExtra("sec", sec);
                        startActivity(intent);
                        finish();
                    }else {
                        finish();
                    }

                } else {
                    Toast.makeText(UpdateStudentDetails.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }, this, params);
    }




    private void fetchCategory() {
        HashMap<String, String> params = new HashMap<String, String>();
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.CASTELIST_URL, casteapiCallback, this, params);
    }

    ApiHandler.ApiCallback casteapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            if (error == null) {
                try {
                    categorylist = CasteBean.parseCasteArray(jsonObject.getJSONArray(Constants.CASTEDATA));
                    CategoryAdapter adapter = new CategoryAdapter(UpdateStudentDetails.this, categorylist);
                    spinnerCategory.setAdapter(adapter);
                    spinnerCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if (i > 0) {
                                casteid = categorylist.get(i - 1).getCaste_id();

                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                    // setting here spinner item selected with position..
                    int i = 0;
                    String id=user.getCastid();
                    for ( CasteBean obj : categorylist) {
                        if (obj.getCaste_id().equalsIgnoreCase(id)) {
                            spinnerCategory.setSelection(i + 1);
                            break;
                        }
                        i++;
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {

            }
        }
    };

    private void fetchClass() {
        HashMap<String, String> params = new HashMap<String, String>();
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.CLASSLIST_URL, classapiCallback, this, params);
    }

    ApiHandler.ApiCallback classapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            if (error == null) {
                try {
                    classList = ClassBean.parseClassArray(jsonObject.getJSONArray(Constants.CLASSDATA));
                    ClassAdapter adapter = new ClassAdapter(UpdateStudentDetails.this, classList);
                    spinnerClass.setAdapter(adapter);
                    spinnerClass.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if (i > 0) {
                                classid = classList.get(i - 1).getId();
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {


                        }
                    });
                    // setting here spinner item selected with position..
                    int i = 0;
                    String id=user.getClass_id();
                    for ( ClassBean obj : classList) {
                        if (obj.getId().equalsIgnoreCase(id)) {
                            spinnerClass.setSelection(i + 1);
                            break;
                        }
                        i++;
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                // Utils.showSnackBar(error.getMessage(), parent);
            }
        }
    };

    private void setuserData() {

        txtRegNo.setEnabled(false);

        if(user.getMother_name()!=null){
            edtMotherName.setText(user.getMother_name());
        }else{
            edtMotherName.setText("");
        }

        if(user.getWeight()!=null){
            edtWeight.setText(user.getWeight());
        }else{
            edtWeight.setText("");
        }

        if(user.getHeight()!=null){
            edtHeight.setText(user.getHeight());
        }else{
            edtHeight.setText("");
        }

        if(user.getMother_name()!=null){
            edtMotherName.setText(user.getMother_name());
        }else{
            edtMotherName.setText("");
        }

        if(user.getAadhaar()!=null){
            edtEmail.setText(user.getAadhaar());
        }else{
            edtEmail.setText("");
        }

        if(user.getFather_name()!=null){
            edtFatherName.setText(user.getFather_name());
        }else{
            edtFatherName.setText("");
        }

        if(user.getF_name()!=null){
            edtFirstName.setText(user.getF_name());
        }else{
            edtFirstName.setText("");
        }

        if(user.getL_name()!=null){
            edtlastName.setText(user.getL_name());
        }else{
            edtlastName.setText("");
        }

        if(user.getCroll_no()!=null){
            edtRoll.setText(user.getCroll_no());
        }else{
            edtRoll.setText("");
        }

        if(user.getId()!=null){
            txtRegNo.setText(user.getId());
        }else{
            txtRegNo.setText("");
        }

        if(user.getDob()!=null){
            edtDob.setText(user.getDob());
        }else{
            edtDob.setText("");
        }

        if(user.getAddress()!=null){
            edtAddress.setText(user.getAddress());
        }else{
            edtAddress.setText("");
        }

        if(user.getPr_city()!=null){
            edtCity.setText(user.getPr_city());
        }else{
            edtCity.setText("");
        }

        if(user.getPr_state()!=null){
            edtState.setText(user.getPr_state());
        }else{
            edtState.setText("");
        }

        if(user.getPr_pincode()!=null){
            edtpinCode.setText(user.getPr_pincode());
        }else{
            edtpinCode.setText("");
        }

        if(user.getPhoneno()!=null){
            edtPhone.setText(user.getPhoneno());
        }else{
            edtPhone.setText("");
        }

        if(user.getMothersmobile()!=null){
            edtMotherPhone.setText(user.getMothersmobile());
        }else{
            edtMotherPhone.setText("");
        }

        if(user.getFathermobile()!=null){
            edtFatherPhone.setText(user.getFathermobile());
        }else{
            edtFatherPhone.setText("");
        }

    }


    @OnClick(R.id.imgBack)
    public void onback() {
        imgBack.startAnimation(animation);
        commonBack();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        commonBack();
    }

    private void commonBack() {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:
                        finish();
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        //No button clicked
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Are you sure, You want to exit?").setPositiveButton("Yes", dialogClickListener)
                .setNegativeButton("No", dialogClickListener).show();
    }
}