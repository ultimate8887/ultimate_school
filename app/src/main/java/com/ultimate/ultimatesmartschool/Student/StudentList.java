package com.ultimate.ultimatesmartschool.Student;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;
import com.ultimate.ultimatesmartschool.AddmissionForm.HouseSpinnerAdapter;
import com.ultimate.ultimatesmartschool.BeanModule.ClassBean;
import com.ultimate.ultimatesmartschool.BeanModule.CommonBean;
import com.ultimate.ultimatesmartschool.BeanModule.Studentbean;
import com.ultimate.ultimatesmartschool.BeanModule.User;
import com.ultimate.ultimatesmartschool.Login.LoginActivity;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.StudentAttendance.ClassAdapterAttend;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.CommonProgress;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class StudentList extends AppCompatActivity implements Studentadapter.StudentPro {
    ArrayList<ClassBean> classList = new ArrayList<>();
    @BindView(R.id.parent)
    RelativeLayout parent;
    //@BindView(R.id.spinner) Spinner spinnerClass;
//    @BindView(R.id.spinner2)
//    MultiSelectSpinner multiselectSpinnerclass;
    @BindView(R.id.spinnerClass)
    Spinner spinnerClass;
    String classid = "", class_name = "", check = "";
    ;
    private int loaded = 0;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    List<String> classidsel;
    ArrayList<Studentbean> hwList = new ArrayList<>();
    private LinearLayoutManager layoutManager;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerview;
    private Studentadapter adapter;
    @BindView(R.id.textNorecord)
    TextView txtNorecord;
    @BindView(R.id.totalRecord)
    TextView totalRecord;
    CommonProgress commonProgress;
    @BindView(R.id.export)
    FloatingActionButton export;
    @BindView(R.id.secLyt)
    RelativeLayout secLyt;
    @BindView(R.id.textView9)
    TextView textView9;
    private ArrayList<CommonBean> groupList = new ArrayList<>();
    @BindView(R.id.spinnersection)
    Spinner spinnerHouse;
    public String feeid = "",houseid="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_list);
        ButterKnife.bind(this);
        commonProgress = new CommonProgress(StudentList.this);
        classidsel = new ArrayList<>();
        //fetchClass();

        if (getIntent().getExtras() != null) {
            classid = getIntent().getExtras().getString("classid");
            check = getIntent().getExtras().getString("check");
            // Toast.makeText(getApplicationContext(),"classid "+classid,Toast.LENGTH_LONG).show();
        }
        fetchClass_new();

        layoutManager = new LinearLayoutManager(StudentList.this);
        recyclerview.setLayoutManager(layoutManager);
        adapter = new Studentadapter(hwList, StudentList.this, this);
        recyclerview.setAdapter(adapter);
        if (check.equalsIgnoreCase("house")){
            txtTitle.setText("House-Wise List");
            secLyt.setVisibility(View.VISIBLE);
            textView9.setText("Select house");
        }else{
            txtTitle.setText("Class-Wise List");
            secLyt.setVisibility(View.GONE);
        }

    }


    private void fetchClass_new() {
        commonProgress.show();
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("check", "true");
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.CLASSLIST_URL, classapiCallback1, this, params);
    }

    ApiHandler.ApiCallback classapiCallback1 = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            commonProgress.dismiss();
            if (error == null) {
                try {
                    classList = ClassBean.parseClassArray(jsonObject.getJSONArray(Constants.CLASSDATA));
                    ClassAdapterAttend adapter = new ClassAdapterAttend(StudentList.this, classList,1);
                    spinnerClass.setAdapter(adapter);
                    spinnerClass.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if (i > 0) {
                                classid = classList.get(i - 1).getId();
                                class_name = classList.get(i - 1).getName();
                            }
                            if (check.equalsIgnoreCase("house")){
                                fetchHouse();
                            }else{
                                fetchStudent(classid);
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {


                        }
                    });
//                    // setting here spinner item selected with position..
                    if (!classid.equalsIgnoreCase("")) {
                        int i = 0;
                        String id = classid;
                        for (ClassBean obj : classList) {
                            if (obj.getId().equalsIgnoreCase(id)) {
                                spinnerClass.setSelection(i + 1);
                                fetchStudent(classid);
                                break;
                            }
                            i++;
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                // Utils.showSnackBar(error.getMessage(), parent);
            }
        }
    };

    private void fetchHouse() {
        ErpProgress.showProgressBar(this, "Please wait...");
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("check","house");
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.GROUP_LIST_URL, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                ErpProgress.cancelProgressBar();
                if (error == null) {

                    try {
                        groupList.clear();
                        groupList = CommonBean.parseCommonArray(jsonObject.getJSONArray(Constants.GROUP_DATA));
                        final HouseSpinnerAdapter adapter = new HouseSpinnerAdapter(StudentList.this, groupList,1);
                        spinnerHouse.setAdapter(adapter);
                        spinnerHouse.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                if (position > 0) {
                                    houseid = groupList.get(position - 1).getId();
                                } else {
                                    houseid="";
                                }
                                fetchStudent(classid);
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
//
//                    int i = 0;
//                    String id=user.getEs_housesid();
//                    for ( CommonBean obj : groupList) {
//                        if (obj.getId().equalsIgnoreCase(id)) {
//                            spinnerHouse.setSelection(i + 1);
//                            break;
//                        }
//                        i++;
//                    }

                } else {
                    //Utils.showSnackBar(error.getMessage(), parent);
                }
            }
        }, this, params);
    }



    @OnClick(R.id.imgBack)
    public void onback() {
        finish();
    }


//    private void fetchClass() {
//        if (loaded == 0) {
//            commonProgress.show();
//        }
//        loaded++;
//
//        HashMap<String, String> params = new HashMap<String, String>();
//        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.CLASSLIST_URL, classapiCallback, this, params);
//    }
//
//    ApiHandler.ApiCallback classapiCallback = new ApiHandler.ApiCallback() {
//        @Override
//        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
//            ErpProgress.cancelProgressBar();
//            commonProgress.dismiss();
//            if (error == null) {
//                try {
//                    if (classList != null) {
//                        classList.clear();
//                    }
//                    classList = ClassBean.parseClassArray(jsonObject.getJSONArray(Constants.CLASSDATA));
//                    List<String> dstring = new ArrayList<>();
//                    for (ClassBean cobj : classList) {
//                        dstring.add(cobj.getName());
//                        Log.e("getclassid",cobj.getId());
//                    }
//
//                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(StudentList.this, android.R.layout.simple_list_item_multiple_choice, dstring);
//
//                    multiselectSpinnerclass.setListAdapter(adapter).setListener(new BaseMultiSelectSpinner.MultiSpinnerListener() {
//                        @Override
//                        public void onItemsSelected(boolean[] selected) {
//
//                            Log.e("dcfcds", "fdsf");
//                            String value = multiselectSpinnerclass.getSpinnerText();
//                            List<String> clas = new ArrayList<>();
//                            classidsel.clear();
//                            if (!value.isEmpty()) {
//                                clas = Arrays.asList(value.split("\\s*,\\s*"));
//                            }
//                            for (String dval : clas) {
//                                for (ClassBean obj : classList) {
//                                    if (obj.getName().equalsIgnoreCase(dval)) {
//                                        classidsel.add(obj.getId());
//                                        //  textView7.setVisibility(View.GONE);
//
//                                        break;
//                                    }
//                                }
//                            }
//
//                            fetchStudent();
//                            Log.e("dcfcds", value + "  ,  classid:  " + classidsel);
//                            // }
//                        }
//                    }).setSelectAll(false)
//                            .setSpinnerItemLayout(R.layout.support_simple_spinner_dropdown_item)
//                            .setTitle("Select Class")
//
//                            .setMaxSelectedItems(1)
//                    ;
//
//
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//
//            } else {
//                Utils.showSnackBar(error.getMessage(), parent);
//                if (error.getStatusCode() == 405) {
//                    User.logout();
//                    startActivity(new Intent(StudentList.this, LoginActivity.class));
//                    finish();
//                }
//            }
//        }
//    };


    public void fetchStudent(String classid) {
        commonProgress.show();
        HashMap<String, String> params = new HashMap<String, String>();

        if (check.equalsIgnoreCase("house")){
            params.put("class_id", classid);
            params.put("house_id", houseid);
            params.put("all", "5");
        }else{
            params.put("class_id", classid);
        }
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.studentlistclass, apiCallback, this, params);

    }

    ApiHandler.ApiCallback apiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            commonProgress.dismiss();
            if (error == null) {
                try {
                    if (hwList != null) {
                        hwList.clear();
                    }
                    JSONArray jsonArray = jsonObject.getJSONArray("student_list");
                    hwList = Studentbean.parseHWArray(jsonArray);
                    if (hwList.size() > 0) {
                        adapter.setHQList(hwList);
                        adapter.notifyDataSetChanged();
                        txtNorecord.setVisibility(View.GONE);
                        totalRecord.setText("Total Entries:- " + String.valueOf(hwList.size()));
                        export.setVisibility(View.VISIBLE);
                    } else {
                        export.setVisibility(View.GONE);
                        totalRecord.setText("Total Entries:- 0");
                        hwList.clear();
                        adapter.setHQList(hwList);
                        adapter.notifyDataSetChanged();
                        txtNorecord.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                export.setVisibility(View.GONE);
                totalRecord.setText("Total Entries:- 0");
                txtNorecord.setVisibility(View.VISIBLE);
                hwList.clear();
                adapter.setHQList(hwList);
                adapter.notifyDataSetChanged();
            }
        }
    };


    @SuppressLint("RestrictedApi")
    @OnClick(R.id.export)
    public void submit() {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.S) {
            importData();
        } else {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {

                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    Toast.makeText(this, "Please provide storage permission from app settings", Toast.LENGTH_LONG).show();
                } else {

                    ActivityCompat.requestPermissions(this,
                            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            1);
                }
            } else {
                importData();

            }
        }
    }

    @SuppressLint("RestrictedApi")
    private void importData() {
        // File filePath = new File(Environment.getExternalStorageDirectory() + "/Demo.xls");
        Workbook wb = new HSSFWorkbook();

        Cell cell = null;

        String ndate = "", sub_staff = "", sub_date = "";

        sub_staff = class_name;

        Sheet sheet = null;
        sheet = wb.createSheet(sub_staff + " Students List");
        //Now column and row
        Row row = sheet.createRow(0);

        cell = row.createCell(0);
        cell.setCellValue("S No");

        cell = row.createCell(1);
        cell.setCellValue("Roll No");

        cell = row.createCell(2);
        cell.setCellValue("Class/Section");

        cell = row.createCell(3);
        cell.setCellValue("Student Name");

        cell = row.createCell(4);
        cell.setCellValue("Father Name");

        cell = row.createCell(5);
        cell.setCellValue("Phone");

        cell = row.createCell(6);
        cell.setCellValue("House Name");


        //column width
        sheet.setColumnWidth(0, (20 * 100));
        sheet.setColumnWidth(1, (20 * 150));
        sheet.setColumnWidth(2, (30 * 150));
        sheet.setColumnWidth(3, (30 * 250));
        sheet.setColumnWidth(4, (30 * 200));
        sheet.setColumnWidth(5, (20 * 100));
        sheet.setColumnWidth(6, (30 * 250));

        for (int i = 0; i < hwList.size(); i++) {

            Row row1 = sheet.createRow(i + 1);

            cell = row1.createCell(0);
            cell.setCellValue(i + 1);

            cell = row1.createCell(1);
            cell.setCellValue(hwList.get(i).getId());

            cell = row1.createCell(2);
            cell.setCellValue(hwList.get(i).getClass_name() + " (" + hwList.get(i).getSection_name() + ")");

            cell = row1.createCell(3);
            cell.setCellValue((hwList.get(i).getName()));
            //  cell.setCellStyle(cellStyle);

            cell = row1.createCell(4);
            cell.setCellValue(hwList.get(i).getFather_name());

            cell = row1.createCell(5);
            cell.setCellValue(hwList.get(i).getPhoneno());

            cell = row1.createCell(6);

            if(hwList.get(i).getEs_housename() != null){
                cell.setCellValue(hwList.get(i).getEs_housename());
            }else{
                cell.setCellValue("N/A");
            }

            sheet.setColumnWidth(0, (20 * 100));
            sheet.setColumnWidth(1, (20 * 150));
            sheet.setColumnWidth(2, (30 * 150));
            sheet.setColumnWidth(3, (30 * 250));
            sheet.setColumnWidth(4, (30 * 200));
            sheet.setColumnWidth(5, (20 * 100));
            sheet.setColumnWidth(6, (30 * 250));

        }
        String fileName;

        fileName = sub_staff + "_students_list_" + System.currentTimeMillis() + ".xls";

        // File file = new File(getActivity().getExternalFilesDir(null), fileName);

        File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS), fileName);

        FileOutputStream fileOutputStream = null;

        try {
            fileOutputStream = new FileOutputStream(file);
            wb.write(fileOutputStream);
            Utils.openSuccessDialog("Success!", "Export file in " + file, "", this);
        } catch (IOException e) {
            Utils.openErrorDialog("Error writing Exception: " + e, this);
        } catch (Exception e) {
            Utils.openErrorDialog("Failed to save file due to Exception: " + e, this);
        } finally {
            try {
                if (null != fileOutputStream) {
                    fileOutputStream.close();
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

    }

    @Override
    public void StudentProfile(Studentbean std) {
        Gson gson = new Gson();
        String student = gson.toJson(std, Studentbean.class);
        Intent i = new Intent(StudentList.this, StudentProfile.class);
        i.putExtra("student", student);
        startActivity(i);
    }

    @Override
    public void onUpdateCallback(Studentbean std) {
        Gson gson = new Gson();
        String student = gson.toJson(std, Studentbean.class);
        Intent i = new Intent(this, UpdateStudentDetails.class);
        i.putExtra("student", student);
        i.putExtra("tag", "class");
        i.putExtra("class", classid);
        i.putExtra("check", check);
        startActivity(i);
    }

    @Override
    public void onBlockCallback(Studentbean std) {
        commonProgress.show();
        HashMap<String, String> params = new HashMap<>();
        params.put("stu_id", std.getId());
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.BLOCKSTUDENTURL, apisendmsgCallback, this, params);
    }


    @Override
    public void stdCallBack(Studentbean std) {
        String phoneNo = std.getPhoneno();
        Log.i("email", phoneNo);
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + phoneNo));
        startActivity(intent);
    }

    @Override
    public void onResume() {
        super.onResume();
        //fetchStudent(classid);
    }

    @Override
    public void stdEmailCallBack(Studentbean std) {

        String phoneNo = std.getPhoneno();
        Uri uri = Uri.parse("smsto:" + phoneNo);
        Intent i = new Intent(Intent.ACTION_SENDTO, uri);
        i.setPackage("com.whatsapp");
        startActivity(Intent.createChooser(i, "Chat Now!"));
    }


    ApiHandler.ApiCallback apisendmsgCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            commonProgress.dismiss();
            ErpProgress.cancelProgressBar();
            if (error == null) {

                Utils.showSnackBar("Student Blocked!", parent);
                fetchStudent(classid);

            } else {
                Log.e("error", error.getMessage() + "");
                Utils.showSnackBar(error.getMessage(), parent);
                // Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_LONG).show();
                if (error.getStatusCode() == 405) {
                    User.logout();
                    startActivity(new Intent(StudentList.this, LoginActivity.class));
                    finish();
                }
            }
        }
    };

    @Override
    public void onDeleteCallback(Studentbean std) {
        commonProgress.show();
        HashMap<String, String> params = new HashMap<>();
        params.put("h_id", std.getId());
        params.put("check", "student");
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.DELETEHW, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                ErpProgress.cancelProgressBar();
                commonProgress.dismiss();
                if (error == null) {

                    Utils.showSnackBar("Student Deleted!", parent);
                    // getActivity().finish();
                    fetchStudent(classid);
                } else {
                    Log.e("error", error.getMessage() + "");
                    Utils.showSnackBar(error.getMessage(), parent);
                    if (error.getStatusCode() == 405) {
                        User.logout();
                        startActivity(new Intent(StudentList.this, LoginActivity.class));
                        finish();
                    }
                }
            }
        }, this, params);
    }
}
