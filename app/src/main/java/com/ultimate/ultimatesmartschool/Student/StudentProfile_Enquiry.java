package com.ultimate.ultimatesmartschool.Student;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.siyamed.shapeimageview.CircularImageView;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;
import com.ultimate.ultimatesmartschool.BeanModule.Studentbean;
import com.ultimate.ultimatesmartschool.BeanModule.User;
import com.ultimate.ultimatesmartschool.R;

import androidmads.library.qrgenearator.QRGContents;
import androidmads.library.qrgenearator.QRGEncoder;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class StudentProfile_Enquiry extends AppCompatActivity {
    private Bitmap bitmap;
    @BindView(R.id.txtName)
    TextView txtName;
    @BindView(R.id.imgUser)
    CircularImageView imgUser;
    @BindView(R.id.imgPro)
    ImageView imgPro;
    @BindView(R.id.txtClass)
    TextView txtClass;
    @BindView(R.id.txtRollNo)
    TextView txtRollNo;
    @BindView(R.id.txtDOB)
    TextView txtDOB;
    @BindView(R.id.txtAadhar)
    TextView txtAadhar;
    @BindView(R.id.txtGender)
    TextView txtGender;
    @BindView(R.id.txtAddress)
    TextView txtAddress;
    @BindView(R.id.txtContact)
    TextView txtContact;
    @BindView(R.id.txtEmail)
    TextView txtEmail;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.txtRegNo)TextView txtRegNo;
    @BindView(R.id.txtusername)TextView txtusername;
    @BindView(R.id.txtpassword)TextView txtpassword;
    @BindView(R.id.txtdoj)TextView txtdoj;
    @BindView(R.id.txtSection)TextView txtSection;
    @BindView(R.id.txtfatherContact)TextView txtfatherContact;
    @BindView(R.id.txtmotherContact)TextView txtmotherContact;
    @BindView(R.id.txthostlstatus)TextView txthostlstatus;
    @BindView(R.id.txtcategory)TextView txtcategory;
    @BindView(R.id.txtmothername)TextView txtmothername;
    @BindView(R.id.txtfathername)TextView txtfathername;



    @BindView(R.id.txtprev_school)TextView txtprev_school;
    @BindView(R.id.txtreason_for_leave)TextView txtreason_for_leave;
    @BindView(R.id.txtRefrence)TextView txtRefrence;
    @BindView(R.id.txtNo_sibling_in)TextView txtNo_sibling_in;
    @BindView(R.id.txtNo_sibling_other)TextView txtNo_sibling_other;
    @BindView(R.id.txtNo_sibling)TextView txtNo_sibling;
    @BindView(R.id.txtfatherDesignation)TextView txtfatherDesignation;
    @BindView(R.id.txtfathermail)TextView txtfathermail;
    @BindView(R.id.txtfatherOccupation)TextView txtfatherOccupation;
    @BindView(R.id.txtfatherQualification)TextView txtfatherQualification;


    @BindView(R.id.txtHeight)TextView txtHeight;
    @BindView(R.id.txtWeight)TextView txtWeight;

    @BindView(R.id.viewdoc)
    Button viewdoc;
    Studentbean user;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_profile_enquiry);
        ButterKnife.bind(this);
        txtTitle.setText("Student Details");

        if (User.getCurrentUser().getSchoolData().getFi_school_id().equalsIgnoreCase("XYZGGSMT")) {
            viewdoc.setVisibility(View.VISIBLE);
        } else {
            viewdoc.setVisibility(View.GONE);
        }

        if (getIntent() != null && getIntent().hasExtra("student")) {
            Gson gson = new Gson();
            user = gson.fromJson(getIntent().getStringExtra("student"), Studentbean.class);
        } else {
            return;
        }

//        if (user.getFather_name() != null) {
//
//            txtfathername.setText("Father's Name: " + user.getFather_name());
//        } else {
//            txtfathername.setText("Father's Name Not Found! ");
//        }

        if (user.getFather_name()!=null) {
            String title = getColoredSpanned("<b>"+"Father's Name : "+"</b>", "#000000");
            String Name = getColoredSpanned(user.getFather_name(), "#5A5C59");
            txtfathername.setText(Html.fromHtml(title + " " + Name));
        }else {
            txtfathername.setText("Father's Name : Not Mentioned");
        }

        if (user.getClass_name()!=null) {
            String title = getColoredSpanned("<b>"+"Preferred Class : "+"</b>", "#000000");
            String Name = getColoredSpanned("<b>"+user.getClass_name()+"</b>", "#383737");
            txtHeight.setText(Html.fromHtml(title + " " + Name));
        }else {
            txtHeight.setText("Not Mentioned");
        }

        if (user.getPre_prev_class()!=null) {
            String title = getColoredSpanned("<b>"+"Previous Class : "+"</b>", "#000000");
            String Name = getColoredSpanned("<b>"+user.getPre_prev_class()+"</b>", "#383737");
            txtWeight.setText(Html.fromHtml(title + " " + Name));
        }else {
            txtWeight.setText("Not Mentioned");
        }


//        if(user.getHeight()!=null){
//            txtHeight.setText(user.getClass_name());
//        }else{
//            txtHeight.setText("Not Mentioned");
//        }
//        if(user.getWeight()!=null){
//            txtWeight.setText(user.getPre_prev_class());
//        }else{
//            txtWeight.setText("Not Mentioned");
//        }


//        if (user.getAdmissiondate() != null) {
//            txtdoj.setText("Date of Joining: " + user.getAdmissiondate());
//        }
//        if (user.getFathermobile() != null) {
//            txtfatherContact.setText("Father's Mob: " + user.getFathermobile());
//        } else {
//            txtfatherContact.setText("Not Mentioned");
//        }

        if (user.getPhoneno() != null) {
            String title = getColoredSpanned("<b>"+"Mobile : "+"</b>", "#000000");
            String Name = getColoredSpanned("</u>" +user.getPhoneno()+"</u>", "#5A5C59");
            txtfatherContact.setText(Html.fromHtml(title + " " + Name));
        }else {
            txtfatherContact.setText("Mobile: Not Mentioned");
        }

        txtfatherContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String phoneNo = user.getFathermobile();
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:" + phoneNo));
                startActivity(intent);
            }
        });

        if (user.getMothersmobile() != null) {
            txtmotherContact.setText("Mother's Mob: " + user.getMothersmobile());
        } else {
            txtmotherContact.setText("Not Mentioned");
        }

        txtmotherContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String phoneNo = user.getMothersmobile();
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:" + phoneNo));
                startActivity(intent);
            }
        });

        if (user.getHostelstatus() != null) {
            txthostlstatus.setText("Hostel Status: " + user.getHostelstatus());
        } else {
            txthostlstatus.setText("Not Mentioned");
        }

        if (user.getSessionname() != null ) {
            txtcategory.setText("Category: " + user.getCategoryname() + "/" + "Session: " + user.getSessionname());
        } else if (user.getCategoryname() != null){
            txtcategory.setText("Category: " + user.getCategoryname());
        }else {
            txtcategory.setText("Not Mentioned");
        }

        if (user.getUsername() != null) {
            txtusername.setText(user.getUsername());
        }
        if (user.getPassword() != null) {
            txtpassword.setText(user.getPassword());
        }

        txtName.setText(user.getName());

        if (user.getSection_name() != null) {
            txtClass.setText(user.getClass_name() + "(" + user.getSection_name() + ")");
        } else {
            txtClass.setText(user.getClass_name());
        }

        if (user.getPr_city()!=null) {
            String title = getColoredSpanned("<b>"+"City/Village : "+"</b>", "#000000");
            String Name = getColoredSpanned(user.getPr_city(), "#5A5C59");
            txtcategory.setText(Html.fromHtml(title + " " + Name));
        }else {
            txtcategory.setText("City/Village : Not Mentioned");
        }

        if (user.getDistance_from_school()!=null) {
            String title = getColoredSpanned("<b>"+"Distance from school (km) : "+"</b>", "#000000");
            String Name = getColoredSpanned(user.getDistance_from_school(), "#5A5C59");
            txtdoj.setText(Html.fromHtml(title + " " + Name));
        }else {
            txtdoj.setText("Distance from school(km) : Not Mentioned");
        }

        if (user.getPr_state()!=null) {
            String title = getColoredSpanned("<b>"+"District : "+"</b>", "#000000");
            String Name = getColoredSpanned(user.getPr_state(), "#5A5C59");
            txtContact.setText(Html.fromHtml(title + " " + Name));
        }else {
            txtContact.setText("District : Not Mentioned");
        }

        if (user.getPr_pincode()!=null) {
            String title = getColoredSpanned("<b>"+"PinCode : "+"</b>", "#000000");
            String Name = getColoredSpanned(user.getPr_pincode(), "#5A5C59");
            txtEmail.setText(Html.fromHtml(title + " " + Name));
        }else {
            txtEmail.setText("PinCode : Not Mentioned");
        }

        if (user.getPre_emailid()!=null) {
            String title = getColoredSpanned("<b>"+"Email Id : "+"</b>", "#000000");
            String Name = getColoredSpanned(user.getPre_emailid(), "#5A5C59");
            txtfathermail.setText(Html.fromHtml(title + " " + Name));
        }else {
            txtfathermail.setText("Email Id : Not Mentioned");
        }

        if (user.getNo_of_siblings()!=null) {
            String title = getColoredSpanned("<b>"+"Total number of siblings : "+"</b>", "#000000");
            String Name = getColoredSpanned(user.getNo_of_siblings(), "#5A5C59");
            txtNo_sibling.setText(Html.fromHtml(title + " " + Name));
        }else {
            txtNo_sibling.setText("Total number of siblings : Not Mentioned");
        }
        if (user.getNo_of_siblings_other_school()!=null) {
            String title = getColoredSpanned("<b>"+"Siblings in other school : "+"</b>", "#000000");
            String Name = getColoredSpanned(user.getNo_of_siblings_other_school(), "#5A5C59");
            txtNo_sibling_other.setText(Html.fromHtml(title + " " + Name));
        }else {
            txtNo_sibling_other.setText("Siblings in other school : Not Mentioned");
        }
        if (user.getNo_of_siblings_in_school()!=null) {
            String title = getColoredSpanned("<b>"+"Siblings in our school : "+"</b>", "#000000");
            String Name = getColoredSpanned(user.getNo_of_siblings_in_school(), "#5A5C59");
            txtNo_sibling_in.setText(Html.fromHtml(title + " " + Name));
        }else {
            txtNo_sibling_in.setText("Siblings in our school : Not Mentioned");
        }
        if (user.getReference()!=null) {
            String title = getColoredSpanned("<b>"+"Reference : "+"</b>", "#000000");
            String Name = getColoredSpanned(user.getReference(), "#5A5C59");
            txtRefrence.setText(Html.fromHtml(title + " " + Name));
        }else {
            txtRefrence.setText("Reference : Not Mentioned");
        }

        if (user.getPre_prev_school()!=null) {
            String title = getColoredSpanned("<b>"+"Previous School : "+"</b>", "#000000");
            String Name = getColoredSpanned(user.getPre_prev_school(), "#5A5C59");
            txtprev_school.setText(Html.fromHtml(title + " " + Name));
        }else {
            txtprev_school.setText("Previous School : Not Mentioned");
        }
        if (user.getReason_for_leave_school()!=null) {
            String title = getColoredSpanned("<b>"+"Reason for leaving : "+"</b>", "#000000");
            String Name = getColoredSpanned(user.getReason_for_leave_school(), "#5A5C59");
            txtreason_for_leave.setText(Html.fromHtml(title + " " + Name));
        }else {
            txtreason_for_leave.setText("Reason for leaving : Not Mentioned");
        }


        if (user.getFatheroccu()!=null) {
            String title = getColoredSpanned("<b>"+"Occupation : "+"</b>", "#000000");
            String Name = getColoredSpanned(user.getFatheroccu(), "#5A5C59");
            txtfatherOccupation.setText(Html.fromHtml(title + " " + Name));
        }else {
            txtfatherOccupation.setText("Occupation : Not Mentioned");
        }
        if (user.getQualification()!=null) {
            String title = getColoredSpanned("<b>"+"Qualification : "+"</b>", "#000000");
            String Name = getColoredSpanned(user.getQualification(), "#5A5C59");
            txtfatherQualification.setText(Html.fromHtml(title + " " + Name));
        }else {
            txtfatherQualification.setText("Qualification : Not Mentioned");
        }

        if (user.getDesignation()!=null) {
            String title = getColoredSpanned("<b>"+"Designation : "+"</b>", "#000000");
            String Name = getColoredSpanned(user.getDesignation(), "#5A5C59");
            txtfatherDesignation.setText(Html.fromHtml(title + " " + Name));
        }else {
            txtfatherDesignation.setText("Designation : Not Mentioned");
        }


//        if (user.getEmail() != null){
//            txtEmail.setText(user.getEmail());
//        }else{
//            txtEmail.setText("Not Mentioned");
//        }

//        txtEmail.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                String email=user.getEmail();
//                Intent intent = new Intent(Intent.ACTION_SENDTO); // it's not ACTION_SEND
//                intent.setType("text/plain");
//                intent.putExtra(Intent.EXTRA_SUBJECT, "Subject of email");
//                intent.putExtra(Intent.EXTRA_TEXT, "Body of email");
//                intent.setData(Uri.parse("mailto:"+email)); // or just "mailto:" for blank
//                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK); // this will make such that when user returns to your app, your app is displayed, instead of the email app.
//                startActivity(intent);
//            }
//        });


//        if (user.getPhoneno()!=null) {
//            txtContact.setText(user.getPhoneno());
//        }else{
//            txtContact.setText("Not Mentioned");
//        }
//        txtContact.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                String phoneNo=user.getPhoneno();
//                Intent intent = new Intent(Intent.ACTION_DIAL);
//                intent.setData(Uri.parse("tel:"+ phoneNo));
//                startActivity(intent);
//            }
//        });

        if (user.getGender()!=null) {
            String title = getColoredSpanned("<b>"+"Gender : "+"</b>", "#000000");
            String Name = getColoredSpanned(user.getGender(), "#5A5C59");
            txtGender.setText(Html.fromHtml(title + " " + Name));
        }else {
            txtGender.setText("Gender : Not Mentioned");
        }

//        if (user.getGender().length() > 0) {
//            txtGender.setText(user.getGender());
//        }else {
//            txtGender.setText("Not Mentioned");
//        }
        if(user.getAadhaar()!=null) {
            txtAadhar.setText(user.getAadhaar());
        }else {
            txtAadhar.setText("Not Mentioned");
        }


        if (user.getAddress().length() > 0) {
            txtAddress.setText(user.getAddress());
        }else {
            txtAddress.setText("Not Mentioned");
        }

        if (user.getId()!=null) {
            txtRegNo.setText(user.getId());
        }
        if(user.getCroll_no()!=null){

            txtRollNo.setText(user.getCroll_no());
        }else{
            txtRollNo.setVisibility(View.GONE);
        }

        if (user.getBg()!=null){
            txtDOB.setText("DOB: "+user.getDob()+"("+user.getBg()+")");
        }else{
            txtDOB.setText("DOB: "+user.getDob());
        }

        if (user.getProfile() != null)
            Picasso.get().load(user.getProfile()).placeholder(getResources().getDrawable(R.drawable.stud)).into(imgUser);

        createQR();

    }

    private void createQR() {
        String inputValue="";
        if (user.getId() !=null){
            inputValue=user.getId()+""+User.getCurrentUser().getSchoolData().getFi_school_id();
        }
        if (!inputValue.equalsIgnoreCase("")) {
            //Toast.makeText(getApplicationContext(),"hiiii "+inputValue,Toast.LENGTH_LONG).show();
            QRGEncoder qrgEncoder = new QRGEncoder(inputValue, null, QRGContents.Type.TEXT, 800);
//            qrgEncoder.setColorBlack(R.color.dark_black);
//            qrgEncoder.setColorWhite(R.color.white);
            // Getting QR-Code as Bitmap
            bitmap = qrgEncoder.getBitmap();
            // Setting Bitmap to ImageView
            imgPro.setImageBitmap(bitmap);

        }
    }

    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }

    @OnClick(R.id.imgBack)
    public void backFinish() {
        finish();
    }
    @OnClick(R.id.viewdoc)
    public void viewdoc() {
        Intent intent = new Intent(StudentProfile_Enquiry.this, ViewStudentDocument.class);
        intent.putExtra("stuid", user.getId());
        startActivity(intent);


    }
}