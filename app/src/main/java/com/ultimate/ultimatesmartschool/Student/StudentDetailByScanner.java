package com.ultimate.ultimatesmartschool.Student;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.google.gson.Gson;
import com.journeyapps.barcodescanner.BarcodeCallback;
import com.journeyapps.barcodescanner.BarcodeResult;
import com.journeyapps.barcodescanner.DecoratedBarcodeView;
import com.ultimate.ultimatesmartschool.BeanModule.Studentbean;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;

public class StudentDetailByScanner extends AppCompatActivity {

    @BindView(R.id.lightButton)
    ImageView flashImageView;
    @BindView(R.id.parent)
    LinearLayout parent;
    @BindView(R.id.barcode_scanner)
    DecoratedBarcodeView barcodeScannerView;

    private String date;
    private boolean flashState = false;
    private ArrayList<Studentbean> mData = new ArrayList<>();
    private Studentbean abc;
    private TextView name, fname, text;
    private ImageView img;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_detail_by_scanner);
        ButterKnife.bind(this);

        ActivityCompat.requestPermissions(StudentDetailByScanner.this,
                new String[]{Manifest.permission.CAMERA}, 1);

        // Initialize the date
        java.util.Calendar calendar = java.util.Calendar.getInstance(TimeZone.getDefault());
        date = new java.text.SimpleDateFormat("yyyy-MM-dd").format(calendar.getTime());

        ImageView imgback = (ImageView) findViewById(R.id.imgBack);
        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        setupBarcodeScanner();
        setupFlashButton();
    }

    private void setupBarcodeScanner() {
        barcodeScannerView.decodeContinuous(new BarcodeCallback() {
            @Override
            public void barcodeResult(BarcodeResult result) {
                if (result != null) {
                    handleResult(result.getText());
                    barcodeScannerView.pause();  // Stop scanning after getting the result
                }
            }

            @Override
            public void possibleResultPoints(List<com.google.zxing.ResultPoint> resultPoints) {
            }
        });
    }

    private void setupFlashButton() {
//        flashImageView.setOnClickListener(v -> {
//            flashState = !flashState;
//            barcodeScannerView.setTorch(flashState);
//            flashImageView.setImageResource(flashState ? R.drawable.ic_flash_off : R.drawable.ic_flash_on);
//            Toast.makeText(getApplicationContext(), flashState ? "Flashlight turned on" : "Flashlight turned off", Toast.LENGTH_SHORT).show();
//        });
    }

    private void handleResult(String student_id) {
        searchStudent(student_id);
    }

    public void searchStudent(String student_id) {
        ErpProgress.showProgressBar(this, "Please wait...");
        HashMap<String, String> params = new HashMap<>();
        params.put("stu_id", student_id);
        if (date != null) {
            params.put("mdate", date);
        }

        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.STUDENTBYSCANNING_URLNEW, studentapiCallback, this, params);
    }

    ApiHandler.ApiCallback studentapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            if (error == null) {
                try {
                    if (mData != null) {
                        mData.clear();
                    }
                    //openDialoggg();
                    if (mData != null) {
                        mData.clear();
                        mData = Studentbean.parseHWArray(jsonObject.getJSONArray("student_list"));
                        abc = mData.get(0);
                        updateDialogWithStudentData();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                mData.clear();
                // Handle the error accordingly
            }
        }
    };

    private void updateDialogWithStudentData() {

        Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.custom_dialogqrscan);
        View v = dialog.getWindow().getDecorView();
        v.setBackgroundResource(android.R.color.transparent);
        text = (TextView) dialog.findViewById(R.id.txtRegNo);
        // text.setText(rawResult.getText());
        img = (CircularImageView) dialog.findViewById(R.id.img);
        // img.setImageResource(R.drawable.ic_done_gr);
        name = (TextView) dialog.findViewById(R.id.nametxtone);
        fname = (TextView) dialog.findViewById(R.id.classtxtone);

        Button webSearch = (Button) dialog.findViewById(R.id.searchButton);


        ImageView close = (ImageView) dialog.findViewById(R.id.closss);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
                //   dialog.dismiss();
            }
        });

        webSearch.setText("View Student Details");
        webSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                viewProfile();
                barcodeScannerView.resume();
            }
        });

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                //   dialog.dismiss();
            }
        });

        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                barcodeScannerView.resume();
            }
        });

        text.setText(mData.get(0).getId());
        fname.setText(mData.get(0).getClass_name() + "(" + mData.get(0).getSection_name() + ")");
        if ("male".equalsIgnoreCase(mData.get(0).getGender())) {
            name.setText(mData.get(0).getName() + "\nS/O\n" + mData.get(0).getFather_name());
        } else {
            name.setText(mData.get(0).getName() + "\nD/O\n" + mData.get(0).getFather_name());
        }
        if (mData.get(0).getProfile() != null) {
            Picasso.get().load(mData.get(0).getProfile()).placeholder(R.drawable.stud).into(img);
        } else {
            Picasso.get().load(mData.get(0).getProfile()).placeholder(R.drawable.f_student).into(img);
        }

        dialog.show();
        Window window = dialog.getWindow();
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    }

    private void openDialoggg() {
        // Implement dialog opening logic here
    }

    @Override
    public void onResume() {
        super.onResume();
        barcodeScannerView.resume();
    }

    @Override
    public void onPause() {
        super.onPause();
        barcodeScannerView.pause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        barcodeScannerView.pause();
    }

    private void viewProfile() {
        Gson gson = new Gson();
        String student = gson.toJson(abc, Studentbean.class);
        Intent i = new Intent(StudentDetailByScanner.this, StudentProfile.class);
        i.putExtra("student", student);
        startActivity(i);
        finish();
    }
}
