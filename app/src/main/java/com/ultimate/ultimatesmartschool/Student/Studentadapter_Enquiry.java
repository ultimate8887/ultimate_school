package com.ultimate.ultimatesmartschool.Student;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.daimajia.swipe.SimpleSwipeListener;
import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputLayout;
import com.squareup.picasso.Picasso;
import com.ultimate.ultimatesmartschool.BeanModule.Studentbean;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Studentadapter_Enquiry extends RecyclerView.Adapter<Studentadapter_Enquiry.Viewholder> implements Filterable {
    public ArrayList<Studentbean> stulist;
    private ArrayList<Studentbean> filterModelClass;
    Context mContext;
    Studentadapter_Enquiry.StudentPro mStudentPro;
    public Studentadapter_Enquiry(ArrayList<Studentbean> stulist, Context mContext, Studentadapter_Enquiry.StudentPro mStudentPro) {
        this.mContext = mContext;
        this.mStudentPro=mStudentPro;
        this.stulist = stulist;
        this.filterModelClass = stulist;
    }

    @Override
    public Studentadapter_Enquiry.Viewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.new_item_news_new, parent, false);
        Studentadapter_Enquiry.Viewholder viewholder = new Studentadapter_Enquiry.Viewholder(view);
        return viewholder;
    }

    @Override
    public void onBindViewHolder(final Studentadapter_Enquiry.Viewholder holder, @SuppressLint("RecyclerView") final int position) {

        holder.inpSchoolId.setVisibility(View.GONE);
       // holder.lyt.setVisibility(View.GONE);
       // holder.phone.setVisibility(View.INVISIBLE);

        Studentbean mData=stulist.get(position);

        // holder.employee_id.setText("Emp.ID: " + " " + viewstafflist.get(position).getId());

        if (mData.getId() != null) {
            String title = getColoredSpanned("Enquiry No: ", "#000000");
            String Name = getColoredSpanned(mData.getId(), "#5A5C59");
            holder.employee_id.setText(Html.fromHtml(title + " " + Name));
        }

        if (mData.getEs_housesid() != null) {
            holder.txtHouse.setVisibility(View.VISIBLE);
            String title = getColoredSpanned("House: ", "#000000");
            String Name = getColoredSpanned(mData.getEs_housename(), "#5A5C59");
            holder.txtHouse.setText(Html.fromHtml(title + " " + Name));
        }else {
            holder.txtHouse.setVisibility(View.GONE);
            holder.txtHouse.setText("House: Not Mentioned");
        }

        // holder.employee_name.setText("Name: " + " " + viewstafflist.get(position).getName());
        if (mData.getName() != null) {
            String title = getColoredSpanned("Name: ", "#000000");
            String Name = getColoredSpanned(mData.getName(), "#5A5C59");
            holder.employee_name.setText(Html.fromHtml(title + " " + Name));
        }else {
            holder.employee_name.setText("Name: Not Mentioned");
        }

        //  holder.employee_designation.setText("Designation: " + " " + viewstafflist.get(position).getDesignation());
        if (mData.getAdmissiondate() != null) {

        //for Today
        java.util.Calendar calendar = java.util.Calendar.getInstance(TimeZone.getDefault());
        Date setdate = calendar.getTime();
        SimpleDateFormat fmtOut = new SimpleDateFormat("dd MMM, yyyy");
        String dateString = fmtOut.format(setdate);
        //for Yesterday
        DateFormat dateFormat = new SimpleDateFormat("dd MMM, yyyy");
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);
        String dateString1= dateFormat.format(cal.getTime());

        String check= Utils.getDateFormated(mData.getAdmissiondate());
        if (check.equalsIgnoreCase(dateString)){
            //  holder.lytLine.setVisibility(View.VISIBLE);
            // holder.txtDT.setText("Today");
            String title = getColoredSpanned("Enquiry Date: ", "#000000");
            //String Name = getColoredSpanned(messagelist.get(position).getFrom_type(), "#7D7D7D");
            String l_Name = getColoredSpanned("Today", "#e31e25");
            holder.employee_designation.setText(Html.fromHtml(title + " " + l_Name));
            // holder.txtDT.setText("Today at "+time);
            //  holder.vieww.setBackground(mContext.getResources().getDrawable(R.drawable.a_card_lyt2));
        }else if (check.equalsIgnoreCase(dateString1)){

            String title = getColoredSpanned("Enquiry Date: ", "#000000");
            //String Name = getColoredSpanned(messagelist.get(position).getFrom_type(), "#7D7D7D");
            String l_Name = getColoredSpanned("Yesterday", "#1C8B3B");
            holder.employee_designation.setText(Html.fromHtml(title + " " + l_Name));

            //  holder.lytLine.setVisibility(View.GONE);
            //  holder.vieww.setBackground(mContext.getResources().getDrawable(R.drawable.card_lyt2));
        }else {
            String title = getColoredSpanned("Enquiry Date: ", "#000000");
            String Name = getColoredSpanned(Utils.getDateFormated(mData.getAdmissiondate()), "#5A5C59");
            holder.employee_designation.setText(Html.fromHtml(title + " " + Name));
        }

        }else {
            holder.employee_designation.setText("Admission Date: N/A");
        }

        //  holder.post.setText("Post:" + " " + viewstafflist.get(position).getPost());
        if (mData.getFather_name() != null) {
            String title = getColoredSpanned("Father's Name: ", "#000000");
            String Name = getColoredSpanned(mData.getFather_name(), "#5A5C59");
            holder.post.setText(Html.fromHtml(title + " " + Name));
        }else {
            holder.post.setText("Father's Name: Not Mentioned");
        }

        //  holder.employee_dob.setText("Date of Birth: " + " " + viewstafflist.get(position).getDateofbirth());
        if (mData.getDob() != null) {
            String title = getColoredSpanned("Date of Birth: ", "#000000");
            String Name = getColoredSpanned(Utils.getDateFormated(mData.getDob()), "#5A5C59");
            holder.employee_dob.setText(Html.fromHtml(title + " " + Name));
        }else {
            holder.employee_dob.setText("DOB: Not Mentioned");
        }

        // holder.employee_doj.setText("Date of Joining: " + " " + viewstafflist.get(position).getDateofjoining());
        if (mData.getSection_name() != null) {
            String title = getColoredSpanned("Preferred Class: ", "#000000");
            String Name = getColoredSpanned(mData.getClass_name()+"("+stulist.get(position).getSection_name()+")", "#5A5C59");
            holder.employee_doj.setText(Html.fromHtml(title + " " + Name));
        }else if (mData.getClass_name() != null) {
            String title = getColoredSpanned("Preferred Class: ", "#000000");
            String Name = getColoredSpanned(mData.getClass_name(), "#5A5C59");
            holder.employee_doj.setText(Html.fromHtml(title + " " + Name));
        }
        else {
            holder.employee_doj.setText("Class: Not Mentioned");
        }



        holder.phoneLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mData.getPhoneno() != null) {
                    mStudentPro.stdCallBack(stulist.get(position));
                }else {
                    Toast.makeText(mContext,"Phone Number Not Found!",Toast.LENGTH_SHORT).show();
                }
            }
        });


        holder.googleLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (mData.getPhoneno() != null) {
                    mStudentPro.stdEmailCallBack(stulist.get(position));
                }else {
                    Toast.makeText(mContext,"Number Not Found!",Toast.LENGTH_SHORT).show();
                }

            }
        });

        if (mData.getGender()!=null) {
            if (mData.getGender().equalsIgnoreCase("male")) {
                if (stulist.get(position).getProfile() != null) {
                    //  Picasso.with(context).load(viewstafflist.get(position).getImage()).placeholder(context.getResources().getDrawable(R.drawable.man)).into(holder.image);
                    Utils.progressImg1(stulist.get(position).getProfile(), holder.image, mContext);
                } else {
                    // Picasso.with(context).load(String.valueOf(context.getResources().getDrawable(R.drawable.staffimg))).into(holder.image);
                    Picasso.get().load(R.drawable.stud).into(holder.image);

                }
            } else {
                if (stulist.get(position).getProfile() != null) {
                    //  Picasso.with(context).load(viewstafflist.get(position).getImage()).placeholder(context.getResources().getDrawable(R.drawable.man)).into(holder.image);
                    Utils.progressImg1(stulist.get(position).getProfile(), holder.image, mContext);
                } else {
                    // Picasso.with(context).load(String.valueOf(context.getResources().getDrawable(R.drawable.staffimg))).into(holder.image);
                    Picasso.get().load(R.drawable.stud).into(holder.image);

                }
            }
        }else{
            Picasso.get().load(R.drawable.stud).into(holder.image);
        }



        if (mData.getAge() != null) {
            if (mData.getAge() != null) {
                String tit = getColoredSpanned("Age: ", "#000000");
                String title = getColoredSpanned("  "+mData.getAge(), "#F4212C");
                String Name = getColoredSpanned(" years old", "#5A5C59");
                holder.phone.setText(Html.fromHtml(tit + " "+ title + " " + Name));
            }else {
                holder.phone.setText("Age: Not Found");
            }

            holder.inpSchoolId.setHint("Birthday Card Seen Status");
            if (mData.getDate_time() != null) {
                //holder.login_device.setText("Birthday Card Seen at " + Utils.getTimeHr(mData.getDate_time()));
                String title = getColoredSpanned("Seen at ", "#000000");
                String Name = getColoredSpanned("</u>" +Utils.getTimeHr(mData.getDate_time())+"</u>", "#1C8B3B");
                holder.login_device.setText(Html.fromHtml(title + " " + Name));

            }else{

                String title = getColoredSpanned("Not Seen yet", "#F4212C");
                String Name = getColoredSpanned("", "#5A5C59");
                holder.login_device.setText(Html.fromHtml(title + " " + Name));

                // holder.login_device.setText("Not Seen yet");
            }
           // holder.update.setVisibility(View.GONE);
          //  holder.trash.setVisibility(View.GONE);
            holder.email.setVisibility(View.GONE);
           // holder.delete.setVisibility(View.GONE);
            holder.view5.setVisibility(View.GONE);
        }else {
          //  holder.trash.setVisibility(View.VISIBLE);
            holder.inpSchoolId.setHint("Login Device");
           // holder.delete.setVisibility(View.VISIBLE);
          //  holder.update.setVisibility(View.VISIBLE);
            if (mData.getPre_prev_school() != null) {
                String title = getColoredSpanned("Previous School: ", "#000000");
                String Name = getColoredSpanned("</u>" +mData.getPre_prev_school()+"</u>", "#5A5C59");
                holder.email.setText(Html.fromHtml(title + " " + Name));
            }else {
                holder.email.setText("Previous School: Not Mentioned");
            }

            if ( mData.getPr_city() != null) {

                String title = getColoredSpanned("City/Village: ", "#000000");
                String Name = getColoredSpanned(mData.getPr_city(), "#5A5C59");
                holder.phone.setText(Html.fromHtml(title + " " + Name));
            }else {
                holder.phone.setText("City/Village: Not Mentioned");
            }


            holder.phone.setVisibility(View.VISIBLE);
            holder.view5.setVisibility(View.VISIBLE);
            if (mData.getMobile_name() != null) {
                holder.login_device.setText(mData.getMobile_brand()+"-"+mData.getMobile_name());
            }else {
                holder.login_device.setText("Not Detect");
            }





        }





        // holder.txtid.setText("ID:"+stulist.get(position).getId());

        holder.parent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mStudentPro!= null){
                    mStudentPro.StudentProfile(stulist.get(position));
                }
            }
        });



    }

    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }

    public interface StudentPro{
        void StudentProfile(Studentbean std);
        void onUpdateCallback(Studentbean std);
        void onDeleteCallback(Studentbean std);
        void onBlockCallback(Studentbean std);
        void stdCallBack(Studentbean std);
        void stdEmailCallBack(Studentbean std);
    }
    @Override
    public int getItemCount() {
        if (stulist != null)
            return stulist.size();
        return 0;
    }



//    @Override
//    public int getSwipeLayoutResourceId(int position) {
//        return R.id.swipe;
//    }

    public void setHQList(ArrayList<Studentbean> stulist) {
        this.stulist = stulist;
    }

    public class Viewholder extends RecyclerView.ViewHolder {


//        @BindView(R.id.lyt)
//        LinearLayout lyt;
        @BindView(R.id.id)
        TextView employee_id;
        @BindView(R.id.txtstuName)
        EditText employee_name;
        @BindView(R.id.txtDT)
        TextView employee_designation;
        @BindView(R.id.txtdob)
        TextView employee_dob;
        @BindView(R.id.txtdoj)
        TextView employee_doj;
        @BindView(R.id.txtReason)
        EditText login_device;
        @BindView(R.id.txtFatherName)
        TextView post;
        @BindView(R.id.profile)
        CircularImageView image;
        @BindView(R.id.txtPhone)
        TextView phone;
        @BindView(R.id.googleLogin)
        FloatingActionButton googleLogin;
        @BindView(R.id.phoneLogin)
        FloatingActionButton phoneLogin;
        @BindView(R.id.txtHouse)
        TextView txtHouse;
        @BindView(R.id.email)
        TextView email;
        @BindView(R.id.parent)
        RelativeLayout parent;
        @BindView(R.id.inpUsername)
        TextInputLayout inpSchoolId;


        @BindView(R.id.view5)
        View view5;

        public Viewholder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            parent =(RelativeLayout)itemView.findViewById(R.id.parent);

//            circleimg=(CircularImageView)itemView.findViewById(R.id.circleimg);
//
//            textViewData = (TextView) itemView.findViewById(R.id.name);
//            txtFather = (TextView) itemView.findViewById(R.id.fathername);
//            txtMobile = (TextView) itemView.findViewById(R.id.mobileno);
//            txtClass = (TextView) itemView.findViewById(R.id.classess);
//            // textViewPos = (TextView) itemView.findViewById(R.id.position);
//
//            txtid=(TextView)itemView.findViewById(R.id.id);
        }
    }

    @Override
    public Filter getFilter() {

        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {

                String Key = constraint.toString();
                if (Key.isEmpty()) {

                    filterModelClass = stulist ;

                }
                else {
                    ArrayList<Studentbean> lstFiltered = new ArrayList<>();
                    for (Studentbean row : stulist) {

                        if (row.getName().toLowerCase().contains(Key.toLowerCase()) || row.getPr_city().toLowerCase().contains(Key.toLowerCase())
                                || row.getPre_prev_school().toLowerCase().contains(Key.toLowerCase())
                                || row.getClass_name().toLowerCase().contains(Key.toLowerCase())){
                            lstFiltered.add(row);
                        }

                    }

                    filterModelClass = lstFiltered;

                }


                FilterResults filterResults = new FilterResults();
                filterResults.values= filterModelClass;
                return filterResults;

            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {


                stulist = (ArrayList<Studentbean>) results.values;
                notifyDataSetChanged();

            }
        };




    }
}
