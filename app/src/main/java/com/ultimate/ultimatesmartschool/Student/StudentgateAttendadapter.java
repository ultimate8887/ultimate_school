package com.ultimate.ultimatesmartschool.Student;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.daimajia.swipe.SimpleSwipeListener;
import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.squareup.picasso.Picasso;
import com.ultimate.ultimatesmartschool.BeanModule.Studentbean;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.StaffAttendance.StaffAttendBean;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class StudentgateAttendadapter extends RecyclerSwipeAdapter<StudentgateAttendadapter.Viewholder> implements Filterable {
    public ArrayList<Studentbean> stulist;
    private ArrayList<Studentbean> filterModelClass;
    Context mContext;

    public StudentgateAttendadapter(ArrayList<Studentbean> stulist, Context mContext) {
        this.mContext = mContext;
        this.stulist = stulist;
        this.filterModelClass = stulist;
    }

    @Override
    public StudentgateAttendadapter.Viewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.staff_item_news, parent, false);
        StudentgateAttendadapter.Viewholder viewholder = new StudentgateAttendadapter.Viewholder(view);
        return viewholder;
    }

    @Override
    public void onBindViewHolder(final StudentgateAttendadapter.Viewholder holder, final int position) {


        final Studentbean data = stulist.get(position);
        holder.spinnerAttend.setVisibility(View.GONE);
        if (stulist.get(position).getGender().equalsIgnoreCase("Male")) {
            if (stulist.get(position).getProfile() != null) {
                Picasso.get().load(stulist.get(position).getProfile()).placeholder(R.drawable.stud).into(holder.circleimg);
            } else {
                Picasso.get().load(R.drawable.stud).into(holder.circleimg);
            }
        }else{
            if (stulist.get(position).getProfile() != null) {
                Picasso.get().load(stulist.get(position).getProfile()).placeholder(R.drawable.f_student).into(holder.circleimg);
            } else {
                Picasso.get().load(R.drawable.f_student).into(holder.circleimg);
            }
        }

//        holder.txtName.setText(data.getStudent_name());
//        holder.txtRegNo.setText("Father: "+data.getStudent_id());
//        holder.txtFname.setText("Father: "+data.getFather_name());

        //  holder.employee_designation.setText("Designation: " + " " + viewstafflist.get(position).getDesignation());
        if (data.getName() != null) {
            String title = getColoredSpanned("Name: ", "#5A5C59");
            String Name = getColoredSpanned(data.getName(), "#383737");
            holder.txtName.setText(Html.fromHtml(title + " " + Name));
        }
        else {
            holder.txtName.setText("Name: Not Mentioned");
        }

        //  holder.employee_designation.setText("Designation: " + " " + viewstafflist.get(position).getDesignation());
        if (data.getId() != null) {
            String title = getColoredSpanned("Reg.No: ", "#5A5C59");
            String Name = getColoredSpanned(data.getId(), "#383737");
            holder.txtRegNo.setText(Html.fromHtml(title + " " + Name));
        }
        else {
            holder.txtRegNo.setText("ID: Not Mentioned");
        }

        //  holder.employee_designation.setText("Designation: " + " " + viewstafflist.get(position).getDesignation());
        if (data.getFather_name() != null) {
            String title = getColoredSpanned("Father: ", "#5A5C59");
            String Name = getColoredSpanned(data.getFather_name(), "#383737");
            holder.txtFname.setText(Html.fromHtml(title + " " + Name));
        }
        else {
            holder.txtFname.setText("Father: Not Mentioned");
        }

        if (data.getClass_name()!=null){
            holder.txtDT.setText(data.getClass_name());
        }else {
            holder.txtDT.setText("Not Mentioned");
        }

        holder.check_in.setText(Utils.getDateTimeFormatedWithAMPM(stulist.get(position).getAt_attendance_date()));


    }

    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }



    public interface StudentPro{
        void StudentProfile(Studentbean std);
        void onUpdateCallback(Studentbean std);
        void stdCallBack(Studentbean std);
        void stdEmailCallBack(Studentbean std);
    }
    @Override
    public int getItemCount() {
        if (stulist != null)
            return stulist.size();
        return 0;
    }



    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe;
    }

    public void setHQList(ArrayList<Studentbean> stulist) {
        this.stulist = stulist;
    }

    public class Viewholder extends RecyclerView.ViewHolder {
        @BindView(R.id.txtRegNo)
        TextView txtRegNo;
        @BindView(R.id.txtName)
        TextView txtName;
        @BindView(R.id.spinnerAttend)
        Spinner spinnerAttend;
        @BindView(R.id.txtFname)
        TextView txtFname;
        @BindView(R.id.circleimg)
        CircularImageView circleimg;
        @BindView(R.id.txtReason)
        EditText check_in;
        @BindView(R.id.txtDT)
        TextView txtDT;

        public Viewholder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }

    @Override
    public Filter getFilter() {

        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {

                String Key = constraint.toString();
                if (Key.isEmpty()) {

                    filterModelClass = stulist;

                } else {
                    ArrayList<Studentbean> lstFiltered = new ArrayList<>();
                    for (Studentbean row : stulist) {

                        if (row.getName().toLowerCase().contains(Key.toLowerCase()) || row.getId().toLowerCase().contains(Key.toLowerCase())) {
                            lstFiltered.add(row);
                        }

                    }

                    filterModelClass = lstFiltered;

                }


                FilterResults filterResults = new FilterResults();
                filterResults.values = filterModelClass;
                return filterResults;

            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {


                stulist = (ArrayList<Studentbean>) results.values;
                notifyDataSetChanged();

            }
        };


    }

    }
