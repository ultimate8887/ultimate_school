package com.ultimate.ultimatesmartschool.Student;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.cuneytayyildiz.gestureimageview.GestureImageView;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.squareup.picasso.Picasso;
import com.ultimate.ultimatesmartschool.BeanModule.Studentbean;
import com.ultimate.ultimatesmartschool.Ebook.WebViewActivity;
import com.ultimate.ultimatesmartschool.Homework.GETHomeworkActivity;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ViewStudentDocument extends AppCompatActivity {
    String stuid;
    ArrayList<Studentbean> mData = new ArrayList<>();
    @BindView(R.id.imageView6)
    ImageView aadharimageview;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.imageView610th)ImageView imageView610th;
    @BindView(R.id.imageView612th)ImageView imageView612th;
    @BindView(R.id.imageView6scholcert)ImageView imageView6scholcert;
    @BindView(R.id.imageView6scoltc)ImageView imageView6scoltc;
    @BindView(R.id.imageView6grad)ImageView imageView6grad;

    @BindView(R.id.addpdf)TextView aadharpdf;
    @BindView(R.id.addpdf10th)TextView tenthpdf;
    @BindView(R.id.addpdf12th)TextView twelthpdf;

    @BindView(R.id.addpdfscholcert)TextView scscholcertpdf;
    @BindView(R.id.addpdfscoltc)TextView scoltcpdf;
    @BindView(R.id.addpdfgrad)TextView gradpdf;



    @BindView(R.id.addimage)TextView aadharimage;
    @BindView(R.id.addimage10th)TextView tenthimage;
    @BindView(R.id.addimage12th)TextView twelthimage;

    @BindView(R.id.addimagescholcert)TextView scscholcertimage;
    @BindView(R.id.addimagescoltc)TextView scoltcimage;
    @BindView(R.id.addimagegrad)TextView gradimage;
    BottomSheetDialog mBottomSheetDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_student_document);
        ButterKnife.bind(this);
        txtTitle.setText("View Documents");
        Bundle bundle = getIntent().getExtras();
        stuid = bundle.getString("stuid");
        searchStudent(stuid);
    }



    @OnClick(R.id.imgBack)
    public void onback() {
        finish();
    }
    @OnClick(R.id.imageView6)
    public void imageView6() {

        if(mData.get(0).getAadhar_image()!=null) {
            mBottomSheetDialog = new BottomSheetDialog(this);
            final View sheetView = getLayoutInflater().inflate(R.layout.image_lyt, null);
            mBottomSheetDialog.setContentView(sheetView);
            ImageView imgShow = (ImageView) mBottomSheetDialog.findViewById(R.id.imgShow);
            // ImageView imgShow = (ImageView) EventDialog.findViewById(R.id.imgShow);


            ImageView imgDownload = (ImageView) mBottomSheetDialog.findViewById(R.id.imgDownload);
            imgDownload.setVisibility(View.GONE);
            Picasso.get().load(mData.get(0).getAadhar_image()).placeholder(getResources().getDrawable(R.drawable.home)).into(imgShow);

            mBottomSheetDialog.show();
            Window window = mBottomSheetDialog.getWindow();
            window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        }else{
            Toast.makeText(getApplicationContext(),"Image Not Available",Toast.LENGTH_LONG).show();
        }

    }
    @OnClick(R.id.imageView612th)
    public void imageView612th() {

        if(mData.get(0).getTwelvemarksheet_image()!=null) {
            mBottomSheetDialog = new BottomSheetDialog(this);
            final View sheetView = getLayoutInflater().inflate(R.layout.image_lyt, null);
            mBottomSheetDialog.setContentView(sheetView);
            ImageView imgShow = (ImageView) mBottomSheetDialog.findViewById(R.id.imgShow);
            // ImageView imgShow = (ImageView) EventDialog.findViewById(R.id.imgShow);


            ImageView imgDownload = (ImageView) mBottomSheetDialog.findViewById(R.id.imgDownload);
            imgDownload.setVisibility(View.GONE);
            Picasso.get().load(mData.get(0).getTwelvemarksheet_image()).placeholder(getResources().getDrawable(R.drawable.home)).into(imgShow);

            mBottomSheetDialog.show();
            Window window = mBottomSheetDialog.getWindow();
            window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        }else{
            Toast.makeText(getApplicationContext(),"Image Not Available",Toast.LENGTH_LONG).show();
        }

    }


    @OnClick(R.id.imageView610th)
    public void imageView610th() {

        if(mData.get(0).getTenmarksheet_image()!=null) {
            mBottomSheetDialog = new BottomSheetDialog(this);
            final View sheetView = getLayoutInflater().inflate(R.layout.image_lyt, null);
            mBottomSheetDialog.setContentView(sheetView);
            ImageView imgShow = (ImageView) mBottomSheetDialog.findViewById(R.id.imgShow);
            // ImageView imgShow = (ImageView) EventDialog.findViewById(R.id.imgShow);


            ImageView imgDownload = (ImageView) mBottomSheetDialog.findViewById(R.id.imgDownload);
            imgDownload.setVisibility(View.GONE);
            Picasso.get().load(mData.get(0).getTenmarksheet_image()).placeholder(getResources().getDrawable(R.drawable.home)).into(imgShow);

            mBottomSheetDialog.show();
            Window window = mBottomSheetDialog.getWindow();
            window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        }else{
            Toast.makeText(getApplicationContext(),"Image Not Available",Toast.LENGTH_LONG).show();
        }

    }

    @OnClick(R.id.imageView6scholcert)
    public void imageView6scholcert() {

        if(mData.get(0).getSchol_certif_image()!=null) {
            mBottomSheetDialog = new BottomSheetDialog(this);
            final View sheetView = getLayoutInflater().inflate(R.layout.image_lyt, null);
            mBottomSheetDialog.setContentView(sheetView);
            ImageView imgShow = (ImageView) mBottomSheetDialog.findViewById(R.id.imgShow);
            // ImageView imgShow = (ImageView) EventDialog.findViewById(R.id.imgShow);


            ImageView imgDownload = (ImageView) mBottomSheetDialog.findViewById(R.id.imgDownload);
            imgDownload.setVisibility(View.GONE);
            Picasso.get().load(mData.get(0).getSchol_certif_image()).placeholder(getResources().getDrawable(R.drawable.home)).into(imgShow);

            mBottomSheetDialog.show();
            Window window = mBottomSheetDialog.getWindow();
            window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        }else{
            Toast.makeText(getApplicationContext(),"Image Not Available",Toast.LENGTH_LONG).show();
        }

    }


    @OnClick(R.id.imageView6scoltc)
    public void imageView6scoltc() {

        if(mData.get(0).getSchol_trans_certif_image()!=null) {
            mBottomSheetDialog = new BottomSheetDialog(this);
            final View sheetView = getLayoutInflater().inflate(R.layout.image_lyt, null);
            mBottomSheetDialog.setContentView(sheetView);
            ImageView imgShow = (ImageView) mBottomSheetDialog.findViewById(R.id.imgShow);
            // ImageView imgShow = (ImageView) EventDialog.findViewById(R.id.imgShow);


            ImageView imgDownload = (ImageView) mBottomSheetDialog.findViewById(R.id.imgDownload);
            imgDownload.setVisibility(View.GONE);
            Picasso.get().load(mData.get(0).getSchol_trans_certif_image()).placeholder(getResources().getDrawable(R.drawable.home)).into(imgShow);

            mBottomSheetDialog.show();
            Window window = mBottomSheetDialog.getWindow();
            window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        }else{
            Toast.makeText(getApplicationContext(),"Image Not Available",Toast.LENGTH_LONG).show();
        }

    }

    @OnClick(R.id.imageView6grad)
    public void imageView6grad() {

        if(mData.get(0).getGraduation_image()!=null) {
            mBottomSheetDialog = new BottomSheetDialog(this);
            final View sheetView = getLayoutInflater().inflate(R.layout.image_lyt, null);
            mBottomSheetDialog.setContentView(sheetView);
            ImageView imgShow = (ImageView) mBottomSheetDialog.findViewById(R.id.imgShow);
            // ImageView imgShow = (ImageView) EventDialog.findViewById(R.id.imgShow);


            ImageView imgDownload = (ImageView) mBottomSheetDialog.findViewById(R.id.imgDownload);
            imgDownload.setVisibility(View.GONE);
            Picasso.get().load(mData.get(0).getGraduation_image()).placeholder(getResources().getDrawable(R.drawable.home)).into(imgShow);

            mBottomSheetDialog.show();
            Window window = mBottomSheetDialog.getWindow();
            window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        }else{
            Toast.makeText(getApplicationContext(),"Image Not Available",Toast.LENGTH_LONG).show();
        }

    }
    @OnClick(R.id.addimagegrad)
    public void addimagegrad() {

        if(mData.get(0).getGraduation_image()!=null) {
            mBottomSheetDialog = new BottomSheetDialog(this);
            final View sheetView = getLayoutInflater().inflate(R.layout.image_lyt, null);
            mBottomSheetDialog.setContentView(sheetView);
            ImageView imgShow = (ImageView) mBottomSheetDialog.findViewById(R.id.imgShow);
            // ImageView imgShow = (ImageView) EventDialog.findViewById(R.id.imgShow);


            ImageView imgDownload = (ImageView) mBottomSheetDialog.findViewById(R.id.imgDownload);
            imgDownload.setVisibility(View.GONE);
            Picasso.get().load(mData.get(0).getGraduation_image()).placeholder(getResources().getDrawable(R.drawable.home)).into(imgShow);

            mBottomSheetDialog.show();
            Window window = mBottomSheetDialog.getWindow();
            window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        }else{
            Toast.makeText(getApplicationContext(),"Image Not Available",Toast.LENGTH_LONG).show();
        }

    }

    @OnClick(R.id.addimagescoltc)
    public void addimagescoltc() {

        if(mData.get(0).getSchol_trans_certif_image()!=null) {
            mBottomSheetDialog = new BottomSheetDialog(this);
            final View sheetView = getLayoutInflater().inflate(R.layout.image_lyt, null);
            mBottomSheetDialog.setContentView(sheetView);
            ImageView imgShow = (ImageView) mBottomSheetDialog.findViewById(R.id.imgShow);
            // ImageView imgShow = (ImageView) EventDialog.findViewById(R.id.imgShow);


            ImageView imgDownload = (ImageView) mBottomSheetDialog.findViewById(R.id.imgDownload);
            imgDownload.setVisibility(View.GONE);
            Picasso.get().load(mData.get(0).getSchol_trans_certif_image()).placeholder(getResources().getDrawable(R.drawable.home)).into(imgShow);

            mBottomSheetDialog.show();
            Window window = mBottomSheetDialog.getWindow();
            window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        }else{
            Toast.makeText(getApplicationContext(),"Image Not Available",Toast.LENGTH_LONG).show();
        }

    }


    @OnClick(R.id.addimagescholcert)
    public void addimagescholcert() {

        if(mData.get(0).getSchol_certif_image()!=null) {
            mBottomSheetDialog = new BottomSheetDialog(this);
            final View sheetView = getLayoutInflater().inflate(R.layout.image_lyt, null);
            mBottomSheetDialog.setContentView(sheetView);
            ImageView imgShow = (ImageView) mBottomSheetDialog.findViewById(R.id.imgShow);
            // ImageView imgShow = (ImageView) EventDialog.findViewById(R.id.imgShow);


            ImageView imgDownload = (ImageView) mBottomSheetDialog.findViewById(R.id.imgDownload);
            imgDownload.setVisibility(View.GONE);
            Picasso.get().load(mData.get(0).getSchol_certif_image()).placeholder(getResources().getDrawable(R.drawable.home)).into(imgShow);

            mBottomSheetDialog.show();
            Window window = mBottomSheetDialog.getWindow();
            window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        }else{
            Toast.makeText(getApplicationContext(),"Image Not Available",Toast.LENGTH_LONG).show();
        }

    }



    @OnClick(R.id.addimage12th)
    public void addimage12th() {

        if(mData.get(0).getTwelvemarksheet_image()!=null) {
            mBottomSheetDialog = new BottomSheetDialog(this);
            final View sheetView = getLayoutInflater().inflate(R.layout.image_lyt, null);
            mBottomSheetDialog.setContentView(sheetView);
            ImageView imgShow = (ImageView) mBottomSheetDialog.findViewById(R.id.imgShow);
            // ImageView imgShow = (ImageView) EventDialog.findViewById(R.id.imgShow);


            ImageView imgDownload = (ImageView) mBottomSheetDialog.findViewById(R.id.imgDownload);
            imgDownload.setVisibility(View.GONE);
            Picasso.get().load(mData.get(0).getTwelvemarksheet_image()).placeholder(getResources().getDrawable(R.drawable.home)).into(imgShow);

            mBottomSheetDialog.show();
            Window window = mBottomSheetDialog.getWindow();
            window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        }else{
            Toast.makeText(getApplicationContext(),"Image Not Available",Toast.LENGTH_LONG).show();
        }

    }


    @OnClick(R.id.addimage10th)
    public void addimage10th() {

        if(mData.get(0).getTenmarksheet_image()!=null) {
            mBottomSheetDialog = new BottomSheetDialog(this);
            final View sheetView = getLayoutInflater().inflate(R.layout.image_lyt, null);
            mBottomSheetDialog.setContentView(sheetView);
            ImageView imgShow = (ImageView) mBottomSheetDialog.findViewById(R.id.imgShow);
            // ImageView imgShow = (ImageView) EventDialog.findViewById(R.id.imgShow);


            ImageView imgDownload = (ImageView) mBottomSheetDialog.findViewById(R.id.imgDownload);
            imgDownload.setVisibility(View.GONE);
            Picasso.get().load(mData.get(0).getTenmarksheet_image()).placeholder(getResources().getDrawable(R.drawable.home)).into(imgShow);

            mBottomSheetDialog.show();
            Window window = mBottomSheetDialog.getWindow();
            window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        }else{
            Toast.makeText(getApplicationContext(),"Image Not Available",Toast.LENGTH_LONG).show();
        }

    }
    @OnClick(R.id.addimage)
    public void addimage() {



        if(mData.get(0).getAadhar_image()!=null) {
            mBottomSheetDialog = new BottomSheetDialog(this);
            final View sheetView = getLayoutInflater().inflate(R.layout.image_lyt, null);
            mBottomSheetDialog.setContentView(sheetView);
            ImageView imgShow = (ImageView) mBottomSheetDialog.findViewById(R.id.imgShow);
            // ImageView imgShow = (ImageView) EventDialog.findViewById(R.id.imgShow);


            ImageView imgDownload = (ImageView) mBottomSheetDialog.findViewById(R.id.imgDownload);
            imgDownload.setVisibility(View.GONE);
            Picasso.get().load(mData.get(0).getAadhar_image()).placeholder(getResources().getDrawable(R.drawable.home)).into(imgShow);

            mBottomSheetDialog.show();
            Window window = mBottomSheetDialog.getWindow();
            window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        }else{
            Toast.makeText(getApplicationContext(),"Image Not Available",Toast.LENGTH_LONG).show();
        }

    }

    @OnClick(R.id.addpdf)
    public void aadharpdf() {
        if(mData.get(0).getAadhar_pdf()!=null) {
        Intent intent = new Intent(ViewStudentDocument.this, WebViewActivity.class);
        intent.putExtra("MY_kEY",mData.get(0).getAadhar_pdf());
        startActivity(intent);
        }else{
            Toast.makeText(getApplicationContext(),"PDF Not Available",Toast.LENGTH_LONG).show();
        }

    }


    @OnClick(R.id.addpdf10th)
    public void addpdf10th() {
        if(mData.get(0).getTenmarksheet_pdf()!=null) {
        Intent intent = new Intent(ViewStudentDocument.this, WebViewActivity.class);
        intent.putExtra("MY_kEY",mData.get(0).getTenmarksheet_pdf());
        startActivity(intent);
        }else{
            Toast.makeText(getApplicationContext(),"PDF Not Available",Toast.LENGTH_LONG).show();
        }

    }


    @OnClick(R.id.addpdf12th)
    public void addpdf12th() {
        if(mData.get(0).getTwelvemarksheet_pdf()!=null) {
            Intent intent = new Intent(ViewStudentDocument.this, WebViewActivity.class);
            intent.putExtra("MY_kEY", mData.get(0).getTwelvemarksheet_pdf());
            startActivity(intent);
        }else{
            Toast.makeText(getApplicationContext(),"PDF Not Available",Toast.LENGTH_LONG).show();
        }

    }


    @OnClick(R.id.addpdfscholcert)
    public void addpdfscholcert() {
        if(mData.get(0).getSchol_certif_pdf()!=null) {
        Intent intent = new Intent(ViewStudentDocument.this, WebViewActivity.class);
        intent.putExtra("MY_kEY",mData.get(0).getSchol_certif_pdf());
        startActivity(intent);
        }else{
            Toast.makeText(getApplicationContext(),"PDF Not Available",Toast.LENGTH_LONG).show();
        }
    }



    @OnClick(R.id.addpdfscoltc)
    public void addpdfscoltc() {
        if(mData.get(0).getSchol_trans_certif_pdf()!=null) {
        Intent intent = new Intent(ViewStudentDocument.this, WebViewActivity.class);
        intent.putExtra("MY_kEY",mData.get(0).getSchol_trans_certif_pdf());
        startActivity(intent);
        }else{
            Toast.makeText(getApplicationContext(),"PDF Not Available",Toast.LENGTH_LONG).show();
        }
    }

    @OnClick(R.id.addpdfgrad)
    public void addpdfgrad() {
        if(mData.get(0).getGraduation_pdf()!=null) {
        Intent intent = new Intent(ViewStudentDocument.this, WebViewActivity.class);
        intent.putExtra("MY_kEY",mData.get(0).getGraduation_pdf());
        startActivity(intent);
        }else{
            Toast.makeText(getApplicationContext(),"PDF Not Available",Toast.LENGTH_LONG).show();
        }
    }
    public void searchStudent(String student_id) {
        ErpProgress.showProgressBar(this, "Please wait...");
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("stu_id",student_id);

        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.STUDENTBYSCANNING_URLNEW, studentapiCallback, this, params);

    }

    ApiHandler.ApiCallback studentapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {

            if (error == null) {
                ErpProgress.cancelProgressBar();
                try {
                    if (mData != null) {
                        mData.clear();
                    }

                    if (mData != null){
                        mData.clear();
                        mData= Studentbean.parseHWArray(jsonObject.getJSONArray("student_list"));
//                        abc=mData.get(0);
//
//                        name.setText(mData.get(0).getName()+"("+mData.get(0).getClass_name()+")"+mData.get(0).getSection_name());
//                        fname.setText(mData.get(0).getFather_name());
                        if(mData.get(0).getAadhar_image()!=null) {
                            Picasso.get().load(mData.get(0).getAadhar_image()).placeholder(getResources().getDrawable(R.drawable.aadhar_card_png)).into(aadharimageview);
                        }

                        if(mData.get(0).getTenmarksheet_image()!=null) {
                            Picasso.get().load(mData.get(0).getTenmarksheet_image()).placeholder(getResources().getDrawable(R.drawable.boardten)).into(imageView610th);
                        }
                        if(mData.get(0).getTwelvemarksheet_image()!=null) {
                            Picasso.get().load(mData.get(0).getTwelvemarksheet_image()).placeholder(getResources().getDrawable(R.drawable.boardten)).into(imageView612th);
                        }
                        if(mData.get(0).getSchol_certif_image()!=null) {
                            Picasso.get().load(mData.get(0).getSchol_certif_image()).placeholder(getResources().getDrawable(R.drawable.certificate_icon)).into(imageView6scholcert);
                        }
                        if(mData.get(0).getSchol_trans_certif_image()!=null) {
                            Picasso.get().load(mData.get(0).getSchol_trans_certif_image()).placeholder(getResources().getDrawable(R.drawable.certificate_icon)).into(imageView6scoltc);
                        }
                        if(mData.get(0).getSchol_trans_certif_image()!=null) {
                            Picasso.get().load(mData.get(0).getGraduation_image()).placeholder(getResources().getDrawable(R.drawable.certificate_icon)).into(imageView6grad);
                        }

                      //  Log.e("hii detail",mData.get(0).getTenmarksheet_image());

//                        adapter.setHQList(mData);
//                        adapter.notifyDataSetChanged();

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {

                mData.clear();
//                adapter.setHQList(mData);
//                adapter.notifyDataSetChanged();
            }
        }
    };
}