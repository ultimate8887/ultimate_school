package com.ultimate.ultimatesmartschool.Student;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.siyamed.shapeimageview.CircularImageView;
import com.github.siyamed.shapeimageview.RoundedImageView;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;
import com.ultimate.ultimatesmartschool.BeanModule.Studentbean;
import com.ultimate.ultimatesmartschool.BeanModule.User;
import com.ultimate.ultimatesmartschool.R;

import androidmads.library.qrgenearator.QRGContents;
import androidmads.library.qrgenearator.QRGEncoder;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class StudentProfile extends AppCompatActivity {
    private Bitmap bitmap;
    @BindView(R.id.txtName)
    TextView txtName;
    @BindView(R.id.imgUser)
    CircularImageView imgUser;
    @BindView(R.id.imgPro)
    ImageView imgPro;
    @BindView(R.id.txtClass)
    TextView txtClass;
    @BindView(R.id.txtRollNo)
    TextView txtRollNo;
    @BindView(R.id.txtDOB)
    TextView txtDOB;
    @BindView(R.id.txtAadhar)
    TextView txtAadhar;
    @BindView(R.id.txtGender)
    TextView txtGender;
    @BindView(R.id.txtAddress)
    TextView txtAddress;
    @BindView(R.id.txtContact)
    TextView txtContact;
    @BindView(R.id.txtEmail)
    TextView txtEmail;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.txtRegNo)TextView txtRegNo;
    @BindView(R.id.txtusername)TextView txtusername;
    @BindView(R.id.txtpassword)TextView txtpassword;
    @BindView(R.id.txtdoj)TextView txtdoj;
    @BindView(R.id.txtSection)TextView txtSection;
    @BindView(R.id.txtfatherContact)TextView txtfatherContact;
    @BindView(R.id.txtmotherContact)TextView txtmotherContact;
    @BindView(R.id.txthostlstatus)TextView txthostlstatus;
    @BindView(R.id.txtcategory)TextView txtcategory;
    @BindView(R.id.txtmothername)TextView txtmothername;
    @BindView(R.id.txtfathername)TextView txtfathername;
    @BindView(R.id.txtmothermail)TextView txtmothermail;
    @BindView(R.id.txtfathermail)TextView txtfathermail;

    @BindView(R.id.txtHeight)TextView txtHeight;
    @BindView(R.id.txtWeight)TextView txtWeight;

    @BindView(R.id.viewdoc)
    Button viewdoc;
    Studentbean user;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_profile);
        ButterKnife.bind(this);
        txtTitle.setText("Student Profile");

        if (User.getCurrentUser().getSchoolData().getFi_school_id().equalsIgnoreCase("XYZGGSMT")) {
            viewdoc.setVisibility(View.VISIBLE);
        } else {
            viewdoc.setVisibility(View.GONE);
        }

        if (getIntent() != null && getIntent().hasExtra("student")) {
            Gson gson = new Gson();
            user = gson.fromJson(getIntent().getStringExtra("student"), Studentbean.class);
        } else {
            return;
        }
        if (user.getSection_name() != null) {

            txtSection.setText("Section: " + user.getSection_name());
        } else {
            txtSection.setText("Section Not Found! ");
        }
        if (user.getFather_name() != null) {

            txtfathername.setText("Father's Name: " + user.getFather_name());
        } else {
            txtfathername.setText("Father's Name Not Found! ");
        }
        if (user.getMother_name() != null) {

            txtmothername.setText("Mother's Name: " + user.getMother_name());
        } else {
            txtmothername.setText("Mother's Name Not Found! ");
        }
        if(user.getHeight()!=null){
            txtHeight.setText(user.getHeight());
        }else{
            txtHeight.setText("Not Mentioned");
        }
        if(user.getWeight()!=null){
            txtWeight.setText(user.getWeight());
        }else{
            txtWeight.setText("Not Mentioned");
        }


        if (user.getAdmissiondate() != null) {
            txtdoj.setText("Date of Joining: " + user.getAdmissiondate());
        }
        if (user.getFathermobile() != null) {
            txtfatherContact.setText("Father's Mob: " + user.getFathermobile());
        } else {
            txtfatherContact.setText("Not Mentioned");
        }
        txtfatherContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String phoneNo = user.getFathermobile();
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:" + phoneNo));
                startActivity(intent);
            }
        });

        if (user.getMothersmobile() != null) {
            txtmotherContact.setText("Mother's Mob: " + user.getMothersmobile());
        } else {
            txtmotherContact.setText("Not Mentioned");
        }

        txtmotherContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String phoneNo = user.getMothersmobile();
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:" + phoneNo));
                startActivity(intent);
            }
        });

        if (user.getHostelstatus() != null) {
            txthostlstatus.setText("Hostel Status: " + user.getHostelstatus());
        } else {
            txthostlstatus.setText("Not Mentioned");
        }

        if (user.getSessionname() != null ) {
            txtcategory.setText("Category: " + user.getCategoryname() + "/" + "Session: " + user.getSessionname());
        } else if (user.getCategoryname() != null){
            txtcategory.setText("Category: " + user.getCategoryname());
        }else {
            txtcategory.setText("Not Mentioned");
        }

        if (user.getUsername() != null) {
            txtusername.setText(user.getUsername());
        }
        if (user.getPassword() != null) {
            txtpassword.setText(user.getPassword());
        }
        txtName.setText(user.getName());
        if (user.getSection_name() != null) {
            txtClass.setText(user.getClass_name() + "(" + user.getSection_name() + ")");
        } else {
            txtClass.setText(user.getClass_name());
        }


        if (user.getEmail() != null){
            txtEmail.setText(user.getEmail());
         }else{
            txtEmail.setText("Not Mentioned");
        }

        txtEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String email=user.getEmail();
                Intent intent = new Intent(Intent.ACTION_SENDTO); // it's not ACTION_SEND
                intent.setType("text/plain");
                intent.putExtra(Intent.EXTRA_SUBJECT, "Subject of email");
                intent.putExtra(Intent.EXTRA_TEXT, "Body of email");
                intent.setData(Uri.parse("mailto:"+email)); // or just "mailto:" for blank
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK); // this will make such that when user returns to your app, your app is displayed, instead of the email app.
                startActivity(intent);
            }
        });
        if (user.getPhoneno()!=null) {
            txtContact.setText(user.getPhoneno());
        }else{
            txtContact.setText("Not Mentioned");
        }
        txtContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String phoneNo=user.getPhoneno();
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:"+ phoneNo));
                startActivity(intent);
            }
        });

        if (user.getGender().length() > 0) {
            txtGender.setText(user.getGender());
        }else {
            txtGender.setText("Not Mentioned");
        }
        if(user.getAadhaar()!=null) {
            txtAadhar.setText(user.getAadhaar());
        }else {
            txtAadhar.setText("Not Mentioned");
        }


        if (user.getAddress().length() > 0) {
            txtAddress.setText(user.getAddress());
        }else {
            txtAddress.setText("Not Mentioned");
        }

        if (user.getId()!=null) {
            txtRegNo.setText(user.getId());
        }
        if(user.getCroll_no()!=null){

            txtRollNo.setText(user.getCroll_no());
        }else{
            txtRollNo.setVisibility(View.GONE);
        }

        if (user.getBg()!=null){
            txtDOB.setText("DOB: "+user.getDob()+"("+user.getBg()+")");
        }else{
            txtDOB.setText("DOB: "+user.getDob());
        }

        if (user.getProfile() != null)
            Picasso.get().load(user.getProfile()).placeholder(getResources().getDrawable(R.drawable.stud)).into(imgUser);

       createQR();

    }

    private void createQR() {
        String inputValue="";
        if (user.getId() !=null){
            inputValue=user.getId()+""+User.getCurrentUser().getSchoolData().getFi_school_id();
        }
        if (!inputValue.equalsIgnoreCase("")) {
            //Toast.makeText(getApplicationContext(),"hiiii "+inputValue,Toast.LENGTH_LONG).show();
            QRGEncoder qrgEncoder = new QRGEncoder(inputValue, null, QRGContents.Type.TEXT, 800);
//            qrgEncoder.setColorBlack(R.color.dark_black);
//            qrgEncoder.setColorWhite(R.color.white);
            // Getting QR-Code as Bitmap
            bitmap = qrgEncoder.getBitmap();
            // Setting Bitmap to ImageView
            imgPro.setImageBitmap(bitmap);

        }
    }

    @OnClick(R.id.imgBack)
    public void backFinish() {
        finish();
    }
    @OnClick(R.id.viewdoc)
    public void viewdoc() {
        Intent intent = new Intent(StudentProfile.this, ViewStudentDocument.class);
        intent.putExtra("stuid", user.getId());
        startActivity(intent);


    }

}
