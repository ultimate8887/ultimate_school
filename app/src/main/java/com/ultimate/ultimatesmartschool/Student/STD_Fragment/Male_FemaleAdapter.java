package com.ultimate.ultimatesmartschool.Student.STD_Fragment;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.ultimate.ultimatesmartschool.BeanModule.ClassBean;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.StudentAttendance.SubjectWiseAttendeance.AttendReportAdapter;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Male_FemaleAdapter extends RecyclerView.Adapter<Male_FemaleAdapter.MyViewHolder> {

    private Context mContext;
    ArrayList<ClassBean> dataList;
    public Male_FemaleAdapter(Context mContext, ArrayList<ClassBean> dataList) {
        this.mContext = mContext;
        this.dataList = dataList;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.mon_class_report_new_male_female, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        if (position == 0) {
            holder.lytHeader.setVisibility(View.GONE);
            holder.lytData.setVisibility(View.GONE);
        } else {
            holder.lytHeader.setVisibility(View.GONE);
            holder.lytData.setVisibility(View.VISIBLE);
            final ClassBean data = dataList.get(position - 1);
            holder.txtRoll.setText(data.getName());
//            holder.txtFName.setText(data.getFather_name());
//            if(data.get)
            holder.txtAbsent.setText(data.getFemale_student());
         //   holder.txtLeave.setText(data.getMale_student());
            holder.txtPresent.setText(data.getMale_student());

            int total=0,a=0,p=0,l=0;
            a= Integer.parseInt(data.getFemale_student());
            p= Integer.parseInt(data.getMale_student());
          //  l= Integer.parseInt(data.getL_student());
            total=a+p+l;
            holder.txtName.setText(String.valueOf(total));

        }

    }

    @Override
    public int getItemCount() {
        return (dataList.size() + 1);
    }


    public void setList(ArrayList<ClassBean> dataList) {
        this.dataList = dataList;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txtAbsent)
        TextView txtAbsent;
        @BindView(R.id.txtLeave)
        TextView txtLeave;
        @BindView(R.id.txtPresent)
        TextView txtPresent;
        @BindView(R.id.txtName)
        TextView txtName;
        @BindView(R.id.txtRoll)
        TextView txtRoll;
        @BindView(R.id.lytData)
        LinearLayout lytData;
        @BindView(R.id.lytHeader)
        LinearLayout lytHeader;
        @BindView(R.id.txtFName)
        TextView txtFName;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
