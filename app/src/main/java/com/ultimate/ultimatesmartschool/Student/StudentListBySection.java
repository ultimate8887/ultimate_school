package com.ultimate.ultimatesmartschool.Student;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;
import com.ultimate.ultimatesmartschool.BeanModule.ClassBean;
import com.ultimate.ultimatesmartschool.BeanModule.SectionBean;
import com.ultimate.ultimatesmartschool.BeanModule.Studentbean;
import com.ultimate.ultimatesmartschool.BeanModule.User;
import com.ultimate.ultimatesmartschool.Homework.SectionnewAdapter;
import com.ultimate.ultimatesmartschool.Login.LoginActivity;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.StudentAttendance.ClassAdapterAttend;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.CommonProgress;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class StudentListBySection extends AppCompatActivity implements Studentadapter.StudentPro {
    RecyclerView.LayoutManager layoutManager;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    //private Studentadapter adapter;
    private Studentadapter adapter;
    @BindView(R.id.textNorecord)
    TextView txtNorecord;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.parent)
    RelativeLayout parent;
    ArrayList<Studentbean> hwList = new ArrayList<>();
    private int loaded = 0;
    ArrayList<ClassBean> classList = new ArrayList<>();
    List<String> classidsel;
    String classid = "", class_name = "";
    @BindView(R.id.totalRecord)
    TextView totalRecord;
    @BindView(R.id.spinnerClass)
    Spinner spinnerClass;
    @BindView(R.id.spinnersection)
    Spinner spinnersection;
    ArrayList<SectionBean> sectionList = new ArrayList<>();
    String sectionid = "";
    String sectionname;
    boolean isDark = false;
    @BindView(R.id.imgBack)
    ImageView back;
    CommonProgress commonProgress;
    @BindView(R.id.export)
    FloatingActionButton export;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_list_by_name_i_d);
        ButterKnife.bind(this);
        commonProgress = new CommonProgress(StudentListBySection.this);
        classidsel = new ArrayList<>();
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        //fetchStudent();
        layoutManager = new LinearLayoutManager(StudentListBySection.this);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new Studentadapter(hwList, StudentListBySection.this, this);
        recyclerView.setAdapter(adapter);
        txtTitle.setText("Section-Wise List");
        parent.setBackgroundColor(getResources().getColor(R.color.white));
        //fetchClass();
        if (getIntent().getExtras() != null) {
            classid = getIntent().getExtras().getString("classid");
            sectionid= getIntent().getExtras().getString("sec");
        }
        fetchClass_new();

        isDark = !isDark;
        if (isDark) {

            parent.setBackgroundColor(getResources().getColor(R.color.white));


        } else {
            parent.setBackgroundColor(getResources().getColor(R.color.white));

        }

    }

    private void fetchClass_new() {
        commonProgress.show();
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("check", "true");
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.CLASSLIST_URL, classapiCallback1, this, params);
    }

    ApiHandler.ApiCallback classapiCallback1 = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            commonProgress.dismiss();
            if (error == null) {
                try {
                    classList = ClassBean.parseClassArray(jsonObject.getJSONArray(Constants.CLASSDATA));
                    ClassAdapterAttend adapter = new ClassAdapterAttend(StudentListBySection.this, classList,0);
                    spinnerClass.setAdapter(adapter);
                    spinnerClass.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if (i > 0) {
                                classid = classList.get(i - 1).getId();
                                class_name = classList.get(i - 1).getName();
                                //  fetchStudent();
                                fetchSection();
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {


                        }
                    });
//                    // setting here spinner item selected with position..
                    if (!classid.equalsIgnoreCase("")) {
                        int i = 0;
                        String id = classid;
                        for (ClassBean obj : classList) {
                            if (obj.getId().equalsIgnoreCase(id)) {
                                spinnerClass.setSelection(i + 1);
                                fetchStudent();
                                break;
                            }
                            i++;
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                // Utils.showSnackBar(error.getMessage(), parent);
            }
        }
    };

//    private void fetchClass() {
//        if (loaded == 0) {
//            commonProgress.show();
//        }
//        loaded++;
//
//        HashMap<String, String> params = new HashMap<String, String>();
//        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.CLASSLIST_URL, classapiCallback, this, params);
//    }
//
//    ApiHandler.ApiCallback classapiCallback = new ApiHandler.ApiCallback() {
//        @Override
//        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
//            ErpProgress.cancelProgressBar();
//            commonProgress.dismiss();
//            if (error == null) {
//                try {
//                    if (classList != null) {
//                        classList.clear();
//                    }
//                    classList = ClassBean.parseClassArray(jsonObject.getJSONArray(Constants.CLASSDATA));
//                    List<String> dstring = new ArrayList<>();
//                    for (ClassBean cobj : classList) {
//                        dstring.add(cobj.getName());
//                        Log.e("getclassid",cobj.getId());
//                    }
//
//                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(StudentListBySection.this, android.R.layout.simple_list_item_multiple_choice, dstring);
//
//                    multiselectSpinnerclass.setListAdapter(adapter).setListener(new BaseMultiSelectSpinner.MultiSpinnerListener() {
//                        @Override
//                        public void onItemsSelected(boolean[] selected) {
//                            fetchSection();
//                            Log.e("dcfcds", "fdsf");
//                            String value = multiselectSpinnerclass.getSpinnerText();
//                            List<String> clas = new ArrayList<>();
//                            classidsel.clear();
//                            if (!value.isEmpty()) {
//                                clas = Arrays.asList(value.split("\\s*,\\s*"));
//                            }
//                            for (String dval : clas) {
//                                for (ClassBean obj : classList) {
//                                    if (obj.getName().equalsIgnoreCase(dval)) {
//                                        classidsel.add(obj.getId());
//                                        //  textView7.setVisibility(View.GONE);
//
//                                        break;
//                                    }
//                                }
//                            }
//
//                            //fetchHomework();
//                            Log.e("dcfcds", value + "  ,  classid:  " + classidsel);
//                            // }
//                        }
//                    }).setSelectAll(false)
//                            .setSpinnerItemLayout(R.layout.support_simple_spinner_dropdown_item)
//                            .setTitle("Select Class")
//
//                            .setMaxSelectedItems(1)
//                    ;
//
//
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//
//            } else {
//                Utils.showSnackBar(error.getMessage(), parent);
//                if (error.getStatusCode() == 405) {
//                    User.logout();
//                    startActivity(new Intent(StudentListBySection.this, LoginActivity.class));
//                    finish();
//                }
//            }
//        }
//    };


    private void fetchSection() {
        commonProgress.show();
        HashMap<String, String> params = new HashMap<String, String>();
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.SECTIONLIST_URL, sectionapiCallback, this, params);
    }

    ApiHandler.ApiCallback sectionapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            commonProgress.dismiss();
            if (error == null) {
                try {
                    sectionList = SectionBean.parseCommonArray(jsonObject.getJSONArray("hostelroom_data"));
                    SectionnewAdapter adapter = new SectionnewAdapter(StudentListBySection.this, sectionList);
                    spinnersection.setAdapter(adapter);
                    spinnersection.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if (i > 0) {
                                sectionid = sectionList.get(i - 1).getSection_id();
                                sectionname = sectionList.get(i - 1).getSection_name();
                                fetchStudent();
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                    if (!sectionid.equalsIgnoreCase("")) {
                        int i = 0;
                        String id = sectionid;
                        for (SectionBean obj : sectionList) {
                            if (obj.getSection_id().equalsIgnoreCase(id)) {
                                spinnersection.setSelection(i + 1);
                                fetchStudent();
                                break;
                            }
                            i++;
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Utils.showSnackBar(error.getMessage(), parent);
                if (error.getStatusCode() == 405) {
                    User.logout();
                    startActivity(new Intent(StudentListBySection.this, LoginActivity.class));
                    finish();
                }
            }
        }
    };

    public void fetchStudent() {
        //  ErpProgress.showProgressBar(this,"please wait...");
        if (loaded == 0) {
            commonProgress.show();
        }
        loaded++;
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("class_id", String.valueOf(classid));
        params.put("section_id", sectionid);
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.STUDENTLISTCLSSECWISE_URL, apiCallback, this, params);

    }

    ApiHandler.ApiCallback apiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            commonProgress.dismiss();
            if (error == null) {
                ErpProgress.cancelProgressBar();
                try {
                    if (hwList != null) {
                        hwList.clear();
                    }
                    JSONArray jsonArray = jsonObject.getJSONArray("student_list");
                    hwList = Studentbean.parseHWArray(jsonArray);
                    if (hwList.size() > 0) {
                        adapter.setHQList(hwList);
                        adapter.notifyDataSetChanged();
                        txtNorecord.setVisibility(View.GONE);
                        totalRecord.setText("Total Entries:- " + String.valueOf(hwList.size()));
                        export.setVisibility(View.VISIBLE);
                    } else {
                        export.setVisibility(View.GONE);
                        totalRecord.setText("Total Entries:- 0");
                        hwList.clear();
                        adapter.setHQList(hwList);
                        adapter.notifyDataSetChanged();
                        txtNorecord.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                export.setVisibility(View.GONE);
                totalRecord.setText("Total Entries:- 0");
                txtNorecord.setVisibility(View.VISIBLE);
                hwList.clear();
                adapter.setHQList(hwList);
                adapter.notifyDataSetChanged();
            }
        }
    };


    @SuppressLint("RestrictedApi")
    @OnClick(R.id.export)
    public void submit() {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.S) {
            importData();
        } else {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {

                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    Toast.makeText(this, "Please provide storage permission from app settings", Toast.LENGTH_LONG).show();
                } else {

                    ActivityCompat.requestPermissions(this,
                            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            1);
                }
            } else {
                importData();

            }
        }
    }

    @SuppressLint("RestrictedApi")
    private void importData() {
        // File filePath = new File(Environment.getExternalStorageDirectory() + "/Demo.xls");
        Workbook wb = new HSSFWorkbook();

        Cell cell = null;

        String ndate = "", sub_staff = "", sub_date = "";

        sub_staff = class_name + "_" + sectionname;

        Sheet sheet = null;
        sheet = wb.createSheet(sub_staff + " Students List");
        //Now column and row
        Row row = sheet.createRow(0);

        cell = row.createCell(0);
        cell.setCellValue("S No");

        cell = row.createCell(1);
        cell.setCellValue("Roll No");

        cell = row.createCell(2);
        cell.setCellValue("Class/Section");

        cell = row.createCell(3);
        cell.setCellValue("Student Name");

        cell = row.createCell(4);
        cell.setCellValue("Father Name");

        cell = row.createCell(5);
        cell.setCellValue("Phone");


        //column width
        sheet.setColumnWidth(0, (20 * 100));
        sheet.setColumnWidth(1, (20 * 150));
        sheet.setColumnWidth(2, (30 * 150));
        sheet.setColumnWidth(3, (30 * 250));
        sheet.setColumnWidth(4, (30 * 200));
        sheet.setColumnWidth(5, (20 * 100));


        for (int i = 0; i < hwList.size(); i++) {

            Row row1 = sheet.createRow(i + 1);

            cell = row1.createCell(0);
            cell.setCellValue(i + 1);

            cell = row1.createCell(1);
            cell.setCellValue(hwList.get(i).getId());

            cell = row1.createCell(2);
            cell.setCellValue(hwList.get(i).getClass_name() + " (" + hwList.get(i).getSection_name() + ")");

            cell = row1.createCell(3);
            cell.setCellValue((hwList.get(i).getName()));
            //  cell.setCellStyle(cellStyle);

            cell = row1.createCell(4);
            cell.setCellValue(hwList.get(i).getFather_name());

            cell = row1.createCell(5);
            cell.setCellValue(hwList.get(i).getPhoneno());

            sheet.setColumnWidth(0, (20 * 100));
            sheet.setColumnWidth(1, (20 * 150));
            sheet.setColumnWidth(2, (30 * 150));
            sheet.setColumnWidth(3, (30 * 250));
            sheet.setColumnWidth(4, (30 * 200));
            sheet.setColumnWidth(5, (20 * 100));

        }
        String fileName;

        fileName = sub_staff + "_students_list_" + System.currentTimeMillis() + ".xls";

        // File file = new File(getActivity().getExternalFilesDir(null), fileName);

        File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS), fileName);

        FileOutputStream fileOutputStream = null;

        try {
            fileOutputStream = new FileOutputStream(file);
            wb.write(fileOutputStream);
            Utils.openSuccessDialog("Success!", "Export file in " + file, "", this);
        } catch (IOException e) {
            Utils.openErrorDialog("Error writing Exception: " + e, this);
        } catch (Exception e) {
            Utils.openErrorDialog("Failed to save file due to Exception: " + e, this);
        } finally {
            try {
                if (null != fileOutputStream) {
                    fileOutputStream.close();
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

    }

    @SuppressLint("RestrictedApi")
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 1 && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            importData();
        } else {
            Toast.makeText(getApplicationContext(), "Permission Denied", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void StudentProfile(Studentbean std) {
        Gson gson = new Gson();
        String student = gson.toJson(std, Studentbean.class);
        Intent i = new Intent(StudentListBySection.this, StudentProfile.class);
        i.putExtra("student", student);
        startActivity(i);
    }

    @Override
    public void onUpdateCallback(Studentbean std) {
        Gson gson = new Gson();
        String student = gson.toJson(std, Studentbean.class);
        Intent i = new Intent(this, UpdateStudentDetails.class);
        i.putExtra("student", student);
        i.putExtra("tag", "sec");
        i.putExtra("sec", sectionid);
        i.putExtra("class", classid);
        startActivity(i);
    }

    @Override
    public void onBlockCallback(Studentbean std) {
        commonProgress.show();
        HashMap<String, String> params = new HashMap<>();
        params.put("stu_id", std.getId());
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.BLOCKSTUDENTURL, apisendmsgCallback, this, params);
    }


    @Override
    public void stdCallBack(Studentbean std) {
        String phoneNo = std.getPhoneno();
        Log.i("email", phoneNo);
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + phoneNo));
        startActivity(intent);
    }

    @Override
    public void stdEmailCallBack(Studentbean std) {

        String phoneNo = std.getPhoneno();
        Uri uri = Uri.parse("smsto:" + phoneNo);
        Intent i = new Intent(Intent.ACTION_SENDTO, uri);
        i.setPackage("com.whatsapp");
        startActivity(Intent.createChooser(i, "Chat Now!"));
    }

    @Override
    public void onResume() {
        super.onResume();
        //  fetchStudent();
    }

    ApiHandler.ApiCallback apisendmsgCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            if (error == null) {

//                    Toast.makeText(getApplicationContext(),jsonObject.getString("msg"),Toast.LENGTH_LONG).show();
                Utils.showSnackBar("Student Blocked!", parent);
                fetchStudent();

            } else {
                Log.e("error", error.getMessage() + "");
                Utils.showSnackBar(error.getMessage(), parent);
                // Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_LONG).show();
                if (error.getStatusCode() == 405) {
                    User.logout();
                    startActivity(new Intent(StudentListBySection.this, LoginActivity.class));
                    finish();
                }
            }
        }
    };
    @Override
    public void onDeleteCallback(Studentbean std) {
        commonProgress.show();
        HashMap<String, String> params = new HashMap<>();
        params.put("h_id", std.getId());
        params.put("check", "student");
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.DELETEHW, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                ErpProgress.cancelProgressBar();
                commonProgress.dismiss();
                if (error == null) {

                    Utils.showSnackBar("Student Deleted!", parent);
                    // getActivity().finish();
                    fetchStudent();
                } else {
                    Log.e("error", error.getMessage() + "");
                    Utils.showSnackBar(error.getMessage(), parent);
                    if (error.getStatusCode() == 405) {
                        User.logout();
                        startActivity(new Intent(StudentListBySection.this, LoginActivity.class));
                        finish();
                    }
                }
            }
        }, this, params);
    }
}
