package com.ultimate.ultimatesmartschool.Student.STD_Fragment;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

public class StdTabPager extends FragmentStatePagerAdapter {

    public StdTabPager(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new AllStudentListFragment();
            case 1:
                return new ActiveStudentListFragment();
            case 2:
                return new NonActiveStudentListFragment();
            default:
                break;
        }
        return null;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        // Generate title based on item position
        switch (position) {
            case 0:
                return "All Students";
            case 1:
                return "Active";
            case 2:
                return "Non-Active";
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return 3;
    }
}
