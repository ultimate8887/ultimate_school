package com.ultimate.ultimatesmartschool.Student.STD_Fragment;

import android.content.Context;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.ultimate.ultimatesmartschool.Assignment.CommonAssignFragment;

public class StdTabPager_Enquiry extends FragmentStatePagerAdapter {
    int v;
    Context context;
    public StdTabPager_Enquiry(FragmentManager fm, int v, Context context) {
        super(fm);
        this.context = context;
        this.v=v;
        //Log.e("Send_StdFragment",String.valueOf(v));
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new EnquiryFragment("p",v,context);
            case 1:
                return new EnquiryFragment("a",v,context);
            case 2:
                return new EnquiryFragment("e",v,context);
            default:
                break;
        }
        return null;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        // Generate title based on item position
        switch (position) {
            case 0:
                return "Pending";
            case 1:
                return "Done";
            case 2:
                return "Decline";
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return 3;
    }
}
