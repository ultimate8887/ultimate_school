package com.ultimate.ultimatesmartschool.Student.STD_Fragment;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.daimajia.swipe.SimpleSwipeListener;
import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputLayout;
import com.squareup.picasso.Picasso;
import com.ultimate.ultimatesmartschool.BeanModule.Studentbean;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Student.Studentadapter;
import com.ultimate.ultimatesmartschool.Utility.Utils;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SaprateAdapter extends RecyclerSwipeAdapter<SaprateAdapter.Viewholder> implements Filterable {
    public ArrayList<Studentbean> stulist;
    private ArrayList<Studentbean> filterModelClass;
    Context mContext;
    SaprateAdapter.StudentPro mStudentPro;
    int value;
    public SaprateAdapter(ArrayList<Studentbean> stulist, Context mContext, SaprateAdapter.StudentPro mStudentPro,int value) {
        this.mContext = mContext;
        this.mStudentPro=mStudentPro;
        this.stulist = stulist;
        this.filterModelClass = stulist;
        this.value = value;
    }

    @Override
    public SaprateAdapter.Viewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.new_item_news, parent, false);
        SaprateAdapter.Viewholder viewholder = new SaprateAdapter.Viewholder(view);
        return viewholder;
    }

    @Override
    public void onBindViewHolder(final SaprateAdapter.Viewholder holder, @SuppressLint("RecyclerView") final int position) {


        //  holder.image.setAnimation(AnimationUtils.loadAnimation(mContext,R.anim.fade_transition_animation));
        //  holder.parent.setAnimation(AnimationUtils.loadAnimation(mContext,R.anim.fade_scale_animation));

        Studentbean mData=stulist.get(position);

        // holder.employee_id.setText("Emp.ID: " + " " + viewstafflist.get(position).getId());

        if (mData.getId() != null) {
            String title = getColoredSpanned("Admission No: ", "#000000");
            String Name = getColoredSpanned(mData.getId(), "#5A5C59");
            holder.employee_id.setText(Html.fromHtml(title + " " + Name));
        }

        // holder.employee_name.setText("Name: " + " " + viewstafflist.get(position).getName());
        if (mData.getName() != null) {
            String title = getColoredSpanned("Name: ", "#000000");
            String Name = getColoredSpanned(mData.getName(), "#5A5C59");
            holder.employee_name.setText(Html.fromHtml(title + " " + Name));
        }else {
            holder.employee_name.setText("Name: Not Mentioned");
        }

        if (mData.getEs_housesid() != null) {
            holder.txtHouse.setVisibility(View.VISIBLE);
            String title = getColoredSpanned("House: ", "#000000");
            String Name = getColoredSpanned(mData.getEs_housename(), "#5A5C59");
            holder.txtHouse.setText(Html.fromHtml(title + " " + Name));
        }else {
            holder.txtHouse.setVisibility(View.GONE);
            holder.txtHouse.setText("House: Not Mentioned");
        }

        //  holder.post.setText("Post:" + " " + viewstafflist.get(position).getPost());
        if (mData.getFather_name() != null) {
            String title = getColoredSpanned("Father's Name: ", "#000000");
            String Name = getColoredSpanned(mData.getFather_name(), "#5A5C59");
            holder.post.setText(Html.fromHtml(title + " " + Name));
        }else {
            holder.post.setText("Father's Name: Not Mentioned");
        }

        //  holder.employee_dob.setText("Date of Birth: " + " " + viewstafflist.get(position).getDateofbirth());
        if (mData.getDob() != null) {
            String title = getColoredSpanned("Date of Birth: ", "#000000");
            String Name = getColoredSpanned(Utils.getDateFormated(mData.getDob()), "#5A5C59");
            holder.employee_dob.setText(Html.fromHtml(title + " " + Name));
        }else {
            holder.employee_dob.setText("DOB: Not Mentioned");
        }

        // holder.employee_doj.setText("Date of Joining: " + " " + viewstafflist.get(position).getDateofjoining());
        if (mData.getSection_name() != null) {
            String title = getColoredSpanned("Class: ", "#000000");
            String Name = getColoredSpanned(mData.getClass_name()+"("+stulist.get(position).getSection_name()+")", "#5A5C59");
            holder.employee_doj.setText(Html.fromHtml(title + " " + Name));
        }else if (mData.getClass_name() != null) {
            String title = getColoredSpanned("Class: ", "#000000");
            String Name = getColoredSpanned(mData.getClass_name(), "#5A5C59");
            holder.employee_doj.setText(Html.fromHtml(title + " " + Name));
        }
        else {
            holder.employee_doj.setText("Class: Not Mentioned");
        }

        if (value!=1){
            //  holder.status.setVisibility(View.VISIBLE);
            String attend="";
            if(mData.getDevice_token() !=null){
                attend ="Active";
                holder.employee_designation.setTextColor(ContextCompat.getColor(mContext, R.color.present));
                holder.image.setBorderColor(ContextCompat.getColor(mContext, R.color.present));
            }else {
                attend="Non-Active";
                holder.employee_designation.setTextColor(ContextCompat.getColor(mContext, R.color.absent));
                holder.image.setBorderColor(ContextCompat.getColor(mContext, R.color.absent));
            }
            holder.employee_designation.setText(attend);
        }else {
            // holder.status.setVisibility(View.GONE);
        }


        if ( mData.getEmail() != null) {

            String title = getColoredSpanned("Email: ", "#000000");
            String Name = getColoredSpanned("</u>" +mData.getEmail()+"</u>", "#5A5C59");
            holder.email.setText(Html.fromHtml(title + " " + Name));
        }else {
            holder.email.setText("Email: Not Mentioned");
        }



        holder.phoneLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mData.getPhoneno() != null) {
                    mStudentPro.stdCallBack(stulist.get(position));
                }else {
                    Toast.makeText(mContext,"Phone Number Not Found!",Toast.LENGTH_SHORT).show();
                }
            }
        });


        holder.googleLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (mData.getPhoneno() != null) {
                    mStudentPro.stdEmailCallBack(stulist.get(position));
                }else {
                    Toast.makeText(mContext,"Number Not Found!",Toast.LENGTH_SHORT).show();
                }

            }
        });


        if (mData.getGender().equalsIgnoreCase("male")) {
            if (stulist.get(position).getProfile() != null) {
                //  Picasso.with(context).load(viewstafflist.get(position).getImage()).placeholder(context.getResources().getDrawable(R.drawable.man)).into(holder.image);
                Utils.progressImg(stulist.get(position).getProfile(),holder.image,mContext);
            } else {
                // Picasso.with(context).load(String.valueOf(context.getResources().getDrawable(R.drawable.staffimg))).into(holder.image);
                Picasso.get().load(R.drawable.boy).into(holder.image);

            }
        }else {
            if (stulist.get(position).getProfile() != null) {
                //  Picasso.with(context).load(viewstafflist.get(position).getImage()).placeholder(context.getResources().getDrawable(R.drawable.man)).into(holder.image);
                Utils.progressImg(stulist.get(position).getProfile(),holder.image,mContext);
            } else {
                // Picasso.with(context).load(String.valueOf(context.getResources().getDrawable(R.drawable.staffimg))).into(holder.image);
                Picasso.get().load(R.drawable.female_teacher).into(holder.image);

            }
        }


        mItemManger.bindView(holder.itemView, position);



        if (mData.getAge() != null) {
            if (mData.getAge() != null) {
                String tit = getColoredSpanned("Age: ", "#000000");
                String title = getColoredSpanned("  "+mData.getAge(), "#F4212C");
                String Name = getColoredSpanned(" years old", "#5A5C59");
                holder.phone.setText(Html.fromHtml(tit + " "+ title + " " + Name));
            }else {
                holder.phone.setText("Age: Not Found");
            }


            holder.inpSchoolId.setHint("Birthday Card Seen Status");
            if (mData.getDate_time() != null) {
                //holder.login_device.setText("Birthday Card Seen at " + Utils.getTimeHr(mData.getDate_time()));
                String title = getColoredSpanned("Seen at ", "#000000");
                String Name = getColoredSpanned("</u>" +Utils.getTimeHr(mData.getDate_time())+"</u>", "#1C8B3B");
                holder.login_device.setText(Html.fromHtml(title + " " + Name));

            }else{

                String title = getColoredSpanned("Not Seen yet", "#F4212C");
                String Name = getColoredSpanned("", "#5A5C59");
                holder.login_device.setText(Html.fromHtml(title + " " + Name));

                // holder.login_device.setText("Not Seen yet");
            }
            holder.update.setVisibility(View.GONE);
            holder.email.setVisibility(View.GONE);
//            holder.phone.setVisibility(View.GONE);
            holder.view5.setVisibility(View.GONE);
        }else {
            holder.inpSchoolId.setHint("Login Device");
            // holder.email.setVisibility(View.VISIBLE);
            holder.update.setVisibility(View.VISIBLE);
            if (mData.getPhoneno() != null) {
                String title = getColoredSpanned("Mobile: ", "#000000");
                String Name = getColoredSpanned("</u>" +mData.getPhoneno()+"</u>", "#5A5C59");
                holder.phone.setText(Html.fromHtml(title + " " + Name));
            }else {
                holder.phone.setText("Mobile: Not Mentioned");
            }


            holder.phone.setVisibility(View.VISIBLE);
            holder.view5.setVisibility(View.VISIBLE);
            if (mData.getMobile_name() != null) {
                holder.login_device.setText(mData.getMobile_brand()+"-"+mData.getMobile_name());
                holder.login_device.setTextColor(ContextCompat.getColor(mContext, R.color.present));

            }else {
                holder.login_device.setText("Not Detect");
                holder.login_device.setTextColor(ContextCompat.getColor(mContext, R.color.textMedHeavy));

            }

            holder.trash.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View view) {

                    if (mStudentPro != null) {
                        mStudentPro.onUpdateCallback(stulist.get(position));
                    }
                }
            });

            holder.update.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View view) {
                    //  Toast.makeText(view.getContext(), "clicked " + holder.employee_name.getText().toString() + "!", Toast.LENGTH_SHORT).show();

                    AlertDialog.Builder builder2 = new AlertDialog.Builder(mContext);
                    builder2.setMessage("Do you want to Block? ");
                    builder2.setCancelable(false);
                    builder2.setPositiveButton(
                            "Yes", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    if (mStudentPro != null) {
                                        mStudentPro.onBlockCallback(stulist.get(position));

                                    }
                                    stulist.remove(position);
                                    notifyItemRemoved(position);
                                    notifyItemRangeChanged(position, stulist.size());
                                    mItemManger.closeAllItems();
                                    //     Toast.makeText(view.getContext(), "Blocked " + holder.employee_name.getText().toString() + "!", Toast.LENGTH_SHORT).show();
                                }
                            });
                    builder2.setNegativeButton(
                            "No", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();
                                }
                            });
                    AlertDialog alert11 = builder2.create();
                    alert11.show();

                }
            });

            holder.delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View view) {
                    //  Toast.makeText(view.getContext(), "clicked " + holder.employee_name.getText().toString() + "!", Toast.LENGTH_SHORT).show();

                    AlertDialog.Builder builder2 = new AlertDialog.Builder(mContext);
                    builder2.setMessage("Do you want to delete this student permanently? ");
                    builder2.setCancelable(false);
                    builder2.setPositiveButton(
                            "Yes", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    if (mStudentPro != null) {
                                        mStudentPro.onDeleteCallback(stulist.get(position));

                                    }
                                    stulist.remove(position);
                                    notifyItemRemoved(position);
                                    notifyItemRangeChanged(position, stulist.size());
                                    mItemManger.closeAllItems();
                                    //      Toast.makeText(view.getContext(), "Deleted " + holder.employee_name.getText().toString() + "!", Toast.LENGTH_SHORT).show();
                                }
                            });
                    builder2.setNegativeButton(
                            "No", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();
                                }
                            });
                    AlertDialog alert11 = builder2.create();
                    alert11.show();

                }
            });


        }


        holder.swipeLayout.setShowMode(SwipeLayout.ShowMode.PullOut);
        holder.swipeLayout.addSwipeListener(new SimpleSwipeListener() {
            @Override
            public void onOpen(SwipeLayout layout) {

            }

            @Override
            public void onStartClose(SwipeLayout layout) {
                holder.swipeLayout.close();
            }
        });

        holder.swipeLayout.setOnDoubleClickListener(new SwipeLayout.DoubleClickListener() {
            @Override
            public void onDoubleClick(SwipeLayout layout, boolean surface) {
                Toast.makeText(mContext, "DoubleClick", Toast.LENGTH_SHORT).show();
            }
        });


        // holder.txtid.setText("ID:"+stulist.get(position).getId());

        holder.parent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mStudentPro!= null){
                    mStudentPro.StudentProfile(stulist.get(position));
                }
            }
        });



    }

    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }

    public interface StudentPro{
        void StudentProfile(Studentbean std);
        void onUpdateCallback(Studentbean std);
        void onDeleteCallback(Studentbean std);
        void onBlockCallback(Studentbean std);
        void stdCallBack(Studentbean std);
        void stdEmailCallBack(Studentbean std);
    }
    @Override
    public int getItemCount() {
        if (stulist != null)
            return stulist.size();
        return 0;
    }



    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe;
    }

    public void setHQList(ArrayList<Studentbean> stulist) {
        this.stulist = stulist;
    }

    public class Viewholder extends RecyclerView.ViewHolder {

        @BindView(R.id.swipe)
        SwipeLayout swipeLayout;
        @BindView(R.id.id)
        TextView employee_id;
        @BindView(R.id.txtstuName)
        EditText employee_name;
        @BindView(R.id.txtDT)
        TextView employee_designation;
        @BindView(R.id.txtdob)
        TextView employee_dob;
        @BindView(R.id.txtdoj)
        TextView employee_doj;
        @BindView(R.id.txtReason)
        EditText login_device;
        @BindView(R.id.txtFatherName)
        TextView post;
        @BindView(R.id.profile)
        CircularImageView image;
        @BindView(R.id.txtPhone)
        TextView phone;
        @BindView(R.id.googleLogin)
        FloatingActionButton googleLogin;
        @BindView(R.id.phoneLogin)
        FloatingActionButton phoneLogin;
        //        @BindView(R.id.fphone)
//        TextView fphone;
        @BindView(R.id.txtHouse)
        TextView txtHouse;
        @BindView(R.id.email)
        TextView email;
        @BindView(R.id.parent)
        RelativeLayout parent;
        @BindView(R.id.inpUsername)
        TextInputLayout inpSchoolId;
        @BindView(R.id.trash)
        ImageView trash;
        @BindView(R.id.update)
        ImageView update;

        @BindView(R.id.delete)
        ImageView delete;

        @BindView(R.id.view5)
        View view5;

        public Viewholder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            parent =(RelativeLayout)itemView.findViewById(R.id.parent);

//            circleimg=(CircularImageView)itemView.findViewById(R.id.circleimg);
//
//            textViewData = (TextView) itemView.findViewById(R.id.name);
//            txtFather = (TextView) itemView.findViewById(R.id.fathername);
//            txtMobile = (TextView) itemView.findViewById(R.id.mobileno);
//            txtClass = (TextView) itemView.findViewById(R.id.classess);
//            // textViewPos = (TextView) itemView.findViewById(R.id.position);
//
//            txtid=(TextView)itemView.findViewById(R.id.id);
        }
    }

    @Override
    public Filter getFilter() {

        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {

                String Key = constraint.toString();
                if (Key.isEmpty()) {

                    filterModelClass = stulist ;

                }
                else {
                    ArrayList<Studentbean> lstFiltered = new ArrayList<>();
                    for (Studentbean row : stulist) {

                        if (row.getName().toLowerCase().contains(Key.toLowerCase())||row.getId().toLowerCase().contains(Key.toLowerCase())){
                            lstFiltered.add(row);
                        }

                    }

                    filterModelClass = lstFiltered;

                }


                FilterResults filterResults = new FilterResults();
                filterResults.values= filterModelClass;
                return filterResults;

            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {


                stulist = (ArrayList<Studentbean>) results.values;
                notifyDataSetChanged();

            }
        };

    }
}
