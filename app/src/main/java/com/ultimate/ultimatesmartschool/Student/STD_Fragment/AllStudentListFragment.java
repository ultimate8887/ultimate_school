package com.ultimate.ultimatesmartschool.Student.STD_Fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetSequence;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;
import com.ultimate.ultimatesmartschool.BeanModule.Studentbean;
import com.ultimate.ultimatesmartschool.BeanModule.User;
import com.ultimate.ultimatesmartschool.Login.LoginActivity;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Student.StudentProfile;
import com.ultimate.ultimatesmartschool.Student.Studentadapter;
import com.ultimate.ultimatesmartschool.Student.StudentgateAttendadapter;
import com.ultimate.ultimatesmartschool.Student.UpdateStudentDetails;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.CommonProgress;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.content.Context.MODE_PRIVATE;


public class AllStudentListFragment extends Fragment implements SaprateAdapter.StudentPro {


    RecyclerView NewsRecyclerview;
    //NewsAdapter newsAdapter;
    //List<NewsItem> mData;
    SharedPreferences sharedPreferences;
    FloatingActionButton fabSwitcher;
    boolean isDark = false;
    RelativeLayout rootLayout;
    EditText searchInput ;
    CharSequence search="";
    ArrayList<Studentbean> mData = new ArrayList<>();
    @BindView(R.id.textNorecord)
    TextView txtNorecord;
    @BindView(R.id.root_layout)
    RelativeLayout parent;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.imgBack)
    ImageView back;
    @BindView(R.id.totalRecord)
    TextView totalRecord;
    RecyclerView.LayoutManager layoutManager;
    private SaprateAdapter newsAdapter;
    public String classname="";
    CommonProgress commonProgress;
    @BindView(R.id.export)
    FloatingActionButton export;

    public AllStudentListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=  inflater.inflate(R.layout.fragment_common_student_list, container, false);

        ButterKnife.bind(this,view);

        commonProgress=new CommonProgress(getActivity());
        fabSwitcher = view.findViewById(R.id.fab_switcher);
        rootLayout = view.findViewById(R.id.root_layout);
        searchInput = view.findViewById(R.id.search_input);
        NewsRecyclerview = view.findViewById(R.id.news_rv);
        txtTitle.setVisibility(View.GONE);
        back.setVisibility(View.GONE);
        //  searchInput.setBackgroundResource(R.drawable.search_input_dark_style);
        rootLayout.setBackgroundColor(getResources().getColor(R.color.white));

        layoutManager = new LinearLayoutManager(getActivity());
        NewsRecyclerview.setLayoutManager(layoutManager);
        // newsAdapter = new NewsAdapter(this,mData);
        newsAdapter = new SaprateAdapter(mData,getActivity(),this,2);
        NewsRecyclerview.setAdapter(newsAdapter);
        fetchStudent();

        isDark = !isDark ;
        if (isDark) {

            rootLayout.setBackgroundColor(getResources().getColor(R.color.white));
            //      searchInput.setBackgroundResource(R.drawable.search_input_dark_style);

        }
        else {
            rootLayout.setBackgroundColor(getResources().getColor(R.color.white));
            //          searchInput.setBackgroundResource(R.drawable.search_input_style);
        }

        if (!search.toString().isEmpty()){

            newsAdapter.getFilter().filter(search);

        }

        searchInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if(s.length()==0)
                {
                    fetchStudent();
                }else {

                    newsAdapter.getFilter().filter(s);
                    search = s;
                    totalRecord.setText("Search Record:- " +String.valueOf(newsAdapter.stulist.size()));
                    if (newsAdapter.stulist.size()==0){
                        export.setVisibility(View.GONE);
                    }

                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        return view;
    }

    public void fetchStudent() {
        // ErpProgress.showProgressBar(this,"please wait...");
        commonProgress.show();
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("class_id", "0");
        params.put("all", "1");
        params.put("tag", "all");
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.studentlistclass, apiCallback, getActivity(), params);

    }

    ApiHandler.ApiCallback apiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {

            commonProgress.dismiss();
            if (error == null) {

                try {
                    if (mData != null) {
                        mData.clear();
                    }
                    if (mData != null){
                        mData.clear();
                        mData=Studentbean.parseHWArray(jsonObject.getJSONArray("student_list"));
                        classname=mData.get(0).getClass_name();
                        newsAdapter.setHQList(mData);
                        newsAdapter.notifyDataSetChanged();

                        totalRecord.setVisibility(View.VISIBLE);
                        totalRecord.setText("Total Students:- "+String.valueOf(mData.size()));
                        txtNorecord.setVisibility(View.GONE);
                        export.setVisibility(View.VISIBLE);
                        if (!restorePrefDataP()){
                            setShowcaseViewP();
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                export.setVisibility(View.GONE);

                txtNorecord.setVisibility(View.VISIBLE);
                mData.clear();


                Toast.makeText(getActivity(),"No record found",Toast.LENGTH_LONG).show();


                newsAdapter.setHQList(mData);
                newsAdapter.notifyDataSetChanged();


            }
        }
    };

    private void setShowcaseViewP() {
        new TapTargetSequence(getActivity())
                .targets(TapTarget.forView(export,"Export Excel!","Tap the Export button to export Students list.")
                        .outerCircleColor(R.color.light)
                        .outerCircleAlpha(0.96f)
                        .targetCircleColor(R.color.white)
                        .titleTextSize(20)
                        .titleTextColor(R.color.white)
                        .descriptionTextSize(14)
                        .descriptionTextColor(R.color.black)
                        .textColor(R.color.black)
                        .textTypeface(Typeface.SANS_SERIF)
                        .dimColor(R.color.black)
                        .drawShadow(true)
                        .cancelable(false)
                        .tintTarget(true)
                        .transparentTarget(true)
                        .targetRadius(40)).listener(new TapTargetSequence.Listener() {
                    @RequiresApi(api = Build.VERSION_CODES.N)
                    @Override
                    public void onSequenceFinish() {
                        // Toast.makeText(getApplicationContext(),"Sequence Finished",Toast.LENGTH_SHORT).show();
                        savePrefDataP();

                    }

                    @Override
                    public void onSequenceStep(TapTarget lastTarget, boolean targetClicked) {
                        // Toast.makeText(getActivity(),"GREAT!",Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onSequenceCanceled(TapTarget lastTarget) {

                    }
                }).start();
    }

    private void savePrefDataP(){
        sharedPreferences=getActivity().getSharedPreferences("boarding_pref_view_export",MODE_PRIVATE);
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putBoolean("IsFirstTimeVisit_view_export",true);
        editor.apply();
    }

    private boolean restorePrefDataP(){
        sharedPreferences = getActivity().getSharedPreferences("boarding_pref_view_export",MODE_PRIVATE);
        return sharedPreferences.getBoolean("IsFirstTimeVisit_view_export",false);
    }

    @SuppressLint("RestrictedApi")
    @OnClick(R.id.export)
    public void submit() {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.S) {
            importData();
        } else {
            if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {

                if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    Toast.makeText(getActivity(), "Please provide storage permission from app settings", Toast.LENGTH_LONG).show();
                } else {

                    ActivityCompat.requestPermissions(getActivity(),
                            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            1);
                }
            } else {
                importData();

            }
        }
    }

    @SuppressLint("RestrictedApi")
    private void importData() {
        // File filePath = new File(Environment.getExternalStorageDirectory() + "/Demo.xls");
        Workbook wb = new HSSFWorkbook();

        Cell cell = null;

        String ndate="",sub_staff="",sub_date="";

        sub_staff="All";

        Sheet sheet = null;
        sheet = wb.createSheet(sub_staff+" Students List");
        //Now column and row
        Row row = sheet.createRow(0);

        cell = row.createCell(0);
        cell.setCellValue("S No");

        cell = row.createCell(1);
        cell.setCellValue("Roll No");

        cell = row.createCell(2);
        cell.setCellValue("Class/Section");

        cell = row.createCell(3);
        cell.setCellValue("Student Name");

        cell = row.createCell(4);
        cell.setCellValue("Father Name");

        cell = row.createCell(5);
        cell.setCellValue("Gender");

        cell = row.createCell(6);
        cell.setCellValue("Phone");

        cell = row.createCell(7);
        cell.setCellValue("Login Status");


        //column width
        sheet.setColumnWidth(0, (20 * 100));
        sheet.setColumnWidth(1, (20 * 150));
        sheet.setColumnWidth(2, (30 * 150));
        sheet.setColumnWidth(3, (30 * 250));
        sheet.setColumnWidth(4, (30 * 200));
        sheet.setColumnWidth(5, (20 * 100));
        sheet.setColumnWidth(6, (20 * 200));
        sheet.setColumnWidth(7, (30 * 200));

        for (int i = 0; i < mData.size(); i++) {

            Row row1 = sheet.createRow(i + 1);

            cell = row1.createCell(0);
            cell.setCellValue(i+1);

            cell = row1.createCell(1);
            cell.setCellValue(mData.get(i).getId());

            cell = row1.createCell(2);
            cell.setCellValue(mData.get(i).getClass_name()+" ("+mData.get(i).getSection_name()+")");

            cell = row1.createCell(3);
            cell.setCellValue((mData.get(i).getName()));
            //  cell.setCellStyle(cellStyle);

            cell = row1.createCell(4);
            cell.setCellValue(mData.get(i).getFather_name());


            cell = row1.createCell(5);
            cell.setCellValue(mData.get(i).getGender());

            cell = row1.createCell(6);
            cell.setCellValue(mData.get(i).getPhoneno());


            cell = row1.createCell(7);
            String status="";
            if (mData.get(i).getMobile_name()!=null){
                status="Active";
            }else {
                status="Non-Active";
            }
            cell.setCellValue(status);


            sheet.setColumnWidth(0, (20 * 100));
            sheet.setColumnWidth(1, (20 * 150));
            sheet.setColumnWidth(2, (30 * 150));
            sheet.setColumnWidth(3, (30 * 250));
            sheet.setColumnWidth(4, (30 * 200));
            sheet.setColumnWidth(5, (20 * 100));
            sheet.setColumnWidth(6, (20 * 200));
            sheet.setColumnWidth(7, (30 * 200));

        }
        String fileName;

        fileName = sub_staff+"_students_list_"+ System.currentTimeMillis() + ".xls";

        // File file = new File(getActivity().getExternalFilesDir(null), fileName);

        File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS), fileName);

        FileOutputStream fileOutputStream = null;

        try {
            fileOutputStream = new FileOutputStream(file);
            wb.write(fileOutputStream);
            Utils.openSuccessDialog("Success!","Export file in "+file,"",getActivity());
        } catch (IOException e) {
            Utils.openErrorDialog("Error writing Exception: "+ e,getActivity());
        } catch (Exception e) {
            Utils.openErrorDialog("Failed to save file due to Exception: "+ e,getActivity());
        } finally {
            try {
                if (null != fileOutputStream) {
                    fileOutputStream.close();
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

    }

    @SuppressLint("RestrictedApi")
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == 1 && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            importData();
        } else {
            Toast.makeText(getActivity(), "Permission Denied", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        fetchStudent();
    }


    @Override
    public void onDeleteCallback(Studentbean std) {
        commonProgress.show();
        HashMap<String, String> params = new HashMap<>();
        params.put("h_id", std.getId());
        params.put("check", "student");
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.DELETEHW, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                ErpProgress.cancelProgressBar();
                commonProgress.dismiss();
                if (error == null) {

                    Utils.showSnackBar("Student Deleted!", parent);
                    // getActivity().finish();
                    fetchStudent();
                } else {
                    Log.e("error", error.getMessage() + "");
                    Utils.showSnackBar(error.getMessage(), parent);
                    if (error.getStatusCode() == 405) {
                        User.logout();
                        startActivity(new Intent(getActivity(), LoginActivity.class));
                        getActivity().finish();
                    }
                }
            }
        }, getActivity(), params);
    }

    @Override
    public void StudentProfile(Studentbean std) {
        Gson gson  = new Gson();
        String student = gson.toJson(std,Studentbean.class);
        Intent i = new Intent(getActivity(), StudentProfile.class);
        i.putExtra("student",student);
        startActivity(i);

    }

    @Override
    public void onUpdateCallback(Studentbean std) {
        //   Toast.makeText(getApplicationContext(),"Enter!",Toast.LENGTH_SHORT).show();
        Gson gson  = new Gson();
        String student = gson.toJson(std,Studentbean.class);
        Intent i = new Intent(getActivity(), UpdateStudentDetails.class);
        i.putExtra("student",student);
        i.putExtra("tag", "frag");
        startActivity(i);

    }

    @Override
    public void onBlockCallback(Studentbean std) {
        commonProgress.show();
        HashMap<String, String> params = new HashMap<>();
        params.put("stu_id", std.getId());
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.BLOCKSTUDENTURL, apisendmsgCallback, getActivity(), params);
    }

    ApiHandler.ApiCallback apisendmsgCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            if (error == null) {

//                    Toast.makeText(getApplicationContext(),jsonObject.getString("msg"),Toast.LENGTH_LONG).show();
                Utils.showSnackBar("Student Blocked!", parent);
                // getActivity().finish();
                fetchStudent();

            } else {
                Log.e("error", error.getMessage() + "");
                Utils.showSnackBar(error.getMessage(), parent);
                // Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_LONG).show();
                if (error.getStatusCode() == 405) {
                    User.logout();
                    startActivity(new Intent(getActivity(), LoginActivity.class));
                    getActivity().finish();
                }
            }
        }
    };

    @Override
    public void stdCallBack(Studentbean std) {
        String phoneNo = std.getPhoneno();
        Log.i("email",phoneNo);
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:"+ phoneNo));
        startActivity(intent);
    }

    @Override
    public void stdEmailCallBack(Studentbean std) {

        String phoneNo = std.getPhoneno();
        Uri uri = Uri.parse("smsto:" + phoneNo);
        Intent i = new Intent(Intent.ACTION_SENDTO, uri);
        i.setPackage("com.whatsapp");
        startActivity(Intent.createChooser(i, "Chat Now!"));
    }
}