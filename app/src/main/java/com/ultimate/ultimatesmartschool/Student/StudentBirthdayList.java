package com.ultimate.ultimatesmartschool.Student;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewSwitcher;

import com.android.volley.Request;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.squareup.picasso.Picasso;
import com.ultimate.ultimatesmartschool.BeanModule.ClassBean;
import com.ultimate.ultimatesmartschool.BeanModule.Studentbean;
import com.ultimate.ultimatesmartschool.BeanModule.User;
import com.ultimate.ultimatesmartschool.Fee.AddFeesQRActivity;
import com.ultimate.ultimatesmartschool.Login.LoginActivity;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.CommonProgress;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import nl.dionsegijn.konfetti.KonfettiView;
import nl.dionsegijn.konfetti.models.Shape;
import nl.dionsegijn.konfetti.models.Size;

public class StudentBirthdayList extends AppCompatActivity implements Studentadapter.StudentPro {
    Dialog mBottomSheetDialog1;
    ArrayList<ClassBean> classList = new ArrayList<>();

    KonfettiView konfettiView;
    Button btnSetting;
    ImageButton export;
    ImageView close;
    TextView bdayMsg;
    RelativeLayout layout0, layoutW;
    ViewSwitcher logo0, logo1, logo2, logo3, logow, logox, logoy, logoz;
    @BindView(R.id.parent)
    RelativeLayout parent;
    //@BindView(R.id.spinner) Spinner spinnerClass;
//    @BindView(R.id.spinner2)
//    MultiSelectSpinner multiselectSpinnerclass;
    @BindView(R.id.spinerVehicletype)
    Spinner spinnerClass;
    String classid = "";
    private int loaded = 0;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    List<String> classidsel;
    ArrayList<Studentbean> hwList = new ArrayList<>();
    private LinearLayoutManager layoutManager;
    @BindView(R.id.recyclerviewlist)
    RecyclerView recyclerview;
    @BindView(R.id.dialog)
    ImageView dialog;
    private Studentadapter adapter;
    @BindView(R.id.textNorecord)
    TextView txtNorecord;
    @BindView(R.id.textView8)
    TextView textView8;
    @BindView(R.id.totalRecord)
    TextView totalRecord;
    CommonProgress commonProgress;
    public String date;
    Animation animation;
    String tag="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_staff_leave_list);
        ButterKnife.bind(this);
        getTodayDate();
        if (getIntent().getExtras() !=null){
            tag = getIntent().getExtras().getString("id");
        }else {
            tag="";
        }

        animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.btn_blink_animation);
        commonProgress = new CommonProgress(StudentBirthdayList.this);
        classidsel = new ArrayList<>();
        textView8.setText("Filter Class");
        if (tag.equalsIgnoreCase("weekly")){
            txtTitle.setText("Upcoming Birthday List");
        }else {
            txtTitle.setText("Today's Birthday List");
        }
        dialog.setVisibility(View.GONE);
        //fetchClass();
        fetchClass_new();
        layoutManager = new LinearLayoutManager(StudentBirthdayList.this);
        recyclerview.setLayoutManager(layoutManager);
        adapter = new Studentadapter(hwList, StudentBirthdayList.this, this);
        recyclerview.setAdapter(adapter);

    }

    @OnClick(R.id.dialog)
    public void dialog() {
        Intent intent = new Intent(StudentBirthdayList.this, AddFeesQRActivity.class);
        intent.putExtra("key", "card");
        startActivity(intent);
    }

    public void getTodayDate() {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.HOUR_OF_DAY, 00);
        c.set(Calendar.MINUTE, 00);
        c.set(Calendar.SECOND, 00);
        Date setdate = c.getTime();
        SimpleDateFormat fmtOut = new SimpleDateFormat("dd MMM, yyyy");
        String dateString = fmtOut.format(setdate);
        // adddate.setText(dateString);
        SimpleDateFormat dateFrmOut = new SimpleDateFormat("yyyy-MM-dd");
        date = dateFrmOut.format(setdate);
    }

    private void fetchClass_new() {
        commonProgress.show();
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("check", "true");
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.CLASSLIST_URL, classapiCallback1, this, params);
    }

    ApiHandler.ApiCallback classapiCallback1 = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            commonProgress.dismiss();
            if (error == null) {
                try {
                    classList = ClassBean.parseClassArray(jsonObject.getJSONArray(Constants.CLASSDATA));
                    NewClassAdapter adapter = new NewClassAdapter(StudentBirthdayList.this, classList);
                    spinnerClass.setAdapter(adapter);
                    spinnerClass.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if (i > 0) {
                                classid = classList.get(i - 1).getId();
//                                fetchStudent(classid);
                            }
                            fetchStudent(i);
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {


                        }
                    });
//                    // setting here spinner item selected with position..
//                    int i = 0;
//                    String id=user.getClass_id();
//                    for ( ClassBean obj : classList) {
//                        if (obj.getId().equalsIgnoreCase(id)) {
//                            spinnerClass.setSelection(i + 1);
//                            break;
//                        }
//                        i++;
//                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                // Utils.showSnackBar(error.getMessage(), parent);
            }
        }
    };

    @OnClick(R.id.imgBack)
    public void onback() {
        finish();
    }


//    private void fetchClass() {
//        if (loaded == 0) {
//            commonProgress.show();
//        }
//        loaded++;
//
//        HashMap<String, String> params = new HashMap<String, String>();
//        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.CLASSLIST_URL, classapiCallback, this, params);
//    }
//
//    ApiHandler.ApiCallback classapiCallback = new ApiHandler.ApiCallback() {
//        @Override
//        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
//            ErpProgress.cancelProgressBar();
//            commonProgress.dismiss();
//            if (error == null) {
//                try {
//                    if (classList != null) {
//                        classList.clear();
//                    }
//                    classList = ClassBean.parseClassArray(jsonObject.getJSONArray(Constants.CLASSDATA));
//                    List<String> dstring = new ArrayList<>();
//                    for (ClassBean cobj : classList) {
//                        dstring.add(cobj.getName());
//                        Log.e("getclassid",cobj.getId());
//                    }
//
//                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(StudentList.this, android.R.layout.simple_list_item_multiple_choice, dstring);
//
//                    multiselectSpinnerclass.setListAdapter(adapter).setListener(new BaseMultiSelectSpinner.MultiSpinnerListener() {
//                        @Override
//                        public void onItemsSelected(boolean[] selected) {
//
//                            Log.e("dcfcds", "fdsf");
//                            String value = multiselectSpinnerclass.getSpinnerText();
//                            List<String> clas = new ArrayList<>();
//                            classidsel.clear();
//                            if (!value.isEmpty()) {
//                                clas = Arrays.asList(value.split("\\s*,\\s*"));
//                            }
//                            for (String dval : clas) {
//                                for (ClassBean obj : classList) {
//                                    if (obj.getName().equalsIgnoreCase(dval)) {
//                                        classidsel.add(obj.getId());
//                                        //  textView7.setVisibility(View.GONE);
//
//                                        break;
//                                    }
//                                }
//                            }
//
//                            fetchStudent();
//                            Log.e("dcfcds", value + "  ,  classid:  " + classidsel);
//                            // }
//                        }
//                    }).setSelectAll(false)
//                            .setSpinnerItemLayout(R.layout.support_simple_spinner_dropdown_item)
//                            .setTitle("Select Class")
//
//                            .setMaxSelectedItems(1)
//                    ;
//
//
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//
//            } else {
//                Utils.showSnackBar(error.getMessage(), parent);
//                if (error.getStatusCode() == 405) {
//                    User.logout();
//                    startActivity(new Intent(StudentList.this, LoginActivity.class));
//                    finish();
//                }
//            }
//        }
//    };


    public void fetchStudent(int i) {
        commonProgress.show();
        HashMap<String, String> params = new HashMap<String, String>();
        if (i == 0) {
            params.put("classid", "");
        } else {
            params.put("classid", classid);
        }
        params.put("check", "student");
        params.put("date", date);
        params.put("tag", tag);
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.birthdaylist, apiCallback, this, params);

    }

    ApiHandler.ApiCallback apiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            commonProgress.dismiss();
            if (error == null) {
                try {
                    if (hwList != null) {
                        hwList.clear();
                    }
                    JSONArray jsonArray = jsonObject.getJSONArray("birthday_data");
                    hwList = Studentbean.parseHWArray(jsonArray);
                    if (hwList.size() > 0) {
                        adapter.setHQList(hwList);
                        adapter.notifyDataSetChanged();
                        txtNorecord.setVisibility(View.GONE);
                        totalRecord.setText("Total Entries:- " + String.valueOf(hwList.size()));
                    } else {
                        totalRecord.setText("Total Entries:- 0");
                        hwList.clear();
                        adapter.setHQList(hwList);
                        adapter.notifyDataSetChanged();
                        txtNorecord.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                totalRecord.setText("Total Entries:- 0");
                txtNorecord.setVisibility(View.VISIBLE);
                hwList.clear();
                adapter.setHQList(hwList);
                adapter.notifyDataSetChanged();
            }
        }
    };

    @SuppressLint("MissingInflatedId")
    @Override
    public void StudentProfile(Studentbean std) {
        mBottomSheetDialog1 = new Dialog(this);
        final View sheetView = getLayoutInflater().inflate(R.layout.birthday_dialog_new, null);
        mBottomSheetDialog1.setContentView(sheetView);

        mBottomSheetDialog1.setCancelable(false);


        RelativeLayout scrollView = (RelativeLayout) sheetView.findViewById(R.id.parent);

        CircularImageView profile = (CircularImageView) sheetView.findViewById(R.id.profile);
        CircularImageView school_profile = (CircularImageView) sheetView.findViewById(R.id.school_profile);

        TextView birth_name = (TextView) sheetView.findViewById(R.id.birth_name);
        TextView school_name = (TextView) sheetView.findViewById(R.id.school_name);
        TextView std_name = (TextView) sheetView.findViewById(R.id.std_name);
        export = (ImageButton) sheetView.findViewById(R.id.export);
        close = (ImageView) sheetView.findViewById(R.id.close);

        // Define an array of strings
        String[] strings = {String.valueOf(R.string.bday1),String.valueOf(R.string.bday2),String.valueOf(R.string.bday3), String.valueOf(R.string.bday4),
                String.valueOf(R.string.bday5),String.valueOf(R.string.bday6),String.valueOf(R.string.bday7),String.valueOf(R.string.bday8),String.valueOf(R.string.bday9)};
        // Generate a random index
        Random random = new Random();
        int randomIndex = random.nextInt(strings.length);
        // Select the string at the random index
        String selectedString = strings[randomIndex];
        // Do something with the selected string (e.g., print it)
        // System.out.println(selectedString);
        birth_name.setText(getResources().getText(Integer.parseInt(selectedString)));

        std_name.setText(std.getName());
        school_name.setText(User.getCurrentUser().getSchoolData().getName());
        //school_name.setText(User.getCurrentUser().getSchoolData().getName()+"\n"+User.getCurrentUser().getSchoolData().getName());

        if (User.getCurrentUser().getSchoolData().getLogo()!=null) {
            Picasso.get().load(User.getCurrentUser().getSchoolData().getLogo()).placeholder(R.color.transparent).into(school_profile);
        } else {
            Picasso.get().load(R.color.transparent).into(school_profile);
        }


        if (std.getProfile()!=null) {
            Picasso.get().load(std.getProfile()).placeholder(R.color.transparent).into(profile);
        } else {
            Picasso.get().load(R.color.transparent).into(profile);
        }

        final MediaPlayer mp = MediaPlayer.create(this, R.raw.bdaytone);
        mp.start();
        mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                mp.release();
            }
        });

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                close.startAnimation(animation);
                mp.release();
                // saveData();
                mBottomSheetDialog1.dismiss();

            }
        });

        export.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                export.startAnimation(animation);
                export.setVisibility(View.INVISIBLE);
                close.setVisibility(View.INVISIBLE);
//                viewed.setVisibility(View.VISIBLE);

                Bitmap bitmap = getBitmapFromView(scrollView, scrollView.getChildAt(0).getHeight(), scrollView.getChildAt(0).getWidth());
                // Bitmap bitmap = Bitmap.createBitmap(v1.getDrawingCache());
                // v1.setDrawingCacheEnabled(false);
                saveImageToGallery(bitmap,std.getName());

            }
        });

        mBottomSheetDialog1.show();
        Window window = mBottomSheetDialog1.getWindow();
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    }

    private void saveImageToGallery(Bitmap bitmap,String name){

        OutputStream fos;
        String school=User.getCurrentUser().getSchoolData().getName();
        long time= System.currentTimeMillis();
        try{

            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q){

                ContentResolver resolver = getContentResolver();
                ContentValues contentValues =  new ContentValues();
                contentValues.put(MediaStore.MediaColumns.DISPLAY_NAME, name+"_BirthdayCard"+time+ ".jpg");
                contentValues.put(MediaStore.MediaColumns.MIME_TYPE, "image/jpeg");
                contentValues.put(MediaStore.MediaColumns.RELATIVE_PATH, Environment.DIRECTORY_PICTURES + File.separator + school);
                Uri imageUri = resolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, contentValues);

                fos = resolver.openOutputStream(Objects.requireNonNull(imageUri));

                Toast.makeText(this, "Image Saved", Toast.LENGTH_SHORT).show();

            }
            else{
                String imagesDir = Environment.getExternalStoragePublicDirectory(
                        Environment.DIRECTORY_PICTURES).toString() + File.separator + school;

                File file = new File(imagesDir);

                if (!file.exists()) {
                    file.mkdir();
                }

                File image = new File(imagesDir, name+"_BirthdayCard"+time+ ".jpg");
                fos = new FileOutputStream(image);

                Toast.makeText(this, "Image saved to internal!!", Toast.LENGTH_SHORT).show();
                //resetOpTimes();

            }

            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            Objects.requireNonNull(fos);


        }catch(Exception e){

            Toast.makeText(this, "Image not saved \n" + e.toString(), Toast.LENGTH_SHORT).show();
        }
        export.setVisibility(View.VISIBLE);
        close.setVisibility(View.VISIBLE);
    }

    private Bitmap getBitmapFromView(View view, int height, int width) {
        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        Drawable bgDrawable = view.getBackground();
        if (bgDrawable != null)
            bgDrawable.draw(canvas);
        else
            canvas.drawColor(Color.WHITE);
        view.draw(canvas);
        return bitmap;
    }

    private void layoutOneView() {

        // we didnt initialized the layout0 and layoutW


        layout0.setVisibility(View.VISIBLE);
        layoutW.setVisibility(View.GONE);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (logo0.getDisplayedChild() == 0) {
                    logo0.showNext();
                    bdayMsg.setText(getResources().getText(R.string.bday2));
                } else logo0.showPrevious();
            }
        }, 10000);  // 10000 = 10 seconds

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (logo1.getDisplayedChild() == 0) {
                    logo1.showNext();
                    bdayMsg.setText(getResources().getText(R.string.bday3));
                } else logo1.showPrevious();
            }
        }, 20000);  // we will increase seconds in every viewswitcher... you will understand it in the output


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (logo2.getDisplayedChild() == 0) {
                    logo2.showNext();
                    bdayMsg.setText(getResources().getText(R.string.bday4));
                } else logo2.showPrevious();
            }
        }, 30000);  // 10000 = 10 seconds


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (logo3.getDisplayedChild() == 0) {
                    logo3.showNext();
                    bdayMsg.setText(getResources().getText(R.string.bday5));
                } else logo3.showPrevious();
            }
        }, 40000);  // 10000 = 10 seconds

        // one layout is not visible in first 40 seconds... now its term to visible to layoutW

        layoutTwoView();
    }

    private void layoutTwoView() {

        layout0.setVisibility(View.GONE);
        layoutW.setVisibility(View.VISIBLE);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (logow.getDisplayedChild() == 0) {
                    logow.showNext();
                    bdayMsg.setText(getResources().getText(R.string.bday6));
                } else logow.showPrevious();
            }
        }, 10000);
        // we will write 10000, not 50000 becuase the another layoutW is in begin state

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (logox.getDisplayedChild() == 0) {
                    logox.showNext();
                    bdayMsg.setText(getResources().getText(R.string.bday7));
                } else logox.showPrevious();
            }
        }, 20000);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (logoy.getDisplayedChild() == 0) {
                    logoy.showNext();
                    bdayMsg.setText(getResources().getText(R.string.bday8));
                } else logoy.showPrevious();
            }
        }, 30000);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (logoz.getDisplayedChild() == 0) {
                    logoz.showNext();
                    bdayMsg.setText(getResources().getText(R.string.bday9));
                } else logoz.showPrevious();
            }
        }, 40000);

    }

    @Override
    public void onUpdateCallback(Studentbean std) {
//        HashMap<String, String> params = new HashMap<>();
//        params.put("stu_id", std.getId());
//        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.BLOCKSTUDENTURL, apisendmsgCallback, this, params);
    }

    @Override
    public void onDeleteCallback(Studentbean std) {

    }

    @Override
    public void onBlockCallback(Studentbean std) {

    }

    @Override
    public void stdCallBack(Studentbean std) {
        String phoneNo = std.getPhoneno();
        Log.i("email", phoneNo);
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + phoneNo));
        startActivity(intent);
    }

    @Override
    public void stdEmailCallBack(Studentbean std) {

        String phoneNo = std.getPhoneno();
        Uri uri = Uri.parse("smsto:" + phoneNo);
        Intent i = new Intent(Intent.ACTION_SENDTO, uri);
        i.setPackage("com.whatsapp");
        startActivity(Intent.createChooser(i, "Chat Now!"));
    }


    ApiHandler.ApiCallback apisendmsgCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            if (error == null) {
                try {
                    Toast.makeText(getApplicationContext(), jsonObject.getString("msg"), Toast.LENGTH_LONG).show();
                    Utils.showSnackBar(jsonObject.getString("msg"), parent);
                    finish();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("error", error.getMessage() + "");
                Utils.showSnackBar(error.getMessage(), parent);
                // Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_LONG).show();
                if (error.getStatusCode() == 405) {
                    User.logout();
                    startActivity(new Intent(StudentBirthdayList.this, LoginActivity.class));
                    finish();
                }
            }
        }
    };
}