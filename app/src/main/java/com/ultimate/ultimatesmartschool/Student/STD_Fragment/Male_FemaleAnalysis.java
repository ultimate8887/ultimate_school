package com.ultimate.ultimatesmartschool.Student.STD_Fragment;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.ultimate.ultimatesmartschool.BeanModule.ClassBean;
import com.ultimate.ultimatesmartschool.Examination.ClassAdapter;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.StudentAttendance.SubjectWiseAttendeance.AttendReportAdapter;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.CommonProgress;
import com.ultimate.ultimatesmartschool.Utility.Constants;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class Male_FemaleAnalysis extends AppCompatActivity {

    @BindView(R.id.recyclerviewlist)
    RecyclerView recyclerView;
    Dialog dialog;
    @BindView(R.id.parent)
    RelativeLayout parent;
    @BindView(R.id.lytHeader)
    LinearLayout lytHeader;

    @BindView(R.id.lytData)
    LinearLayout lytData;

    @BindView(R.id.txtName)
    TextView t_mf;

    @BindView(R.id.txtPresent)
    TextView t_m;

    @BindView(R.id.txtAbsent)
    TextView t_f;

    @BindView(R.id.txtRoll)
    TextView class_sec;

    int male=0,female=0,total_mf=0;

    int check=0;

    ArrayList<ClassBean> dataList = new ArrayList<>();
    private Male_FemaleAdapter adapter;
    CommonProgress commonProgress;
    @BindView(R.id.textNorecord)
    TextView txtNorecord;
    @BindView(R.id.totalRecord)
    TextView totalRecord;
    @BindView(R.id.today_date)
    TextView cal_text;
    @BindView(R.id.root1)
    RelativeLayout root1;
    String from_date = "";
    String image_url = "", school = "", className = "";
    String classid = "";
    ArrayList<ClassBean> classList = new ArrayList<>();
    @BindView(R.id.spinerVehicletype)
    Spinner spinnerClass;
    @BindView(R.id.txtTitle)
    TextView txtTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_male_female_analysis);
        ButterKnife.bind(this);
        commonProgress = new CommonProgress(this);
        //   root1.setVisibility(View.VISIBLE);
        txtTitle.setText("Male Female Analysis");
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new Male_FemaleAdapter(this, dataList);
        recyclerView.setAdapter(adapter);
        fetchClass();
        fetchClassData();

    }


    @OnClick(R.id.imgBack)
    public void backFinish() {
        finish();
    }

    private void fetchClass() {
        HashMap<String, String> params = new HashMap<String, String>();
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.CLASSLIST_URL, classapiCallback, getApplicationContext(), params);
    }


    ApiHandler.ApiCallback classapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            if (error == null) {
                try {
                    classList = ClassBean.parseClassArray(jsonObject.getJSONArray(Constants.CLASSDATA));
                    ClassAdapter adapter = new ClassAdapter(getApplicationContext(), classList);
                    spinnerClass.setAdapter(adapter);
                    spinnerClass.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if (i > 0) {
                                classid = "";
                                className = "";
                                classid = classList.get(i - 1).getId();
                                className = classList.get(i - 1).getName();
                                fetchClassData();
                            }else{
                                classid = "";
                                className = "";
                                fetchClassData();
                            }

                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
                // Utils.showSnackBar(error.getMessage(), view.findViewById(R.id.parent));
            }
        }
    };


    private void fetchClassData() {
        check=0;
        male=0;
        female=0;
        total_mf=0;
        commonProgress.show();
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("class_id", classid);
        params.put("date", from_date);
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.CLASSLIST_FEES_URL, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                commonProgress.dismiss();
                if (error == null) {
                    try {
                        ArrayList<ClassBean> attendList = ClassBean.parseClassArray(jsonObject.getJSONArray(Constants.CLASSDATA));
                        dataList.clear();
                        dataList.addAll(attendList);
                        adapter.notifyDataSetChanged();
                        totalRecord.setText("Total Entries:- " + String.valueOf(dataList.size()));
                        //-----------end------------
                        txtNorecord.setVisibility(View.GONE);
                        if (check==0) {
                            check++;
                            for (int i = 0; i < dataList.size(); i++) {
                                male += Integer.parseInt(dataList.get(i).getMale_student());
                                female += Integer.parseInt(dataList.get(i).getFemale_student());
                            }
                        }
                        setData();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    totalRecord.setText("Total Entries:- 0");
                    txtNorecord.setVisibility(View.VISIBLE);
                    Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }, this, params);

    }

    private void setData() {
        if(!classid.equalsIgnoreCase("")){
            class_sec.setText(className);
        }else{
            class_sec.setText("All Classes");
        }
        lytHeader.setVisibility(View.VISIBLE);
        lytData.setVisibility(View.VISIBLE);
        t_f.setText(String.valueOf(female));
        t_m.setText(String.valueOf(male));
        total_mf=male+female;
        t_mf.setText(String.valueOf(total_mf));
    }

}