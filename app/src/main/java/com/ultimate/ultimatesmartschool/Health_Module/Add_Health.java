package com.ultimate.ultimatesmartschool.Health_Module;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.squareup.picasso.Picasso;
import com.ultimate.ultimatesmartschool.BeanModule.SectionBean;
import com.ultimate.ultimatesmartschool.BeanModule.Studentbean;
import com.ultimate.ultimatesmartschool.BeanModule.User;
import com.ultimate.ultimatesmartschool.ClassTest.Messageclass_adpter;
import com.ultimate.ultimatesmartschool.Examination.CheckReportCard;
import com.ultimate.ultimatesmartschool.Examination.ReportCard;
import com.ultimate.ultimatesmartschool.Homework.SectionnewAdapter;
import com.ultimate.ultimatesmartschool.Leave.OptionAdapter;
import com.ultimate.ultimatesmartschool.Leave.OptionBean;
import com.ultimate.ultimatesmartschool.Leave.StudentLeaveList;
import com.ultimate.ultimatesmartschool.Login.LoginActivity;
import com.ultimate.ultimatesmartschool.MSG_NEW.Fragments.Stu_adapterGds;
import com.ultimate.ultimatesmartschool.Performance_Report.OptionAdapter_new;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.StuGatePass.ClassBeans;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.CommonProgress;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;
import com.ultimate.ultimatesmartschool.Utility.ErpProgressVert;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class Add_Health extends AppCompatActivity {

    ArrayList<ClassBeans> classList = new ArrayList<>();
    ArrayList<SectionBean> sectionList = new ArrayList<>();
    private ArrayList<Studentbean> stuList = new ArrayList<>();

    @BindView(R.id.spinnerGames)
    Spinner spinnerGames;
    @BindView(R.id.spinnerMedicine)
    Spinner spinnerMedicine;
    @BindView(R.id.spinnerClasssec)
    Spinner classspin;
    @BindView(R.id.spinnersection)
    Spinner spinnersection;
    @BindView(R.id.spinnerStudent)
    Spinner spinnerStudent;
    @BindView(R.id.buttonnxtdetil)
    Button buttonnxtdetil;
    @BindView(R.id.parent)
    LinearLayout parent;

    String logo = "", s_sign = "", a_sign = "", school = "", name = "", testDate = "";
    String sectionid = "";
    String sectionname;
    String classid = "";
    String stu_id = "";
    private String examid = "";
    private String acdemicde = "";
    private String exam_name = "";
    private Stu_adapterGds adapterstu;
    String classname = "";
    private int loaded = 0;

    @BindView(R.id.edtName)
    EditText edtName;
    @BindView(R.id.edtFather)
    EditText edtFather;
    @BindView(R.id.edtRegistration)
    EditText edtRegistration;
    @BindView(R.id.edtMobile)
    EditText edtMobile;
    @BindView(R.id.student_lyt)
    LinearLayout student_lyt;
    @BindView(R.id.lyt_health)
    LinearLayout lyt_health;
    @BindView(R.id.dp)
    ImageView dp;
    CommonProgress commonProgress;


    @BindView(R.id.imgBack)
    ImageView back;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    ArrayList<OptionBean> medicineList = new ArrayList<>();
    ArrayList<OptionBean> gamesList = new ArrayList<>();
    String stuname = "",stuname_f="", dob = "", mother_name = "", profile = "";
    TextView Date, Upload;
    EditText edtAllergy, edtDisease, remarks,edtSubmitted;
    String medicine = "", games = "";
    OptionAdapter_new medicineAdapter, gamesAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_health);
        ButterKnife.bind(this);
        commonProgress = new CommonProgress(this);
        txtTitle.setText("Health & Medicine");
        edtAllergy = (EditText) findViewById(R.id.edtAllergy);
        edtDisease = (EditText) findViewById(R.id.edtDisease);
        edtSubmitted = (EditText) findViewById(R.id.edtSubmitted);
        remarks = (EditText) findViewById(R.id.TestTopics);
        Date = (TextView) findViewById(R.id.Date);

        medicineList.add(new OptionBean("2"));
        medicineList.add(new OptionBean("3"));
        medicineList.add(new OptionBean("4"));
        medicineList.add(new OptionBean("5"));

        medicineAdapter = new OptionAdapter_new(Add_Health.this, medicineList,"1");
        //set the ArrayAdapter to the spinner
        spinnerMedicine.setAdapter(medicineAdapter);
        //attach the listener to the spinner
        spinnerMedicine.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i != 0) {
                    medicine = medicineList.get(i - 1).getName();
                }else {
                    medicine="1";
                }
                //  fetchleavelist(tag);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        gamesList.add(new OptionBean("Yes"));
        gamesList.add(new OptionBean("No"));

        gamesAdapter = new OptionAdapter_new(Add_Health.this, gamesList,"Select");
        //set the ArrayAdapter to the spinner
        spinnerGames.setAdapter(gamesAdapter);
        //attach the listener to the spinner
        spinnerGames.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i != 0) {
                    games = gamesList.get(i - 1).getName();
                }else {
                    games="";
                }

                //  fetchleavelist(tag);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        Date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openDatePicker();

            }
        });

//        fetchAcademicyearlist();
//        fetchexamnamelist();
        fetchclasslist();
    }

    public void openDatePicker() {
        Calendar cal = Calendar.getInstance(TimeZone.getDefault()); // Get current date
        DatePickerDialog datePicker = new DatePickerDialog(this,
                R.style.MyDatePickerDialogTheme, ondate,
                cal.get(Calendar.YEAR),
                cal.get(Calendar.MONTH),
                cal.get(Calendar.DAY_OF_MONTH));
        // datePicker.getDatePicker().setMinDate(new Date().getTime());
        datePicker.setCancelable(false);
        datePicker.show();
    }

    DatePickerDialog.OnDateSetListener ondate = new DatePickerDialog.OnDateSetListener() {


        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

            Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
            calendar.set(year, monthOfYear, dayOfMonth);
            java.util.Date sub_date = calendar.getTime();
            Calendar value_datePicker = calendar;
            SimpleDateFormat fmtOut = new SimpleDateFormat("dd MMM, yyyy");
            String dateString = fmtOut.format(sub_date);
            Date.setText("Expire Date :  "+dateString);
            SimpleDateFormat fmtOut1 = new SimpleDateFormat("yyyy-MM-dd");
            testDate = fmtOut1.format(sub_date);
        }
    };

    private void fetchclasslist() {

        HashMap<String, String> params = new HashMap<String, String>();
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.CLASSLIST_URL, classapiCallback, this, params);
    }


    ApiHandler.ApiCallback classapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            if (error == null) {
                try {
                    classList = ClassBeans.parseClassArray(jsonObject.getJSONArray(Constants.CLASSDATA));
                    Messageclass_adpter adapter = new Messageclass_adpter(Add_Health.this, classList);
                    classspin.setAdapter(adapter);
                    classspin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

//                            if (i == 0) {

                            if (adapterstu != null) {
                                stuList.clear();
                                adapterstu.notifyDataSetChanged();
                            }
                            //  }
                            if (i > 0) {

                                classid = classList.get(i - 1).getId();
                                classname = classList.get(i - 1).getName();
                                stu_id = "";
                                fetchsection();
                                //   userprofile(classid);
                            } else {
                                classid = "";
                            }

                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {

                Utils.showSnackBar(error.getMessage(), parent);
            }
        }
    };

    private void fetchsection() {
        commonProgress.show();
        HashMap<String, String> params = new HashMap<String, String>();
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.SECTIONLIST_URL, sectionapiCallback, this, params);
    }

    ApiHandler.ApiCallback sectionapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            commonProgress.dismiss();
            if (error == null) {
                try {
                    sectionList = SectionBean.parseCommonArray(jsonObject.getJSONArray("hostelroom_data"));
                    SectionnewAdapter adapter = new SectionnewAdapter(Add_Health.this, sectionList);
                    spinnersection.setAdapter(adapter);
                    spinnersection.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            sectionid = "";
                            sectionname = "";
                            if (i > 0) {
                                sectionid = sectionList.get(i - 1).getSection_id();
                                sectionname = sectionList.get(i - 1).getSection_name();
                                fetchStudent(classid, sectionid);
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Utils.showSnackBar(error.getMessage(), parent);
                if (error.getStatusCode() == 405) {
                    User.logout();
                    startActivity(new Intent(Add_Health.this, LoginActivity.class));
                    finish();
                }
            }
        }
    };

    private void fetchStudent(String classid, String sectionid) {

        commonProgress.show();
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("class_id", classid);
        params.put("section_id", sectionid);
        Log.e("class_id", classid);
        Log.e("section_id", sectionid);
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.STUDENTLISTCLSSECWISE_URL, apiCallbackstudnt, this, params);
    }

    final ApiHandler.ApiCallback apiCallbackstudnt = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            commonProgress.dismiss();
            if (error == null) {
                try {
                    stuList = Studentbean.parseHWArray(jsonObject.getJSONArray("student_list"));
                    adapterstu = new Stu_adapterGds(Add_Health.this, stuList);
                    spinnerStudent.setAdapter(adapterstu);
                    spinnerStudent.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                            edtFather.setText("");
                            edtName.setText("");
                            edtRegistration.setText("");
                            edtMobile.setText("");
                            Picasso.get().load(R.drawable.stud).into(dp);

                            if (i > 0) {
                                stu_id = stuList.get(i - 1).getId();
                                stuname = stuList.get(i - 1).getName();

                                dob = stuList.get(i - 1).getDob();
                                mother_name = stuList.get(i - 1).getMother_name();
                                profile = stuList.get(i - 1).getProfile();
                                stuname_f=stuList.get(i - 1).getFather_name();

                                student_lyt.setVisibility(View.VISIBLE);
                                lyt_health.setVisibility(View.VISIBLE);
                                buttonnxtdetil.setVisibility(View.VISIBLE);

                                if (stuList.get(i - 1).getGender().equalsIgnoreCase("Male")) {
                                    if (stuList.get(i - 1).getProfile() != null) {
                                        Utils.progressImg(stuList.get(i - 1).getProfile(), dp, Add_Health.this);
                                        // Picasso.with(getActivity()).load(stuList.get(i-1).getProfile()).placeholder(R.drawable.stud).into(dp);
                                    } else {
                                        Picasso.get().load(R.drawable.stud).into(dp);
                                    }
                                } else {
                                    if (stuList.get(i - 1).getProfile() != null) {
                                        Utils.progressImg(stuList.get(i - 1).getProfile(), dp, Add_Health.this);
                                        //  Picasso.with(getActivity()).load(stuList.get(i-1).getProfile()).placeholder(R.drawable.f_student).into(dp);
                                    } else {
                                        Picasso.get().load(R.drawable.f_student).into(dp);
                                    }
                                }

                                edtFather.setText(stuList.get(i - 1).getFather_name());
                                edtName.setText(stuList.get(i - 1).getName());
                                edtRegistration.setText(stuList.get(i - 1).getClass_name() + "(" + stuList.get(i - 1).getId() + ")");
                                edtMobile.setText(stuList.get(i - 1).getPhoneno());

                                a_sign = stuList.get(i - 1).getA_signature();
                                s_sign = stuList.get(i - 1).getS_signature();

                            } else {

                                stu_id = "";
                            }

                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                lyt_health.setVisibility(View.GONE);
                buttonnxtdetil.setVisibility(View.GONE);
                student_lyt.setVisibility(View.GONE);
            }
        }
    };

    private boolean checkValid() {
        boolean valid = true;
        String errorMsg = null;

        if (classid.isEmpty() || classid == "") {
            valid = false;
            errorMsg = "Kindly select class";
        } else if (sectionid.isEmpty() || sectionid == "") {
            valid = false;
            errorMsg = "Kindly select class section";
        } else if (stu_id.isEmpty() || stu_id == "") {
            valid = false;
            errorMsg = "Kindly enter class Student";
        } else if (edtAllergy.getText().toString().isEmpty()) {
            valid = false;
            errorMsg = "Kindly enter Allergy";
        }else if (edtDisease.getText().toString().isEmpty()) {
            valid = false;
            errorMsg = "Kindly enter Disease";
        }else if (edtSubmitted.getText().toString().isEmpty()) {
            valid = false;
            errorMsg = "Medicine Submitted in School \nfield is required";
        }else if (medicine.isEmpty() || medicine == "") {
            valid = false;
            errorMsg = "How many Time Take \nMedicine in school time \nfield is required";
        }else if (testDate.isEmpty() || testDate == "") {
            valid = false;
            errorMsg = "Kindly add Expire Date of Medicine";
        }else  {
            if (games.isEmpty() || games == "") {
                valid = false;
                errorMsg = "Is Student Take part \nin Games during Medical \nIssues field is required";
            }
        }
//        else {
//            valid = false;
//            errorMsg = "Kindly select Exam name";
//
//        }

        if (!valid) {
            // Utils.showSnackBar(errorMsg, parent);
            Toast.makeText(getApplicationContext(), errorMsg, Toast.LENGTH_SHORT).show();
        }
        return valid;
    }

    @OnClick(R.id.buttonnxtdetil)
    public void nextbutton() {
        if (checkValid()) {
            ErpProgressVert.showProgressBar(Add_Health.this,"Please Wait......");
            HashMap<String, String> params = new HashMap<String, String>();
            params.put("es_regid", stu_id);
            params.put("class_id", classid);
            params.put("sec_id", sectionid);
            params.put("es_sname", stuname);
            params.put("es_fname", stuname_f);
            params.put("es_allergy", edtAllergy.getText().toString().trim());
            params.put("es_disease", edtDisease.getText().toString().trim());
            params.put("es_medicine_time", medicine);
            params.put("es_medicine", edtSubmitted.getText().toString().trim());
            params.put("es_expirydate", testDate);
            params.put("es_medical_issue", games);
            params.put("es_remarks", remarks.getText().toString().trim());
            ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.ADD_HEALTH, new ApiHandler.ApiCallback() {
                @Override
                public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                    ErpProgressVert.cancelProgressBar();
                    if (error == null) {
                        Utils.showSnackBar("Submit Successfully", parent);
                        finish();
                    } else {
                        Utils.showSnackBar("Fail To Upload", parent);
                        Log.e("16955", error.getMessage() + "");
                    }
                }
            }, this, params);
        }

    }
}