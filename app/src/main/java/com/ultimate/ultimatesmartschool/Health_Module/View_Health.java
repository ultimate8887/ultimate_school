package com.ultimate.ultimatesmartschool.Health_Module;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.ultimate.ultimatesmartschool.BeanModule.ClassBean;
import com.ultimate.ultimatesmartschool.BeanModule.User;
import com.ultimate.ultimatesmartschool.Examination.ClassAdapter;
import com.ultimate.ultimatesmartschool.Leave.OptionBean;
import com.ultimate.ultimatesmartschool.Leave.Staffleavelistadapter;
import com.ultimate.ultimatesmartschool.Leave.Stafflistbean;
import com.ultimate.ultimatesmartschool.Login.LoginActivity;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Transport.FuelExpenseList;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.CommonProgress;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class View_Health extends AppCompatActivity implements HealthAdapter.StudentPro {

    @BindView(R.id.recyclerviewlist)
    RecyclerView recyclerView;
    @BindView(R.id.parent)
    RelativeLayout parent;
    RecyclerView.LayoutManager layoutManager;
    HealthAdapter adapter;
    ArrayList<HealthBean> leavelist = new ArrayList<>();
    @BindView(R.id.textNorecord)
    TextView txtNorecord;
    int loaded = 0;
    String classid="";
    ArrayList<ClassBean> classList = new ArrayList<>();
    @BindView(R.id.totalRecord)
    TextView totalRecord;

    @BindView(R.id.spinerVehicletype)
    Spinner spinnerClass;

    CommonProgress commonProgress;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    String viewwwww="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_staff_leave_list);
        ButterKnife.bind(this);
        commonProgress=new CommonProgress(this);
        viewwwww = getIntent().getStringExtra("view");
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        if (viewwwww.equalsIgnoreCase("activity")) {
            adapter = new HealthAdapter(leavelist, this,this,1);
            txtTitle.setText("Health & Activity");
        } else {
            adapter = new HealthAdapter(leavelist, this,this,2);
            txtTitle.setText("Health & Medicine");
        }
        recyclerView.setAdapter(adapter);
        fetchClass();
    }

    @OnClick(R.id.imgBack)
    public void backCall(){
        finish();
    }


    private void fetchClass() {
        if (loaded == 0) {
            commonProgress.show();
        }
        loaded++;

        HashMap<String, String> params = new HashMap<String, String>();
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.CLASSLIST_URL, classapiCallback,getApplicationContext(), params);
    }


    ApiHandler.ApiCallback classapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            commonProgress.dismiss();
            if (error == null) {
                try {
                    classList = ClassBean.parseClassArray(jsonObject.getJSONArray(Constants.CLASSDATA));
                    ClassAdapter adapter = new ClassAdapter(getApplicationContext(), classList);
                    spinnerClass.setAdapter(adapter);
                    spinnerClass.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if (i > 0) {
                                classid = classList.get(i - 1).getId();
                                fetchleavelist(classid);
                            }else {
                                fetchleavelist(classid);
                            }

                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_LONG).show();
                // Utils.showSnackBar(error.getMessage(), view.findViewById(R.id.parent));
            }
        }
    };

    private void fetchleavelist(String classid) {
        if (loaded == 0) {
            commonProgress.show();
        }
        loaded++;
        //   ErpProgress.showProgressBar(this, "Please wait...");

        HashMap<String, String> params = new HashMap<>();
        if (!classid.equalsIgnoreCase("")){
            params.put("class_id", classid);
        } else {
            params.put("class_id", "");
        }
        params.put("view_type", "class");
        params.put("check", viewwwww);
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.HEALTH,apicallback, this, params);

    }

    ApiHandler.ApiCallback apicallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            commonProgress.dismiss();
            if (error == null) {
                try {
                    if (leavelist != null) {
                        leavelist.clear();
                    }
                    JSONArray jsonArray = jsonObject.getJSONArray("hw_data");
                    leavelist = HealthBean.parseHWArray(jsonArray);
                    if (leavelist.size() > 0) {
                        adapter.setHQList(leavelist);
                        totalRecord.setText("Total Entries:- "+String.valueOf(leavelist.size()));
                        adapter.notifyDataSetChanged();
                        txtNorecord.setVisibility(View.GONE);
                    } else {
                        totalRecord.setText("Total Entries:- 0");
                        adapter.setHQList(leavelist);
                        adapter.notifyDataSetChanged();
                        txtNorecord.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                totalRecord.setText("Total Entries:- 0");
                txtNorecord.setVisibility(View.VISIBLE);
                leavelist.clear();
                adapter.setHQList(leavelist);
                adapter.notifyDataSetChanged();
                //Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_LONG).show();
                Utils.showSnackBar(error.getMessage(), parent);
            }
        }
    };

    @Override
    public void StudentProfile(HealthBean std) {

    }

    @Override
    public void onUpdateCallback(HealthBean std) {

    }

    @Override
    public void onDelecallback(HealthBean homeworkbean) {
        commonProgress.show();
        HashMap<String, String> params = new HashMap<>();
        params.put("check", "health_act");
        params.put("h_id", homeworkbean.getId());
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.DELETEHW, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                ErpProgress.cancelProgressBar();
                commonProgress.dismiss();
                if (error == null) {
                    int pos = leavelist.indexOf(homeworkbean);
                    leavelist.remove(pos);
                    adapter.setHQList(leavelist);
                    adapter.notifyDataSetChanged();
                    Utils.showSnackBar("Deleted Successfully!", parent);
                    totalRecord.setText("Total Entries:- "+String.valueOf(leavelist.size()));
                } else {
                    Log.e("error", error.getMessage() + "");
                    Utils.showSnackBar(error.getMessage(), parent);
                    if (error.getStatusCode() == 405) {
                        User.logout();
                        startActivity(new Intent(View_Health.this, LoginActivity.class));
                        finish();
                    }
                }
            }
        }, this, params);
    }
}