package com.ultimate.ultimatesmartschool.Health_Module;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.daimajia.swipe.SimpleSwipeListener;
import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.squareup.picasso.Picasso;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Transport.FuelExpenseAdapter;
import com.ultimate.ultimatesmartschool.Transport.Fuelbean;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class HealthAdapter extends RecyclerSwipeAdapter<HealthAdapter.Viewholder> {
    public ArrayList<HealthBean> stulist;
    Context mContext;
    StudentPro mStudentPro;
    int value;

    public HealthAdapter(ArrayList<HealthBean> stulist, Context mContext, StudentPro mStudentPro,int value) {
        this.mContext = mContext;
        this.mStudentPro=mStudentPro;
        this.stulist = stulist;
        this.value = value;
    }

    @Override
    public Viewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_news_health, parent, false);
        Viewholder viewholder = new Viewholder(view);
        return viewholder;
    }

    @Override
    public void onBindViewHolder(final Viewholder viewHolder, @SuppressLint("RecyclerView") final int position) {

//        viewHolder.circleimg.setAnimation(AnimationUtils.loadAnimation(mContext,R.anim.fade_transition_animation));
//        viewHolder.parent.setAnimation(AnimationUtils.loadAnimation(mContext,R.anim.fade_scale_animation));
        HealthBean mData=stulist.get(position);

        if (stulist.get(position).getProfile() != null) {
            Picasso.get().load(stulist.get(position).getProfile()).placeholder(R.drawable.receptionistsss).into(viewHolder.circleimg);
        } else {
            Picasso.get().load(R.drawable.receptionistsss).into(viewHolder.circleimg);
        }

        //for Today
        Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
        Date setdate = calendar.getTime();
        SimpleDateFormat fmtOut = new SimpleDateFormat("dd MMM, yyyy");
        String dateString = fmtOut.format(setdate);
        //for Yesterday
        DateFormat dateFormat = new SimpleDateFormat("dd MMM, yyyy");
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);
        String dateString1= dateFormat.format(cal.getTime());


//        if (mData.getDate() != null) {
//            String title = getColoredSpanned("", "#000000");
//            String Name = getColoredSpanned(Utils.getDateTimeFormatedWithAMPM(mData.getDate()), "#5A5C59");
//            viewHolder.txtid1.setText(Html.fromHtml(title + " " + Name));
//        }

        String check= Utils.getDateFormated(stulist.get(position).getDate());
        if (check.equalsIgnoreCase(dateString)){
            //  holder.lytLine.setVisibility(View.VISIBLE);
            // holder.txtDT.setText("Today");
            String title = getColoredSpanned("Today", "#e31e25");
            //String Name = getColoredSpanned(messagelist.get(position).getFrom_type(), "#7D7D7D");
            String l_Name = getColoredSpanned("", "#5A5C59");
            viewHolder.txtid1.setText(Html.fromHtml(title + " " + l_Name));
            // holder.txtDT.setText("Today at "+time);
            //  holder.vieww.setBackground(mContext.getResources().getDrawable(R.drawable.a_card_lyt2));
        }else if (check.equalsIgnoreCase(dateString1)){
            String title = getColoredSpanned("Yesterday", "#1C8B3B");
            //String Name = getColoredSpanned(messagelist.get(position).getFrom_type(), "#7D7D7D");
            String l_Name = getColoredSpanned("", "#5A5C59");
            viewHolder.txtid1.setText(Html.fromHtml(title + " " + l_Name));
            //  holder.lytLine.setVisibility(View.GONE);
            //  holder.vieww.setBackground(mContext.getResources().getDrawable(R.drawable.card_lyt2));
        }else {
            viewHolder.txtid1.setText(Utils.getDateTimeFormatedWithAMPM(stulist.get(position).getDate()));
        }


        if (mData.getClass_name() != null) {
            String title = getColoredSpanned("Class & Sec: ", "#000000");
            String Name = getColoredSpanned(mData.getClass_name()+"-"+mData.getSection_name(), "#5A5C59");
            viewHolder.txtid.setText(Html.fromHtml(title + " " + Name));
        }

        // holder.employee_name.setText("Name: " + " " + viewstafflist.get(position).getName());
        if (mData.getEs_sname() != null) {
            String title = getColoredSpanned("Name: ", "#000000");
            String Name = getColoredSpanned(mData.getEs_sname()+"("+mData.getEs_regid()+")", "#5A5C59");
            viewHolder.textViewData.setText(Html.fromHtml(title + " " + Name));
        }else {
            viewHolder.textViewData.setText("Name: Not Mentioned");
        }

        //  holder.post.setText("Post:" + " " + viewstafflist.get(position).getPost());
        if (mData.getEs_fname() != null) {
            String title = getColoredSpanned("Father Name: ", "#000000");
            String Name = getColoredSpanned(mData.getEs_fname(), "#5A5C59");
            viewHolder.txtFather.setText(Html.fromHtml(title + " " + Name));
        }else {
            viewHolder.txtFather.setText("Not Mentioned");
        }

        if (value==1){
            viewHolder.evn_img1.setVisibility(View.GONE);
            // holder.employee_doj.setText("Date of Joining: " + " " + viewstafflist.get(position).getDateofjoining());
            if (mData.getEs_allergy() != null) {
                String title = getColoredSpanned("Health Components: ", "#000000");
                String Name = getColoredSpanned("<b>"+mData.getHealth()+"</b>", "#5A5C59");
                viewHolder.txtClass.setText(Html.fromHtml(title + " " + Name));
            } else {
                viewHolder.txtClass.setText("Not Mentioned");
            }

            if (mData.getEs_disease() != null) {
                String title = getColoredSpanned("Type of Vision: ", "#000000");
                String Name = getColoredSpanned("<b>"+mData.getVision()+"</b>", "#5A5C59");
                viewHolder.txtMobile.setText(Html.fromHtml(title + " " + Name));
            }else {
                viewHolder.txtMobile.setText("Not Mentioned");
            }

            if (mData.getEs_medicine() != null) {
                String title = getColoredSpanned("Teeth Occlusion: ", "#000000");
                String Name = getColoredSpanned("<b>"+mData.getTeeth()+"</b>", "#5A5C59");
                viewHolder.mor_img.setText(Html.fromHtml(title + " " + Name));
            }else {
                viewHolder.mor_img.setText("Not Mentioned");
            }

            if (mData.getEs_medicine_time() != null) {
                String title = getColoredSpanned("HPE: ", "#000000");
                String Name = getColoredSpanned("<b>"+mData.getHpe()+"</b>", "#5A5C59");
                viewHolder.txtMobile1.setText(Html.fromHtml(title + " " + Name));
            }else {
                viewHolder.txtMobile1.setText("Not Mentioned");
            }


            if (mData.getEs_medical_issue() != null) {
                String title = getColoredSpanned("Height: ", "#000000");
                String Name = getColoredSpanned("<b>"+mData.getHeight()+"</b>", "#5A5C59");
                viewHolder.evn_img.setText(Html.fromHtml(title + " " + Name));
            }else {
                viewHolder.evn_img.setText("Not Mentioned");
            }

            if (mData.getComment() != null) {
                String title = getColoredSpanned("Width: ", "#000000");
                String Name = getColoredSpanned("<b>"+mData.getWidth()+"</b>", "#5A5C59");
                viewHolder.evn_img2.setText(Html.fromHtml(title + " " + Name));
            }else {
                viewHolder.evn_img2.setText("Not Mentioned");
            }
        }else{
            viewHolder.evn_img1.setVisibility(View.VISIBLE);
            // holder.employee_doj.setText("Date of Joining: " + " " + viewstafflist.get(position).getDateofjoining());
            if (mData.getEs_allergy() != null) {
                String title = getColoredSpanned("Type of allergy: ", "#000000");
                String Name = getColoredSpanned("<b>"+mData.getEs_allergy()+"</b>", "#5A5C59");
                viewHolder.txtClass.setText(Html.fromHtml(title + " " + Name));
            } else {
                viewHolder.txtClass.setText("Not Mentioned");
            }

            if (mData.getEs_disease() != null) {
                String title = getColoredSpanned("Type of disease: ", "#000000");
                String Name = getColoredSpanned("<b>"+mData.getEs_disease()+"</b>", "#5A5C59");
                viewHolder.txtMobile.setText(Html.fromHtml(title + " " + Name));
            }else {
                viewHolder.txtMobile.setText("Not Mentioned");
            }

            if (mData.getEs_medicine() != null) {
                String title = getColoredSpanned("Medicine submitted in school: ", "#000000");
                String Name = getColoredSpanned("<b>"+mData.getEs_medicine()+"</b>", "#5A5C59");
                viewHolder.mor_img.setText(Html.fromHtml(title + " " + Name));
            }else {
                viewHolder.mor_img.setText("Not Mentioned");
            }

            if (mData.getEs_medicine_time() != null) {
                String title = getColoredSpanned("How many time take medicine: ", "#000000");
                String Name = getColoredSpanned("<b>"+mData.getEs_medicine_time()+"</b>", "#5A5C59");
                viewHolder.txtMobile1.setText(Html.fromHtml(title + " " + Name));
            }else {
                viewHolder.txtMobile1.setText("Not Mentioned");
            }

            String check1= Utils.getDateFormated(stulist.get(position).getEs_expirydate());
            if (check1.equalsIgnoreCase(dateString)){
                //  holder.lytLine.setVisibility(View.VISIBLE);
                // holder.txtDT.setText("Today");
                String title = getColoredSpanned("Today", "#e31e25");
                //String Name = getColoredSpanned(messagelist.get(position).getFrom_type(), "#7D7D7D");
                String l_Name = getColoredSpanned("Expire date of medicine: ", "#OOOOOO");
                viewHolder.evn_img.setText(Html.fromHtml(l_Name + " " + title));
                // holder.txtDT.setText("Today at "+time);
                //  holder.vieww.setBackground(mContext.getResources().getDrawable(R.drawable.a_card_lyt2));
            }else if (check1.equalsIgnoreCase(dateString1)){
                String title = getColoredSpanned("Yesterday", "#1C8B3B");
                //String Name = getColoredSpanned(messagelist.get(position).getFrom_type(), "#7D7D7D");
                String l_Name = getColoredSpanned("Expire date of medicine: ", "#OOOOOO");
                viewHolder.evn_img.setText(Html.fromHtml(l_Name + " " + title));
                //  holder.lytLine.setVisibility(View.GONE);
                //  holder.vieww.setBackground(mContext.getResources().getDrawable(R.drawable.card_lyt2));
            }else {
                String title = getColoredSpanned("Expire date of medicine: ", "#000000");
                String Name = getColoredSpanned("<b>"+Utils.getDateFormated(mData.getEs_expirydate()+"</b>"), "#5A5C59");
                viewHolder.evn_img.setText(Html.fromHtml(title + " " + Name));
            }


            if (mData.getEs_medical_issue() != null) {
                String title = getColoredSpanned("Is student take part in games: ", "#000000");
                String Name = getColoredSpanned("<b>"+mData.getEs_medical_issue()+"</b>", "#5A5C59");
                viewHolder.evn_img1.setText(Html.fromHtml(title + " " + Name));
            }else {
                viewHolder.evn_img1.setText("Not Mentioned");
            }

            if (mData.getComment() != null) {
                String title = getColoredSpanned("Remarks: ", "#000000");
                String Name = getColoredSpanned(mData.getComment(), "#5A5C59");
                viewHolder.evn_img2.setText(Html.fromHtml(title + " " + Name));
            }else {
                viewHolder.evn_img2.setText("Not Mentioned");
            }
        }

        Animation animation = AnimationUtils.loadAnimation(mContext, R.anim.btn_blink_animation);

        viewHolder.swipeLayout.setShowMode(SwipeLayout.ShowMode.PullOut);
        viewHolder.swipeLayout.addSwipeListener(new SimpleSwipeListener() {
            @Override
            public void onOpen(SwipeLayout layout) {

            }

            @Override
            public void onStartClose(SwipeLayout layout) {
                viewHolder.swipeLayout.close();
            }
        });


        viewHolder.trash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder1 = new AlertDialog.Builder(mContext);
                builder1.setMessage("Do you want to delete? ");
                builder1.setCancelable(false);
                builder1.setPositiveButton(
                        "Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                                if (mStudentPro != null) {
                                    mStudentPro.onDelecallback(stulist.get(position));
//                                    Toast.makeText(listner, "deleted", Toast.LENGTH_SHORT).show();
                                }
                                // mAdapterCallback.deleteNotification(notificationList.get(postion));
                            }
                        });
                builder1.setNegativeButton(
                        "No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        });
                AlertDialog alert11 = builder1.create();
                alert11.show();
            }
        });

    }

    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }

    public interface StudentPro{
        public void StudentProfile(HealthBean std);
        public void onUpdateCallback(HealthBean std);

        public void onDelecallback(HealthBean std);

    }
    @Override
    public int getItemCount() {
        if (stulist != null)
            return stulist.size();
        return 0;
    }



    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe;
    }

    public void setHQList(ArrayList<HealthBean> stulist) {
        this.stulist = stulist;
    }

    public class Viewholder extends RecyclerView.ViewHolder {
        //        SwipeLayout swipeLayout;
        TextView textViewData, txtMobile,evn_img,mor_img,txtMobile1, txtFather, txtClass,txtid,txtid1,status,evn_img1,evn_img2;
        ImageView trash,imgOfDialog,update;
        // CardView update;
        ImageView arrow;
        RelativeLayout parent,root;
        CircularImageView circleimg;
        SwipeLayout swipeLayout;

        public Viewholder(View itemView) {
            super(itemView);
            root =(RelativeLayout)itemView.findViewById(R.id.root);
            parent =(RelativeLayout)itemView.findViewById(R.id.parent);
            circleimg=(CircularImageView)itemView.findViewById(R.id.circleimg);
            swipeLayout = (SwipeLayout) itemView.findViewById(R.id.swipe);
            textViewData = (TextView) itemView.findViewById(R.id.name);
            txtFather = (TextView) itemView.findViewById(R.id.fathername);
            txtMobile = (TextView) itemView.findViewById(R.id.mobileno);
            txtMobile1 = (TextView) itemView.findViewById(R.id.mobileno1);
            mor_img = (TextView) itemView.findViewById(R.id.mor_img);
            evn_img = (TextView) itemView.findViewById(R.id.evn_img);
            evn_img1 = (TextView) itemView.findViewById(R.id.evn_img1);
            evn_img2 = (TextView) itemView.findViewById(R.id.evn_img2);
            txtClass = (TextView) itemView.findViewById(R.id.classess);
            status = (TextView) itemView.findViewById(R.id.status);
            update = (ImageView) itemView.findViewById(R.id.update);
            trash = (ImageView) itemView.findViewById(R.id.trash);
            imgOfDialog = (ImageView) itemView.findViewById(R.id.imgOfDialog);
            txtid=(TextView)itemView.findViewById(R.id.textViewid);
            txtid1=(TextView)itemView.findViewById(R.id.textViewid1);
            arrow = (ImageView) itemView.findViewById(R.id.arrow);
            arrow.setVisibility(View.GONE);
        }
    }
}
