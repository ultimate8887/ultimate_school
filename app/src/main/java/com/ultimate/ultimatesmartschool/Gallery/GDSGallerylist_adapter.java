package com.ultimate.ultimatesmartschool.Gallery;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import java.util.ArrayList;

import me.relex.circleindicator.CircleIndicator;

public class GDSGallerylist_adapter extends RecyclerView.Adapter<GDSGallerylist_adapter.Viewholder>{
    private final Context context;
    ArrayList<GalleryPhoto_bean> eventList;
    Eventcallback mEventcallback;

    public GDSGallerylist_adapter(ArrayList<GalleryPhoto_bean> eventList, Context context, Eventcallback mEventcallback) {
        this.context=context;
        this.eventList=eventList;
        this.mEventcallback=mEventcallback;
    }

    @NonNull
    @Override
    public GDSGallerylist_adapter.Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.event_adapt_lay, parent, false);
        GDSGallerylist_adapter.Viewholder viewholder = new GDSGallerylist_adapter.Viewholder(view);
        return viewholder;
    }

    @Override
    public void onBindViewHolder(@NonNull final GDSGallerylist_adapter.Viewholder holder, @SuppressLint("RecyclerView") final int position) {
        holder.textTittle.setText(eventList.get(position).getTitle());

        holder.eventcordinate.setVisibility(View.GONE);

        holder.textdescription.setVisibility(View.GONE);
        holder.lastdateprticipation.setVisibility(View.GONE);
        holder.fromdate.setText("Created On:"+ Utils.getDateFormated(eventList.get(position).getFromdate()));

        ViewPagerAdapters mAdapter = new  ViewPagerAdapters(context,eventList.get(position).getImage(),eventList.get(position).getMsg());
        holder.intro_images.setAdapter(mAdapter);
        holder.intro_images.setCurrentItem(0);
        holder.indicator.setViewPager(holder.intro_images);
        holder.updateevent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mEventcallback.onUpdateCallback(eventList.get(position));
            }
        });

        holder.deleteevent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
                builder1.setMessage("Do you want to delete? ");
                builder1.setCancelable(false);
                builder1.setPositiveButton(
                        "Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                                if (mEventcallback != null) {

                                    mEventcallback.onDelecallback(eventList.get(position));
                                }
                            }
                        });
                builder1.setNegativeButton(
                        "No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        });
                AlertDialog alert11 = builder1.create();
                alert11.show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return eventList.size();
    }

    public void setEventList(ArrayList<GalleryPhoto_bean> eventList) {
        this.eventList = eventList;
    }

    public class Viewholder extends RecyclerView.ViewHolder {
        public TextView textTittle,textdescription,fromdate,todate,lastdateprticipation,eventcordinate;
        public ViewPager intro_images;
        LinearLayout pager_indicator;
        public CircleIndicator indicator;
        public ImageView updateevent,deleteevent;

        public Viewholder(View itemView) {
            super(itemView);
            indicator = itemView.findViewById(R.id.indicator);
            textTittle= itemView.findViewById(R.id.eventtitle);
            textdescription=itemView.findViewById(R.id.eventdescription);
            intro_images= itemView.findViewById(R.id.pager_introduction);
            updateevent=itemView.findViewById(R.id.update);
            deleteevent=itemView.findViewById(R.id.trash);
            fromdate= itemView.findViewById(R.id.fromdatetext);
            todate=itemView.findViewById(R.id.todatetext);
            lastdateprticipation=itemView.findViewById(R.id.lastdateprticipation);
            eventcordinate=itemView.findViewById(R.id.eventcordinate);
        }
    }



    public interface Eventcallback {
        public void onUpdateCallback(GalleryPhoto_bean eventlist_bean);

        public void onDelecallback(GalleryPhoto_bean eventlist_bean);

        void DisplayImage(final GalleryPhoto_bean obj);
    }
}
