package com.ultimate.ultimatesmartschool.Gallery;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.viewpager.widget.PagerAdapter;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

public class ViewPagerAdapters extends PagerAdapter {

    private final Context context;
    private final ArrayList<String> image;
    String msg;
    public ViewPagerAdapters(Context context, ArrayList<String> image, String msg) {
        this.context=context;
        this.image=image;
        this.msg=msg;
    }

    @Override
    public int getCount() {
        int size = 0;
        if(image!= null){
            size = image.size();
        }
        if(msg!= null){
            size= size+1;
        }
        return size;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }
    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.pager_itemsec, container, false);

        ImageView imageView = itemView.findViewById(R.id.img_pager_item);
        TextView txtText= (TextView)itemView.findViewById(R.id.clsswrktexttt);

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                ErpProgress.showProgressBar(context, "downloading...");
//                Picasso.with(context).load(image.get(position)).into(imgTraget);
//                Toast.makeText(context, "Downloaded successfully", Toast.LENGTH_LONG).show();


            }
        });
        if(msg!= null){
            if(position==0){
                txtText.setVisibility(View.VISIBLE);
                txtText.setText(msg);
                txtText.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        openDialog(msg);
                    }
                });
            }else{
                Picasso.get().load(image.get(position-1)).into(imageView);
            }
        }else{
            Picasso.get().load(image.get(position)).into(imageView);
        }

        container.addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((RelativeLayout) object);
    }

    private void openDialog(String msg) {
        final Dialog logoutDialog = new Dialog(context);
        logoutDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        logoutDialog.setCancelable(true);
        logoutDialog.setContentView(R.layout.msg_lyt);
        logoutDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        TextView txt2 = (TextView) logoutDialog.findViewById(R.id.txt2);
        txt2.setText(msg);
        logoutDialog.show();
    }


}
