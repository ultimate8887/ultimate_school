package com.ultimate.ultimatesmartschool.Gallery.NewGallery;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.google.gson.Gson;
import com.ultimate.ultimatesmartschool.BeanModule.User;
import com.ultimate.ultimatesmartschool.Gallery.GDSGallerylist_adapter;
import com.ultimate.ultimatesmartschool.Gallery.GalleryPhoto_bean;
import com.ultimate.ultimatesmartschool.Gallery.ViewGallery;
import com.ultimate.ultimatesmartschool.Login.LoginActivity;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.CommonProgress;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ViewGalleryGrid extends AppCompatActivity implements GDSGallerylist_adapter_New.Eventcallback {

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.imgBack)
    ImageView back;
    @BindView(R.id.parent)
    RelativeLayout parent;
    @BindView(R.id.textNorecord)TextView txtNorecord;
    private GDSGallerylist_adapter_New adapter;
    ArrayList<GalleryPhoto_bean> eventList = new ArrayList<>();
    private LinearLayoutManager layoutManager;
    @BindView(R.id.totalRecord)
    TextView totalRecord;
    CommonProgress commonProgress;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_gallery);
        ButterKnife.bind(this);
        commonProgress=new CommonProgress(this);
        recyclerView.setNestedScrollingEnabled(false);
        txtTitle.setText("Photo Gallery");
        recyclerView.setLayoutManager(new GridLayoutManager(this,2));
        adapter = new GDSGallerylist_adapter_New(eventList, ViewGalleryGrid.this,this);
        recyclerView.setAdapter(adapter);
        fetchEventlist();
    }

    private void fetchEventlist() {
        commonProgress.show();
        HashMap<String, String> params = new HashMap<String, String>();
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.GDSGALLERYLIST_URL, apiCallback, this, params);

    }
    ApiHandler.ApiCallback apiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            commonProgress.dismiss();
            if (error == null) {
                try {
                    if (eventList != null) {
                        eventList.clear();
                    }
                    JSONArray jsonArray = jsonObject.getJSONArray("event_data");
                    eventList = GalleryPhoto_bean.parseEVENTArray(jsonArray);
                    if (eventList.size() > 0) {
                        adapter.setEventList(eventList);
                        adapter.notifyDataSetChanged();
                        txtNorecord.setVisibility(View.GONE);
                        totalRecord.setText("Total Entries:- "+String.valueOf(eventList.size()));
                    } else {
                        totalRecord.setText("Total Entries:- 0");
                        adapter.setEventList(eventList);
                        adapter.notifyDataSetChanged();
                        txtNorecord.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                txtNorecord.setVisibility(View.VISIBLE);
                eventList.clear();
                adapter.setEventList(eventList);
                totalRecord.setText("Total Entries:- 0");
                adapter.notifyDataSetChanged();
            }
        }
    };
    @OnClick(R.id.imgBack)
    public void onback() {
        finish();
    }

    @Override
    public void onDelecallback(final GalleryPhoto_bean eventlist_bean) {
        commonProgress.show();
        HashMap<String, String> params = new HashMap<>();
        params.put("event_id", eventlist_bean.getEvent_id());
        params.put("check", "image");
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.DELETEGALARY_URL, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                commonProgress.dismiss();
                if (error == null) {
                    int pos = eventList.indexOf(eventlist_bean);
                    eventList.remove(pos);
                    adapter.setEventList(eventList);
                    adapter.notifyDataSetChanged();
                    totalRecord.setText("Total Entries:- "+String.valueOf(eventList.size()));
                    Toast.makeText(getApplicationContext(),"Deleted Successfully",Toast.LENGTH_SHORT).show();
                } else {
                    Log.e("error", error.getMessage() + "");
                    Utils.showSnackBar(error.getMessage(), parent);
                    Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_SHORT).show();
                    if (error.getStatusCode() == 405) {
                        User.logout();
                        startActivity(new Intent(ViewGalleryGrid.this, LoginActivity.class));
                        finish();
                    }
                }
            }
        }, this, params);
    }

    @Override
    public void DisplayImage(GalleryPhoto_bean data) {
        Gson gson = new Gson();
        String classworkdata = gson.toJson(data, GalleryPhoto_bean.class);
        Intent intent = new Intent(ViewGalleryGrid.this, IMGGridView.class);
        intent.putExtra("data", classworkdata);
        startActivity(intent);
    }

}