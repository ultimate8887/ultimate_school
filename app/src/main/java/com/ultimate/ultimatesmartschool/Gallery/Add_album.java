package com.ultimate.ultimatesmartschool.Gallery;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.ultimate.ultimatesmartschool.BeanModule.User;
import com.ultimate.ultimatesmartschool.Login.LoginActivity;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.CommonProgress;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class Add_album extends AppCompatActivity {
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.textNorecord)
    TextView textNorecord;
    @BindView(R.id.text)
    TextView totalRecord;
    @BindView(R.id.edtalbumnm)
    EditText albumname;
    @BindView(R.id.parent)
    RelativeLayout parent;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    private AlbmlistAdapter adapter;
    ArrayList<Albumlistbean> hwList = new ArrayList<>();
    private LinearLayoutManager layoutManager;
    CommonProgress commonProgress;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_album);
        ButterKnife.bind(this);
        commonProgress=new CommonProgress(this);
        txtTitle.setText("Add Album");
        layoutManager=new LinearLayoutManager(Add_album.this);
        recyclerView.setLayoutManager(layoutManager);
        adapter=new AlbmlistAdapter(hwList,Add_album.this);
        recyclerView.setAdapter(adapter);
    }

    @Override
    protected void onResume() {
        super.onResume();

        fetchalbumlist();
    }

    public void fetchalbumlist(){
        albumname.setText("");
        commonProgress.show();
        HashMap<String, String> params = new HashMap<String, String>();
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.ALBUMLIST_URL, albmapiCallback, this, params);

    }

    ApiHandler.ApiCallback albmapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            commonProgress.dismiss();
            if (error == null) {
                try {
                    if (hwList != null) {
                        hwList.clear();
                    }
                    JSONArray jsonArray = jsonObject.getJSONArray("admin_data");
                    hwList = Albumlistbean.parseALBMlistarray(jsonArray);
                    if (hwList.size() > 0) {
                        adapter.setalbmList(hwList);
                        adapter.notifyDataSetChanged();
                        textNorecord.setVisibility(View.GONE);
                        totalRecord.setText("Total Entries:- "+String.valueOf(hwList.size()));
                    } else {
                        adapter.setalbmList(hwList);
                        adapter.notifyDataSetChanged();
                        textNorecord.setVisibility(View.VISIBLE);
                        totalRecord.setText("Total Entries:- 0");

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                textNorecord.setVisibility(View.VISIBLE);
                hwList.clear();
                adapter.setalbmList(hwList);
                totalRecord.setText("Total Entries:- 0");
                adapter.notifyDataSetChanged();
            }
        }
    };



    @OnClick(R.id.add_album)public void addalbum(){
        if (checkValid()) {
            commonProgress.show();
            HashMap<String, String> params = new HashMap<String, String>();
            params.put("albm_title", albumname.getText().toString());
            ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.ADDALBUM_URL, apiCallback, this, params);
        }

    }
    ApiHandler.ApiCallback apiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            commonProgress.dismiss();
            if (error == null) {
                try {
                    Utils.showSnackBar(jsonObject.getString("msg"), parent);
                    fetchalbumlist();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("error", error.getMessage() + "");
                Utils.showSnackBar(error.getMessage(), parent);
                if (error.getStatusCode() == 405) {
                    User.logout();
                    startActivity(new Intent(Add_album.this, LoginActivity.class));
                    finish();
                }
            }
        }
    };
    private boolean checkValid() {
        boolean valid = true;
        String errorMsg = null;
        if (albumname.getText().toString().trim().length() <= 0) {
            valid = false;
            errorMsg = "Please enter Comment";
        }
        if (!valid) {
            Utils.showSnackBar(errorMsg, parent);
        }
        return valid;
    }

    @OnClick(R.id.imgBack)
    public void onback() {
        finish();
    }
}
