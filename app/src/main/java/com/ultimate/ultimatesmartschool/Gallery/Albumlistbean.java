package com.ultimate.ultimatesmartschool.Gallery;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Albumlistbean {

    private static String ALBUM_ID = "album_id";
    private static String PHOTO_ID = "photo_id";
    private static String ALBUM_NAME = "album_name";
    private static String CREATED_Date = "created_date";


    /**
     * album_id : 6
     * album_name : Happy
     * created_date : 2019-01-10
     */

    private String album_id;
    private String album_name;
    private String created_date;
    /**
     * photo_id : 0
     */

    private String photo_id;

    public String getAlbum_id() {
        return album_id;
    }

    public void setAlbum_id(String album_id) {
        this.album_id = album_id;
    }

    public String getAlbum_name() {
        return album_name;
    }

    public void setAlbum_name(String album_name) {
        this.album_name = album_name;
    }

    public String getCreated_date() {
        return created_date;
    }

    public void setCreated_date(String created_date) {
        this.created_date = created_date;
    }

    public static ArrayList<Albumlistbean> parseALBMlistarray(JSONArray jsonArray) {
        ArrayList<Albumlistbean> list = new ArrayList<Albumlistbean>();
        try {

            for (int i = 0; i < jsonArray.length(); i++) {
                Albumlistbean p =  parseALBMlistobject(jsonArray.getJSONObject(i));
                if (p != null) {
                    list.add(p);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return list;
    }

    private static Albumlistbean parseALBMlistobject(JSONObject jsonObject) {
        Albumlistbean adminlistbean=new Albumlistbean();

        try {
            if (jsonObject.has(ALBUM_ID)) {
                adminlistbean.setAlbum_id(jsonObject.getString(ALBUM_ID));
            }
            if (jsonObject.has(ALBUM_NAME) && !jsonObject.getString(ALBUM_NAME).isEmpty() && !jsonObject.getString(ALBUM_NAME).equalsIgnoreCase("null")) {
                adminlistbean.setAlbum_name(jsonObject.getString(ALBUM_NAME));
            }
            if (jsonObject.has(CREATED_Date) && !jsonObject.getString(CREATED_Date).isEmpty() && !jsonObject.getString(CREATED_Date).equalsIgnoreCase("null")) {
                adminlistbean.setCreated_date(jsonObject.getString(CREATED_Date));
            }
            if (jsonObject.has(PHOTO_ID) && !jsonObject.getString(PHOTO_ID).isEmpty() && !jsonObject.getString(PHOTO_ID).equalsIgnoreCase("null")) {
                adminlistbean.setPhoto_id(jsonObject.getString(PHOTO_ID));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return adminlistbean;
    }

    public String getPhoto_id() {
        return photo_id;
    }

    public void setPhoto_id(String photo_id) {
        this.photo_id = photo_id;
    }
}
