package com.ultimate.ultimatesmartschool.Gallery;

import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.DebugLog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class GalleryPhoto_bean {

    private static String ID="event_id";
    private static String IMAGE="image";
    private static String TITLE="title";
    private static String DESCRIPTION="description";
    private static String FROMDATE="fromdate";
    private static String TODATE="todate";
    private static String MSG="msg";
    private static String LATDATE="lastdate";
    private String type;
    private String s_file;
    private String created_on;
    private static String ETITLE="event_name";


    public String getCreated_on() {
        return created_on;
    }

    public void setCreated_on(String created_on) {
        this.created_on = created_on;
    }

    public String getS_file() {
        return s_file;
    }

    public void setS_file(String s_file) {
        this.s_file = s_file;
    }

    /**
     * title : Happy
     * description :  Happy birthday..
     * image : ["office_admin/images/evnt_01042019_0901380.jpg ","office_admin/images/evnt_01042019_0901381.jpg ","office_admin/images/evnt_01042019_0901382.jpg ","office_admin/images/evnt_01042019_0901383.jpg"]
     * fromdate : 2019-01-04
     * todate : 2019-01-10
     */

    public String getEvent_name() {
        return event_name;
    }

    public void setEvent_name(String event_name) {
        this.event_name = event_name;
    }

    private String event_name;

    private String title;
    private String description;
    private String fromdate;
    private String todate;
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
    public String getEs_coordinator() {
        return es_coordinator;
    }

    public void setEs_coordinator(String es_coordinator) {
        this.es_coordinator = es_coordinator;
    }

    private String es_coordinator;
    private ArrayList<String> image;

    private String msg;
    /**
     * event_id : 5
     */

    private String event_id;
    /**
     * lastdate : 2019-01-08
     */

    private String lastdate;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFromdate() {
        return fromdate;
    }

    public void setFromdate(String fromdate) {
        this.fromdate = fromdate;
    }

    public String getTodate() {
        return todate;
    }

    public void setTodate(String todate) {
        this.todate = todate;
    }

    public ArrayList<String> getImage() {
        return image;
    }

    public void setImage(ArrayList<String> image) {
        this.image = image;
    }
    public static ArrayList<GalleryPhoto_bean> parseEVENTArray(JSONArray arrayObj) {
        ArrayList<GalleryPhoto_bean> list = new ArrayList<GalleryPhoto_bean>();
        try {

            for (int i = 0; i < arrayObj.length(); i++) {
                GalleryPhoto_bean p = parseEVENTObject(arrayObj.getJSONObject(i));
                if (p != null) {
                    list.add(p);
                }
            }
        } catch (Exception e) {
            DebugLog.printLog("exp",e.getMessage());
            e.printStackTrace();
        }
        return list;
    }

    public static GalleryPhoto_bean parseEVENTObject(JSONObject jsonObject) {
        GalleryPhoto_bean casteObj = new GalleryPhoto_bean();
        try {
            if (jsonObject.has(ID)) {
                casteObj.setEvent_id(jsonObject.getString(ID));
            }
            if (jsonObject.has(IMAGE) && !jsonObject.getString(IMAGE).isEmpty() && !jsonObject.getString(IMAGE).equalsIgnoreCase("null")) {
                JSONArray img_arr = jsonObject.getJSONArray(IMAGE);
                ArrayList<String> img_arrs = new ArrayList<>();
                for(int i=0;i<img_arr.length();i++){
                    img_arrs.add(Constants.getImageBaseURL()+img_arr.getString(i));
                }
                casteObj.setImage(img_arrs);
            }

            if (jsonObject.has(TITLE) && !jsonObject.getString(TITLE).isEmpty() && !jsonObject.getString(TITLE).equalsIgnoreCase("null")) {
                casteObj.setTitle(jsonObject.getString(TITLE));
            }
            if (jsonObject.has(DESCRIPTION) && !jsonObject.getString(DESCRIPTION).isEmpty() && !jsonObject.getString(DESCRIPTION).equalsIgnoreCase("null")) {
                casteObj.setDescription(jsonObject.getString(DESCRIPTION));
            }
            if (jsonObject.has(FROMDATE) && !jsonObject.getString(FROMDATE).isEmpty() && !jsonObject.getString(FROMDATE).equalsIgnoreCase("null")) {
                casteObj.setFromdate(jsonObject.getString(FROMDATE));
            }

            if (jsonObject.has(ETITLE) && !jsonObject.getString(ETITLE).isEmpty() && !jsonObject.getString(ETITLE).equalsIgnoreCase("null")) {
                casteObj.setEvent_name(jsonObject.getString(ETITLE));
            }

            if (jsonObject.has(TODATE) && !jsonObject.getString(TODATE).isEmpty() && !jsonObject.getString(TODATE).equalsIgnoreCase("null")) {
                casteObj.setTodate(jsonObject.getString(TODATE));
            } if (jsonObject.has(MSG) && !jsonObject.getString(MSG).isEmpty() && !jsonObject.getString(MSG).equalsIgnoreCase("null")) {
                casteObj.setMsg(jsonObject.getString(MSG));
            }
            if (jsonObject.has(LATDATE) && !jsonObject.getString(LATDATE).isEmpty() && !jsonObject.getString(LATDATE).equalsIgnoreCase("null")) {
                casteObj.setLastdate(jsonObject.getString(LATDATE));
            }
            if (jsonObject.has("type") && !jsonObject.getString("type").isEmpty() && !jsonObject.getString("type").equalsIgnoreCase("null")) {
                casteObj.setType(jsonObject.getString("type"));
            }
            if (jsonObject.has("s_file") && !jsonObject.getString("s_file").isEmpty() && !jsonObject.getString("s_file").equalsIgnoreCase("null")) {
                casteObj.setS_file(jsonObject.getString("s_file"));
            }
            if (jsonObject.has("created_on") && !jsonObject.getString("created_on").isEmpty() && !jsonObject.getString("created_on").equalsIgnoreCase("null")) {
                casteObj.setCreated_on(jsonObject.getString("created_on"));
            }
            if(jsonObject.has(""));


        } catch (JSONException e) {
            e.printStackTrace();
        }
        return casteObj;
    }


    public String getEvent_id() {
        return event_id;
    }

    public void setEvent_id(String event_id) {
        this.event_id = event_id;
    }

    public String getLastdate() {
        return lastdate;
    }

    public void setLastdate(String lastdate) {
        this.lastdate = lastdate;
    }

}
