package com.ultimate.ultimatesmartschool.Gallery;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import java.util.ArrayList;

public class AlbmlistAdapter extends RecyclerSwipeAdapter<AlbmlistAdapter.Viewholder> {

    ArrayList<Albumlistbean> hq_list;

    Context listner;

    public AlbmlistAdapter(ArrayList<Albumlistbean> hq_list, Context listner) {
        this.hq_list = hq_list;
        this.listner = listner;
    }


    @Override
    public Viewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.albm_ada_lay_new, parent, false);
        AlbmlistAdapter.Viewholder viewholder = new AlbmlistAdapter.Viewholder(view);
        return viewholder;
    }

    @Override
    public void onBindViewHolder(final AlbmlistAdapter.Viewholder holder, final int position) {

        holder.albm_name.setEnabled(false);
        if (hq_list.get(position).getAlbum_id() != null) {
            String title = getColoredSpanned("Album ID: ", "#000000");
            String Name = getColoredSpanned(hq_list.get(position).getAlbum_id(), "#5A5C59");
            holder.albm_id.setText(Html.fromHtml(title + " " + Name));
        }

      //  holder.albm_id.setText("Album Id: "+hq_list.get(position).getAlbum_id());
        holder.albm_name.setText(hq_list.get(position).getAlbum_name());
        holder.createdon.setText("Created-On: "+ Utils.getDateFormated(hq_list.get(position).getCreated_date()));



    }

    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }

    @Override
    public int getItemCount() {
        return hq_list.size();
    }


    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe;
    }

    public void setalbmList(ArrayList<Albumlistbean> hq_list) {
        this.hq_list = hq_list;
    }

    public class Viewholder extends RecyclerView.ViewHolder {
        public TextView albm_id;
        public TextView albm_name;
        public TextView createdon;
        public ImageView iv2, trash, update;
        SwipeLayout swipeLayout;

        public Viewholder(View itemView) {
            super(itemView);

            albm_id = itemView.findViewById(R.id.albm_id);
            albm_name = itemView.findViewById(R.id.albm_name);
            createdon = itemView.findViewById(R.id.status);
//            iv2 = itemView.findViewById(R.id.imagefoto);
//            trash = (ImageView) itemView.findViewById(R.id.trash);
//            update = (ImageView) itemView.findViewById(R.id.update);
        }
    }
}
