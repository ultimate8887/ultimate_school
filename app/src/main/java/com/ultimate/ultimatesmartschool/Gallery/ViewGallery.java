package com.ultimate.ultimatesmartschool.Gallery;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.ultimate.ultimatesmartschool.BeanModule.User;
import com.ultimate.ultimatesmartschool.Login.LoginActivity;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ViewGallery extends AppCompatActivity implements GDSGallerylist_adapter.Eventcallback{
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.imgBack)
    ImageView back;
    @BindView(R.id.parent)
    RelativeLayout parent;
    @BindView(R.id.textNorecord)TextView txtNorecord;
    private GDSGallerylist_adapter adapter;
    ArrayList<GalleryPhoto_bean> eventList = new ArrayList<>();
    private LinearLayoutManager layoutManager;
    @BindView(R.id.totalRecord)
    TextView totalRecord;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_gallery);
        ButterKnife.bind(this);
        recyclerView.setNestedScrollingEnabled(false);
        txtTitle.setText("Gallery");
        layoutManager = new LinearLayoutManager(ViewGallery.this);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new GDSGallerylist_adapter(eventList, ViewGallery.this,this);
        recyclerView.setAdapter(adapter);
    }

    private void fetchEventlist() {
        ErpProgress.showProgressBar(this,"Please wait...");
        HashMap<String, String> params = new HashMap<String, String>();
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.GDSGALLERYLIST_URL, apiCallback, this, params);

    }
    ApiHandler.ApiCallback apiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();

            if (error == null) {
                try {
                    if (eventList != null) {
                        eventList.clear();
                    }
                    JSONArray jsonArray = jsonObject.getJSONArray("event_data");
                    eventList = GalleryPhoto_bean.parseEVENTArray(jsonArray);
                    if (eventList.size() > 0) {
                        adapter.setEventList(eventList);
                        adapter.notifyDataSetChanged();
                        txtNorecord.setVisibility(View.GONE);
                        totalRecord.setText("Total Entries:- "+String.valueOf(eventList.size()));
                    } else {
                        totalRecord.setText("Total Entries:- 0");
                        adapter.setEventList(eventList);
                        adapter.notifyDataSetChanged();
                        txtNorecord.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                txtNorecord.setVisibility(View.VISIBLE);
                eventList.clear();
                adapter.setEventList(eventList);
                totalRecord.setText("Total Entries:- 0");
                adapter.notifyDataSetChanged();
            }
        }
    };
    @OnClick(R.id.imgBack)
    public void onback() {
        finish();
    }
    @Override
    protected void onResume() {
        super.onResume();
        fetchEventlist();
    }
    @Override
    public void onUpdateCallback(GalleryPhoto_bean eventlist_bean) {

//        Gson gson = new Gson();
//        String eventdata = gson.toJson(eventlist_bean, Eventlist_bean.class);
//        Intent intent = new Intent(GDSGalleryAct.this,UpdateEventActivity.class);
//        intent.putExtra("evntdata", eventdata);
//        startActivity(intent);

    }

    @Override
    public void onDelecallback(final GalleryPhoto_bean eventlist_bean) {

        HashMap<String, String> params = new HashMap<>();
        params.put("event_id", eventlist_bean.getEvent_id());
        params.put("check", "image");
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.DELETEGALARY_URL, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                ErpProgress.cancelProgressBar();
                if (error == null) {
                    Toast.makeText(getApplicationContext(),"Deleted Successfully",Toast.LENGTH_SHORT).show();
                    int pos = eventList.indexOf(eventlist_bean);
                    eventList.remove(pos);
                    adapter.setEventList(eventList);
                    adapter.notifyDataSetChanged();
                    totalRecord.setText("Total Entries:- "+String.valueOf(eventList.size()));
                } else {
                    Log.e("error", error.getMessage() + "");
                    Utils.showSnackBar(error.getMessage(), parent);
                    Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_SHORT).show();
                    if (error.getStatusCode() == 405) {
                        User.logout();
                        startActivity(new Intent(ViewGallery.this, LoginActivity.class));
                        finish();
                    }
                }
            }
        }, this, params);
    }

    @Override
    public void DisplayImage(GalleryPhoto_bean obj) {

    }
}
