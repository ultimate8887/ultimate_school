package com.ultimate.ultimatesmartschool.Gallery;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.ClipData;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.bumptech.glide.Glide;
import com.kroegerama.imgpicker.BottomSheetImagePicker;
import com.ultimate.ultimatesmartschool.BeanModule.ClassBean;
import com.ultimate.ultimatesmartschool.BeanModule.SectionBean;
import com.ultimate.ultimatesmartschool.BeanModule.User;
import com.ultimate.ultimatesmartschool.Homework.SectionnewAdapter;
import com.ultimate.ultimatesmartschool.Login.LoginActivity;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.StudentAttendance.ClassAdapterAttend;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.CommonProgress;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class Add_photo extends AppCompatActivity implements BottomSheetImagePicker.OnImagesSelectedListener{
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.edtphotonm)
    EditText photoname;

    int PICK_IMAGE_MULTIPLE = 1;
    String imageEncoded;
    List<String> imagesEncodedList;

    @BindView(R.id.img)
    ImageView img;
    @BindView(R.id.img1)
    ImageView img1;
    @BindView(R.id.img2)
    ImageView img2;

    @BindView(R.id.text_s)
    TextView text_s;
    @BindView(R.id.text1_s)
    TextView text1_s;
    @BindView(R.id.text2_s)
    TextView text2_s;


    ArrayList<ClassBean> classList = new ArrayList<>();
    @BindView(R.id.spinnerClass)
    Spinner spinnerClass;
    String classid = "";

    @BindView(R.id.spinnersection)
    Spinner spinnersection;
    ArrayList<SectionBean> sectionList = new ArrayList<>();
    String sectionid="";
    String sectionname="";


    String gender="";
    String[] genderList = {
            "select gender","male",
            "female",
    };

    @BindView(R.id.spinnerGender)
    Spinner spinnerGender;

    @BindView(R.id.hDay)
    RelativeLayout hDay;
    @BindView(R.id.oneDay)
    RelativeLayout oneDay;
    @BindView(R.id.multiDay)
    RelativeLayout multiDay;
    String apply="all";
    @BindView(R.id.leaveradio)
    RadioGroup radioGroup;
    @BindView(R.id.radioone)
    RadioButton radioone;
    @BindView(R.id.radiomulti)
    RadioButton radiomulti;

    @BindView(R.id.gender_lyt)
    LinearLayout gender_lyt;
    @BindView(R.id.class_lyt)
    LinearLayout class_lyt;

    @BindView(R.id.parent)
    RelativeLayout parent;
    @BindView(R.id.spinnerselectalbm)
    Spinner spinerAlbum;
    ArrayList<Albumlistbean> albmlist = new ArrayList<>();
    SpinAlbumlistadapter adapter;
    String album_id;
    String album_name;
    private ViewGroup imageContainer;
    ArrayList<String> image;
    private Bitmap hwbitmap;
    String encoded;
    CommonProgress commonProgress;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_photo);
        ButterKnife.bind(this);
        commonProgress=new CommonProgress(this);
      //  parent.setAnimation(AnimationUtils.loadAnimation(getApplicationContext(),R.anim.fade_scale_animation));
        imageContainer = findViewById(R.id.imageContainer);
        txtTitle.setText("Add Photo");
        fetchalbumlist();
    }

    @OnClick(R.id.imgBack)
    public void onback() {
        finish();
    }

    public void fetchalbumlist(){
        commonProgress.show();
        HashMap<String, String> params = new HashMap<String, String>();
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.ALBUMLIST_URL, albmapiCallback, this, params);

    }

    ApiHandler.ApiCallback albmapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            commonProgress.dismiss();
            if (error == null) {
                try {
                    albmlist = Albumlistbean.parseALBMlistarray(jsonObject.getJSONArray("admin_data"));
                    adapter = new SpinAlbumlistadapter(Add_photo.this, albmlist);
                    spinerAlbum.setAdapter(adapter);
                    spinerAlbum.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if (i > 0) {
                                album_id = albmlist.get(i-1).getAlbum_id();
                                album_name=albmlist.get(i-1).getAlbum_name();
                                Log.e("classsid",album_id);

                            }else{
                                album_id="";
                            }

                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Utils.showSnackBar(error.getMessage(), parent);
            }
        }
    };


    @OnClick(R.id.hDay)
    public void hDay() {
        apply="gender";
        radioone.setChecked(true);
        gender_lyt.setVisibility(View.VISIBLE);
        class_lyt.setVisibility(View.GONE);
        img1.setVisibility(View.GONE);
        img.setVisibility(View.VISIBLE);
        text1_s.setVisibility(View.GONE);
        text2_s.setVisibility(View.GONE);
        text_s.setVisibility(View.VISIBLE);
        img2.setVisibility(View.GONE);
    //    textView11.setText(getString(R.string.leavedate));
       // edittextreason.setText(" ");
        oneDay.setBackgroundColor(Color.parseColor("#00000000"));
        multiDay.setBackgroundColor(Color.parseColor("#00000000"));
        hDay.setBackgroundColor(Color.parseColor("#66000000"));
        getGender();
    }

    private void getGender() {
        ArrayAdapter<String> genderAdapter = new ArrayAdapter<String>(this, android.
                R.layout.simple_spinner_dropdown_item, genderList);
        spinnerGender.setAdapter(genderAdapter);
        spinnerGender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                gender = genderList[i];
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }


    @OnClick(R.id.oneDay)
    public void oneDay() {
        apply="all";
        radioone.setChecked(true);
        class_lyt.setVisibility(View.GONE);
        gender_lyt.setVisibility(View.GONE);
        img.setVisibility(View.GONE);
        img1.setVisibility(View.VISIBLE);
        img2.setVisibility(View.GONE);
        text_s.setVisibility(View.GONE);
        text2_s.setVisibility(View.GONE);
        text1_s.setVisibility(View.VISIBLE);
     //   textView11.setText(getString(R.string.leavedate));
       // edittextreason.setText(" ");
        hDay.setBackgroundColor(Color.parseColor("#00000000"));
        multiDay.setBackgroundColor(Color.parseColor("#00000000"));
        oneDay.setBackgroundColor(Color.parseColor("#66000000"));
    }

    @OnClick(R.id.multiDay)
    public void multiDay() {
        apply="class";
        radiomulti.setChecked(true);
        class_lyt.setVisibility(View.VISIBLE);
        gender_lyt.setVisibility(View.VISIBLE);
        img.setVisibility(View.GONE);
        img2.setVisibility(View.VISIBLE);
        img1.setVisibility(View.GONE);
        text_s.setVisibility(View.GONE);
        text1_s.setVisibility(View.GONE);
        text2_s.setVisibility(View.VISIBLE);
     //   textView11.setText(getString(R.string.fromdate));
      //  edittextreason.setText(" ");
        hDay.setBackgroundColor(Color.parseColor("#00000000"));
        oneDay.setBackgroundColor(Color.parseColor("#00000000"));
        multiDay.setBackgroundColor(Color.parseColor("#66000000"));
        fetchClass();
        getGender();
    }
    private void fetchClass() {
//        if (loaded == 0) {
            commonProgress.show();
//        }
//        loaded++;

        HashMap<String, String> params = new HashMap<String, String>();
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.CLASSLIST_URL, classapiCallback,getApplicationContext(), params);
    }


    ApiHandler.ApiCallback classapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            commonProgress.dismiss();
            if (error == null) {
                try {
                    classList = ClassBean.parseClassArray(jsonObject.getJSONArray(Constants.CLASSDATA));
                    ClassAdapterAttend adapter = new ClassAdapterAttend(getApplicationContext(), classList,0);
                    spinnerClass.setAdapter(adapter);
                    spinnerClass.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if (i > 0) {
                                classid = classList.get(i - 1).getId();
                                fetchSection();
                            }

                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_LONG).show();
                // Utils.showSnackBar(error.getMessage(), view.findViewById(R.id.parent));
            }
        }
    };

    private void fetchSection() {
        commonProgress.show();
        HashMap<String, String> params = new HashMap<String, String>();
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.SECTIONLIST_URL, sectionapiCallback, this, params);
    }

    ApiHandler.ApiCallback sectionapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            commonProgress.dismiss();
            if (error == null) {
                try {
                    sectionList = SectionBean.parseCommonArray(jsonObject.getJSONArray("hostelroom_data"));
                    SectionnewAdapter adapter = new SectionnewAdapter(Add_photo.this, sectionList);
                    spinnersection.setAdapter(adapter);
                    spinnersection.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if (i > 0) {
                                sectionid = sectionList.get(i - 1).getSection_id();
                                sectionname = sectionList.get(i - 1).getSection_name();
                                // fetchHomework();
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Utils.showSnackBar(error.getMessage(), parent);

            }
        }
    };

    @SuppressLint("ResourceType")
    @OnClick(R.id.addimage)
    public void click() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.UPSIDE_DOWN_CAKE) { // Android 14+ (API 34)
            //Toast.makeText(this, "Multi Images", Toast.LENGTH_LONG).show();
            // initialising intent
            Intent intent = new Intent();
            // setting type to select to be image
            intent.setType("image/*");
            // allowing multiple image to be selected
            intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(Intent.createChooser(intent, "Select Multiple Images"), PICK_IMAGE_MULTIPLE);

        } else if (Build.VERSION.SDK_INT > Build.VERSION_CODES.S) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_MEDIA_IMAGES)
                    != PackageManager.PERMISSION_GRANTED) {

                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_MEDIA_IMAGES)) {
                    Toast.makeText(this, "Please provide storage permission from app settings", Toast.LENGTH_LONG).show();
                } else {

                    ActivityCompat.requestPermissions(this,
                            new String[]{Manifest.permission.READ_MEDIA_IMAGES},
                            1);
                }
            } else {
                //Toast.makeText(this, "Multi Images", Toast.LENGTH_LONG).show();
                // initialising intent
                Intent intent = new Intent();
                // setting type to select to be image
                intent.setType("image/*");
                // allowing multiple image to be selected
                intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Multiple Images"), PICK_IMAGE_MULTIPLE);
            }

        } else {

            if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {

                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    Toast.makeText(this, "Please provide storage permission from app settings", Toast.LENGTH_LONG).show();
                } else {

                    ActivityCompat.requestPermissions(this,
                            new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                            1);

                }
            } else {
                onMultiImageViewClick();
                // showFileChooser();
            }

        }
    }

    @SuppressLint("ResourceType")
    private void onMultiImageViewClick() {
                new BottomSheetImagePicker.Builder(getString(R.xml.provider_paths))
                .multiSelect(1, 10)
                .multiSelectTitles(
                        R.plurals.pick_multi,
                        R.plurals.pick_multi_more,
                        R.string.pick_multi_limit
                )
                .peekHeight(R.dimen.peekHeight)
                .columnSize(R.dimen.columnSize)
                .requestTag("multi")
                .show(getSupportFragmentManager(), null);

    }

    @SuppressLint("MissingSuperCall")
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // callPicker();
                    onMultiImageViewClick();
                } else {
                    // permission denied, boo! Disable the
                    Toast.makeText(this,"Please provide storage permission from app settings, as it is mandatory to access your files!",Toast.LENGTH_LONG).show();
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request.
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            // When an Image is picked
            if (requestCode == PICK_IMAGE_MULTIPLE && resultCode == RESULT_OK
                    && null != data) {
                // Get the Image from data

                String[] filePathColumn = { MediaStore.Images.Media.DATA };
                imagesEncodedList = new ArrayList<String>();
                if(data.getData()!=null){

                    Uri mImageUri=data.getData();

                    // Get the cursor
                    Cursor cursor = getContentResolver().query(mImageUri,
                            filePathColumn, null, null, null);
                    // Move to first row
                    cursor.moveToFirst();

                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    imageEncoded  = cursor.getString(columnIndex);
                    cursor.close();

                } else {
                    if (data.getClipData() != null) {
                        ClipData mClipData = data.getClipData();
                        ArrayList<Uri> mArrayUri = new ArrayList<Uri>();
                        for (int i = 0; i < mClipData.getItemCount(); i++) {

                            ClipData.Item item = mClipData.getItemAt(i);
                            Uri uri = item.getUri();
                            mArrayUri.add(uri);
                            // Get the cursor
                            Cursor cursor = getContentResolver().query(uri, filePathColumn, null, null, null);
                            // Move to first row
                            cursor.moveToFirst();

                            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                            imageEncoded  = cursor.getString(columnIndex);
                            imagesEncodedList.add(imageEncoded);
                            cursor.close();

                        }
                        commonCode(mArrayUri);
                        Log.e("LOG_TAG", "Selected Images" + mArrayUri.size());
//                        Toast.makeText(this, "Selected Images" + imagesEncodedList.size(),
//                                Toast.LENGTH_LONG).show();
                    }
                }
            } else {
                Toast.makeText(this, "You haven't picked Image",
                        Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG)
                    .show();
            Log.e("LOG_TAG", "Exception is " + e);
        }


    }
    private void commonCode(List<? extends Uri> list) {
        imageContainer.removeAllViews();
        image= new ArrayList<>();
        for (Uri uri : list) {
            ImageView iv = (ImageView) LayoutInflater.from(this).inflate(R.layout.scrollitem_image, imageContainer, false);
            imageContainer.addView(iv);
            Glide.with(this).load(uri).into(iv);
            hwbitmap = Utils.getBitmapFromUri(Add_photo.this, uri, 2048);
            encoded = Utils.encodeToBase64(hwbitmap, Bitmap.CompressFormat.JPEG, 50);
            // encoded = String.format("data:image/jpeg;base64,%s", encoded);
            image.add(encoded);

        }
    }

    @Override
    public void onImagesSelected(List<? extends Uri> list, String s) {
        commonCode(list);
    }
    @OnClick(R.id.add_photo_btn)
    public void save() {
            if (apply.equalsIgnoreCase("class")){
                if (classid.equalsIgnoreCase("")){
                    Toast.makeText(getApplicationContext(),"kindly select class",Toast.LENGTH_SHORT).show();

                }else if (sectionid.equalsIgnoreCase("")){
                    Toast.makeText(getApplicationContext(),"kindly select class section",Toast.LENGTH_SHORT).show();

                }else {
                    if (checkValid()) {
                        commonProgress.show();
                        HashMap<String, String> params = new HashMap<String, String>();
                        params.put("image", String.valueOf(image));
                        params.put("album_id", album_id);
                        params.put("apply",apply);
                        params.put("p_title", photoname.getText().toString());
                        params.put("type", "image");
                        params.put("class_id", classid);
                        params.put("section_id", sectionid);
                        if (gender.equalsIgnoreCase("select gender")){
                            params.put("gender", "");
                        }else{
                            params.put("gender", gender);
                        }

                        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.ADDPHOTO_URL, addphoapiCallback, this, params);
                    }
                    }
            }else if (apply.equalsIgnoreCase("gender")){
                if (gender.equalsIgnoreCase("Select gender")){
                    Toast.makeText(getApplicationContext(),"kindly select gender",Toast.LENGTH_SHORT).show();
                }else {
                    if (checkValid()) {
                        commonProgress.show();
                        HashMap<String, String> params = new HashMap<String, String>();
                        params.put("image", String.valueOf(image));
                        params.put("album_id", album_id);
                        params.put("apply",apply);
                        params.put("p_title", photoname.getText().toString());
                        params.put("type", "image");
                        params.put("gender", gender);
                        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.ADDPHOTO_URL, addphoapiCallback, this, params);
                    }
                }
            }else {
                if (checkValid()) {
                commonProgress.show();
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("image", String.valueOf(image));
                params.put("album_id", album_id);
                params.put("apply",apply);
                params.put("p_title", photoname.getText().toString());
                params.put("type", "image");

                ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.ADDPHOTO_URL, addphoapiCallback, this, params);
            }
        }

    }
    ApiHandler.ApiCallback addphoapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            commonProgress.dismiss();
            if (error == null) {
                   // Utils.showSnackBar(jsonObject.getString("msg"), parent);
                    Toast.makeText(getApplicationContext(),"Uploaded successfully!",Toast.LENGTH_SHORT).show();
                    finish();

            } else {
                Log.e("error", error.getMessage() + "");
                Utils.showSnackBar(error.getMessage(), parent);
                if (error.getStatusCode() == 405) {
                    User.logout();
                    startActivity(new Intent(Add_photo.this, LoginActivity.class));
                    finish();
                }
            }
        }
    };

        private boolean checkValid() {
            boolean valid = true;
            String errorMsg = null;
            if (album_id.isEmpty()) {
                valid = false;
                errorMsg = "Please Select Album";
            } else
            if (photoname.getText().toString().trim().length() <= 0) {
                valid = false;
                errorMsg = "Please Enter Photo Title";
            } else  if (hwbitmap == null) {
                valid = false;
                errorMsg = "Please Put your Image";
            }
            if (!valid) {
                Utils.showSnackBar(errorMsg, parent);
            }
            return valid;
        }
    }


