package com.ultimate.ultimatesmartschool.Gallery.NewGallery;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.squareup.picasso.Picasso;
import com.ultimate.ultimatesmartschool.Gallery.GalleryPhoto_bean;
import com.ultimate.ultimatesmartschool.Message.InboxActivity;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class IMGGridView extends AppCompatActivity implements IMG_Adapter.IMGCallback {

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.imgBack)
    ImageView back;
    @BindView(R.id.parent)
    RelativeLayout parent;
    private LinearLayoutManager layoutManager;
    GalleryPhoto_bean data;
    IMG_Adapter offerBannerAdapter;
    @BindView(R.id.totalRecord)
    TextView totalRecord;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_gallery);
        ButterKnife.bind(this);
        Bundle intent_value = getIntent().getExtras();
        if (intent_value == null) {
            return;
        }
        if (intent_value.containsKey("data")) {
            Gson gson = new Gson();
            data = gson.fromJson(intent_value.getString("data"), GalleryPhoto_bean.class);
            txtTitle.setText(data.getTitle());
            totalRecord.setText("Total Images:- "+String.valueOf(data.getImage().size()));
            recyclerView.setLayoutManager(new GridLayoutManager(this,2));
            //recyclerviewlist.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
            offerBannerAdapter = new IMG_Adapter(data.getImage(),this,this);
            recyclerView.setAdapter(offerBannerAdapter);

        } else {
            return;
        }
    }
    @OnClick(R.id.imgBack)
    public void onback() {
        finish();
    }

    @Override
    public void displayImage(String obj) {
        Dialog mBottomSheetDialog = new Dialog(this);
        final View sheetView = getLayoutInflater().inflate(R.layout.image_lyt, null);
        mBottomSheetDialog.setContentView(sheetView);
        mBottomSheetDialog.setCancelable(true);

        Animation animation;
        animation = AnimationUtils.loadAnimation(this, R.anim.btn_blink_animation);
        ImageView imgShow = (ImageView) sheetView.findViewById(R.id.imgShow);


        ImageView imgDownload = (ImageView) sheetView.findViewById(R.id.imgDownload);
        imgDownload.setVisibility(View.GONE);
       // Utils.progressImg_two(obj,imgShow, IMGGridView.this,"");
        Picasso.get().load(obj).into(imgShow);

        mBottomSheetDialog.show();
        Window window = mBottomSheetDialog.getWindow();
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

    }
}