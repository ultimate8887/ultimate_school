package com.ultimate.ultimatesmartschool.Gallery;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.ultimate.ultimatesmartschool.R;

import java.util.ArrayList;

public class SpinAlbumlistadapter extends BaseAdapter {
    Context context;
    ArrayList<Albumlistbean> adminlist;
    LayoutInflater inflter;
    public SpinAlbumlistadapter(Context context, ArrayList<Albumlistbean> adminlist) {
        this.context=context;
        this.adminlist=adminlist;
        inflter = (LayoutInflater.from(context));
    }

    @Override
    public int getCount() {
        return adminlist.size() + 1;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = inflter.inflate(R.layout.spinner_lyt_msg, null);
        TextView label = convertView.findViewById(R.id.txtText);
        if (position == 0) {
            // Default selected Spinner item
            label.setText("Albums");
        } else {
            Albumlistbean classObj = adminlist.get(position - 1);
            label.setText(classObj.getAlbum_name());
        }

        return convertView;
    }
}
