package com.ultimate.ultimatesmartschool.Examination;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.google.gson.Gson;
import com.ultimate.ultimatesmartschool.BeanModule.User;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AddMarksSubWiseGDS extends AppCompatActivity {
    Examdetailbean examdetailbean;
    @BindView(R.id.subjectnam)
    TextView subjectnam;
    @BindView(R.id.totalmarksnam)TextView totalmarksnam;
    @BindView(R.id.passmarks)TextView passmarks;
    @BindView(R.id.testdate)TextView examdate;
    @BindView(R.id.stdrecycler)
    RecyclerView recyclerView;
    @BindView(R.id.resultsave)
    Button resultsave;
    @BindView(R.id.parem)
    LinearLayout parent;
    UploadMarksSubWiseAdapter adapter;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.imgBack)
    ImageView back;
    String classid,sectionid;
    ArrayList<ResultExamListSubBean> StuddentListBeanList = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_marks_sub_wise_g_d_s);
        ButterKnife.bind(this);
        txtTitle.setText("Examination");
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        resultsave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SaveResult();
            }
        });
        if(getIntent()!= null && getIntent().hasExtra("subdetail")){
            Gson gson = new Gson();
            examdetailbean = gson.fromJson(getIntent().getStringExtra("subdetail"),Examdetailbean.class);
        }else {
            return;
        }
        subjectnam.setText("Subject: "+examdetailbean.getSubject_name());
        totalmarksnam.setText("Total Marks: "+examdetailbean.getTotal_marks());
        passmarks.setText("Pass Marks: "+examdetailbean.getPass_marks());
        examdate.setText("Exam Date: "+(Utils.getDateFormated(examdetailbean.getExam_date())));
        classid = getIntent().getExtras().getString("classid");
        sectionid = getIntent().getExtras().getString("section");
        // Log.e("meraclassid",classid);
        recyclerView.setLayoutManager(new LinearLayoutManager(AddMarksSubWiseGDS.this));
        adapter = new UploadMarksSubWiseAdapter(AddMarksSubWiseGDS.this, StuddentListBeanList);
        recyclerView.setAdapter(adapter);
        fetchStudentList();
    }

    private void fetchStudentList() {
        ErpProgress.showProgressBar(this, "Please wait...");
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("class_id", classid);
        params.put("section", sectionid);
        params.put("subject_id",examdetailbean.getSubject_id());
        params.put("examdetail_id",examdetailbean.getExamdetailid());

        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.StudentListbyexamurlgds, studentlistapiCallback, this, params);
    }

    ApiHandler.ApiCallback studentlistapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            if (error == null) {
                try {
                    JSONArray jsonArray = jsonObject.getJSONArray("student_list");
                    StuddentListBeanList = ResultExamListSubBean.parsestunamemrkArray(jsonArray);



                    if (StuddentListBeanList.size() > 0) {
                        adapter.setStudentList(StuddentListBeanList);
                        adapter.notifyDataSetChanged();
                    } else {
                        adapter.setStudentList(StuddentListBeanList);
                        adapter.notifyDataSetChanged();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    };


    private void SaveResult() {
        if (checkValid1()) {
            ErpProgress.showProgressBar(this, "Please wait...");
            ArrayList<String> marksList = new ArrayList<>();
            ArrayList<String> studentList = new ArrayList<>();
            ArrayList<String> marksidlist = new ArrayList<>();
            for (int i = 0; i < adapter.getStudentList().size(); i++) {
                marksList.add(adapter.getStudentList().get(i).getMarks());
                studentList.add(adapter.getStudentList().get(i).getId());
                marksidlist.add(adapter.getStudentList().get(i).getMarksid());
            }
            HashMap<String, String> params = new HashMap<String, String>();
            // params.put("test_id", test_id);
            params.put("stu_id", String.valueOf(studentList));
            Log.e("st_id", String.valueOf(studentList));
            params.put("given_mark", String.valueOf(marksList));
            Log.e("marks", String.valueOf(marksList));
            params.put("examdetail_id", examdetailbean.getExamdetailid());
            Log.e("examdetailid", examdetailbean.getExamdetailid());
            params.put("marksid", String.valueOf(marksidlist));
            params.put("userid", User.getCurrentUser().getId());
            Log.e("marksid", String.valueOf(marksidlist));

           ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.marksuploadsubwiseurl, uploadtestmarksapiCallback, this, params);
        }
    }
    ApiHandler.ApiCallback uploadtestmarksapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            if (error == null) {
                Toast.makeText(AddMarksSubWiseGDS.this, "UPLOADED SUCEES", Toast.LENGTH_SHORT).show();
                finish();
            } else {
                Toast.makeText(AddMarksSubWiseGDS.this,error.getMessage(), Toast.LENGTH_SHORT).show();
                Log.e("16955", error.getMessage() + "");
            }
        }
    };

    private boolean checkValid1() {
        boolean valid = true;
        String errorMsg = null;
        int marks = 0;
        for (int i = 0; i < adapter.getStudentList().size(); i++) {
            try {
                marks = Integer.parseInt(String.valueOf(adapter.getStudentList().get(i).getMarks()));
            }catch (NumberFormatException ex){

                errorMsg = "Please enter the Marks or valid marks!";
            }
            if (marks >100 || adapter.getStudentList().get(i).getMarks() == null || adapter.getStudentList().get(i).getMarks().isEmpty()) {
                valid = false;
                errorMsg = "Please enter the Marks or valid marks!";
                break;
            }

//            ResultExamListSubBean sub = adapter.getStudentList().get(i);
//            if (sub.getMarks() == null|| sub.getMarks().isEmpty()) {
//                valid = false;
//                errorMsg = "please enter  marks!";
//                break;
//            }


        }
        if (!valid) {

            Utils.showSnackBar(errorMsg, parent);
        }
        return valid;
    }
}
