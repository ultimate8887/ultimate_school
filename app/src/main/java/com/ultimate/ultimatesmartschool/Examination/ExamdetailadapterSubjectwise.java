package com.ultimate.ultimatesmartschool.Examination;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ExamdetailadapterSubjectwise extends RecyclerView.Adapter<ExamdetailadapterSubjectwise.Viewholder> {
    ArrayList<Examdetailbean> examdetaillist;
    Context context;
    Mycallback mAdaptercall;
    public ExamdetailadapterSubjectwise(ArrayList<Examdetailbean> examdetaillist, Context context,Mycallback mAdaptercall) {
        this.context=context;
        this.examdetaillist=examdetaillist;
        this.mAdaptercall= mAdaptercall;

    }

    @NonNull
    @Override
    public ExamdetailadapterSubjectwise.Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.exmdetailadasubjectwise_lay, parent, false);
        ExamdetailadapterSubjectwise.Viewholder viewholder = new ExamdetailadapterSubjectwise.Viewholder(view);
        return viewholder;
    }

    @Override
    public void onBindViewHolder(@NonNull ExamdetailadapterSubjectwise.Viewholder holder, final int position) {
        holder.subname.setText((CharSequence) examdetaillist.get(position).getSubject_name());
        holder.doe.setText(Utils.getDateFormated( examdetaillist.get(position).getExam_date()));
        holder.textexamduration.setText("Duration:" + " " + examdetaillist.get(position).getExam_duration());
        holder.exmfullmrks.setText("Total Marks:" + " " + examdetaillist.get(position).getTotal_marks());
        holder.exmpassmrksss.setText("Pass Marks:" + " "+ examdetaillist.get(position).getPass_marks());

        holder.upldmarks.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                if (mAdaptercall != null) {
                    mAdaptercall.onUploadmarksCallback(examdetaillist.get(position));
                }

            }
        });

        holder.fviewresult.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                if (mAdaptercall != null) {
                    mAdaptercall.onViewResultCallback(examdetaillist.get(position));
                }

            }
        });

    }

    @Override
    public int getItemCount() {
        return examdetaillist.size();
    }

    public void setexmdetailList(ArrayList<Examdetailbean> examdetaillist) {
        this.examdetaillist = examdetaillist;
    }

    public interface Mycallback {

        public void onViewResultCallback(Examdetailbean examdetailbean);
        public void onUploadmarksCallback(Examdetailbean examdetailbean);
    }

    public class Viewholder extends RecyclerView.ViewHolder {
        @BindView(R.id.textsubjectnm)
        TextView subname;
        @BindView(R.id.textdateofexm)TextView doe;
        @BindView(R.id.textexamduration)TextView textexamduration;
        @BindView(R.id.exmfullmrks)TextView exmfullmrks;
        @BindView(R.id.exmpassmrksss)TextView exmpassmrksss;
        @BindView(R.id.fupldreslttext)TextView upldmarks;
        @BindView(R.id.fviewresult)TextView fviewresult;

        public Viewholder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}
