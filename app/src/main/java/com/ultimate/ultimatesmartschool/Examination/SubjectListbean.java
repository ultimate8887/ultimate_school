package com.ultimate.ultimatesmartschool.Examination;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class SubjectListbean {

    private static String ID = "id";
    private static String NAME = "name";
    private static String EXAM_DATE = "exam_date";
    private static String EXAM_DUR = "exam_dur";
    private static String EXAM_MARKS=" exam_markS";
    private static String PASS_MARKS="pass_marks";
    private String exam_Date;
    private String exam_dur;
    private String exam_markS;
    private String pass_marks;
    private String exam_Dateshow;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    String name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    String id;

    public static ArrayList<SubjectListbean> parsesubjectArray(JSONArray jsonArray) {
        ArrayList list = new ArrayList<SubjectListbean>();
        try {
            for (int i = 0; i < jsonArray.length(); i++) {
                SubjectListbean p = parsesubjObject(jsonArray.getJSONObject(i));
                if (p != null) {
                    list.add(p);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        return list;
    }

    private static SubjectListbean parsesubjObject(JSONObject jsonObject) {
        SubjectListbean casteObj=new SubjectListbean();
        try {
            if (jsonObject.has(ID)) {
                casteObj.setId(jsonObject.getString(ID));
            }
            if (jsonObject.has(NAME) && !jsonObject.getString(NAME).isEmpty() && !jsonObject.getString(NAME).equalsIgnoreCase("null")) {
                casteObj.setName(jsonObject.getString(NAME));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return casteObj;
    }

    public String getExam_Date() {
        return exam_Date;
    }

    public void setExam_Date(String exam_Date) {
        this.exam_Date = exam_Date;
    }


    public String getExam_dur() {
        return exam_dur;
    }

    public void setExam_dur(String exam_dur ) {
        this.exam_dur=exam_dur;
    }

    public String getExam_markS() {
        return exam_markS;
    }

    public void setExam_markS(String exam_markS) {
        this.exam_markS = exam_markS;
    }

    public String getPass_marks() {
        return pass_marks;
    }

    public void setPass_marks(String pass_marks) {
        this.pass_marks = pass_marks;
    }

    public void setExam_Dateshowdb(String exam_Dateshow) {
        this.exam_Dateshow = exam_Dateshow;
    }

    public String getExam_Dateshowdb() {
        return exam_Dateshow;
    }
}
