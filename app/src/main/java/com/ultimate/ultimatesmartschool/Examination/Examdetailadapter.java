package com.ultimate.ultimatesmartschool.Examination;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Examdetailadapter extends RecyclerView.Adapter<Examdetailadapter.Viewholder> {
    ArrayList<Examdetailbean> examdetaillist;
    Context context;
    public Examdetailadapter(ArrayList<Examdetailbean> examdetaillist, Context context) {
        this.context=context;
        this.examdetaillist=examdetaillist;

    }

    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.exmdetailada_lay, parent, false);
        Examdetailadapter.Viewholder viewholder = new Examdetailadapter.Viewholder(view);
        return viewholder;
    }

    @Override
    public void onBindViewHolder(@NonNull Viewholder holder, int position) {
        holder.subname.setText((CharSequence) examdetaillist.get(position).getSubject_name());
        holder.doe.setText(Utils.getDateFormated( examdetaillist.get(position).getExam_date()));


        String title1 = getColoredSpanned("Duration: ", "#383737");
        String Name1 = getColoredSpanned(examdetaillist.get(position).getExam_duration(), "#5A5C59");
        String t1 = getColoredSpanned("Hour", "#5A5C59");
        holder.textexamduration.setText(Html.fromHtml(title1 + " " + Name1 + " " + t1));

        String title = getColoredSpanned("Total Marks: ", "#383737");
        String Name = getColoredSpanned(examdetaillist.get(position).getTotal_marks(), "#5A5C59");
        holder.exmfullmrks.setText(Html.fromHtml(title + " " + Name));
        //  holder.exmfullmrks.setText("Total Marks:" + " " + examdetaillist.get(position).getTotal_marks());
        // holder.exmpassmrksss.setText("Pass Marks:" + " "+ examdetaillist.get(position).getPass_marks());
        String title2 = getColoredSpanned("Pass Marks: ", "#383737");
        String Name2 = getColoredSpanned(examdetaillist.get(position).getPass_marks(), "#5A5C59");
        holder.exmpassmrksss.setText(Html.fromHtml(title2 + " " + Name2));

    }

    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }

    @Override
    public int getItemCount() {
        return examdetaillist.size();
    }

    public void setexmdetailList(ArrayList<Examdetailbean> examdetaillist) {
        this.examdetaillist = examdetaillist;
    }

    public class Viewholder extends RecyclerView.ViewHolder {
        @BindView(R.id.textsubjectnm)
        TextView subname;
        @BindView(R.id.textdateofexm)TextView doe;
        @BindView(R.id.textexamduration)TextView textexamduration;
        @BindView(R.id.exmfullmrks)TextView exmfullmrks;
        @BindView(R.id.exmpassmrksss)TextView exmpassmrksss;

        public Viewholder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}
