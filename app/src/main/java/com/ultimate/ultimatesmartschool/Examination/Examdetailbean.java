package com.ultimate.ultimatesmartschool.Examination;

import com.ultimate.ultimatesmartschool.Utility.DebugLog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Examdetailbean {

    private static String SUBJECT_ID="subject_id";
    private static String EXAMDATE="exam_date";
    private static String EXAM_DURATION="exam_duration";
    private static String TOTAL_MARKS="total_marks";
    private static String PASSMARKS="pass_marks";
    private static String SUBJECT_NAME="subject_name";

    /**
     * subject_id : 0
     * exam_date : 0000-00-00 00:00:00
     * exam_duration : 57
     * total_marks : 0
     * pass_marks : 0
     * subject_name : null
     */

    private String subject_id;
    private String exam_date;
    private String exam_duration;
    private String total_marks;
    private String pass_marks;
    private String subject_name;
    private String given_marks;
    private String examdetailid;

    private String get_marks;
    private String marksid;

    private String grand_total_marks;
    private String grand_obtained_marks;

    public String getGrand_total_marks() {
        return grand_total_marks;
    }

    public void setGrand_total_marks(String grand_total_marks) {
        this.grand_total_marks = grand_total_marks;
    }

    public String getGrand_obtained_marks() {
        return grand_obtained_marks;
    }

    public void setGrand_obtained_marks(String grand_obtained_marks) {
        this.grand_obtained_marks = grand_obtained_marks;
    }

    public String getGrand_grade() {
        return grand_grade;
    }

    public void setGrand_grade(String grand_grade) {
        this.grand_grade = grand_grade;
    }

    private String grand_grade;



    public String getMarksid() {
        return marksid;
    }

    public void setMarksid(String marksid) {
        this.marksid = marksid;
    }

    public String getGet_marks() {
        return get_marks;
    }

    public void setGet_marks(String get_marks) {
        this.get_marks = get_marks;
    }

    public String getExamdetailid() {
        return examdetailid;
    }

    public void setExamdetailid(String examdetailid) {
        this.examdetailid = examdetailid;
    }

    public String getSubject_id() {
        return subject_id;
    }

    public void setSubject_id(String subject_id) {
        this.subject_id = subject_id;
    }

    public String getExam_date() {
        return exam_date;
    }

    public void setExam_date(String exam_date) {
        this.exam_date = exam_date;
    }

    public String getExam_duration() {
        return exam_duration;
    }

    public void setExam_duration(String exam_duration) {
        this.exam_duration = exam_duration;
    }

    public String getTotal_marks() {
        return total_marks;
    }

    public void setTotal_marks(String total_marks) {
        this.total_marks = total_marks;
    }

    public String getPass_marks() {
        return pass_marks;
    }

    public void setPass_marks(String pass_marks) {
        this.pass_marks = pass_marks;
    }

    public Object getSubject_name() {
        return subject_name;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    /**
     * class_name : NURSERY1
     */

    private String grade;

    public void setSubject_name(String subject_name) {
        this.subject_name = subject_name;
    }


    public static ArrayList<Examdetailbean> parseexamDetal_Array(JSONArray jsonArray) {
        ArrayList<Examdetailbean> list=new ArrayList<>();
        try {

            for (int i = 0; i < jsonArray.length(); i++) {
                Examdetailbean p = parseexamdetalObject(jsonArray.getJSONObject(i));
                if (p != null) {
                    list.add(p);
                }
            }
        } catch (Exception e) {
            DebugLog.printLog("exp",e.getMessage());
            e.printStackTrace();
        }
        return list;
    }

    private static Examdetailbean parseexamdetalObject(JSONObject jsonObject) {
        Examdetailbean casteObj = new Examdetailbean();
        try {

            if (jsonObject.has(SUBJECT_ID) && !jsonObject.getString(SUBJECT_ID).isEmpty() && !jsonObject.getString(SUBJECT_ID).equalsIgnoreCase("null")) {
                casteObj.setSubject_id(jsonObject.getString(SUBJECT_ID));
            }
            if (jsonObject.has(EXAMDATE) && !jsonObject.getString(EXAMDATE).isEmpty() && !jsonObject.getString(EXAMDATE).equalsIgnoreCase("null")) {
                casteObj.setExam_date(jsonObject.getString(EXAMDATE));
            }

            if (jsonObject.has(EXAM_DURATION) && !jsonObject.getString(EXAM_DURATION).isEmpty() && !jsonObject.getString(EXAM_DURATION).equalsIgnoreCase("null")) {
                casteObj.setExam_duration(jsonObject.getString(EXAM_DURATION));
            }
            if (jsonObject.has(TOTAL_MARKS) && !jsonObject.getString(TOTAL_MARKS).isEmpty() && !jsonObject.getString(TOTAL_MARKS).equalsIgnoreCase("null")) {
                casteObj.setTotal_marks(jsonObject.getString(TOTAL_MARKS));
            }

            if (jsonObject.has(PASSMARKS) && !jsonObject.getString(PASSMARKS).isEmpty() && !jsonObject.getString(PASSMARKS).equalsIgnoreCase("null")) {
                casteObj.setPass_marks(jsonObject.getString(PASSMARKS));
            }
            if (jsonObject.has(SUBJECT_NAME) && !jsonObject.getString(SUBJECT_NAME).isEmpty() && !jsonObject.getString(SUBJECT_NAME).equalsIgnoreCase("null")) {
                casteObj.setSubject_name(jsonObject.getString(SUBJECT_NAME));
            }
            if (jsonObject.has("examdetailid") && !jsonObject.getString("examdetailid").isEmpty() && !jsonObject.getString("examdetailid").equalsIgnoreCase("null")) {
                casteObj.setExamdetailid(jsonObject.getString("examdetailid"));
            }

            if (jsonObject.has("grade") && !jsonObject.getString("grade").isEmpty() && !jsonObject.getString("grade").equalsIgnoreCase("null")) {
                casteObj.setGrade(jsonObject.getString("grade"));
            }

            if (jsonObject.has("get_marks") && !jsonObject.getString("get_marks").isEmpty() && !jsonObject.getString("get_marks").equalsIgnoreCase("null")) {
                casteObj.setGiven_marks(jsonObject.getString("get_marks"));
            }

            if (jsonObject.has("marksid") && !jsonObject.getString("marksid").isEmpty() && !jsonObject.getString("marksid").equalsIgnoreCase("null")) {
                casteObj.setMarksid(jsonObject.getString("marksid"));
            }

            if (jsonObject.has("grand_total_marks") && !jsonObject.getString("grand_total_marks").isEmpty() && !jsonObject.getString("grand_total_marks").equalsIgnoreCase("null")) {
                casteObj.setGrand_total_marks(jsonObject.getString("grand_total_marks"));
            }

            if (jsonObject.has("grand_obtained_marks") && !jsonObject.getString("grand_obtained_marks").isEmpty() && !jsonObject.getString("grand_obtained_marks").equalsIgnoreCase("null")) {
                casteObj.setGrand_obtained_marks(jsonObject.getString("grand_obtained_marks"));
            }

            if (jsonObject.has("grand_grade") && !jsonObject.getString("grand_grade").isEmpty() && !jsonObject.getString("grand_grade").equalsIgnoreCase("null")) {
                casteObj.setGrand_grade(jsonObject.getString("grand_grade"));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return casteObj;
    }

    public void setGiven_marks(String given_marks) {
        this.given_marks = given_marks;
    }

    public String getGiven_marks() {
        return given_marks;
    }

}
