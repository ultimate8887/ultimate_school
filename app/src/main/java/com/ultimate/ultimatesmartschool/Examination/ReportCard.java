package com.ultimate.ultimatesmartschool.Examination;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetSequence;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.squareup.picasso.Picasso;
import com.ultimate.ultimatesmartschool.BeanModule.User;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.CommonProgress;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ReportCard extends AppCompatActivity {


    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    private ArrayList<Exambean> examList = new ArrayList<>();
    ArrayList<Sessionexambean> sessionlist = new ArrayList<>();
    ArrayList<Examdetailbean> examdetaillist = new ArrayList<>();
    ReportCardAdapter examdetailadapter;
    Exmnamespinadapter exmnamespinadapter;
    Acadmicyrspinadpter acadmicyrspinadpter;
    private String examid = "";
    private String dob1 = "";
    private String exmnm = "";
    private String classnm = "";
    private Sessionexambean acdemic;
    @BindView(R.id.e_instruction)
    TextView e_instruction;
    Spinner spinner_session, spinner_exam;
    BottomSheetDialog mBottomSheetDialog;
    CommonProgress commonProgress;
    Animation animation;
    String logo="",s_sign="",a_sign="",school="",address="",date="";
    int totalScore = 0,totatMarks= 0,final_totatMarks= 0,gradeMarks= 0;
    double dou_totalScore=0,dou_marks = 0,dou_totatMarks=0;
    @BindView(R.id.class_name)
    TextView class_name;
    @BindView(R.id.roll)
    TextView roll;
    @BindView(R.id.s_name)
    TextView s_name;
    @BindView(R.id.export)
    FloatingActionButton imageView;
    SharedPreferences sharedPreferences;
    @BindView(R.id.p_name)
    TextView p_name;
    @BindView(R.id.e_name)
    TextView e_name;
    @BindView(R.id.a_year)
    TextView a_year;
    @BindView(R.id.t_score)
    TextView t_score;
    @BindView(R.id.submit)
    TextView submit;
    @BindView(R.id.t_marks)
    TextView t_marks;
    @BindView(R.id.s_signature)
    ImageView s_signature;
    @BindView(R.id.signature)
    ImageView p_signature;
    @BindView(R.id.logo)
    ImageView w_logo;
    @BindView(R.id.circleimgrecepone)
    CircularImageView circleImgLogo;

    @BindView(R.id.std_profile)
    ImageView std_profile;
    @BindView(R.id.txtSchool)
    TextView txtTitle;
    @BindView(R.id.percentage)
    TextView percentagees;
    private String mother_name = "",profile="",sch_name="",s_address="";
    String name="",f_name="",id="",classid="",classname="",sectionid="",sectionname="",fromdate="",todate="";
    @BindView(R.id.parent)
    LinearLayout parent;
    @BindView(R.id.layt)
    RelativeLayout layt;
    @BindView(R.id.note)
    RelativeLayout note;
    @BindView(R.id.imgBackmsg)
    ImageView back;
    @BindView(R.id.scrollView)
    NestedScrollView scrollView;

    Window window;

    @BindView(R.id.class_name1)
    TextView class_name1;
    @BindView(R.id.class_name2)
    TextView class_name2;

    @BindView(R.id.dob1)
    TextView dob11;
    @BindView(R.id.dob2)
    TextView dob2;


    @BindView(R.id.s_name_mother)
    TextView s_name_mother;

    @BindView(R.id.dob)
    TextView dob;

    @BindView(R.id.date)
    TextView p_date;

    @BindView(R.id.txtAffillated)
    TextView txtAffillated;

    @BindView(R.id.txtAdd)
    TextView txtAdd;

    @BindView(R.id.term)
    TextView term;

    @BindView(R.id.total_lyt)
    LinearLayout total_lyt;
    @BindView(R.id.percentage_lyt)
    LinearLayout percentage_lyt;


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_exam_wise);
        ButterKnife.bind(this);
//        window=this.getWindow();
//        window.setStatusBarColor(this.getResources().getColor(R.color.transparent));
        commonProgress=new CommonProgress(this);
        animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.btn_blink_animation);
        if (getIntent().getExtras() != null) {
            name = getIntent().getExtras().getString("name");
            examid = getIntent().getExtras().getString("examid");
            exmnm = getIntent().getExtras().getString("exam_name");
            f_name = getIntent().getExtras().getString("f_name");
            id = getIntent().getExtras().getString("id");

            dob1 = getIntent().getExtras().getString("dob");
            mother_name= getIntent().getExtras().getString("mother_name");
            profile= getIntent().getExtras().getString("profile");
            mother_name= getIntent().getExtras().getString("mother_name");
            profile= getIntent().getExtras().getString("profile");

//            logo = getIntent().getExtras().getString("logo");
//            school = getIntent().getExtras().getString("school");
//            a_sign = getIntent().getExtras().getString("a_sign");
//            s_sign = getIntent().getExtras().getString("s_sign");
//            address = getIntent().getExtras().getString("address");

            classid = getIntent().getExtras().getString("class");
            classname = getIntent().getExtras().getString("class_name");
            sectionid = getIntent().getExtras().getString("sectionid");
            sectionname = getIntent().getExtras().getString("sectionname");
            fromdate = getIntent().getExtras().getString("fromdate");
            todate = getIntent().getExtras().getString("todate");
            userprofile(classid);
        }

        recyclerView.setLayoutManager(new LinearLayoutManager(ReportCard.this));
        examdetailadapter = new ReportCardAdapter(examdetaillist, this);
        recyclerView.setAdapter(examdetailadapter);
        getTodayDate();

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                back.startAnimation(animation);
                finish();
            }
        });
    }

    private void userprofile(String classid) {
        // UltimateProgress.showProgressBar(getActivity(),"Please Wait........!");
//        commonProgress.show();
        HashMap<String, String> params = new HashMap<>();
        params.put("user_id", User.getCurrentUser().getId());
        params.put("check", "fetch");
        params.put("sec_id", sectionid);
        params.put("classid", classid);

        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.UPDATE_SCHOOL_INFO, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error ) {
                //  commonProgress.dismiss();
                //  ErpProgress.cancelProgressBar();
                if (error == null) {
                    try {
                        //  Log.e("daaaaaaaaaaaaaaa",jsonObject.getJSONObject(Constants.USERDATA).toString());
                        a_sign = jsonObject.getJSONObject(Constants.USERDATA).getString("a_signature");
                        s_sign = jsonObject.getJSONObject(Constants.USERDATA).getString("s_signature");
                        logo = jsonObject.getJSONObject(Constants.USERDATA).getString("logo");
                        address = jsonObject.getJSONObject(Constants.USERDATA).getString("address");
//                        s_sign = jsonObject.getJSONObject(Constants.USERDATA).getString("s_signature");
                        school = jsonObject.getJSONObject(Constants.USERDATA).getString("name");

                        // name = jsonObject.getJSONObject(Constants.USERDATA).getString("s_signature");
                        fetchexamResults();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }, this, params);
    }


    private void setData_school() {
        if (!logo.equalsIgnoreCase("")) {
            //  Utils.setImageUri(ReportCard.this,logo,w_logo);
            Picasso.get().load(logo).placeholder(R.color.transparent).into(w_logo);
            // w_logo.setColorFilter(ContextCompat.getColor(this, R.color.water_mark), android.graphics.PorterDuff.Mode.MULTIPLY);

        } else {
            Picasso.get().load(R.color.transparent).into(w_logo);
        }

        if (!a_sign.equalsIgnoreCase("") || a_sign!=null) {
            Utils.setImageUri(ReportCard.this,a_sign,p_signature);
            // Picasso.with(AddFeesQRActivity.this).load(image_url).placeholder(R.color.transparent).into(image);
        }
        else {
            Picasso.get().load(R.color.transparent).into(p_signature);
        }
        if (!s_sign.equalsIgnoreCase("") || s_sign!=null) {
            Utils.setImageUri(ReportCard.this,s_sign,s_signature);
            // Picasso.with(AddFeesQRActivity.this).load(image_url).placeholder(R.color.transparent).into(image);
        } else {
            Picasso.get().load(R.color.transparent).into(s_signature);
        }
    }
    public void fetchexamResults(){
        commonProgress.show();
        String acdexm_year = fromdate + todate;
        HashMap<String, String> params = new HashMap<>();
        //params.put("grp_id",groupid);
        params.put("class_id", classid);
        params.put("stu_id",id);
        params.put("exam_id",examid);
        params.put("acadmic_year",acdexm_year);
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.Examdetailbyclassstu, apiCallbackexmdetail, this, params);
    }

    ApiHandler.ApiCallback apiCallbackexmdetail = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            commonProgress.dismiss();
            // mBottomSheetDialog.dismiss();
            if (error == null) {
                try {
                    if(examdetaillist!=null){
                        examdetaillist.clear();
                    }
                    if (examdetaillist != null) {
                        JSONArray jsonArray = jsonObject.getJSONArray("examdetail_data");
                        //classnm=examdeta.getClass_name();
                        examdetaillist = Examdetailbean.parseexamDetal_Array(jsonArray);
                        examdetailadapter.setSyllabusList(examdetaillist);
                        //setanimation on adapter...
                        recyclerView.getAdapter().notifyDataSetChanged();
                        //  recyclerView.scheduleLayoutAnimation();
                        //-----------end------------
                        //  txtNorecord.setVisibility(View.GONE);

                        for (int i = 0; i<examdetaillist.size(); i++)
                        {

                            String marks="",s_marks="",score="",grade_sub="";
                            marks=examdetaillist.get(i).getTotal_marks().trim();
                            score=examdetaillist.get(i).getGiven_marks().trim();
                            s_marks=examdetaillist.get(i).getTotal_marks().trim();


                            if (marks.equalsIgnoreCase("A")||marks.equalsIgnoreCase("A+")
                                    ||marks.equalsIgnoreCase("B")||marks.equalsIgnoreCase("B+")
                                    ||marks.equalsIgnoreCase("C")||marks.equalsIgnoreCase("C+")
                                    ||marks.equalsIgnoreCase("D")||marks.equalsIgnoreCase("D+")
                                    ||marks.equalsIgnoreCase("E")||marks.equalsIgnoreCase("E+")
                                    ||marks.equalsIgnoreCase("F")||marks.equalsIgnoreCase("F+")
                            ){
                                marks="0";
                            }else {
                                marks=examdetaillist.get(i).getTotal_marks().trim();
                            }


                            dou_totatMarks += Double.parseDouble(examdetaillist.get(i).getTotal_marks().trim());

                            try {
                                dou_marks = Double.parseDouble(examdetaillist.get(i).getGiven_marks().trim());
                                dou_totalScore += dou_marks;
                            } catch (NumberFormatException e) {
                                System.err.println("Error parsing marks for entry " + i + ": " + examdetaillist.get(i).getGiven_marks());
                                // Handle the exception appropriately
                            }

                         //   totatMarks += Integer.parseInt(marks);

                            if (score.equalsIgnoreCase("A")||score.equalsIgnoreCase("A+")
                                    ||score.equalsIgnoreCase("B")||score.equalsIgnoreCase("B+")
                                    ||score.equalsIgnoreCase("C")||score.equalsIgnoreCase("C+")
                                    ||score.equalsIgnoreCase("D")||score.equalsIgnoreCase("D+")
                                    ||score.equalsIgnoreCase("E")||score.equalsIgnoreCase("E+")
                                    ||score.equalsIgnoreCase("F")||score.equalsIgnoreCase("F+")
                                    ||score.equalsIgnoreCase("NH")||score.equalsIgnoreCase("nh")
                                    ||score.equalsIgnoreCase("NP")||score.equalsIgnoreCase("np")
                                    ||score.equalsIgnoreCase("A1")||score.equalsIgnoreCase("A2")
                                    ||score.equalsIgnoreCase("B1")||score.equalsIgnoreCase("B2")
                                    ||score.equalsIgnoreCase("C1")||score.equalsIgnoreCase("C2")
                                    ||score.equalsIgnoreCase("D1")||score.equalsIgnoreCase("D2")
                                    ||score.equalsIgnoreCase("E1")||score.equalsIgnoreCase("E2")
                                    ||score.equalsIgnoreCase("F1")||score.equalsIgnoreCase("F2")
                            )
                            {

                                if (s_marks.equalsIgnoreCase("A")||s_marks.equalsIgnoreCase("A+")
                                        ||s_marks.equalsIgnoreCase("B")||s_marks.equalsIgnoreCase("B+")
                                        ||s_marks.equalsIgnoreCase("C")||s_marks.equalsIgnoreCase("C+")
                                        ||s_marks.equalsIgnoreCase("D")||s_marks.equalsIgnoreCase("D+")
                                        ||s_marks.equalsIgnoreCase("E")||s_marks.equalsIgnoreCase("E+")
                                        ||s_marks.equalsIgnoreCase("F")||s_marks.equalsIgnoreCase("F+")
                                ){
                                    s_marks="0";
                                }else {
                                    s_marks=examdetaillist.get(i).getTotal_marks().trim();
                                }
                                grade_sub=s_marks;
                                score="0";
                            }else if(score.equalsIgnoreCase("N/A")||score.equalsIgnoreCase("NA")
                                    ||score.equalsIgnoreCase("AB")||score.equalsIgnoreCase("ab")
                                    ||score.equalsIgnoreCase("ABSENT")||score.equalsIgnoreCase("absent")
                                    ||score.equalsIgnoreCase("le")||score.equalsIgnoreCase("leave")){
                                grade_sub="0";
                                score="0";
                            }else {
                                grade_sub="0";
                                score=examdetaillist.get(i).getGiven_marks().trim();
                            }

                          //  gradeMarks += Integer.parseInt(grade_sub);
                          //  totalScore += Integer.parseInt(score);
                        }
                        setData_school();
                        setData();

                    } else {
                        examdetailadapter.setSyllabusList(examdetaillist);
                        examdetailadapter.notifyDataSetChanged();
                        //  txtNorecord.setVisibility(View.VISIBLE);

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Toast.makeText(getApplicationContext(),exmnm+" Examination Result is not available, \nkindly Check another Examination result",Toast.LENGTH_LONG).show();
                // txtNorecord.setVisibility(View.VISIBLE);
                examdetaillist.clear();
                examdetailadapter.setSyllabusList(examdetaillist);
                examdetailadapter.notifyDataSetChanged();
                finish();
            }
        }
    };


    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }

    public void getTodayDate() {
        java.util.Calendar c = java.util.Calendar.getInstance();
        c.set(java.util.Calendar.HOUR_OF_DAY, 00);
        c.set(java.util.Calendar.MINUTE, 00);
        c.set(java.util.Calendar.SECOND, 00);
        Date setdate = c.getTime();
        java.text.SimpleDateFormat fmtOut = new java.text.SimpleDateFormat("dd MMM, yyyy");
        String dateString = fmtOut.format(setdate);
        java.text.SimpleDateFormat dateFrmOut = new java.text.SimpleDateFormat("yyyy-MM-dd");
        date = dateFrmOut.format(setdate);
    }

    private void setShowcaseViewP() {
        new TapTargetSequence(this)
                .targets(TapTarget.forView(imageView,"Export Report Card!","Tap the download button to download Report Card.")
                        .outerCircleColor(R.color.light)
                        .outerCircleAlpha(0.96f)
                        .targetCircleColor(R.color.white)
                        .titleTextSize(20)
                        .titleTextColor(R.color.white)
                        .descriptionTextSize(14)
                        .descriptionTextColor(R.color.black)
                        .textColor(R.color.black)
                        .textTypeface(Typeface.SANS_SERIF)
                        .dimColor(R.color.black)
                        .drawShadow(true)
                        .cancelable(false)
                        .tintTarget(true)
                        .transparentTarget(true)
                        .targetRadius(40)).listener(new TapTargetSequence.Listener() {
                    @RequiresApi(api = Build.VERSION_CODES.N)
                    @Override
                    public void onSequenceFinish() {
                        // Toast.makeText(getApplicationContext(),"Sequence Finished",Toast.LENGTH_SHORT).show();
                        savePrefDataP();

                    }

                    @Override
                    public void onSequenceStep(TapTarget lastTarget, boolean targetClicked) {
                        // Toast.makeText(getActivity(),"GREAT!",Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onSequenceCanceled(TapTarget lastTarget) {

                    }
                }).start();
    }

    private void savePrefDataP(){
        sharedPreferences=this.getSharedPreferences("boarding_pref_view_export_re",MODE_PRIVATE);
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putBoolean("IsFirstTimeVisit_view_export_re",true);
        editor.apply();
    }

    private boolean restorePrefDataP(){
        sharedPreferences = this.getSharedPreferences("boarding_pref_view_export_re",MODE_PRIVATE);
        return sharedPreferences.getBoolean("IsFirstTimeVisit_view_export_re",false);
    }


    private void setData() {
        parent.setVisibility(View.VISIBLE);
        layt.setVisibility(View.VISIBLE);
        note.setVisibility(View.GONE);
        User data=User.getCurrentUser();
        imageView.setVisibility(View.VISIBLE);

        if (!restorePrefDataP()){
            setShowcaseViewP();
        }


//        s_name.setText(name);
//        p_name.setText(f_name);
//        class_name.setText(classname);
//        roll.setText(id);
//        e_name.setText(exmnm);
//        school=User.getCurrentUser().getSchoolData().getName();
//        txtTitle.setText(school);


        if (name != null) {
            String title = getColoredSpanned("<b>"+"Name : "+"</b>", "#000000");
            String Name = getColoredSpanned(name, "#000800");
            s_name.setText(Html.fromHtml(title + " " + Name));
        }

        if (!mother_name.equalsIgnoreCase("")) {
            String title = getColoredSpanned("<b>"+"Mother's Name : "+"</b>", "#000000");
            String Name = getColoredSpanned(mother_name, "#000800");
            s_name_mother.setText(Html.fromHtml(title + " " + Name));
        }

        if (f_name != null) {
            String title = getColoredSpanned("<b>"+"Father's Name : "+"</b>", "#000000");
            String Name = getColoredSpanned(f_name, "#000800");
            p_name.setText(Html.fromHtml(title + " " + Name));
        }

//        if (classname != null) {
//            String title = getColoredSpanned("<b>"+"Class : "+"</b>", "#000000");
//            String Name = getColoredSpanned(classname+"-"+sectionname, "#000800");
//            class_name.setText(Html.fromHtml(title + " " + Name));
//        }
//
//        if (id != null) {
//            String title = getColoredSpanned("<b>"+"Admission No. : "+"</b>", "#000000");
//            String Name = getColoredSpanned(id, "#000800");
//            roll.setText(Html.fromHtml(title + " " + Name));
//        }
//
//        if (dob1 != null) {
//            String title = getColoredSpanned("<b>"+"Date of birth : "+"</b>", "#000000");
//            String Name = getColoredSpanned(Utils.getDateFormated(dob1), "#000800");
//            dob.setText(Html.fromHtml(title + " " + Name));
//        }


        String start="",end="",str=classname;


        if (str.equalsIgnoreCase("1st")){
            start="1";
            end="st";
        }else if (str.equalsIgnoreCase("2nd")){
            start="2";
            end="nd";
        }else if (str.equalsIgnoreCase("3rd")){
            start="3";
            end="rd";
        }else if (str.equalsIgnoreCase("4th")){
            start="4";
            end="th";
        }else if (str.equalsIgnoreCase("5th")){
            start="5";
            end="th";
        }else if (str.equalsIgnoreCase("6th")){
            start="6";
            end="th";
        }else if (str.equalsIgnoreCase("7th")){
            start="7";
            end="th";
        }else if (str.equalsIgnoreCase("8th")){
            start="8";
            end="th";
        }else if (str.equalsIgnoreCase("9th")){
            start="9";
            end="th";
        }else if (str.equalsIgnoreCase("10th")){
            start="10";
            end="th";
        }else {
            start=classname;
            // start="";
            end="";
        }


        if (classname != null) {
            String title = getColoredSpanned("<b>"+"Class : "+"</b>", "#000000");
            String Name = getColoredSpanned(start+"", "#000800");
            class_name.setText(Html.fromHtml(title + " " + Name));
            class_name1.setText(end);
            String title1 = getColoredSpanned("", "#000000");
            String Name1 = getColoredSpanned(""+sectionname, "#000800");
            class_name2.setText(Html.fromHtml(title1 + "" + Name1));

        }

        if (data.getId() != null) {
            String title = "";
            if (User.getCurrentUser().getSchoolData().getFi_school_id().equalsIgnoreCase("MSS2024")){
                title = getColoredSpanned("<b>"+"House No. : "+"</b>", "#000000");
            }else{
                title = getColoredSpanned("<b>"+"Admission No. : "+"</b>", "#000000");
            }
            String Name = getColoredSpanned(id, "#000800");
            roll.setText(Html.fromHtml(title + " " + Name));
        }


        String start1="",end1="",str1=Utils.getDayOnly(dob1);


        if (str1.equalsIgnoreCase("01")){
            start1="1";
            end1="st";
        }else if (str1.equalsIgnoreCase("02")){
            start1="2";
            end1="nd";
        }else if (str1.equalsIgnoreCase("03")){
            start1="3";
            end1="rd";
        }else if (str1.equalsIgnoreCase("04")){
            start1="4";
            end1="th";
        }else if (str1.equalsIgnoreCase("05")){
            start1="5";
            end1="th";
        }else if (str1.equalsIgnoreCase("06")){
            start1="6";
            end1="th";
        }else if (str1.equalsIgnoreCase("07")){
            start1="7";
            end1="th";
        }else if (str1.equalsIgnoreCase("08")){
            start1="8";
            end1="th";
        }else if (str1.equalsIgnoreCase("09")){
            start1="9";
            end1="th";
        }else if (str1.equalsIgnoreCase("31")){
            start1="31";
            end1="st";
        }else {
            start1=str1;
            // start="";
            end1="th";
        }




        if (dob1 != null) {
            String title = getColoredSpanned("<b>"+"DOB : "+"</b>", "#000000");
            String Name = getColoredSpanned(start1, "#000800");
            dob.setText(Html.fromHtml(title + " " + Name));
            dob11.setText(end1);
            String title1 = getColoredSpanned("", "#000000");
            String Name1 = getColoredSpanned(""+Utils.getMonthYearOnly(dob1), "#000800");
            dob2.setText(Html.fromHtml(title1 + "" + Name1));
        }

        e_name.setText(exmnm+"");

        p_date.setText(Utils.getDateFormated(date)+"");

        e_instruction.setText("Grading Scholastic Areas\n" +
                "Minimum 33% marks required for pass\n" +
                "Grade are awarded on 8-point grading scale as follows:");

        //  school=data.getSchoolData().getName();

        txtTitle.setText(school);

        if (User.getCurrentUser().getSchoolData().getFi_school_id().equalsIgnoreCase("MSS2024")){
            txtAffillated.setText("(Affiliated to CBSE - 1630194)");
        }if (User.getCurrentUser().getSchoolData().getFi_school_id().equalsIgnoreCase("WPS")){
            txtAffillated.setText("(Affiliated to CBSE New Delhi - 1630580)");
        }else {
            txtAffillated.setText("(Affiliated to CBSE, New Delhi)");
        }

        txtAdd.setText(address);

      //  final_totatMarks=totatMarks-gradeMarks;
      //  t_marks.setText(String.valueOf(totalScore)+"/"+String.valueOf(final_totatMarks));
      //  t_score.setText(String.valueOf(totalScore));


        if (fromdate != null) {
            String title = getColoredSpanned("<b>"+"Academic Year : "+"</b>", "#000000");
            String Name = getColoredSpanned(Utils.getYearOnlyNEW(fromdate)+"-"+Utils.getYearOnlyNEW(todate), "#000800");
            a_year.setText(Html.fromHtml(title + " " + Name));
        }

        if (exmnm.equalsIgnoreCase("PERIODIC TEST - 1") || exmnm.equalsIgnoreCase("PERIODIC TEST-1")){
            term.setText("Term - 1");
        }else if (exmnm.equalsIgnoreCase("PERIODIC TEST - 2" ) || exmnm.equalsIgnoreCase("PERIODIC TEST-2")){
            term.setText("Term - 2");
        }else if (exmnm.equalsIgnoreCase("PERIODIC TEST - 3" ) || exmnm.equalsIgnoreCase("PERIODIC TEST-3")){
            term.setText("Term - 3");
        }else{
            term.setText("Term - 1");
        }

        //   a_year.setText("Academic Year : "+acdemic.getStart_date()+"-"+acdemic.getEnd_date());

        if (profile!=null) {
            Utils.setImageUri(ReportCard.this,profile,std_profile);
            //Picasso.with(ReportCard.this).load(logo).placeholder(R.color.transparent).into(circleImgLogo);
            // w_logo.setColorFilter(ContextCompat.getColor(this, R.color.water_mark), android.graphics.PorterDuff.Mode.MULTIPLY);
        } else {
            Picasso.get().load(R.color.transparent).into(std_profile);
        }


        if (!logo.equalsIgnoreCase("")) {
            //  Utils.setImageUri(ReportCard.this,logo,w_logo);
            Picasso.get().load(logo).placeholder(R.color.transparent).into(circleImgLogo);
            // w_logo.setColorFilter(ContextCompat.getColor(this, R.color.water_mark), android.graphics.PorterDuff.Mode.MULTIPLY);

        } else {
            Picasso.get().load(R.color.transparent).into(w_logo);
        }

//        User data=User.getCurrentUser();

//
//

//        final_totatMarks=totatMarks-gradeMarks;
//        t_marks.setText(String.valueOf(totalScore)+"/"+String.valueOf(final_totatMarks));
//        t_score.setText(String.valueOf(totalScore));

//        acdemicde=Utility.getDateFormated(fromdate)+" to "+Utility.getDateFormated(todate);
//        a_year.setText(acdemicde);

//        double scoreeee = Double.parseDouble(t_score.getText().toString());
//        double markssss = Double.parseDouble(t_marks.getText().toString());
//
//        double percentage = (scoreeee / markssss) * 100;
//        NumberFormat nm = NumberFormat.getNumberInstance();
//        percentagees.setText(nm.format(percentage)+"%");
       //  txtTitle.setText((int) percentage);

     //   Toast.makeText(getApplicationContext(),User.getCurrentUser().getSchoolData().get(),Toast.LENGTH_LONG).show();

        if(User.getCurrentUser().getSchoolData().getFi_school_id().equalsIgnoreCase("WPS") ||
                User.getCurrentUser().getSchoolData().getFi_school_id().equalsIgnoreCase("SXYZ")||
                User.getCurrentUser().getSchoolData().getFi_school_id().equalsIgnoreCase("XYZ")){

            total_lyt.setVisibility(View.VISIBLE);
            percentage_lyt.setVisibility(View.VISIBLE);

            DecimalFormat df = new DecimalFormat("#.##");

            String formattedScore = df.format(dou_totalScore);
            String formattedMarks = df.format(dou_totatMarks);
//        final_totatMarks=totatMarks-gradeMarks;


//        double scoreeee = Double.parseDouble(t_score.getText().toString());
//        double markssss = Double.parseDouble(t_marks.getText().toString());

            double formattedPercentage = (dou_totalScore / dou_totatMarks) * 100;
            NumberFormat nm = NumberFormat.getNumberInstance();
            percentagees.setText(nm.format(formattedPercentage)+"%");
            // txtTitle.setText((int) percentage);

            // double formattedPercentage = Double.parseDouble(df.format(percentage));
            String grand_grade = "-";

            if (formattedPercentage >= 91) {
                grand_grade = "A+";
            } else if (formattedPercentage >= 81 && formattedPercentage <= 90.99) {
                grand_grade = "A";
            } else if (formattedPercentage >= 71 && formattedPercentage <= 80.99) {
                grand_grade = "B+";
            } else if (formattedPercentage >= 61 && formattedPercentage <= 70.99) {
                grand_grade = "B";
            } else if (formattedPercentage >= 51 && formattedPercentage <= 60.99) {
                grand_grade = "C+";
            } else if (formattedPercentage >= 41 && formattedPercentage <= 50.99) {
                grand_grade = "C";
            } else if (formattedPercentage >= 33 && formattedPercentage <= 40.99) {
                grand_grade = "D";
            } else if (formattedPercentage >= 0 && formattedPercentage <= 32.99) {
                grand_grade = "E";
            } else {
                grand_grade = "-";
            }

         //   System.out.println("Grade: " + grand_grade);


            t_marks.setText(String.valueOf(formattedScore)+"/"+String.valueOf(formattedMarks));
            t_score.setText(String.valueOf(grand_grade));

        }else{
         //   Toast.makeText(getApplicationContext(),"else",Toast.LENGTH_LONG).show();
            total_lyt.setVisibility(View.GONE);
            percentage_lyt.setVisibility(View.GONE);
        }

    }

    @OnClick(R.id.submit)
    public void submit() {
// create bitmap screen capture
        submit.startAnimation(animation);
        layt.setVisibility(View.GONE);
        note.setVisibility(View.GONE);
//        View v1 = getWindow().getDecorView().getRootView();
//        v1.setDrawingCacheEnabled(true);
        Bitmap bitmap = getBitmapFromView(scrollView, scrollView.getChildAt(0).getHeight(), scrollView.getChildAt(0).getWidth());
        // Bitmap bitmap = Bitmap.createBitmap(v1.getDrawingCache());
        // v1.setDrawingCacheEnabled(false);
        saveImageToGallery(bitmap);
    }


    @OnClick(R.id.export)
    public void dialog() {
        submit.startAnimation(animation);
        layt.setVisibility(View.GONE);
        note.setVisibility(View.GONE);
//        View v1 = getWindow().getDecorView().getRootView();
//        v1.setDrawingCacheEnabled(true);
        //     Bitmap bitmap = getBitmapFromView(scrollView, scrollView.getChildAt(0).getHeight(), scrollView.getChildAt(0).getWidth());
        // Bitmap bitmap = Bitmap.createBitmap(v1.getDrawingCache());
        // v1.setDrawingCacheEnabled(false);

// Assuming the getBitmapFromView method returns a Bitmap object

// Get the width and height of the desired capture area
        int captureWidth = scrollView.getWidth();
        int captureHeight = scrollView.getHeight();

// Get the visible portion of the ScrollView
        int visibleWidth = scrollView.getChildAt(0).getWidth();
        int visibleHeight = scrollView.getChildAt(0).getHeight();

// Adjust the capture width and height if necessary
        if (visibleWidth < captureWidth) {
            captureWidth = visibleWidth;
        }

        if (visibleHeight < captureHeight) {
            captureHeight = visibleHeight;
        }

// Capture the bitmap with adjusted parameters
        Bitmap bitmap = getBitmapFromView(scrollView, captureHeight, captureWidth);

        saveImageToGallery(bitmap);
    }



    //create bitmap from the ScrollView
    private Bitmap getBitmapFromView(View view, int height, int width) {
        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        Drawable bgDrawable = view.getBackground();
        if (bgDrawable != null)
            bgDrawable.draw(canvas);
        else
            canvas.drawColor(Color.WHITE);
        view.draw(canvas);
        return bitmap;
    }


    private void saveImageToGallery(Bitmap bitmap){

        OutputStream fos;
        long time= System.currentTimeMillis();
        try{

            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q){

                ContentResolver resolver = getContentResolver();
                ContentValues contentValues =  new ContentValues();
                contentValues.put(MediaStore.MediaColumns.DISPLAY_NAME, "report_card_img" +time+ ".jpg");
                contentValues.put(MediaStore.MediaColumns.MIME_TYPE, "image/jpeg");
                contentValues.put(MediaStore.MediaColumns.RELATIVE_PATH, Environment.DIRECTORY_PICTURES + File.separator + school);
                Uri imageUri = resolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, contentValues);

                fos = resolver.openOutputStream(Objects.requireNonNull(imageUri));


                Toast.makeText(this, "Image Saved", Toast.LENGTH_SHORT).show();

            }
            else{

//                // Save image to gallery
//                String savedImageURL = MediaStore.Images.Media.insertImage(getContentResolver(), bitmap, "Bird", "Image of bird");
//
//                // Parse the gallery image url to uri
//                Uri savedImageURI = Uri.parse(savedImageURL);

                //with folder name
                String imagesDir = Environment.getExternalStoragePublicDirectory(
                        Environment.DIRECTORY_PICTURES).toString() + File.separator + school;

                File file = new File(imagesDir);

                if (!file.exists()) {
                    file.mkdir();
                }

                File image = new File(imagesDir, "report_card_img" +time+ ".jpg");
                fos = new FileOutputStream(image);

                Toast.makeText(this, "Image saved to internal!!", Toast.LENGTH_SHORT).show();
                //resetOpTimes();

            }

            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            Objects.requireNonNull(fos);


        }catch(Exception e){

            Toast.makeText(this, "Image not saved \n" + e.toString(), Toast.LENGTH_SHORT).show();
        }
        layt.setVisibility(View.VISIBLE);
        // note.setVisibility(View.VISIBLE);
    }

}