package com.ultimate.ultimatesmartschool.Examination;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.ultimate.ultimatesmartschool.BeanModule.SectionBean;
import com.ultimate.ultimatesmartschool.BeanModule.Studentbean;
import com.ultimate.ultimatesmartschool.BeanModule.User;
import com.ultimate.ultimatesmartschool.ClassTest.Messageclass_adpter;
import com.ultimate.ultimatesmartschool.Homework.SectionnewAdapter;
import com.ultimate.ultimatesmartschool.Login.LoginActivity;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.StuGatePass.ClassBeans;
import com.ultimate.ultimatesmartschool.StudentAttendance.Spinner_stu_adapterGds;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class GDSUploadExamMrks extends AppCompatActivity {
    @BindView(R.id.imgBack)
    ImageView back;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.spinneracdmicyrsec)
    Spinner acadmicyrspin;
    @BindView(R.id.spinnerClasssec)Spinner classspin;
    @BindView(R.id.spinnerStudent)Spinner spinnerStudent;
    @BindView(R.id.spinnerexamsec)Spinner exmnamspinn;
    @BindView(R.id.recyclerViewsdetail)
    RecyclerView recyclerView;
    RecyclerView.LayoutManager layoutManager;
    @BindView(R.id.buttonnxtdetil)
    Button btnnextdt;
    @BindView(R.id.parent)
    RelativeLayout parent;
    @BindView(R.id.ggdetail)RelativeLayout fstlayout;
    @BindView(R.id.lllll)RelativeLayout scndlayout;
    @BindView(R.id.stunm)TextView stunm;
    @BindView(R.id.classnm)TextView clasname;
    @BindView(R.id.examnam)TextView examnaname;
    @BindView(R.id.acdmicyr)TextView academicyr;
    ArrayList<ClassBeans> classList = new ArrayList<>();
    //ArrayList<Student_msg_bean> stuList = new ArrayList<>();
    // Spinner_stu_adapter adapterstu;
    String classid = "";
    String stu_id;
    private ArrayList<Exambean> examList = new ArrayList<>();
    ArrayList<Sessionexambean> sessionlist = new ArrayList<>();
    private Exmnamespinadapter exmnamespinadapter;
    private Acadmicyrspinadpter acadmicyrspinadpter;
    private String examid="";
    private String acdemicde="";
    private String exmnm="";
    private Sessionexambean acdemic;
    String stuname;
    String classname;
    private int loaded = 0;
    ExamUploadMarksAdapter examUploadMarksAdapter;
    ArrayList<Examdetailbean> examdetaillist = new ArrayList<>();
    @BindView(R.id.submitexam)Button submitmarks;
    ArrayList<String> givenmrkslist;
    ArrayList<String> subjectlist;
    ArrayList<String> examdetailidist;
    ArrayList<String> marksid;
    String sturoll;
    @BindView(R.id.spinnersection)Spinner spinnersection;
    String sectionid="";
    String sectionname;
    ArrayList<SectionBean> sectionList = new ArrayList<>();
    private ArrayList<Studentbean> stuList;
    private Spinner_stu_adapterGds adapterstu;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_g_d_s_upload_exam_mrks);
        ButterKnife.bind(this);
        txtTitle.setText("Upload Marks");
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        scndlayout.setVisibility(View.GONE);
        fetchclasslist();
        fetchsection();
        fetchexamnamelist();
        fetchAcademicyearlist();


        layoutManager = new LinearLayoutManager(GDSUploadExamMrks.this);
        recyclerView.setLayoutManager(layoutManager);
        examUploadMarksAdapter = new ExamUploadMarksAdapter(examdetaillist, GDSUploadExamMrks.this);
        recyclerView.setAdapter(examUploadMarksAdapter);
    }

    private void fetchclasslist() {

        HashMap<String, String> params = new HashMap<String, String>();
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.CLASSLIST_URL, classapiCallback, this, params);
    }


    ApiHandler.ApiCallback classapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            if (error == null) {
                try {
                    classList = ClassBeans.parseClassArray(jsonObject.getJSONArray(Constants.CLASSDATA));
                    Messageclass_adpter adapter = new Messageclass_adpter(GDSUploadExamMrks.this, classList);
                    classspin.setAdapter(adapter);
                    classspin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                            if (i == 0) {

                                if (adapterstu != null) {
                                    stuList.clear();
                                    adapterstu.notifyDataSetChanged();
                                }
                            }
                            if (i > 0) {

                                classid = classList.get(i-1).getId();
                                classname=classList.get(i-1).getName();
                                stu_id="";

                                Log.e("classsid",classid);


                            }else{
                                classid="";
                            }

                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {

                Utils.showSnackBar(error.getMessage(), parent);
            }
        }
    };
    private void fetchsection() {
        HashMap<String, String> params = new HashMap<String, String>();
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.SECTIONLIST_URL, sectionapiCallback, this, params);
    }

    ApiHandler.ApiCallback sectionapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            if (error == null) {
                try {
                    sectionList = SectionBean.parseCommonArray(jsonObject.getJSONArray("hostelroom_data"));
                    SectionnewAdapter adapter = new SectionnewAdapter(GDSUploadExamMrks.this, sectionList);
                    spinnersection.setAdapter(adapter);
                    spinnersection.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if (i > 0) {
                                sectionid = sectionList.get(i - 1).getSection_id();
                                sectionname=sectionList.get(i - 1).getSection_name();
                                fetchStudent(classid,sectionid);
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Utils.showSnackBar(error.getMessage(), parent);
                if (error.getStatusCode() == 405) {
                    User.logout();
                    startActivity(new Intent(GDSUploadExamMrks.this, LoginActivity.class));
                    finish();
                }
            }
        }
    };

    private void fetchStudent(String classid,String sectionid) {


        HashMap<String, String> params = new HashMap<String, String>();
        params.put("class_id",classid);
        params.put("section_id",sectionid);
        Log.e("class_id",classid);
        Log.e("section_id",sectionid);
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.STUDENTLISTCLSSECWISE_URL, apiCallbackstudnt, this, params);
    }

    ApiHandler.ApiCallback apiCallbackstudnt = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            if (error == null) {
                try {
                    stuList = Studentbean.parseHWArray(jsonObject.getJSONArray("student_list"));
                    adapterstu = new Spinner_stu_adapterGds(GDSUploadExamMrks.this, stuList);
                    spinnerStudent.setAdapter(adapterstu);
                    spinnerStudent.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if (i > 0) {
                                stu_id = stuList.get(i-1).getId();
                                stuname=stuList.get(i-1).getName();
                                sturoll=stuList.get(i-1).getRoll_no();

                            } else {

                                stu_id = "";
                            }

                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {

            }
        }
    };

    private void fetchexamnamelist() {
        HashMap<String, String> params = new HashMap<>();

        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.addexam, apiCallback, this, params);

    }

    ApiHandler.ApiCallback apiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            if(error==null) {
                try {
                    JSONArray jsonArray = jsonObject.getJSONArray("examname_data");
                    examList = Exambean.parseexamArray(jsonArray);
                    exmnamespinadapter = new Exmnamespinadapter(GDSUploadExamMrks.this, examList);
                    exmnamspinn.setAdapter(exmnamespinadapter);
                    exmnamspinn.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            if (position > 0) {
                                examid = examList.get(position - 1).getId();
                                exmnm = examList.get(position - 1).getExamname();
                            } else {
                                examid = "";
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }else{
                Utils.showSnackBar(error.getMessage(), parent);
            }
        }
    };

    private void fetchAcademicyearlist() {
        HashMap<String, String> params = new HashMap<>();

        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.COLGACADEMICYAER_URL, apiCallbackacdemicyear, this, params);
        //ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.SESSION_URL, apiCallbackacdemicyear, this, params);
    }

    ApiHandler.ApiCallback apiCallbackacdemicyear = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            if(error==null) {
                try {
                    JSONArray jsonArray = jsonObject.getJSONArray("session_data");
                    sessionlist = Sessionexambean.parsesessionArray(jsonArray);
                    acadmicyrspinadpter = new Acadmicyrspinadpter(GDSUploadExamMrks.this, sessionlist);
                    acadmicyrspin.setAdapter(acadmicyrspinadpter);
                    acadmicyrspin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            if (position > 0) {
                                acdemic = sessionlist.get(position - 1);
                                acdemicde = sessionlist.get(position - 1).getStart_date() + sessionlist.get(position - 1).getEnd_date();

                            } else {
                                acdemic = null;
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }else{
                Utils.showSnackBar(error.getMessage(), parent);
            }
        }
    };

    @OnClick(R.id.buttonnxtdetil)
    public void nextbutton(){

        if (checkValid()) {
            fetchexamdetail();
            stunm.setText(stuname+"("+sturoll+")");
            clasname.setText(classname);
            examnaname.setText(exmnm);
            academicyr.setText(acdemicde);
            fstlayout.setVisibility(View.GONE);
            scndlayout.setVisibility(View.VISIBLE);
            submitmarks.setVisibility(View.VISIBLE);
        } else {
            fstlayout.setVisibility(View.VISIBLE);

        }


    }
    private boolean checkValid() {
        boolean valid = true;
        String errorMsg = null;
        if (classid.isEmpty() || classid == "") {
            valid = false;
            errorMsg = "Please select class";

        }
        else
        if (stu_id.isEmpty() || stu_id == "") {
            valid = false;
            errorMsg = "Please select Student";
        }
        else if (examid == null) {
            valid = false;
            errorMsg = "please select Exam name";

        } else if (acdemic == null) {
            valid = false;
            errorMsg = "please select Academic Year";
        }

        if (!valid) {
            Utils.showSnackBar(errorMsg, parent);
        }
        return valid;
    }
    public void fetchexamdetail(){
        String acdexm_year = acdemic.getStart_date() + acdemic.getEnd_date();
        if (loaded == 0) {
            ErpProgress.showProgressBar(this, "Please wait...");
        }
        loaded++;
        HashMap<String, String> params = new HashMap<>();
        params.put("stu_id",stu_id);
        params.put("class_id",classid);
        params.put("exam_id",examid);
        params.put("acadmic_year",acdexm_year);
        params.put("userid",User.getCurrentUser().getId());
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.Examdetailbyclassstu, apiCallbackexmdetail, this, params);


    }

    ApiHandler.ApiCallback apiCallbackexmdetail = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            if (error == null) {
                try {
                    if(examdetaillist!=null){
                        examdetaillist.clear();
                    }
                    JSONArray jsonArray = jsonObject.getJSONArray("examdetail_data");
                    examdetaillist = Examdetailbean.parseexamDetal_Array(jsonArray);
//
                    Log.d("TAG", "onDataFetched: " + examdetaillist.size());

                    examUploadMarksAdapter.setexmdetailList(examdetaillist);
                    examUploadMarksAdapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Utils.showSnackBar(error.getMessage(), parent);
                if (error.getStatusCode() == 405) {
                    User.logout();
                    startActivity(new Intent(GDSUploadExamMrks.this, LoginActivity.class));
                    finish();
                }
            }
        }
    };



    @OnClick(R.id.submitexam)
    public void save() {
        if (checkValid1()) {
            ErpProgress.showProgressBar(this,"please wait....");
            String acd_year = acdemic.getStart_date() + acdemic.getEnd_date();
            givenmrkslist = new ArrayList<>();
            subjectlist = new ArrayList<>();
            examdetailidist=new ArrayList<>();
            marksid=new ArrayList<>();
            for (int i = 0; i < examUploadMarksAdapter.getExamdetailmarksList().size(); i++) {
                givenmrkslist.add(examUploadMarksAdapter.getExamdetailmarksList().get(i).getGiven_marks());
                subjectlist.add(examUploadMarksAdapter.getExamdetailmarksList().get(i).getSubject_id());
                examdetailidist.add(examUploadMarksAdapter.getExamdetailmarksList().get(i).getExamdetailid());
                marksid.add(examUploadMarksAdapter.getExamdetailmarksList().get(i).getMarksid());
            }
            HashMap<String, String> params = new HashMap<String, String>();
            params.put("subject_id", String.valueOf(subjectlist));
            params.put("given_mark", String.valueOf(givenmrkslist));
            Log.e("given_mark",String.valueOf(givenmrkslist));
            params.put("stu_id",stu_id);
            params.put("examdetail_id", String.valueOf(examdetailidist));
            Log.e("examdetailid",String.valueOf(examdetailidist));
            params.put("marksid", String.valueOf(marksid));
            params.put("userid", User.getCurrentUser().getId());
            Log.e("marksid",String.valueOf(marksid));
            //params.put("exam_id",examid);
//            params.put("exm_date", String.valueOf(examdatelist));
//            params.put("exam_dur", String.valueOf(exmdurlist));
//            params.put("tmarks", String.valueOf(totalmrkslist));
//            params.put("pass_mark", String.valueOf(passmrkslist));
//            params.put("class_id", classid);
//            params.put("acd_id", String.valueOf(examid));
//
            // params.put("acd_year", acd_year);
            ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.UPLOAD_EXAM_MARKS, uploadexamCallback, this, params);
        }
    }
    ApiHandler.ApiCallback uploadexamCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            if (error == null) {
                Toast.makeText(GDSUploadExamMrks.this, "UPLOADED SUCEES", Toast.LENGTH_SHORT).show();
                finish();
            } else {
                Toast.makeText(GDSUploadExamMrks.this,error.getMessage(), Toast.LENGTH_SHORT).show();
                Log.e("16955", error.getMessage() + "");
            }
        }
    };

    private boolean checkValid1() {
        boolean valid = true;
        String errorMsg = null;
        int marks = 0;
        for (int i = 0; i < examUploadMarksAdapter.getExamdetailmarksList().size(); i++) {

            Examdetailbean sub = examUploadMarksAdapter.getExamdetailmarksList().get(i);
            try {
                marks=Integer.parseInt(String.valueOf(sub.getGiven_marks()));
            }catch (NumberFormatException ex){

                errorMsg = "Please enter the Marks or valid marks!";
            }

            if (marks >100 || sub.getGiven_marks() == null|| sub.getGiven_marks().isEmpty()) {
                valid = false;
                errorMsg = "please enter marks or valid marks!";
                break;
            }


        }
        if (!valid) {
            Utils.showSnackBar(errorMsg, parent);
        }
        return valid;
    }

}
