package com.ultimate.ultimatesmartschool.Examination;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;

import com.ultimate.ultimatesmartschool.BeanModule.User;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Utility.CommonProgress;

import butterknife.BindView;
import butterknife.ButterKnife;

public class GazetteWebviewActivity extends AppCompatActivity {
    @BindView(R.id.webviewabout)
    WebView myWebView;
    String link, s_key, staff_id, class_id, quizid;
    boolean loadingFinished = true;
    boolean redirect = false;
    @BindView(R.id.imgBack)
    ImageView imgback;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.txtSub)
    TextView txtSub;
    CommonProgress commonProgress;
    private String examid = "";
    private String dob1 = "";
    private String exmnm = "";
    private String classnm = "";
    private String mother_name = "", profile = "", sch_name = "", s_address = "";
    String name = "", f_name = "", id = "", classid = "", classname = "", sectionid = "", sectionname = "", session = "", todate = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gazette_webview);
        ButterKnife.bind(this);
        commonProgress = new CommonProgress(this);
        if (getIntent().getExtras() != null) {
            name = getIntent().getExtras().getString("name");
            // Toast.makeText(getApplicationContext(), name, Toast.LENGTH_SHORT).show();
            examid = getIntent().getExtras().getString("examid");
            classid = getIntent().getExtras().getString("class");
            classname = getIntent().getExtras().getString("class_name");
            sectionid = getIntent().getExtras().getString("sectionid");
            sectionname = getIntent().getExtras().getString("sectionname");
            session = getIntent().getExtras().getString("session");
            if(name.equalsIgnoreCase("BFPS")) {
                txtSub.setVisibility(View.GONE);
            }else {
                txtSub.setVisibility(View.VISIBLE);
                txtSub.setText(classname + "-" + sectionname);
            }
        }
        txtTitle.setText("Examination Gazette");
        staff_id = User.getCurrentUser().getId();
        s_key = User.getCurrentUser().getSchoolData().getFi_school_id();
        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder1 = new AlertDialog.Builder(GazetteWebviewActivity.this);
                builder1.setMessage("Are you sure, you want to exit? ");
                builder1.setCancelable(false);
                builder1.setPositiveButton(
                        "Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                finish();
                            }
                        });
                builder1.setNegativeButton(
                        "No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        });
                AlertDialog alert11 = builder1.create();
                alert11.show();
            }
        });

        myWebView.requestFocus();
        myWebView.getSettings().setDefaultTextEncodingName("utf-8");
        myWebView.setVerticalScrollBarEnabled(true);
        myWebView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }
        });
        myWebView.getSettings().setJavaScriptEnabled(true);
        myWebView.getSettings().setLightTouchEnabled(true);
        myWebView.getSettings().setBuiltInZoomControls(true);
        myWebView.setWebChromeClient(new WebChromeClient() {
                                         @Override
                                         public void onProgressChanged(WebView view, int newProgress) {
                                             if (newProgress < 100) {
                                                 commonProgress.show();
                                             }
                                             if (newProgress == 100) {
                                                 commonProgress.dismiss();
                                             }
                                         }


                                     }
        );


//        String link = "https://ultimatesolutiongroup.com/staff_panel/marks_report_app.php?s_key="+s_key+"&class="
//                + classid + "&section=" + sectionid+ "&exam=" + examid+ "&academic_year=" + session;
//                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(link)));

        String link = "";
        if (User.getCurrentUser().getSchoolData().getFi_school_id().equalsIgnoreCase("BFPS")) {
            link = "https://ultimatesolutiongroup.com/staff_panel/marks_report_bfps_app.php?s_key=BFPS";
        } else {

            link = "https://ultimatesolutiongroup.com/staff_panel/marks_report_app.php?s_key=" + s_key + "&class="
                    + classid + "&section=" + sectionid + "&exam=" + examid + "&academic_year=" + session;
        }


        Log.e("result", link);
        myWebView.loadUrl(link);
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // Check if the key event was the Back button and if there's history
        if ((keyCode == KeyEvent.KEYCODE_BACK) && myWebView.canGoBack()) {
            // myWebView.goBack();
            return true;
        }
        // If it wasn't the Back key or there's no web page history, bubble up to the default
        // system behavior (probably exit the activity)
        return super.onKeyDown(keyCode, event);
    }
}