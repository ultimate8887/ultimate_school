package com.ultimate.ultimatesmartschool.Examination;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.ultimate.ultimatesmartschool.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Instruction_Adapter extends RecyclerView.Adapter<Instruction_Adapter.Viewholder> {
    ArrayList<Instruction> sy_list;
    Context mContext;

    public Instruction_Adapter(ArrayList<Instruction> sy_list, Context mContext) {
        this.sy_list = sy_list;
        this.mContext = mContext;
    }


    @Override
    public Viewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.inst_lyt, parent, false);
        Viewholder viewholder = new Viewholder(view);
        return viewholder;

    }

    @Override
    public void onBindViewHolder(Viewholder holder, final int position) {
        Instruction mData=sy_list.get(position);
        String sno= String.valueOf(position+1);
        if (mData.getTitle() != null) {
            holder.txtText.setText(sno+". "+mData.getTitle());
        } else{
            holder.txtText.setText("Not Mentioned");
        }


    }

    @Override
    public int getItemCount() {
        return sy_list.size();
    }

    public void setSyllabusList(ArrayList<Instruction> sy_list) {
        this.sy_list = sy_list;
    }

    public class Viewholder extends RecyclerView.ViewHolder {
        @BindView(R.id.txtText)
        TextView txtText;


        public Viewholder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
