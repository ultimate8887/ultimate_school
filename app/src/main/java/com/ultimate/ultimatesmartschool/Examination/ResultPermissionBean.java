package com.ultimate.ultimatesmartschool.Examination;

import com.ultimate.ultimatesmartschool.BeanModule.ClassBean;
import com.ultimate.ultimatesmartschool.BeanModule.CommonBean;
import com.ultimate.ultimatesmartschool.Utility.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ResultPermissionBean {

    private static String ID = "id";
    /**
     * id : 2
     * name : NURSERY
     * school_id : 0
     * group_id : 9
     * order_by : 1
     */

    private String id;
    private String name;
    private String group_id;
    private String class_id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGroup_id() {
        return group_id;
    }

    public void setGroup_id(String group_id) {
        this.group_id = group_id;
    }

    public String getClass_id() {
        return class_id;
    }

    public void setClass_id(String class_id) {
        this.class_id = class_id;
    }

    public String getGroup_name() {
        return group_name;
    }

    public void setGroup_name(String group_name) {
        this.group_name = group_name;
    }

    public String getClass_name() {
        return class_name;
    }

    public void setClass_name(String class_name) {
        this.class_name = class_name;
    }

    public String getCreated_date() {
        return created_date;
    }

    public void setCreated_date(String created_date) {
        this.created_date = created_date;
    }

    public String getResult_status() {
        return result_status;
    }

    public void setResult_status(String result_status) {
        this.result_status = result_status;
    }

    private String group_name;
    private String class_name;
    private String created_date;
    private String result_status;





    public static ArrayList<ResultPermissionBean> parseClassArray(JSONArray arrayObj) {
        ArrayList list = new ArrayList<ResultPermissionBean>();
        try {
            for (int i = 0; i < arrayObj.length(); i++) {
                ResultPermissionBean p = parseClassObject(arrayObj.getJSONObject(i));
                if (p != null) {
                    list.add(p);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public static ResultPermissionBean parseClassObject(JSONObject jsonObject) {
        ResultPermissionBean casteObj = new ResultPermissionBean();
        try {
            if (jsonObject.has(ID)) {
                casteObj.setId(jsonObject.getString(ID));
            }

            if (jsonObject.has("group_id") && !jsonObject.getString("group_id").isEmpty() && !jsonObject.getString("group_id").equalsIgnoreCase("null")) {
                casteObj.setGroup_id(jsonObject.getString("group_id"));
            }

            if (jsonObject.has("class_id") && !jsonObject.getString("class_id").isEmpty() && !jsonObject.getString("class_id").equalsIgnoreCase("null")) {
                casteObj.setClass_id(jsonObject.getString("class_id"));
            }
            if (jsonObject.has("class_name") && !jsonObject.getString("class_name").isEmpty() && !jsonObject.getString("class_name").equalsIgnoreCase("null")) {
                casteObj.setClass_name(jsonObject.getString("class_name"));

            }  if (jsonObject.has("group_name") && !jsonObject.getString("group_name").isEmpty() && !jsonObject.getString("group_name").equalsIgnoreCase("null")) {
                casteObj.setGroup_name(jsonObject.getString("group_name"));

            }  if (jsonObject.has("created_date") && !jsonObject.getString("created_date").isEmpty() && !jsonObject.getString("created_date").equalsIgnoreCase("null")) {
                casteObj.setCreated_date(jsonObject.getString("created_date"));
            }

            if (jsonObject.has("result_status") && !jsonObject.getString("result_status").isEmpty() && !jsonObject.getString("result_status").equalsIgnoreCase("null")) {
                casteObj.setResult_status(jsonObject.getString("result_status"));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return casteObj;
    }




}
