package com.ultimate.ultimatesmartschool.Examination;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Hall_ticket_adapter extends RecyclerView.Adapter<Hall_ticket_adapter.Viewholder> {
    ArrayList<Examdetailbean> examdetaillist;
    Context context;
    int type;
    public Hall_ticket_adapter(ArrayList<Examdetailbean> examdetaillist, Context context, int type) {
        this.context=context;
        this.examdetaillist=examdetaillist;
        this.type=type;

    }

    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.admitcard_lay_new, parent, false);
        Viewholder viewholder = new Viewholder(view);
        return viewholder;
    }

    @Override
    public void onBindViewHolder(@NonNull Viewholder holder, int position) {

        Examdetailbean mData=examdetaillist.get(position);

        holder.subject.setText((CharSequence) examdetaillist.get(position).getSubject_name());
        holder.date.setText(Utils.getDateFormated(examdetaillist.get(position).getExam_date()));
        holder.day.setText(Utils.getDateOnlyNew(examdetaillist.get(position).getExam_date()));
//        String title1 = getColoredSpanned("", "#383737");
//        String Name1 = getColoredSpanned(examdetaillist.get(position).getExam_duration(), "#5A5C59");
//        String t1 = getColoredSpanned("Hour", "#5A5C59");
//        holder.day.setText(Html.fromHtml(title1 + " " + Name1 + " " + t1));

        String title = getColoredSpanned("", "#000800");
        String Name = getColoredSpanned(examdetaillist.get(position).getExam_duration(), "#000800");
        holder.time.setText(Html.fromHtml(title + " " + Name));
        //  holder.exmfullmrks.setText("Total Marks:" + " " + examdetaillist.get(position).getTotal_marks());
        // holder.exmpassmrksss.setText("Pass Marks:" + " "+ examdetaillist.get(position).getPass_marks());
        String title2 = getColoredSpanned("", "#383737");
        String Name2 = getColoredSpanned(examdetaillist.get(position).getPass_marks(), "#5A5C59");
        holder.sNo.setText(String.valueOf(position+1));

    }

    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }

    @Override
    public int getItemCount() {
        return examdetaillist.size();
    }

    public void setexmdetailList(ArrayList<Examdetailbean> examdetaillist) {
        this.examdetaillist = examdetaillist;
    }

    public class Viewholder extends RecyclerView.ViewHolder {
        //        @BindView(R.id.textsubjectnm)
//        TextView subname;
//        @BindView(R.id.textdateofexm)TextView doe;
//        @BindView(R.id.textexamduration)TextView textexamduration;
//        @BindView(R.id.exmfullmrks)TextView exmfullmrks;
//        @BindView(R.id.exmpassmrksss)TextView exmpassmrksss;
//        @BindView(R.id.exmobtainmrksss)TextView exmobtainmrksss;
//        @BindView(R.id.txt3)TextView txt2;
//        @BindView(R.id.txt4)TextView txt4;
        @BindView(R.id.sNo)
        TextView sNo;
        @BindView(R.id.date)TextView date;
        @BindView(R.id.day)TextView day;
        @BindView(R.id.subject)TextView subject;
        @BindView(R.id.time)TextView time;
        public Viewholder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }

}
