package com.ultimate.ultimatesmartschool.Examination;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.Request;
import com.ultimate.ultimatesmartschool.BeanModule.ClassBean;
import com.ultimate.ultimatesmartschool.BeanModule.CommonBean;
import com.ultimate.ultimatesmartschool.BeanModule.User;
import com.ultimate.ultimatesmartschool.Login.LoginActivity;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class Exam_Detail_Activity extends AppCompatActivity {
    @BindView(R.id.imgBack)
    ImageView back;
    @BindView(R.id.spinneracdmicyrsec)
    Spinner acadmicyrspin;
    @BindView(R.id.spinnerClasssec)Spinner classspin;
    @BindView(R.id.spinnerGroupsec)Spinner groupspin;
    @BindView(R.id.spinnerexamsec)Spinner exmnamspinn;
    @BindView(R.id.recyclerViewsdetail)
    RecyclerView recyclerView;
    RecyclerView.LayoutManager layoutManager;
    @BindView(R.id.buttonnxtdetil)
    Button btnnextdt;
    @BindView(R.id.exmdetal)
    RelativeLayout parent;
    @BindView(R.id.ggdetail)RelativeLayout fstlayout;
    @BindView(R.id.lllll)RelativeLayout scndlayout;
    @BindView(R.id.groupnm)
    TextView groupname;
    @BindView(R.id.classnm)TextView classname;
    @BindView(R.id.examnam)TextView examnaname;
    @BindView(R.id.acdmicyr)TextView academicyr;
    private ArrayList<CommonBean> groupList = new ArrayList<>();
    public ArrayList<ClassBean> classList = new ArrayList<>();
    private ArrayList<Exambean> examList = new ArrayList<>();
    ArrayList<Sessionexambean> sessionlist = new ArrayList<>();
    ArrayList<Examdetailbean> examdetaillist = new ArrayList<>();
    private String groupid = "";
    private String classid = "";
    private String examid="";
    private String acdemicde="";
    private String exmnm="";
    private String grpnm="";
    private String clssnm="";
    private ClassAdapter classadapter;
    private Exmnamespinadapter exmnamespinadapter;
    private Acadmicyrspinadpter acadmicyrspinadpter;
    private Examdetailadapter examdetailadapter;
    private int loaded = 0;
    private Sessionexambean acdemic;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exam__detail_);
        ButterKnife.bind(this);
        txtTitle.setText("Examination");
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        scndlayout.setVisibility(View.GONE);
        fetchgrouplist();
        fetchexamnamelist();
        fetchAcademicyearlist();
        layoutManager = new LinearLayoutManager(Exam_Detail_Activity.this);
        recyclerView.setLayoutManager(layoutManager);
        examdetailadapter = new Examdetailadapter(examdetaillist, Exam_Detail_Activity.this);
        recyclerView.setAdapter(examdetailadapter);
    }

    private void fetchgrouplist() {
        ErpProgress.showProgressBar(this, "Please wait...");
        HashMap<String, String> params = new HashMap<String, String>();
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.GROUP_LIST_URL, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                ErpProgress.cancelProgressBar();
                if (error == null) {


                    try {
                        groupList.clear();
                        groupList = CommonBean.parseCommonArray(jsonObject.getJSONArray(Constants.GROUP_DATA));
                        final GroupSpinnerAdapter adapter = new GroupSpinnerAdapter(Exam_Detail_Activity.this, groupList,0);
                        groupspin.setAdapter(adapter);
                        groupspin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                if (position > 0) {
                                    groupid = groupList.get(position - 1).getId();
                                    grpnm=groupList.get(position-1).getName();

                                    classid = "";
                                    fetchClasslist(groupid);
                                } else {
                                    if (classadapter != null) {
                                        classList.clear();
                                        classid = "";
                                        groupid = "";
                                        classadapter.notifyDataSetChanged();
                                    }
                                }
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }else{
                    Utils.showSnackBar(error.getMessage(), parent);
                }
            }
        },this,params);
    }

    private void fetchClasslist(String groupid) {

        HashMap<String, String> params = new HashMap<String, String>();
        params.put(Constants.ID, groupid);
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.GROUPCLASSLIST_URL, classapiCallback, this, params);
    }
    ApiHandler.ApiCallback classapiCallback=new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            if (error == null) {
                try {
                    classList = ClassBean.parseClassArray(jsonObject.getJSONArray(Constants.CLASSDATA));
                    classadapter = new ClassAdapter(Exam_Detail_Activity.this, classList);
                    classspin.setAdapter(classadapter);
                    classspin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            if (position > 0) {
                                classid = classList.get(position - 1).getId();
                                clssnm=classList.get(position-1).getName();
                                //fetchsujectname(classid);
                            } else {
                                classid = "";
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }else{
                Utils.showSnackBar(error.getMessage(), parent);
            }
        }
    };


    private void fetchexamnamelist() {
        HashMap<String, String> params = new HashMap<>();

        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.addexam, apiCallback, this, params);

    }

    ApiHandler.ApiCallback apiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            if(error==null) {
                try {
                    JSONArray jsonArray = jsonObject.getJSONArray("examname_data");
                    examList = Exambean.parseexamArray(jsonArray);
                    exmnamespinadapter = new Exmnamespinadapter(Exam_Detail_Activity.this, examList);
                    exmnamspinn.setAdapter(exmnamespinadapter);
                    exmnamspinn.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            if (position > 0) {
                                examid = examList.get(position - 1).getId();
                                exmnm = examList.get(position - 1).getExamname();
                            } else {
                                examid = "";
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }else{
                Utils.showSnackBar(error.getMessage(), parent);
            }
        }
    };


    private void fetchAcademicyearlist() {
        HashMap<String, String> params = new HashMap<>();

        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.COLGACADEMICYAER_URL, apiCallbackacdemicyear, this, params);
        //ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.SESSION_URL, apiCallbackacdemicyear, this, params);
    }

    ApiHandler.ApiCallback apiCallbackacdemicyear = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            if(error==null) {
                try {
                    JSONArray jsonArray = jsonObject.getJSONArray("session_data");
                    sessionlist = Sessionexambean.parsesessionArray(jsonArray);
                    acadmicyrspinadpter = new Acadmicyrspinadpter(Exam_Detail_Activity.this, sessionlist);
                    acadmicyrspin.setAdapter(acadmicyrspinadpter);
                    acadmicyrspin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            if (position > 0) {
                                acdemic = sessionlist.get(position - 1);
                                acdemicde = sessionlist.get(position - 1).getStart_date() + sessionlist.get(position - 1).getEnd_date();

                            } else {
                                acdemic = null;
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }else{
                Utils.showSnackBar(error.getMessage(), parent);
            }
        }
    };
    @OnClick(R.id.buttonnxtdetil)
    public void nextbutton(){

        if (checkValid()) {
            fetchexamdetail();
            groupname.setText(grpnm);
            classname.setText(clssnm);
            examnaname.setText(exmnm);
            academicyr.setText(acdemicde);
            fstlayout.setVisibility(View.GONE);
            scndlayout.setVisibility(View.VISIBLE);
        } else {
            fstlayout.setVisibility(View.VISIBLE);

        }


    }


    public void fetchexamdetail(){
        String acdexm_year = acdemic.getStart_date() + acdemic.getEnd_date();
        if (loaded == 0) {
            ErpProgress.showProgressBar(this, "Please wait...");
        }
        loaded++;
        HashMap<String, String> params = new HashMap<>();
        params.put("grp_id",groupid);
        params.put("class_id",classid);
        params.put("exam_id",examid);
        params.put("acadmic_year",acdexm_year);
        //  params.put("userid",User.getCurrentUser().getId());
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.Examdetail, apiCallbackexmdetail, this, params);


    }

    ApiHandler.ApiCallback apiCallbackexmdetail = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            if (error == null) {
                try {
                    if(examdetaillist!=null){
                        examdetaillist.clear();
                    }
                    JSONArray jsonArray = jsonObject.getJSONArray("examdetail_data");
                    examdetaillist = Examdetailbean.parseexamDetal_Array(jsonArray);
//
                    Log.d("TAG", "onDataFetched: " + examdetaillist.size());

                    examdetailadapter.setexmdetailList(examdetaillist);
                    examdetailadapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Utils.showSnackBar(error.getMessage(), parent);
                if (error.getStatusCode() == 405) {
                    User.logout();
                    startActivity(new Intent(Exam_Detail_Activity.this, LoginActivity.class));
                    finish();
                }
            }
        }
    };







    private boolean checkValid() {
        boolean valid = true;
        String errorMsg = null;
        if (groupid.isEmpty() || groupid == "") {
            valid = false;
            errorMsg = "Please select Group";
        } else if (classid.isEmpty() || classid == "") {
            valid = false;
            errorMsg = "Please select class";

        } else if (examid == null) {
            valid = false;
            errorMsg = "please select Exam name";

        } else if (acdemic == null) {
            valid = false;
            errorMsg = "please select Academic Year";
        }

        if (!valid) {
            Utils.showSnackBar(errorMsg, parent);
        }
        return valid;
    }
}
