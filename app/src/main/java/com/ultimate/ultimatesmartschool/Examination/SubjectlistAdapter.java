package com.ultimate.ultimatesmartschool.Examination;

import android.content.Context;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.ultimate.ultimatesmartschool.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SubjectlistAdapter extends RecyclerView.Adapter<SubjectlistAdapter.Viewholder> {

    private ArrayList<SubjectListbean> subjectList;
    Context scheduleActivity;
    TimeandDate timeandDate;

    public SubjectlistAdapter(ArrayList<SubjectListbean> subjectList, Context scheduleActivity, TimeandDate timeandDate) {
        this.subjectList = subjectList;
        this.scheduleActivity = scheduleActivity;
        this.timeandDate = timeandDate;
    }



    @Override
    public Viewholder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.subjectada_lay, viewGroup, false);
        SubjectlistAdapter.Viewholder viewholder = new SubjectlistAdapter.Viewholder(view, new MyCustomEditTextListener(), new MyCustomEditTextListenerexam(), new passMarksListner());
        return viewholder;
    }

    @Override
    public void onBindViewHolder(Viewholder viewholder, final int i) {

     //   viewholder.subject.setText(subjectList.get(i).getName());

        String title1 = getColoredSpanned("Subject Name:- ", "#5A5C59");
        String Name1 = getColoredSpanned(subjectList.get(i).getName(), "#1C8B3B");
        String t1 = getColoredSpanned("", "#5A5C59");
        viewholder.subject.setText(Html.fromHtml(title1 + " " + Name1 + " " + t1));


        viewholder.myCustomEditTextListener.updatePosition(viewholder.getAdapterPosition());
        viewholder.duration.getEditText().setText(subjectList.get(viewholder.getAdapterPosition()).getExam_dur());
        viewholder.myCustomEditTextListenerexam.updatePositionsec(viewholder.getAdapterPosition());
        viewholder.marks.getEditText().setText(subjectList.get(viewholder.getAdapterPosition()).getExam_markS());
        viewholder.myCustomerEditListnerpassmk.updatePositionthird(viewholder.getAdapterPosition());
        viewholder.passmrks.getEditText().setText(subjectList.get(viewholder.getAdapterPosition()).getPass_marks());
        if (subjectList.get(i).getExam_Date() == null) {
            viewholder.date.setText("Examination Date");
        } else {
            viewholder.date.setText(subjectList.get(i).getExam_Date());
        }
        viewholder.date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                timeandDate.ondatecallback(subjectList.get(i));
            }
        });
    }

    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }


    @Override
    public int getItemCount() {
        return subjectList.size();
    }

    public void setsubliat(ArrayList<SubjectListbean> subjectList) {
        this.subjectList = subjectList;
    }

    public ArrayList<SubjectListbean> getSubjectList() {
        return subjectList;
    }


    public class Viewholder extends RecyclerView.ViewHolder {
        @BindView(R.id.txtsubjname)
        TextView subject;
        @BindView(R.id.examdate)
        TextView date;
        @BindView(R.id.exmdur)
        TextInputLayout duration;
        @BindView(R.id.totalmarks)
        TextInputLayout marks;
        @BindView(R.id.passmrk)
        TextInputLayout passmrks;
        public MyCustomEditTextListener myCustomEditTextListener;
        public MyCustomEditTextListenerexam myCustomEditTextListenerexam;
        public passMarksListner myCustomerEditListnerpassmk;

        public Viewholder(View itemView, MyCustomEditTextListener myCustomEditTextListener, MyCustomEditTextListenerexam myCustomEditTextListenerexam, passMarksListner myCustomerEditListnerpassmk) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            this.myCustomEditTextListener = myCustomEditTextListener;
            this.duration.getEditText().addTextChangedListener(myCustomEditTextListener);
            this.myCustomEditTextListenerexam = myCustomEditTextListenerexam;
            this.marks.getEditText().addTextChangedListener(myCustomEditTextListenerexam);
            this.myCustomerEditListnerpassmk = myCustomerEditListnerpassmk;
            this.passmrks.getEditText().addTextChangedListener(myCustomerEditListnerpassmk);
        }
    }

    public interface TimeandDate {
        public void ondatecallback(SubjectListbean subjectListbean);
    }


    private class MyCustomEditTextListener implements TextWatcher {
        private int position;

        public void updatePosition(int position) {
            this.position = position;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            subjectList.get(position).setExam_dur(String.valueOf(s));
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    }

    private class MyCustomEditTextListenerexam implements TextWatcher {
        private int positions;

        public void updatePositionsec(int positions) {
            this.positions = positions;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            subjectList.get(positions).setExam_markS(String.valueOf(s));
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    }

    private class passMarksListner implements TextWatcher {
        private int position;

        public void updatePositionthird(int position) {
            this.position = position;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            subjectList.get(position).setPass_marks(String.valueOf(s));

        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    }
}
