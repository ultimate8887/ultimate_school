package com.ultimate.ultimatesmartschool.Examination;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.textfield.TextInputLayout;
import com.ultimate.ultimatesmartschool.BeanModule.User;
import com.ultimate.ultimatesmartschool.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class UploadMarksSubWiseAdapter extends RecyclerView.Adapter<UploadMarksSubWiseAdapter.ViewHolder> {
        Context context;
        static String k;
        private ArrayList<ResultExamListSubBean> MainImageUploadInfoList;
        // private ad m;

    public UploadMarksSubWiseAdapter(Context context, ArrayList<ResultExamListSubBean> MainImageUploadInfoList) {

            this.MainImageUploadInfoList = MainImageUploadInfoList;
            this.context = context;
            //this.m = m1;
        }

        @Override
        public UploadMarksSubWiseAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.studsubwisemark_list_lyt, parent, false);

            UploadMarksSubWiseAdapter.ViewHolder viewHolder = new UploadMarksSubWiseAdapter.ViewHolder(view,new MyCstmEditTextListener());

            return viewHolder;
        }


        @Override
        public void onBindViewHolder(final UploadMarksSubWiseAdapter.ViewHolder holder, final int position) {

            holder.studentsnames.setText(MainImageUploadInfoList.get(position).getName() + "(" + MainImageUploadInfoList.get(position).getId() + ")");

            holder.myCustomEditTextListener.updatePosition(holder.getAdapterPosition());
            holder.studentsnumber.getEditText().setText(MainImageUploadInfoList.get(holder.getAdapterPosition()).getMarks());
            //  holder.studentsnumber.setText(MainImageUploadInfoList.get(holder.getAdapterPosition()).getMarks());
            if(MainImageUploadInfoList.get(position).getMarks()!=null){

                holder.exmobtainmrksss.setVisibility(View.VISIBLE);
                //holder.exmobtainmrksss.setText("Obtained Marks: "+MainImageUploadInfoList.get(position).getExammarks());
                holder.exmobtainmrksss.setText("Obtained Marks: "+MainImageUploadInfoList.get(position).getMarks());
                holder.studentsnumber.setHint("Add marks ?");
                //holder.studentsnumber.setText(MainImageUploadInfoList.get(position).getExammarks());
            }else{
                holder.exmobtainmrksss.setVisibility(View.GONE);

            }

            holder.studentsnumber.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //  m.methodcall(MainImageUploadInfoList.get(position), k);
                }
            });
        }

        @Override
        public int getItemCount() {

            return MainImageUploadInfoList.size();
        }

        public void setStudentList(ArrayList<ResultExamListSubBean> MainImageUploadInfoList) {
            this.MainImageUploadInfoList = MainImageUploadInfoList;
        }

        public ArrayList<ResultExamListSubBean> getStudentList() {
            return MainImageUploadInfoList;
        }

        static class ViewHolder extends RecyclerView.ViewHolder {
            public TextView studentsnames;
            public TextInputLayout studentsnumber;

            @BindView(R.id.exmobtainmrksss)TextView exmobtainmrksss;
            public MyCstmEditTextListener myCustomEditTextListener;
            public ViewHolder(View itemView, MyCstmEditTextListener myCustomEditTextListener) {
                super(itemView);
                ButterKnife.bind(this, itemView);
                studentsnames = (TextView) itemView.findViewById(R.id.studentnames);
                studentsnumber = (TextInputLayout) itemView.findViewById(R.id.studentsnumber);
                this.myCustomEditTextListener = myCustomEditTextListener;
                this.studentsnumber.getEditText().addTextChangedListener(myCustomEditTextListener);

            }

        }

//    public interface ad {
//        public void methodcall(ResultTestListBean obj, String k);
//    }

        private class MyCstmEditTextListener implements TextWatcher {
            private int position;

            public void updatePosition(int position) {
                this.position = position;
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {



                MainImageUploadInfoList.get(position).setMarks(String.valueOf(s));
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        }
}
