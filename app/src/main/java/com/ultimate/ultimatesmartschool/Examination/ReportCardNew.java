package com.ultimate.ultimatesmartschool.Examination;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetSequence;
import com.github.chrisbanes.photoview.PhotoView;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.squareup.picasso.Picasso;
import com.ultimate.ultimatesmartschool.BeanModule.ClassBean;
import com.ultimate.ultimatesmartschool.BeanModule.SectionBean;
import com.ultimate.ultimatesmartschool.BeanModule.Studentbean;
import com.ultimate.ultimatesmartschool.Homework.SectionnewAdapter;
import com.ultimate.ultimatesmartschool.MSG_NEW.Fragments.Stu_adapterGds;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.CommonProgress;
import com.ultimate.ultimatesmartschool.Utility.Constants;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ReportCardNew extends AppCompatActivity {

    CommonProgress commonProgress;
    Animation animation;
    String reportcard="",s_sign="",a_sign="",school="";
    @BindView(R.id.imgBackmsg)
    ImageView back;
    @BindView(R.id.image)
    PhotoView image;
    SharedPreferences sharedPreferences;
    @BindView(R.id.export)
    FloatingActionButton imageView;
    @BindView(R.id.textNorecord)
    TextView textNorecord;
    @BindView(R.id.txtTitle)
    TextView txttitle;
    @BindView(R.id.dialog)
    ImageView dialog;
    @BindView(R.id.spinnerStudent)
    Spinner spinnerStudent;
    private ArrayList<Studentbean> stuList= new ArrayList<>();
    private Stu_adapterGds adapterstu;

    BottomSheetDialog mBottomSheetDialog;
    ArrayList<ClassBean> classList = new ArrayList<>();
    Spinner spinnersection;
    Spinner spinnerClass;
    Spinner spinnersubject;
    String sub_id= "", sub_name= "" ,sectionid="",sectionname="",classid = "",className = "";
    int check=0;
    String stu_id="";
    ArrayList<SectionBean> sectionList = new ArrayList<>();

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_card_new);
        ButterKnife.bind(this);

        animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.btn_blink_animation);
        commonProgress=new CommonProgress(this);
        openDialog();

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                back.startAnimation(animation);
                finish();
            }
        });
    }
    @RequiresApi(api = Build.VERSION_CODES.N)
    private void openDialog() {

        mBottomSheetDialog = new BottomSheetDialog(this);
        final View sheetView = getLayoutInflater().inflate(R.layout.search_dialog, null);
        mBottomSheetDialog.setContentView(sheetView);
        mBottomSheetDialog.setCancelable(false);

        spinnerClass=(Spinner) sheetView.findViewById(R.id.spinnerClass);
        spinnersection=(Spinner) sheetView.findViewById(R.id.spinnersection);
        spinnersubject=(Spinner) sheetView.findViewById(R.id.spinnersubject);
        ImageView close=(ImageView) sheetView.findViewById(R.id.close);
        TextView txtSetup=(TextView) sheetView.findViewById(R.id.txtSetup);
        txtSetup.setText("View Student-Wise \nReport-card");
        fetchClass();
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //assignData();
                if (check==0) {
                    finish();
                    mBottomSheetDialog.dismiss();
                }else {
                    mBottomSheetDialog.dismiss();
                }
            }
        });


        Button btnYes= (Button) sheetView.findViewById(R.id.btnYes);


        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (classid.equalsIgnoreCase(""))  {
                    Toast.makeText(ReportCardNew.this,"Kindly Select Class!", Toast.LENGTH_SHORT).show();
                }  else if (sectionid.equalsIgnoreCase("")){
                    Toast.makeText(ReportCardNew.this,"Kindly Select Class Section!", Toast.LENGTH_SHORT).show();
                }else {
                    check++;
                    fetchStudent(classid,sectionid);
                    setCommonData();
                }

            }
        });
        mBottomSheetDialog.show();

    }
    private void setCommonData() {

        txttitle.setText(className+"("+sectionname+")");
        // txtSub.setText(sub_name);
    }
    private void fetchStudent(String classid,String sectionid) {
        commonProgress.show();
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("class_id",classid);
        params.put("section_id",sectionid);
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.STUDENTLISTCLSSECWISE_URL, apiCallbackstudnt, this, params);
    }

    ApiHandler.ApiCallback apiCallbackstudnt = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            mBottomSheetDialog.dismiss();
            commonProgress.dismiss();
            if (error == null) {
                try {
                    stuList = Studentbean.parseHWArray(jsonObject.getJSONArray("student_list"));
                    adapterstu = new Stu_adapterGds(ReportCardNew.this, stuList);
                    spinnerStudent.setAdapter(adapterstu);
                    spinnerStudent.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                            if (i > 0) {
                                stu_id = stuList.get(i - 1).getId();
                                userprofile(stu_id);
                            } else {
                                stu_id = "";
                                imageView.setVisibility(View.GONE);
                                Picasso.get().load(R.color.transparent).into(image);
                                textNorecord.setText("No student selected");
                                textNorecord.setVisibility(View.VISIBLE);

                            }

                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {

            }
        }
    };

    @RequiresApi(api = Build.VERSION_CODES.N)
    @OnClick(R.id.dialog)
    public void dialogggg() {
        openDialog();
    }

    private void fetchClass() {
        // ErpProgress.showProgressBar(getActivity(), "Please wait...");
        commonProgress.show();
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("check","inch");
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.CLASSLIST_URL, classapiCallback, getApplicationContext(), params);
    }

    ApiHandler.ApiCallback classapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            commonProgress.dismiss();
            if (error == null) {
                try {
                    classList = ClassBean.parseClassArray(jsonObject.getJSONArray(Constants.CLASSDATA));
                    ClassAdapter adapter = new ClassAdapter(getApplicationContext(), classList);
                    spinnerClass.setAdapter(adapter);
                    spinnerClass.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if (i > 0) {
                                sub_id= "";
                                sub_name= "" ;
                                sectionid="";
                                sectionname="";
                                classid = "";
                                className = "";
                                classid = classList.get(i - 1).getId();
                                className= classList.get(i - 1).getName();
                                fetchsection();
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {
                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_LONG).show();
                //  Utils.showSnackBar(error.getMessage(), view.findViewById(R.id.parent));
            }
        }
    };

    private void fetchsection() {
        HashMap<String, String> params = new HashMap<String, String>();
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.SECTIONLIST_URL, sectionapiCallback, getApplicationContext(), params);
    }

    ApiHandler.ApiCallback sectionapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            if (error == null) {
                try {
                    sectionList = SectionBean.parseCommonArray(jsonObject.getJSONArray("hostelroom_data"));
                    SectionnewAdapter adapter = new SectionnewAdapter(getApplicationContext(), sectionList);
                    spinnersection.setAdapter(adapter);
                    spinnersection.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if (i > 0) {
                                sectionid = sectionList.get(i - 1).getSection_id();
                                sectionname=sectionList.get(i - 1).getSection_name();
                                //  fetchStudentList(date);
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_LONG).show();

            }
        }
    };


    private void setShowcaseViewP() {
        new TapTargetSequence(this)
                .targets(TapTarget.forView(imageView,"Export Report Card!","Tap the download button to download Report Card.")
                        .outerCircleColor(R.color.light)
                        .outerCircleAlpha(0.96f)
                        .targetCircleColor(R.color.white)
                        .titleTextSize(20)
                        .titleTextColor(R.color.white)
                        .descriptionTextSize(14)
                        .descriptionTextColor(R.color.black)
                        .textColor(R.color.black)
                        .textTypeface(Typeface.SANS_SERIF)
                        .dimColor(R.color.black)
                        .drawShadow(true)
                        .cancelable(false)
                        .tintTarget(true)
                        .transparentTarget(true)
                        .targetRadius(40)).listener(new TapTargetSequence.Listener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onSequenceFinish() {
                // Toast.makeText(getApplicationContext(),"Sequence Finished",Toast.LENGTH_SHORT).show();
                savePrefDataP();
            }

            @Override
            public void onSequenceStep(TapTarget lastTarget, boolean targetClicked) {
                // Toast.makeText(getActivity(),"GREAT!",Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSequenceCanceled(TapTarget lastTarget) {

            }
        }).start();
    }

    private void savePrefDataP(){
        sharedPreferences=this.getSharedPreferences("boarding_pref_view_export_re",MODE_PRIVATE);
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putBoolean("IsFirstTimeVisit_view_export_re",true);
        editor.apply();
    }

    private boolean restorePrefDataP(){
        sharedPreferences = this.getSharedPreferences("boarding_pref_view_export_re",MODE_PRIVATE);
        return sharedPreferences.getBoolean("IsFirstTimeVisit_view_export_re",false);
    }

    @OnClick(R.id.export)
    public void dialog() {
        BitmapDrawable bitmapDrawable = (BitmapDrawable) image.getDrawable();
        Bitmap bitmap = bitmapDrawable.getBitmap();
        saveImageToGallery(bitmap);

    }



    private void saveImageToGallery(Bitmap bitmap){

        OutputStream fos;

        try{

            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q){

                ContentResolver resolver = this.getContentResolver();
                ContentValues contentValues =  new ContentValues();
                contentValues.put(MediaStore.MediaColumns.DISPLAY_NAME, "report_card_img" + ".jpg");
                contentValues.put(MediaStore.MediaColumns.MIME_TYPE, "image/jpeg");
                contentValues.put(MediaStore.MediaColumns.RELATIVE_PATH, Environment.DIRECTORY_PICTURES + File.separator + school);
                Uri imageUri = resolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, contentValues);

                fos = resolver.openOutputStream(Objects.requireNonNull(imageUri));


                Toast.makeText(this, "Image Saved", Toast.LENGTH_SHORT).show();
            }
            else{

//                // Save image to gallery
//                String savedImageURL = MediaStore.Images.Media.insertImage(getContentResolver(), bitmap, "Bird", "Image of bird");
//
//                // Parse the gallery image url to uri
//                Uri savedImageURI = Uri.parse(savedImageURL);

                //with folder name
                String imagesDir = Environment.getExternalStoragePublicDirectory(
                        Environment.DIRECTORY_PICTURES).toString() + File.separator + school;

                File file = new File(imagesDir);

                if (!file.exists()) {
                    file.mkdir();
                }

                File image = new File(imagesDir, "report_card_img" + ".jpg");
                fos = new FileOutputStream(image);

                Toast.makeText(this, "Image saved to internal!!", Toast.LENGTH_SHORT).show();
                //resetOpTimes();

            }

            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            Objects.requireNonNull(fos);

        }catch(Exception e){

            Toast.makeText(this, "Image not saved \n" + e.toString(), Toast.LENGTH_SHORT).show();
        }

    }

    private void userprofile(String stu_id) {
        // UltimateProgress.showProgressBar(getActivity(),"Please Wait........!");
        commonProgress.show();
        HashMap<String, String> params = new HashMap<>();
        params.put("sub_id", stu_id);
        params.put("check", "fetch");

        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.UPDATE_SCHOOL_INFO, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error ) {

                //  ErpProgress.cancelProgressBar();
                if (error == null) {
                    try {
                        Log.e("daaaaaaaaaaaaaaa",jsonObject.getJSONObject(Constants.USERDATA).toString());
                        reportcard = jsonObject.getJSONObject(Constants.USERDATA).getString("reportcard");
                        school = jsonObject.getJSONObject(Constants.USERDATA).getString("name");
                        if (!reportcard.equalsIgnoreCase("https://ultimatesolutiongroup.com/office_admin/images/reportcard/")) {
                            //  Utils.setImageUri(ReportCard.this,logo,w_logo);
                            Picasso.get().load(reportcard).placeholder(R.color.transparent).into(image);
                            imageView.setVisibility(View.VISIBLE);
                            if (!restorePrefDataP()){
                                setShowcaseViewP();
                            }
                            textNorecord.setVisibility(View.GONE);
                            commonProgress.dismiss();
                        } else {
                            commonProgress.dismiss();
                            Picasso.get().load(R.color.transparent).into(image);
                            imageView.setVisibility(View.GONE);
                            textNorecord.setVisibility(View.VISIBLE);
                            textNorecord.setText("Report card not found, \nkindly upload student \nreport card first.");
                            Toast.makeText(getApplicationContext(), "Report card not found, kindly upload student report card first.", Toast.LENGTH_SHORT).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    commonProgress.dismiss();
                    Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }, this, params);
    }
}