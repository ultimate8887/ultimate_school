package com.ultimate.ultimatesmartschool.Examination;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.ultimate.ultimatesmartschool.BeanModule.User;
import com.ultimate.ultimatesmartschool.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Examclassadapter extends RecyclerView.Adapter<Examclassadapter.Viewholder> {
    private final AddNewexam addNewexam;
    ArrayList<Exambean> examtypelist;
    Context context;

    public Examclassadapter(ArrayList<Exambean> examtypelist,Context context,AddNewexam addNewexam){
        this.context=context;
        this.examtypelist=examtypelist;
        this.addNewexam=addNewexam;


    }

    @Override
    public Viewholder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.gcs_common_lyt, viewGroup, false);
        Examclassadapter.Viewholder viewholder = new Examclassadapter.Viewholder(view);
        return viewholder;
    }

    @Override
    public void onBindViewHolder(final Viewholder viewholder, final int i) {
        if (examtypelist.size() == i) {   //suppose data size is 4 then when position is 4 we have show add UI as it start count from o  then 4th position means  index 5
            viewholder.lytAll.setVisibility(View.GONE);
            viewholder.lytAddData.setVisibility(View.VISIBLE);
            viewholder.lytAddData.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (addNewexam != null) {
                        addNewexam.addEditNewexam(null,-1);
                    }
                }
            });
        } else {
            viewholder.lytAll.setVisibility(View.VISIBLE);
            viewholder.lytAddData.setVisibility(View.GONE);
            viewholder.textView.setText(examtypelist.get(i).getExamname());
            if (User.getCurrentUser().getType() != null){
                if (User.getCurrentUser().getType().equalsIgnoreCase("super")) {
                    viewholder.lytAll.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (addNewexam != null) {
                                addNewexam.addEditNewexam(examtypelist.get(i), i);
                            }
                        }
                    });
                } else {
                    //viewholder.imageView.setVisibility(View.GONE);
                    viewholder.lytAll.setClickable(false);

                }
            }

//            viewholder.lytAll.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    if (addNewexam != null) {
//                        addNewexam.addEditNewexam(examtypelist.get(i), i);
//                    }
//                }
//            });
            if (User.getCurrentUser().getType() != null){
                if (User.getCurrentUser().getType().equalsIgnoreCase("super")) {
                    viewholder.imageView.setVisibility(View.VISIBLE);
                } else {
                    viewholder.imageView.setVisibility(View.GONE);

                }
            }
            viewholder.imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (addNewexam != null) {
                        addNewexam.deleteexam(examtypelist.get(i),i);
                    }
                }
            });

        }


        //viewholder.textView.setText(examtypelist.get(i).getExamname());
    }

    @Override
    public int getItemCount() {
        return examtypelist.size()+1;
    }

    public void setexamList(ArrayList<Exambean> examtypelist) {
        this.examtypelist = examtypelist;
    }


    public class Viewholder extends RecyclerView.ViewHolder {
        public TextView textView;
        public ImageView imageView;
        @BindView(R.id.lytAddData)
        RelativeLayout lytAddData;
        @BindView(R.id.lytAll)
        RelativeLayout lytAll;
        @BindView(R.id.txtAddText)TextView textView2;
        @BindView(R.id.txtSub)
        TextView txtSub;
        public Viewholder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
            textView=(TextView)itemView.findViewById(R.id.txtTitle);
            imageView= (ImageView) itemView.findViewById(R.id.trash);
            textView2.setText("Add Exam");
            txtSub.setVisibility(View.GONE);

        }
    }
    public interface AddNewexam {

        public void addEditNewexam(Exambean data, int i);
        public void deleteexam(Exambean data,int i);
    }
}
