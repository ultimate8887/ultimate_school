package com.ultimate.ultimatesmartschool.Examination;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetSequence;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.ultimate.ultimatesmartschool.BeanModule.ClassBean;
import com.ultimate.ultimatesmartschool.BeanModule.SectionBean;
import com.ultimate.ultimatesmartschool.BeanModule.Studentbean;
import com.ultimate.ultimatesmartschool.BeanModule.User;
import com.ultimate.ultimatesmartschool.Homework.SectionnewAdapter;
import com.ultimate.ultimatesmartschool.MSG_NEW.Fragments.Stu_adapterGds;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.CommonProgress;
import com.ultimate.ultimatesmartschool.Utility.Constants;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ExamWebviewActivity extends AppCompatActivity {
    @BindView(R.id.webviewabout)
    WebView myWebView;
    String s_key, staff_id, class_id, quizid;
    boolean loadingFinished = true;
    boolean redirect = false;
    @BindView(R.id.imgBack)
    ImageView imgback;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    int loaded = 0;
    CommonProgress commonProgress;
    String studentid = "", sub_id = "", sub_name = "", sectionid = "", sectionname = "", classid = "", className = "";
    @BindView(R.id.dialog)
    ImageView dialog;
    SharedPreferences sharedPreferences;
    BottomSheetDialog mBottomSheetDialog;
    int check = 0;
    Spinner spinnersection;
    Spinner spinnerClass;
    Spinner spinnersubject;
    ArrayList<ClassBean> classList = new ArrayList<>();
    ArrayList<SectionBean> sectionList = new ArrayList<>();
    @BindView(R.id.spinnerstudnt)
    Spinner spinnerstudnt;
    @BindView(R.id.textNorecord)
    TextView textNorecord;

    String link ="",tag="";

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exam_webview);
        ButterKnife.bind(this);
        commonProgress = new CommonProgress(this);

        if (getIntent().getExtras() != null) {
            tag = getIntent().getExtras().getString("name");
        }
        if (!restorePrefData()) {
            setShowcaseView();
        } else {
            openDialog();
        }
        staff_id = User.getCurrentUser().getId();
        s_key = User.getCurrentUser().getSchoolData().getFi_school_id();
        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder1 = new AlertDialog.Builder(ExamWebviewActivity.this);
                builder1.setMessage("Are you sure, you want to exit? ");
                builder1.setCancelable(false);
                builder1.setPositiveButton(
                        "Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                finish();
                            }
                        });
                builder1.setNegativeButton(
                        "No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        });
                AlertDialog alert11 = builder1.create();
                alert11.show();
            }
        });

    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // Check if the key event was the Back button and if there's history
        if ((keyCode == KeyEvent.KEYCODE_BACK) && myWebView.canGoBack()) {
            // myWebView.goBack();
            return true;
        }
        // If it wasn't the Back key or there's no web page history, bubble up to the default
        // system behavior (probably exit the activity)
        return super.onKeyDown(keyCode, event);
    }

    private void savePrefData() {
        sharedPreferences = this.getSharedPreferences("boarding_pref_view_attend", MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean("IsFirstTimeVisit_view_attend", true);
        editor.apply();
    }

    private boolean restorePrefData() {
        sharedPreferences = this.getSharedPreferences("boarding_pref_view_attend", MODE_PRIVATE);
        return sharedPreferences.getBoolean("IsFirstTimeVisit_view_attend", false);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void openDialog() {

        mBottomSheetDialog = new BottomSheetDialog(this);
        final View sheetView = getLayoutInflater().inflate(R.layout.search_dialog, null);
        mBottomSheetDialog.setContentView(sheetView);
        mBottomSheetDialog.setCancelable(false);

        spinnerClass = (Spinner) sheetView.findViewById(R.id.spinnerClass);
        spinnersection = (Spinner) sheetView.findViewById(R.id.spinnersection);
        spinnersubject = (Spinner) sheetView.findViewById(R.id.spinnersubject);
        ImageView close = (ImageView) sheetView.findViewById(R.id.close);
        TextView txtSetup = (TextView) sheetView.findViewById(R.id.txtSetup);
        Button btnYes = (Button) sheetView.findViewById(R.id.btnYes);
        // getTodayDate();
        txtSetup.setText("View Report Card");
        //btnYes.setText("View Subject-Wise \nAttendance");
        fetchClass();

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //assignData();
                if (check == 0) {
                    finish();
                    mBottomSheetDialog.dismiss();
                } else {
                    mBottomSheetDialog.dismiss();
                }
            }
        });
        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (classid.equalsIgnoreCase("")) {
                    Toast.makeText(ExamWebviewActivity.this, "Kindly Select Class!", Toast.LENGTH_SHORT).show();
                } else if (sectionid.equalsIgnoreCase("")) {
                    Toast.makeText(ExamWebviewActivity.this, "Kindly Select Class Section!", Toast.LENGTH_SHORT).show();
                } else {
                    check++;
                    fetchStudent(classid, sectionid);
                    setCommonData();
                }

            }
        });
        mBottomSheetDialog.show();

    }

    private void setCommonData() {
        myWebView.setVisibility(View.VISIBLE);
        txtTitle.setText(className + "(" + sectionname + ")");
        // txtSub.setText(sub_name);
    }

    private void fetchClass() {
        if (loaded == 0) {
            commonProgress.show();
        }
        loaded++;

        HashMap<String, String> params = new HashMap<String, String>();
        params.put("check", "inch");
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.CLASSLIST_URL, classapiCallback, getApplicationContext(), params);
    }


    ApiHandler.ApiCallback classapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            commonProgress.dismiss();
            if (error == null) {
                try {
                    classList = ClassBean.parseClassArray(jsonObject.getJSONArray(Constants.CLASSDATA));
                    ClassAdapter adapter = new ClassAdapter(getApplicationContext(), classList);
                    spinnerClass.setAdapter(adapter);
                    spinnerClass.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if (i > 0) {
                                sub_id = "";
                                sub_name = "";
                                sectionid = "";
                                sectionname = "";
                                classid = "";
                                className = "";
                                classid = classList.get(i - 1).getId();
                                className = classList.get(i - 1).getName();
                                fetchsection();
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
                // Utils.showSnackBar(error.getMessage(), view.findViewById(R.id.parent));
            }
        }
    };

    private void fetchsection() {
        HashMap<String, String> params = new HashMap<String, String>();
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.SECTIONLIST_URL, sectionapiCallback, getApplicationContext(), params);
    }

    ApiHandler.ApiCallback sectionapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            if (error == null) {
                try {
                    sectionList = SectionBean.parseCommonArray(jsonObject.getJSONArray("hostelroom_data"));
                    SectionnewAdapter adapter = new SectionnewAdapter(getApplicationContext(), sectionList);
                    spinnersection.setAdapter(adapter);
                    spinnersection.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if (i > 0) {
                                sectionid = sectionList.get(i - 1).getSection_id();
                                sectionname = sectionList.get(i - 1).getSection_name();
                                // fetchHomework();
                                //  fetchStudent(classid,sectionid);
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
                // Utils.showSnackBar(error.getMessage(), parent);

            }
        }
    };

    private void fetchStudent(String classid, String sectionid) {
        commonProgress.show();
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("class_id", classid);
        params.put("section_id", sectionid);
//        Log.e("class_id",classid);
//        Log.e("section_id",sectionid);
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.STUDENTLISTCLSSECWISE_URL, apiCallbackstudnt, getApplicationContext(), params);
    }

    private ArrayList<Studentbean> stuList;
    private Stu_adapterGds adapterstu;
    ApiHandler.ApiCallback apiCallbackstudnt = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            commonProgress.dismiss();
            mBottomSheetDialog.dismiss();
            if (error == null) {
                try {
                    stuList = Studentbean.parseHWArray(jsonObject.getJSONArray("student_list"));
                    adapterstu = new Stu_adapterGds(getApplicationContext(), stuList);
                    spinnerstudnt.setAdapter(adapterstu);
                    spinnerstudnt.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if (i > 0) {
                                studentid = stuList.get(i - 1).getId();
                                //lytCalender.setVisibility(View.VISIBLE);
                                setReportData();

                            } else {
                                myWebView.setVisibility(View.GONE);
                                studentid = "";
                            }

                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Toast.makeText(getApplicationContext(), "Student Not Found", Toast.LENGTH_LONG).show();
            }
        }
    };

    private void setReportData() {
        myWebView.setVisibility(View.VISIBLE);
        myWebView.requestFocus();
        myWebView.getSettings().setDefaultTextEncodingName("utf-8");
        myWebView.setVerticalScrollBarEnabled(true);
        myWebView.setWebViewClient(new WebViewClient(){
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }
        });
        myWebView.getSettings().setJavaScriptEnabled(true);
        myWebView.getSettings().setLightTouchEnabled(true);
        myWebView.getSettings().setBuiltInZoomControls(true);
        myWebView.setWebChromeClient(new WebChromeClient() {
                                         @Override
                                         public void onProgressChanged(WebView view, int newProgress) {
                                             if (newProgress < 100) {
                                                 commonProgress.show();
                                             }
                                             if (newProgress == 100) {
                                                 commonProgress.dismiss();
                                                 textNorecord.setVisibility(View.GONE);
                                             }
                                         }


                                     }
        );
        if (tag.equalsIgnoreCase("half")) {
        //   link = "https://ultimatesolutiongroup.com/onlinetest/adminResult.php?s_key=" + s_key +"";

        if(User.getCurrentUser().getSchoolData().getFi_school_id().equalsIgnoreCase("BFPS")){

//           link="https://ultimatesolutiongroup.com/staff_panel/" +
//                   "app_view_report_card_bfps_term1.php?class_id=8&section_id=1&student_id=221500&s_key=BFPS";


            link = "https://ultimatesolutiongroup.com/staff_panel/app_view_report_card_bfps_term1.php?class_id="
                    + classid + "&section_id=" + sectionid + "&student_id=" + studentid + "&s_key=" + s_key;

        }else if(User.getCurrentUser().getSchoolData().getFi_school_id().equalsIgnoreCase("MSS")) {
            if (className.equalsIgnoreCase("11th") || className.equalsIgnoreCase("12th") ||
                    className.equalsIgnoreCase("11") || className.equalsIgnoreCase("12")){

                link="https://ultimatesolutiongroup.com/staff_panel/app_view_report_card_mss_termtwelves.php?class_id"
                        +classid+"&section_id="+sectionid+"&student_id="+studentid+"&s_key="+s_key;

            } else if (className.equalsIgnoreCase("9th") || className.equalsIgnoreCase("10th") ||
                    className.equalsIgnoreCase("9") || className.equalsIgnoreCase("10")) {
                link="https://ultimatesolutiongroup.com/staff_panel/app_view_report_card_mss_termtens.php?class_id="
                        +classid+"&section_id="+sectionid+"&student_id="+studentid+"&s_key="+s_key;

            }else{

                link="https://ultimatesolutiongroup.com/staff_panel/app_view_report_card_mss_term_stu1.php?class_id="
                        +classid+"&section_id="+sectionid+"&student_id="+studentid+"&s_key="+s_key;
            }
        }else {

            String staff_id=User.getCurrentUser().getId();
            link = "https://ultimatesolutiongroup.com/staff_panel/app_view_report_card_mss_term1_gnd.php?class_id="
                    + classid + "&section_id=" + sectionid + "&student_id=" + studentid + "&s_key=" + s_key+ "&staff_id=" + staff_id;
        }

        } else {
            String staff_id=User.getCurrentUser().getId();
            link = "https://ultimatesolutiongroup.com/staff_panel/app_report_card_admin.php?class_id="
                    + classid + "&section_id=" + sectionid + "&student_id=" + studentid + "&s_key=" + s_key;
        }

        Log.e("result",link);
        myWebView.loadUrl(link);
    }


    private void setShowcaseView() {
        new TapTargetSequence(this)
                .targets(TapTarget.forView(dialog, "Search Button!", "Tap the Search Button to View Student-Wise Report card.")
                        .outerCircleColor(R.color.light)
                        .outerCircleAlpha(0.96f)
                        .targetCircleColor(R.color.white)
                        .titleTextSize(20)
                        .titleTextColor(R.color.white)
                        .descriptionTextSize(14)
                        .descriptionTextColor(R.color.black)
                        .textColor(R.color.black)
                        .textTypeface(Typeface.SANS_SERIF)
                        .dimColor(R.color.black)
                        .drawShadow(true)
                        .cancelable(false)
                        .tintTarget(true)
                        .transparentTarget(true)
                        .targetRadius(60)).listener(new TapTargetSequence.Listener() {
                    @RequiresApi(api = Build.VERSION_CODES.N)
                    @Override
                    public void onSequenceFinish() {
                        // Toast.makeText(getApplicationContext(),"Sequence Finished",Toast.LENGTH_SHORT).show();
                        savePrefData();
                        openDialog();
                    }

                    @Override
                    public void onSequenceStep(TapTarget lastTarget, boolean targetClicked) {
                        // Toast.makeText(getActivity(),"GREAT!",Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onSequenceCanceled(TapTarget lastTarget) {

                    }
                }).start();
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @OnClick(R.id.dialog)
    public void dialogggg() {
        openDialog();
    }
}