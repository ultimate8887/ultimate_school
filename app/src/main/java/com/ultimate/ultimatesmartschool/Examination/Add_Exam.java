package com.ultimate.ultimatesmartschool.Examination;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.ultimate.ultimatesmartschool.BeanModule.User;
import com.ultimate.ultimatesmartschool.Login.LoginActivity;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Add_Exam extends AppCompatActivity implements Examclassadapter.AddNewexam {
    Dialog exmdialog;
    ArrayList<Exambean> examList = new ArrayList<>();
    @BindView(R.id.exampage)
    RelativeLayout parent;
    RecyclerView.LayoutManager layoutManager;
    private Examclassadapter adapter;
    @BindView(R.id.imgBack)
    ImageView back;
    @BindView(R.id.recyclerViewaddexm)
    RecyclerView recyclerView;
    int loaded = 0;
    String classid = "";
    String subid = "";

    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add__exam);
        ButterKnife.bind(this);
        txtTitle.setText("Examination");
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        layoutManager = new LinearLayoutManager(Add_Exam.this);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new Examclassadapter(examList, this, this);
        recyclerView.setAdapter(adapter);
        fetchexam();
    }

    private void fetchexam() {
        if(loaded==0){
            ErpProgress.showProgressBar(this,"please wait....");
        }loaded++;
        HashMap<String, String> params = new HashMap<>();
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.addexam, apiCallback, this, params);
    }

    ApiHandler.ApiCallback apiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            // txtupload.setVisibility(View.VISIBLE);
            if (error == null) {
                try {
                    if (examList != null) {
                        examList.clear();
                    }
                    JSONArray jsonArray = jsonObject.getJSONArray("examname_data");
                    examList = Exambean.parseexamArray(jsonArray);
                    // Log.d("TAG", "onDataFetched: " + examList.size());
                    //adapter.setHQList(obj);
                    adapter.setexamList(examList);
                    adapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Utils.showSnackBar(error.getMessage(), parent);
                if (error.getStatusCode() == 405) {
                    User.logout();
                    startActivity(new Intent(Add_Exam.this, LoginActivity.class));
                    finish();
                }
            }
        }
    };

    @Override
    public void addEditNewexam(final Exambean data, final int i) {
        exmdialog = new Dialog(this);
        exmdialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        exmdialog.setCancelable(true);
        exmdialog.setContentView(R.layout.add_exams_dialog);
        final EditText edtTitle = (EditText) exmdialog.findViewById(R.id.edtTitleexm);
        Button btnSubmit = (Button) exmdialog.findViewById(R.id.btnSubmitexm);
        if (i != -1) {
            edtTitle.setText(examList.get(i).getExamname());
            btnSubmit.setText("Update");
        }

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (i == -1) {
                    addNewexam(edtTitle.getText().toString());
                } else {
                    editexam(data, i, edtTitle.getText().toString());
                }
                adapter.notifyDataSetChanged();
                exmdialog.dismiss();
//                Intent intent=new Intent(Add_Exam.this,ScheduleActivity.class);
//                intent.putExtra("id",data.getId());
//                startActivity(intent);
                //updateInst();
            }
        });
        exmdialog.show();
    }


    private void editexam(final Exambean data, final int i, String s) {
        HashMap<String, String> params = new HashMap<>();
        params.put("id", data.getId());
        params.put("examname", s);
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.addupdateexam, apiCallbackaddup, this, params);
    }

    private void addNewexam(String s) {
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("examname", s);
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.addupdateexam, apiCallbackaddup, this, params);

    }


    ApiHandler.ApiCallback apiCallbackaddup = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            if (error == null) {
                try {
                    examList.clear();
                    Toast.makeText(Add_Exam.this, jsonObject.getString("msg"), Toast.LENGTH_SHORT).show();
                    JSONArray jsonArray = jsonObject.getJSONArray("examname_data");
                    examList = Exambean.parseexamArray(jsonArray);
                    adapter.setexamList(examList);
                    adapter.notifyDataSetChanged();
                    exmdialog.dismiss();
                    //updateInst();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("error", error.getMessage() + "");
                Utils.showSnackBar(error.getMessage(), parent);
            }
        }
    };

    @Override
    public void deleteexam(final Exambean data, final int i) {
        DialogInterface.OnClickListener dialogclicllistner=new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:
                        HashMap<String, String> params = new HashMap<String, String>();
                        params.put("id", data.getId());
                        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.delexam, new ApiHandler.ApiCallback() {
                            @Override
                            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                                if (error == null) {
                                    Toast.makeText(Add_Exam.this, "Deleted Successfully", Toast.LENGTH_SHORT).show();
                                    examList.remove(i);
                                    adapter.notifyDataSetChanged();

                                } else {
                                    Utils.showSnackBar(error.getMessage(), parent);
                                }
                            }
                        }, Add_Exam.this, params);
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        //No button clicked
                        break;
                }
            }
        };
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Are you sure you want to delete?").setPositiveButton("Yes", dialogclicllistner)
                .setNegativeButton("No", dialogclicllistner).show();
    }
}
