package com.ultimate.ultimatesmartschool.Examination;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.ultimate.ultimatesmartschool.BeanModule.ClassBean;
import com.ultimate.ultimatesmartschool.BeanModule.CommonBean;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ScheduleActivity extends AppCompatActivity implements SubjectlistAdapter.TimeandDate {
    @BindView(R.id.imgBack)
    ImageView back;
    private ArrayList<Exambean> examList = new ArrayList<>();
    ArrayList<Sessionexambean> sessionlist = new ArrayList<>();
    private ArrayList<CommonBean> dataList = new ArrayList<>();
    private ArrayList<CommonBean> groupList = new ArrayList<>();
    static ArrayList<SubjectListbean> subjectList = new ArrayList<>();
    private String groupid = "";
    private String classid = "";
    private ClassAdapter classadapter;
    Exmnamespinadapter exmnamespinadapter;
    Acadmicyrspinadpter acadmicyrspinadpter;
    private static SubjectlistAdapter madapter;
    public ArrayList<ClassBean> classList = new ArrayList<>();
    @BindView(R.id.exampagew)
    RelativeLayout parent;
    @BindView(R.id.spinnerGroup)
    Spinner spinnergrp;
    @BindView(R.id.spinnerClass)
    Spinner spinnerclss;
    @BindView(R.id.spinnerexam)
    Spinner spinnerexmname;
    @BindView(R.id.spinneracdmicyr)
    Spinner spinneracdmicyr;
    @BindView(R.id.buttonnxt)
    Button buttonnext;
    private Sessionexambean acdemic;
    //private Exambean examid;
    private String examid = "";
    @BindView(R.id.recyclerViewsub)
    RecyclerView recyclerView;
    RecyclerView.LayoutManager layoutManager;
    @BindView(R.id.gg)
    RelativeLayout stat;
    @BindView(R.id.lll)
    RelativeLayout bstat;
    private static SubjectListbean subject;
    @BindView(R.id.submitexam)
    Button submit;
    String exam_id;
    ArrayList<String> examdatelist;
    ArrayList<String> exmdurlist;
    ArrayList<String> totalmrkslist;
    ArrayList<String> passmrkslist;
    ArrayList<String> subjectlist;

    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_schedule);
        ButterKnife.bind(this);
        txtTitle.setText("Schedule Exam");
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        fetchgroup();
        fetchAcademicyear();
        fetchexamname();
        layoutManager = new LinearLayoutManager(ScheduleActivity.this);
        recyclerView.setLayoutManager(layoutManager);
        madapter = new SubjectlistAdapter(subjectList, ScheduleActivity.this, this);
        recyclerView.setAdapter(madapter);
        bstat.setVisibility(View.GONE);
        buttonnext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (checkValid()) {
                    fetchsujectname(classid);
                    stat.setVisibility(View.GONE);
                    bstat.setVisibility(View.VISIBLE);
                } else {
                    stat.setVisibility(View.VISIBLE);

                }


            }
        });
    }

    private boolean checkValid() {
        boolean valid = true;
        String errorMsg = null;
        if (groupid.isEmpty() || groupid == "") {
            valid = false;
            errorMsg = "Please select Group";
        } else if (classid.isEmpty() || classid == "") {
            valid = false;
            errorMsg = "Please select class";

        } else if (examid == null) {
            valid = false;
            errorMsg = "please select Exam name";

        } else if (acdemic == null) {
            valid = false;
            errorMsg = "please select Academic Year";
        }

        if (!valid) {
            Utils.showSnackBar(errorMsg, parent);
        }
        return valid;
    }

    private void fetchgroup() {
        ErpProgress.showProgressBar(this, "Please wait...");
        HashMap<String, String> params = new HashMap<String, String>();
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.GROUP_LIST_URL, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                ErpProgress.cancelProgressBar();
                if (error == null) {

                    try {
                        groupList.clear();
                        groupList = CommonBean.parseCommonArray(jsonObject.getJSONArray(Constants.GROUP_DATA));
                        final GroupSpinnerAdapter adapter = new GroupSpinnerAdapter(ScheduleActivity.this, groupList,0);
                        spinnergrp.setAdapter(adapter);
                        spinnergrp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                if (position > 0) {
                                    groupid = groupList.get(position - 1).getId();
                                    classid = "";
                                    fetchClass(groupid);
                                } else {
                                    if (classadapter != null) {
                                        classList.clear();
                                        classid = "";
                                        groupid = "";
                                        classadapter.notifyDataSetChanged();
                                    }
                                }
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    Utils.showSnackBar(error.getMessage(), parent);
                }
            }
        }, this, params);
    }

    private void fetchClass(String id) {
        // ErpProgress.showProgressBar(this, "Please wait...");
        HashMap<String, String> params = new HashMap<String, String>();
        params.put(Constants.ID, id);
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.GROUPCLASSLIST_URL, classapiCallback, this, params);
    }

    ApiHandler.ApiCallback classapiCallback = new ApiHandler.ApiCallback() {

        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            if (error == null) {
                try {
                    classList = ClassBean.parseClassArray(jsonObject.getJSONArray(Constants.CLASSDATA));
                    classadapter = new ClassAdapter(ScheduleActivity.this, classList);
                    spinnerclss.setAdapter(classadapter);
                    spinnerclss.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if (i > 0) {
                                classid = classList.get(i - 1).getId();
                                //fetchsujectname(classid);
                            } else {
                                classid = "";
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Utils.showSnackBar(error.getMessage(), parent);
            }

        }
    };

    private void fetchexamname() {
        //ErpProgress.showProgressBar(this, "Please wait...");
        HashMap<String, String> params = new HashMap<>();

        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.addexam, apiCallback, this, params);

    }

    ApiHandler.ApiCallback apiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            if (error == null) {
                try {
                    JSONArray jsonArray = jsonObject.getJSONArray("examname_data");
                    examList = Exambean.parseexamArray(jsonArray);
                    exmnamespinadapter = new Exmnamespinadapter(ScheduleActivity.this, examList);
                    spinnerexmname.setAdapter(exmnamespinadapter);
                    spinnerexmname.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            if (position > 0) {
                                examid = examList.get(position - 1).getId();
                            } else {
                                examid = "";
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }else{
                Utils.showSnackBar(error.getMessage(), parent);
            }
        }
    };

    private void fetchAcademicyear() {
        //ErpProgress.showProgressBar(this, "Please wait...");
        HashMap<String, String> params = new HashMap<>();
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.SESSION_URL, apiCallbackacdemicyear, this, params);
    }

    ApiHandler.ApiCallback apiCallbackacdemicyear = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            if (error == null) {
                try {
                    JSONArray jsonArray = jsonObject.getJSONArray("session_data");
                    sessionlist = Sessionexambean.parsesessionArray(jsonArray);
                    acadmicyrspinadpter = new Acadmicyrspinadpter(ScheduleActivity.this, sessionlist);
                    spinneracdmicyr.setAdapter(acadmicyrspinadpter);
                    spinneracdmicyr.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            if (position > 0) {
                                acdemic = sessionlist.get(position - 1);

                            } else {
                                acdemic = null;
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }else{
                Utils.showSnackBar(error.getMessage(), parent);
            }
        }
    };


    private void fetchsujectname(String classid) {
        // ErpProgress.showProgressBar(this, "Please wait...");
        HashMap<String, String> params = new HashMap<String, String>();
        params.put(Constants.ID, classid);

        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.SUB_LIST_URL, subapiCallback, this, params);

    }

    ApiHandler.ApiCallback subapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            // ErpProgress.cancelProgressBar();
            if (error == null) {
                try {
                    JSONArray jsonArray = jsonObject.getJSONArray("sub_data");
                    subjectList = SubjectListbean.parsesubjectArray(jsonArray);
                    if (subjectList.size() > 0) {
                        madapter.setsubliat(subjectList);
                        madapter.notifyDataSetChanged();
                    } else {
                        madapter.setsubliat(subjectList);
                        madapter.notifyDataSetChanged();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Utils.showSnackBar(error.getMessage(), parent);
            }
        }
    };

    @Override
    public void ondatecallback(SubjectListbean subjectListbean) {
        subject = null;
        subject = subjectListbean;
        DatePickerFragment mDatePicker = new DatePickerFragment();
        mDatePicker.show(getFragmentManager(), "Select date");

    }


    public static class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);
            DatePickerDialog dialog = new DatePickerDialog(getActivity(), this, year, month, day);
            dialog.getDatePicker().setMinDate(c.getTimeInMillis());
            return dialog;
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            Calendar c = Calendar.getInstance();
            c.set(year, month, day);
            String text_date = new SimpleDateFormat("dd MMM, yyyy").format(c.getTime());
            int pos = subjectList.indexOf(subject);
            subject.setExam_Date(text_date);
            subjectList.set(pos, subject);
            madapter.notifyItemChanged(pos);
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            String examdate = simpleDateFormat.format(c.getTime());
            subject.setExam_Dateshowdb(examdate);

        }
    }


    @OnClick(R.id.submitexam)
    public void save() {
        if (checkValid1()) {
            ErpProgress.showProgressBar(this,"please wait....");
            String acd_year = acdemic.getStart_date() + acdemic.getEnd_date();
            subjectlist = new ArrayList<>();
            examdatelist = new ArrayList<>();
            exmdurlist = new ArrayList<>();
            totalmrkslist = new ArrayList<>();
            passmrkslist = new ArrayList<>();

            for (int i = 0; i < madapter.getSubjectList().size(); i++) {
                subjectlist.add(madapter.getSubjectList().get(i).getId());
                examdatelist.add(madapter.getSubjectList().get(i).getExam_Dateshowdb());
                String org;
                int minutes;
                String minut = madapter.getSubjectList().get(i).getExam_dur();
                Log.d("nnnnnm", minut);
                minutes = Integer.parseInt(minut);
                if (minutes > 60) {
                    int h = minutes / 60;
                    int m = (minutes - (60 * h));
                    org = h + ":" + m;
                   // org = h +"h" + ":" + m +"m";
                } else {
                  //  org = "00:" + minutes+"m";
                    org = "00:" + minutes+"";
                }
                exmdurlist.add(org);
                totalmrkslist.add(madapter.getSubjectList().get(i).getExam_markS());
                passmrkslist.add(madapter.getSubjectList().get(i).getPass_marks());
            }
            HashMap<String, String> params = new HashMap<String, String>();
            params.put("subject_id", String.valueOf(subjectlist));
            params.put("exm_date", String.valueOf(examdatelist));
            params.put("exam_dur", String.valueOf(exmdurlist));
            params.put("tmarks", String.valueOf(totalmrkslist));
            params.put("pass_mark", String.valueOf(passmrkslist));
            params.put("class_id", classid);
            params.put("acd_id", String.valueOf(examid));
            params.put("group_id", groupid);
            params.put("acd_year", acd_year);
            ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.UPLOAD_EXAM, uploadexamCallback, this, params);
        }
    }

    ApiHandler.ApiCallback uploadexamCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            if (error == null) {
                Toast.makeText(ScheduleActivity.this, "UPLOADED SUCCESS", Toast.LENGTH_SHORT).show();
                finish();
            } else {
                Toast.makeText(ScheduleActivity.this, "UPLOADED FAILED", Toast.LENGTH_SHORT).show();
                Log.e("16955", error.getMessage() + "");
            }
        }
    };

    @Override
    protected void onResume() {
        super.onResume();
    }


    private boolean checkValid1() {
        boolean valid = true;
        String errorMsg = null;
        for (int i = 0; i < madapter.getSubjectList().size(); i++) {

            SubjectListbean sub = madapter.getSubjectList().get(i);
            if (sub.getExam_Dateshowdb() == null || sub.getExam_Dateshowdb().isEmpty()) {
                valid = false;
                errorMsg = "Please enter the Full Data!";
                break;
            } else if (sub.getExam_dur() == null || sub.getExam_dur().isEmpty()) {
                valid = false;
                errorMsg = "Please enter exam duration!";
                break;
            } else if (sub.getExam_markS() == null|| sub.getExam_markS().isEmpty()) {
                valid = false;
                errorMsg = "please enter total marks!";
                break;
            } else if (sub.getPass_marks() == null|| sub.getPass_marks().isEmpty()) {
                valid = false;
                errorMsg = "please enter pass marks!";
                break;
            }


        }
        if (!valid) {
            Utils.showSnackBar(errorMsg, parent);
        }
        return valid;
    }
}
