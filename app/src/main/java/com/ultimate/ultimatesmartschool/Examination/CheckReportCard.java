package com.ultimate.ultimatesmartschool.Examination;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.squareup.picasso.Picasso;
import com.ultimate.ultimatesmartschool.BeanModule.SectionBean;
import com.ultimate.ultimatesmartschool.BeanModule.Studentbean;
import com.ultimate.ultimatesmartschool.BeanModule.User;
import com.ultimate.ultimatesmartschool.ClassTest.Messageclass_adpter;
import com.ultimate.ultimatesmartschool.Fee.CommonFeesListActivity;
import com.ultimate.ultimatesmartschool.Homework.SectionnewAdapter;
import com.ultimate.ultimatesmartschool.Login.LoginActivity;
import com.ultimate.ultimatesmartschool.MSG_NEW.Fragments.Stu_adapterGds;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.StuGatePass.ClassBeans;
import com.ultimate.ultimatesmartschool.StudentAttendance.Spinner_stu_adapterGds;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.CommonProgress;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CheckReportCard extends AppCompatActivity {

    @BindView(R.id.imgBack) ImageView back;
    @BindView(R.id.txtTitle) TextView txtTitle;

    String stuname="",dob="",mother_name="",profile="";

    @BindView(R.id.spinneracdmicyrsec) Spinner acadmicyrspin;
    @BindView(R.id.spinnerexamsec)Spinner exmnamspinn;
    @BindView(R.id.spinnerClasssec)Spinner classspin;
    @BindView(R.id.spinnersection)Spinner spinnersection;
    @BindView(R.id.spinnerStudent)Spinner spinnerStudent;
    @BindView(R.id.buttonnxtdetil) Button buttonnxtdetil;
    @BindView(R.id.parent) LinearLayout parent;


    ArrayList<Sessionexambean> sessionlist = new ArrayList<>();
    private ArrayList<Exambean> examList = new ArrayList<>();
    ArrayList<ClassBeans> classList = new ArrayList<>();
    ArrayList<SectionBean> sectionList = new ArrayList<>();
    private ArrayList<Studentbean> stuList= new ArrayList<>();

    private Acadmicyrspinadpter acadmicyrspinadpter;
    private Exmnamespinadapter exmnamespinadapter;
    private Stu_adapterGds adapterstu;

    String sectionid="";
    String sectionname;
    String classid = "";
    String stu_id="";
    private String examid="";
    private String acdemicde="";
    private String exam_name="";
    private Sessionexambean acdemic;


    String logo="",s_sign="",a_sign="",school="",name="",address="",tag="";
    @BindView(R.id.stulayout) RelativeLayout stulayout;

    String classname="";
    private int loaded = 0;

    @BindView(R.id.edtName) EditText edtName;
    @BindView(R.id.edtFather) EditText edtFather;
    @BindView(R.id.edtRegistration) EditText edtRegistration;
    @BindView(R.id.edtMobile) EditText edtMobile;
    @BindView(R.id.student_lyt) LinearLayout student_lyt;
    @BindView(R.id.dp) ImageView dp;
    CommonProgress commonProgress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_report_card);
        ButterKnife.bind(this);

        if (getIntent().getExtras() != null) {
            tag = getIntent().getExtras().getString("name");
            if (tag.equalsIgnoreCase("gazette")) {
                txtTitle.setText("Examination Gazette");
                stulayout.setVisibility(View.GONE);
            } else {
                txtTitle.setText("Student-Wise Report Card");
                stulayout.setVisibility(View.VISIBLE);
            }
        }

        commonProgress=new CommonProgress(this);
        //txtTitle.setText("Student-Wise Report Card");
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        fetchAcademicyearlist();
        fetchexamnamelist();
        fetchclasslist();
       // fetchsection();

    }

    private void fetchAcademicyearlist() {
        commonProgress.show();
        HashMap<String, String> params = new HashMap<>();
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.COLGACADEMICYAER_URL, apiCallbackacdemicyear, this, params);
        //ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.SESSION_URL, apiCallbackacdemicyear, this, params);
    }

    ApiHandler.ApiCallback apiCallbackacdemicyear = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            commonProgress.dismiss();
            if(error==null) {
                try {
                    JSONArray jsonArray = jsonObject.getJSONArray("session_data");
                    sessionlist = Sessionexambean.parsesessionArray(jsonArray);
                    acadmicyrspinadpter = new Acadmicyrspinadpter(CheckReportCard.this, sessionlist);
                    acadmicyrspin.setAdapter(acadmicyrspinadpter);
                    acadmicyrspin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            if (position > 0) {
                                acdemic = sessionlist.get(position - 1);
                                acdemicde = sessionlist.get(position - 1).getStart_date() + sessionlist.get(position - 1).getEnd_date();

                            } else {
                                acdemic = null;
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }else{
                Utils.showSnackBar(error.getMessage(), parent);
            }
        }
    };

    private void fetchexamnamelist() {
        HashMap<String, String> params = new HashMap<>();

        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.addexam, apiCallback, this, params);

    }

    ApiHandler.ApiCallback apiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            if(error==null) {
                try {
                    JSONArray jsonArray = jsonObject.getJSONArray("examname_data");
                    examList = Exambean.parseexamArray(jsonArray);
                    exmnamespinadapter = new Exmnamespinadapter(CheckReportCard.this, examList);
                    exmnamspinn.setAdapter(exmnamespinadapter);
                    exmnamspinn.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            if (position > 0) {
                                examid = examList.get(position - 1).getId();
                                exam_name = examList.get(position - 1).getExamname();
                            } else {
                                examid = "";
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }else{
                Utils.showSnackBar(error.getMessage(), parent);
            }
        }
    };

    private void fetchclasslist() {

        HashMap<String, String> params = new HashMap<String, String>();
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.CLASSLIST_URL, classapiCallback, this, params);
    }


    ApiHandler.ApiCallback classapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            if (error == null) {
                try {
                    classList = ClassBeans.parseClassArray(jsonObject.getJSONArray(Constants.CLASSDATA));
                    Messageclass_adpter adapter = new Messageclass_adpter(CheckReportCard.this, classList);
                    classspin.setAdapter(adapter);
                    classspin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

//                            if (i == 0) {

                                if (adapterstu != null) {
                                    stuList.clear();
                                    adapterstu.notifyDataSetChanged();
                                }
                          //  }
                            if (i > 0) {

                                classid = classList.get(i-1).getId();
                                classname=classList.get(i-1).getName();
                                stu_id="";


                                Log.e("classsid",classid);

                                fetchsection();
                                userprofile(classid);
                            }else{
                                classid="";
                            }

                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {

                Utils.showSnackBar(error.getMessage(), parent);
            }
        }
    };

    private void userprofile(String classid) {
        // UltimateProgress.showProgressBar(getActivity(),"Please Wait........!");
//        commonProgress.show();
        HashMap<String, String> params = new HashMap<>();
        params.put("user_id", User.getCurrentUser().getId());
        params.put("check", "fetch");
        params.put("classid", classid);

        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.UPDATE_SCHOOL_INFO, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error ) {
                //  commonProgress.dismiss();
                //  ErpProgress.cancelProgressBar();
                if (error == null) {
                    try {
                        //  Log.e("daaaaaaaaaaaaaaa",jsonObject.getJSONObject(Constants.USERDATA).toString());
                        logo = jsonObject.getJSONObject(Constants.USERDATA).getString("logo");
                        school = jsonObject.getJSONObject(Constants.USERDATA).getString("name");

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }, this, params);
    }


    private void fetchsection() {
        commonProgress.show();
        HashMap<String, String> params = new HashMap<String, String>();
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.SECTIONLIST_URL, sectionapiCallback, this, params);
    }

    ApiHandler.ApiCallback sectionapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            commonProgress.dismiss();
            if (error == null) {
                try {
                    sectionList = SectionBean.parseCommonArray(jsonObject.getJSONArray("hostelroom_data"));
                    SectionnewAdapter adapter = new SectionnewAdapter(CheckReportCard.this, sectionList);
                    spinnersection.setAdapter(adapter);
                    spinnersection.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            sectionid="";
                            sectionname="";
                            if (i > 0) {
                                sectionid = sectionList.get(i - 1).getSection_id();
                                sectionname=sectionList.get(i - 1).getSection_name();
                                if (!tag.equalsIgnoreCase("gazette")) {
                                    fetchStudent(classid,sectionid);
                                }else{
                                    student_lyt.setVisibility(View.GONE);
                                    buttonnxtdetil.setVisibility(View.VISIBLE);
                                }
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Utils.showSnackBar(error.getMessage(), parent);
                if (error.getStatusCode() == 405) {
                    User.logout();
                    startActivity(new Intent(CheckReportCard.this, LoginActivity.class));
                    finish();
                }
            }
        }
    };

    private void fetchStudent(String classid,String sectionid) {

        commonProgress.show();
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("class_id",classid);
        params.put("section_id",sectionid);
        Log.e("class_id",classid);
        Log.e("section_id",sectionid);
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.STUDENTLISTCLSSECWISE_URL, apiCallbackstudnt, this, params);
    }

    final ApiHandler.ApiCallback apiCallbackstudnt = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            commonProgress.dismiss();
            if (error == null) {
                try {
                    stuList = Studentbean.parseHWArray(jsonObject.getJSONArray("student_list"));
                    adapterstu = new Stu_adapterGds(CheckReportCard.this, stuList);
                    spinnerStudent.setAdapter(adapterstu);
                    spinnerStudent.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                            edtFather.setText("");
                            edtName.setText("");
                            edtRegistration.setText("");
                            edtMobile.setText("");
                            Picasso.get().load(R.drawable.stud).into(dp);


                            if (i > 0) {
                                stu_id = stuList.get(i - 1).getId();
                                stuname = stuList.get(i - 1).getName();

                                dob = stuList.get(i - 1).getDob();
                                mother_name = stuList.get(i - 1).getMother_name();
                                profile = stuList.get(i - 1).getProfile();

                                student_lyt.setVisibility(View.VISIBLE);
                                buttonnxtdetil.setVisibility(View.VISIBLE);

                                if (stuList.get(i-1).getGender().equalsIgnoreCase("Male")) {
                                    if (stuList.get(i-1).getProfile() != null) {
                                        Utils.progressImg(stuList.get(i-1).getProfile(),dp,CheckReportCard.this);
                                        // Picasso.with(getActivity()).load(stuList.get(i-1).getProfile()).placeholder(R.drawable.stud).into(dp);
                                    } else {
                                        Picasso.get().load(R.drawable.stud).into(dp);
                                    }
                                }else{
                                    if (stuList.get(i-1).getProfile() != null) {
                                        Utils.progressImg(stuList.get(i-1).getProfile(),dp,CheckReportCard.this);
                                        //  Picasso.with(getActivity()).load(stuList.get(i-1).getProfile()).placeholder(R.drawable.f_student).into(dp);
                                    } else {
                                        Picasso.get().load(R.drawable.f_student).into(dp);
                                    }
                                }

                                edtFather.setText(stuList.get(i-1).getFather_name());
                                edtName.setText(stuList.get(i-1).getName());
                                edtRegistration.setText(stuList.get(i-1).getClass_name()+"("+stuList.get(i-1).getId()+")");
                                edtMobile.setText(stuList.get(i-1).getPhoneno());

                                a_sign = stuList.get(i-1).getA_signature();
                                s_sign = stuList.get(i-1).getS_signature();

                            } else {

                                stu_id = "";
                            }

                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                buttonnxtdetil.setVisibility(View.GONE);
                student_lyt.setVisibility(View.GONE);
            }
        }
    };

    private boolean checkValid() {
        boolean valid = true;
        String errorMsg = null;

        if (acdemic == null) {
            valid = false;
            errorMsg = "Kindly select Academic Year";
        }
        else if (examid.isEmpty() || examid == "") {
            valid = false;
            errorMsg = "Kindly select Exam name";

        }

        else if (classid.isEmpty() || classid == "") {
            valid = false;
            errorMsg = "Kindly select class";

        }
        else if (sectionid.isEmpty() || sectionid == "") {
            valid = false;
            errorMsg = "Kindly select class section";
        }
//        else
//        if (stu_id.isEmpty() || stu_id == "") {
//            valid = false;
//            errorMsg = "Kindly select class Student";
//        }


        if (!valid) {
           // Utils.showSnackBar(errorMsg, parent);
            Toast.makeText(getApplicationContext(),errorMsg,Toast.LENGTH_SHORT).show();
        }
        return valid;
    }
    @OnClick(R.id.buttonnxtdetil)
    public void nextbutton(){

        if (checkValid()) {
            if (tag.equalsIgnoreCase("gazette")) {
                Intent intent = new Intent(CheckReportCard.this, GazetteWebviewActivity.class);
                intent.putExtra("id", stu_id);
                intent.putExtra("examid", examid);
                intent.putExtra("exam_name", exam_name);
                intent.putExtra("name", edtName.getText().toString());
                intent.putExtra("f_name", edtFather.getText().toString());
                intent.putExtra("class", classid);
                intent.putExtra("class_name", classname);
                intent.putExtra("sectionid", sectionid);
                intent.putExtra("sectionname", sectionname);

                intent.putExtra("logo", logo);
                intent.putExtra("school", school);
                intent.putExtra("a_sign", a_sign);
                intent.putExtra("s_sign", s_sign);

                intent.putExtra("dob", dob);
                intent.putExtra("mother_name", mother_name);
                intent.putExtra("profile", profile);
//            intent.putExtra("name", name);
                intent.putExtra("address", address);

                intent.putExtra("session", acdemic.getStart_date()+acdemic.getEnd_date());
                intent.putExtra("todate", acdemic.getEnd_date());
                startActivity(intent);
            } else {
                if (stu_id.isEmpty() || stu_id == ""){
                    Toast.makeText(getApplicationContext(),"Kindly select class Student",Toast.LENGTH_SHORT).show();
                }else{
                    Intent intent = new Intent(CheckReportCard.this, ReportCard.class);
                    intent.putExtra("id", stu_id);
                    intent.putExtra("examid", examid);
                    intent.putExtra("exam_name", exam_name);
                    intent.putExtra("name", edtName.getText().toString());
                    intent.putExtra("f_name", edtFather.getText().toString());
                    intent.putExtra("class", classid);
                    intent.putExtra("class_name", classname);
                    intent.putExtra("sectionid", sectionid);
                    intent.putExtra("sectionname", sectionname);

                    intent.putExtra("logo", logo);
                    intent.putExtra("school", school);
                    intent.putExtra("a_sign", a_sign);
                    intent.putExtra("s_sign", s_sign);

                    intent.putExtra("dob", dob);
                    intent.putExtra("mother_name", mother_name);
                    intent.putExtra("profile", profile);
                    intent.putExtra("name", stuname);
                    intent.putExtra("address", address);

                    intent.putExtra("fromdate", acdemic.getStart_date());
                    intent.putExtra("todate", acdemic.getEnd_date());
                    startActivity(intent);

                }

            }

        }

    }


}