package com.ultimate.ultimatesmartschool.Examination;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.Request;
import com.google.gson.Gson;
import com.ultimate.ultimatesmartschool.BeanModule.ClassBean;
import com.ultimate.ultimatesmartschool.BeanModule.CommonBean;
import com.ultimate.ultimatesmartschool.BeanModule.SectionBean;
import com.ultimate.ultimatesmartschool.BeanModule.Studentbean;
import com.ultimate.ultimatesmartschool.BeanModule.User;
import com.ultimate.ultimatesmartschool.Homework.SectionnewAdapter;
import com.ultimate.ultimatesmartschool.Login.LoginActivity;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.StudentAttendance.Spinner_stu_adapterGds;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.CommonProgress;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class GDSUploadExammrksSubjectWise extends AppCompatActivity implements ExamdetailadapterSubjectwise.Mycallback{
    @BindView(R.id.imgBack)
    ImageView back;
    @BindView(R.id.spinneracdmicyrsec)
    Spinner acadmicyrspin;
    @BindView(R.id.spinnerClasssec)Spinner classspin;
    @BindView(R.id.spinnerGroupsec)Spinner groupspin;
    @BindView(R.id.spinnerexamsec)Spinner exmnamspinn;
    @BindView(R.id.recyclerViewsdetail)
    RecyclerView recyclerView;
    RecyclerView.LayoutManager layoutManager;
    @BindView(R.id.buttonnxtdetil)
    Button btnnextdt;
    @BindView(R.id.exmdetal)
    RelativeLayout parent;
    @BindView(R.id.ggdetail)
    LinearLayout fstlayout;

    @BindView(R.id.instruction)
    TextView instruction;

    Instruction_Adapter instruction_adapter;
    private ArrayList<Instruction> instructions = new ArrayList<>();

    @BindView(R.id.instructiont)
    TextView instructiont;

    @BindView(R.id.lytHeader)
    LinearLayout lytHeader;

    @BindView(R.id.lllll)RelativeLayout scndlayout;
    @BindView(R.id.groupnm)
    TextView groupname;
    @BindView(R.id.classnm)TextView classname;
    @BindView(R.id.examnam)TextView examnaname;
    @BindView(R.id.acdmicyr)TextView academicyr;
    private ArrayList<CommonBean> groupList = new ArrayList<>();
    public ArrayList<ClassBean> classList = new ArrayList<>();
    private ArrayList<Exambean> examList = new ArrayList<>();
    ArrayList<Sessionexambean> sessionlist = new ArrayList<>();
    ArrayList<Examdetailbean> examdetaillist = new ArrayList<>();
    private String groupid = "";
    private String classid = "";
    private String examid="";
    private String acdemicde="";
    private String exmnm="";
    private String grpnm="";
    private String clssnm="";
    private ClassAdapter classadapter;
    private Exmnamespinadapter exmnamespinadapter;
    private Acadmicyrspinadpter acadmicyrspinadpter;
    private ExamdetailadapterSubjectwise examdetailadapter;
    private int loaded = 0;
    private Sessionexambean acdemic;
    @BindView(R.id.txtTitle)
    TextView txtTitle;

    ArrayList<SectionBean> sectionList = new ArrayList<>();
    private ArrayList<Studentbean> stuList;
    private Spinner_stu_adapterGds adapterstu;
    String sectionid,sectionname="";
    CommonProgress commonProgress;
    String viewwwww = "";
    Hall_ticket_adapter examdetailadapter_new;

    @BindView(R.id.recyclerView12)
    RecyclerView recyclerViewInst;

    @BindView(R.id.spinnersection)Spinner spinnersection;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_g_d_s_upload_exammrks_subject_wise);
        ButterKnife.bind(this);
        commonProgress=new CommonProgress(this);

        viewwwww = getIntent().getStringExtra("view");


        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        scndlayout.setVisibility(View.GONE);
        fetchgrouplist();
        fetchexamnamelist();
        fetchAcademicyearlist();
        layoutManager = new LinearLayoutManager(GDSUploadExammrksSubjectWise.this);
        recyclerView.setLayoutManager(layoutManager);

        if (viewwwww.equalsIgnoreCase("admit_card")) {
            txtTitle.setText("Admit Card");
            examdetailadapter_new = new Hall_ticket_adapter(examdetaillist, GDSUploadExammrksSubjectWise.this, 2);
            recyclerView.setAdapter(examdetailadapter_new);

            recyclerViewInst.setLayoutManager(new LinearLayoutManager(GDSUploadExammrksSubjectWise.this));
            instruction_adapter = new Instruction_Adapter(instructions, GDSUploadExammrksSubjectWise.this);
            recyclerViewInst.setAdapter(instruction_adapter);

        } else {
            txtTitle.setText("Upload Marks Subject-Wise");
            examdetailadapter = new ExamdetailadapterSubjectwise(examdetaillist, GDSUploadExammrksSubjectWise.this,this);
            recyclerView.setAdapter(examdetailadapter);
        }


    }

    private void fetchgrouplist() {
        commonProgress.show();
        HashMap<String, String> params = new HashMap<String, String>();
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.GROUP_LIST_URL, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                commonProgress.dismiss();
                if (error == null) {
                    try {
                        groupList.clear();
                        groupList = CommonBean.parseCommonArray(jsonObject.getJSONArray(Constants.GROUP_DATA));
                        final GroupSpinnerAdapter adapter = new GroupSpinnerAdapter(GDSUploadExammrksSubjectWise.this, groupList,0);
                        groupspin.setAdapter(adapter);
                        groupspin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                if (position > 0) {
                                    groupid = groupList.get(position - 1).getId();
                                    grpnm=groupList.get(position-1).getName();
                                    classid = "";
                                    fetchClasslist(groupid);
                                } else {
                                    if (classadapter != null) {
                                        classList.clear();
                                        classid = "";
                                        groupid = "";
                                        classadapter.notifyDataSetChanged();
                                    }
                                }
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }else{
                    Utils.showSnackBar(error.getMessage(), parent);
                }
            }
        },this,params);
    }

    private void fetchClasslist(String groupid) {
        commonProgress.show();
        HashMap<String, String> params = new HashMap<String, String>();
        params.put(Constants.ID, groupid);
        params.put("check","true");
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.GROUPCLASSLIST_URL, classapiCallback, this, params);
    }
    ApiHandler.ApiCallback classapiCallback=new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            commonProgress.dismiss();
            if (error == null) {
                try {
                    classList = ClassBean.parseClassArray(jsonObject.getJSONArray(Constants.CLASSDATA));
                    classadapter = new ClassAdapter(GDSUploadExammrksSubjectWise.this, classList);
                    classspin.setAdapter(classadapter);
                    classspin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            if (position > 0) {
                                classid = classList.get(position - 1).getId();
                                clssnm=classList.get(position-1).getName();
                                //fetchsujectname(classid);
                                fetchsection();
                            } else {
                                classid = "";
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }else{
                Utils.showSnackBar(error.getMessage(), parent);
            }
        }
    };
    private void fetchsection() {
        commonProgress.show();
        HashMap<String, String> params = new HashMap<String, String>();
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.SECTIONLIST_URL, sectionapiCallback, this, params);
    }

    ApiHandler.ApiCallback sectionapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            commonProgress.dismiss();
            if (error == null) {
                try {
                    sectionList = SectionBean.parseCommonArray(jsonObject.getJSONArray("hostelroom_data"));
                    SectionnewAdapter adapter = new SectionnewAdapter(GDSUploadExammrksSubjectWise.this, sectionList);
                    spinnersection.setAdapter(adapter);
                    spinnersection.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if (i > 0) {
                                sectionid = sectionList.get(i - 1).getSection_id();
                                sectionname = sectionList.get(i - 1).getSection_name();
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Utils.showSnackBar(error.getMessage(), parent);
                if (error.getStatusCode() == 405) {
                    User.logout();
                    startActivity(new Intent(GDSUploadExammrksSubjectWise.this, LoginActivity.class));
                    finish();
                }
            }
        }
    };

    private void fetchexamnamelist() {
        HashMap<String, String> params = new HashMap<>();
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.addexam, apiCallback, this, params);

    }

    ApiHandler.ApiCallback apiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            if(error==null) {
                try {
                    JSONArray jsonArray = jsonObject.getJSONArray("examname_data");
                    examList = Exambean.parseexamArray(jsonArray);
                    exmnamespinadapter = new Exmnamespinadapter(GDSUploadExammrksSubjectWise.this, examList);
                    exmnamspinn.setAdapter(exmnamespinadapter);
                    exmnamspinn.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            if (position > 0) {
                                examid = examList.get(position - 1).getId();
                                exmnm = examList.get(position - 1).getExamname();
                            } else {
                                examid = "";
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }else{
                Utils.showSnackBar(error.getMessage(), parent);
            }
        }
    };


    private void fetchAcademicyearlist() {
        HashMap<String, String> params = new HashMap<>();

        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.COLGACADEMICYAER_URL, apiCallbackacdemicyear, this, params);
        //ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.SESSION_URL, apiCallbackacdemicyear, this, params);
    }

    ApiHandler.ApiCallback apiCallbackacdemicyear = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            if(error==null) {
                try {
                    JSONArray jsonArray = jsonObject.getJSONArray("session_data");
                    sessionlist = Sessionexambean.parsesessionArray(jsonArray);
                    acadmicyrspinadpter = new Acadmicyrspinadpter(GDSUploadExammrksSubjectWise.this, sessionlist);
                    acadmicyrspin.setAdapter(acadmicyrspinadpter);
                    acadmicyrspin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            if (position > 0) {
                                acdemic = sessionlist.get(position - 1);
                                acdemicde = Utils.getDateFormated(sessionlist.get(position - 1).getStart_date()) + " to "+ Utils.getDateFormated(sessionlist.get(position - 1).getEnd_date());

                            } else {
                                acdemic = null;
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }else{
                Utils.showSnackBar(error.getMessage(), parent);
            }
        }
    };
    @OnClick(R.id.buttonnxtdetil)
    public void nextbutton(){

        if (checkValid()) {

//            if (viewwwww.equalsIgnoreCase("admit_card")) {
//                fetchexamdetailNew();
//            } else {
            fetchexamdetail();
            //    }

            groupname.setText(grpnm);
            classname.setText(clssnm +"-"+sectionname);
            examnaname.setText(exmnm);
            academicyr.setText(acdemicde);
            fstlayout.setVisibility(View.GONE);
            scndlayout.setVisibility(View.VISIBLE);
        } else {
            fstlayout.setVisibility(View.VISIBLE);

        }


    }



    public void fetchexamdetail(){
        String acdexm_year = acdemic.getStart_date() + acdemic.getEnd_date();
        if (loaded == 0) {
            commonProgress.show();
        }
        loaded++;
        HashMap<String, String> params = new HashMap<>();
        params.put("grp_id",groupid);
        params.put("class_id",classid);
        params.put("exam_id",examid);
        params.put("userid",User.getCurrentUser().getId());
        params.put("acadmic_year",acdexm_year);
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.Examdetail, apiCallbackexmdetail, this, params);

    }

    ApiHandler.ApiCallback apiCallbackexmdetail = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            commonProgress.dismiss();
            if (error == null) {
                try {
                    if(examdetaillist!=null){
                        examdetaillist.clear();
                    }
                    JSONArray jsonArray = jsonObject.getJSONArray("examdetail_data");
                    examdetaillist = Examdetailbean.parseexamDetal_Array(jsonArray);

                    if (viewwwww.equalsIgnoreCase("admit_card")) {
                        Log.d("TAG", "onDataFetched: " + examdetaillist.size());
                        examdetailadapter_new.setexmdetailList(examdetaillist);
                        examdetailadapter_new.notifyDataSetChanged();
//                        txtNorecord.setVisibility(View.GONE);
//                        instructiont.setVisibility(View.VISIBLE);
//                        instruction.setVisibility(View.VISIBLE);
                        lytHeader.setVisibility(View.VISIBLE);
                        setData();

                        instruction.setVisibility(View.GONE);
                        fetchInstruction();

                    } else {
                        Log.d("TAG", "onDataFetched: " + examdetaillist.size());
                        examdetailadapter.setexmdetailList(examdetaillist);
                        examdetailadapter.notifyDataSetChanged();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
//                    txtNorecord.setVisibility(View.VISIBLE);
                instructiont.setVisibility(View.GONE);
                instruction.setVisibility(View.GONE);
                lytHeader.setVisibility(View.GONE);
                Utils.showSnackBar(error.getMessage(), parent);
                if (error.getStatusCode() == 405) {
                    User.logout();
                    startActivity(new Intent(GDSUploadExammrksSubjectWise.this, LoginActivity.class));
                    finish();
                }
            }
        }
    };

    public void fetchInstruction() {
        HashMap<String, String> params = new HashMap<>();
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.INSTRUCTION, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                if (error == null) {
                    try {
                        if (instructions != null) {
                            instructions.clear();
                        }

                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        //classnm=examdeta.getClass_name();
                        instructions = Instruction.parsesessionArray(jsonArray);

                        if (instructions.size() > 0) {
                            instruction_adapter.setSyllabusList(instructions);
                            //setanimation on adapter...
                            recyclerViewInst.getAdapter().notifyDataSetChanged();
                            recyclerViewInst.scheduleLayoutAnimation();
                            //-----------end------------
                            recyclerViewInst.setVisibility(View.VISIBLE);
                            instructiont.setVisibility(View.VISIBLE);

                        } else {
                            instruction_adapter.setSyllabusList(instructions);
                            instruction_adapter.notifyDataSetChanged();
                            recyclerViewInst.setVisibility(View.GONE);
                            instructiont.setVisibility(View.GONE);

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    // Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }, this, params);
    }

    private void setData() {

        String title1 = getColoredSpanned("" + "INSTRUCTION TO BE FOLLOWED:\n" + "", "#383737");
        String Name1 = getColoredSpanned("1. Assessment will be held during school hours.\n"
                + "2. There will be no preparatory holiday.\n"
                + "3. Snacks will be served on the day of assessment at 11 A.M.\n"
                + "4. School will be closed at 1:30 P.M after serving Brunch.\n"
                + "5. Summer Vacation will commence from June 4th, 2023.\n"
                + "6. Result will be shared/ Uploaded on ‘Student Management App’.", "#5A5C59");
        instructiont.setText(Html.fromHtml(title1 + "\n" + "" + " "));
        instruction.setText("1. Assessment will be held during school hours.\n"
                + "2. There will be no preparatory holiday.\n"
                + "3. Snacks will be served on the day of assessment at 11 A.M.\n"
                + "4. School will be closed at 1:30 P.M after serving Brunch.\n"
                + "5. Summer Vacation will commence from June 4th, 2023.\n"
                + "6. Result will be shared/ Uploaded on ‘Student Management App’.");
    }

    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }
    private boolean checkValid() {
        boolean valid = true;
        String errorMsg = null;

        if (acdemic == null) {
            valid = false;
            errorMsg = "please select Academic Year";
        }else if (examid.isEmpty() || examid == "") {
            valid = false;
            errorMsg = "please select Exam name";

        } else if (groupid.isEmpty() || groupid == "") {
            valid = false;
            errorMsg = "Please select Group";
        } else if (classid.isEmpty() || classid == "") {
            valid = false;
            errorMsg = "Please select class";

        }else
        if (sectionid.isEmpty() || sectionid == "") {
            valid = false;
            errorMsg = "Please select Section";
        }

        if (!valid) {
            Utils.showSnackBar(errorMsg, parent);
        }
        return valid;
    }

    @Override
    public void onViewResultCallback(Examdetailbean examdetailbean) {

        Gson gson  = new Gson();
        String subdetail = gson.toJson(examdetailbean,Examdetailbean.class);
        Intent i = new Intent(GDSUploadExammrksSubjectWise.this, AddMarksSubWiseGDS.class);
        i.putExtra("subdetail",subdetail);
        i.putExtra("classid",classid);
        i.putExtra("section",sectionid);
        i.putExtra("check","update");
        startActivity(i);


    }

    @Override
    public void onUploadmarksCallback(Examdetailbean examdetailbean) {
        Gson gson  = new Gson();
        String subdetail = gson.toJson(examdetailbean,Examdetailbean.class);
        Intent i = new Intent(GDSUploadExammrksSubjectWise.this, AddMarksSubWiseGDS.class);
        i.putExtra("subdetail",subdetail);
        i.putExtra("classid",classid);
        i.putExtra("section",sectionid);
        i.putExtra("check","add");
        startActivity(i);
    }
}
