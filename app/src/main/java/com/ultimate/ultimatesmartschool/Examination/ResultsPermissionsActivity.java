package com.ultimate.ultimatesmartschool.Examination;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.ultimate.ultimatesmartschool.AddmissionForm.SessionAdapter;
import com.ultimate.ultimatesmartschool.AddmissionForm.SessionBean;
import com.ultimate.ultimatesmartschool.Fee.FeeDetail;
import com.ultimate.ultimatesmartschool.Fee.FeeDetailBean;
import com.ultimate.ultimatesmartschool.Fee.FeeDetaildapter;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Staff.CategoryAdapter;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.CommonProgress;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;
import com.ultimate.ultimatesmartschool.Utility.ErpProgressVert;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ResultsPermissionsActivity extends AppCompatActivity {
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.spinnerFinancialYear)
    Spinner spinnerFinancialYear;
    @BindView(R.id.spinnerFinancialYear1)
    Spinner spinnerexmname;
    @BindView(R.id.root1)
    RelativeLayout root1;
    @BindView(R.id.parent)
    RelativeLayout parent;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    private CategoryAdapter adapter;
    private LinearLayoutManager layoutManager;

    Animation animation;
    @BindView(R.id.dialog)
    ImageView dialog;
    @BindView(R.id.vieww)
    View vieww;

    @BindView(R.id.send)
    Button send;

    SharedPreferences sharedPreferences;
    @BindView(R.id.totalRecord)
    TextView totalRecord;
    @BindView(R.id.textNorecord)
    TextView txtNorecord;
    CommonProgress commonProgress;

    ArrayList<ResultPermissionBean> list=new ArrayList<>();
    private ResultPermissionAdapter mAdapter;

    private ArrayList<Exambean> examList = new ArrayList<>();
    Exmnamespinadapter exmnamespinadapter;
    private String examid = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fee_detail);
        ButterKnife.bind(this);
        txtTitle.setText("Add Result Permission");

        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        mAdapter = new ResultPermissionAdapter(this, list);
        recyclerView.setAdapter(mAdapter);

        txtNorecord.setVisibility(View.VISIBLE);
        dialog.setVisibility(View.GONE);
        root1.setVisibility(View.VISIBLE);
        vieww.setVisibility(View.VISIBLE);
        commonProgress=new CommonProgress(this);
        animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.btn_blink_animation);
        fetchSession();
    }

    private void fetchSession() {
        commonProgress.show();
        HashMap<String, String> params = new HashMap<String, String>();
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.SESSION_URL, sessionapiCallback, this, params);
    }

    ApiHandler.ApiCallback sessionapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            commonProgress.dismiss();
            if (error == null) {
                try {
                    final ArrayList<SessionBean> sessionlist = SessionBean.parseSessionArray(jsonObject.getJSONArray(Constants.SESSIONDATA));
                    SessionAdapter adapter = new SessionAdapter(ResultsPermissionsActivity.this, sessionlist);
                    spinnerFinancialYear.setAdapter(adapter);
                    spinnerFinancialYear.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            SessionBean session = sessionlist.get(i);
                            // fetchFeeDetail(session);
                            fetchexamname(session);
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Utils.showSnackBar(error.getMessage(), parent);
            }
        }
    };

    private void fetchexamname(SessionBean session) {
        commonProgress.show();
        HashMap<String, String> params = new HashMap<>();

        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.addexam, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                commonProgress.dismiss();
                if (error == null) {
                    try {
                        JSONArray jsonArray = jsonObject.getJSONArray("examname_data");
                        examList = Exambean.parseexamArray(jsonArray);
                        exmnamespinadapter = new Exmnamespinadapter(ResultsPermissionsActivity.this, examList);
                        spinnerexmname.setAdapter(exmnamespinadapter);
                        spinnerexmname.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                if (position > 0) {
                                    examid = examList.get(position - 1).getId();
                                    fetchExamDetail(session,examid);
                                } else {
                                    examid = "";
                                }

                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }else{
                    Utils.showSnackBar(error.getMessage(), parent);
                }
            }
        }, this, params);

    }

    private void fetchExamDetail(SessionBean acdemic,String examid) {
        String acdexm_year = acdemic.getStart_date() + acdemic.getEnd_date();

        commonProgress.show();
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("exam_id",examid);
        params.put("acadmic_year",acdexm_year);
        String url = Constants.getBaseURL() + Constants.RESULT_URL;
        ApiHandler.apiHit(Request.Method.POST, url, apiCallback5, this, params);
    }

    ApiHandler.ApiCallback apiCallback5 = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            commonProgress.dismiss();
            if (error == null) {
                try {
                    if (list != null)
                        list.clear();
                    list = ResultPermissionBean.parseClassArray(jsonObject.getJSONArray("exam_data"));
                    mAdapter.setDataList(list);
                    mAdapter.notifyDataSetChanged();
                    txtNorecord.setVisibility(View.GONE);
                    totalRecord.setText("Total Entries:- "+String.valueOf(list.size()));
                    send.setVisibility(View.VISIBLE);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                send.setVisibility(View.GONE);
                totalRecord.setText("Total Entries:- 0");
                txtNorecord.setVisibility(View.VISIBLE);
                if (list != null)
                    list.clear();
                mAdapter.setDataList(list);
                mAdapter.notifyDataSetChanged();
                Utils.showSnackBar(error.getMessage(), parent);
            }
        }
    };


    @OnClick(R.id.imgBack)
    public void backClick() {
        finish();
    }



    @OnClick(R.id.send)
    public void send() {
        commonProgress.show();
        list =mAdapter.getAttendList();
        ArrayList<String> attendList = new ArrayList<>();
        ArrayList<String> id = new ArrayList<>();
        ArrayList<String> st_id = new ArrayList<>();
        ArrayList<String> st_name = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            attendList.add(list.get(i).getResult_status());
            st_id.add(list.get(i).getGroup_name());
            id.add(list.get(i).getId());
            st_name.add(list.get(i).getClass_name());
        }
        Log.e("list: ",attendList.toString().trim()+","+id.toString().trim()+","+st_id.toString().trim()+","+st_name.toString().trim());

        HashMap<String, String> params = new HashMap<String, String>();
        params.put("attend", String.valueOf(attendList));
        params.put("id", String.valueOf(id));
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.ADD_RESULT_URL, uploadtestmarksapiCallback, this, params);
    }
    ApiHandler.ApiCallback uploadtestmarksapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgressVert.cancelProgressBar();
            commonProgress.dismiss();
            if (error == null) {
                    Toast.makeText(ResultsPermissionsActivity.this, "UPDATED SUCCESSFULLY", Toast.LENGTH_SHORT).show();
                finish();
            } else {
                Toast.makeText(ResultsPermissionsActivity.this,error.getMessage(), Toast.LENGTH_SHORT).show();
                Log.e("16955", error.getMessage() + "");
            }
        }
    };
}