package com.ultimate.ultimatesmartschool.Examination;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.SwitchCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.ultimate.ultimatesmartschool.BeanModule.ClassBean;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.SchoolSetup.ClassesAdapter;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ResultPermissionAdapter extends RecyclerView.Adapter<ResultPermissionAdapter.MyViewHolder> {

//    private final ResultPermissionAdapter.AddNewGroup mAddNewGroup;
    private Context mContext;
    ArrayList<ResultPermissionBean> dataList;

    ResultPermissionAdapter(Context mContext, ArrayList<ResultPermissionBean> dataList) {
        this.mContext = mContext;
        this.dataList = dataList;
    }

    public void setDataList(ArrayList<ResultPermissionBean> dataList) {
        this.dataList = dataList;
    }

    public ArrayList<ResultPermissionBean> getAttendList() {
        return dataList;
    }

    @Override
    public ResultPermissionAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        LayoutInflater inflater = LayoutInflater.from(
                parent.getContext());
        View v =
                inflater.inflate(R.layout.gcs_common_lyt_new, parent, false);
        // set the view's size, margins, paddings and layout parameters
        ResultPermissionAdapter.MyViewHolder vh = new ResultPermissionAdapter.MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ResultPermissionAdapter.MyViewHolder holder, final int position) {

            ResultPermissionBean data=dataList.get(position);
            holder.lytAll.setVisibility(View.VISIBLE);
            holder.lytAddData.setVisibility(View.GONE);


            if (data.getClass_name()!=null) {
                holder.txtTitle.setText(data.getClass_name() + "-Class");
            }else {
                holder.txtTitle.setText("Not Mentioned");
            }

              if (data.getGroup_name()!=null) {
                    holder.txtSub.setText(data.getGroup_name() + "-Class");
                 }else {
                   holder.txtSub.setText("Not Mentioned");
                }

                if (data.getResult_status()!=null) {
                    String one="";
                    if (data.getResult_status().equalsIgnoreCase("yes")) {
                      holder.on_off_switch.setChecked(true);
                      one="yes";
                    } else {
                        holder.on_off_switch.setChecked(false);
                        one="no";
                    }
                   // Toast.makeText(mContext,data.getClass_name()+one, Toast.LENGTH_SHORT).show();
                    data.setResult_status(one);
                  }else {
                   holder.txtSub.setText("Not Mentioned");
                }

               holder.on_off_switch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                   @Override
                   public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                       String two="";
                       if (compoundButton.isChecked()){
                           two="yes";
                       }else {
                           two="no";
                       }
                      // Toast.makeText(mContext,data.getClass_name()+two+b, Toast.LENGTH_SHORT).show();
                       data.setResult_status(two);
                   }
               });




//            holder.lytAll.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    if (mAddNewGroup != null) {
//                        mAddNewGroup.addEditNewGroup(dataList.get(position));
//                    }
//                }
//            });
//            holder.imgTrash.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    if (mAddNewGroup != null) {
//                        mAddNewGroup.deleteGroup(dataList.get(position),position);
//                    }
//                }
//            });


    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txtTitle)
        TextView txtTitle;
        @BindView(R.id.txtSub)
        TextView txtSub;
        @BindView(R.id.on_off_switch)
        SwitchCompat on_off_switch;
        @BindView(R.id.lytAddData)
        RelativeLayout lytAddData;
        @BindView(R.id.lytAll)
        RelativeLayout lytAll;
        @BindView(R.id.txtAddText)
        TextView txtAddText;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            txtAddText.setText("Add Class");
        }
    }

//    public interface AddNewGroup {
//        public void addEditNewGroup(ResultPermissionBean data);
//        public void deleteGroup(ResultPermissionBean data);
//    }
}
