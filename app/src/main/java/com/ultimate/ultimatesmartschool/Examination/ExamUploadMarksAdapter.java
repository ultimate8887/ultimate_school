package com.ultimate.ultimatesmartschool.Examination;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.textfield.TextInputLayout;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ExamUploadMarksAdapter extends RecyclerView.Adapter<ExamUploadMarksAdapter.Viewholder> {

    ArrayList<Examdetailbean> examdetaillist;
    Context context;

    public ExamUploadMarksAdapter(ArrayList<Examdetailbean> examdetaillist, Context context) {
        this.context=context;
        this.examdetaillist=examdetaillist;

    }

    @NonNull
    @Override
    public ExamUploadMarksAdapter.Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.exmuploadmarksdetailada_lay, parent, false);
        ExamUploadMarksAdapter.Viewholder viewholder = new ExamUploadMarksAdapter.Viewholder(view,new passMarksListner());
        return viewholder;
    }

    @Override
    public void onBindViewHolder(@NonNull ExamUploadMarksAdapter.Viewholder holder, int position) {
        holder.subname.setText((CharSequence) examdetaillist.get(position).getSubject_name());
        holder.doe.setText(Utils.getDateFormated( examdetaillist.get(position).getExam_date()));
        holder.textexamduration.setText("Duration:" + " " + examdetaillist.get(position).getExam_duration());
        holder.exmfullmrks.setText("Total Marks:" + " " + examdetaillist.get(position).getTotal_marks());
        holder.exmpassmrksss.setText("Pass Marks:" + " "+ examdetaillist.get(position).getPass_marks());
        if(examdetaillist.get(position).getGiven_marks()!=null){
            holder.exmobtainmrksss.setVisibility(View.VISIBLE);
            //holder.exmobtainmrksss.setText("Obtained Marks: "+examdetaillist.get(position).getGet_marks());
            holder.exmobtainmrksss.setText("Marks id: "+examdetaillist.get(position).getMarksid());
            holder.passmrks.setHint("Want to Update Marks ?");
//    holder.myCustomerEditListnerpassmk.updatePositionthird(holder.getAdapterPosition());
//    holder.passmrks.getEditText().setText(examdetaillist.get(position).getGet_marks());
        }else {
            holder.exmobtainmrksss.setVisibility(View.GONE);

        }
        holder.myCustomerEditListnerpassmk.updatePositionthird(holder.getAdapterPosition());
        holder.passmrks.getEditText().setText(examdetaillist.get(holder.getAdapterPosition()).getGiven_marks());
    }

    @Override
    public int getItemCount() {
        return examdetaillist.size();
    }

    public void setexmdetailList(ArrayList<Examdetailbean> examdetaillist) {
        this.examdetaillist = examdetaillist;
    }
    public ArrayList<Examdetailbean> getExamdetailmarksList() {
        return examdetaillist;
    }

    public class Viewholder extends RecyclerView.ViewHolder {
        @BindView(R.id.textsubjectnm)
        TextView subname;
        @BindView(R.id.textdateofexm)TextView doe;
        @BindView(R.id.textexamduration)TextView textexamduration;
        @BindView(R.id.exmfullmrks)TextView exmfullmrks;
        @BindView(R.id.exmpassmrksss)TextView exmpassmrksss;
        @BindView(R.id.exmobtainmrksss)TextView exmobtainmrksss;
        @BindView(R.id.passmrk)
        TextInputLayout passmrks;
        public passMarksListner myCustomerEditListnerpassmk;
        public Viewholder(View itemView,passMarksListner myCustomerEditListnerpassmk) {
            super(itemView);
            ButterKnife.bind(this,itemView);

            this.myCustomerEditListnerpassmk = myCustomerEditListnerpassmk;
            this.passmrks.getEditText().addTextChangedListener(myCustomerEditListnerpassmk);
        }
    }


    private class passMarksListner implements TextWatcher {
        private int position;

        public void updatePositionthird(int position) {
            this.position = position;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            examdetaillist.get(position).setGiven_marks(String.valueOf(s));

        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    }
}
