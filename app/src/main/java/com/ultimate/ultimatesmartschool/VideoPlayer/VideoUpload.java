package com.ultimate.ultimatesmartschool.VideoPlayer;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;

import com.android.volley.Request;


import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.provider.MediaStore;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.ultimate.ultimatesmartschool.BeanModule.ClassBean;
import com.ultimate.ultimatesmartschool.BeanModule.SubjectBeancls;
import com.ultimate.ultimatesmartschool.BeanModule.User;
import com.ultimate.ultimatesmartschool.Examination.ClassAdapter;
import com.ultimate.ultimatesmartschool.Homework.AdapterOfSubjectList;
import com.ultimate.ultimatesmartschool.Login.LoginActivity;
import com.ultimate.ultimatesmartschool.OnlineClass.AddOnlineClassLink;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.CommonProgress;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;
import com.ultimate.ultimatesmartschool.Utility.ErpProgressVert;
import com.ultimate.ultimatesmartschool.Utility.FileUtils;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.apptik.widget.multiselectspinner.BaseMultiSelectSpinner;
import io.apptik.widget.multiselectspinner.MultiSelectSpinner;

public class VideoUpload extends AppCompatActivity implements View.OnClickListener{
    ArrayList<ClassBean> classList = new ArrayList<>();
    private TextView buttonChoose;
    private Button buttonUpload;
    private TextView textView;
    String classid = "";
    private TextView textViewResponse;
    String fileBase64 = null;
    private static final int SELECT_VIDEO = 3;
    List<String> classidsel;
    private String selectedPath;
    @BindView(R.id.parent)
    RelativeLayout parent;
    @BindView(R.id.spinnerClass)
    Spinner spinnerClass;
    private int loaded = 0;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    ArrayList<SubjectBeancls> subjectList = new ArrayList<>();
    AdapterOfSubjectList adaptersub;
    String sub_id="", sub_name="";
    @BindView(R.id.spinnersection)
    Spinner spinnersection;
    @BindView(R.id.title)
    EditText title;
    CommonProgress commonProgress;
    float  length;
    String size;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_upload);
        ButterKnife.bind(this);
        commonProgress=new CommonProgress(this);
      //  parent.setAnimation(AnimationUtils.loadAnimation(getApplicationContext(),R.anim.fade_scale_animation));
        classidsel = new ArrayList<>();
        fetchClass();
        txtTitle.setText("Upload Video");
        buttonChoose = (TextView) findViewById(R.id.buttonChoose);
        buttonUpload = (Button) findViewById(R.id.buttonUpload);

        textView = (TextView) findViewById(R.id.textView);
        //textViewResponse = (TextView) findViewById(R.id.textViewResponse);

        buttonChoose.setOnClickListener(this);
        buttonUpload.setOnClickListener(this);
    }
    @OnClick(R.id.imgBack)
    public void onback() {
        finish();
    }

    private void fetchClass() {
        if (loaded == 0) {
            commonProgress.show();
        }
        loaded++;

        HashMap<String, String> params = new HashMap<String, String>();
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.CLASSLIST_URL, classapiCallback,getApplicationContext(), params);
    }


    ApiHandler.ApiCallback classapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            commonProgress.dismiss();
            if (error == null) {
                try {
                    classList = ClassBean.parseClassArray(jsonObject.getJSONArray(Constants.CLASSDATA));
                    ClassAdapter adapter = new ClassAdapter(getApplicationContext(), classList);
                    spinnerClass.setAdapter(adapter);
                    spinnerClass.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if (i > 0) {
                                classid = classList.get(i - 1).getId();
                                fetchSubject();
                            }

                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_LONG).show();
                // Utils.showSnackBar(error.getMessage(), view.findViewById(R.id.parent));
            }
        }
    };

    private void fetchSubject() {
        commonProgress.show();
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("id", classid);
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.SubjectList, subapiCallback, this, params);

    }

    ApiHandler.ApiCallback subapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            /* */
            commonProgress.dismiss();
            if (error == null) {
                try {

                    subjectList =   SubjectBeancls.parseClassArray(jsonObject.getJSONArray("sub_data"));

                    adaptersub = new AdapterOfSubjectList(VideoUpload.this, subjectList);
                    spinnersection.setAdapter(adaptersub);
                    spinnersection.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if (i != 0) {
                                sub_name = subjectList.get(i - 1).getName();
                                sub_id = subjectList.get(i - 1).getId();

                            }

                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    };

    private void chooseVideo() {
        Intent intent = new Intent();
        intent.setType("video/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivityForResult(Intent.createChooser(intent, "Select a Video"), SELECT_VIDEO);


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == SELECT_VIDEO) {
                System.out.println("SELECT_VIDEO");
                Uri selectedImageUri = data.getData();
                // selectedPath = getPath(selectedImageUri);
                selectedPath = FileUtils.getRealPath(this, selectedImageUri);
                File file = new File(selectedPath);
                length = file.length();
                length = length/1000000;
                size = formatSize(file.length());
                try {
                    if((float)length >30.0f){
                        Toast.makeText(getApplicationContext(),"Sorry, Your selected file size too large("+size+"), Kindly compress your file first to upload!" +
                                "(Max Size of file uploading should be less than 25MB)",Toast.LENGTH_LONG).show();                    }else {
                        fileBase64 = Utils.encodeFileToBase64Binary(selectedPath);
                        Bitmap bitmap = ThumbnailUtils.createVideoThumbnail(selectedPath, MediaStore.Video.Thumbnails.MICRO_KIND);
                        ByteArrayOutputStream stream = new ByteArrayOutputStream();
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 10, stream);
                        ((ImageView)findViewById(R.id.imageView6)).setImageBitmap(bitmap);
                        //  textView.setText(selectedPath+length+"mb");
                        textView.setVisibility(View.VISIBLE);
                        textView.setText("Path: " + selectedPath + "\nSize: " + size);
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }


            }
        } else {
            Toast.makeText(this, "Selection Cancelled", Toast.LENGTH_SHORT).show();
        }
    }

    public static String formatSize(long size) {
        String suffix = null;

        if (size >= 1024) {
            suffix = " Bytes";
            size /= 1024;
            if (size >= 1024) {
                suffix = " MB";
                size /= 1024;
            }
        }
        StringBuilder resultBuffer = new StringBuilder(Long.toString(size));

        int commaOffset = resultBuffer.length() - 3;
        while (commaOffset > 0) {
            resultBuffer.insert(commaOffset, ',');
            commaOffset -= 3;
        }
        if (suffix != null) resultBuffer.append(suffix);
        return resultBuffer.toString();
    }

    public String getPath(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        String document_id = cursor.getString(0);
        document_id = document_id.substring(document_id.lastIndexOf(":") + 1);
        cursor.close();

        cursor = getContentResolver().query(
                android.provider.MediaStore.Video.Media.EXTERNAL_CONTENT_URI,
                null, MediaStore.Images.Media._ID + " = ? ", new String[]{document_id}, null);
        cursor.moveToFirst();
        @SuppressLint("Range") String path = cursor.getString(cursor.getColumnIndex(MediaStore.Video.Media.DATA));
        cursor.close();

        return path;
    }




    @Override
    public void onClick(View v) {
        if (v == buttonChoose) {
            chooseVideo();
        }
        if (v == buttonUpload) {
            uploadVideo2();
        }
    }

    private void uploadVideo2() {
        if (checkValid()) {
            commonProgress.show();
//        DebugLog.printLog("base6d", Utils.encodeFileToBase64Binary(myFile)+"   fgd    "+myFile);
            HashMap<String, String> params = new HashMap<String, String>();
            params.put("topic", title.getText().toString());
            params.put("class_id", classid);
            params.put("user_id", User.getCurrentUser().getId());
            params.put("sub_id", sub_id);
            // params.put("mark", edtMarks.getText().toString());
            params.put("last_date", "date");
            params.put("type", "video");
            if (fileBase64 != null)
                params.put("file", fileBase64);
            ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.ADDVIDEOlearn_URL, createassignmentapiCallback, this, params);
        }
    }
    ApiHandler.ApiCallback createassignmentapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgressVert.cancelProgressBar();
            commonProgress.dismiss();
            if (error == null) {
                Utils.showSnackBar("Upload Successfully", parent);
                finish();
            } else {
                Utils.showSnackBar(error.getMessage(), parent);
                Log.e("16955", error.getMessage() + "");
            }
        }
    };

    private boolean checkValid() {

        boolean valid = true;
        String errorMsg = null;

        if (sub_id.isEmpty() || sub_id == "") {
            valid = false;
            errorMsg = "Please select subject!";
        }

        else
        if (title.getText().toString().trim().length() <= 0) {
            valid = false;
            errorMsg = "Please enter  topic!";
        }



        if (!valid) {
            Utils.showSnackBar(errorMsg, parent);
        }
        return valid;
    }
}