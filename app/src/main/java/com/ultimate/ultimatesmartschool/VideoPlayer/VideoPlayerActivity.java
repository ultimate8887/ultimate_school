package com.ultimate.ultimatesmartschool.VideoPlayer;

import androidx.appcompat.app.AppCompatActivity;

import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.VideoView;

import com.google.gson.Gson;
import com.ultimate.ultimatesmartschool.OnlineClass.OnlineclsBean;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Utility.CommonProgress;
import com.ultimate.ultimatesmartschool.Utility.Constants;

import butterknife.BindView;
import butterknife.ButterKnife;

public class VideoPlayerActivity extends AppCompatActivity {
    private VideoView videoView;
    OnlineclsBean hostlroombean;
    boolean edit = false;
    String link="",tag="";
    ImageView imgback;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    CommonProgress commonProgress;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_player);
        ButterKnife.bind(this);
        commonProgress=new CommonProgress(this);
        videoView = findViewById(R.id.videoView);
        imgback = (ImageView) findViewById(R.id.imgBack);
        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        if (getIntent().getExtras() != null) {
                link = getIntent().getExtras().getString("video_data");
                tag = getIntent().getExtras().getString("video_name");
                txtTitle.setText(tag);
                edit = true;
            commonProgress.show();
            loadVideo(link);
        }

    }
    private void loadVideo(String link) {
        String video_id= Constants.getImageBaseURL()+link;
        Uri videoUri = Uri.parse(video_id);
        videoView.setVideoURI(videoUri);

        MediaController mediaController = new MediaController(this);
        mediaController.setAnchorView(videoView);
        videoView.setMediaController(mediaController);

        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                commonProgress.cancel();
                videoView.start();
            }
        });

        videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                commonProgress.cancel();
                // Handle video playback completion if needed
            }
        });
    }
}