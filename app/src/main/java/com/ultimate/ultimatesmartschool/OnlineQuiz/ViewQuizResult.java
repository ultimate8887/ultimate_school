package com.ultimate.ultimatesmartschool.OnlineQuiz;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.google.gson.Gson;
import com.ultimate.ultimatesmartschool.BeanModule.User;
import com.ultimate.ultimatesmartschool.Ebook.OnlineQuizBean;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ViewQuizResult extends AppCompatActivity implements OnlineQuizResAdapter.Mycallback{
    OnlineQuizBean onlineQuizBean;
    boolean edit = false;
    RecyclerView recyclerView;
    RelativeLayout parent;

    OnlineQuizResAdapter adapter;
    ArrayList<OnlineQuizResBean> ebooklist = new ArrayList<>();
    @BindView(R.id.textNorecord)
    TextView txtNorecord;
    ImageView imgback;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_quiz_result);
        ButterKnife.bind(this);
        txtTitle.setText("Online Quiz Result");
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView11);
        parent = (RelativeLayout) findViewById(R.id.testadd);

        imgback = (ImageView) findViewById(R.id.imgBack);
        recyclerView.setLayoutManager(new LinearLayoutManager(ViewQuizResult.this));
        adapter = new OnlineQuizResAdapter(ebooklist, ViewQuizResult.this, this);
        recyclerView.setAdapter(adapter);

        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        if (getIntent().getExtras() != null) {
            if (getIntent().getExtras().containsKey("onlineQuiz_data")) {
                Gson gson = new Gson();
                onlineQuizBean = gson.fromJson(getIntent().getExtras().getString("onlineQuiz_data"), OnlineQuizBean.class);
                edit = true;

            }
        }
        fetchOnlineQuizresult();
    }
    private void fetchOnlineQuizresult() {
        ErpProgress.showProgressBar(this, "Please wait...");
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("class_id", onlineQuizBean.getClass_id());
        params.put("quiz_id", onlineQuizBean.getQuiz_id());
        params.put("user_id", User.getCurrentUser().getId());
        params.put("s_key", User.getCurrentUser().getSchoolData().getFi_school_id());
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.getonlinequizresult, apiCallback, this, params);
    }

    ApiHandler.ApiCallback apiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            if (error == null) {
                try {
                    if (ebooklist != null) {
                        ebooklist.clear();
                    }
                    JSONArray jsonArray = jsonObject.getJSONArray("hostelroom_data");
                    ebooklist = OnlineQuizResBean.parseSyllabusArray(jsonArray);
                    if (ebooklist.size() > 0) {
                        adapter.setSyllabusList(ebooklist);
                        adapter.notifyDataSetChanged();
                        txtNorecord.setVisibility(View.GONE);

                    } else {
                        adapter.setSyllabusList(ebooklist);
                        adapter.notifyDataSetChanged();
                        txtNorecord.setVisibility(View.VISIBLE);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {

                txtNorecord.setVisibility(View.VISIBLE);
                ebooklist.clear();
                adapter.setSyllabusList(ebooklist);
                adapter.notifyDataSetChanged();
            }
        }
    };


    @Override
    public void onMethodCallback(OnlineQuizResBean onlineQuizBean) {

    }

    @Override
    public void viewPdf(OnlineQuizResBean onlineQuizBean) {

    }



}
