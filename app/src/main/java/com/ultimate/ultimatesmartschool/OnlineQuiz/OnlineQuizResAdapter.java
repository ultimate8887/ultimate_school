package com.ultimate.ultimatesmartschool.OnlineQuiz;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.ultimate.ultimatesmartschool.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OnlineQuizResAdapter extends RecyclerView.Adapter<OnlineQuizResAdapter.Viewholder> {
    ArrayList<OnlineQuizResBean> sy_list;
    Mycallback mAdaptercall;
    Context mContext;

    public OnlineQuizResAdapter(ArrayList<OnlineQuizResBean> sy_list, Context mContext, Mycallback mAdaptercall) {
        this.mAdaptercall = mAdaptercall;
        this.sy_list = sy_list;
        this.mContext = mContext;
    }


    @Override
    public Viewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.quizres_adpt_lyt, parent, false);
        Viewholder viewholder = new Viewholder(view);
        return viewholder;

    }

    @Override
    public void onBindViewHolder(Viewholder holder, final int position) {
        holder.txtPdf.setText("Score: "+sy_list.get(position).getScore());
        holder.txtSub.setText(sy_list.get(position).getStudent_name()+"("+sy_list.get(position).getStudent_id()+")");
    }

    public interface Mycallback {
        public void onMethodCallback(OnlineQuizResBean onlineQuizBean);

        public void viewPdf(OnlineQuizResBean onlineQuizBean);
    }

    @Override
    public int getItemCount() {
        return sy_list.size();
    }

    public void setSyllabusList(ArrayList<OnlineQuizResBean> sy_list) {
        this.sy_list = sy_list;
    }

    public class Viewholder extends RecyclerView.ViewHolder {
        @BindView(R.id.txtPdf)
        TextView txtPdf;
        @BindView(R.id.imgData)
        ImageView imgData;
        @BindView(R.id.txttitle)
        TextView txttitle;
        @BindView(R.id.txtSub)
        TextView txtSub;
        @BindView(R.id.txtwriter)
        TextView txtwriter;
        @BindView(R.id.txtauthor)
        TextView txtauthor;
        @BindView(R.id.txttime)TextView txttime;
        @BindView(R.id.starttest)TextView starttest;
        public Viewholder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }



}
