package com.ultimate.ultimatesmartschool.Query;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.ultimate.ultimatesmartschool.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class QueeryAdapter extends RecyclerView.Adapter<QueeryAdapter.ViewHolder> {
    Context context;
    private Adaptercall mAdaptercall;
    private List<QuerryBean> MainImageUploadInfoList;

    public QueeryAdapter(Context context, List<QuerryBean> MainImageUploadInfoList, Adaptercall mAdaptercall) {
        this.MainImageUploadInfoList = MainImageUploadInfoList;
        this.context = context;
        this.mAdaptercall = mAdaptercall;
    }

    @Override
    public QueeryAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recylerquerry, parent, false);
        QueeryAdapter.ViewHolder viewHolder = new QueeryAdapter.ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final QueeryAdapter.ViewHolder holder, final int position) {
        final QuerryBean querryBean = MainImageUploadInfoList.get(position);
        holder.bind(querryBean, context);
        holder.chatclick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAdaptercall.methodcall(MainImageUploadInfoList.get(position));
            }
        });
    }

    @Override
    public int getItemCount() {
        return MainImageUploadInfoList.size();
    }

    public void setQuareyList(ArrayList<QuerryBean> MainImageUploadInfoList) {
        this.MainImageUploadInfoList = MainImageUploadInfoList;
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tittle)
        TextView txttittle;
        @BindView(R.id.coom)
        TextView comment;
        @BindView(R.id.date)
        TextView date1;
        @BindView(R.id.txtClass)
        TextView txtClass;
        @BindView(R.id.txtName)
        TextView txtName;
        @BindView(R.id.txtFn)
        TextView txtFn;
        CardView chatclick;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            chatclick = (CardView) itemView.findViewById(R.id.chatclik);
        }

        public void bind(final QuerryBean item, Context context) {
            txttittle.setText(item.getTitle());
            comment.setText(item.getMsg());
            String your_format = "";
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm");
            Date date = null;
            try {
                date = sdf.parse(item.getDate());
                your_format = new SimpleDateFormat("dd MMM ,yyyy hh:mm a").format(date);
                date1.setText(your_format);
            } catch (ParseException e) {
                System.out.println(e.toString()); //date format error
            }
            Calendar cal = Calendar.getInstance();
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM ,yyyy");

            if (dateFormat.format(cal.getTime()).equals(new SimpleDateFormat("dd MMM ,yyyy").format(date))) {
                date1.setText(new SimpleDateFormat("hh:mm a").format(date));
            }

            txtClass.setText("Class: "+item.getClassX());
            txtName.setText("Name: "+item.getName()+"("+item.getId()+")");
            txtFn.setText("Father: "+item.getFather_name());
        }
    }

    public interface Adaptercall {
        public void methodcall(QuerryBean obj);

    }
}
