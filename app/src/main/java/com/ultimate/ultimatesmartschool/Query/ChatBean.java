package com.ultimate.ultimatesmartschool.Query;

public class ChatBean {

    public String getQuery_id() {
        return query_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public String getMsg() {
        return msg;
    }

    public ChatBean(String query_id, String user_id, String msg) {
        this.query_id = query_id;
        this.user_id = user_id;
        this.msg = msg;
    }

    String query_id;
    String user_id;
    String msg;
}
