package com.ultimate.ultimatesmartschool.Query;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import com.android.volley.Request;
import com.ultimate.ultimatesmartschool.BeanModule.User;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class Querry extends AppCompatActivity implements QueeryAdapter.Adaptercall{
    QueeryAdapter queerylisttadapter;
    ArrayList<QuerryBean> querrylist = new ArrayList<>();
    RecyclerView recyclerView;
    int loaded=0;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_querry);
        ButterKnife.bind(this);
        txtTitle.setText("Query");
        recyclerView=(RecyclerView)findViewById(R.id.querryrecycler);
        recyclerView.setLayoutManager(new LinearLayoutManager(Querry.this));
        fetchquerry();
        queerylisttadapter = new QueeryAdapter(Querry.this,querrylist,Querry.this);
        recyclerView.setAdapter(queerylisttadapter);
    }

    @OnClick(R.id.imgBack)
    public void callBack(){
        finish();
    }

    private void fetchquerry() {
        if(loaded==0){
            ErpProgress.showProgressBar(this,"please wait...");
        }loaded++;
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("user_id", User.getCurrentUser().getId());
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.querry1, querryapiCallback, this, params);
    }

    ApiHandler.ApiCallback querryapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            if (error == null) {
                try {
                    JSONArray jsonArray = jsonObject.getJSONArray("querry_data");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                        QuerryBean dm = new QuerryBean(jsonObject1.getString("id"),jsonObject1.getString("msg"),
                                jsonObject1.getString("title"),jsonObject1.getString("date"),jsonObject1.getString("name"),jsonObject1.getString("father_name"),jsonObject1.getString("class"));
                        querrylist.add(dm);
                    }
                    queerylisttadapter.setQuareyList(querrylist);
                    queerylisttadapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
            }
        }
    };

    @Override
    public void methodcall(QuerryBean obj) {
        Intent i=new Intent(Querry.this,QuerryChat.class);
        i.putExtra("query_id",obj.getId());
        i.putExtra("query_tittle",obj.getTitle());
        i.putExtra("query_cmnt",obj.getMsg());
        startActivity(i);
    }
}
