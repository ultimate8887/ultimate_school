package com.ultimate.ultimatesmartschool.Query;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.ultimate.ultimatesmartschool.BeanModule.User;
import com.ultimate.ultimatesmartschool.R;

import java.util.ArrayList;
import java.util.List;

public class ChatAdapterClass extends RecyclerView.Adapter<ChatAdapterClass.ViewHolder> {
    Context context;
    private List<ChatBean> MainImageUploadInfoList;

    public ChatAdapterClass(Context context, List<ChatBean> MainImageUploadInfoList) {
        this.MainImageUploadInfoList = MainImageUploadInfoList;
        this.context = context;
    }

    @Override
    public ChatAdapterClass.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.msg_list_lyt, parent, false);

        ChatAdapterClass.ViewHolder viewHolder = new ChatAdapterClass.ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ChatAdapterClass.ViewHolder holder, final int position) {

        final ChatBean chatBean = MainImageUploadInfoList.get(position);
        holder.bind(chatBean, context);
    }

    @Override
    public int getItemCount() {

        return MainImageUploadInfoList.size();
    }

    public void setMsgList(ArrayList<ChatBean> MainImageUploadInfoList) {
        this.MainImageUploadInfoList = MainImageUploadInfoList;
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        TextView msgSend, msgRec;

        public ViewHolder(View itemView) {
            super(itemView);
            msgSend = (TextView) itemView.findViewById(R.id.msgSend);
            msgRec = (TextView) itemView.findViewById(R.id.msgRec);
        }

        public void bind(final ChatBean item, Context context) {
            if (User.getCurrentUser().getId().equals(item.getUser_id())) {
                msgSend.setText(item.getMsg());

                msgSend.setVisibility(View.VISIBLE);
                msgRec.setVisibility(View.GONE);
            } else {
                msgRec.setText(item.getMsg());

                msgSend.setVisibility(View.GONE);
                msgRec.setVisibility(View.VISIBLE);
            }
        }
    }
}
