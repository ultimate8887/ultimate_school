package com.ultimate.ultimatesmartschool.Query;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.ultimate.ultimatesmartschool.BeanModule.User;
import com.ultimate.ultimatesmartschool.Login.LoginActivity;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class QuerryChat extends AppCompatActivity {
    @BindView(R.id.txtQTitle)
    TextView txtQTitle;
    @BindView(R.id.txtComment)
    TextView txtComment;
    @BindView(R.id.txtTitle)
    TextView txtTitle;

    String id, cmnt, tiitle;
    ImageView sendmsg;
    EditText sendtext;
    ChatAdapterClass chatlisttadapter;
    ArrayList<ChatBean> querrylistmsg = new ArrayList<>();
    RecyclerView recyclerView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_querry_chat);
        ButterKnife.bind(this);

        id = getIntent().getExtras().getString("query_id").toString();
        tiitle = getIntent().getExtras().getString("query_tittle").toString();
        cmnt = getIntent().getExtras().getString("query_cmnt").toString();
        txtTitle.setText("Query");
        txtComment.setText(cmnt);
        txtQTitle.setText(tiitle);
        sendtext = (EditText) findViewById(R.id.editCmmnt);
        sendmsg = (ImageView) findViewById(R.id.sendMsg);
        recyclerView = (RecyclerView) findViewById(R.id.msgrecy);
        recyclerView.setLayoutManager(new LinearLayoutManager(QuerryChat.this));

        sendmsg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                send_msg();
                sendtext.setText("");
            }
        });
        fetchmsg();
        chatlisttadapter = new ChatAdapterClass(QuerryChat.this, querrylistmsg);
        recyclerView.setAdapter(chatlisttadapter);
    }

    @OnClick(R.id.imgBack)
    public void callBack() {
        finish();
    }

    @OnClick(R.id.txtComment)
    public void viewComment() {
        final Dialog cmntDialog = new Dialog(QuerryChat.this);
        cmntDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        cmntDialog.setCancelable(true);
        cmntDialog.setContentView(R.layout.qureycommentdialog);
        TextView ok = (TextView) cmntDialog.findViewById(R.id.oook);
        TextView msg = (TextView) cmntDialog.findViewById(R.id.msg);
        msg.setText(cmnt);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cmntDialog.dismiss();
            }
        });
        cmntDialog.show();
    }

    private void fetchmsg() {
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("query_id", id);
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.Responsemsg, msgapi1Callback, this, params);
    }

    ApiHandler.ApiCallback msgapi1Callback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            if (error == null) {
                try {
                    JSONArray jsonArray = jsonObject.getJSONArray("querry_data");
                    if (querrylistmsg != null) {
                        querrylistmsg.clear();
                    }
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                        ChatBean dm = new ChatBean(jsonObject1.getString("id"),
                                jsonObject1.getString("user_id"),
                                jsonObject1.getString("msg"));
                        querrylistmsg.add(dm);
                    }
                    chatlisttadapter.setMsgList(querrylistmsg);
                    chatlisttadapter.notifyDataSetChanged();
                    recyclerView.scrollToPosition(querrylistmsg.size());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                if (error.getStatusCode() == 405) {
                    User.logout();
                    startActivity(new Intent(QuerryChat.this, LoginActivity.class));
                    finish();
                }
            }
        }
    };

    private void send_msg() {
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("user_id", User.getCurrentUser().getId());
        params.put("query_id", id);
        params.put("msg", sendtext.getText().toString());
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.ResponseQuery, msgapi1Callback, this, params);
    }

    ApiHandler.ApiCallback msgapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            if (error == null) {
                Toast.makeText(QuerryChat.this, "MSG Uploaded Succesfully", Toast.LENGTH_LONG).show();

            } else {
                Toast.makeText(QuerryChat.this, error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }
    };
}
