package com.ultimate.ultimatesmartschool.Query;

public class QuerryBean {

    String id;
    String msg;
    String title;
    String date;
    /**
     * name : fname flname
     * father_name : fname
     * class : NURSERY1
     */

    private String name;
    private String father_name;
    private String classX;

    public QuerryBean(String id, String msg, String title, String date,String name,String father_name,String classX) {
        this.id = id;
        this.msg = msg;
        this.title = title;
        this.date = date;
        this.name=name;
        this.father_name=father_name;
        this.classX=classX;
    }

    public String getId() {
        return id;
    }

    public String getMsg() {
        return msg;
    }

    public String getTitle() {
        return title;
    }

    public String getDate() {
        return date;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFather_name() {
        return father_name;
    }

    public void setFather_name(String father_name) {
        this.father_name = father_name;
    }

    public String getClassX() {
        return classX;
    }

    public void setClassX(String classX) {
        this.classX = classX;
    }
}
