package com.ultimate.ultimatesmartschool.Security;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.github.siyamed.shapeimageview.BubbleImageView;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.squareup.picasso.Picasso;
import com.ultimate.ultimatesmartschool.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SecurityAdapter extends RecyclerView.Adapter<SecurityAdapter.Viewholder> {
    Context context;
    ArrayList<SecurityBean> securitylist;
    public SecurityAdapter(Context context, ArrayList<SecurityBean> securitylist) {
        this.context=context;
        this.securitylist=securitylist;
    }

    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.secu_adapt_layout,parent,false);
        SecurityAdapter.Viewholder viewholder=new SecurityAdapter.Viewholder(view);
        return viewholder;
    }

    @Override
    public void onBindViewHolder(@NonNull Viewholder holder, int position) {
        holder.parent.setAnimation(AnimationUtils.loadAnimation(context,R.anim.fade_scale_animation));

        if (securitylist.get(position).getImage() != null) {
            Picasso.get().load(securitylist.get(position).getImage()).placeholder(context.getResources().getDrawable(R.drawable.studd)).into(holder.circleImageView);
        }
        holder.name.setText("Visitor Name:"+securitylist.get(position).getVistior_name());
        holder.phonenum.setText("Mobile No.:"+securitylist.get(position).getPhone());
        holder.address.setText("Address:"+securitylist.get(position).getAddress());
        holder.purpose.setText("Purpose:"+securitylist.get(position).getPurpose());
        holder.proff.setText("Proof Type:"+securitylist.get(position).getProff_type());
        holder.proffnum.setText("Proof Detail:"+securitylist.get(position).getProff_detail());
        holder.keynum.setText("Visitor ID:"+securitylist.get(position).getVisitor_id());
        holder.checkintime.setText("IN-Time:"+securitylist.get(position).getVisit_in_time());
        holder.checkouttime.setText("OUT-Time:"+securitylist.get(position).getVisit_out_time());
    }

    @Override
    public int getItemCount() {
        return securitylist.size();
    }

    public void setsecuList(ArrayList<SecurityBean> securitylist) {
        this.securitylist = securitylist;
    }

    public class Viewholder extends RecyclerView.ViewHolder {
        @BindView(R.id.circleimgrecep)
        BubbleImageView circleImageView;
        @BindView(R.id.txtname)
        TextView name;
        @BindView(R.id.txtnumberss)TextView phonenum;
        @BindView(R.id.txtaddresss)TextView address;
        @BindView(R.id.txtpurpose)TextView purpose;
        @BindView(R.id.txtIdentityproff)TextView proff;
        @BindView(R.id.txtproffno)TextView proffnum;
        @BindView(R.id.txtvisitotid)TextView keynum;
        @BindView(R.id.txtcheckin)TextView checkintime;
        @BindView(R.id.txtcheckout)TextView checkouttime;
        @BindView(R.id.card1)
        CardView parent;
        public Viewholder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }


}
