package com.ultimate.ultimatesmartschool.Security;

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.github.siyamed.shapeimageview.CircularImageView;
import com.squareup.picasso.Picasso;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.Utils;


import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Common_adapter extends RecyclerView.Adapter<Common_adapter.Viewholder>implements Filterable {

    Context context;
    ArrayList<CheckInlistbean> securitylist;
    ArrayList<CheckInlistbean> filterModelClassa;
    private ValueFilter valueFilter;
    Mycallback mAdaptercall;
    Animation animBlink;

    public Common_adapter(ArrayList<CheckInlistbean> securitylist, Context context, Mycallback mAdaptercall) {
        this.context=context;
        this.securitylist=securitylist;
        this.mAdaptercall=mAdaptercall;
        this.filterModelClassa=securitylist;
    }


    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.common_secu_adapt_layout,parent,false);
        Common_adapter.Viewholder viewholder=new Common_adapter.Viewholder(view);
        return viewholder;
    }

    @Override
    public void onBindViewHolder(@NonNull Viewholder holder, @SuppressLint("RecyclerView") final int position) {

        final CheckInlistbean mData = securitylist.get(position);

        holder.txtstuName.setEnabled(false);
        holder.txtReason.setEnabled(false);
        holder.txtcontprsn.setEnabled(false);
        holder.txtproof.setEnabled(false);
        String mclass = "";

        if (mData.getEs_securityid() != null) {
            String title = getColoredSpanned("ID: ", "#000000");
            String Name = getColoredSpanned(mData.getEs_securityid(), "#5A5C59");
            holder.id.setText(Html.fromHtml(title + " " + Name));
        }

//        if (mData.getGate_class() != null)
//            mclass = mData.getGate_class();
//

        if (mData.getSec_contact_person() != null) {
            String title = getColoredSpanned("Contact No: ", "#5A5C59");
            String Name = getColoredSpanned(mData.getSec_contact_person(), "#7D7D7D");
            holder.txtPhone.setText(Html.fromHtml(title + " " + Name));
        }
        if (mData.getSec_mode_app() != null) {
            String title = getColoredSpanned("To Meet: ", "#5A5C59");
            String Name = getColoredSpanned(mData.getSec_mode_app(), "#7D7D7D");
            holder.txtFatherName.setText(Html.fromHtml(title + " " + Name));
        }

        Log.i("Dateeeeeeeeeee", Utils.getDateTimeFormated(mData.getSec_time_in()));

        if (mData.getGate_datetime().equalsIgnoreCase("Morning")){
            holder.txtDT.setText(Utils.getDateTimeFormated(mData.getSec_time_in())+"AM");
        }else {
            holder.txtDT.setText(Utils.getDateTimeFormated(mData.getSec_time_in())+"PM");
        }

        // holder.txtDT.setText(Utility.getDateTimeFormated(mData.getGate_time()));
        holder.txtReason.setText(mData.getSec_purpose());

        if (mData.getSec_name() != null) {
            String title = getColoredSpanned("Name: ", "#5A5C59");
            String Name = getColoredSpanned(mData.getSec_name(), "#7D7D7D");
            holder.txtstuName.setText(Html.fromHtml(title + " " + Name));
        }

        if (mData.getSec_make_vehicle() != null) {
            holder.txtcontprsn.setVisibility(View.VISIBLE);
            holder.guardian_add.setVisibility(View.VISIBLE);
            String title = getColoredSpanned("Vehicle Type: ", "#5A5C59");
            String Name = getColoredSpanned(mData.getSec_make_vehicle(), "#7D7D7D");
            holder.txtcontprsn.setText(Html.fromHtml(title + " " + Name));
        }else {
            holder.txtcontprsn.setText("Not Mentioned");
        }

        if (mData.getSec_vehicle_no() != null) {
            holder.txtcontprsnnum.setVisibility(View.VISIBLE);
            String title = getColoredSpanned("Vehicle No: ", "#5A5C59");
            String Name = getColoredSpanned(mData.getSec_vehicle_no(), "#7D7D7D");
            holder.txtcontprsnnum.setText(Html.fromHtml(title + " " + Name));
        } else {
            holder.txtcontprsnnum.setText("Not Mentioned");
        }

        if (mData.getSec_colour() != null) {
            holder.txtrelation.setVisibility(View.VISIBLE);
            String title = getColoredSpanned("Vehicle Color: ", "#5A5C59");
            String Name = getColoredSpanned(mData.getSec_colour(), "#7D7D7D");
            holder.txtrelation.setText(Html.fromHtml(title + " " + Name));
        }else {
            holder.txtrelation.setText("Not Mentioned");
        }


        if (mData.getGate_datetime() != null) {
            holder.txtdandtofcall.setVisibility(View.VISIBLE);
            String title = getColoredSpanned("Check In: ", "#1C8B3B");
            String Name = getColoredSpanned(mData.getGate_datetime()+ " at " + mData.getGate_timein(), "#7D7D7D");
            holder.txtdandtofcall.setText(Html.fromHtml(title + " " + Name));
        }


        if (mData.getProf_type() != null) {
            String title = getColoredSpanned("Proof Type: ", "#5A5C59");
            String Name = getColoredSpanned(mData.getProf_type(), "#7D7D7D");
            holder.txtproof.setText(Html.fromHtml(title + " " + Name));
        }

        if (mData.getProf_value() != null) {
            String title = getColoredSpanned("Proof No: ", "#5A5C59");
            String Name = getColoredSpanned(mData.getProf_value(), "#7D7D7D");
            holder.txtproofn.setText(Html.fromHtml(title + " " + Name));
        }

        String path= Constants.getImageBaseURL()+ "office_admin/images/gatepass/";

        if (mData.getStaff_sign() != null) {
            Picasso.get().load(path+mData.getStaff_sign()).placeholder(R.color.white).into(holder.staff_sign);
            Log.e("chekkkkkkkkkkk",mData.getStaff_sign());
        }

//        if (mData.getAdmin_sign() != null) {
//            Picasso.with(context).load(path+mData.getAdmin_sign()).placeholder(R.color.white).into(holder.admin_sign);
//        }else {
        holder.p_title.setVisibility(View.GONE);
        holder.admin_sign.setVisibility(View.GONE);
//        }

        if (mData.getStd_sign() != null) {
            Picasso.get().load(path+mData.getStd_sign()).placeholder(R.color.white).into(holder.std_sign);
        } else {
            holder.sign_lyt.setVisibility(View.GONE);
        }

        if (mData.getImage() != null) {
            Picasso.get().load(path+mData.getImage()).placeholder(R.color.white).into(holder.profile);
        } else {
            holder.profile.setVisibility(View.GONE);
        }

        if (mData.getDocimage() != null) {
            Picasso.get().load(path+mData.getDocimage()).placeholder(R.color.white).into(holder.docprofile);
        } else {
            holder.docprofile.setVisibility(View.GONE);
        }

        holder.docprofile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mAdaptercall.onApproveCallbackDoc(securitylist.get(position));
            }
        });

        holder.p_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.p_add.setVisibility(View.GONE);
                holder.p_mini.setVisibility(View.VISIBLE);
                holder.moree.setVisibility(View.VISIBLE);
            }
        });

        if (mData.getVerification().equalsIgnoreCase("pending")) {

            // holder.edtCheckOut.setVisibility(View.VISIBLE);
            holder.txtcheckOut.setVisibility(View.GONE);
//
            Animation anim = new AlphaAnimation(0.0f, 1.0f);
            anim.setDuration(500); //You can manage the blinking time with this parameter
            anim.setStartOffset(50);
            anim.setRepeatMode(Animation.REVERSE);
            anim.setRepeatCount(Animation.INFINITE);
            holder.edtCheckOuts.startAnimation(anim);

            //  holder.edtCheckOuts.setSelected(true);
            holder.edtCheckOut.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mAdaptercall.onApproveCallback(securitylist.get(position));
                }
            });
        }else {

            holder.edtCheckOut.setVisibility(View.GONE);
            holder.txtcheckOut.setVisibility(View.VISIBLE);



            if (mData.getGate_datetime_out() != null) {
                String title = getColoredSpanned("Check Out: ", "#F4212C");
                String Name = getColoredSpanned(mData.getGate_datetime_out()+ " at " + mData.getGate_timeout(), "#7D7D7D");
                holder.txtcheckOut.setText(Html.fromHtml(title + " " + Name));
            }
        }


        holder.moree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.p_add.setVisibility(View.VISIBLE);
                holder.p_mini.setVisibility(View.GONE);
                holder.moree.setVisibility(View.GONE);
            }
        });


        holder.guardian_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.guardian_add.setVisibility(View.GONE);
                holder.guardian_mini.setVisibility(View.VISIBLE);
                holder.more.setVisibility(View.VISIBLE);
            }
        });

        holder.more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.guardian_add.setVisibility(View.VISIBLE);
                holder.guardian_mini.setVisibility(View.GONE);
                holder.more.setVisibility(View.GONE);
            }
        });


    }

    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }


    @Override
    public Filter getFilter() {
        if (valueFilter == null) {
            valueFilter = new ValueFilter();
        }
        return valueFilter;
    }
    private class ValueFilter extends Filter{


        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();

            if ( constraint != null && constraint.length() > 0) {
                ArrayList<CheckInlistbean> filterList = new ArrayList<>();

                for (int i = 0; i < filterModelClassa.size(); i++) {
                    if ((filterModelClassa.get(i).getSec_name().toUpperCase()).contains(constraint.toString().toUpperCase())||(filterModelClassa.get(i).getSec_contact_person().toUpperCase()).contains(constraint.toString().toUpperCase())) {
                        filterList.add(filterModelClassa.get(i));
                    }
                }
                results.count = filterList.size();
                results.values = filterList;
            } else  {
                results.count = securitylist.size();
                results.values = securitylist;
                //servicedetaillist = filterModelClassa;

            }
            return results;


        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            securitylist = (ArrayList<CheckInlistbean>) results.values;
            notifyDataSetChanged();
        }
    }
    @Override
    public int getItemCount() {
        return securitylist.size();
    }

    public void setsecuList(ArrayList<CheckInlistbean> securitylist) {
        this.securitylist = securitylist;
    }
    public interface Mycallback {
        public void onApproveCallback(CheckInlistbean checkInlistbean);
        public void onApproveCallbackDoc(CheckInlistbean checkInlistbean);

    }
    public class Viewholder extends RecyclerView.ViewHolder {

        @BindView(R.id.txtFatherName)
        TextView txtFatherName;
        @BindView(R.id.txtPhone)
        TextView txtPhone;
        @BindView(R.id.txtDT)
        TextView txtDT;
        @BindView(R.id.id)
        TextView id;
        @BindView(R.id.p_title)
        TextView p_title;
        @BindView(R.id.txtABy)
        TextView txtABy;
        @BindView(R.id.txtReason)
        EditText txtReason;
        @BindView(R.id.txtstuName)
        EditText txtstuName;
        @BindView(R.id.txtcontprsn)
        EditText txtcontprsn;
        @BindView(R.id.txtcontprsnnum)
        TextView txtcontprsnnum;
        @BindView(R.id.txtrelation)
        TextView txtrelation;
        @BindView(R.id.txtconfirmationnum)
        TextView txtconfirmationnum;
        @BindView(R.id.txtdandtofcall)
        TextView txtdandtofcall;
        @BindView(R.id.std_sign)
        ImageView std_sign;
        @BindView(R.id.staff_sign)
        ImageView staff_sign;
        @BindView(R.id.admin_sign)
        ImageView admin_sign;
        @BindView(R.id.profile)
        CircularImageView profile;
        @BindView(R.id.sign_lyt)
        CardView sign_lyt;
        @BindView(R.id.more)
        RelativeLayout more;
        @BindView(R.id.moree)
        RelativeLayout moree;
        @BindView(R.id.p_add)
        TextView p_add;
        @BindView(R.id.p_mini)
        TextView p_mini;
        @BindView(R.id.txtproof)
        EditText txtproof;
        @BindView(R.id.txtproofn)
        TextView txtproofn;
        @BindView(R.id.guardian_add)
        TextView guardian_add;
        @BindView(R.id.guardian_mini)
        TextView guardian_mini;
        @BindView(R.id.txtcheckOut)
        TextView txtcheckOut;

        @BindView(R.id.edtCheckOut)
        RelativeLayout edtCheckOut;
        @BindView(R.id.edtCheckOuts)
        TextView edtCheckOuts;

        @BindView(R.id.docprofile)
        ImageView docprofile;

        public Viewholder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }

}
