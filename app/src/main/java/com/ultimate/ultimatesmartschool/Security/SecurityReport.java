package com.ultimate.ultimatesmartschool.Security;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;
import android.widget.TextView;

import com.google.android.material.tabs.TabLayout;
import com.ultimate.ultimatesmartschool.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SecurityReport extends AppCompatActivity {
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.tab_layout)
    TabLayout tabLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_security_report);
        ButterKnife.bind(this);
        txtTitle.setText("CheckIn-Out Detail");
        //Tab layout
        final ViewPager viewPager = (ViewPager) findViewById(R.id.pager);
        tabLayout.setupWithViewPager(viewPager);
        final PagerAdapter adapter = new SecurityreportPager
                (getSupportFragmentManager());
        viewPager.setAdapter(adapter);
    }

    @OnClick(R.id.imgBack)
    public void callBackF() {
        finish();
    }
    }
