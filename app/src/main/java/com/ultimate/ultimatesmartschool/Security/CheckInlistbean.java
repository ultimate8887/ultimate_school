package com.ultimate.ultimatesmartschool.Security;



import com.ultimate.ultimatesmartschool.Utility.DebugLog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class CheckInlistbean {

    private static String ID="es_securityid";
    private static String IMAGE="image";
    private static String VISITOR_NAME ="sec_name";
    private static String PHONE ="sec_contact_person";
    private static String ADDRESS ="sec_address";
    private static String PURPOSE ="sec_purpose";
    private static String PROFF_TYPE ="prof_type";
    private static String PROFF_DETAIL ="prof_value";
//    private static String VISITOR_ID ="visitor_id";
    private static String VISIT_IN_TIME ="sec_time_in";
    private static String VISIT_OUT_TIME ="sec_time_out";

    private String es_securityid;

    public String getDocimage() {
        return docimage;
    }

    public void setDocimage(String docimage) {
        this.docimage = docimage;
    }

    private String docimage;

    public String getVerification() {
        return verification;
    }

    public void setVerification(String verification) {
        this.verification = verification;
    }

    private String verification;
    private String sec_name;

    public String getEs_securityid() {
        return es_securityid;
    }

    public void setEs_securityid(String es_securityid) {
        this.es_securityid = es_securityid;
    }

    public String getSec_name() {
        return sec_name;
    }

    public void setSec_name(String sec_name) {
        this.sec_name = sec_name;
    }

    public String getSec_contact_person() {
        return sec_contact_person;
    }

    public void setSec_contact_person(String sec_contact_person) {
        this.sec_contact_person = sec_contact_person;
    }

    public String getSec_address() {
        return sec_address;
    }

    public void setSec_address(String sec_address) {
        this.sec_address = sec_address;
    }

    public String getSec_purpose() {
        return sec_purpose;
    }

    public void setSec_purpose(String sec_purpose) {
        this.sec_purpose = sec_purpose;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getProf_type() {
        return prof_type;
    }

    public void setProf_type(String prof_type) {
        this.prof_type = prof_type;
    }

    public String getProf_value() {
        return prof_value;
    }

    public void setProf_value(String prof_value) {
        this.prof_value = prof_value;
    }

    public String getSec_time_in() {
        return sec_time_in;
    }

    public void setSec_time_in(String sec_time_in) {
        this.sec_time_in = sec_time_in;
    }

    public String getSec_time_out() {
        return sec_time_out;
    }

    public void setSec_time_out(String sec_time_out) {
        this.sec_time_out = sec_time_out;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    private String sec_contact_person;
    private String sec_address;
    private String sec_purpose;
    private String image;
    private String prof_type;
    private String prof_value;
    private String sec_time_in;
    private String sec_time_out;
    private String status;

    public String getSec_mode_app() {
        return sec_mode_app;
    }

    public void setSec_mode_app(String sec_mode_app) {
        this.sec_mode_app = sec_mode_app;
    }

    private String sec_mode_app;

    public String getGate_datetime() {
        return gate_datetime;
    }

    public void setGate_datetime(String gate_datetime) {
        this.gate_datetime = gate_datetime;
    }

    public String getGate_timeout() {
        return gate_timeout;
    }

    public void setGate_timeout(String gate_timeout) {
        this.gate_timeout = gate_timeout;
    }

    public String getStaff_sign() {
        return staff_sign;
    }

    public void setStaff_sign(String staff_sign) {
        this.staff_sign = staff_sign;
    }

    public String getStd_sign() {
        return std_sign;
    }

    public void setStd_sign(String std_sign) {
        this.std_sign = std_sign;
    }

    public String getGate_timein() {
        return gate_timein;
    }

    public void setGate_timein(String gate_timein) {
        this.gate_timein = gate_timein;
    }

    public String getGate_datetime_out() {
        return gate_datetime_out;
    }

    public void setGate_datetime_out(String gate_datetime_out) {
        this.gate_datetime_out = gate_datetime_out;
    }

    public String getSec_vehicle_no() {
        return sec_vehicle_no;
    }

    public void setSec_vehicle_no(String sec_vehicle_no) {
        this.sec_vehicle_no = sec_vehicle_no;
    }

    public String getSec_colour() {
        return sec_colour;
    }

    public void setSec_colour(String sec_colour) {
        this.sec_colour = sec_colour;
    }

    public String getSec_make_vehicle() {
        return sec_make_vehicle;
    }

    public void setSec_make_vehicle(String sec_make_vehicle) {
        this.sec_make_vehicle = sec_make_vehicle;
    }



    private String gate_datetime;
    private String gate_timeout;
    private String staff_sign;
    private String std_sign;
    private String gate_timein;
    private String gate_datetime_out;
    private String sec_vehicle_no;
    private String sec_colour;
    private String sec_make_vehicle;

    public String getEs_category() {
        return es_category;
    }

    public void setEs_category(String es_category) {
        this.es_category = es_category;
    }

    private String es_category;


    public static ArrayList<CheckInlistbean> parsereceptioistArray(JSONArray jsonArray) {
        ArrayList<CheckInlistbean> list = new ArrayList<CheckInlistbean>();
        try {

            for (int i = 0; i < jsonArray.length(); i++) {
                CheckInlistbean p = parserecepObject(jsonArray.getJSONObject(i));
                if (p != null) {
                    list.add(p);
                }
            }
        } catch (Exception e) {
            DebugLog.printLog("exp",e.getMessage());
            e.printStackTrace();
        }
        return list;
    }

    private static CheckInlistbean parserecepObject(JSONObject jsonObject) {

        CheckInlistbean casteObj = new CheckInlistbean();
        try {
            if (jsonObject.has(ID)) {
                casteObj.setEs_securityid(jsonObject.getString(ID));
            }
            if (jsonObject.has(IMAGE) && !jsonObject.getString(IMAGE).isEmpty() && !jsonObject.getString(IMAGE).equalsIgnoreCase("null")) {
                casteObj.setImage(jsonObject.getString(IMAGE));
            }
            if (jsonObject.has(VISITOR_NAME) && !jsonObject.getString(VISITOR_NAME).isEmpty() && !jsonObject.getString(VISITOR_NAME).equalsIgnoreCase("null")) {
                casteObj.setSec_name(jsonObject.getString(VISITOR_NAME));
            }
            if (jsonObject.has(PHONE) && !jsonObject.getString(PHONE).isEmpty() && !jsonObject.getString(PHONE).equalsIgnoreCase("null")) {
                casteObj.setSec_contact_person(jsonObject.getString(PHONE));
            }
            if (jsonObject.has(ADDRESS) && !jsonObject.getString(ADDRESS).isEmpty() && !jsonObject.getString(ADDRESS).equalsIgnoreCase("null")) {
                casteObj.setSec_address(jsonObject.getString(ADDRESS));
            }
            if (jsonObject.has(PURPOSE) && !jsonObject.getString(PURPOSE).isEmpty() && !jsonObject.getString(PURPOSE).equalsIgnoreCase("null")) {
                casteObj.setSec_purpose(jsonObject.getString(PURPOSE));
            }
            if (jsonObject.has(PROFF_TYPE) && !jsonObject.getString(PROFF_TYPE).isEmpty() && !jsonObject.getString(PROFF_TYPE).equalsIgnoreCase("null")) {
                casteObj.setProf_type(jsonObject.getString(PROFF_TYPE));
            }
            if (jsonObject.has(PROFF_DETAIL) && !jsonObject.getString(PROFF_DETAIL).isEmpty() && !jsonObject.getString(PROFF_DETAIL).equalsIgnoreCase("null")) {
                casteObj.setProf_value(jsonObject.getString(PROFF_DETAIL));
            }

            if (jsonObject.has(VISIT_IN_TIME) && !jsonObject.getString(VISIT_IN_TIME).isEmpty() && !jsonObject.getString(VISIT_IN_TIME).equalsIgnoreCase("null")) {
                casteObj.setSec_time_in(jsonObject.getString(VISIT_IN_TIME));
            }
            if (jsonObject.has(VISIT_OUT_TIME) && !jsonObject.getString(VISIT_OUT_TIME).isEmpty() && !jsonObject.getString(VISIT_OUT_TIME).equalsIgnoreCase("null")) {
                casteObj.setSec_time_out(jsonObject.getString(VISIT_OUT_TIME));
            }

            if (jsonObject.has("status") && !jsonObject.getString("status").isEmpty() && !jsonObject.getString("status").equalsIgnoreCase("null")) {
                casteObj.setStatus(jsonObject.getString("status"));
            }

            if (jsonObject.has("gate_datetime") && !jsonObject.getString("gate_datetime").isEmpty() && !jsonObject.getString("gate_datetime").equalsIgnoreCase("null")) {
                casteObj.setGate_datetime(jsonObject.getString("gate_datetime"));
            }

            if (jsonObject.has("gate_timeout") && !jsonObject.getString("gate_timeout").isEmpty() && !jsonObject.getString("gate_timeout").equalsIgnoreCase("null")) {
                casteObj.setGate_timeout(jsonObject.getString("gate_timeout"));
            }

            if (jsonObject.has("gate_timein") && !jsonObject.getString("gate_timein").isEmpty() && !jsonObject.getString("gate_timein").equalsIgnoreCase("null")) {
                casteObj.setGate_timein(jsonObject.getString("gate_timein"));
            }
            if (jsonObject.has("gate_datetime_out") && !jsonObject.getString("gate_datetime_out").isEmpty() && !jsonObject.getString("gate_datetime_out").equalsIgnoreCase("null")) {
                casteObj.setGate_datetime_out(jsonObject.getString("gate_datetime_out"));
            }

            if (jsonObject.has("sec_vehicle_no") && !jsonObject.getString("sec_vehicle_no").isEmpty() && !jsonObject.getString("sec_vehicle_no").equalsIgnoreCase("null")) {
                casteObj.setSec_vehicle_no(jsonObject.getString("sec_vehicle_no"));
            }

            if (jsonObject.has("sec_colour") && !jsonObject.getString("sec_colour").isEmpty() && !jsonObject.getString("sec_colour").equalsIgnoreCase("null")) {
                casteObj.setSec_colour(jsonObject.getString("sec_colour"));
            }
            if (jsonObject.has("sec_make_vehicle") && !jsonObject.getString("sec_make_vehicle").isEmpty() && !jsonObject.getString("sec_make_vehicle").equalsIgnoreCase("null")) {
                casteObj.setSec_make_vehicle(jsonObject.getString("sec_make_vehicle"));
            }

            if (jsonObject.has("std_sign") && !jsonObject.getString("std_sign").isEmpty() && !jsonObject.getString("std_sign").equalsIgnoreCase("null")) {
                casteObj.setStd_sign(jsonObject.getString("std_sign"));
            }
            if (jsonObject.has("staff_sign") && !jsonObject.getString("staff_sign").isEmpty() && !jsonObject.getString("staff_sign").equalsIgnoreCase("null")) {
                casteObj.setStaff_sign(jsonObject.getString("staff_sign"));
            }
            if (jsonObject.has("sec_mode_app") && !jsonObject.getString("sec_mode_app").isEmpty() && !jsonObject.getString("sec_mode_app").equalsIgnoreCase("null")) {
                casteObj.setSec_mode_app(jsonObject.getString("sec_mode_app"));
            }

            if (jsonObject.has("verification") && !jsonObject.getString("verification").isEmpty() && !jsonObject.getString("verification").equalsIgnoreCase("null")) {
                casteObj.setVerification(jsonObject.getString("verification"));
            }

            if (jsonObject.has("docimage") && !jsonObject.getString("docimage").isEmpty() && !jsonObject.getString("docimage").equalsIgnoreCase("null")) {
                casteObj.setDocimage(jsonObject.getString("docimage"));
            }

            if (jsonObject.has("es_category") && !jsonObject.getString("es_category").isEmpty() && !jsonObject.getString("es_category").equalsIgnoreCase("null")) {
                casteObj.setEs_category(jsonObject.getString("es_category"));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return casteObj;
    }
}
