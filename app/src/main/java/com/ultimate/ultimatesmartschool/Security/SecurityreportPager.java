package com.ultimate.ultimatesmartschool.Security;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

public class SecurityreportPager extends FragmentPagerAdapter {

    int tabCount;
    // tab titles
    private String[] tabTitles = new String[]{"CheckIn List", "CheckOut List"};

    public SecurityreportPager(FragmentManager fm) {
        super(fm);
        this.tabCount =2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabTitles[position];
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                CheckInDetailFrag tab1 = new CheckInDetailFrag();
                return tab1;
            case 1:
                CheckOutDetailFrag tab2 = new CheckOutDetailFrag();
                return tab2;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return tabCount;
    }
}
