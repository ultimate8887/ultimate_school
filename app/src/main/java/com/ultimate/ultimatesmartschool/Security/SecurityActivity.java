package com.ultimate.ultimatesmartschool.Security;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.text.Html;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetSequence;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.ultimate.ultimatesmartschool.BeanModule.CommonBean;
import com.ultimate.ultimatesmartschool.BeanModule.User;
import com.ultimate.ultimatesmartschool.Examination.GroupSpinnerAdapter;
import com.ultimate.ultimatesmartschool.Leave.OptionAdapter;
import com.ultimate.ultimatesmartschool.Leave.OptionBean;
import com.ultimate.ultimatesmartschool.Leave.StaffLeaveList;
import com.ultimate.ultimatesmartschool.Leave.Staffleavelistadapter;
import com.ultimate.ultimatesmartschool.Leave.Stafflistbean;
import com.ultimate.ultimatesmartschool.Login.LoginActivity;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.CommonProgress;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SecurityActivity extends AppCompatActivity implements Common_adapter.Mycallback {
    @BindView(R.id.recyclerviewlist)
    RecyclerView recyclerView;
    @BindView(R.id.parent)
    RelativeLayout parent;
    RecyclerView.LayoutManager layoutManager;
    ArrayList<CheckInlistbean> adminlist=new ArrayList<>();
    @BindView(R.id.textNorecord)
    TextView txtNorecord;
    int loaded = 0;
    String tag="all",save="All";
    SharedPreferences sharedPreferences;
    @BindView(R.id.totalRecord)
    TextView totalRecord;

    ArrayList<OptionBean> vehicleList = new ArrayList<>();
    @BindView(R.id.spinerVehicletype)
    Spinner vehicleType;

    @BindView(R.id.spinerVehicletype2)Spinner spinnerGroup;
    private ArrayList<CommonBean> groupList = new ArrayList<>();
    private String groupid = "",groupName = "";


    CommonProgress commonProgress;
    Common_adapter adminlistadapter;
    @BindView(R.id.txtTitle)TextView txtTitle;
    @BindView(R.id.export)
    FloatingActionButton export;
   int count=0;
    @BindView(R.id.root2)RelativeLayout root2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_staff_leave_list);
        ButterKnife.bind(this);
        commonProgress=new CommonProgress(this);
        layoutManager=new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        adminlistadapter=new Common_adapter(adminlist,this,this);
        recyclerView.setAdapter(adminlistadapter);
        txtTitle.setText("Check-In/Out List");
        root2.setVisibility(View.VISIBLE);
        //  vehicleList.add(new OptionBean("All"));
        vehicleList.add(new OptionBean("Check-In"));
        vehicleList.add(new OptionBean("Check-Out"));

        fetchGroup(tag);

        OptionAdapter dataAdapter = new OptionAdapter(SecurityActivity.this, vehicleList);
        //set the ArrayAdapter to the spinner
        vehicleType.setAdapter(dataAdapter);
        //attach the listener to the spinner
        vehicleType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i != 0) {
                    tag = vehicleList.get(i - 1).getName();
                    if (tag.equalsIgnoreCase("Check-In")){
                        tag="pending";
                        save="Check-In";
                    }else{
                        tag="verify";
                        save="Check-Out";
                    }
                }else {
                    tag="all";
                    save="All";
                }
                if (count!=0) {
                    fetchCheckinlist(tag);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

//         fetchCheckinlist();

    }



    @SuppressLint("RestrictedApi")
    @OnClick(R.id.export)
    public void submit() {
//        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
//            if (this.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED) {
//                String[] permissions = {Manifest.permission.WRITE_EXTERNAL_STORAGE};
//                requestPermissions(permissions, 1);
//            } else {
//                importData();
//            }
//        } else {
//            importData();
//        }
//
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.S) {
                importData();
        } else {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {

                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    Toast.makeText(this, "Please provide storage permission from app settings", Toast.LENGTH_LONG).show();
                } else {

                    ActivityCompat.requestPermissions(this,
                            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            1);
                }
            } else {
                importData();

            }
        }

   //  importData();
    }

    @SuppressLint("RestrictedApi")
    private void importData() {
        // File filePath = new File(Environment.getExternalStorageDirectory() + "/Demo.xls");
        Workbook wb = new HSSFWorkbook();

        Cell cell = null;

        String ndate="",sub_staff="",sub_date="";
//        ndate= Utils.getDateOnlyNEW(date)+","+Utils.getMonthFormated(date);
//        sub_date=ndate.substring(0,3);


        sub_date=save+" Visitor list";


        Sheet sheet = null;
        sheet = wb.createSheet("Visitor list");
        //Now column and row
        Row row = sheet.createRow(0);

        cell = row.createCell(0);
        cell.setCellValue("S No");

        cell = row.createCell(1);
        cell.setCellValue("Name");

        cell = row.createCell(2);
        cell.setCellValue("To Meet");

        cell = row.createCell(3);
        cell.setCellValue("Mobile");

        cell = row.createCell(4);
        cell.setCellValue("Check-In at");

        cell = row.createCell(5);
        cell.setCellValue("Check-Out at");

        cell = row.createCell(6);
        cell.setCellValue("Proof Type");

        cell = row.createCell(7);
        cell.setCellValue("Proof no");

        cell = row.createCell(8);
        cell.setCellValue("Vehicle Type");

        cell = row.createCell(9);
        cell.setCellValue("Vehicle No");

        cell = row.createCell(10);
        cell.setCellValue("Vehicle Color");

        cell = row.createCell(11);
        cell.setCellValue("Purpose");

        cell = row.createCell(12);
        cell.setCellValue("Category");


        //column width
        sheet.setColumnWidth(0, (20 * 100));
        sheet.setColumnWidth(1, (20 * 150));
        sheet.setColumnWidth(2, (30 * 150));
        sheet.setColumnWidth(3, (30 * 250));
        sheet.setColumnWidth(4, (30 * 300));
        sheet.setColumnWidth(5, (20 * 300));
        sheet.setColumnWidth(6, (30 * 200));
        sheet.setColumnWidth(7, (30 * 200));
        sheet.setColumnWidth(8, (20 * 200));
        sheet.setColumnWidth(9, (30 * 200));
        sheet.setColumnWidth(10, (30 * 200));
        sheet.setColumnWidth(11, (30 * 200));
        sheet.setColumnWidth(12, (30 * 200));

        for (int i = 0; i < adminlist.size(); i++) {

            Row row1 = sheet.createRow(i + 1);

            cell = row1.createCell(0);
            cell.setCellValue(i+1);

            cell = row1.createCell(1);
            cell.setCellValue(adminlist.get(i).getSec_name());

            cell = row1.createCell(2);
            cell.setCellValue(adminlist.get(i).getSec_mode_app());

            cell = row1.createCell(3);

            cell.setCellValue((adminlist.get(i).getSec_contact_person()));
            //  cell.setCellStyle(cellStyle);

            cell = row1.createCell(4);

            cell.setCellValue(adminlist.get(i).getGate_datetime()+ " at " + adminlist.get(i).getGate_timein());

            cell = row1.createCell(5);


            if (adminlist.get(i).getVerification().equalsIgnoreCase("pending")) {
                cell.setCellValue("Pending");
            }else {
                cell.setCellValue(adminlist.get(i).getGate_datetime()+ " at " + adminlist.get(i).getGate_timeout());
            }


            cell = row1.createCell(6);
            cell.setCellValue(adminlist.get(i).getProf_type());

            cell = row1.createCell(7);
            cell.setCellValue(adminlist.get(i).getProf_value());

            cell = row1.createCell(8);

            cell.setCellValue((adminlist.get(i).getSec_make_vehicle()));


            cell = row1.createCell(9);
            cell.setCellValue(adminlist.get(i).getSec_vehicle_no());

            cell = row1.createCell(10);
            cell.setCellValue(adminlist.get(i).getSec_colour());

            cell = row1.createCell(11);

            cell.setCellValue(adminlist.get(i).getSec_purpose());

            cell = row1.createCell(12);
            if (!groupid.equalsIgnoreCase("")) {
                cell.setCellValue(groupName);
            }else{
                if (adminlist.get(i).getEs_category()!=null) {
                    cell.setCellValue(adminlist.get(i).getEs_category());
                }else{
                    cell.setCellValue("N/A");
                }
            }

            sheet.setColumnWidth(0, (20 * 100));
            sheet.setColumnWidth(1, (20 * 150));
            sheet.setColumnWidth(2, (30 * 150));
            sheet.setColumnWidth(3, (30 * 250));
            sheet.setColumnWidth(4, (30 * 300));
            sheet.setColumnWidth(5, (20 * 300));
            sheet.setColumnWidth(6, (30 * 200));
            sheet.setColumnWidth(7, (30 * 200));
            sheet.setColumnWidth(8, (20 * 200));
            sheet.setColumnWidth(9, (30 * 200));
            sheet.setColumnWidth(10, (30 * 200));
            sheet.setColumnWidth(11, (30 * 200));
            sheet.setColumnWidth(12, (30 * 200));

        }
        String fileName;
        fileName = sub_date+"_" +System.currentTimeMillis() + ".xls";


        // File file = new File(getActivity().getExternalFilesDir(null), fileName);

        File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS), fileName);

        FileOutputStream fileOutputStream = null;

        try {
            fileOutputStream = new FileOutputStream(file);
            wb.write(fileOutputStream);
            Utils.openSuccessDialog("Success!","Export file in "+file,"",this);
        } catch (IOException e) {
            Utils.openErrorDialog("Error writing Exception: "+ e,this);
        } catch (Exception e) {
            Utils.openErrorDialog("Failed to save file due to Exception: "+ e,this);
        } finally {
            try {
                if (null != fileOutputStream) {
                    fileOutputStream.close();
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

    }

    @OnClick(R.id.imgBack)
    public void backCall(){
        finish();
    }


    private void fetchGroup(String tag) {
        commonProgress.show();
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("check", "visitor");
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.GROUP_LIST_URL, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                ErpProgress.cancelProgressBar();
                commonProgress.dismiss();
                if (error == null) {
                    try {
                        groupList.clear();
                        groupList = CommonBean.parseCommonArray(jsonObject.getJSONArray(Constants.GROUP_DATA));
                        GroupSpinnerAdapter adapter = new GroupSpinnerAdapter(SecurityActivity.this, groupList,1);
                        spinnerGroup.setAdapter(adapter);
                        spinnerGroup.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                if (i > 0) {
                                    groupid = groupList.get(i - 1).getId();
                                    groupName= groupList.get(i - 1).getName();
                                }else {
                                    groupid = "";
                                    groupName= "";
                                }
                                fetchCheckinlist(tag);
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> adapterView) {

                            }
                        });

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    Utils.showSnackBar(error.getMessage(), parent);
                }
            }
        }, this, params);
    }



    private void fetchCheckinlist(String tag) {
        count++;
        //  if (loaded == 0) {
        // ErpProgress.showProgressBar(getContext(), "Please wait...");
        commonProgress.show();
        //  }
        loaded++;
        HashMap<String,String> params=new HashMap<>();
        params.put("user_id", User.getCurrentUser().getId());
        params.put("date", "");
        params.put("check", tag);
        params.put("tag", "category");
        params.put("cate_id", groupid);
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.SECURITYcheckin_LIST, apiCallback, this, params);
    }

    ApiHandler.ApiCallback apiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            commonProgress.dismiss();
            // txtupload.setVisibility(View.VISIBLE);
            if (error == null) {
                try {
                    if (adminlist != null) {
                        adminlist.clear();
                    }
                    JSONArray jsonArray = jsonObject.getJSONArray("visitor_data");
                    adminlist = CheckInlistbean.parsereceptioistArray(jsonArray);
                    if (adminlist.size() > 0) {
                        adminlistadapter.setsecuList(adminlist);
                        totalRecord.setText("Total Entries:- "+String.valueOf(adminlist.size()));
                        adminlistadapter.notifyDataSetChanged();
                        txtNorecord.setVisibility(View.GONE);
                        export.setVisibility(View.VISIBLE);
                        if (!restorePrefDataP()){
                            setShowcaseViewP();
                        }
                    } else {
                        adminlist.clear();
                        totalRecord.setText("Total Entries:- "+String.valueOf(adminlist.size()));
                        adminlistadapter.setsecuList(adminlist);
                        adminlistadapter.notifyDataSetChanged();
                        txtNorecord.setVisibility(View.VISIBLE);
                        export.setVisibility(View.GONE);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                adminlist.clear();
                export.setVisibility(View.GONE);
                txtNorecord.setVisibility(View.VISIBLE);
                totalRecord.setText("Total Entries:- "+String.valueOf(adminlist.size()));
                adminlistadapter.setsecuList(adminlist);
                adminlistadapter.notifyDataSetChanged();
                Utils.showSnackBar(error.getMessage(), parent);
            }
        }
    };

    private void setShowcaseViewP() {
        new TapTargetSequence(this)
                .targets(TapTarget.forView(export,"Export Excel!","Tap the Export button to export Check-In/Out Visitor list.")
                        .outerCircleColor(R.color.light)
                        .outerCircleAlpha(0.96f)
                        .targetCircleColor(R.color.white)
                        .titleTextSize(20)
                        .titleTextColor(R.color.white)
                        .descriptionTextSize(14)
                        .descriptionTextColor(R.color.black)
                        .textColor(R.color.black)
                        .textTypeface(Typeface.SANS_SERIF)
                        .dimColor(R.color.black)
                        .drawShadow(true)
                        .cancelable(false)
                        .tintTarget(true)
                        .transparentTarget(true)
                        .targetRadius(40)).listener(new TapTargetSequence.Listener() {
                    @RequiresApi(api = Build.VERSION_CODES.N)
                    @Override
                    public void onSequenceFinish() {
                        // Toast.makeText(getApplicationContext(),"Sequence Finished",Toast.LENGTH_SHORT).show();
                        savePrefDataP();

                    }

                    @Override
                    public void onSequenceStep(TapTarget lastTarget, boolean targetClicked) {
                        // Toast.makeText(getActivity(),"GREAT!",Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onSequenceCanceled(TapTarget lastTarget) {

                    }
                }).start();
    }

    private void savePrefDataP(){
        sharedPreferences=this.getSharedPreferences("boarding_pref_view_export_visitor",MODE_PRIVATE);
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putBoolean("IsFirstTimeVisit_view_export_visitor",true);
        editor.apply();
    }

    private boolean restorePrefDataP(){
        sharedPreferences = this.getSharedPreferences("boarding_pref_view_export_visitor",MODE_PRIVATE);
        return sharedPreferences.getBoolean("IsFirstTimeVisit_view_export_visitor",false);
    }




    @Override
    public void onApproveCallback(CheckInlistbean checkInlistbean) {

    }

    @Override
    public void onApproveCallbackDoc(CheckInlistbean checkInlistbean) {

    }
}
