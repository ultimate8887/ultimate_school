package com.ultimate.ultimatesmartschool.Security;

import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.DebugLog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class SecurityBean {

    private static String ID="id";
    private static String IMAGE="image";
    private static String VISITOR_NAME ="vistior_name";
    private static String PHONE ="phone";
    private static String ADDRESS ="address";
    private static String PURPOSE ="purpose";
    private static String PROFF_TYPE ="proff_type";
    private static String PROFF_DETAIL ="proff_detail";
    private static String VISITOR_ID ="visitor_id";
    private static String VISIT_IN_TIME ="visit_in_time";
    private static String VISIT_OUT_TIME ="visit_out_time";
    /**
     * id : 18
     * visitor_id : iBR12606
     * vistior_name : Govind kishor
     * phone : 9872565851
     * address : patna
     * purpose : watch school
     * image : office_admin/images/visit_06262018_060635.jpg
     * proff_type : Adhar Card
     * proff_detail : 9887727288
     * visit_in_time : 2018-06-26 12:04:00
     * visit_out_time : 0000-00-00 00:00:00
     */

    private String id;
    private String visitor_id;
    private String vistior_name;
    private String phone;
    private String address;
    private String purpose;
    private String image;
    private String proff_type;
    private String proff_detail;
    private String visit_in_time;
    private String visit_out_time;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getVisitor_id() {
        return visitor_id;
    }

    public void setVisitor_id(String visitor_id) {
        this.visitor_id = visitor_id;
    }

    public String getVistior_name() {
        return vistior_name;
    }

    public void setVistior_name(String vistior_name) {
        this.vistior_name = vistior_name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPurpose() {
        return purpose;
    }

    public void setPurpose(String purpose) {
        this.purpose = purpose;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getProff_type() {
        return proff_type;
    }

    public void setProff_type(String proff_type) {
        this.proff_type = proff_type;
    }

    public String getProff_detail() {
        return proff_detail;
    }

    public void setProff_detail(String proff_detail) {
        this.proff_detail = proff_detail;
    }

    public String getVisit_in_time() {
        return visit_in_time;
    }

    public void setVisit_in_time(String visit_in_time) {
        this.visit_in_time = visit_in_time;
    }

    public String getVisit_out_time() {
        return visit_out_time;
    }

    public void setVisit_out_time(String visit_out_time) {
        this.visit_out_time = visit_out_time;
    }

    public static ArrayList<SecurityBean> parsereceptioistArray(JSONArray jsonArray) {
        ArrayList<SecurityBean> list = new ArrayList<SecurityBean>();
        try {

            for (int i = 0; i < jsonArray.length(); i++) {
                SecurityBean p = parserecepObject(jsonArray.getJSONObject(i));
                if (p != null) {
                    list.add(p);
                }
            }
        } catch (Exception e) {
            DebugLog.printLog("exp",e.getMessage());
            e.printStackTrace();
        }
        return list;
    }

    private static SecurityBean parserecepObject(JSONObject jsonObject) {

        SecurityBean casteObj = new SecurityBean();
        try {
            if (jsonObject.has(ID)) {
                casteObj.setId(jsonObject.getString(ID));
            }
            if (jsonObject.has(IMAGE) && !jsonObject.getString(IMAGE).isEmpty() && !jsonObject.getString(IMAGE).equalsIgnoreCase("null")) {
                casteObj.setImage(Constants.getImageBaseURL()+jsonObject.getString(IMAGE));
            }
            if (jsonObject.has(VISITOR_NAME) && !jsonObject.getString(VISITOR_NAME).isEmpty() && !jsonObject.getString(VISITOR_NAME).equalsIgnoreCase("null")) {
                casteObj.setVistior_name(jsonObject.getString(VISITOR_NAME));
            }
            if (jsonObject.has(PHONE) && !jsonObject.getString(PHONE).isEmpty() && !jsonObject.getString(PHONE).equalsIgnoreCase("null")) {
                casteObj.setPhone(jsonObject.getString(PHONE));
            }
            if (jsonObject.has(ADDRESS) && !jsonObject.getString(ADDRESS).isEmpty() && !jsonObject.getString(ADDRESS).equalsIgnoreCase("null")) {
                casteObj.setAddress(jsonObject.getString(ADDRESS));
            }
            if (jsonObject.has(PURPOSE) && !jsonObject.getString(PURPOSE).isEmpty() && !jsonObject.getString(PURPOSE).equalsIgnoreCase("null")) {
                casteObj.setPurpose(jsonObject.getString(PURPOSE));
            }
            if (jsonObject.has(PROFF_TYPE) && !jsonObject.getString(PROFF_TYPE).isEmpty() && !jsonObject.getString(PROFF_TYPE).equalsIgnoreCase("null")) {
                casteObj.setProff_type(jsonObject.getString(PROFF_TYPE));
            }
            if (jsonObject.has(PROFF_DETAIL) && !jsonObject.getString(PROFF_DETAIL).isEmpty() && !jsonObject.getString(PROFF_DETAIL).equalsIgnoreCase("null")) {
                casteObj.setProff_detail(jsonObject.getString(PROFF_DETAIL));
            }
            if (jsonObject.has(VISITOR_ID) && !jsonObject.getString(VISITOR_ID).isEmpty() && !jsonObject.getString(VISITOR_ID).equalsIgnoreCase("null")) {
                casteObj.setVisitor_id(jsonObject.getString(VISITOR_ID));
            }
            if (jsonObject.has(VISIT_IN_TIME) && !jsonObject.getString(VISIT_IN_TIME).isEmpty() && !jsonObject.getString(VISIT_IN_TIME).equalsIgnoreCase("null")) {
                casteObj.setVisit_in_time(jsonObject.getString(VISIT_IN_TIME));
            }
            if (jsonObject.has(VISIT_OUT_TIME) && !jsonObject.getString(VISIT_OUT_TIME).isEmpty() && !jsonObject.getString(VISIT_OUT_TIME).equalsIgnoreCase("null")) {
                casteObj.setVisit_out_time(jsonObject.getString(VISIT_OUT_TIME));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return casteObj;
    }

}
