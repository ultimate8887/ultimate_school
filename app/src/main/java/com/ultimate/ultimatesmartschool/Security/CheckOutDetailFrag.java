package com.ultimate.ultimatesmartschool.Security;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.ultimate.ultimatesmartschool.BeanModule.User;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;
import com.ultimate.ultimatesmartschool.Utility.Utils;


import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CheckOutDetailFrag extends Fragment implements Common_adapter.Mycallback {

    @BindView(R.id.recyadmnlst)
    RecyclerView r1;
    @BindView(R.id.parenradmin)
    RelativeLayout parent;
    RecyclerView.LayoutManager layoutManager;
    Common_adapter adminlistadapter;
    ArrayList<CheckInlistbean> adminlist=new ArrayList<>();
    private int loaded = 0;
    View view;
    @BindView(R.id.textNorecord)
    TextView txtNorecord;
    @BindView(R.id.totalRecord)
    TextView totalRecord;
    private SearchView search;
    @BindView(R.id.export)
    FloatingActionButton export;
    public CheckOutDetailFrag() {
        // Required empty public constructor
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.common_frag, container, false);
        ButterKnife.bind(this, view);
        search =(SearchView) view.findViewById(R.id.search);
        layoutManager=new LinearLayoutManager(getActivity());
        r1.setLayoutManager(layoutManager);
        adminlistadapter=new Common_adapter(adminlist,getActivity(),this);
        r1.setAdapter(adminlistadapter);

       // fetchCheckoutlist();
        search.setActivated(true);
        search.setQueryHint("Search here by Name/Mobile");
        search.onActionViewExpanded();
        search.setIconified(false);
        search.clearFocus();
        search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                search.setIconified(true);
                search.clearFocus();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if(newText.length()==0)
                {
                    fetchCheckoutlist();
                }else {
                    adminlistadapter.getFilter().filter(newText);
                }

                return false;
            }
        });
        return view;


    }


    @SuppressLint("RestrictedApi")
    @OnClick(R.id.export)
    public void submit() {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
            if (getActivity().checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED) {
                String[] permissions = {Manifest.permission.WRITE_EXTERNAL_STORAGE};
                requestPermissions(permissions, 1);
            } else {
                importData();
            }
        } else {
            importData();
        }
    }

    @SuppressLint("RestrictedApi")
    private void importData() {
        // File filePath = new File(Environment.getExternalStorageDirectory() + "/Demo.xls");
        Workbook wb = new HSSFWorkbook();

        Cell cell = null;

        String ndate="",sub_staff="",sub_date="";
//        ndate= Utils.getDateOnlyNEW(date)+","+Utils.getMonthFormated(date);
//        sub_date=ndate.substring(0,3);


        sub_date="Check-Out Visitor list";


        Sheet sheet = null;
        sheet = wb.createSheet("Check-Out Visitor list");
        //Now column and row
        Row row = sheet.createRow(0);

        cell = row.createCell(0);
        cell.setCellValue("S No");

        cell = row.createCell(1);
        cell.setCellValue("Name");

        cell = row.createCell(2);
        cell.setCellValue("To Meet");

        cell = row.createCell(3);
        cell.setCellValue("Mobile");

        cell = row.createCell(4);
        cell.setCellValue("Check-In at");

        cell = row.createCell(5);
        cell.setCellValue("Check-Out at");

        cell = row.createCell(6);
        cell.setCellValue("Proof Type");

        cell = row.createCell(7);
        cell.setCellValue("Proof no");

        cell = row.createCell(8);
        cell.setCellValue("Vehicle Type");

        cell = row.createCell(9);
        cell.setCellValue("Vehicle No");

        cell = row.createCell(10);
        cell.setCellValue("Vehicle Color");

        cell = row.createCell(11);
        cell.setCellValue("Purpose");


        //column width
        sheet.setColumnWidth(0, (20 * 100));
        sheet.setColumnWidth(1, (20 * 150));
        sheet.setColumnWidth(2, (30 * 150));
        sheet.setColumnWidth(3, (30 * 250));
        sheet.setColumnWidth(4, (30 * 300));
        sheet.setColumnWidth(5, (20 * 300));
        sheet.setColumnWidth(6, (30 * 200));
        sheet.setColumnWidth(7, (30 * 200));
        sheet.setColumnWidth(8, (20 * 200));
        sheet.setColumnWidth(9, (30 * 200));
        sheet.setColumnWidth(10, (30 * 200));

        for (int i = 0; i < adminlist.size(); i++) {

            Row row1 = sheet.createRow(i + 1);

            cell = row1.createCell(0);
            cell.setCellValue(i+1);

            cell = row1.createCell(1);
            cell.setCellValue(adminlist.get(i).getSec_name());

            cell = row1.createCell(2);
            cell.setCellValue(adminlist.get(i).getSec_mode_app());

            cell = row1.createCell(3);

            cell.setCellValue((adminlist.get(i).getSec_contact_person()));
            //  cell.setCellStyle(cellStyle);

            cell = row1.createCell(4);

            cell.setCellValue(adminlist.get(i).getGate_datetime()+ " at " + adminlist.get(i).getGate_timein());

            cell = row1.createCell(5);
            cell.setCellValue(adminlist.get(i).getGate_datetime()+ " at " + adminlist.get(i).getGate_timeout());

            cell = row1.createCell(6);
            cell.setCellValue(adminlist.get(i).getProf_type());

            cell = row1.createCell(7);
            cell.setCellValue(adminlist.get(i).getProf_value());

            cell = row1.createCell(8);

            cell.setCellValue((adminlist.get(i).getSec_make_vehicle()));


            cell = row1.createCell(9);
            cell.setCellValue(adminlist.get(i).getSec_vehicle_no());

            cell = row1.createCell(10);
            cell.setCellValue(adminlist.get(i).getSec_colour());

            cell = row1.createCell(11);

            cell.setCellValue((adminlist.get(i).getSec_purpose()));



            sheet.setColumnWidth(0, (20 * 100));
            sheet.setColumnWidth(1, (20 * 150));
            sheet.setColumnWidth(2, (30 * 150));
            sheet.setColumnWidth(3, (30 * 250));
            sheet.setColumnWidth(4, (30 * 300));
            sheet.setColumnWidth(5, (20 * 300));
            sheet.setColumnWidth(6, (30 * 200));
            sheet.setColumnWidth(7, (30 * 200));
            sheet.setColumnWidth(8, (20 * 200));
            sheet.setColumnWidth(9, (30 * 200));
            sheet.setColumnWidth(10, (30 * 200));

        }
        String fileName;
        fileName = sub_date+"_" +System.currentTimeMillis() + ".xls";


        // File file = new File(getActivity().getExternalFilesDir(null), fileName);

        File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS), fileName);

        FileOutputStream fileOutputStream = null;

        try {
            fileOutputStream = new FileOutputStream(file);
            wb.write(fileOutputStream);
            Utils.openSuccessDialog("Success!","Export file in "+file,"",getActivity());
        } catch (IOException e) {
            Utils.openErrorDialog("Error writing Exception: "+ e,getActivity());
        } catch (Exception e) {
            Utils.openErrorDialog("Failed to save file due to Exception: "+ e,getActivity());
        } finally {
            try {
                if (null != fileOutputStream) {
                    fileOutputStream.close();
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

    }

    private void fetchCheckoutlist() {
        HashMap<String,String> params=new HashMap<>();
        params.put("user_id", User.getCurrentUser().getId());
        params.put("date", "");
        params.put("check", "");
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.SECURITYcheckin_LIST, apiCallback, getContext(), params);
    }

    ApiHandler.ApiCallback apiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            // txtupload.setVisibility(View.VISIBLE);
            if (error == null) {
                try {
                    if (adminlist != null) {
                        adminlist.clear();
                    }
                    JSONArray jsonArray = jsonObject.getJSONArray("visitor_data");
                    adminlist = CheckInlistbean.parsereceptioistArray(jsonArray);
                    if (adminlist.size() > 0) {
                        adminlistadapter.setsecuList(adminlist);
                        adminlistadapter.notifyDataSetChanged();
                        totalRecord.setText("Total Entries:- "+String.valueOf(adminlist.size()));
                        txtNorecord.setVisibility(View.GONE);
                        export.setVisibility(View.VISIBLE);

                    } else {
                        totalRecord.setText("Total Entries:- "+String.valueOf(adminlist.size()));
                        adminlistadapter.setsecuList(adminlist);
                        adminlistadapter.notifyDataSetChanged();
                        txtNorecord.setVisibility(View.VISIBLE);
                        export.setVisibility(View.GONE);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                export.setVisibility(View.GONE);
                totalRecord.setText("Total Entries:- "+String.valueOf(adminlist.size()));
                txtNorecord.setVisibility(View.VISIBLE);
              //  Utility.showSnackBar(error.getMessage(), parent);

            }
        }
    };



    @Override
    public void onResume() {
        super.onResume();
        fetchCheckoutlist();
    }

    @Override
    public void onApproveCallbackDoc(CheckInlistbean checkInlistbean) {
        // String path= Constants.getImageBaseURL()+ "office_admin/images/gatepass/";
        String path= Constants.getImageBaseURL()+ "office_admin/images/gatepass/"+checkInlistbean.getDocimage();
        Dialog mBottomSheetDialog = new Dialog(getActivity());
        final View sheetView = getLayoutInflater().inflate(R.layout.image_lyt, null);
        mBottomSheetDialog.setContentView(sheetView);
        mBottomSheetDialog.setCancelable(true);
        ImageView imgShow = (ImageView) sheetView.findViewById(R.id.imgShow);
        ImageView imgDownload = (ImageView) sheetView.findViewById(R.id.imgDownload);
        imgDownload.setVisibility(View.GONE);
        Utils.setImageUri(getContext(),path,imgShow);
        //Picasso.with(this).load(message_bean.getImage()).into(imgShow);
        mBottomSheetDialog.show();
        Window window = mBottomSheetDialog.getWindow();
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    }
    @Override
    public void onApproveCallback(CheckInlistbean checkInlistbean) {

    }
}
