package com.ultimate.ultimatesmartschool.NewOnlineClass;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.TargetApi;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.webkit.PermissionRequest;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.ultimate.ultimatesmartschool.OnlineClass.OnlineclsBean;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;

import butterknife.BindView;
import butterknife.ButterKnife;

public class WebActivitynew extends AppCompatActivity {
//    @BindView(R.id.web_view)
//    WebView webView;
//    private WebChromeClient.CustomViewCallback customViewCallback;
//    OnlineclsBean hostlroombean;
//    boolean edit = false;
////    @BindView(R.id.txtTitle)
////    TextView txtTitle;
//    boolean loadingFinished = true;
//    boolean redirect = false;
//    String link;
////    @BindView(R.id.imgBackmsg)
////    ImageView back;
//    Animation animation;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_activitynew);
        ButterKnife.bind(this);
//        animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.btn_blink_animation);
//        link="http://elms.xyzultimatesolution.com/index.php/bbb-room/540/";
//        webView.setWebViewClient(new WebActivitynew.Browser_Home());
//        webView.setWebChromeClient(new WebActivitynew.ChromeClient());
//        WebSettings webSettings = webView.getSettings();
//        webSettings.setUserAgentString("Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36");
//        webSettings.setJavaScriptEnabled(true);
//        webView.getSettings().setAllowFileAccessFromFileURLs(true);
//        webView.getSettings().setAllowUniversalAccessFromFileURLs(true);
//        webSettings.setAllowFileAccess(true);
//        webSettings.setAppCacheEnabled(true);
//        webSettings.setBuiltInZoomControls(true);
//        webView.getSettings().setBuiltInZoomControls(true);
//        webView.getSettings().setDomStorageEnabled(true);
//        webView.getSettings().setDatabaseEnabled(true);
//        webView.getSettings().setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
//        webView.getSettings().setLoadWithOverviewMode(true);
//        webView.getSettings().setUseWideViewPort(true);
//
//        loadWebSite();
//        ErpProgress.showProgressBar(WebActivitynew.this,"Please wait...");
//
//        webView.setWebViewClient(new WebViewClient(){
//            @Override
//            public void onPageFinished(WebView view, String url) {
//                if(!redirect){
//                    loadingFinished = true;
//                }
//
//                if(loadingFinished && !redirect){
//                    ErpProgress.cancelProgressBar();
//                } else{
//                    redirect = false;
//                }
//
//            }
//        });
//
//
//        webView.setWebChromeClient(new WebChromeClient(){
//
//            @Override
//            public void onPermissionRequest(final PermissionRequest request) {
//
//                Log.d("TAG", "onPermissionRequest");
//                WebActivitynew.this.runOnUiThread(new Runnable() {
//                    @TargetApi(Build.VERSION_CODES.M)
//                    @Override
//                    public void run() {
//                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//                            request.grant(request.getResources());
//                        }
//
//                    }
//                });
//
//            }
//        });
    }


//    @Override
//    public void onConfigurationChanged(Configuration newConfig) {
//        super.onConfigurationChanged(newConfig);
//    }
//
//    @Override
//    public void onSaveInstanceState(Bundle outState) {
//        super.onSaveInstanceState(outState);
//        webView.saveState(outState);
//    }
//
//    @Override
//    protected void onRestoreInstanceState(Bundle savedInstanceState) {
//        super.onRestoreInstanceState(savedInstanceState);
//        webView.restoreState(savedInstanceState);
//
//    }
//
//    private void loadWebSite() {
//        webView.loadUrl(link);
//    }
//
//    private class Browser_Home extends WebViewClient {
//        Browser_Home(){}
//
//        @Override
//        public void onPageStarted(WebView view, String url, Bitmap favicon) {
//            super.onPageStarted(view, url, favicon);
//        }
//
//        @Override
//        public void onPageFinished(WebView view, String url) {
//            super.onPageFinished(view, url);
//        }
//    }
//
//    private class ChromeClient extends WebChromeClient {
//        private View mCustomView;
//        private WebChromeClient.CustomViewCallback mCustomViewCallback;
//        protected FrameLayout mFullscreenContainer;
//        private int mOriginalOrientation;
//        private int mOriginalSystemUiVisibility;
//
//        ChromeClient() {
//        }
//
//        public Bitmap getDefaultVideoPoster() {
//            if (mCustomView == null) {
//                return null;
//            }
//            return BitmapFactory.decodeResource(getApplicationContext().getResources(), 2130837573);
//        }
//
//        public void onHideCustomView() {
//            ((FrameLayout) getWindow().getDecorView()).removeView(this.mCustomView);
//            this.mCustomView = null;
//            getWindow().getDecorView().setSystemUiVisibility(this.mOriginalSystemUiVisibility);
//            setRequestedOrientation(this.mOriginalOrientation);
//            this.mCustomViewCallback.onCustomViewHidden();
//            this.mCustomViewCallback = null;
//        }
//
//        public void onShowCustomView(View paramView, WebChromeClient.CustomViewCallback paramCustomViewCallback) {
//            if (this.mCustomView != null) {
//                onHideCustomView();
//                return;
//            }
//            this.mCustomView = paramView;
//            this.mOriginalSystemUiVisibility = getWindow().getDecorView().getSystemUiVisibility();
//            this.mOriginalOrientation = getRequestedOrientation();
//            this.mCustomViewCallback = paramCustomViewCallback;
//            ((FrameLayout) getWindow().getDecorView()).addView(this.mCustomView, new FrameLayout.LayoutParams(-1, -1));
//            getWindow().getDecorView().setSystemUiVisibility(3846 | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
//        }
//
//
//    }
}