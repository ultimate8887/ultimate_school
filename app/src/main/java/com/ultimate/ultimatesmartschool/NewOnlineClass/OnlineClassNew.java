package com.ultimate.ultimatesmartschool.NewOnlineClass;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.ultimate.ultimatesmartschool.BeanModule.ClassBean;
import com.ultimate.ultimatesmartschool.BeanModule.User;
import com.ultimate.ultimatesmartschool.Login.LoginActivity;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.apptik.widget.multiselectspinner.BaseMultiSelectSpinner;
import io.apptik.widget.multiselectspinner.MultiSelectSpinner;

public class OnlineClassNew extends AppCompatActivity {
    ArrayList<ClassBean> classList = new ArrayList<>();
    @BindView(R.id.testadd)
    RelativeLayout parent;
    //@BindView(R.id.spinner) Spinner spinnerClass;
    @BindView(R.id.spinner)
    MultiSelectSpinner multiselectSpinnerclass;
    String classid = "";
    private int loaded = 0;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    List<String> classidsel;
    @BindView(R.id.textView7)TextView textView7;
    ArrayList<OnlineclassBean> subjectList = new ArrayList<>();
    String link;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_online_class_new);
        ButterKnife.bind(this);
        classidsel = new ArrayList<>();
        fetchSubject();
        fetchClass();
        txtTitle.setText("Online Class");
    }
    @OnClick(R.id.imgBack)
    public void onback() {
        finish();
    }
    private void fetchClass() {
        if (loaded == 0) {
            ErpProgress.showProgressBar(this, "Please wait...");
        }
        loaded++;

        HashMap<String, String> params = new HashMap<String, String>();
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.CLASSLIST_URL, classapiCallback, this, params);
    }

    ApiHandler.ApiCallback classapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            if (error == null) {
                try {
                    if (classList != null) {
                        classList.clear();
                    }
                    classList = ClassBean.parseClassArray(jsonObject.getJSONArray(Constants.CLASSDATA));
                    List<String> dstring = new ArrayList<>();
                    for (ClassBean cobj : classList) {
                        dstring.add(cobj.getName());
                        Log.e("getclassid",cobj.getId());
                    }

                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(OnlineClassNew.this, android.R.layout.simple_list_item_multiple_choice, dstring);

                    multiselectSpinnerclass.setListAdapter(adapter).setListener(new BaseMultiSelectSpinner.MultiSpinnerListener() {
                        @Override
                        public void onItemsSelected(boolean[] selected) {

                            Log.e("dcfcds", "fdsf");
                            String value = multiselectSpinnerclass.getSpinnerText();
                            List<String> clas = new ArrayList<>();
                            classidsel.clear();
                            if (!value.isEmpty()) {
                                clas = Arrays.asList(value.split("\\s*,\\s*"));
                            }
                            for (String dval : clas) {
                                for (ClassBean obj : classList) {
                                    if (obj.getName().equalsIgnoreCase(dval)) {
                                        classidsel.add(obj.getId());
                                        //  textView7.setVisibility(View.GONE);

                                        break;
                                    }
                                }
                            }

                            fetchSubject();
                            Log.e("dcfcds", value + "  ,  classid:  " + classidsel);
                            // }
                        }
                    }).setSelectAll(false)
                            .setSpinnerItemLayout(androidx.appcompat.R.layout.support_simple_spinner_dropdown_item)
                            .setTitle("Select Class")

                            .setMaxSelectedItems(1)
                    ;


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            } else {
                Utils.showSnackBar(error.getMessage(), parent);
                if (error.getStatusCode() == 405) {
                    User.logout();
                    startActivity(new Intent(OnlineClassNew.this, LoginActivity.class));
                    finish();
                }
            }
        }
    };


    private void fetchSubject() {
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("class_id", String.valueOf(classidsel));
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.live_classurl, subapiCallback, this, params);

    }

    ApiHandler.ApiCallback subapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            /* */

            if (error == null) {
                try {
                    subjectList =   OnlineclassBean.parseClassArray(jsonObject.getJSONArray("class_data"));
                    link=subjectList.get(0).getLink();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    };
    @OnClick(R.id.button3)
    public void save() {
        if(checkValid()) {
            String url = link;
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(url));
            startActivity(i);
        }

    }


    private boolean checkValid() {

        boolean valid = true;
        String errorMsg = null;
        if (link.isEmpty()) {
            valid = false;
            errorMsg = "Please select class";
        }

        if (!valid) {
            Toast.makeText(getApplicationContext(),errorMsg,Toast.LENGTH_LONG).show();

        }
        return valid;
    }
}