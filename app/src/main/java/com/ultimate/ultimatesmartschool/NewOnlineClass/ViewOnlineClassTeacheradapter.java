package com.ultimate.ultimatesmartschool.NewOnlineClass;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.daimajia.swipe.SimpleSwipeListener;
import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;
import com.ultimate.ultimatesmartschool.Ebook.EbookBean;
import com.ultimate.ultimatesmartschool.Ebook.Ebookadapter;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import java.util.ArrayList;

public class ViewOnlineClassTeacheradapter extends RecyclerSwipeAdapter<ViewOnlineClassTeacheradapter.Viewholder> {
    ArrayList<ViewOnlineClassTeacherbean> hq_list;

    Context listner;

    public ViewOnlineClassTeacheradapter(ArrayList<ViewOnlineClassTeacherbean> hq_list, Context listener) {

        this.hq_list = hq_list;
        this.listner = listener;
    }


    @Override
    public Viewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.ebookada_lay, parent, false);
        Viewholder viewholder = new Viewholder(view);
        return viewholder;

    }

    @Override
    public void onBindViewHolder(final Viewholder holder, final int position) {
        holder.parent.setAnimation(AnimationUtils.loadAnimation(listner,R.anim.fade_scale_animation));
        holder.t1.setText("Teacher Id : "+hq_list.get(position).getUser_id());
        holder.textwriter.setText("Teacher Name: "+hq_list.get(position).getUser_name());
        holder.t2.setText(hq_list.get(position).getTiming());
        holder.subName.setVisibility(View.INVISIBLE);

        mItemManger.bindView(holder.itemView, position);

        holder.swipeLayout.setShowMode(SwipeLayout.ShowMode.PullOut);
        holder.swipeLayout.addSwipeListener(new SimpleSwipeListener() {
            @Override
            public void onOpen(SwipeLayout layout) {

            }

            @Override
            public void onStartClose(SwipeLayout layout) {
                holder.swipeLayout.close();
            }
        });








    }



    @Override
    public int getItemCount() {
        return hq_list.size();
    }

    public void setHQList(ArrayList<ViewOnlineClassTeacherbean> hq_list) {
        this.hq_list = hq_list;
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe;
    }


    public class Viewholder extends RecyclerView.ViewHolder {
        public TextView t1;
        public TextView t2;
        CardView parent;
        public TextView iv2,txtpdf,subName,textwriter;
        public ImageView trash, update;
        SwipeLayout swipeLayout;
        View view1;
        public Viewholder(View itemView) {
            super(itemView);
            parent =(CardView)itemView.findViewById(R.id.card1);
            swipeLayout = (SwipeLayout) itemView.findViewById(R.id.swipe);
            t1 = (TextView) itemView.findViewById(R.id.textcomment);
            t2 = (TextView) itemView.findViewById(R.id.textdate);
            subName= (TextView) itemView.findViewById(R.id.subName);
            textwriter=(TextView)itemView.findViewById(R.id.textwriter) ;
            iv2 = (TextView) itemView.findViewById(R.id.txtimage);
            txtpdf = (TextView) itemView.findViewById(R.id.txtpdf);

            trash = (ImageView) itemView.findViewById(R.id.trash);
            update = (ImageView) itemView.findViewById(R.id.update);
            view1=(View)itemView.findViewById(R.id.view1);
        }
    }
}
