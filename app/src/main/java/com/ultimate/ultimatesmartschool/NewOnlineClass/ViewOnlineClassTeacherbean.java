package com.ultimate.ultimatesmartschool.NewOnlineClass;

import com.ultimate.ultimatesmartschool.Ebook.EbookBean;
import com.ultimate.ultimatesmartschool.Utility.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ViewOnlineClassTeacherbean {

    private String id;
    private String user_id;
    private String user_name;
    private String timing;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getTiming() {
        return timing;
    }

    public void setTiming(String timing) {
        this.timing = timing;
    }

    public static ArrayList<ViewOnlineClassTeacherbean> parseSyllabusArray(JSONArray arrayObj) {
        ArrayList<ViewOnlineClassTeacherbean> list = new ArrayList<ViewOnlineClassTeacherbean>();
        try {

            for (int i = 0; i < arrayObj.length(); i++) {
                ViewOnlineClassTeacherbean p = parseSyllabusObject(arrayObj.getJSONObject(i));
                if (p != null) {
                    list.add(p);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public static ViewOnlineClassTeacherbean parseSyllabusObject(JSONObject jsonObject) {
        ViewOnlineClassTeacherbean casteObj = new ViewOnlineClassTeacherbean();
        try {
            if (jsonObject.has("id")&& !jsonObject.getString("id").isEmpty() && !jsonObject.getString("id").equalsIgnoreCase("null")) {
                casteObj.setId(jsonObject.getString("id"));
            }
            if (jsonObject.has("user_id") && !jsonObject.getString("user_id").isEmpty() && !jsonObject.getString("user_id").equalsIgnoreCase("null")) {
                casteObj.setUser_id(jsonObject.getString("user_id"));
            }
            if (jsonObject.has("user_name") && !jsonObject.getString("user_name").isEmpty() && !jsonObject.getString("user_name").equalsIgnoreCase("null")) {
                casteObj.setUser_name(jsonObject.getString("user_name"));
            }
            if (jsonObject.has("timing") && !jsonObject.getString("timing").isEmpty() && !jsonObject.getString("timing").equalsIgnoreCase("null")) {
                casteObj.setTiming(jsonObject.getString("timing"));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return casteObj;
    }
}
