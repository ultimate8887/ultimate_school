package com.ultimate.ultimatesmartschool.NewOnlineClass;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.TargetApi;
import android.graphics.Bitmap;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.PermissionRequest;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;

public class NewOnlineClass extends AppCompatActivity {
//    WebView webView;
//
//    WebSettings webSettings;
//    boolean loadingFinished = true;
//    boolean redirect = false;
//    ProgressBar progressBar;
//    final String CUSTOM_TAB_PACKAGE_NAME = "com.android.chrome";
//   String url;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_online_class);
//        WebView webView = (WebView) findViewById(R.id.web_view);
//        url="http://elms.xyzultimatesolution.com/";
//        progressBar=(ProgressBar)findViewById(R.id.progress_bar);
//        try {
//            Log.d("BBB URL is ", url);
//
//            Log.d("WebView Version", "=======>" + webView.getSettings().getUserAgentString());
//            //webView = (WebView) view.findViewById(R.id.webviewBBB);
//            setUpWebViewDefaults(webView);
//            webView.setWebChromeClient(new MyWebClient());
//            webView.setWebViewClient(new MyBrowser());
//            webView.getSettings().setJavaScriptEnabled(true);
//            webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
//            webView.getSettings().setUserAgentString("Mozilla/5.0 (Linux; Android 5.1.1; Nexus 5 Build/LMY48B; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/43.0.2357.65 Mobile Safari/537.36");
//
//            // AppRTC requires third party cookies to work
//            CookieManager cookieManager = CookieManager.getInstance();
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//                cookieManager.setAcceptThirdPartyCookies(webView, true);
//            }
//            webView.loadUrl(url);
//            ErpProgress.showProgressBar(NewOnlineClass.this,"Please wait...");
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

    }
//    private static class MyBrowser extends WebViewClient {
//        @Override
//        public boolean shouldOverrideUrlLoading(WebView view, String url) {
//            view.loadUrl(url);
//            return true;
//        }
//
//        @Override
//        public void onPageStarted(
//                WebView view, String url, Bitmap favicon) {
//            Log.i("Tag", "page started:" + url);
//            super.onPageStarted(view, url, favicon);
//        }
//
//        @Override
//        public void onPageFinished(WebView view, final String url) {
//
//
//                ErpProgress.cancelProgressBar();
//
//            Log.i("Tag", "page finished:" + url);
//        }
//
//        //Show loader on url load
//        public void onLoadResource(final WebView view, String url) {
//            Uri uri = Uri.parse(url);
////            CustomTabsIntent.Builder intentBuilder = new CustomTabsIntent.Builder();
////            //Open the Custom Tab
////            intentBuilder.build().launchUrl(getContext(), uri);
//        }
//
//        @Override
//        public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError er) {
//            handler.proceed();
//        }
//
//    }
//
//    private class MyWebClient extends WebChromeClient {
//
//
//        @Override
//        public void onPermissionRequest(PermissionRequest request) {
//
//            Log.d("TAG", "onPermissionRequest");
//            NewOnlineClass.this.runOnUiThread(new Runnable() {
//                @TargetApi(Build.VERSION_CODES.M)
//                @Override
//                public void run() {
//                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//                        request.grant(request.getResources());
//                    }
//
//                }
//            });
//        }
//
//
//    }
//
//
//
//    private void setUpWebViewDefaults(WebView webView) {
//        WebSettings settings = webView.getSettings();
//
//        // Enable Javascript
//        settings.setJavaScriptEnabled(true);
//
//        // Use WideViewport and Zoom out if there is no viewport defined
//        settings.setUseWideViewPort(true);
//        settings.setLoadWithOverviewMode(true);
//
//        // Enable pinch to zoom without the zoom buttons
//        settings.setBuiltInZoomControls(true);
//
//        // Allow use of Local Storage
//        settings.setDomStorageEnabled(true);
//
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            // Hide the zoom controls for HONEYCOMB+
//            settings.setDisplayZoomControls(false);
//        }
//
//        // Enable remote debugging via chrome://inspect
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            WebView.setWebContentsDebuggingEnabled(true);
//        }
//
//        webView.setWebViewClient(new WebViewClient());
//
//        webView.canGoBack();
//        webView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            webView.getSettings().setSafeBrowsingEnabled(true);
//        }
//        webView.getSettings().setLoadsImagesAutomatically(true);
//        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
//        webView.getSettings().setBuiltInZoomControls(true);
//        webView.getSettings().setDomStorageEnabled(true);
//        webView.getSettings().setDatabaseEnabled(true);
//        webView.getSettings().setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
//        webView.getSettings().setLoadWithOverviewMode(true);
//        webView.getSettings().setUseWideViewPort(true);
//        webView.getSettings().setAppCacheEnabled(true);
//        webView.getSettings().setUserAgentString("Mozilla/5.0 (Linux; Android 5.1.1; Nexus 5 Build/LMY48B; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/43.0.2357.65 Mobile Safari/537.36");
//
//        // AppRTC requires third party cookies to work
//        CookieManager cookieManager = CookieManager.getInstance();
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            cookieManager.setAcceptThirdPartyCookies(webView, true);
//        }
//    }
}