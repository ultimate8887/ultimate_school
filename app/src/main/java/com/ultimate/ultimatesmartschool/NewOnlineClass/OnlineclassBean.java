package com.ultimate.ultimatesmartschool.NewOnlineClass;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class OnlineclassBean {
    private static String ID = "id";
    private static String LINK = "link";

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getClass_id() {
        return class_id;
    }

    public void setClass_id(String class_id) {
        this.class_id = class_id;
    }

    /**
     * id : 2
     * name : NURSERY
     * school_id : 0
     * group_id : 9
     * order_by : 1
     */

    private String id;
    private String link;

    private String class_id;
    public static ArrayList<OnlineclassBean> parseClassArray(JSONArray arrayObj) {
        ArrayList list = new ArrayList<OnlineclassBean>();
        try {
            for (int i = 0; i < arrayObj.length(); i++) {
                OnlineclassBean p = parseClassObject(arrayObj.getJSONObject(i));
                if (p != null) {
                    list.add(p);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public static OnlineclassBean parseClassObject(JSONObject jsonObject) {
        OnlineclassBean casteObj = new OnlineclassBean();
        try {
            if (jsonObject.has("id")) {
                casteObj.setId(jsonObject.getString("id"));
            }
            if (jsonObject.has("link") && !jsonObject.getString("link").isEmpty() && !jsonObject.getString("link").equalsIgnoreCase("null")) {
                casteObj.setLink(jsonObject.getString("link"));
            }
            if (jsonObject.has("class_id") && !jsonObject.getString("class_id").isEmpty() && !jsonObject.getString("class_id").equalsIgnoreCase("null")) {
                casteObj.setClass_id(jsonObject.getString("class_id"));
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }
        return casteObj;
    }
}
