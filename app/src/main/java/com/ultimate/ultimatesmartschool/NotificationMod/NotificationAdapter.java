package com.ultimate.ultimatesmartschool.NotificationMod;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.ultimate.ultimatesmartschool.R;

import java.util.ArrayList;

import butterknife.BindView;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.ViewHolder> {

    private final Context mContext;
    ArrayList<NotificationBean> notificationList;
    private AdapterCallback mAdapterCallback;
    @BindView(R.id.imgSpeech)
    ImageView imageView;
    Animation animation;
    public void setNotificationList(ArrayList<NotificationBean> notificationList) {
        this.notificationList = notificationList;


    }

    public interface AdapterCallback {
        public void deleteNotification(NotificationBean mNotificationBean);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView textView;
        public TextView textView2;
        public TextView textView3,txtDate;
        ImageView imgDelete;


        public ViewHolder(View itemView) {
            super(itemView);
            textView = (TextView) itemView.findViewById(R.id.textView);
            textView2 = (TextView) itemView.findViewById(R.id.textView2);
            textView3 = (TextView) itemView.findViewById(R.id.textView3);
            imgDelete = (ImageView) itemView.findViewById(R.id.imgSpeech);
            txtDate= (TextView) itemView.findViewById(R.id.txtDate);
        }
    }

    public NotificationAdapter(ArrayList<NotificationBean> notificationList, AdapterCallback mAdapterCallback, Context mContext) {
        this.notificationList = notificationList;
        this.mAdapterCallback = mAdapterCallback;
        this.mContext=mContext;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int i) {


        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.activity_notification_adapt, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;

    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int postion) {
      //  viewHolder.parent.setAnimation(AnimationUtils.loadAnimation(mContext,R.anim.up_to_down));
        animation = AnimationUtils.loadAnimation(mContext, R.anim.btn_blink_animation);
        viewHolder.textView.setText(notificationList.get(postion).getN_title());
        viewHolder.textView2.setText(notificationList.get(postion).getN_time());
        viewHolder.textView3.setText(notificationList.get(postion).getN_msg());
        viewHolder.txtDate.setText(notificationList.get(postion).getN_Date());
        viewHolder.imgDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                viewHolder.imgDelete.startAnimation(animation);
                mAdapterCallback.deleteNotification(notificationList.get(postion));

//                AlertDialog.Builder builder1 = new AlertDialog.Builder(mContext);
//                builder1.setMessage("Do you want to delete? ");
//                builder1.setCancelable(false);
//                builder1.setPositiveButton(
//                        "Yes", new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialogInterface, int i) {
//                                mAdapterCallback.deleteNotification(notificationList.get(postion));
//                            }
//                        });
//                builder1.setNegativeButton(
//                        "No", new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialogInterface, int i) {
//                                dialogInterface.dismiss();
//                            }
//                        });
//                AlertDialog alert11 = builder1.create();
//                alert11.show();


            }
        });
    }

    @Override
    public int getItemCount() {
        return notificationList.size();
    }


}
