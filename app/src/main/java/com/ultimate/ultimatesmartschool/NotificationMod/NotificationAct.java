package com.ultimate.ultimatesmartschool.NotificationMod;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.ultimate.ultimatesmartschool.BeanModule.User;
import com.ultimate.ultimatesmartschool.Login.LoginActivity;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class NotificationAct extends AppCompatActivity implements NotificationAdapter.AdapterCallback, TextToSpeech.OnInitListener{
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    int count=0,speech=0;
    TextToSpeech textToSpeech;
    String textholder="",title="";
    ArrayList<NotificationBean> datalist = new ArrayList<>();
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.txtDeleteAll)
    TextView txtDeleteAll;
    private RecyclerView.LayoutManager layoutManager;
    private NotificationAdapter adapter1;
    int loaded = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        ButterKnife.bind(this);
        txtTitle.setText("Notification");
        textToSpeech = new TextToSpeech(this, this);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        adapter1 = new NotificationAdapter(datalist, this, this);
        recyclerView.setAdapter(adapter1);
        fetchNotification();
    }

    @OnClick(R.id.imgBack)
    public void imgBack() {
        finish();
    }

    public void fetchNotification() {
        if (loaded == 0) {
            ErpProgress.showProgressBar(this, "Please wait...");
        }
        loaded++;
        HashMap<String, String> params = new HashMap<>();
        params.put("id", User.getCurrentUser().getId());
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.NOTIFICATIONURL, apiCallback, this, params);
    }

    ApiHandler.ApiCallback apiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            if (error == null) {
                try {
                    JSONArray noticeArray = jsonObject.getJSONArray("notice_data");
                    datalist = NotificationBean.parseNotificationArray(noticeArray);

                    if (datalist.size() > 0) {
                        adapter1.setNotificationList(datalist);
                        adapter1.notifyDataSetChanged();
                        txtDeleteAll.setVisibility(View.GONE);
                    } else {
                        txtDeleteAll.setVisibility(View.GONE);
                    }


                } catch (JSONException e) {
                    e.printStackTrace();

                }
            } else {
                txtDeleteAll.setVisibility(View.GONE);
                Toast.makeText(getApplicationContext(), "Something went wrong!", Toast.LENGTH_SHORT).show();
                if (error.getStatusCode() == 405) {
                    User.logout();
                    startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                   finish();
                }
            }
        }
    };

    @Override
    public void deleteNotification(final NotificationBean notiList) {
        speech=1;
        textholder = notiList.getN_title()+" ! "+ notiList.getN_msg();
        TextToSpeechFunction();
//        HashMap<String, String> params = new HashMap<>();
//        if (mNotificationBean == null) {
//            params.put("id", "0");
//            params.put("all", "1");
//        } else {
//            params.put("id", mNotificationBean.getN_id());
//            params.put("all", "0");
//        }
//        params.put("user_id", User.getCurrentUser().getId());
//
//        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.DELETENOTIFICATIONURL, new ApiHandler.ApiCallback() {
//
//            @Override
//            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
//                if (error == null) {
//                    if (mNotificationBean == null) {
//                        datalist.clear();
//                        adapter1.setNotificationList(datalist);
//                        adapter1.notifyDataSetChanged();
//                        txtDeleteAll.setVisibility(View.GONE);
//                    } else {
//                        int pos = datalist.indexOf(mNotificationBean);
//                        datalist.remove(pos);
//                        adapter1.notifyItemRemoved(pos);
//                        if (datalist.size() <= 0) {
//                            txtDeleteAll.setVisibility(View.GONE);
//                        }
//                    }
//                } else {
//                    if (error.getStatusCode() == 405) {
//                        User.logout();
//                        startActivity(new Intent(getApplicationContext(), LoginActivity.class));
//                        finish();
//                    }
//                }
//            }
//        }, this, params);
    }


    //text to speech programmatically...
    public void TextToSpeechFunction() {
        textToSpeech.speak(textholder, TextToSpeech.QUEUE_FLUSH, null);
    }


    @Override
    public void onDestroy() {
        if (textToSpeech != null) {
            textToSpeech.shutdown();
        }
        super.onDestroy();
    }

    @OnClick(R.id.txtDeleteAll)
    public void setTxtDeleteAll()
    {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(getApplicationContext());
        builder1.setMessage("Do you want to delete all? ");
        builder1.setCancelable(false);
        builder1.setPositiveButton(
                "Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        deleteNotification(null);
                    }
                });
        builder1.setNegativeButton(
                "No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

    @Override
    public void onInit(int status) {
        if (status == TextToSpeech.SUCCESS) {
            textToSpeech.setLanguage(Locale.US);
            if (speech==0) {
                TextToSpeechFunction();
            }
        }
    }
}
