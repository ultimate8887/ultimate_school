package com.ultimate.ultimatesmartschool.SchoolActivity;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.ultimate.ultimatesmartschool.R;

import java.util.ArrayList;

public class GroupActivityAdapter extends BaseAdapter {
    private final ArrayList<ActivityNameBean> casteList;
    Context context;
    LayoutInflater inflter;
    int value=0;

    public GroupActivityAdapter(Context applicationContext, ArrayList<ActivityNameBean> casteList,int value) {
        this.context = applicationContext;
        this.casteList = casteList;
        this.value = value;
        inflter = (LayoutInflater.from(applicationContext));
    }

    @Override
    public int getCount() {
        return casteList.size() + 1;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        view = inflter.inflate(R.layout.spinner_lyt, null);
        TextView label = (TextView) view.findViewById(R.id.txtText);
        if (i == 0) {
            // Default selected Spinner item
            if (value==2){
                label.setText("Select Activity");
            }else {
                label.setText("Select group");
            }
        } else {
            ActivityNameBean classObj = casteList.get(i - 1);
            label.setText(classObj.getName());
        }

        return view;
    }
}
