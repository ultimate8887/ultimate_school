package com.ultimate.ultimatesmartschool.SchoolActivity;

import com.ultimate.ultimatesmartschool.BeanModule.CommonBean;
import com.ultimate.ultimatesmartschool.Utility.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ActivityNameBean {

    private static String ID = "id";
    private static String NAME = "name";
    private static String FROM_DATE = "from_date";
    private static String TO_DATE = "to_date";
    private static String VENUE = "venue";
    private static String VENUE_ADD = "venue_address";
    /**
     * id : 2
     * name : NURSERY
     * school_id : 0
     * group_id : 9
     * order_by : 1
     */

    private String id;
    private String name;

    private String from_date;
    private String to_date;
    private String venue;

    public String getFrom_date() {
        return from_date;
    }

    public void setFrom_date(String from_date) {
        this.from_date = from_date;
    }

    public String getTo_date() {
        return to_date;
    }

    public void setTo_date(String to_date) {
        this.to_date = to_date;
    }

    public String getVenue() {
        return venue;
    }

    public void setVenue(String venue) {
        this.venue = venue;
    }

    public String getVenue_address() {
        return venue_address;
    }

    public void setVenue_address(String venue_address) {
        this.venue_address = venue_address;
    }

    private String venue_address;

    /**
     * group_data : {"id":"1","name":"Pre-Primary(Nry-Prep2)"}
     */

    public CommonBean getGroup_data() {
        return group_data;
    }

    public void setGroup_data(CommonBean group_data) {
        this.group_data = group_data;
    }

    private CommonBean group_data;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }



    public static ArrayList<ActivityNameBean> parseClassArray(JSONArray arrayObj) {
        ArrayList list = new ArrayList<ActivityNameBean>();
        try {
            for (int i = 0; i < arrayObj.length(); i++) {
                ActivityNameBean p = parseClassObject(arrayObj.getJSONObject(i));
                if (p != null) {
                    list.add(p);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public static ActivityNameBean parseClassObject(JSONObject jsonObject) {
        ActivityNameBean casteObj = new ActivityNameBean();
        try {
            if (jsonObject.has(ID)) {
                casteObj.setId(jsonObject.getString(ID));
            }
            if (jsonObject.has(NAME) && !jsonObject.getString(NAME).isEmpty() && !jsonObject.getString(NAME).equalsIgnoreCase("null")) {
                casteObj.setName(jsonObject.getString(NAME));
            }
            if (jsonObject.has(FROM_DATE) && !jsonObject.getString(FROM_DATE).isEmpty() && !jsonObject.getString(FROM_DATE).equalsIgnoreCase("null")) {
                casteObj.setFrom_date(jsonObject.getString(FROM_DATE));
            }
            if (jsonObject.has(TO_DATE) && !jsonObject.getString(TO_DATE).isEmpty() && !jsonObject.getString(TO_DATE).equalsIgnoreCase("null")) {
                casteObj.setTo_date(jsonObject.getString(TO_DATE));
            }
            if (jsonObject.has(VENUE) && !jsonObject.getString(VENUE).isEmpty() && !jsonObject.getString(VENUE).equalsIgnoreCase("null")) {
                casteObj.setVenue(jsonObject.getString(VENUE));
            }
            if (jsonObject.has(VENUE_ADD) && !jsonObject.getString(VENUE_ADD).isEmpty() && !jsonObject.getString(VENUE_ADD).equalsIgnoreCase("null")) {
                casteObj.setVenue_address(jsonObject.getString(VENUE_ADD));
            }

            if (jsonObject.has(Constants.GROUP_DATA)) {
                casteObj.setGroup_data(CommonBean.parseCommonObject(jsonObject.getJSONObject(Constants.GROUP_DATA)));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return casteObj;
    }

}
