package com.ultimate.ultimatesmartschool.SchoolActivity;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetSequence;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.ultimate.ultimatesmartschool.BeanModule.CommonBean;
import com.ultimate.ultimatesmartschool.Examination.GroupSpinnerAdapter;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.CommonProgress;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;
import com.ultimate.ultimatesmartschool.Utility.Utils;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ViewStudents extends AppCompatActivity implements ActivityStudentAdapter.Mycallback {

    SharedPreferences sharedPreferences;
    @BindView(R.id.imgBack)
    ImageView back;

    @BindView(R.id.txtTitle)
    TextView txtTitle;

    @BindView(R.id.txtSub)
    TextView txtSub;



    @BindView(R.id.p_recyclerview)
    RecyclerView p_recyclerview;
    @BindView(R.id.v_recyclerview)
    RecyclerView v_recyclerview;
    Spinner spinnerGroup,spinnerActivity;
    Spinner spinnersubject;
    private String activity_id = "";
    private String activity_name = "";

    @BindView(R.id.oneDay)
    RelativeLayout p_day;

    @BindView(R.id.parent)
    RelativeLayout parent;

    @BindView(R.id.multiDay)
    RelativeLayout v_day;

    @BindView(R.id.img1)
    ImageView img1;
    @BindView(R.id.img2)
    ImageView img2;

    String apply="pending";
    Dialog mBottomSheetDialog;
    @BindView(R.id.p_lyt)
    LinearLayout p_lyt;

    @BindView(R.id.v_lyt)
    LinearLayout v_lyt;

    @BindView(R.id.cal_img)
    ImageView cal_img;
    @BindView(R.id.today_date)
    TextView today_date;
    @BindView(R.id.dialog)
    ImageView dialog;
    int check=0;
    private LinearLayoutManager layoutManager,layoutManager1;

    Animation animation;
    @BindView(R.id.textNorecord)
    TextView txtNorecord;
    @BindView(R.id.totalRecord)
    TextView totalRecord;

    CommonProgress commonProgress;
    String sub_id= "", sub_name= "" ,sectionid="",sectionname="",classid = "",className = "",check_view = "";

    String from_date = "";
    private int loaded = 0;
    ActivityStudentAdapter p_mAdapter;
    ArrayList<ActivityStudentBean> p_list=new ArrayList<>();

    GroupActivityAdapter adapter;
    ArrayList<ActivityNameBean> dataList = new ArrayList<>();
    private ArrayList<CommonBean> groupList = new ArrayList<>();
    private String groupid = "",staffid="",staffname="",staffimg="";
    private String group_name = "";

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_students);
        ButterKnife.bind(this);
        commonProgress=new CommonProgress(this);
        animation = AnimationUtils.loadAnimation(this, R.anim.btn_blink_animation);
        if (getIntent().getExtras() != null) {
            staffid = getIntent().getExtras().getString("staffid");
            staffname = getIntent().getExtras().getString("staffname");
            staffimg = getIntent().getExtras().getString("staffimg");
            if (!restorePrefData()){
                setShowcaseView();
            }else {
                openDialog();
            }
        }
        layoutManager = new LinearLayoutManager(this);
        p_recyclerview.setLayoutManager(layoutManager);
        p_mAdapter = new ActivityStudentAdapter(p_list, this,this,2);
        p_recyclerview.setAdapter(p_mAdapter);

    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @OnClick(R.id.dialog)
    public void dialogggg() {
        openDialog();
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void openDialog() {
        mBottomSheetDialog = new BottomSheetDialog(this);
        final View sheetView = getLayoutInflater().inflate(R.layout.search_dialog, null);
        mBottomSheetDialog.setContentView(sheetView);
        mBottomSheetDialog.setCancelable(false);
        spinnerGroup=(Spinner) sheetView.findViewById(R.id.spinnerClass);
        spinnerActivity=(Spinner) sheetView.findViewById(R.id.spinnersection);
        spinnersubject=(Spinner) sheetView.findViewById(R.id.spinnersubject);
        ImageView close=(ImageView) sheetView.findViewById(R.id.close);
        TextView txtSetup=(TextView) sheetView.findViewById(R.id.txtSetup);

        TextView textView7=(TextView) sheetView.findViewById(R.id.textView7);
        TextView textsection=(TextView) sheetView.findViewById(R.id.textsection);

        textView7.setText("Category");
        textsection.setText("Activity");

        txtSetup.setText("View Student Activity");
        //txtTitle.setText("Mark Attendance");
        fetchGroup();
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //assignData();
                if (check==0) {
                    finish();
                    mBottomSheetDialog.dismiss();
                }else {
                    mBottomSheetDialog.dismiss();
                }
            }
        });


        Button btnYes= (Button) sheetView.findViewById(R.id.btnYes);


        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (groupid.equalsIgnoreCase(""))  {
                    Toast.makeText(ViewStudents.this,"Kindly Select Category!", Toast.LENGTH_SHORT).show();
                }else if (activity_id.equalsIgnoreCase("")){
                    Toast.makeText(ViewStudents.this,"Kindly Select Activity!", Toast.LENGTH_SHORT).show();
                }  else {
                    check++;
                    p_fetchhwlist(from_date,"");

                    setCommonData();
                }

            }
        });
        mBottomSheetDialog.show();

    }

    private void fetchGroup() {
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("staff_id", staffid);
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.CATEGORY_LIST_URL, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                ErpProgress.cancelProgressBar();
                commonProgress.dismiss();
                if (error == null) {
                    try {
                        groupList.clear();
                        groupList = CommonBean.parseCommonArray(jsonObject.getJSONArray(Constants.GROUP_DATA));
                        GroupSpinnerAdapter_new adapter1 = new GroupSpinnerAdapter_new(ViewStudents.this, groupList);
                        spinnerGroup.setAdapter(adapter1);
                        spinnerGroup.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                                if (i > 0) {
                                    groupid = "";
                                    group_name = "";
                                    groupid = groupList.get(i - 1).getId();
                                    group_name = groupList.get(i - 1).getName();
                                    fetchActivity(groupid);
                                } else {
                                    groupid = "";
                                    group_name = "";
                                    // fetchActivity(groupid);
                                    // recyclerView.setVisibility(View.GONE);
                                }
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> adapterView) {

                            }
                        });

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    Utils.showSnackBar(error.getMessage(), parent);
                    Toast.makeText(ViewStudents.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }, this, params);
    }


    private void fetchActivity(String groupid) {
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("group_id", groupid);
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.Activity_URL, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                ErpProgress.cancelProgressBar();
                commonProgress.dismiss();
                if (error == null) {
                    try {
                        dataList.clear();
                        dataList = ActivityNameBean.parseClassArray(jsonObject.getJSONArray(Constants.CLASSDATA));
                        adapter = new GroupActivityAdapter(ViewStudents.this, dataList, 2);
                        spinnerActivity.setAdapter(adapter);
                        spinnerActivity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                if (i > 0) {
                                    activity_id = "";
                                    activity_name = "";
                                    activity_id = dataList.get(i - 1).getId();
                                    activity_name = dataList.get(i - 1).getName();
                                    //setActivityData(dataList.get(i - 1));
                                } else {
                                    activity_id = "";
                                    activity_name = "";
                                }
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> adapterView) {

                            }
                        });

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    Utils.showSnackBar(error.getMessage(), parent);
                    Toast.makeText(ViewStudents.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }, this, params);
    }


    private void setCommonData() {
        txtSub.setVisibility(View.VISIBLE);
        txtTitle.setText(staffname+"");
        txtSub.setText(group_name+"\n("+activity_name+")");
    }


    private void setShowcaseView() {
        new TapTargetSequence(this)
                .targets(TapTarget.forView(dialog,"Search Button!","Tap the Search button to Search Student Activity List.")
                        .outerCircleColor(R.color.light)
                        .outerCircleAlpha(0.96f)
                        .targetCircleColor(R.color.white)
                        .titleTextSize(20)
                        .titleTextColor(R.color.white)
                        .descriptionTextSize(14)
                        .descriptionTextColor(R.color.black)
                        .textColor(R.color.black)
                        .textTypeface(Typeface.SANS_SERIF)
                        .dimColor(R.color.black)
                        .drawShadow(true)
                        .cancelable(false)
                        .tintTarget(true)
                        .transparentTarget(true)
                        .targetRadius(60)).listener(new TapTargetSequence.Listener() {
                    @RequiresApi(api = Build.VERSION_CODES.N)
                    @Override
                    public void onSequenceFinish() {
                        // Toast.makeText(getApplicationContext(),"Sequence Finished",Toast.LENGTH_SHORT).show();
                        savePrefData();
                        openDialog();
                    }

                    @Override
                    public void onSequenceStep(TapTarget lastTarget, boolean targetClicked) {
                        // Toast.makeText(getActivity(),"GREAT!",Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onSequenceCanceled(TapTarget lastTarget) {

                    }
                }).start();
    }

    private void savePrefData(){
        sharedPreferences=this.getSharedPreferences("boarding_pref_add_attend_search",MODE_PRIVATE);
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putBoolean("IsFirstTimeVisit_add_attend_search",true);
        editor.apply();
    }

    private boolean restorePrefData(){
        sharedPreferences = this.getSharedPreferences("boarding_pref_add_attend_search",MODE_PRIVATE);
        return sharedPreferences.getBoolean("IsFirstTimeVisit_add_attend_search",false);
    }

    @OnClick(R.id.imgBack)
    public void backFinish() {
        back.startAnimation(animation);
        finish();
    }

    @OnClick(R.id.root)
    public void cal_lyttttt(){

        Calendar cal = Calendar.getInstance(TimeZone.getDefault()); // Get current date
        DatePickerDialog datePicker = new DatePickerDialog(this,
                R.style.MyDatePickerDialogTheme, ondate,
                cal.get(Calendar.YEAR),
                cal.get(Calendar.MONTH),
                cal.get(Calendar.DAY_OF_MONTH));
        datePicker.getDatePicker();
        datePicker.setCancelable(false);
        datePicker.show();

    }

    DatePickerDialog.OnDateSetListener ondate = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
            calendar.set(year, monthOfYear, dayOfMonth);
            calendar.set(Calendar.HOUR_OF_DAY, 00);
            calendar.set(Calendar.MINUTE, 00);
            calendar.set(Calendar.SECOND, 00);
            Date setdate = calendar.getTime();
            SimpleDateFormat fmtOut = new SimpleDateFormat("dd MMM, yyyy");
            String dateString = fmtOut.format(setdate);
            today_date.setText(dateString);
            SimpleDateFormat dateFrmOut = new SimpleDateFormat("yyyy-MM-dd");
            from_date = dateFrmOut.format(setdate);
            // Toast.makeText(HomeWorkByDate.this,from_date,Toast.LENGTH_SHORT).show();
            if (apply.equalsIgnoreCase("Pending")){
                p_fetchhwlist(from_date,"pending");
            }else {
                p_fetchhwlist(from_date,"");
            }

        }
    };

    @OnClick(R.id.oneDay)
    public void oneDay() {
        apply="pending";
        img1.setVisibility(View.VISIBLE);
        img2.setVisibility(View.GONE);
        v_day.setBackgroundColor(Color.parseColor("#00000000"));
        p_day.setBackgroundColor(Color.parseColor("#66000000"));
        p_fetchhwlist(from_date,"pending");
    }

    @OnClick(R.id.multiDay)
    public void multiDay() {
        apply="verified";
        img2.setVisibility(View.VISIBLE);
        img1.setVisibility(View.GONE);
        p_day.setBackgroundColor(Color.parseColor("#00000000"));
        v_day.setBackgroundColor(Color.parseColor("#66000000"));
        p_fetchhwlist(from_date,"");
    }


    private void p_fetchhwlist(String from_date,String tag) {
        commonProgress.show();
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("es_groupsid", groupid);
        params.put("es_activityid", activity_id);
        params.put("staff_id", staffid);
        if (!from_date.equalsIgnoreCase("")){
            params.put("date", from_date);
        }else {
            params.put("date", "");
        }
        params.put("check", tag);

        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.GET_ACT_URL, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                mBottomSheetDialog.dismiss();
                commonProgress.dismiss();
                if (error == null) {
                    try {
                        if (p_list != null) {
                            p_list.clear();
                        }
                        JSONArray jsonArray = jsonObject.getJSONArray(Constants.GROUP_DATA);
                        p_list = ActivityStudentBean.parseClassArray(jsonArray);
                        if (p_list.size() > 0) {
                            p_mAdapter.setGatePassList(p_list);
                            //setanimation on adapter...
                            p_recyclerview.getAdapter().notifyDataSetChanged();
                            p_recyclerview.scheduleLayoutAnimation();
                            totalRecord.setText("Total Entries:- "+String.valueOf(p_list.size()));
                            //-----------end------------
                            txtNorecord.setVisibility(View.GONE);
                        } else {
                            totalRecord.setText("Total Entries:- 0");
                            p_mAdapter.setGatePassList(p_list);
                            p_mAdapter.notifyDataSetChanged();
                            txtNorecord.setVisibility(View.VISIBLE);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    totalRecord.setText("Total Entries:- 0");
                    txtNorecord.setVisibility(View.VISIBLE);
                    p_list.clear();
                    p_mAdapter.setGatePassList(p_list);
                    p_mAdapter.notifyDataSetChanged();
                    Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_SHORT).show();
                }
            }
        }, this, params);
    }

    @Override
    public void onApproveCallback(ActivityStudentBean homeworkbean) {

    }
}