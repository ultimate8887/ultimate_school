package com.ultimate.ultimatesmartschool.provider;

import androidx.core.content.FileProvider;

/**
 * Created by YohanSandun
 */

public class PickImageFileProvider extends FileProvider {
    // This empty class is for enable users to use multiple file providers
}
