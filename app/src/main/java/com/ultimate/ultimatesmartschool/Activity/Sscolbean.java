package com.ultimate.ultimatesmartschool.Activity;

import com.ultimate.ultimatesmartschool.Utility.DebugLog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Sscolbean {


    private String id;
    private String name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static ArrayList<Sscolbean> parseHWArray(JSONArray arrayObj) {
        ArrayList<Sscolbean> list = new ArrayList<Sscolbean>();
        try {

            for (int i = 0; i < arrayObj.length(); i++) {
                Sscolbean p = parseHWObject(arrayObj.getJSONObject(i));
                if (p != null) {
                    list.add(p);
                }
            }
        } catch (Exception e) {
            DebugLog.printLog("exp",e.getMessage());
            e.printStackTrace();
        }
        return list;
    }

    public static Sscolbean parseHWObject(JSONObject jsonObject) {
        Sscolbean casteObj = new Sscolbean();
        try {
            if (jsonObject.has("id")) {
                casteObj.setId(jsonObject.getString("id"));
            }

            if (jsonObject.has("name") && !jsonObject.getString("name").isEmpty() && !jsonObject.getString("name").equalsIgnoreCase("null")) {
                casteObj.setName(jsonObject.getString("name"));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return casteObj;
    }
}
