package com.ultimate.ultimatesmartschool.Activity;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.github.siyamed.shapeimageview.CircularImageView;

import com.ultimate.ultimatesmartschool.BeanModule.User;
import com.ultimate.ultimatesmartschool.ForCamera.IPickResult;
import com.ultimate.ultimatesmartschool.ForCamera.PickImageDialog;
import com.ultimate.ultimatesmartschool.ForCamera.PickResult;
import com.ultimate.ultimatesmartschool.ForCamera.PickSetup;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class UpdateProfileActivity extends AppCompatActivity implements IPickResult {

    private int selection;
    private Bitmap userBitmap=null, userBitmapProf=null;
    private int VERIFY_NUMBER = 1000;
    Animation animation;
    @BindView(R.id.circleimgrecepone)
    CircularImageView image;
    @BindView(R.id.nametxtone)
    TextView name;
    @BindView(R.id.classtxtone)
    TextView classname;
    @BindView(R.id.imgBackmsg)
    ImageView back;
    @BindView(R.id.edit_profile)
    ImageView edit_profile;
    @BindView(R.id.submit)
    TextView submit;
    String image_url="";

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_profile);
        ButterKnife.bind(this);

        animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.btn_blink_animation);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                back.startAnimation(animation);
                finish();
            }
        });
        userprofile();
        pick_img();
    }

    private void userprofile() {
        HashMap<String, String> params = new HashMap<>();
        params.put("user_id", User.getCurrentUser().getId());
        if (userBitmapProf != null) {
            String encoded = Utils.encodeToBase64(userBitmapProf, Bitmap.CompressFormat.JPEG, 90);
            encoded = String.format("data:image/jpeg;base64,%s", encoded);
            params.put("image", encoded);
        }else{
            params.put("image", "");
        }

        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.UPDATE_IMG, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error ) {
                ErpProgress.cancelProgressBar();
                if (error == null) {
                    try {
                        Log.i("USERDATA", jsonObject.getJSONObject(Constants.USERDATA).getString("profile"));
                        image_url=jsonObject.getJSONObject(Constants.USERDATA).getString("profile");
                        Utils.setNaviHeaderDatas(name,classname,image,image_url,UpdateProfileActivity.this);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(UpdateProfileActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }, this, params);
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    @OnClick(R.id.edit_profile)
    public void edit_profile() {
        animation = AnimationUtils.loadAnimation(this, R.anim.btn_blink_animation);
        edit_profile.startAnimation(animation);
        pick_img();
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @SuppressLint("ResourceType")
    private void pick_img() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.UPSIDE_DOWN_CAKE) { // Android 14+ (API 34)
           onImageViewClick();

        } else if (Build.VERSION.SDK_INT > Build.VERSION_CODES.S) {
                if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_MEDIA_IMAGES)
                        != PackageManager.PERMISSION_GRANTED) {

                    if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_MEDIA_IMAGES)) {
                        Toast.makeText(this, "Please provide storage permission from app settings", Toast.LENGTH_LONG).show();
                    } else {

                        ActivityCompat.requestPermissions(this,
                                new String[]{Manifest.permission.READ_MEDIA_IMAGES},
                                1);
                    }
                } else {

                    onImageViewClick();
                }

            } else {
                if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {

                    if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                        Toast.makeText(this, "Please provide storage permission from app settings", Toast.LENGTH_LONG).show();
                    } else {

                        ActivityCompat.requestPermissions(this,
                                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                                1);
                    }
                } else {
                    onImageViewClick();

                }
        }
    }

    private void onImageViewClick() {
        //  type = 1;
        PickSetup setup = new PickSetup();
        // super.customize(setup);

        PickImageDialog.build(setup)
                //.setOnClick(this)
                .show(this);
    }

    @Override
    public void onPickResult(PickResult r) {
        if (r.getError() == null) {
            //If you want the Bitmap.
            image.setImageBitmap(r.getBitmap());
            userBitmapProf= r.getBitmap();
            submit.setVisibility(View.VISIBLE);
        } else {
            //Handle possible errors
            //TODO: do what you have to do with r.getError();
            Toast.makeText(this, r.getError().getMessage(), Toast.LENGTH_LONG).show();
            submit.setVisibility(View.GONE);
        }


    }
    @OnClick(R.id.submit)
    public void submitt() {
        submit.startAnimation(animation);
        ErpProgress.showProgressBar(UpdateProfileActivity.this,"Please Wait........!");
        HashMap<String, String> params = new HashMap<>();
        params.put("user_id", User.getCurrentUser().getId());
        if (userBitmapProf != null) {
            String encoded = Utils.encodeToBase64(userBitmapProf, Bitmap.CompressFormat.JPEG, 90);
            encoded = String.format("data:image/jpeg;base64,%s", encoded);
            params.put("image", encoded);
        }else{
            params.put("image", "");
        }

        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.UPDATE_IMG, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error ) {
                ErpProgress.cancelProgressBar();
                if (error == null) {
                    try {
                        Log.i("USERDATA", jsonObject.getJSONObject(Constants.USERDATA).getString("profile"));

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Toast.makeText(UpdateProfileActivity.this, "Profile Updated Successfully", Toast.LENGTH_SHORT).show();
                    //   Log.i("USERDATA", User.getCurrentUser().getAadhar_image());
                    finish();
                } else {
                    Toast.makeText(UpdateProfileActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }, this, params);
    }
}