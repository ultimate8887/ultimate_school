package com.ultimate.ultimatesmartschool.Activity;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.daimajia.swipe.SimpleSwipeListener;
import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.squareup.picasso.Picasso;
import com.ultimate.ultimatesmartschool.BeanModule.Studentbean;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Student.Studentadapter;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class UserAdapter extends RecyclerSwipeAdapter<UserAdapter.Viewholder> implements Filterable {
    public ArrayList<User_Permission> stulist;
    private ArrayList<User_Permission> filterModelClass;
    Context mContext;
    StudentPro mStudentPro;
    public UserAdapter(ArrayList<User_Permission> stulist, Context mContext, StudentPro mStudentPro) {
        this.mContext = mContext;
        this.mStudentPro=mStudentPro;
        this.stulist = stulist;
        this.filterModelClass = stulist;
    }

    @Override
    public UserAdapter.Viewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.user_new_item_news, parent, false);
        UserAdapter.Viewholder viewholder = new UserAdapter.Viewholder(view);
        return viewholder;
    }

    @Override
    public void onBindViewHolder(final UserAdapter.Viewholder holder, @SuppressLint("RecyclerView") final int position) {

        holder.image.setAnimation(AnimationUtils.loadAnimation(mContext,R.anim.fade_transition_animation));
        holder.parent.setAnimation(AnimationUtils.loadAnimation(mContext,R.anim.fade_scale_animation));

        User_Permission mData=stulist.get(position);

        if (mData.getId() != null) {
            String title = getColoredSpanned("Reg.No: ", "#000000");
            String Name = getColoredSpanned(mData.getId(), "#5A5C59");
            holder.employee_id.setText(Html.fromHtml(title + " " + Name));
        }

        // holder.employee_name.setText("Name: " + " " + viewstafflist.get(position).getName());
        if (mData.getLastname() != null) {
            String title = getColoredSpanned("Name: ", "#000000");
            String Name = getColoredSpanned(mData.getFirstname()+" "+mData.getLastname(), "#5A5C59");
            holder.employee_name.setText(Html.fromHtml(title + " " + Name));
        }else {
            String title = getColoredSpanned("Name: ", "#000000");
            String Name = getColoredSpanned(mData.getFirstname(), "#5A5C59");
            holder.employee_name.setText(Html.fromHtml(title + " " + Name));
        }

        //  holder.employee_designation.setText("Designation: " + " " + viewstafflist.get(position).getDesignation());
        if (mData.getType() != null) {
            String title = getColoredSpanned("", "#000000");
            String Name = getColoredSpanned(mData.getType(), "#5A5C59");
            holder.employee_designation.setText(Html.fromHtml(title + " " + Name));
        }else {
            holder.employee_designation.setText("Not Mentioned");
        }

        //  holder.post.setText("Post:" + " " + viewstafflist.get(position).getPost());
        if (mData.getAddress() != null) {
            String title = getColoredSpanned("Address: ", "#000000");
            String Name = getColoredSpanned(mData.getAddress(), "#5A5C59");
            holder.post.setText(Html.fromHtml(title + " " + Name));
        }else {
            holder.post.setText("Address: Not Mentioned");
        }


//        if (mData.getDob() != null) {
//            String title = getColoredSpanned("Date of Birth: ", "#000000");
//            String Name = getColoredSpanned(Utils.getDateFormated(mData.getDob()), "#5A5C59");
//            holder.employee_dob.setText(Html.fromHtml(title + " " + Name));
//        }else {
//            holder.employee_dob.setText("DOB: Not Mentioned");
//        }

//
//        if (mData.getSection_name() != null) {
//            String title = getColoredSpanned("Class: ", "#000000");
//            String Name = getColoredSpanned(mData.getClass_name()+"("+stulist.get(position).getSection_name()+")", "#5A5C59");
//            holder.employee_doj.setText(Html.fromHtml(title + " " + Name));
//        }else if (mData.getClass_name() != null) {
//            String title = getColoredSpanned("Class: ", "#000000");
//            String Name = getColoredSpanned(mData.getClass_name(), "#5A5C59");
//            holder.employee_doj.setText(Html.fromHtml(title + " " + Name));
//        }
//        else {
//            holder.employee_doj.setText("Class: Not Mentioned");
//        }


        if (mData.getPhoneno() != null) {
            String title = getColoredSpanned("Mobile: ", "#000000");
            String Name = getColoredSpanned("</u>" +mData.getPhoneno()+"</u>", "#5A5C59");
            holder.phone.setText(Html.fromHtml(title + " " + Name));
        }else {
            holder.phone.setText("Mobile: Not Mentioned");
        }

        if ( mData.getEmail() != null) {

            String title = getColoredSpanned("Email: ", "#000000");
            String Name = getColoredSpanned("</u>" +mData.getEmail()+"</u>", "#5A5C59");
            holder.email.setText(Html.fromHtml(title + " " + Name));
        }else {
            holder.email.setText("Email: Not Mentioned");
        }

        if (mData.getMobile_name() != null) {
            holder.login_device.setText(mData.getMobile_brand()+"-"+mData.getMobile_name());
        }else {
            holder.login_device.setText("Not Detect");
        }

        holder.phoneLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mData.getPhoneno() != null) {
                    mStudentPro.stdCallBack(stulist.get(position));
                }else {
                    Toast.makeText(mContext,"Phone Number Not Found!",Toast.LENGTH_SHORT).show();
                }
            }
        });


        holder.googleLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (mData.getPhoneno() != null) {
                    mStudentPro.stdEmailCallBack(stulist.get(position));
                }else {
                    Toast.makeText(mContext,"Number Not Found!",Toast.LENGTH_SHORT).show();
                }

            }
        });



            if (stulist.get(position).getProfile() != null) {
                //  Picasso.with(context).load(viewstafflist.get(position).getImage()).placeholder(context.getResources().getDrawable(R.drawable.man)).into(holder.image);
                Utils.progressImg2(stulist.get(position).getProfile(),holder.image,mContext);
            } else {
                // Picasso.with(context).load(String.valueOf(context.getResources().getDrawable(R.drawable.staffimg))).into(holder.image);
                Picasso.get().load(R.drawable.boy).into(holder.image);

            }


        mItemManger.bindView(holder.itemView, position);
        holder.trash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                AlertDialog.Builder builder1 = new AlertDialog.Builder(mContext);
                builder1.setMessage("Do you want to delete? ");
                builder1.setCancelable(false);
                builder1.setPositiveButton(
                        "Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                                stulist.remove(position);
                                notifyItemRemoved(position);
                                notifyItemRangeChanged(position, stulist.size());
                                mItemManger.closeAllItems();
                                Toast.makeText(view.getContext(), "Deleted " + holder.employee_name.getText().toString() + "!", Toast.LENGTH_SHORT).show();
                            }
                        });
                builder1.setNegativeButton(
                        "No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        });
                AlertDialog alert11 = builder1.create();
                alert11.show();

            }
        });


        holder.update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                Toast.makeText(view.getContext(), "clicked " + holder.employee_name.getText().toString() + "!", Toast.LENGTH_SHORT).show();

                AlertDialog.Builder builder2 = new AlertDialog.Builder(mContext);
                builder2.setMessage("Do you want to Block? ");
                builder2.setCancelable(false);
                builder2.setPositiveButton(
                        "Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                if (mStudentPro != null) {
                                    mStudentPro.onUpdateCallback(stulist.get(position));

                                }
                                stulist.remove(position);
                                notifyItemRemoved(position);
                                notifyItemRangeChanged(position, stulist.size());
                                mItemManger.closeAllItems();
                                Toast.makeText(view.getContext(), "Blocked " + holder.employee_name.getText().toString() + "!", Toast.LENGTH_SHORT).show();
                            }
                        });
                builder2.setNegativeButton(
                        "No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        });
                AlertDialog alert11 = builder2.create();
                alert11.show();

            }
        });

        holder.swipeLayout.setShowMode(SwipeLayout.ShowMode.PullOut);
        holder.swipeLayout.addSwipeListener(new SimpleSwipeListener() {
            @Override
            public void onOpen(SwipeLayout layout) {

            }

            @Override
            public void onStartClose(SwipeLayout layout) {
                holder.swipeLayout.close();
            }
        });

        holder.swipeLayout.setOnDoubleClickListener(new SwipeLayout.DoubleClickListener() {
            @Override
            public void onDoubleClick(SwipeLayout layout, boolean surface) {
                Toast.makeText(mContext, "DoubleClick", Toast.LENGTH_SHORT).show();
            }
        });


        // holder.txtid.setText("ID:"+stulist.get(position).getId());

        holder.parent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mStudentPro!= null){
                    mStudentPro.StudentProfile(stulist.get(position));
                }
            }
        });



    }

    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }

    public interface StudentPro{
        void StudentProfile(User_Permission std);
        void onUpdateCallback(User_Permission std);
        void stdCallBack(User_Permission std);
        void stdEmailCallBack(User_Permission std);
    }
    @Override
    public int getItemCount() {
        if (stulist != null)
            return stulist.size();
        return 0;
    }



    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe;
    }

    public void setHQList(ArrayList<User_Permission> stulist) {
        this.stulist = stulist;
    }

    public class Viewholder extends RecyclerView.ViewHolder {

        @BindView(R.id.swipe)
        SwipeLayout swipeLayout;
        @BindView(R.id.id)
        TextView employee_id;
        @BindView(R.id.txtstuName)
        EditText employee_name;
        @BindView(R.id.txtDT)
        TextView employee_designation;
        @BindView(R.id.txtdob)
        TextView employee_dob;
        @BindView(R.id.txtdoj)
        TextView employee_doj;
        @BindView(R.id.txtReason)
        EditText login_device;
        @BindView(R.id.txtFatherName)
        TextView post;
        @BindView(R.id.profile)
        CircularImageView image;
        @BindView(R.id.txtPhone)
        TextView phone;
        @BindView(R.id.googleLogin)
        FloatingActionButton googleLogin;
        @BindView(R.id.phoneLogin)
        FloatingActionButton phoneLogin;
        //        @BindView(R.id.fphone)
//        TextView fphone;
//        @BindView(R.id.ophone)
//        TextView ophone;
        @BindView(R.id.email)
        TextView email;
        @BindView(R.id.parent)
        RelativeLayout parent;

        @BindView(R.id.trash)
        ImageView trash;
        @BindView(R.id.update)
        ImageView update;

        public Viewholder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            parent =(RelativeLayout)itemView.findViewById(R.id.parent);

//            circleimg=(CircularImageView)itemView.findViewById(R.id.circleimg);
//
//            textViewData = (TextView) itemView.findViewById(R.id.name);
//            txtFather = (TextView) itemView.findViewById(R.id.fathername);
//            txtMobile = (TextView) itemView.findViewById(R.id.mobileno);
//            txtClass = (TextView) itemView.findViewById(R.id.classess);
//            // textViewPos = (TextView) itemView.findViewById(R.id.position);
//
//            txtid=(TextView)itemView.findViewById(R.id.id);
        }
    }

    @Override
    public Filter getFilter() {

        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {

                String Key = constraint.toString();
                if (Key.isEmpty()) {

                    filterModelClass = stulist ;

                }
                else {
                    ArrayList<User_Permission> lstFiltered = new ArrayList<>();
                    for (User_Permission row : stulist) {

                        if (row.getFirstname().toLowerCase().contains(Key.toLowerCase())||row.getId().toLowerCase().contains(Key.toLowerCase())){
                            lstFiltered.add(row);
                        }

                    }

                    filterModelClass = lstFiltered;

                }


                FilterResults filterResults = new FilterResults();
                filterResults.values= filterModelClass;
                return filterResults;

            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {


                stulist = (ArrayList<User_Permission>) results.values;
                notifyDataSetChanged();

            }
        };




    }
}