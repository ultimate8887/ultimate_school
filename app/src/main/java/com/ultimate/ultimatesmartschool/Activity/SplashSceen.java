package com.ultimate.ultimatesmartschool.Activity;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.airbnb.lottie.LottieAnimationView;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.squareup.picasso.Picasso;
import com.ultimate.ultimatesmartschool.BeanModule.User;
import com.ultimate.ultimatesmartschool.ForCamera.CameraTesting;
import com.ultimate.ultimatesmartschool.Login.LoginActivity;
import com.ultimate.ultimatesmartschool.MainActivity;
import com.ultimate.ultimatesmartschool.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SplashSceen extends AppCompatActivity {
    @BindView(R.id.logo)
    ImageView logo;
//    @BindView(R.id.title)
//    TextView title;
//    boolean link = true;
    private Animation animation;
//    private long SPLASH_TIME_OUT = 3000;
//    ArrayList<Sscolbean> hwList = new ArrayList<>();
//    @BindView(R.id.logo2)ImageView logo2;
    @BindView(R.id.logo3)ImageView logo3;

    @BindView(R.id.play)
    LottieAnimationView play;
    Window window;
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_splash_sceen);
        ButterKnife.bind(this);

        window=this.getWindow();
        window.setStatusBarColor(this.getResources().getColor(R.color.transparent));

        animation = AnimationUtils.loadAnimation(this, R.anim.logo_animation);
        logo.startAnimation(animation);
        Animation top_curve_anim = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.top_down);
//        logo2.startAnimation(top_curve_anim);
        animation = AnimationUtils.loadAnimation(this,R.anim.enter_from_right_animation);
        Animation center_reveal_anim = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.center_reveal_anim);
        //play.startAnimation(center_reveal_anim);
//        title.startAnimation(animation);

        User currentUser = User.getCurrentUser();
        if (currentUser != null && currentUser.getId() != null && currentUser.getSchoolData() != null) {
            Picasso.get().load(currentUser.getSchoolData().getLogo()).into(logo);
        }

        Thread splashTread = new Thread() {
            @SuppressLint("SuspiciousIndentation")
            @Override
            public void run() {
                try {
                    int waited = 0;
                    // Splash screen pause time
                    while (waited < 2000) {
                        sleep(50);
                        waited += 50;
                    }
                    Intent intent = null;

                 //   User currentUser = User.getCurrentUser();
                   // Log.i("Debug_user_found_bahr", "User is null or User ID is null"+ currentUser.getId());
                    if (currentUser != null) {
                       // Log.i("Debug_user_hai", "Debug_user_hai"+ User.getCurrentUser().toString());
                        if (currentUser.getId() != null) {
                         //   Log.i("Debug_user_id_hai", "Debug_user_id_hai"+ currentUser.getId());
                            intent = new Intent(SplashSceen.this,
                                    MainActivity.class);
                        }else {
                            currentUser.logout();
                            intent = new Intent(SplashSceen.this,
                                    LoginActivity.class);
                            // Handle the null case
                         //   Log.e("Debug_user_id_nhi_hai", "User is null or User ID is null");

                        }
                    }else {
                     intent = new Intent(SplashSceen.this,
                            LoginActivity.class);
                        // Handle the null case
                     //   Log.e("Debug_hai hi nhi", "User is null or User ID is null");

                    }

                    intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    startActivity(intent);
                    SplashSceen.this.finish();

                } catch (InterruptedException e) {
                    // do nothing
                } finally {
                    SplashSceen.this.finish();
                }
            }

        };
        splashTread.start();
    }


}
