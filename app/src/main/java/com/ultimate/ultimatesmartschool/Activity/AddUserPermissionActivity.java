package com.ultimate.ultimatesmartschool.Activity;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.github.siyamed.shapeimageview.CircularImageView;

import com.ultimate.ultimatesmartschool.BeanModule.User;
import com.ultimate.ultimatesmartschool.Fee.PayFeeSActivity;
import com.ultimate.ultimatesmartschool.ForCamera.IPickResult;
import com.ultimate.ultimatesmartschool.ForCamera.PickImageDialog;
import com.ultimate.ultimatesmartschool.ForCamera.PickResult;
import com.ultimate.ultimatesmartschool.ForCamera.PickSetup;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.CommonProgress;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import org.json.JSONObject;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AddUserPermissionActivity extends AppCompatActivity implements IPickResult {

    @BindView(R.id.toMeet)
    EditText email;

    @BindView(R.id.vistname)
    EditText name;


    @BindView(R.id.checkIn)
    Button checkIn;

    private Bitmap userBitmap;
    @BindView(R.id.clickimage)ImageView clickimage;

    @BindView(R.id.visitimage)
    CircularImageView chooseimage;

    @BindView(R.id.vistphoneno)
    EditText phonenumber;

    @BindView(R.id.lastname)
    EditText lastname;

    @BindView(R.id.visitaddress)
    EditText adress;


    @BindView(R.id.visit_add)
    TextView visit_add;
    @BindView(R.id.visit_mini)
    TextView visit_mini;


    @BindView(R.id.p_add1)
    TextView p_add1;
    @BindView(R.id.p_mini1)
    TextView p_mini1;
    @BindView(R.id.stopppp1)
    LinearLayout stopppp1;


    @BindView(R.id.p_add2)
    TextView p_add2;
    @BindView(R.id.p_mini2)
    TextView p_mini2;
    @BindView(R.id.stopppp2)
    LinearLayout stopppp2;


    @BindView(R.id.p_add)
    TextView p_add;
    @BindView(R.id.p_mini)
    TextView p_mini;
    @BindView(R.id.stopppp)
    LinearLayout stopppp;

    @BindView(R.id.v_add)
    TextView v_add;
    @BindView(R.id.v_mini)
    TextView v_mini;
    
    @BindView(R.id.topppp)
    LinearLayout topppp;

    @BindView(R.id.extra_deails)
    LinearLayout extra_deails;

    @BindView(R.id.edtVno)
    EditText username;

    @BindView(R.id.edtColor)
    EditText password;
    @BindView(R.id.txtTitle)
    TextView txtTitle;

    String setup="no",thought="no",banner="no",notice="no",gallery="no",transport="no",staff="no",
            incharge="no",subject="no",staffGateAttend="no",staffAttend="no",saffLeave="no",staffGatePass="no"
            ,idcard="no",library="no",hrm="no",certificate="no"
            ,hostel="no",payroll="no",account="no";

    String enquiry="no",holiday="no",social_post="no",school_activity="no",expense="no",visitor="no",feedback="no";
    String performance="no",planner="no",health="no";

    String student="no",form="no",timetable="no",homework="no",classwork="no",assign="no",exam="no",classtest="no",
            datesheet="no",syllabus="no",stdGateAttend="no",stdAttend="no",stdLeave="no",stdGatePass="no"
            ,fee="no",ebook="no",elearning="no",sea="no";




    @BindView(R.id.icon)
    ImageView icon;
    @BindView(R.id.icons)
    ImageView icons;
    @BindView(R.id.radioGroupSetup)
    RadioGroup radioGroupSetup;

    @BindView(R.id.icon1)
    ImageView icon1;
    @BindView(R.id.icons1)
    ImageView icons1;
    @BindView(R.id.radioGroupThought)
    RadioGroup radioGroupThought;

    @BindView(R.id.icon3)
    ImageView icon3;
    @BindView(R.id.icons3)
    ImageView icons3;
    @BindView(R.id.radioGroupBanners)
    RadioGroup radioGroupBanners;

    @BindView(R.id.icon4)
    ImageView icon4;
    @BindView(R.id.icons4)
    ImageView icons4;
    @BindView(R.id.radioGroupNotice)
    RadioGroup radioGroupNotice;

    @BindView(R.id.icon5)
    ImageView icon5;
    @BindView(R.id.icons5)
    ImageView icons5;
    @BindView(R.id.radioGroupGallery)
    RadioGroup radioGroupGallery;

    @BindView(R.id.icon6)
    ImageView icon6;
    @BindView(R.id.icons6)
    ImageView icons6;
    @BindView(R.id.radioGroupTransport)
    RadioGroup radioGroupTransport;



    @BindView(R.id.staff_icon)
    ImageView staff_icon;
    @BindView(R.id.staff_icons)
    ImageView staff_icons;
    @BindView(R.id.radioGroupStaff)
    RadioGroup radioGroupStaff;

    @BindView(R.id.staff_icon1)
    ImageView staff_icon1;
    @BindView(R.id.staff_icons1)
    ImageView staff_icons1;
    @BindView(R.id.radioGroupIncharge)
    RadioGroup radioGroupIncharge;

    @BindView(R.id.staff_icon2)
    ImageView staff_icon2;
    @BindView(R.id.staff_icons2)
    ImageView staff_icons2;
    @BindView(R.id.radioGroupSubject)
    RadioGroup radioGroupSubject;

    @BindView(R.id.staff_icon3)
    ImageView staff_icon3;
    @BindView(R.id.staff_icons3)
    ImageView staff_icons3;
    @BindView(R.id.radioGroupGate)
    RadioGroup radioGroupGate;

    @BindView(R.id.staff_icon4)
    ImageView staff_icon4;
    @BindView(R.id.staff_icons4)
    ImageView staff_icons4;
    @BindView(R.id.radioGroupStaffAttendance)
    RadioGroup radioGroupStaffAttendance;

    @BindView(R.id.staff_icon5)
    ImageView staff_icon5;
    @BindView(R.id.staff_icons5)
    ImageView staff_icons5;
    @BindView(R.id.radioGroupLeave)
    RadioGroup radioGroupLeave;

    @BindView(R.id.staff_icon6)
    ImageView staff_icon6;
    @BindView(R.id.staff_icons6)
    ImageView staff_icons6;
    @BindView(R.id.radioGroupGatePass)
    RadioGroup radioGroupGatePass;



    @BindView(R.id.std_icon)
    ImageView std_icon;
    @BindView(R.id.std_icons)
    ImageView std_icons;
    @BindView(R.id.radioGroupStd)
    RadioGroup radioGroupStd;

    @BindView(R.id.std_icon1)
    ImageView std_icon1;
    @BindView(R.id.std_icons1)
    ImageView std_icons1;
    @BindView(R.id.radioGroupAdmission)
    RadioGroup radioGroupAdmission;

    @BindView(R.id.std_icon2)
    ImageView std_icon2;
    @BindView(R.id.std_icons2)
    ImageView std_icons2;
    @BindView(R.id.radioGroupStdGateAttend)
    RadioGroup radioGroupStdGateAttend;

    @BindView(R.id.std_icon3)
    ImageView std_icon3;
    @BindView(R.id.std_icons3)
    ImageView std_icons3;
    @BindView(R.id.radioGroupStdAttendance)
    RadioGroup radioGroupStdAttendance;

    @BindView(R.id.std_icon4)
    ImageView std_icon4;
    @BindView(R.id.std_icons4)
    ImageView std_icons4;
    @BindView(R.id.radioGroupStdTimetable)
    RadioGroup radioGroupStdTimetable;

    @BindView(R.id.std_icon5)
    ImageView std_icon5;
    @BindView(R.id.std_icons5)
    ImageView std_icons5;
    @BindView(R.id.radioGroupStdHomework)
    RadioGroup radioGroupStdHomework;

    @BindView(R.id.std_icon6)
    ImageView std_icon6;
    @BindView(R.id.std_icons6)
    ImageView std_icons6;
    @BindView(R.id.radioGroupStdClasswork)
    RadioGroup radioGroupStdClasswork;

    @BindView(R.id.std_icon7)
    ImageView std_icon7;
    @BindView(R.id.std_icons7)
    ImageView std_icons7;
    @BindView(R.id.radioGroupStdAssignment)
    RadioGroup radioGroupStdAssignment;

    @BindView(R.id.std_icon8)
    ImageView std_icon8;
    @BindView(R.id.std_icons8)
    ImageView std_icons8;
    @BindView(R.id.radioGroupStdExamination)
    RadioGroup radioGroupStdExamination;

    @BindView(R.id.std_icon9)
    ImageView std_icon9;
    @BindView(R.id.std_icons9)
    ImageView std_icons9;
    @BindView(R.id.radioGroupStdTest)
    RadioGroup radioGroupStdTest;

    @BindView(R.id.std_icon10)
    ImageView std_icon10;
    @BindView(R.id.std_icons10)
    ImageView std_icons10;
    @BindView(R.id.radioGroupStdDatesheet)
    RadioGroup radioGroupStdDatesheet;

    @BindView(R.id.std_icon11)
    ImageView std_icon11;
    @BindView(R.id.std_icons11)
    ImageView std_icons11;
    @BindView(R.id.radioGroupStdLeave)
    RadioGroup radioGroupStdLeave;

    @BindView(R.id.std_icon12)
    ImageView std_icon12;
    @BindView(R.id.std_icons12)
    ImageView std_icons12;
    @BindView(R.id.radioGroupStdSyllabus)
    RadioGroup radioGroupStdSyllabus;

    @BindView(R.id.std_icon13)
    ImageView std_icon13;
    @BindView(R.id.std_icons13)
    ImageView std_icons13;
    @BindView(R.id.radioGroupStdFee)
    RadioGroup radioGroupStdFee;

    @BindView(R.id.std_icon14)
    ImageView std_icon14;
    @BindView(R.id.std_icons14)
    ImageView std_icons14;
    @BindView(R.id.radioGroupStdGatePass)
    RadioGroup radioGroupStdGatePass;

    @BindView(R.id.std_icon15)
    ImageView std_icon15;
    @BindView(R.id.std_icons15)
    ImageView std_icons15;
    @BindView(R.id.radioGroupStdEbook)
    RadioGroup radioGroupStdEbook;

    @BindView(R.id.std_icon16)
    ImageView std_icon16;
    @BindView(R.id.std_icons16)
    ImageView std_icons16;
    @BindView(R.id.radioGroupStdELearning)
    RadioGroup radioGroupStdELearning;

    @BindView(R.id.std_icon161)
    ImageView std_icon161;
    @BindView(R.id.std_icons161)
    ImageView std_icons161;
    @BindView(R.id.radioGroupStdPerformance)
    RadioGroup radioGroupStdPerformance;

    @BindView(R.id.std_icon162)
    ImageView std_icon162;
    @BindView(R.id.std_icons162)
    ImageView std_icons162;
    @BindView(R.id.radioGroupStdEPlanner)
    RadioGroup radioGroupStdEPlanner;

    @BindView(R.id.std_icon163)
    ImageView std_icon163;
    @BindView(R.id.std_icons163)
    ImageView std_icons163;
    @BindView(R.id.radioGroupStdHealth)
    RadioGroup radioGroupStdHealth;


    @BindView(R.id.std_icon17)
    ImageView std_icon17;
    @BindView(R.id.std_icons17)
    ImageView std_icons17;
    @BindView(R.id.radioGroupIdCard)
    RadioGroup radioGroupIdCard;

    @BindView(R.id.std_icon18)
    ImageView std_icon18;
    @BindView(R.id.std_icons18)
    ImageView std_icons18;
    @BindView(R.id.radioGroupLibrary)
    RadioGroup radioGroupLibrary;

    @BindView(R.id.std_icon19)
    ImageView std_icon19;
    @BindView(R.id.std_icons19)
    ImageView std_icons19;
    @BindView(R.id.radioGroupHrm)
    RadioGroup radioGroupHrm;

    @BindView(R.id.std_icon20)
    ImageView std_icon20;
    @BindView(R.id.std_icons20)
    ImageView std_icons20;
    @BindView(R.id.radioGroupCertificate)
    RadioGroup radioGroupCertificate;

    @BindView(R.id.std_icon21)
    ImageView std_icon21;
    @BindView(R.id.std_icons21)
    ImageView std_icons21;
    @BindView(R.id.radioGroupHostel)
    RadioGroup radioGroupHostel;

    @BindView(R.id.std_icon22)
    ImageView std_icon22;
    @BindView(R.id.std_icons22)
    ImageView std_icons22;
    @BindView(R.id.radioGroupPayroll)
    RadioGroup radioGroupPayroll;

    @BindView(R.id.std_icon23)
    ImageView std_icon23;
    @BindView(R.id.std_icons23)
    ImageView std_icons23;
    @BindView(R.id.radioGroupAccounts)
    RadioGroup radioGroupAccounts;
    CommonProgress commonProgress;


    //
    @BindView(R.id.std_icon231)
    ImageView std_icon231;
    @BindView(R.id.std_icons231)
    ImageView std_icons231;
    @BindView(R.id.radioGroupEnquiry)
    RadioGroup radioGroupEnquiry;


    @BindView(R.id.std_icon232)
    ImageView std_icon232;
    @BindView(R.id.std_icons232)
    ImageView std_icons232;
    @BindView(R.id.radioGroupHoliday)
    RadioGroup radioGroupHoliday;


    @BindView(R.id.std_icon233)
    ImageView std_icon233;
    @BindView(R.id.std_icons233)
    ImageView std_icons233;
    @BindView(R.id.radioGroupSocial)
    RadioGroup radioGroupSocial;


    @BindView(R.id.std_icon234)
    ImageView std_icon234;
    @BindView(R.id.std_icons234)
    ImageView std_icons234;
    @BindView(R.id.radioGroupschool_activty)
    RadioGroup radioGroupschool_activty;


    @BindView(R.id.std_icon235)
    ImageView std_icon235;
    @BindView(R.id.std_icons235)
    ImageView std_icons235;
    @BindView(R.id.radioGroupFuel)
    RadioGroup radioGroupFuel;


    @BindView(R.id.std_icon236)
    ImageView std_icon236;
    @BindView(R.id.std_icons236)
    ImageView std_icons236;
    @BindView(R.id.radioGroupVisitor)
    RadioGroup radioGroupVisitor;


    @BindView(R.id.std_icon237)
    ImageView std_icon237;
    @BindView(R.id.std_icons237)
    ImageView std_icons237;
    @BindView(R.id.radioGroupFeedback)
    RadioGroup radioGroupFeedback;


    @BindView(R.id.std_icon164)
    ImageView std_icon164;
    @BindView(R.id.std_icons164)
    ImageView std_icons164;
    @BindView(R.id.radioGroupStdESEA)
    RadioGroup radioGroupStdESEA;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_user_permission);
        ButterKnife.bind(this);
        txtTitle.setText("Add User Permission");

        commonProgress = new CommonProgress(this);
        schoolRadio();
        staffRadio();
        stdRadio();

        checkIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendcheckinrequest();
            }
        });

    }


    private void sendcheckinrequest() {

        if (name.getText().toString().isEmpty() || adress.getText().toString().isEmpty() || lastname.getText().toString().isEmpty()
                || phonenumber.getText().toString().isEmpty() || email.getText().toString().isEmpty() ) {
            Toast.makeText(AddUserPermissionActivity.this, "Kindly fill complete User details", Toast.LENGTH_LONG).show();
        } else if (username.getText().toString().isEmpty() || password.getText().toString().isEmpty()) {
            Toast.makeText(getApplicationContext(), "Kindly Set Username/Password", Toast.LENGTH_LONG).show();
        }else {
            HashMap<String, String> params = new HashMap<String, String>();
            commonProgress.show();
            params.put("admin_fname", name.getText().toString());
            params.put("admin_lname", lastname.getText().toString());
            params.put("admin_phoneno",phonenumber.getText().toString() );
            params.put("address",adress.getText().toString() );
            params.put("admin_email",email.getText().toString());

            params.put("admin_username", username.getText().toString());
            params.put("admin_password", password.getText().toString());
            if (userBitmap != null) {
                String encoded = Utils.encodeToBase64(userBitmap, Bitmap.CompressFormat.JPEG, 90);
                encoded = String.format("data:image/jpeg;base64,%s", encoded);
                params.put("image", encoded);
            }else {
                params.put("image", "");
            }


            params.put("s_key", User.getCurrentUser().getSchoolData().getFi_school_id());
            params.put("datesheet", datesheet);
            params.put("std_gatepass", stdGatePass);
            params.put("setup", setup);
            params.put("admission_form", form);
            params.put("feepay", fee);
            params.put("stud_attend", stdAttend);

            params.put("staff", staff);
            params.put("student", student);

            params.put("staff_attend", staffAttend);
            params.put("staff_gate_attend", staffGateAttend);
            params.put("std_gate_attend", stdGateAttend);
            params.put("examination", exam);
            params.put("syllabus", syllabus);
            params.put("timetable", timetable);

            params.put("assignment", assign);
            params.put("homework", homework);
            params.put("classwork", classwork);
            params.put("notice", notice);
            params.put("thought", thought);
            params.put("photo", gallery);

            params.put("ebook", ebook);
            params.put("transport", transport);
            params.put("setup", setup);
            params.put("banner", banner);
            params.put("assign_subject", subject);
            params.put("assign_incharge", incharge);

            params.put("staff_leave", saffLeave);
            params.put("std_leave", stdLeave);
            params.put("elearning", elearning);
            params.put("gatepass", staffGatePass);
            params.put("check", "add");

            params.put("classtest", classtest);

            params.put("idcard", idcard);
            params.put("library", library);
            params.put("hrm", hrm);
            params.put("certificate", certificate);
            params.put("hostel", hostel);
            params.put("payroll", payroll);
            params.put("accounts", account);

            params.put("es_enquiry", enquiry);
            params.put("es_holiday", holiday);
            params.put("es_social_post", social_post);
            params.put("es_school_activity", school_activity);
            params.put("es_expense", expense);
            params.put("es_visitor", visitor);
            params.put("es_feedback", feedback);
            params.put("es_performance", performance);
            params.put("es_planner", planner);
            params.put("es_health", health);
            params.put("es_sea", sea);

            ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.ADD_USER, checkinclassapiCallback, this, params);
        }
    }
    ApiHandler.ApiCallback checkinclassapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            commonProgress.dismiss();
            if (error == null) {
                Toast.makeText(AddUserPermissionActivity.this,"User Created Successfully!",Toast.LENGTH_LONG).show();
                finish();
            }
            else {
                Toast.makeText(AddUserPermissionActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();

            }
        }
    };

    @RequiresApi(api = Build.VERSION_CODES.M)
    @OnClick(R.id.clickimage)
    public void click() {
       // type = 1;

        pick_img();
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @SuppressLint("ResourceType")
    private void pick_img() {

            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.S) {
                if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_MEDIA_IMAGES)
                        != PackageManager.PERMISSION_GRANTED) {

                    if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_MEDIA_IMAGES)) {
                        Toast.makeText(this, "Please provide storage permission from app settings", Toast.LENGTH_LONG).show();
                    } else {

                        ActivityCompat.requestPermissions(this,
                                new String[]{Manifest.permission.READ_MEDIA_IMAGES},
                                1);
                    }
                } else {

                    onImageViewClick();
                }

            } else {
                if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {

                    if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                        Toast.makeText(this, "Please provide storage permission from app settings", Toast.LENGTH_LONG).show();
                    } else {

                        ActivityCompat.requestPermissions(this,
                                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                                1);
                    }
                } else {
                    onImageViewClick();

                }
        }
    }

    private void onImageViewClick() {
        // type = 1;
        PickSetup setup = new PickSetup();
        // super.customize(setup);

        PickImageDialog.build(setup)
                //.setOnClick(this)
                .show(this);
    }

    @Override
    public void onPickResult(PickResult r) {
        if (r.getError() == null) {
            //If you want the Bitmap.
            chooseimage.setImageBitmap(r.getBitmap());
            userBitmap= r.getBitmap();

        } else {
            //Handle possible errors
            //TODO: do what you have to do with r.getError();
            Toast.makeText(this, r.getError().getMessage(), Toast.LENGTH_LONG).show();
        }

    }

    public static boolean checkPermission(Context mContext, String Permission) {
        int result = ContextCompat.checkSelfPermission(mContext, Permission);
        return result == PackageManager.PERMISSION_GRANTED;
    }

    private void stdRadio() {

        // This overrides the radiogroup onCheckListener
        radioGroupStdESEA.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // This will get the radiobutton that has changed in its check state
                RadioButton checkedRadioButton = (RadioButton) group.findViewById(checkedId);
                // This puts the value (true/false) into the variable
                boolean isChecked = checkedRadioButton.isChecked();
                // If the radiobutton that has changed in check state is now checked...
                if (isChecked) {
                    //  Toast.makeText(AddUserPermissionActivity.this,"radioGroupVehicle:" + checkedRadioButton.getText(),Toast.LENGTH_LONG).show();
                    sea = checkedRadioButton.getText().toString();

                    if (checkedRadioButton.getText().toString().equalsIgnoreCase("yes")) {
                        std_icon164.setVisibility(View.VISIBLE);
                        std_icons164.setVisibility(View.GONE);
                    } else {
                        std_icon164.setVisibility(View.INVISIBLE);
                        std_icons164.setVisibility(View.VISIBLE);
                    }


                    // Changes the textview's text to "Checked: example radiobutton text"
                    //  tv.setText("Checked:" + checkedRadioButton.getText());
                }
            }
        });


        // This overrides the radiogroup onCheckListener
        radioGroupStd.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                // This will get the radiobutton that has changed in its check state
                RadioButton checkedRadioButton = (RadioButton)group.findViewById(checkedId);
                // This puts the value (true/false) into the variable
                boolean isChecked = checkedRadioButton.isChecked();
                // If the radiobutton that has changed in check state is now checked...
                if (isChecked)
                {
                    //  Toast.makeText(AddUserPermissionActivity.this,"radioGroupVehicle:" + checkedRadioButton.getText(),Toast.LENGTH_LONG).show();
                    student = checkedRadioButton.getText().toString();

                    if (checkedRadioButton.getText().toString().equalsIgnoreCase("yes")){
                        std_icon.setVisibility(View.VISIBLE);
                        std_icons.setVisibility(View.GONE);
                    }else{
                        std_icon.setVisibility(View.INVISIBLE);
                        std_icons.setVisibility(View.VISIBLE);
                    }


                    // Changes the textview's text to "Checked: example radiobutton text"
                    //  tv.setText("Checked:" + checkedRadioButton.getText());
                }
            }
        });

        // This overrides the radiogroup onCheckListener
        radioGroupAdmission.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                // This will get the radiobutton that has changed in its check state
                RadioButton checkedRadioButton = (RadioButton)group.findViewById(checkedId);
                // This puts the value (true/false) into the variable
                boolean isChecked = checkedRadioButton.isChecked();
                // If the radiobutton that has changed in check state is now checked...
                if (isChecked)
                {
                    //  Toast.makeText(AddUserPermissionActivity.this,"radioGroupVehicle:" + checkedRadioButton.getText(),Toast.LENGTH_LONG).show();
                    form = checkedRadioButton.getText().toString();

                    if (checkedRadioButton.getText().toString().equalsIgnoreCase("yes")){
                        std_icon1.setVisibility(View.VISIBLE);
                        std_icons1.setVisibility(View.GONE);
                    }else{
                        std_icon1.setVisibility(View.INVISIBLE);
                        std_icons1.setVisibility(View.VISIBLE);
                    }


                    // Changes the textview's text to "Checked: example radiobutton text"
                    //  tv.setText("Checked:" + checkedRadioButton.getText());
                }
            }
        });
        // This overrides the radiogroup onCheckListener
        radioGroupStdGateAttend.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                // This will get the radiobutton that has changed in its check state
                RadioButton checkedRadioButton = (RadioButton)group.findViewById(checkedId);
                // This puts the value (true/false) into the variable
                boolean isChecked = checkedRadioButton.isChecked();
                // If the radiobutton that has changed in check state is now checked...
                if (isChecked)
                {
                    //  Toast.makeText(AddUserPermissionActivity.this,"radioGroupVehicle:" + checkedRadioButton.getText(),Toast.LENGTH_LONG).show();
                    stdGateAttend = checkedRadioButton.getText().toString();

                    if (checkedRadioButton.getText().toString().equalsIgnoreCase("yes")){
                        std_icon2.setVisibility(View.VISIBLE);
                        std_icons2.setVisibility(View.GONE);
                    }else{
                        std_icon2.setVisibility(View.INVISIBLE);
                        std_icons2.setVisibility(View.VISIBLE);
                    }


                    // Changes the textview's text to "Checked: example radiobutton text"
                    //  tv.setText("Checked:" + checkedRadioButton.getText());
                }
            }
        });
        // This overrides the radiogroup onCheckListener
        radioGroupStdAttendance.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                // This will get the radiobutton that has changed in its check state
                RadioButton checkedRadioButton = (RadioButton)group.findViewById(checkedId);
                // This puts the value (true/false) into the variable
                boolean isChecked = checkedRadioButton.isChecked();
                // If the radiobutton that has changed in check state is now checked...
                if (isChecked)
                {
                    //  Toast.makeText(AddUserPermissionActivity.this,"radioGroupVehicle:" + checkedRadioButton.getText(),Toast.LENGTH_LONG).show();
                    stdAttend = checkedRadioButton.getText().toString();

                    if (checkedRadioButton.getText().toString().equalsIgnoreCase("yes")){
                        std_icon3.setVisibility(View.VISIBLE);
                        std_icons3.setVisibility(View.GONE);
                    }else{
                        std_icon3.setVisibility(View.INVISIBLE);
                        std_icons3.setVisibility(View.VISIBLE);
                    }


                    // Changes the textview's text to "Checked: example radiobutton text"
                    //  tv.setText("Checked:" + checkedRadioButton.getText());
                }
            }
        });
        // This overrides the radiogroup onCheckListener
        radioGroupStdTimetable.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                // This will get the radiobutton that has changed in its check state
                RadioButton checkedRadioButton = (RadioButton)group.findViewById(checkedId);
                // This puts the value (true/false) into the variable
                boolean isChecked = checkedRadioButton.isChecked();
                // If the radiobutton that has changed in check state is now checked...
                if (isChecked)
                {
                    //  Toast.makeText(AddUserPermissionActivity.this,"radioGroupVehicle:" + checkedRadioButton.getText(),Toast.LENGTH_LONG).show();
                    timetable = checkedRadioButton.getText().toString();

                    if (checkedRadioButton.getText().toString().equalsIgnoreCase("yes")){
                        std_icon4.setVisibility(View.VISIBLE);
                        std_icons4.setVisibility(View.GONE);
                    }else{
                        std_icon4.setVisibility(View.INVISIBLE);
                        std_icons4.setVisibility(View.VISIBLE);
                    }


                    // Changes the textview's text to "Checked: example radiobutton text"
                    //  tv.setText("Checked:" + checkedRadioButton.getText());
                }
            }
        });



        // This overrides the radiogroup onCheckListener
        radioGroupStdHomework.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                // This will get the radiobutton that has changed in its check state
                RadioButton checkedRadioButton = (RadioButton)group.findViewById(checkedId);
                // This puts the value (true/false) into the variable
                boolean isChecked = checkedRadioButton.isChecked();
                // If the radiobutton that has changed in check state is now checked...
                if (isChecked)
                {
                    //  Toast.makeText(AddUserPermissionActivity.this,"radioGroupVehicle:" + checkedRadioButton.getText(),Toast.LENGTH_LONG).show();
                    homework = checkedRadioButton.getText().toString();

                    if (checkedRadioButton.getText().toString().equalsIgnoreCase("yes")){
                        std_icon5.setVisibility(View.VISIBLE);
                        std_icons5.setVisibility(View.GONE);
                    }else{
                        std_icon5.setVisibility(View.INVISIBLE);
                        std_icons5.setVisibility(View.VISIBLE);
                    }


                    // Changes the textview's text to "Checked: example radiobutton text"
                    //  tv.setText("Checked:" + checkedRadioButton.getText());
                }
            }
        });
        // This overrides the radiogroup onCheckListener
        radioGroupStdClasswork.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                // This will get the radiobutton that has changed in its check state
                RadioButton checkedRadioButton = (RadioButton)group.findViewById(checkedId);
                // This puts the value (true/false) into the variable
                boolean isChecked = checkedRadioButton.isChecked();
                // If the radiobutton that has changed in check state is now checked...
                if (isChecked)
                {
                    //  Toast.makeText(AddUserPermissionActivity.this,"radioGroupVehicle:" + checkedRadioButton.getText(),Toast.LENGTH_LONG).show();
                    classwork = checkedRadioButton.getText().toString();

                    if (checkedRadioButton.getText().toString().equalsIgnoreCase("yes")){
                        std_icon6.setVisibility(View.VISIBLE);
                        std_icons6.setVisibility(View.GONE);
                    }else{
                        std_icon6.setVisibility(View.INVISIBLE);
                        std_icons6.setVisibility(View.VISIBLE);
                    }


                    // Changes the textview's text to "Checked: example radiobutton text"
                    //  tv.setText("Checked:" + checkedRadioButton.getText());
                }
            }
        });
        // This overrides the radiogroup onCheckListener
        radioGroupStdAssignment.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                // This will get the radiobutton that has changed in its check state
                RadioButton checkedRadioButton = (RadioButton)group.findViewById(checkedId);
                // This puts the value (true/false) into the variable
                boolean isChecked = checkedRadioButton.isChecked();
                // If the radiobutton that has changed in check state is now checked...
                if (isChecked)
                {
                    //  Toast.makeText(AddUserPermissionActivity.this,"radioGroupVehicle:" + checkedRadioButton.getText(),Toast.LENGTH_LONG).show();
                    assign = checkedRadioButton.getText().toString();

                    if (checkedRadioButton.getText().toString().equalsIgnoreCase("yes")){
                        std_icon7.setVisibility(View.VISIBLE);
                        std_icons7.setVisibility(View.GONE);
                    }else{
                        std_icon7.setVisibility(View.INVISIBLE);
                        std_icons7.setVisibility(View.VISIBLE);
                    }


                    // Changes the textview's text to "Checked: example radiobutton text"
                    //  tv.setText("Checked:" + checkedRadioButton.getText());
                }
            }
        });
        // This overrides the radiogroup onCheckListener
        radioGroupStdExamination.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                // This will get the radiobutton that has changed in its check state
                RadioButton checkedRadioButton = (RadioButton)group.findViewById(checkedId);
                // This puts the value (true/false) into the variable
                boolean isChecked = checkedRadioButton.isChecked();
                // If the radiobutton that has changed in check state is now checked...
                if (isChecked)
                {
                    //  Toast.makeText(AddUserPermissionActivity.this,"radioGroupVehicle:" + checkedRadioButton.getText(),Toast.LENGTH_LONG).show();
                    exam = checkedRadioButton.getText().toString();

                    if (checkedRadioButton.getText().toString().equalsIgnoreCase("yes")){
                        std_icon8.setVisibility(View.VISIBLE);
                        std_icons8.setVisibility(View.GONE);
                    }else{
                        std_icon8.setVisibility(View.INVISIBLE);
                        std_icons8.setVisibility(View.VISIBLE);
                    }


                    // Changes the textview's text to "Checked: example radiobutton text"
                    //  tv.setText("Checked:" + checkedRadioButton.getText());
                }
            }
        });
        // This overrides the radiogroup onCheckListener
        radioGroupStdTest.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                // This will get the radiobutton that has changed in its check state
                RadioButton checkedRadioButton = (RadioButton)group.findViewById(checkedId);
                // This puts the value (true/false) into the variable
                boolean isChecked = checkedRadioButton.isChecked();
                // If the radiobutton that has changed in check state is now checked...
                if (isChecked)
                {
                    //  Toast.makeText(AddUserPermissionActivity.this,"radioGroupVehicle:" + checkedRadioButton.getText(),Toast.LENGTH_LONG).show();
                    classtest = checkedRadioButton.getText().toString();

                    if (checkedRadioButton.getText().toString().equalsIgnoreCase("yes")){
                        std_icon9.setVisibility(View.VISIBLE);
                        std_icons9.setVisibility(View.GONE);
                    }else{
                        std_icon9.setVisibility(View.INVISIBLE);
                        std_icons9.setVisibility(View.VISIBLE);
                    }


                    // Changes the textview's text to "Checked: example radiobutton text"
                    //  tv.setText("Checked:" + checkedRadioButton.getText());
                }
            }
        });
        // This overrides the radiogroup onCheckListener
        radioGroupStdDatesheet.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                // This will get the radiobutton that has changed in its check state
                RadioButton checkedRadioButton = (RadioButton)group.findViewById(checkedId);
                // This puts the value (true/false) into the variable
                boolean isChecked = checkedRadioButton.isChecked();
                // If the radiobutton that has changed in check state is now checked...
                if (isChecked)
                {
                    //  Toast.makeText(AddUserPermissionActivity.this,"radioGroupVehicle:" + checkedRadioButton.getText(),Toast.LENGTH_LONG).show();
                    datesheet = checkedRadioButton.getText().toString();

                    if (checkedRadioButton.getText().toString().equalsIgnoreCase("yes")){
                        std_icon10.setVisibility(View.VISIBLE);
                        std_icons10.setVisibility(View.GONE);
                    }else{
                        std_icon10.setVisibility(View.INVISIBLE);
                        std_icons10.setVisibility(View.VISIBLE);
                    }


                    // Changes the textview's text to "Checked: example radiobutton text"
                    //  tv.setText("Checked:" + checkedRadioButton.getText());
                }
            }
        });
        // This overrides the radiogroup onCheckListener
        radioGroupStdLeave.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                // This will get the radiobutton that has changed in its check state
                RadioButton checkedRadioButton = (RadioButton)group.findViewById(checkedId);
                // This puts the value (true/false) into the variable
                boolean isChecked = checkedRadioButton.isChecked();
                // If the radiobutton that has changed in check state is now checked...
                if (isChecked)
                {
                    //  Toast.makeText(AddUserPermissionActivity.this,"radioGroupVehicle:" + checkedRadioButton.getText(),Toast.LENGTH_LONG).show();
                    stdLeave = checkedRadioButton.getText().toString();

                    if (checkedRadioButton.getText().toString().equalsIgnoreCase("yes")){
                        std_icon11.setVisibility(View.VISIBLE);
                        std_icons11.setVisibility(View.GONE);
                    }else{
                        std_icon11.setVisibility(View.INVISIBLE);
                        std_icons11.setVisibility(View.VISIBLE);
                    }


                    // Changes the textview's text to "Checked: example radiobutton text"
                    //  tv.setText("Checked:" + checkedRadioButton.getText());
                }
            }
        });
        // This overrides the radiogroup onCheckListener
        radioGroupStdSyllabus.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                // This will get the radiobutton that has changed in its check state
                RadioButton checkedRadioButton = (RadioButton)group.findViewById(checkedId);
                // This puts the value (true/false) into the variable
                boolean isChecked = checkedRadioButton.isChecked();
                // If the radiobutton that has changed in check state is now checked...
                if (isChecked)
                {
                    //  Toast.makeText(AddUserPermissionActivity.this,"radioGroupVehicle:" + checkedRadioButton.getText(),Toast.LENGTH_LONG).show();
                    syllabus = checkedRadioButton.getText().toString();

                    if (checkedRadioButton.getText().toString().equalsIgnoreCase("yes")){
                        std_icon12.setVisibility(View.VISIBLE);
                        std_icons12.setVisibility(View.GONE);
                    }else{
                        std_icon12.setVisibility(View.INVISIBLE);
                        std_icons12.setVisibility(View.VISIBLE);
                    }


                    // Changes the textview's text to "Checked: example radiobutton text"
                    //  tv.setText("Checked:" + checkedRadioButton.getText());
                }
            }
        });
        // This overrides the radiogroup onCheckListener
        radioGroupStdFee.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                // This will get the radiobutton that has changed in its check state
                RadioButton checkedRadioButton = (RadioButton)group.findViewById(checkedId);
                // This puts the value (true/false) into the variable
                boolean isChecked = checkedRadioButton.isChecked();
                // If the radiobutton that has changed in check state is now checked...
                if (isChecked)
                {
                    //  Toast.makeText(AddUserPermissionActivity.this,"radioGroupVehicle:" + checkedRadioButton.getText(),Toast.LENGTH_LONG).show();
                    fee = checkedRadioButton.getText().toString();

                    if (checkedRadioButton.getText().toString().equalsIgnoreCase("yes")){
                        std_icon13.setVisibility(View.VISIBLE);
                        std_icons13.setVisibility(View.GONE);
                    }else{
                        std_icon13.setVisibility(View.INVISIBLE);
                        std_icons13.setVisibility(View.VISIBLE);
                    }


                    // Changes the textview's text to "Checked: example radiobutton text"
                    //  tv.setText("Checked:" + checkedRadioButton.getText());
                }
            }
        });
        // This overrides the radiogroup onCheckListener
        radioGroupStdGatePass.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                // This will get the radiobutton that has changed in its check state
                RadioButton checkedRadioButton = (RadioButton)group.findViewById(checkedId);
                // This puts the value (true/false) into the variable
                boolean isChecked = checkedRadioButton.isChecked();
                // If the radiobutton that has changed in check state is now checked...
                if (isChecked)
                {
                    //  Toast.makeText(AddUserPermissionActivity.this,"radioGroupVehicle:" + checkedRadioButton.getText(),Toast.LENGTH_LONG).show();
                    stdGatePass = checkedRadioButton.getText().toString();

                    if (checkedRadioButton.getText().toString().equalsIgnoreCase("yes")){
                        std_icon14.setVisibility(View.VISIBLE);
                        std_icons14.setVisibility(View.GONE);
                    }else{
                        std_icon14.setVisibility(View.INVISIBLE);
                        std_icons14.setVisibility(View.VISIBLE);
                    }


                    // Changes the textview's text to "Checked: example radiobutton text"
                    //  tv.setText("Checked:" + checkedRadioButton.getText());
                }
            }
        });
        // This overrides the radiogroup onCheckListener
        radioGroupStdEbook.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                // This will get the radiobutton that has changed in its check state
                RadioButton checkedRadioButton = (RadioButton)group.findViewById(checkedId);
                // This puts the value (true/false) into the variable
                boolean isChecked = checkedRadioButton.isChecked();
                // If the radiobutton that has changed in check state is now checked...
                if (isChecked)
                {
                    //  Toast.makeText(AddUserPermissionActivity.this,"radioGroupVehicle:" + checkedRadioButton.getText(),Toast.LENGTH_LONG).show();
                    ebook = checkedRadioButton.getText().toString();

                    if (checkedRadioButton.getText().toString().equalsIgnoreCase("yes")){
                        std_icon15.setVisibility(View.VISIBLE);
                        std_icons15.setVisibility(View.GONE);
                    }else{
                        std_icon15.setVisibility(View.INVISIBLE);
                        std_icons15.setVisibility(View.VISIBLE);
                    }


                    // Changes the textview's text to "Checked: example radiobutton text"
                    //  tv.setText("Checked:" + checkedRadioButton.getText());
                }
            }
        });
        // This overrides the radiogroup onCheckListener
        radioGroupStdELearning.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                // This will get the radiobutton that has changed in its check state
                RadioButton checkedRadioButton = (RadioButton)group.findViewById(checkedId);
                // This puts the value (true/false) into the variable
                boolean isChecked = checkedRadioButton.isChecked();
                // If the radiobutton that has changed in check state is now checked...
                if (isChecked)
                {
                    //  Toast.makeText(AddUserPermissionActivity.this,"radioGroupVehicle:" + checkedRadioButton.getText(),Toast.LENGTH_LONG).show();
                    elearning = checkedRadioButton.getText().toString();

                    if (checkedRadioButton.getText().toString().equalsIgnoreCase("yes")){
                        std_icon16.setVisibility(View.VISIBLE);
                        std_icons16.setVisibility(View.GONE);
                    }else{
                        std_icon16.setVisibility(View.INVISIBLE);
                        std_icons16.setVisibility(View.VISIBLE);
                    }


                    // Changes the textview's text to "Checked: example radiobutton text"
                    //  tv.setText("Checked:" + checkedRadioButton.getText());
                }
            }
        });

        // This overrides the radiogroup onCheckListener
        radioGroupStdHealth.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                // This will get the radiobutton that has changed in its check state
                RadioButton checkedRadioButton = (RadioButton)group.findViewById(checkedId);
                // This puts the value (true/false) into the variable
                boolean isChecked = checkedRadioButton.isChecked();
                // If the radiobutton that has changed in check state is now checked...
                if (isChecked)
                {
                    //  Toast.makeText(AddUserPermissionActivity.this,"radioGroupVehicle:" + checkedRadioButton.getText(),Toast.LENGTH_LONG).show();
                    health = checkedRadioButton.getText().toString();

                    if (checkedRadioButton.getText().toString().equalsIgnoreCase("yes")){
                        std_icon161.setVisibility(View.VISIBLE);
                        std_icons161.setVisibility(View.GONE);
                    }else{
                        std_icon161.setVisibility(View.INVISIBLE);
                        std_icons161.setVisibility(View.VISIBLE);
                    }


                    // Changes the textview's text to "Checked: example radiobutton text"
                    //  tv.setText("Checked:" + checkedRadioButton.getText());
                }
            }
        });

        // This overrides the radiogroup onCheckListener
        radioGroupStdPerformance.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                // This will get the radiobutton that has changed in its check state
                RadioButton checkedRadioButton = (RadioButton)group.findViewById(checkedId);
                // This puts the value (true/false) into the variable
                boolean isChecked = checkedRadioButton.isChecked();
                // If the radiobutton that has changed in check state is now checked...
                if (isChecked)
                {
                    //  Toast.makeText(AddUserPermissionActivity.this,"radioGroupVehicle:" + checkedRadioButton.getText(),Toast.LENGTH_LONG).show();
                    performance = checkedRadioButton.getText().toString();

                    if (checkedRadioButton.getText().toString().equalsIgnoreCase("yes")){
                        std_icon162.setVisibility(View.VISIBLE);
                        std_icons162.setVisibility(View.GONE);
                    }else{
                        std_icon162.setVisibility(View.INVISIBLE);
                        std_icons162.setVisibility(View.VISIBLE);
                    }


                    // Changes the textview's text to "Checked: example radiobutton text"
                    //  tv.setText("Checked:" + checkedRadioButton.getText());
                }
            }
        });

        // This overrides the radiogroup onCheckListener
        radioGroupStdEPlanner.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                // This will get the radiobutton that has changed in its check state
                RadioButton checkedRadioButton = (RadioButton)group.findViewById(checkedId);
                // This puts the value (true/false) into the variable
                boolean isChecked = checkedRadioButton.isChecked();
                // If the radiobutton that has changed in check state is now checked...
                if (isChecked)
                {
                    //  Toast.makeText(AddUserPermissionActivity.this,"radioGroupVehicle:" + checkedRadioButton.getText(),Toast.LENGTH_LONG).show();
                    planner = checkedRadioButton.getText().toString();

                    if (checkedRadioButton.getText().toString().equalsIgnoreCase("yes")){
                        std_icon163.setVisibility(View.VISIBLE);
                        std_icons163.setVisibility(View.GONE);
                    }else{
                        std_icon163.setVisibility(View.INVISIBLE);
                        std_icons163.setVisibility(View.VISIBLE);
                    }

                    // Changes the textview's text to "Checked: example radiobutton text"
                    //  tv.setText("Checked:" + checkedRadioButton.getText());
                }
            }
        });


    }

    private void staffRadio() {

        // This overrides the radiogroup onCheckListener
        radioGroupStaff.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                // This will get the radiobutton that has changed in its check state
                RadioButton checkedRadioButton = (RadioButton)group.findViewById(checkedId);
                // This puts the value (true/false) into the variable
                boolean isChecked = checkedRadioButton.isChecked();
                // If the radiobutton that has changed in check state is now checked...
                if (isChecked)
                {
                    //  Toast.makeText(AddUserPermissionActivity.this,"radioGroupVehicle:" + checkedRadioButton.getText(),Toast.LENGTH_LONG).show();
                    staff = checkedRadioButton.getText().toString();

                    if (checkedRadioButton.getText().toString().equalsIgnoreCase("yes")){
                        staff_icon.setVisibility(View.VISIBLE);
                        staff_icons.setVisibility(View.GONE);
                    }else{
                        staff_icon.setVisibility(View.INVISIBLE);
                        staff_icons.setVisibility(View.VISIBLE);
                    }


                    // Changes the textview's text to "Checked: example radiobutton text"
                    //  tv.setText("Checked:" + checkedRadioButton.getText());
                }
            }
        });

        // This overrides the radiogroup onCheckListener
        radioGroupIncharge.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                // This will get the radiobutton that has changed in its check state
                RadioButton checkedRadioButton = (RadioButton)group.findViewById(checkedId);
                // This puts the value (true/false) into the variable
                boolean isChecked = checkedRadioButton.isChecked();
                // If the radiobutton that has changed in check state is now checked...
                if (isChecked)
                {
                    //  Toast.makeText(AddUserPermissionActivity.this,"radioGroupVehicle:" + checkedRadioButton.getText(),Toast.LENGTH_LONG).show();
                    incharge = checkedRadioButton.getText().toString();

                    if (checkedRadioButton.getText().toString().equalsIgnoreCase("yes")){
                        staff_icon1.setVisibility(View.VISIBLE);
                        staff_icons1.setVisibility(View.GONE);
                    }else{
                        staff_icon1.setVisibility(View.INVISIBLE);
                        staff_icons1.setVisibility(View.VISIBLE);
                    }


                    // Changes the textview's text to "Checked: example radiobutton text"
                    //  tv.setText("Checked:" + checkedRadioButton.getText());
                }
            }
        });

        // This overrides the radiogroup onCheckListener
        radioGroupSubject.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                // This will get the radiobutton that has changed in its check state
                RadioButton checkedRadioButton = (RadioButton)group.findViewById(checkedId);
                // This puts the value (true/false) into the variable
                boolean isChecked = checkedRadioButton.isChecked();
                // If the radiobutton that has changed in check state is now checked...
                if (isChecked)
                {
                    //  Toast.makeText(AddUserPermissionActivity.this,"radioGroupVehicle:" + checkedRadioButton.getText(),Toast.LENGTH_LONG).show();
                    subject = checkedRadioButton.getText().toString();

                    if (checkedRadioButton.getText().toString().equalsIgnoreCase("yes")){
                        staff_icon2.setVisibility(View.VISIBLE);
                        staff_icons2.setVisibility(View.GONE);
                    }else{
                        staff_icon2.setVisibility(View.INVISIBLE);
                        staff_icons2.setVisibility(View.VISIBLE);
                    }


                    // Changes the textview's text to "Checked: example radiobutton text"
                    //  tv.setText("Checked:" + checkedRadioButton.getText());
                }
            }
        });

        // This overrides the radiogroup onCheckListener
        radioGroupGate.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                // This will get the radiobutton that has changed in its check state
                RadioButton checkedRadioButton = (RadioButton)group.findViewById(checkedId);
                // This puts the value (true/false) into the variable
                boolean isChecked = checkedRadioButton.isChecked();
                // If the radiobutton that has changed in check state is now checked...
                if (isChecked)
                {
                    //  Toast.makeText(AddUserPermissionActivity.this,"radioGroupVehicle:" + checkedRadioButton.getText(),Toast.LENGTH_LONG).show();
                    staffGateAttend = checkedRadioButton.getText().toString();

                    if (checkedRadioButton.getText().toString().equalsIgnoreCase("yes")){
                        staff_icon3.setVisibility(View.VISIBLE);
                        staff_icons3.setVisibility(View.GONE);
                    }else{
                        staff_icon3.setVisibility(View.INVISIBLE);
                        staff_icons3.setVisibility(View.VISIBLE);
                    }


                    // Changes the textview's text to "Checked: example radiobutton text"
                    //  tv.setText("Checked:" + checkedRadioButton.getText());
                }
            }
        });

        // This overrides the radiogroup onCheckListener
        radioGroupStaffAttendance.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                // This will get the radiobutton that has changed in its check state
                RadioButton checkedRadioButton = (RadioButton)group.findViewById(checkedId);
                // This puts the value (true/false) into the variable
                boolean isChecked = checkedRadioButton.isChecked();
                // If the radiobutton that has changed in check state is now checked...
                if (isChecked)
                {
                    //  Toast.makeText(AddUserPermissionActivity.this,"radioGroupVehicle:" + checkedRadioButton.getText(),Toast.LENGTH_LONG).show();
                    staffAttend = checkedRadioButton.getText().toString();

                    if (checkedRadioButton.getText().toString().equalsIgnoreCase("yes")){
                        staff_icon4.setVisibility(View.VISIBLE);
                        staff_icons4.setVisibility(View.GONE);
                    }else{
                        staff_icon4.setVisibility(View.INVISIBLE);
                        staff_icons4.setVisibility(View.VISIBLE);
                    }


                    // Changes the textview's text to "Checked: example radiobutton text"
                    //  tv.setText("Checked:" + checkedRadioButton.getText());
                }
            }
        });

        // This overrides the radiogroup onCheckListener
        radioGroupLeave.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                // This will get the radiobutton that has changed in its check state
                RadioButton checkedRadioButton = (RadioButton)group.findViewById(checkedId);
                // This puts the value (true/false) into the variable
                boolean isChecked = checkedRadioButton.isChecked();
                // If the radiobutton that has changed in check state is now checked...
                if (isChecked)
                {
                    //  Toast.makeText(AddUserPermissionActivity.this,"radioGroupVehicle:" + checkedRadioButton.getText(),Toast.LENGTH_LONG).show();
                    saffLeave = checkedRadioButton.getText().toString();

                    if (checkedRadioButton.getText().toString().equalsIgnoreCase("yes")){
                        staff_icon5.setVisibility(View.VISIBLE);
                        staff_icons5.setVisibility(View.GONE);
                    }else{
                        staff_icon5.setVisibility(View.INVISIBLE);
                        staff_icons5.setVisibility(View.VISIBLE);
                    }


                    // Changes the textview's text to "Checked: example radiobutton text"
                    //  tv.setText("Checked:" + checkedRadioButton.getText());
                }
            }
        });

        // This overrides the radiogroup onCheckListener
        radioGroupGatePass.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                // This will get the radiobutton that has changed in its check state
                RadioButton checkedRadioButton = (RadioButton)group.findViewById(checkedId);
                // This puts the value (true/false) into the variable
                boolean isChecked = checkedRadioButton.isChecked();
                // If the radiobutton that has changed in check state is now checked...
                if (isChecked)
                {
                    //  Toast.makeText(AddUserPermissionActivity.this,"radioGroupVehicle:" + checkedRadioButton.getText(),Toast.LENGTH_LONG).show();
                    staffGatePass = checkedRadioButton.getText().toString();

                    if (checkedRadioButton.getText().toString().equalsIgnoreCase("yes")){
                        staff_icon6.setVisibility(View.VISIBLE);
                        staff_icons6.setVisibility(View.GONE);
                    }else{
                        staff_icon6.setVisibility(View.INVISIBLE);
                        staff_icons6.setVisibility(View.VISIBLE);
                    }


                    // Changes the textview's text to "Checked: example radiobutton text"
                    //  tv.setText("Checked:" + checkedRadioButton.getText());
                }
            }
        });
    }

    private void schoolRadio() {

        // This overrides the radiogroup onCheckListener
        radioGroupSetup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                // This will get the radiobutton that has changed in its check state
                RadioButton checkedRadioButton = (RadioButton)group.findViewById(checkedId);
                // This puts the value (true/false) into the variable
                boolean isChecked = checkedRadioButton.isChecked();
                // If the radiobutton that has changed in check state is now checked...
                if (isChecked)
                {
                  //  Toast.makeText(AddUserPermissionActivity.this,"radioGroupVehicle:" + checkedRadioButton.getText(),Toast.LENGTH_LONG).show();
                    setup = checkedRadioButton.getText().toString();

                    if (checkedRadioButton.getText().toString().equalsIgnoreCase("yes")){
                       icon.setVisibility(View.VISIBLE);
                       icons.setVisibility(View.GONE);
                    }else{
                        icon.setVisibility(View.INVISIBLE);
                        icons.setVisibility(View.VISIBLE);
                    }


                    // Changes the textview's text to "Checked: example radiobutton text"
                    //  tv.setText("Checked:" + checkedRadioButton.getText());
                }
            }
        });


        // This overrides the radiogroup onCheckListener
        radioGroupThought.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                // This will get the radiobutton that has changed in its check state
                RadioButton checkedRadioButton = (RadioButton)group.findViewById(checkedId);
                // This puts the value (true/false) into the variable
                boolean isChecked = checkedRadioButton.isChecked();
                // If the radiobutton that has changed in check state is now checked...
                if (isChecked)
                {
                    //  Toast.makeText(AddUserPermissionActivity.this,"radioGroupVehicle:" + checkedRadioButton.getText(),Toast.LENGTH_LONG).show();
                    thought = checkedRadioButton.getText().toString();

                    if (checkedRadioButton.getText().toString().equalsIgnoreCase("yes")){
                        icon1.setVisibility(View.VISIBLE);
                        icons1.setVisibility(View.GONE);
                    }else{
                        icon1.setVisibility(View.INVISIBLE);
                        icons1.setVisibility(View.VISIBLE);
                    }


                    // Changes the textview's text to "Checked: example radiobutton text"
                    //  tv.setText("Checked:" + checkedRadioButton.getText());
                }
            }
        });

        // This overrides the radiogroup onCheckListener
        radioGroupBanners.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                // This will get the radiobutton that has changed in its check state
                RadioButton checkedRadioButton = (RadioButton)group.findViewById(checkedId);
                // This puts the value (true/false) into the variable
                boolean isChecked = checkedRadioButton.isChecked();
                // If the radiobutton that has changed in check state is now checked...
                if (isChecked)
                {
                    //  Toast.makeText(AddUserPermissionActivity.this,"radioGroupVehicle:" + checkedRadioButton.getText(),Toast.LENGTH_LONG).show();
                    banner = checkedRadioButton.getText().toString();

                    if (checkedRadioButton.getText().toString().equalsIgnoreCase("yes")){
                        icon3.setVisibility(View.VISIBLE);
                        icons3.setVisibility(View.GONE);
                    }else{
                        icon3.setVisibility(View.INVISIBLE);
                        icons3.setVisibility(View.VISIBLE);
                    }


                    // Changes the textview's text to "Checked: example radiobutton text"
                    //  tv.setText("Checked:" + checkedRadioButton.getText());
                }
            }
        });


        // This overrides the radiogroup onCheckListener
        radioGroupNotice.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                // This will get the radiobutton that has changed in its check state
                RadioButton checkedRadioButton = (RadioButton)group.findViewById(checkedId);
                // This puts the value (true/false) into the variable
                boolean isChecked = checkedRadioButton.isChecked();
                // If the radiobutton that has changed in check state is now checked...
                if (isChecked)
                {
                    //  Toast.makeText(AddUserPermissionActivity.this,"radioGroupVehicle:" + checkedRadioButton.getText(),Toast.LENGTH_LONG).show();
                    notice = checkedRadioButton.getText().toString();

                    if (checkedRadioButton.getText().toString().equalsIgnoreCase("yes")){
                        icon4.setVisibility(View.VISIBLE);
                        icons4.setVisibility(View.GONE);
                    }else{
                        icon4.setVisibility(View.INVISIBLE);
                        icons4.setVisibility(View.VISIBLE);
                    }


                    // Changes the textview's text to "Checked: example radiobutton text"
                    //  tv.setText("Checked:" + checkedRadioButton.getText());
                }
            }
        });


        // This overrides the radiogroup onCheckListener
        radioGroupGallery.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                // This will get the radiobutton that has changed in its check state
                RadioButton checkedRadioButton = (RadioButton)group.findViewById(checkedId);
                // This puts the value (true/false) into the variable
                boolean isChecked = checkedRadioButton.isChecked();
                // If the radiobutton that has changed in check state is now checked...
                if (isChecked)
                {
                    //  Toast.makeText(AddUserPermissionActivity.this,"radioGroupVehicle:" + checkedRadioButton.getText(),Toast.LENGTH_LONG).show();
                    gallery = checkedRadioButton.getText().toString();

                    if (checkedRadioButton.getText().toString().equalsIgnoreCase("yes")){
                        icon5.setVisibility(View.VISIBLE);
                        icons5.setVisibility(View.GONE);
                    }else{
                        icon5.setVisibility(View.INVISIBLE);
                        icons5.setVisibility(View.VISIBLE);
                    }


                    // Changes the textview's text to "Checked: example radiobutton text"
                    //  tv.setText("Checked:" + checkedRadioButton.getText());
                }
            }
        });



        // This overrides the radiogroup onCheckListener
        radioGroupTransport.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                // This will get the radiobutton that has changed in its check state
                RadioButton checkedRadioButton = (RadioButton)group.findViewById(checkedId);
                // This puts the value (true/false) into the variable
                boolean isChecked = checkedRadioButton.isChecked();
                // If the radiobutton that has changed in check state is now checked...
                if (isChecked)
                {
                    //  Toast.makeText(AddUserPermissionActivity.this,"radioGroupVehicle:" + checkedRadioButton.getText(),Toast.LENGTH_LONG).show();
                    transport = checkedRadioButton.getText().toString();

                    if (checkedRadioButton.getText().toString().equalsIgnoreCase("yes")){
                        icon6.setVisibility(View.VISIBLE);
                        icons6.setVisibility(View.GONE);
                    }else{
                        icon6.setVisibility(View.INVISIBLE);
                        icons6.setVisibility(View.VISIBLE);
                    }


                    // Changes the textview's text to "Checked: example radiobutton text"
                    //  tv.setText("Checked:" + checkedRadioButton.getText());
                }
            }
        });


        // This overrides the radiogroup onCheckListener
        radioGroupIdCard.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                // This will get the radiobutton that has changed in its check state
                RadioButton checkedRadioButton = (RadioButton)group.findViewById(checkedId);
                // This puts the value (true/false) into the variable
                boolean isChecked = checkedRadioButton.isChecked();
                // If the radiobutton that has changed in check state is now checked...
                if (isChecked)
                {
                    //  Toast.makeText(AddUserPermissionActivity.this,"radioGroupVehicle:" + checkedRadioButton.getText(),Toast.LENGTH_LONG).show();
                    idcard = checkedRadioButton.getText().toString();

                    if (checkedRadioButton.getText().toString().equalsIgnoreCase("yes")){
                        std_icon17.setVisibility(View.VISIBLE);
                        std_icons17.setVisibility(View.GONE);
                    }else{
                        std_icon17.setVisibility(View.INVISIBLE);
                        std_icons17.setVisibility(View.VISIBLE);
                    }


                    // Changes the textview's text to "Checked: example radiobutton text"
                    //  tv.setText("Checked:" + checkedRadioButton.getText());
                }
            }
        });

        // This overrides the radiogroup onCheckListener
        radioGroupLibrary.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                // This will get the radiobutton that has changed in its check state
                RadioButton checkedRadioButton = (RadioButton)group.findViewById(checkedId);
                // This puts the value (true/false) into the variable
                boolean isChecked = checkedRadioButton.isChecked();
                // If the radiobutton that has changed in check state is now checked...
                if (isChecked)
                {
                    //  Toast.makeText(AddUserPermissionActivity.this,"radioGroupVehicle:" + checkedRadioButton.getText(),Toast.LENGTH_LONG).show();
                    library = checkedRadioButton.getText().toString();

                    if (checkedRadioButton.getText().toString().equalsIgnoreCase("yes")){
                        std_icon18.setVisibility(View.VISIBLE);
                        std_icons18.setVisibility(View.GONE);
                    }else{
                        std_icon18.setVisibility(View.INVISIBLE);
                        std_icons18.setVisibility(View.VISIBLE);
                    }


                    // Changes the textview's text to "Checked: example radiobutton text"
                    //  tv.setText("Checked:" + checkedRadioButton.getText());
                }
            }
        });


        // This overrides the radiogroup onCheckListener
        radioGroupHrm.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                // This will get the radiobutton that has changed in its check state
                RadioButton checkedRadioButton = (RadioButton)group.findViewById(checkedId);
                // This puts the value (true/false) into the variable
                boolean isChecked = checkedRadioButton.isChecked();
                // If the radiobutton that has changed in check state is now checked...
                if (isChecked)
                {
                    //  Toast.makeText(AddUserPermissionActivity.this,"radioGroupVehicle:" + checkedRadioButton.getText(),Toast.LENGTH_LONG).show();
                    hrm = checkedRadioButton.getText().toString();

                    if (checkedRadioButton.getText().toString().equalsIgnoreCase("yes")){
                        std_icon19.setVisibility(View.VISIBLE);
                        std_icons19.setVisibility(View.GONE);
                    }else{
                        std_icon19.setVisibility(View.INVISIBLE);
                        std_icons19.setVisibility(View.VISIBLE);
                    }


                    // Changes the textview's text to "Checked: example radiobutton text"
                    //  tv.setText("Checked:" + checkedRadioButton.getText());
                }
            }
        });


        // This overrides the radiogroup onCheckListener
        radioGroupCertificate.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                // This will get the radiobutton that has changed in its check state
                RadioButton checkedRadioButton = (RadioButton)group.findViewById(checkedId);
                // This puts the value (true/false) into the variable
                boolean isChecked = checkedRadioButton.isChecked();
                // If the radiobutton that has changed in check state is now checked...
                if (isChecked)
                {
                    //  Toast.makeText(AddUserPermissionActivity.this,"radioGroupVehicle:" + checkedRadioButton.getText(),Toast.LENGTH_LONG).show();
                    certificate = checkedRadioButton.getText().toString();

                    if (checkedRadioButton.getText().toString().equalsIgnoreCase("yes")){
                        std_icon20.setVisibility(View.VISIBLE);
                        std_icons20.setVisibility(View.GONE);
                    }else{
                        std_icon20.setVisibility(View.INVISIBLE);
                        std_icons20.setVisibility(View.VISIBLE);
                    }


                    // Changes the textview's text to "Checked: example radiobutton text"
                    //  tv.setText("Checked:" + checkedRadioButton.getText());
                }
            }
        });


        // This overrides the radiogroup onCheckListener
        radioGroupHostel.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                // This will get the radiobutton that has changed in its check state
                RadioButton checkedRadioButton = (RadioButton)group.findViewById(checkedId);
                // This puts the value (true/false) into the variable
                boolean isChecked = checkedRadioButton.isChecked();
                // If the radiobutton that has changed in check state is now checked...
                if (isChecked)
                {
                    //  Toast.makeText(AddUserPermissionActivity.this,"radioGroupVehicle:" + checkedRadioButton.getText(),Toast.LENGTH_LONG).show();
                    hostel = checkedRadioButton.getText().toString();

                    if (checkedRadioButton.getText().toString().equalsIgnoreCase("yes")){
                        std_icon21.setVisibility(View.VISIBLE);
                        std_icons21.setVisibility(View.GONE);
                    }else{
                        std_icon21.setVisibility(View.INVISIBLE);
                        std_icons21.setVisibility(View.VISIBLE);
                    }


                    // Changes the textview's text to "Checked: example radiobutton text"
                    //  tv.setText("Checked:" + checkedRadioButton.getText());
                }
            }
        });


        // This overrides the radiogroup onCheckListener
        radioGroupPayroll.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                // This will get the radiobutton that has changed in its check state
                RadioButton checkedRadioButton = (RadioButton)group.findViewById(checkedId);
                // This puts the value (true/false) into the variable
                boolean isChecked = checkedRadioButton.isChecked();
                // If the radiobutton that has changed in check state is now checked...
                if (isChecked)
                {
                    //  Toast.makeText(AddUserPermissionActivity.this,"radioGroupVehicle:" + checkedRadioButton.getText(),Toast.LENGTH_LONG).show();
                    payroll = checkedRadioButton.getText().toString();

                    if (checkedRadioButton.getText().toString().equalsIgnoreCase("yes")){
                        std_icon22.setVisibility(View.VISIBLE);
                        std_icons22.setVisibility(View.GONE);
                    }else{
                        std_icon22.setVisibility(View.INVISIBLE);
                        std_icons22.setVisibility(View.VISIBLE);
                    }


                    // Changes the textview's text to "Checked: example radiobutton text"
                    //  tv.setText("Checked:" + checkedRadioButton.getText());
                }
            }
        });

        // This overrides the radiogroup onCheckListener
        radioGroupAccounts.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                // This will get the radiobutton that has changed in its check state
                RadioButton checkedRadioButton = (RadioButton)group.findViewById(checkedId);
                // This puts the value (true/false) into the variable
                boolean isChecked = checkedRadioButton.isChecked();
                // If the radiobutton that has changed in check state is now checked...
                if (isChecked)
                {
                    //  Toast.makeText(AddUserPermissionActivity.this,"radioGroupVehicle:" + checkedRadioButton.getText(),Toast.LENGTH_LONG).show();
                    account = checkedRadioButton.getText().toString();

                    if (checkedRadioButton.getText().toString().equalsIgnoreCase("yes")){
                        std_icon23.setVisibility(View.VISIBLE);
                        std_icons23.setVisibility(View.GONE);
                    }else{
                        std_icon23.setVisibility(View.INVISIBLE);
                        std_icons23.setVisibility(View.VISIBLE);
                    }

                    // Changes the textview's text to "Checked: example radiobutton text"
                    //  tv.setText("Checked:" + checkedRadioButton.getText());
                }
            }
        });


        // This overrides the radiogroup onCheckListener
        radioGroupEnquiry.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                // This will get the radiobutton that has changed in its check state
                RadioButton checkedRadioButton = (RadioButton)group.findViewById(checkedId);
                // This puts the value (true/false) into the variable
                boolean isChecked = checkedRadioButton.isChecked();
                // If the radiobutton that has changed in check state is now checked...
                if (isChecked)
                {
                    //  Toast.makeText(AddUserPermissionActivity.this,"radioGroupVehicle:" + checkedRadioButton.getText(),Toast.LENGTH_LONG).show();
                    enquiry = checkedRadioButton.getText().toString();

                    if (checkedRadioButton.getText().toString().equalsIgnoreCase("yes")){
                        std_icon231.setVisibility(View.VISIBLE);
                        std_icons231.setVisibility(View.GONE);
                    }else{
                        std_icon231.setVisibility(View.INVISIBLE);
                        std_icons231.setVisibility(View.VISIBLE);
                    }
                    // Changes the textview's text to "Checked: example radiobutton text"
                    //  tv.setText("Checked:" + checkedRadioButton.getText());
                }
            }
        });

        // This overrides the radiogroup onCheckListener
        radioGroupHoliday.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                // This will get the radiobutton that has changed in its check state
                RadioButton checkedRadioButton = (RadioButton)group.findViewById(checkedId);
                // This puts the value (true/false) into the variable
                boolean isChecked = checkedRadioButton.isChecked();
                // If the radiobutton that has changed in check state is now checked...
                if (isChecked)
                {
                    //  Toast.makeText(AddUserPermissionActivity.this,"radioGroupVehicle:" + checkedRadioButton.getText(),Toast.LENGTH_LONG).show();
                    holiday = checkedRadioButton.getText().toString();

                    if (checkedRadioButton.getText().toString().equalsIgnoreCase("yes")){
                        std_icon232.setVisibility(View.VISIBLE);
                        std_icons232.setVisibility(View.GONE);
                    }else{
                        std_icon232.setVisibility(View.INVISIBLE);
                        std_icons232.setVisibility(View.VISIBLE);
                    }
                    // Changes the textview's text to "Checked: example radiobutton text"
                    //  tv.setText("Checked:" + checkedRadioButton.getText());
                }
            }
        });

        // This overrides the radiogroup onCheckListener
        radioGroupSocial.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                // This will get the radiobutton that has changed in its check state
                RadioButton checkedRadioButton = (RadioButton)group.findViewById(checkedId);
                // This puts the value (true/false) into the variable
                boolean isChecked = checkedRadioButton.isChecked();
                // If the radiobutton that has changed in check state is now checked...
                if (isChecked)
                {
                    //  Toast.makeText(AddUserPermissionActivity.this,"radioGroupVehicle:" + checkedRadioButton.getText(),Toast.LENGTH_LONG).show();
                    social_post = checkedRadioButton.getText().toString();

                    if (checkedRadioButton.getText().toString().equalsIgnoreCase("yes")){
                        std_icon233.setVisibility(View.VISIBLE);
                        std_icons233.setVisibility(View.GONE);
                    }else{
                        std_icon233.setVisibility(View.INVISIBLE);
                        std_icons233.setVisibility(View.VISIBLE);
                    }
                    // Changes the textview's text to "Checked: example radiobutton text"
                    //  tv.setText("Checked:" + checkedRadioButton.getText());
                }
            }
        });

        // This overrides the radiogroup onCheckListener
        radioGroupschool_activty.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                // This will get the radiobutton that has changed in its check state
                RadioButton checkedRadioButton = (RadioButton)group.findViewById(checkedId);
                // This puts the value (true/false) into the variable
                boolean isChecked = checkedRadioButton.isChecked();
                // If the radiobutton that has changed in check state is now checked...
                if (isChecked)
                {
                    //  Toast.makeText(AddUserPermissionActivity.this,"radioGroupVehicle:" + checkedRadioButton.getText(),Toast.LENGTH_LONG).show();
                    school_activity = checkedRadioButton.getText().toString();

                    if (checkedRadioButton.getText().toString().equalsIgnoreCase("yes")){
                        std_icon234.setVisibility(View.VISIBLE);
                        std_icons234.setVisibility(View.GONE);
                    }else{
                        std_icon234.setVisibility(View.INVISIBLE);
                        std_icons234.setVisibility(View.VISIBLE);
                    }
                    // Changes the textview's text to "Checked: example radiobutton text"
                    //  tv.setText("Checked:" + checkedRadioButton.getText());
                }
            }
        });


        // This overrides the radiogroup onCheckListener
        radioGroupFuel.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                // This will get the radiobutton that has changed in its check state
                RadioButton checkedRadioButton = (RadioButton)group.findViewById(checkedId);
                // This puts the value (true/false) into the variable
                boolean isChecked = checkedRadioButton.isChecked();
                // If the radiobutton that has changed in check state is now checked...
                if (isChecked)
                {
                    //  Toast.makeText(AddUserPermissionActivity.this,"radioGroupVehicle:" + checkedRadioButton.getText(),Toast.LENGTH_LONG).show();
                    expense = checkedRadioButton.getText().toString();

                    if (checkedRadioButton.getText().toString().equalsIgnoreCase("yes")){
                        std_icon235.setVisibility(View.VISIBLE);
                        std_icons235.setVisibility(View.GONE);
                    }else{
                        std_icon235.setVisibility(View.INVISIBLE);
                        std_icons235.setVisibility(View.VISIBLE);
                    }
                    // Changes the textview's text to "Checked: example radiobutton text"
                    //  tv.setText("Checked:" + checkedRadioButton.getText());
                }
            }
        });

        // This overrides the radiogroup onCheckListener
        radioGroupVisitor.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                // This will get the radiobutton that has changed in its check state
                RadioButton checkedRadioButton = (RadioButton)group.findViewById(checkedId);
                // This puts the value (true/false) into the variable
                boolean isChecked = checkedRadioButton.isChecked();
                // If the radiobutton that has changed in check state is now checked...
                if (isChecked)
                {
                    //  Toast.makeText(AddUserPermissionActivity.this,"radioGroupVehicle:" + checkedRadioButton.getText(),Toast.LENGTH_LONG).show();
                    visitor = checkedRadioButton.getText().toString();

                    if (checkedRadioButton.getText().toString().equalsIgnoreCase("yes")){
                        std_icon236.setVisibility(View.VISIBLE);
                        std_icons236.setVisibility(View.GONE);
                    }else{
                        std_icon236.setVisibility(View.INVISIBLE);
                        std_icons236.setVisibility(View.VISIBLE);
                    }
                    // Changes the textview's text to "Checked: example radiobutton text"
                    //  tv.setText("Checked:" + checkedRadioButton.getText());
                }
            }
        });


        // This overrides the radiogroup onCheckListener
        radioGroupFeedback.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                // This will get the radiobutton that has changed in its check state
                RadioButton checkedRadioButton = (RadioButton)group.findViewById(checkedId);
                // This puts the value (true/false) into the variable
                boolean isChecked = checkedRadioButton.isChecked();
                // If the radiobutton that has changed in check state is now checked...
                if (isChecked)
                {
                    //  Toast.makeText(AddUserPermissionActivity.this,"radioGroupVehicle:" + checkedRadioButton.getText(),Toast.LENGTH_LONG).show();
                    feedback = checkedRadioButton.getText().toString();

                    if (checkedRadioButton.getText().toString().equalsIgnoreCase("yes")){
                        std_icon237.setVisibility(View.VISIBLE);
                        std_icons237.setVisibility(View.GONE);
                    }else{
                        std_icon237.setVisibility(View.INVISIBLE);
                        std_icons237.setVisibility(View.VISIBLE);
                    }
                    // Changes the textview's text to "Checked: example radiobutton text"
                    //  tv.setText("Checked:" + checkedRadioButton.getText());
                }
            }
        });

    }


    @OnClick(R.id.imgBack)
    public void callBackF() {
       commonBack();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        commonBack();
    }

    private void commonBack() {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(AddUserPermissionActivity.this);
        builder1.setMessage("Are you sure, you want to exit? ");
        builder1.setCancelable(false);
        builder1.setPositiveButton(
                "Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finish();
                    }
                });
        builder1.setNegativeButton(
                "No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

    @OnClick(R.id.p_add)
    public void p_add(){
        p_mini.setVisibility(View.VISIBLE);
        p_add.setVisibility(View.GONE);
        stopppp.setVisibility(View.VISIBLE);
    }


    @OnClick(R.id.p_mini)
    public void p_mini(){
        p_add.setVisibility(View.VISIBLE);
        p_mini.setVisibility(View.GONE);
        stopppp.setVisibility(View.GONE);

    }

    @OnClick(R.id.p_add1)
    public void p_add1(){
        p_mini1.setVisibility(View.VISIBLE);
        p_add1.setVisibility(View.GONE);
        stopppp1.setVisibility(View.VISIBLE);
    }


    @OnClick(R.id.p_mini1)
    public void p_mini1(){
        p_add1.setVisibility(View.VISIBLE);
        p_mini1.setVisibility(View.GONE);
        stopppp1.setVisibility(View.GONE);

    }

    @OnClick(R.id.p_add2)
    public void p_add2(){
        p_mini2.setVisibility(View.VISIBLE);
        p_add2.setVisibility(View.GONE);
        stopppp2.setVisibility(View.VISIBLE);
    }


    @OnClick(R.id.p_mini2)
    public void p_mini2(){
        p_add2.setVisibility(View.VISIBLE);
        p_mini2.setVisibility(View.GONE);
        stopppp2.setVisibility(View.GONE);

    }



    @OnClick(R.id.visit_add)
    public void visit_add(){
        visit_mini.setVisibility(View.VISIBLE);
        visit_add.setVisibility(View.GONE);
        topppp.setVisibility(View.VISIBLE);
    }


    @OnClick(R.id.visit_mini)
    public void visit_mini(){
        visit_add.setVisibility(View.VISIBLE);
        visit_mini.setVisibility(View.GONE);
        topppp.setVisibility(View.GONE);

    }

    @OnClick(R.id.v_add)
    public void v_add(){
        v_mini.setVisibility(View.VISIBLE);
        v_add.setVisibility(View.GONE);
        extra_deails.setVisibility(View.VISIBLE);
    }


    @OnClick(R.id.v_mini)
    public void v_mini(){
        v_add.setVisibility(View.VISIBLE);
        v_mini.setVisibility(View.GONE);
        extra_deails.setVisibility(View.GONE);

    }

}