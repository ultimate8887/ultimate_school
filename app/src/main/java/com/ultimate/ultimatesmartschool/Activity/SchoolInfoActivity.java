package com.ultimate.ultimatesmartschool.Activity;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetSequence;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.squareup.picasso.Picasso;

import com.ultimate.ultimatesmartschool.BeanModule.User;
import com.ultimate.ultimatesmartschool.ForCamera.IPickResult;
import com.ultimate.ultimatesmartschool.ForCamera.PickImageDialog;
import com.ultimate.ultimatesmartschool.ForCamera.PickResult;
import com.ultimate.ultimatesmartschool.ForCamera.PickSetup;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.CommonProgress;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SchoolInfoActivity extends AppCompatActivity implements OnMapReadyCallback, IPickResult {

    CommonProgress commonProgress;

    // map view data
    private GoogleMap map;
    private SupportMapFragment mapFragment;
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 1000;
    private Marker myMarker = null;
    double lat, lng;
    Handler handler = new Handler();
    Geocoder geo;
    List<Address> addresses = null;
    StringBuilder  str = null;
    LatLng latLng;
    String image_url="",school="",phone="",address1="",website="",email1="",latitude="",longitude="";
    String fb_url="",insta_url="",youtube_url="",review_url="";
    @BindView(R.id.toMeet)
    EditText email;

    @BindView(R.id.vistname)
    EditText name;

    @BindView(R.id.visitPurpose)
    EditText website_url;

    @BindView(R.id.face_book)
    EditText face_book;

    @BindView(R.id.insta_gram)
    EditText insta_gram;

    @BindView(R.id.you_tube)
    EditText you_tube;

    @BindView(R.id.submit)
    Button checkIn;

    private Bitmap userBitmap;

    @BindView(R.id.clickimage)
    ImageView clickimage;

    @BindView(R.id.visitimage)
    CircularImageView chooseimage;

    @BindView(R.id.vistphoneno)
    EditText phonenumber;

    @BindView(R.id.visitaddress)
    EditText address;

    @BindView(R.id.txtTitle)
    TextView txtTitle;

    @BindView(R.id.contact_support)
    ImageView contact_support;
    SharedPreferences sharedPreferences;

    int count=0,loded=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_school_info);
        ButterKnife.bind(this);
        txtTitle.setText("Update School Details");
        commonProgress = new CommonProgress(this);
        geo = new Geocoder(getApplicationContext(), Locale.getDefault());
        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map_location);

        checkIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendcheckinrequest();
            }
        });
        userprofile();
    }

    private void savePrefData(){
        sharedPreferences=SchoolInfoActivity.this.getSharedPreferences("boarding_pref_locations",MODE_PRIVATE);
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putBoolean("IsFirstTimeVisit_locations",true);
        editor.apply();
    }

    private boolean restorePrefData(){
        sharedPreferences = SchoolInfoActivity.this.getSharedPreferences("boarding_pref_locations",MODE_PRIVATE);
        return sharedPreferences.getBoolean("IsFirstTimeVisit_locations",false);
    }

    private void setShowcaseView() {
        new TapTargetSequence(this)
                .targets(TapTarget.forView(contact_support,"Update Current Location Button!","Tap the Location button to Update Current School Location " +
                        ",This helps to navigate your School location on Google Map.")
                        .outerCircleColor(R.color.light)
                        .outerCircleAlpha(0.96f)
                        .targetCircleColor(R.color.white)
                        .titleTextSize(20)
                        .titleTextColor(R.color.white)
                        .descriptionTextSize(14)
                        .descriptionTextColor(R.color.black)
                        .textColor(R.color.black)
                        .textTypeface(Typeface.SANS_SERIF)
                        .dimColor(R.color.black)
                        .drawShadow(true)
                        .cancelable(false)
                        .tintTarget(true)
                        .transparentTarget(true)
                        .targetRadius(60)).listener(new TapTargetSequence.Listener() {
            @Override
            public void onSequenceFinish() {
                // Toast.makeText(getApplicationContext(),"Sequence Finished",Toast.LENGTH_SHORT).show();
                 savePrefData();
                if (latitude.equalsIgnoreCase("0") && longitude.equalsIgnoreCase("0")) {
                    openDialog();
                }
            }

            @Override
            public void onSequenceStep(TapTarget lastTarget, boolean targetClicked) {
                // Toast.makeText(getActivity(),"GREAT!",Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSequenceCanceled(TapTarget lastTarget) {

            }
        }).start();


    }

    private void sendcheckinrequest() {

        if (name.getText().toString().isEmpty() || address.getText().toString().isEmpty()
                || phonenumber.getText().toString().isEmpty() || email.getText().toString().isEmpty() ) {
            Toast.makeText(SchoolInfoActivity.this, "Kindly fill complete School details", Toast.LENGTH_LONG).show();
        } else {
            HashMap<String, String> params = new HashMap<String, String>();
            commonProgress.show();
            params.put("fi_schoolname", name.getText().toString());
            params.put("fi_website", website_url.getText().toString());
            params.put("fi_phoneno",phonenumber.getText().toString() );
            params.put("fi_address",address.getText().toString() );
            params.put("fi_email",email.getText().toString() );
            params.put("fb",face_book.getText().toString() );
            params.put("insta",insta_gram.getText().toString() );
            params.put("youtube",you_tube.getText().toString() );
            params.put("review","");
            params.put("check","update" );
            if (userBitmap != null) {
                String encoded = Utils.encodeToBase64(userBitmap, Bitmap.CompressFormat.JPEG, 90);
                encoded = String.format("data:image/jpeg;base64,%s", encoded);
                params.put("logo", encoded);
            }else {
                params.put("logo", "");
            }
            ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.UPDATE_SCHOOL_INFO, checkinclassapiCallback, this, params);
        }
    }
    ApiHandler.ApiCallback checkinclassapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            commonProgress.dismiss();
            if (error == null) {
                Toast.makeText(SchoolInfoActivity.this,"Update Successfully!",Toast.LENGTH_LONG).show();
                finish();
            }
            else {
                Toast.makeText(SchoolInfoActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();

            }
        }
    };

    @OnClick(R.id.contact_support)
    public void contact_supportttt() {
        startActivity(new Intent(SchoolInfoActivity.this,LocationActivity.class));
    }


    @OnClick(R.id.imgBack)
    public void callBackF() {
        commonBack();
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        commonBack();
    }

    private void commonBack() {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(SchoolInfoActivity.this);
        builder1.setMessage("Are you sure, you want to exit? ");
        builder1.setCancelable(false);
        builder1.setPositiveButton(
                "Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finish();
                    }
                });
        builder1.setNegativeButton(
                "No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

//    @Override
//    protected void onResume() {
//        super.onResume();
//        userprofile();
//    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @OnClick(R.id.clickimage)
    public void click() {
        // type = 1;
        pick_img();
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @SuppressLint("ResourceType")
    private void pick_img() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.UPSIDE_DOWN_CAKE) { // Android 14+ (API 34)
          onImageViewClick();
        } else if (Build.VERSION.SDK_INT > Build.VERSION_CODES.S) {
                if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_MEDIA_IMAGES)
                        != PackageManager.PERMISSION_GRANTED) {

                    if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_MEDIA_IMAGES)) {
                        Toast.makeText(this, "Please provide storage permission from app settings", Toast.LENGTH_LONG).show();
                    } else {

                        ActivityCompat.requestPermissions(this,
                                new String[]{Manifest.permission.READ_MEDIA_IMAGES},
                                1);
                    }
                } else {

                    onImageViewClick();
                }

            } else {
                if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {

                    if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                        Toast.makeText(this, "Please provide storage permission from app settings", Toast.LENGTH_LONG).show();
                    } else {

                        ActivityCompat.requestPermissions(this,
                                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                                1);
                    }
                } else {
                    onImageViewClick();

                }
        }
    }

    private void onImageViewClick() {
        // type = 1;
        PickSetup setup = new PickSetup();
        // super.customize(setup);

        PickImageDialog.build(setup)
                //.setOnClick(this)
                .show(this);
    }

    @Override
    public void onPickResult(PickResult r) {
        if (r.getError() == null) {
            //If you want the Bitmap.
            chooseimage.setImageBitmap(r.getBitmap());
            userBitmap= r.getBitmap();

        } else {
            //Handle possible errors
            //TODO: do what you have to do with r.getError();
            Toast.makeText(this, r.getError().getMessage(), Toast.LENGTH_LONG).show();
        }


    }


    public static boolean checkPermission(Context mContext, String Permission) {
        int result = ContextCompat.checkSelfPermission(mContext, Permission);
        return result == PackageManager.PERMISSION_GRANTED;
    }



//    @Override
//    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        if (resultCode == Activity.RESULT_OK) {
//            switch (requestCode) {
//                case CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE:
//                    Uri imageUri = CropImage.getPickImageResultUri(SchoolInfoActivity.this, data);
//                    //  if (type == 1) {
//                    CropImage.activity(imageUri).setCropShape(CropImageView.CropShape.RECTANGLE).setAspectRatio(1, 1)
//                            .start(SchoolInfoActivity.this);
//                    //   }
//
//                    break;
//
//                case CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE:
//                    CropImage.ActivityResult result = CropImage.getActivityResult(data);
//                    Uri resultUri = result.getUri();
//                    userBitmap = Utils.getBitmapFromUri(SchoolInfoActivity.this, resultUri, 2048);
//                    chooseimage.setImageBitmap(userBitmap);
//
//            }
//        }
//    }

    private void userprofile() {
        // UltimateProgress.showProgressBar(getActivity(),"Please Wait........!");
        if(loded==0) {
            commonProgress.show();
        }
        loded++;
        HashMap<String, String> params = new HashMap<>();
        params.put("user_id", User.getCurrentUser().getId());
        params.put("check", "fetch");


        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.UPDATE_SCHOOL_INFO, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error ) {
                commonProgress.dismiss();
                ErpProgress.cancelProgressBar();
                if (error == null) {
                    try {

                        image_url=jsonObject.getJSONObject(Constants.USERDATA).getString("logo");
                        school=jsonObject.getJSONObject(Constants.USERDATA).getString("name");
                        phone=jsonObject.getJSONObject(Constants.USERDATA).getString("phoneno");
                        address1=jsonObject.getJSONObject(Constants.USERDATA).getString("address");
                        email1=jsonObject.getJSONObject(Constants.USERDATA).getString("email");
                        website=jsonObject.getJSONObject(Constants.USERDATA).getString("website");

                        fb_url = jsonObject.getJSONObject(Constants.USERDATA).getString("fb");
                        insta_url = jsonObject.getJSONObject(Constants.USERDATA).getString("insta");
                        youtube_url = jsonObject.getJSONObject(Constants.USERDATA).getString("youtube");

                        latitude=jsonObject.getJSONObject(Constants.USERDATA).getString("latitude");
                        longitude=jsonObject.getJSONObject(Constants.USERDATA).getString("longitude");

                        if (userBitmap == null) {
                            if (!image_url.equalsIgnoreCase("https://ultimatesolutiongroup.com/office_admin/images/school_logo/")) {
                                Picasso.get().load(image_url).placeholder(R.drawable.boy).into(chooseimage);
                            } else {
                                Picasso.get().load(R.drawable.logo).into(chooseimage);
                            }

                            face_book.setText(fb_url);
                            insta_gram.setText(insta_url);
                            you_tube.setText(youtube_url);

                        name.setText(school);
                        phonenumber.setText(phone);
                        email.setText(email1);
                        address.setText(address1);
                        website_url.setText(website);

                        if (!latitude.equalsIgnoreCase("0") && !longitude.equalsIgnoreCase("0")){
                            if (!restorePrefData()){
                                setShowcaseView();
                            }
                            lat= Double.parseDouble(latitude);
                            lng= Double.parseDouble(longitude);
                            mapFragment.getMapAsync(SchoolInfoActivity.this);
                        }else{
                            mapFragment.getMapAsync(SchoolInfoActivity.this);
                            if (!restorePrefData()){
                                setShowcaseView();
                            }else{
                                if (count==0) {
                                    count++;
                                    openDialog();
                                }
                            }

                        }

                        }


                        // Utils.setNaviHeaderDatas(stu_name,class_namessss,navi_profile,image_url,getActivity());
                       // Utils.setNaviHeaderDatas(admin_name,post,navi_profile,image_url,getActivity());

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }, this, params);
    }

    private void openDialog() {
        BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(this);
        final View sheetView = getLayoutInflater().inflate(R.layout.location_dialog, null);
        mBottomSheetDialog.setContentView(sheetView);
        mBottomSheetDialog.setCancelable(false);
        final boolean[] loadingFinished = {true};
        final boolean[] redirect = {false};
        TextView search_location = (TextView) sheetView.findViewById(R.id.search_location);

        search_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(SchoolInfoActivity.this,LocationActivity.class));
                mBottomSheetDialog.dismiss();
            }
        });

        RelativeLayout btnNo = (RelativeLayout) sheetView.findViewById(R.id.btnNo);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBottomSheetDialog.dismiss();
            }
        });
        mBottomSheetDialog.show();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;

        latLng =new LatLng(lat,lng);

        String tittle="";


        tittle=name.getText().toString();

        if (!latitude.equalsIgnoreCase("0") && !longitude.equalsIgnoreCase("0")){

            Marker marker=map.addMarker(new MarkerOptions().position(latLng).title(tittle));
            marker.showInfoWindow();
            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(
                    latLng, 15F);
            map.animateCamera(cameraUpdate,300,null);
            //   map.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng,17F));
            map.setMaxZoomPreference(18);
            map.setMinZoomPreference(10);
           // map.isBuildingsEnabled();
        } else {
            Marker marker=map.addMarker(new MarkerOptions().position(latLng).title("School Location not found!"));
            marker.showInfoWindow();
            map.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng,17F));
            map.setMaxZoomPreference(17);
            map.setMinZoomPreference(17);
        }

    }
}