package com.ultimate.ultimatesmartschool.Activity;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.provider.Settings;
import android.text.Html;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.skyfishjy.library.RippleBackground;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.CommonProgress;
import com.ultimate.ultimatesmartschool.Utility.Constants;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LocationActivity extends AppCompatActivity implements OnMapReadyCallback {


    GoogleMap mGoogleMap;
    SupportMapFragment mapFrag;
    LocationRequest mLocationRequest;
    Location mLastLocation;
    Marker mCurrLocationMarker;
    FusedLocationProviderClient mFusedLocationClient;
    private RippleBackground rippleBg;
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.title10)
    TextView title10;

    @BindView(R.id.head)
    RelativeLayout head;
    @BindView(R.id.confirm_head) RelativeLayout confirm_head;
    @BindView(R.id.add) TextView add;
    String address,area,city,state,pincode;
    int count=0;
    private Animation animation,animation1;
    String longtude,lattude;

    BottomSheetDialog mBottomSheetDialog;
    EditText edtName,edtAddress,edtPhone;
    RelativeLayout veryfy_no;
    Button submitted;
    TextInputLayout inpPhone;

    CommonProgress commonProgress;

    // mobile number verifications
    private static final int RC_SIGN_IN = 123;
    private int VERIFY_NUMBER = 1000;
    FirebaseAuth mAuth;
    String get_number="";
    @BindView(R.id.search_location)
    TextView search_location;
    SharedPreferences sharedPreferences;
    //preferences for device token
    SharedPreferences pref;
    private String deviceid;
    private int deviceIdChecked = 0;
    private JSONObject user_obj;

    @BindView(R.id.get_location)
    TextView get_location;
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location);
        ButterKnife.bind(this);
        commonProgress=new CommonProgress(this);
        transparentfullscreen();
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        rippleBg = findViewById(R.id.ripple_bg);
        animation = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.long_fade_animation);
        animation1 = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.gone_fade_animation);
        mapFrag = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFrag.getMapAsync(this);
    }
    public void CheckGpsStatus(){

        LocationManager locationManager ;
        boolean GpsStatus;
        locationManager = (LocationManager)this.getSystemService(Context.LOCATION_SERVICE);
        assert locationManager != null;
        GpsStatus = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        if(GpsStatus == true) {
            //   location_text.setText("GPS Is Enabled");
            Toast.makeText(LocationActivity.this, "GPS Is Enabled", Toast.LENGTH_SHORT).show();

        } else {
            //  location_text.setText("GPS Is Disabled");
            //    Toast.makeText(LocationActivity.this, "Error to fetch location, kindly turn on your GPS First", Toast.LENGTH_SHORT).show();
            DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch (which) {
                        case DialogInterface.BUTTON_POSITIVE:
                            Intent intent1 = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                            startActivity(intent1);

                            // finish();
                            break;

                        case DialogInterface.BUTTON_NEGATIVE:
                            //No button clicked
                            Toast.makeText(LocationActivity.this, "Error to fetch location, Try again later.", Toast.LENGTH_SHORT).show();
                            finish();
                            break;
                    }
                }
            };
            AlertDialog.Builder builder = new AlertDialog.Builder(LocationActivity.this);
            builder.setCancelable(false).setMessage("Kindly, Enable Your GPS First!").setPositiveButton("Yes", dialogClickListener)
                    .setNegativeButton("No", dialogClickListener).show();

        }
    }

    @Override
    protected void onResume() {
        super.onResume();
       // if (User.getCurrentUser()!=null) {
            CheckGpsStatus();
       // }
    }



    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void transparentfullscreen() {

        Window window=this.getWindow();
        window.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        window.setStatusBarColor(Color.TRANSPARENT);
    }

    @OnClick(R.id.search_location)
    public void search_locationssss() {

        search_location.startAnimation(animation);
        confirm_head.startAnimation(animation1);
        confirm_head.setVisibility(View.GONE);
        count=1;
        commonProgress.show();
     //   mapFrag.getMapAsync(this);
        //Toast.makeText(getApplicationContext(),"address"+address,Toast.LENGTH_SHORT).show();
      //  CheckGpsStatus();
    }

    @OnClick(R.id.get_location)
    public void get_locationsss() {
        get_location.startAnimation(animation);
        updateLocation();
    }

    private void updateLocation() {
        commonProgress.show();
        HashMap<String, String> params = new HashMap<>();
        params.put("check","lat_lng_update" );
        params.put("fi_lat", lattude);
        params.put("fi_lng", longtude);

        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.UPDATE_SCHOOL_INFO, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error ) {
              commonProgress.dismiss();
                if (error == null) {
                        Toast.makeText(LocationActivity.this, "Location Updated!", Toast.LENGTH_SHORT).show();
                        finish();
                } else {
                    Toast.makeText(LocationActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }, this, params);
    }


    @Override
    public void onPause() {
        super.onPause();
        //stop location updates when Activity is no longer active
        if (mFusedLocationClient != null) {
            mFusedLocationClient.removeLocationUpdates(mLocationCallback);
        }
    }

    private boolean restorePrefData(){
        sharedPreferences=getApplicationContext().getSharedPreferences("location_pref",MODE_PRIVATE);
        return sharedPreferences.getBoolean("IsFirstTimeVisit",false);
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
//        if (count==1) {
//           commonProgress.show();
//        }
        mGoogleMap = googleMap;
        mLocationRequest = new LocationRequest();
//        mLocationRequest.setInterval(10000); // two minute interval
//        mLocationRequest.setFastestInterval(10000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                //Location Permission already granted
                mFusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback,
                        Looper.myLooper());
                mGoogleMap.setMyLocationEnabled(true);


                mGoogleMap.setOnMyLocationChangeListener(new GoogleMap.OnMyLocationChangeListener() {
                    @Override
                    public void onMyLocationChange(Location location) {
                        LatLng ltlng=new LatLng(location.getLatitude(),location.getLongitude());
                        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(
                                ltlng, 16F);
                        mGoogleMap.animateCamera(cameraUpdate);
                        String fetch= getAddress(ltlng);
                        // Toast.makeText(getApplicationContext(),"Current address"+fetch,Toast.LENGTH_SHORT).show();
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                rippleBg.setVisibility(View.VISIBLE);
                                rippleBg.startRippleAnimation();
                            }
                        }, 2000);

                        String title1 = getColoredSpanned("Latitude: ", "#5A5C59");
                        String name1 = getColoredSpanned(lattude, "#f85164");

                        String title2 = getColoredSpanned("Longitude: ", "#5A5C59");
                        String name2 = getColoredSpanned(longtude, "#f85164");

                        title.setText(Html.fromHtml(title1 + name1));
                        title10.setText(Html.fromHtml(title2 + name2));

                        add.setText(fetch);
                        if (count==1) {
                            commonProgress.dismiss();
                            count++;
                            head.setVisibility(View.VISIBLE);
                            head.startAnimation(animation);
                        }

                    }
                });

                //  Location location = mGoogleMap.getMyLocation();
//                mGoogleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
//                    @Override
//                    public void onMapClick(LatLng latLng) {
//
//                        MarkerOptions markerOptions = new MarkerOptions();
//                        markerOptions.position(latLng);
//                        //for adding custom marker....
////                        Marker marker;
////                        marker= mGoogleMap.addMarker(markerOptions.title(getAddress(latLng))
////                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.marker)).draggable(true).visible(true));
////                        marker.isInfoWindowShown();
//                        markerOptions.title(getAddress(latLng));
//                        mGoogleMap.clear();
//                        CameraUpdate location = CameraUpdateFactory.newLatLngZoom(
//                                latLng, 19F);
//                        mGoogleMap.animateCamera(location);
//                        mGoogleMap.addMarker(markerOptions);
//                    }
//                });

            } else {
                //Request Location Permission
                checkLocationPermission();
            }
        } else {
            mFusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback,
                    Looper.myLooper());
            mGoogleMap.setMyLocationEnabled(true);
        }
    }

    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }

    private String getAddress(LatLng latLng) {
        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(LocationActivity.this, Locale.getDefault());
        try {
            addresses = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            lattude= String.valueOf(latLng.latitude);
            longtude= String.valueOf(latLng.longitude);
            address = addresses.get(0).getAddressLine(0);
            area=addresses.get(0).getSubLocality();
            city=addresses.get(0).getLocality();
            state=addresses.get(0).getAdminArea();
            pincode=addresses.get(0).getPostalCode();

            //  Toast.makeText(getApplicationContext(),"address "+city+ "\n" +state+ "\n" +pincode,Toast.LENGTH_SHORT).show();
            return address;
        } catch (IOException e) {
            e.printStackTrace();
            Toast.makeText(getApplicationContext(),"error"+e.getMessage(),Toast.LENGTH_SHORT).show();
            return e.getMessage();
        }

    }

    LocationCallback mLocationCallback = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult locationResult) {
            List<Location> locationList = locationResult.getLocations();
            if (locationList.size() > 0) {
                //The last location in the list is the newest
                Location location = locationList.get(locationList.size() - 1);
                mLastLocation = location;
                if (mCurrLocationMarker != null) {
                    mCurrLocationMarker.remove();
                }

                //move map camera
                LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
                CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(latLng.latitude, latLng.longitude)).zoom(16).build();
                mGoogleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            }
        }
    };

    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    private void checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                new AlertDialog.Builder(this)
                        .setTitle("Location Permission Needed")
                        .setMessage("This app needs the Location permission, please accept to use location functionality")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //Prompt the user once explanation has been shown
                                ActivityCompat.requestPermissions(LocationActivity.this,
                                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                        MY_PERMISSIONS_REQUEST_LOCATION );
                            }
                        })
                        .create()
                        .show();


            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION );
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // location-related task you need to do.
                    if (ContextCompat.checkSelfPermission(this,
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {

                        mFusedLocationClient.requestLocationUpdates(mLocationRequest,
                                mLocationCallback, Looper.myLooper());
                        mGoogleMap.setMyLocationEnabled(true);

                    }

                } else {
                    // if not allow a permission, the application will exit
                    Intent intent = new Intent(Intent.ACTION_MAIN);
                    intent.addCategory(Intent.CATEGORY_HOME);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();
                    System.exit(0);
                }
            }
        }
    }


    @OnClick(R.id.imgBack)
    public void callBackF() {
        commonBack();
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        commonBack();
    }

    private void commonBack() {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(LocationActivity.this);
        builder1.setMessage("Are you sure, you want to exit? ");
        builder1.setCancelable(false);
        builder1.setPositiveButton(
                "Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finish();
                    }
                });
        builder1.setNegativeButton(
                "No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

}