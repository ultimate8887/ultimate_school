package com.ultimate.ultimatesmartschool.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetSequence;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.squareup.picasso.Picasso;
import com.ultimate.ultimatesmartschool.BeanModule.User;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.CommonProgress;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SchoolInfo extends AppCompatActivity implements OnMapReadyCallback {

    CommonProgress commonProgress;
    @BindView(R.id.open_google_map)
    TextView open_google_map;
    // map view data
    private GoogleMap map;
    private SupportMapFragment mapFragment;
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 1000;
    private Marker myMarker = null;
    double lat, lng;
    Handler handler = new Handler();
    Geocoder geo;
    List<Address> addresses = null;
    StringBuilder  str = null;
    LatLng latLng;
    String image_url="",school="",phone="",address1="",website="",email1="",latitude="",longitude="";

    @BindView(R.id.txtHead)
    TextView txtHead;
    @BindView(R.id.c_name)
    TextView c_name;
    @BindView(R.id.txtAdd)
    TextView txtAdd;
    @BindView(R.id.website)
    FloatingActionButton websiteee;
    @BindView(R.id.email)
    FloatingActionButton email;
    @BindView(R.id.phone)
    FloatingActionButton phoness;


    @BindView(R.id.youtube)
    FloatingActionButton youtube;
    @BindView(R.id.instagram)
    FloatingActionButton instagram;
    @BindView(R.id.facebook)
    FloatingActionButton facebook;

    String fb_url="",insta_url="",youtube_url="",review_url="";

    @BindView(R.id.contact_support)
    ImageView contact_support;

    @BindView(R.id.txtTitle)
    TextView txtTitle;
    SharedPreferences sharedPreferences;
    @BindView(R.id.circleimgrecepone)
    CircularImageView chooseimage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_school_info2);

        ButterKnife.bind(this);
        txtTitle.setText("School Details");
        commonProgress = new CommonProgress(this);
        geo = new Geocoder(getApplicationContext(), Locale.getDefault());
        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map_location);
        if (!restorePrefData()){
            setShowcaseView();
        }else{
            if (!restorePrefDataSocial()){
                setShowcaseViewSocial();
            }
        }

        userprofile();
        RelativeLayout btnNo= (RelativeLayout) findViewById(R.id.btnNo);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              finish();
            }
        });
    }

    private void setShowcaseViewSocial() {
        new TapTargetSequence(this)
                .targets(
                        TapTarget.forView(youtube,"Youtube Icon",getString(R.string.yout))
                                .outerCircleColor(R.color.light)
                                .outerCircleAlpha(0.96f)
                                .targetCircleColor(R.color.white)
                                .titleTextSize(20)
                                .titleTextColor(R.color.white)
                                .descriptionTextSize(14)
                                .descriptionTextColor(R.color.black)
                                .textColor(R.color.black)
                                .textTypeface(Typeface.SANS_SERIF)
                                .dimColor(R.color.black)
                                .drawShadow(true)
                                .cancelable(false)
                                .tintTarget(true)
                                .transparentTarget(true)
                                .targetRadius(40),
                        TapTarget.forView(facebook,"Facebook Icon",getString(R.string.fb))
                                .outerCircleColor(R.color.light)
                                .outerCircleAlpha(0.96f)
                                .targetCircleColor(R.color.white)
                                .titleTextSize(20)
                                .titleTextColor(R.color.white)
                                .descriptionTextSize(14)
                                .descriptionTextColor(R.color.black)
                                .textColor(R.color.black)
                                .textTypeface(Typeface.SANS_SERIF)
                                .dimColor(R.color.black)
                                .drawShadow(true)
                                .cancelable(false)
                                .tintTarget(true)
                                .transparentTarget(true)
                                .targetRadius(40),
                        TapTarget.forView(instagram,"Instagram Icon",getString(R.string.insta))
                                .outerCircleColor(R.color.light)
                                .outerCircleAlpha(0.96f)
                                .targetCircleColor(R.color.white)
                                .titleTextSize(20)
                                .titleTextColor(R.color.white)
                                .descriptionTextSize(14)
                                .descriptionTextColor(R.color.black)
                                .textColor(R.color.black)
                                .textTypeface(Typeface.SANS_SERIF)
                                .dimColor(R.color.black)
                                .drawShadow(true)
                                .cancelable(false)
                                .tintTarget(true)
                                .transparentTarget(true)
                                .targetRadius(40)).listener(new TapTargetSequence.Listener() {
                    @Override
                    public void onSequenceFinish() {
                        //Toast.makeText(getActivity(),"Sequence Finished",Toast.LENGTH_SHORT).show();
                        savePrefDataSocial();

                    }

                    @Override
                    public void onSequenceStep(TapTarget lastTarget, boolean targetClicked) {

                        // Toast.makeText(getActivity(),"GREAT!",Toast.LENGTH_SHORT).show();

                    }

                    @Override
                    public void onSequenceCanceled(TapTarget lastTarget) {

                    }
                }).start();
    }

    private void savePrefDataSocial(){
        sharedPreferences=getApplicationContext().getSharedPreferences("boarding_pref_SchoolInfo_Social",MODE_PRIVATE);
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putBoolean("IsFirstTimeVisit_SchoolInfo_Social",true);
        editor.apply();
    }

    private boolean restorePrefDataSocial(){
        sharedPreferences = getApplicationContext().getSharedPreferences("boarding_pref_SchoolInfo_Social",MODE_PRIVATE);
        return sharedPreferences.getBoolean("IsFirstTimeVisit_SchoolInfo_Social",false);
    }

    private void setShowcaseView() {

        new TapTargetSequence(this)
                .targets(
                        TapTarget.forView(contact_support,"Share Button","Tap the Share Button to share School details with Google Map Location")
                                .outerCircleColor(R.color.light)
                                .outerCircleAlpha(0.96f)
                                .targetCircleColor(R.color.white)
                                .titleTextSize(20)
                                .titleTextColor(R.color.white)
                                .descriptionTextSize(14)
                                .descriptionTextColor(R.color.black)
                                .textColor(R.color.black)
                                .textTypeface(Typeface.SANS_SERIF)
                                .dimColor(R.color.black)
                                .drawShadow(true)
                                .cancelable(false)
                                .tintTarget(true)
                                .transparentTarget(true)
                                .targetRadius(60),
                        TapTarget.forView(websiteee,"Website Button","Tap the Website button to view School Website")
                                .outerCircleColor(R.color.light)
                                .outerCircleAlpha(0.96f)
                                .targetCircleColor(R.color.white)
                                .titleTextSize(20)
                                .titleTextColor(R.color.white)
                                .descriptionTextSize(14)
                                .descriptionTextColor(R.color.black)
                                .textColor(R.color.black)
                                .textTypeface(Typeface.SANS_SERIF)
                                .dimColor(R.color.black)
                                .drawShadow(true)
                                .cancelable(false)
                                .tintTarget(true)
                                .transparentTarget(true)
                                .targetRadius(60),
                        TapTarget.forView(email,"Email Button","Tap the Email button for send direct email to School")
                                .outerCircleColor(R.color.light)
                                .outerCircleAlpha(0.96f)
                                .targetCircleColor(R.color.white)
                                .titleTextSize(20)
                                .titleTextColor(R.color.white)
                                .descriptionTextSize(14)
                                .descriptionTextColor(R.color.black)
                                .textColor(R.color.black)
                                .textTypeface(Typeface.SANS_SERIF)
                                .dimColor(R.color.black)
                                .drawShadow(true)
                                .cancelable(false)
                                .tintTarget(true)
                                .transparentTarget(true)
                                .targetRadius(60),
                        TapTarget.forView(phoness,"Call Button","Tap the Call button for direct calling to School.")
                                .outerCircleColor(R.color.light)
                                .outerCircleAlpha(0.96f)
                                .targetCircleColor(R.color.white)
                                .titleTextSize(20)
                                .titleTextColor(R.color.white)
                                .descriptionTextSize(14)
                                .descriptionTextColor(R.color.black)
                                .textColor(R.color.black)
                                .textTypeface(Typeface.SANS_SERIF)
                                .dimColor(R.color.black)
                                .drawShadow(true)
                                .cancelable(false)
                                .tintTarget(true)
                                .transparentTarget(true)
                                .targetRadius(60)).listener(new TapTargetSequence.Listener() {
            @Override
            public void onSequenceFinish() {
                //Toast.makeText(getActivity(),"Sequence Finished",Toast.LENGTH_SHORT).show();
                savePrefData();
            }

            @Override
            public void onSequenceStep(TapTarget lastTarget, boolean targetClicked) {

                // Toast.makeText(getActivity(),"GREAT!",Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onSequenceCanceled(TapTarget lastTarget) {

            }
        }).start();
    }

    private void savePrefData(){
        sharedPreferences=getApplicationContext().getSharedPreferences("boarding_pref_SchoolInfo",MODE_PRIVATE);
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putBoolean("IsFirstTimeVisit_SchoolInfo",true);
        editor.apply();
    }

    private boolean restorePrefData(){
        sharedPreferences = getApplicationContext().getSharedPreferences("boarding_pref_SchoolInfo",MODE_PRIVATE);
        return sharedPreferences.getBoolean("IsFirstTimeVisit_SchoolInfo",false);
    }

    @OnClick(R.id.imgBack)
    public void callBackF() {
        finish();
    }

    @OnClick(R.id.contact_support)
    public void share() {
       // contact_support.startAnimation(animation);

//            lat = Double.parseDouble(data.getLatitude());
//            lng= Double.parseDouble(data.getLongitude());
            Log.e("latlong",lat+ "   "+lng);

            String uri = "https://www.google.com/maps/?q=" + lat + "," +lng ;

            String ShareSub="";


          if (!latitude.equalsIgnoreCase("0") && !longitude.equalsIgnoreCase("0")) {

            if (!website.equalsIgnoreCase("") || website!=null ) {
                 ShareSub ="\nSchool Details\n"+school+"\n\nPhone: "+phone+"\nAddress: "+address1 +"\nWebsite: "+website +"\n\nLocation Link: "+uri+"";
            }else{
            ShareSub ="\nSchool Details\n"+school+"\n\nPhone: "+phone+"\nAddress: "+address1 +"\n\nLocation Link: "+uri+"";
              }

          }else{
              if (!website.equalsIgnoreCase("")) {
                  ShareSub ="\nSchool Details\n"+school+"\n\nPhone: "+phone+"\nAddress: "+address1 +"\nWebsite: "+website +"\n\nLocation Link not found";
              }else{
                  ShareSub ="\nSchool Details\n"+school+"\n\nPhone: "+phone+"\nAddress: "+address1 +"\n\nLocation Link not found";
              }
          }

            Log.e("urls",uri);
            Log.e("ShareSub",ShareSub);

            Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);
            shareIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            shareIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, ShareSub);


            shareIntent.putExtra(android.content.Intent.EXTRA_TEXT, ShareSub);
//
            shareIntent.setType("text/plain");
            startActivity(Intent.createChooser(shareIntent,"Share with"));


            shareIntent = new Intent(android.content.Intent.ACTION_SEND);
            shareIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            shareIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, ShareSub);
            shareIntent.putExtra(android.content.Intent.EXTRA_TEXT,  ShareSub);
            shareIntent.setType("text/plain");
            startActivity(Intent.createChooser(shareIntent,"Share with"));





    }


    @OnClick(R.id.open_google_map)
    public void open_google_map() {
      //  open_google_map.startAnimation(animation);
        String uri = String.format(Locale.ENGLISH, "http://maps.google.com/maps?q=loc:%f,%f", lat,lng);
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
        startActivity(intent);
      //  Animatoo.animateShrink(this);
    }

    private void userprofile() {
        // UltimateProgress.showProgressBar(getActivity(),"Please Wait........!");
        commonProgress.show();
        HashMap<String, String> params = new HashMap<>();
        params.put("user_id", User.getCurrentUser().getId());
        params.put("check", "fetch");


        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.UPDATE_SCHOOL_INFO, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error ) {
                commonProgress.dismiss();
                ErpProgress.cancelProgressBar();
                if (error == null) {
                    try {

                        image_url = jsonObject.getJSONObject(Constants.USERDATA).getString("logo");
                        school = jsonObject.getJSONObject(Constants.USERDATA).getString("name");
                        phone = jsonObject.getJSONObject(Constants.USERDATA).getString("phoneno");
                        address1 = jsonObject.getJSONObject(Constants.USERDATA).getString("address");
                        email1 = jsonObject.getJSONObject(Constants.USERDATA).getString("email");
                        website = jsonObject.getJSONObject(Constants.USERDATA).getString("website");

                        fb_url = jsonObject.getJSONObject(Constants.USERDATA).getString("fb");
                        insta_url = jsonObject.getJSONObject(Constants.USERDATA).getString("insta");
                        youtube_url = jsonObject.getJSONObject(Constants.USERDATA).getString("youtube");

                        latitude = jsonObject.getJSONObject(Constants.USERDATA).getString("latitude");
                        longitude = jsonObject.getJSONObject(Constants.USERDATA).getString("longitude");

                        if (!image_url.equalsIgnoreCase("https://ultimatesolutiongroup.com/office_admin/images/school_logo/")) {
                            Picasso.get().load(image_url).placeholder(R.drawable.boy).into(chooseimage);
                        } else {
                            Picasso.get().load(R.drawable.logo).into(chooseimage);
                        }


                        c_name.setText(school);
                      //  phonenumber.setText(phone);
                      //  email.setText(email1);
                        txtAdd.setText(address1);
                      //  website_url.setText(website);



                        if (latitude.equalsIgnoreCase("0") && longitude.equalsIgnoreCase("0")) {
                            open_google_map.setVisibility(View.GONE);
                        }else{
                            open_google_map.setVisibility(View.VISIBLE);
                        }

                        facebook.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                if (!fb_url.equalsIgnoreCase("")) {
                                    String url = fb_url;
                                    Intent i = new Intent(Intent.ACTION_VIEW);
                                    i.setData(Uri.parse(url));
                                    startActivity(i);

                                }else{
                                    Toast.makeText(getApplicationContext(),"Facebook Page Not Found!",Toast.LENGTH_LONG).show();
                                }
                            }
                        });

                        youtube.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                if (!youtube_url.equalsIgnoreCase("")) {
                                    String url = youtube_url;
                                    Intent i = new Intent(Intent.ACTION_VIEW);
                                    i.setData(Uri.parse(url));
                                    startActivity(i);

                                }else{
                                    Toast.makeText(getApplicationContext(),"Youtube Channel Not Found!",Toast.LENGTH_LONG).show();
                                }
                            }
                        });

                        instagram.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                if (!insta_url.equalsIgnoreCase("")) {
                                    String url = insta_url;
                                    Intent i = new Intent(Intent.ACTION_VIEW);
                                    i.setData(Uri.parse(url));
                                    startActivity(i);

                                }else{
                                    Toast.makeText(getApplicationContext(),"Instagram Page Not Found!",Toast.LENGTH_LONG).show();
                                }
                            }
                        });


                        email.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                if (!email1.equalsIgnoreCase("") || email1!=null) {
                                    //  mBottomSheetDialog.dismiss();
                                    //  UltimateProgress.showProgressBar(getActivity(), "Please Wait.....!");
                                    Intent intent = new Intent(Intent.ACTION_SENDTO); // it's not ACTION_SEND
                                    intent.setType("text/plain");
                                    intent.putExtra(Intent.EXTRA_SUBJECT, "Subject of email");
                                    intent.putExtra(Intent.EXTRA_TEXT, "Body of email");
                                    //simplydirect email for setting email,,,
                                    intent.setData(Uri.parse("mailto:" + email1)); // or just "mailto:" for blank
                                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK); // this will make such that when user returns to your app, your app is displayed, instead of the email app.
                                    startActivity(intent);
                                    //   UltimateProgress.cancelProgressBar();
                                }else{
                                    Toast.makeText(getApplicationContext(),"Email Not Found!",Toast.LENGTH_LONG).show();
                                }
                            }
                        });



                        websiteee.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                if (!website.equalsIgnoreCase("")) {
                                    String url = website;
                                    Intent i = new Intent(Intent.ACTION_VIEW);
                                    i.setData(Uri.parse(url));
                                    startActivity(i);

                                }else{
                                    Toast.makeText(getApplicationContext(),"Website Not Found!",Toast.LENGTH_LONG).show();
                                }
                            }
                        });




                        phoness.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                if ( !phone.equalsIgnoreCase("") || phone!=null ) {
                                    // mBottomSheetDialog.dismiss();
                                    // for permission granted....
                                    if (ActivityCompat.checkSelfPermission(SchoolInfo.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                                        ActivityCompat.requestPermissions(SchoolInfo.this, new String[]{Manifest.permission.CALL_PHONE},1);
                                    } else {
                                        Intent intent = new Intent(Intent.ACTION_CALL);
                                        intent.setData(Uri.parse("tel:" + phone));
                                        startActivity(intent);
                                        //   UltimateProgress.cancelProgressBar();
                                    }
                                }else{
                                    Toast.makeText(getApplicationContext(),"Number Not Found!",Toast.LENGTH_LONG).show();
                                }

                            }

                        });


                        lat = Double.parseDouble(latitude);
                        lng = Double.parseDouble(longitude);
                        mapFragment.getMapAsync(SchoolInfo.this);


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }, this, params);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;

        latLng =new LatLng(lat,lng);

        String tittle="";


        tittle=school;

        if (!latitude.equalsIgnoreCase("0") && !longitude.equalsIgnoreCase("0")){

            Marker marker=map.addMarker(new MarkerOptions().position(latLng).title(tittle));
            marker.showInfoWindow();
            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(
                    latLng, 15F);
            map.animateCamera(cameraUpdate,300,null);
            //   map.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng,17F));
            map.setMaxZoomPreference(18);
            map.setMinZoomPreference(10);
            // map.isBuildingsEnabled();
        } else {
            Marker marker=map.addMarker(new MarkerOptions().position(latLng).title("School Location not found!"));
            marker.showInfoWindow();
            map.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng,17F));
            map.setMaxZoomPreference(17);
            map.setMinZoomPreference(17);
        }


    }
}