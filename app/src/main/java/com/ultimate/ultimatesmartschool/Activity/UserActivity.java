package com.ultimate.ultimatesmartschool.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SearchView;
import android.widget.TextView;

import com.android.volley.Request;
import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetSequence;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.gson.Gson;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.CommonProgress;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class UserActivity extends AppCompatActivity implements UserAdapter.StudentPro {

    RecyclerView NewsRecyclerview;

    SharedPreferences sharedPreferences;
    boolean isDark = false;
    RelativeLayout rootLayout;
    SearchView searchInput ;
    CharSequence search="";
    ArrayList<User_Permission> mData = new ArrayList<>();
    @BindView(R.id.textNorecord)
    TextView txtNorecord;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.totalRecord)
    TextView totalRecord;
    @BindView(R.id.contact_support)
    ImageView contact_support;
    RecyclerView.LayoutManager layoutManager;
    private UserAdapter newsAdapter;
    private int loaded = 0;
    String classid = "",g_id = "",b_id = "";
    String staff_id="";
    BottomSheetDialog mBottomSheetDialog;
    CommonProgress commonProgress;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);
        ButterKnife.bind(this);
        commonProgress=new CommonProgress(this);
        txtTitle.setText("User List");
        rootLayout = findViewById(R.id.root_layout);
        searchInput = findViewById(R.id.searchView);
        NewsRecyclerview = findViewById(R.id.recyclerView11);
        //  searchInput.setBackgroundResource(R.drawable.search_input_dark_style);
        rootLayout.setBackgroundColor(getResources().getColor(R.color.white));


        layoutManager = new LinearLayoutManager(UserActivity.this);
        NewsRecyclerview.setLayoutManager(layoutManager);
        // newsAdapter = new NewsAdapter(this,mData);
        newsAdapter = new UserAdapter(mData,UserActivity.this,this);
        NewsRecyclerview.setAdapter(newsAdapter);


        if (!restorePrefData()){
            setShowcaseView();
        }

        isDark = !isDark ;
        if (isDark) {

            rootLayout.setBackgroundColor(getResources().getColor(R.color.white));
            //      searchInput.setBackgroundResource(R.drawable.search_input_dark_style);

        }
        else {
            rootLayout.setBackgroundColor(getResources().getColor(R.color.white));
            //          searchInput.setBackgroundResource(R.drawable.search_input_style);
        }
        if (!search.toString().isEmpty()){

            newsAdapter.getFilter().filter(search);

        }

        searchInput.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }
            @Override
            public boolean onQueryTextChange(String query) {
                //FILTER AS YOU TYPE

                if (query.length()==0){
                   // totalRecord.setText("Total Entries:- "+String.valueOf(loaded));
                    fetchStudent();
                }else {
                    totalRecord.setText("Search Entries:- " +String.valueOf(newsAdapter.stulist.size()));
                }
                newsAdapter.getFilter().filter(query);

                return false;
            }
        });


    }

    @Override
    protected void onResume() {
        super.onResume();
        fetchStudent();
    }

    private void savePrefData(){
        sharedPreferences=UserActivity.this.getSharedPreferences("boarding_pref_user",MODE_PRIVATE);
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putBoolean("IsFirstTimeVisit_user",true);
        editor.apply();
    }

    private boolean restorePrefData(){
        sharedPreferences = UserActivity.this.getSharedPreferences("boarding_pref_user",MODE_PRIVATE);
        return sharedPreferences.getBoolean("IsFirstTimeVisit_user",false);
    }

    private void setShowcaseView() {
        new TapTargetSequence(this)
                .targets(TapTarget.forView(contact_support,"User Permission Button!","Tap the User Permission button to Create new User.")
                        .outerCircleColor(R.color.light)
                        .outerCircleAlpha(0.96f)
                        .targetCircleColor(R.color.white)
                        .titleTextSize(20)
                        .titleTextColor(R.color.white)
                        .descriptionTextSize(14)
                        .descriptionTextColor(R.color.black)
                        .textColor(R.color.black)
                        .textTypeface(Typeface.SANS_SERIF)
                        .dimColor(R.color.black)
                        .drawShadow(true)
                        .cancelable(false)
                        .tintTarget(true)
                        .transparentTarget(true)
                        .targetRadius(60)).listener(new TapTargetSequence.Listener() {
            @Override
            public void onSequenceFinish() {
                // Toast.makeText(getApplicationContext(),"Sequence Finished",Toast.LENGTH_SHORT).show();
                savePrefData();
                //openDialog();
            }

            @Override
            public void onSequenceStep(TapTarget lastTarget, boolean targetClicked) {
                // Toast.makeText(getActivity(),"GREAT!",Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSequenceCanceled(TapTarget lastTarget) {

            }
        }).start();


    }

    @OnClick(R.id.imgBack)
    public void onback() {
        finish();
    }

    @OnClick(R.id.contact_support)
    public void contact_support() {
     startActivity(new Intent(UserActivity.this,AddUserPermissionActivity.class));
    }


    public void fetchStudent() {
        if (loaded == 0) {
            commonProgress.show();
        }
        loaded++;

        HashMap<String, String> params = new HashMap<String, String>();
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.USER_LIST, apiCallback, this, params);

    }

    ApiHandler.ApiCallback apiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            commonProgress.dismiss();
            if (error == null) {
                commonProgress.dismiss();
                try {
                    if (mData != null) {
                        mData.clear();
                    }

                    if (mData != null){
                        mData.clear();
                        mData= User_Permission.parseUser_PermissionArray(jsonObject.getJSONArray("user_data"));
                        newsAdapter.setHQList(mData);
                        newsAdapter.notifyDataSetChanged();
                        totalRecord.setText("Total Entries:- "+String.valueOf(mData.size()));
                        txtNorecord.setVisibility(View.GONE);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                commonProgress.dismiss();
                totalRecord.setText("Total Entries:- 0");
                txtNorecord.setVisibility(View.VISIBLE);
                mData.clear();
                newsAdapter.setHQList(mData);
                newsAdapter.notifyDataSetChanged();
            }
        }
    };


    @Override
    public void StudentProfile(User_Permission std) {
        Gson gson  = new Gson();
        String student = gson.toJson(std,User_Permission.class);
        Intent i = new Intent(UserActivity.this, UpdateUserPermissionActivity.class);
        i.putExtra("user",student);
        startActivity(i);
    }

    @Override
    public void onUpdateCallback(User_Permission std) {

    }

    @Override
    public void stdCallBack(User_Permission std) {
        String phoneNo = std.getPhoneno();
        Log.i("email",phoneNo);
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:"+ phoneNo));
        startActivity(intent);
    }

    @Override
    public void stdEmailCallBack(User_Permission std) {
        String email = std.getEmail();
        Log.i("email",email);
        Intent intent = new Intent(Intent.ACTION_SENDTO); // it's not ACTION_SEND
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_SUBJECT, "Subject of email");
        intent.putExtra(Intent.EXTRA_TEXT, "Body of email");
        intent.setData(Uri.parse("mailto:"+email)); // or just "mailto:" for blank
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK); // this will make such that when user returns to your app, your app is displayed, instead of the email app.
        startActivity(intent);
    }
}