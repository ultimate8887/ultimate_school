package com.ultimate.ultimatesmartschool.Activity;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.ultimate.ultimatesmartschool.BeanModule.SchoolBean;
import com.ultimate.ultimatesmartschool.BeanModule.User;
import com.ultimate.ultimatesmartschool.UltimateSchoolApp;
import com.ultimate.ultimatesmartschool.Utility.Constants;

import org.json.JSONException;
import org.json.JSONObject;

public class UserPermissions {

    private static UserPermissions sInstance;
    public static String PREFRENCES_USER = "userpermissions";

    private String setup;
    private String student_panel;

    public String getSetup() {
        return setup;
    }

    public void setSetup(String setup) {
        this.setup = setup;
    }

    public String getStudent_panel() {
        return student_panel;
    }

    public void setStudent_panel(String student_panel) {
        this.student_panel = student_panel;
    }

    public String getStaff_panel() {
        return staff_panel;
    }

    public void setStaff_panel(String staff_panel) {
        this.staff_panel = staff_panel;
    }

    public String getFeepay() {
        return feepay;
    }

    public void setFeepay(String feepay) {
        this.feepay = feepay;
    }

    public String getStud_attend() {
        return stud_attend;
    }

    public void setStud_attend(String stud_attend) {
        this.stud_attend = stud_attend;
    }

    public String getStaff_attend() {
        return staff_attend;
    }

    public void setStaff_attend(String staff_attend) {
        this.staff_attend = staff_attend;
    }

    public String getExamination() {
        return examination;
    }

    public void setExamination(String examination) {
        this.examination = examination;
    }

    public String getSyllabus() {
        return syllabus;
    }

    public void setSyllabus(String syllabus) {
        this.syllabus = syllabus;
    }

    public String getTimetable() {
        return timetable;
    }

    public void setTimetable(String timetable) {
        this.timetable = timetable;
    }

    public String getAssignment() {
        return assignment;
    }

    public void setAssignment(String assignment) {
        this.assignment = assignment;
    }

    public String getHomework() {
        return homework;
    }

    public void setHomework(String homework) {
        this.homework = homework;
    }

    public String getClasswork() {
        return classwork;
    }

    public void setClasswork(String classwork) {
        this.classwork = classwork;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getNotice() {
        return notice;
    }

    public void setNotice(String notice) {
        this.notice = notice;
    }

    public String getThought() {
        return thought;
    }

    public void setThought(String thought) {
        this.thought = thought;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getEbook() {
        return ebook;
    }

    public void setEbook(String ebook) {
        this.ebook = ebook;
    }

    public String getTransport() {
        return transport;
    }

    public void setTransport(String transport) {
        this.transport = transport;
    }

    public String getIdcard() {
        return idcard;
    }

    public void setIdcard(String idcard) {
        this.idcard = idcard;
    }

    public String getLibrary() {
        return library;
    }

    public void setLibrary(String library) {
        this.library = library;
    }

    public String getVisitor() {
        return visitor;
    }

    public void setVisitor(String visitor) {
        this.visitor = visitor;
    }

    public String getHrm() {
        return hrm;
    }

    public void setHrm(String hrm) {
        this.hrm = hrm;
    }

    public String getCerificate() {
        return cerificate;
    }

    public void setCerificate(String cerificate) {
        this.cerificate = cerificate;
    }

    public String getHostel() {
        return hostel;
    }

    public void setHostel(String hostel) {
        this.hostel = hostel;
    }

    public String getPayroll() {
        return payroll;
    }

    public void setPayroll(String payroll) {
        this.payroll = payroll;
    }

    public String getAccounts() {
        return accounts;
    }

    public void setAccounts(String accounts) {
        this.accounts = accounts;
    }

    public String getElearning() {
        return elearning;
    }

    public void setElearning(String elearning) {
        this.elearning = elearning;
    }

    public String getGatepass() {
        return gatepass;
    }

    public void setGatepass(String gatepass) {
        this.gatepass = gatepass;
    }

    public String getClasstest() {
        return classtest;
    }

    public void setClasstest(String classtest) {
        this.classtest = classtest;
    }

    private String staff_panel;
    private String feepay;
    private String stud_attend;
    private String staff_attend;
    private String examination;
    private String syllabus;
    private String timetable;

    public String getSea() {
        return sea;
    }

    public void setSea(String sea) {
        this.sea = sea;
    }

    private String sea;


    private String assignment;
    private String homework;
    private String classwork;
    private String message;
    private String notice;
    private String thought;
    private String photo;
    private String ebook;
    private String transport;

    private String idcard;
    private String library;
    private String visitor;
    private String hrm;
    private String cerificate;
    private String hostel;
    private String payroll;
    private String accounts;
    private String elearning;

    public String getAdmission_form() {
        return admission_form;
    }

    public void setAdmission_form(String admission_form) {
        this.admission_form = admission_form;
    }

    private String admission_form;
    private String gatepass;
    private String classtest;


    public String getStaff_leave() {
        return staff_leave;
    }

    public void setStaff_leave(String staff_leave) {
        this.staff_leave = staff_leave;
    }

    public String getStd_leave() {
        return std_leave;
    }

    public void setStd_leave(String std_leave) {
        this.std_leave = std_leave;
    }

    private String staff_leave;
    private String std_leave;


    public String getBanner() {
        return banner;
    }

    public void setBanner(String banner) {
        this.banner = banner;
    }

    public String getAdd_staff() {
        return add_staff;
    }

    public void setAdd_staff(String add_staff) {
        this.add_staff = add_staff;
    }

    public String getView_staff() {
        return view_staff;
    }

    public void setView_staff(String view_staff) {
        this.view_staff = view_staff;
    }

    public String getAssign_subject() {
        return assign_subject;
    }

    public void setAssign_subject(String assign_subject) {
        this.assign_subject = assign_subject;
    }

    public String getAssign_incharge() {
        return assign_incharge;
    }

    public void setAssign_incharge(String assign_incharge) {
        this.assign_incharge = assign_incharge;
    }

    private String es_enquiry;
    private String es_holiday;
    private String es_social_post;
    private String es_school_activity;
    private String es_expense;
    private String es_visitor;
    private String es_feedback;
    private String es_performance;
    private String es_planner;

    public String getEs_enquiry() {
        return es_enquiry;
    }

    public void setEs_enquiry(String es_enquiry) {
        this.es_enquiry = es_enquiry;
    }

    public String getEs_holiday() {
        return es_holiday;
    }

    public void setEs_holiday(String es_holiday) {
        this.es_holiday = es_holiday;
    }

    public String getEs_social_post() {
        return es_social_post;
    }

    public void setEs_social_post(String es_social_post) {
        this.es_social_post = es_social_post;
    }

    public String getEs_school_activity() {
        return es_school_activity;
    }

    public void setEs_school_activity(String es_school_activity) {
        this.es_school_activity = es_school_activity;
    }

    public String getEs_expense() {
        return es_expense;
    }

    public void setEs_expense(String es_expense) {
        this.es_expense = es_expense;
    }

    public String getEs_visitor() {
        return es_visitor;
    }

    public void setEs_visitor(String es_visitor) {
        this.es_visitor = es_visitor;
    }

    public String getEs_feedback() {
        return es_feedback;
    }

    public void setEs_feedback(String es_feedback) {
        this.es_feedback = es_feedback;
    }

    public String getEs_performance() {
        return es_performance;
    }

    public void setEs_performance(String es_performance) {
        this.es_performance = es_performance;
    }

    public String getEs_planner() {
        return es_planner;
    }

    public void setEs_planner(String es_planner) {
        this.es_planner = es_planner;
    }

    public String getEs_health() {
        return es_health;
    }

    public void setEs_health(String es_health) {
        this.es_health = es_health;
    }

    private String es_health;

    private String banner;
    private String add_staff;
    private String view_staff;
    private String assign_subject;
    private String assign_incharge;
    private String staff_gate_attend;


    public String getDatesheet() {
        return datesheet;
    }

    public void setDatesheet(String datesheet) {
        this.datesheet = datesheet;
    }

    public String getStd_gatepass() {
        return std_gatepass;
    }

    public void setStd_gatepass(String std_gatepass) {
        this.std_gatepass = std_gatepass;
    }

    private String datesheet;
    private String std_gatepass;


    public String getStaff_gate_attend() {
        return staff_gate_attend;
    }

    public void setStaff_gate_attend(String staff_gate_attend) {
        this.staff_gate_attend = staff_gate_attend;
    }

    public String getStd_gate_attend() {
        return std_gate_attend;
    }

    public void setStd_gate_attend(String std_gate_attend) {
        this.std_gate_attend = std_gate_attend;
    }

    private String std_gate_attend;


    public static void setCurrentUserPermissions(JSONObject jsonObject) {
        UserPermissions user = getCurrentUserPermissions();
        try {

            if (user == null) {
                user = new UserPermissions();
            }

            if (jsonObject.has("std_gatepass") && !jsonObject.getString("std_gatepass").isEmpty() && !jsonObject.getString("std_gatepass").equalsIgnoreCase("null")) {
                user.setStd_gatepass(jsonObject.getString("std_gatepass"));
            }


            if (jsonObject.has("datesheet") && !jsonObject.getString("datesheet").isEmpty() && !jsonObject.getString("datesheet").equalsIgnoreCase("null")) {
                user.setDatesheet(jsonObject.getString("datesheet"));
            }


            if (jsonObject.has("admission_form") && !jsonObject.getString("admission_form").isEmpty() && !jsonObject.getString("admission_form").equalsIgnoreCase("null")) {
                user.setAdmission_form(jsonObject.getString("admission_form"));
            }


            if (jsonObject.has("student_panel") && !jsonObject.getString("student_panel").isEmpty() && !jsonObject.getString("student_panel").equalsIgnoreCase("null")) {
                user.setStudent_panel(jsonObject.getString("student_panel"));
            }

            if (jsonObject.has("staff_panel") && !jsonObject.getString("staff_panel").isEmpty() && !jsonObject.getString("staff_panel").equalsIgnoreCase("null")) {
                user.setStaff_panel(jsonObject.getString("staff_panel"));
            }

            if (jsonObject.has("feepay") && !jsonObject.getString("feepay").isEmpty() && !jsonObject.getString("feepay").equalsIgnoreCase("null")) {
                user.setFeepay(jsonObject.getString("feepay"));
            }

            if (jsonObject.has("stud_attend") && !jsonObject.getString("stud_attend").isEmpty() && !jsonObject.getString("stud_attend").equalsIgnoreCase("null")) {
                user.setStud_attend(jsonObject.getString("stud_attend"));
            }

            if (jsonObject.has("staff_attend") && !jsonObject.getString("staff_attend").isEmpty() && !jsonObject.getString("staff_attend").equalsIgnoreCase("null")) {
                user.setStaff_attend(jsonObject.getString("staff_attend"));
            }

            if (jsonObject.has("examination") && !jsonObject.getString("examination").isEmpty() && !jsonObject.getString("examination").equalsIgnoreCase("null")) {
                user.setExamination(jsonObject.getString("examination"));
            }

            if (jsonObject.has("syllabus") && !jsonObject.getString("syllabus").isEmpty() && !jsonObject.getString("syllabus").equalsIgnoreCase("null")) {
                user.setSyllabus(jsonObject.getString("syllabus"));
            }

            if (jsonObject.has("timetable") && !jsonObject.getString("timetable").isEmpty() && !jsonObject.getString("timetable").equalsIgnoreCase("null")) {
                user.setTimetable(jsonObject.getString("timetable"));
            }

            if (jsonObject.has("assignment") && !jsonObject.getString("assignment").isEmpty() && !jsonObject.getString("assignment").equalsIgnoreCase("null")) {
                user.setAssignment(jsonObject.getString("assignment"));
            }

            if (jsonObject.has("homework") && !jsonObject.getString("homework").isEmpty() && !jsonObject.getString("homework").equalsIgnoreCase("null")) {
                user.setHomework(jsonObject.getString("homework"));
            }
            if (jsonObject.has("classwork") && !jsonObject.getString("classwork").isEmpty() && !jsonObject.getString("classwork").equalsIgnoreCase("null")) {
                user.setClasswork(jsonObject.getString("classwork"));
            }

            if (jsonObject.has("message") && !jsonObject.getString("message").isEmpty() && !jsonObject.getString("message").equalsIgnoreCase("null")) {
                user.setMessage(jsonObject.getString("message"));
            }

            if (jsonObject.has("notice") && !jsonObject.getString("notice").isEmpty() && !jsonObject.getString("notice").equalsIgnoreCase("null")) {
                user.setNotice(jsonObject.getString("notice"));
            }

            if (jsonObject.has("thought") && !jsonObject.getString("thought").isEmpty() && !jsonObject.getString("thought").equalsIgnoreCase("null")) {
                user.setThought(jsonObject.getString("thought"));
            }

            if (jsonObject.has("photo") && !jsonObject.getString("photo").isEmpty() && !jsonObject.getString("photo").equalsIgnoreCase("null")) {
                user.setPhoto(jsonObject.getString("photo"));
            }
            if (jsonObject.has("ebook") && !jsonObject.getString("ebook").isEmpty() && !jsonObject.getString("ebook").equalsIgnoreCase("null")) {
                user.setEbook(jsonObject.getString("ebook"));
            }

            if (jsonObject.has("setup") && !jsonObject.getString("setup").isEmpty() && !jsonObject.getString("setup").equalsIgnoreCase("null")) {
                user.setSetup(jsonObject.getString("setup"));
            }

            if (jsonObject.has("transport") && !jsonObject.getString("transport").isEmpty() && !jsonObject.getString("transport").equalsIgnoreCase("null")) {
                user.setTransport(jsonObject.getString("transport"));
            }
            if (jsonObject.has("idcard") && !jsonObject.getString("idcard").isEmpty() && !jsonObject.getString("idcard").equalsIgnoreCase("null")) {
                user.setIdcard(jsonObject.getString("idcard"));
            }

            if (jsonObject.has("library") && !jsonObject.getString("library").isEmpty() && !jsonObject.getString("library").equalsIgnoreCase("null")) {
                user.setLibrary(jsonObject.getString("library"));
            }



            if (jsonObject.has("visitor") && !jsonObject.getString("visitor").isEmpty() && !jsonObject.getString("visitor").equalsIgnoreCase("null")) {
                user.setVisitor(jsonObject.getString("visitor"));
            }

            if (jsonObject.has("hrm") && !jsonObject.getString("hrm").isEmpty() && !jsonObject.getString("hrm").equalsIgnoreCase("null")) {
                user.setHrm(jsonObject.getString("hrm"));
            }
            if (jsonObject.has("cerificate") && !jsonObject.getString("cerificate").isEmpty() && !jsonObject.getString("cerificate").equalsIgnoreCase("null")) {
                user.setCerificate(jsonObject.getString("cerificate"));
            }

            if (jsonObject.has("hostel") && !jsonObject.getString("hostel").isEmpty() && !jsonObject.getString("hostel").equalsIgnoreCase("null")) {
                user.setHostel(jsonObject.getString("hostel"));
            }



            if (jsonObject.has("payroll") && !jsonObject.getString("payroll").isEmpty() && !jsonObject.getString("payroll").equalsIgnoreCase("null")) {
                user.setPayroll(jsonObject.getString("payroll"));
            }

            if (jsonObject.has("accounts") && !jsonObject.getString("accounts").isEmpty() && !jsonObject.getString("accounts").equalsIgnoreCase("null")) {
                user.setAccounts(jsonObject.getString("accounts"));
            }
            if (jsonObject.has("elearning") && !jsonObject.getString("elearning").isEmpty() && !jsonObject.getString("elearning").equalsIgnoreCase("null")) {
                user.setElearning(jsonObject.getString("elearning"));
            }

            if (jsonObject.has("gatepass") && !jsonObject.getString("gatepass").isEmpty() && !jsonObject.getString("gatepass").equalsIgnoreCase("null")) {
                user.setGatepass(jsonObject.getString("gatepass"));
            }

            if (jsonObject.has("classtest") && !jsonObject.getString("classtest").isEmpty() && !jsonObject.getString("classtest").equalsIgnoreCase("null")) {
                user.setClasstest(jsonObject.getString("classtest"));
            }


            if (jsonObject.has("banner") && !jsonObject.getString("banner").isEmpty() && !jsonObject.getString("banner").equalsIgnoreCase("null")) {
                user.setBanner(jsonObject.getString("banner"));
            }

            if (jsonObject.has("add_staff") && !jsonObject.getString("add_staff").isEmpty() && !jsonObject.getString("add_staff").equalsIgnoreCase("null")) {
                user.setAdd_staff(jsonObject.getString("add_staff"));
            }
            if (jsonObject.has("view_staff") && !jsonObject.getString("view_staff").isEmpty() && !jsonObject.getString("view_staff").equalsIgnoreCase("null")) {
                user.setView_staff(jsonObject.getString("view_staff"));
            }

            if (jsonObject.has("assign_subject") && !jsonObject.getString("assign_subject").isEmpty() && !jsonObject.getString("assign_subject").equalsIgnoreCase("null")) {
                user.setAssign_subject(jsonObject.getString("assign_subject"));
            }

            if (jsonObject.has("sea") && !jsonObject.getString("sea").isEmpty() && !jsonObject.getString("sea").equalsIgnoreCase("null")) {
                user.setSea(jsonObject.getString("sea"));
            }

            if (jsonObject.has("assign_incharge") && !jsonObject.getString("assign_incharge").isEmpty() && !jsonObject.getString("assign_incharge").equalsIgnoreCase("null")) {
                user.setAssign_incharge(jsonObject.getString("assign_incharge"));
            }

            if (jsonObject.has("staff_leave") && !jsonObject.getString("staff_leave").isEmpty() && !jsonObject.getString("staff_leave").equalsIgnoreCase("null")) {
                user.setStaff_leave(jsonObject.getString("staff_leave"));
            }
            if (jsonObject.has("std_leave") && !jsonObject.getString("std_leave").isEmpty() && !jsonObject.getString("std_leave").equalsIgnoreCase("null")) {
                user.setStd_leave(jsonObject.getString("std_leave"));
            }


            if (jsonObject.has("std_gate_attend") && !jsonObject.getString("std_gate_attend").isEmpty() && !jsonObject.getString("std_gate_attend").equalsIgnoreCase("null")) {
                user.setStd_gate_attend(jsonObject.getString("std_gate_attend"));
            }


            if (jsonObject.has("staff_gate_attend") && !jsonObject.getString("staff_gate_attend").isEmpty() && !jsonObject.getString("staff_gate_attend").equalsIgnoreCase("null")) {
                user.setStaff_gate_attend(jsonObject.getString("staff_gate_attend"));
            }


            if (jsonObject.has("es_enquiry") && !jsonObject.getString("es_enquiry").isEmpty() && !jsonObject.getString("es_enquiry").equalsIgnoreCase("null")) {
                user.setEs_enquiry(jsonObject.getString("es_enquiry"));
            }else{
                user.setEs_enquiry("no");
            }

            if (jsonObject.has("es_holiday") && !jsonObject.getString("es_holiday").isEmpty() && !jsonObject.getString("es_holiday").equalsIgnoreCase("null")) {
                user.setEs_holiday(jsonObject.getString("es_holiday"));
            }else{
                user.setEs_holiday("no");
            }


            if (jsonObject.has("es_social_post") && !jsonObject.getString("es_social_post").isEmpty() && !jsonObject.getString("es_social_post").equalsIgnoreCase("null")) {
                user.setEs_social_post(jsonObject.getString("es_social_post"));
            }else{
                user.setEs_social_post("no");
            }

            if (jsonObject.has("es_school_activity") && !jsonObject.getString("es_school_activity").isEmpty() && !jsonObject.getString("es_school_activity").equalsIgnoreCase("null")) {
                user.setEs_school_activity(jsonObject.getString("es_school_activity"));
            }else{
                user.setEs_school_activity("no");
            }
            if (jsonObject.has("es_expense") && !jsonObject.getString("es_expense").isEmpty() && !jsonObject.getString("es_expense").equalsIgnoreCase("null")) {
                user.setEs_expense(jsonObject.getString("es_expense"));
            }else{
                user.setEs_expense("no");
            }

            if (jsonObject.has("es_visitor") && !jsonObject.getString("es_visitor").isEmpty() && !jsonObject.getString("es_visitor").equalsIgnoreCase("null")) {
                user.setEs_visitor(jsonObject.getString("es_visitor"));
            }else{
                user.setEs_visitor("no");
            }

            if (jsonObject.has("es_feedback") && !jsonObject.getString("es_feedback").isEmpty() && !jsonObject.getString("es_feedback").equalsIgnoreCase("null")) {
                user.setEs_feedback(jsonObject.getString("es_feedback"));
            }else{
                user.setEs_feedback("no");
            }

            if (jsonObject.has("es_performance") && !jsonObject.getString("es_performance").isEmpty() && !jsonObject.getString("es_performance").equalsIgnoreCase("null")) {
                user.setEs_performance(jsonObject.getString("es_performance"));
            }else{
                user.setEs_performance("no");
            }
            if (jsonObject.has("es_planner") && !jsonObject.getString("es_planner").isEmpty() && !jsonObject.getString("es_planner").equalsIgnoreCase("null")) {
                user.setEs_planner(jsonObject.getString("es_planner"));
            }else{
                user.setEs_planner("no");
            }

            if (jsonObject.has("es_health") && !jsonObject.getString("es_health").isEmpty() && !jsonObject.getString("es_health").equalsIgnoreCase("null")) {
                user.setEs_health(jsonObject.getString("es_health"));
            }else{
                user.setEs_health("no");
            }


            Gson gson = new Gson();
            String userString = gson.toJson(user, UserPermissions.class);
            SharedPreferences preferences = UltimateSchoolApp.getInstance().getSharedPreferences(Constants.PREF_NAME, Context.MODE_PRIVATE);
            preferences.edit().putString(PREFRENCES_USER, userString).commit();

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static UserPermissions getCurrentUserPermissions() {
        if (sInstance == null) {
            SharedPreferences preferences = UltimateSchoolApp.getInstance().getSharedPreferences(Constants.PREF_NAME, Context.MODE_PRIVATE);
            String userString = preferences.getString(PREFRENCES_USER, null);
            if (userString != null) {
                Gson gson = new Gson();
                sInstance = gson.fromJson(userString, UserPermissions.class);
                return sInstance;
            } else {
                return null;
            }
        } else {
            return sInstance;
        }
    }
    public static void logoutUserPermissions() {
        sInstance = null;
        SharedPreferences pref = UltimateSchoolApp.getInstance().getSharedPreferences(Constants.PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.remove(PREFRENCES_USER);
        editor.commit();
    }



}
