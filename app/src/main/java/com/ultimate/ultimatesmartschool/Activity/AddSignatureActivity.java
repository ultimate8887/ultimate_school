package com.ultimate.ultimatesmartschool.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.squareup.picasso.Picasso;
import com.ultimate.ultimatesmartschool.BeanModule.User;
import com.ultimate.ultimatesmartschool.Fee.AddFeesQRActivity;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.StaffGatePass.GatePassstaffBean;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.CommonProgress;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;
import com.ultimate.ultimatesmartschool.Utility.ErpProgressVert;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AddSignatureActivity extends AppCompatActivity {


    Animation animation;
    @BindView(R.id.circleimgrecepone)
    ImageView image;
    @BindView(R.id.imgBackmsg)
    ImageView back;
    @BindView(R.id.edit_profile)
    ImageView edit_profile;
    @BindView(R.id.edit_sign)
    ImageView edit_sign;
    @BindView(R.id.submit)
    TextView submit;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    String image_url="";
    CommonProgress commonProgress;
    private Bitmap admin_bitmap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_fees_q_r);
        ButterKnife.bind(this);
        txtTitle.setText("Add Digital Signature");
        edit_profile.setVisibility(View.GONE);
        edit_sign.setVisibility(View.VISIBLE);
        commonProgress = new CommonProgress(this);
        animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.btn_blink_animation);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                back.startAnimation(animation);
                finish();
            }
        });
        edit_sign.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edit_sign.startAnimation(animation);
                selectImg();
            }
        });
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submit.startAnimation(animation);
                assignData();
            }
        });
        userprofile();
    }

    private void userprofile() {
        // UltimateProgress.showProgressBar(getActivity(),"Please Wait........!");
        commonProgress.show();
        HashMap<String, String> params = new HashMap<>();
        params.put("id", User.getCurrentUser().getId());
        params.put("check", "sign");
        params.put("logo", "");

        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.UPDATE_SCHOOL_INFO, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error ) {
                commonProgress.dismiss();
                ErpProgress.cancelProgressBar();
                if (error == null) {
                    try {
                        image_url = jsonObject.getJSONObject(Constants.USERDATA).getString("signature");
                        if (!image_url.equalsIgnoreCase("")) {
                            Utils.progressImg_two(image_url,image, AddSignatureActivity.this,"doc");
                            // Picasso.with(AddFeesQRActivity.this).load(image_url).placeholder(R.color.transparent).into(image);
                        } else {
                            Picasso.get().load(R.color.transparent).into(image);
                            Toast.makeText(getApplicationContext(), "Digital Signature not found, kindly add your digital Signature to upload", Toast.LENGTH_SHORT).show();
                            selectImg();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }, this, params);
    }

    private void selectImg() {
        final Dialog warningDialog = new Dialog(this);
        warningDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        warningDialog.setCancelable(true);

        warningDialog.setContentView(R.layout.signature_dialog);
        warningDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        RelativeLayout btnClose = (RelativeLayout) warningDialog.findViewById(R.id.btnNo);

        final CustomSignatureView  signatureView = warningDialog.findViewById(R.id.signature_view);
        //   Button btnClear = (Button) warningDialog.findViewById(R.id.btnClear);
        Button btnYes = (Button) warningDialog.findViewById(R.id.btnOk);
        Button btnClear = (Button) warningDialog.findViewById(R.id.btnClear);
        btnYes.setText("Select");
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                warningDialog.dismiss();
            }
        });

        btnClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signatureView.clear();
            }
        });

        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                warningDialog.dismiss();
                //  ErpProgress.showProgressBar(CheckIn.this, "Please wait...");
                admin_bitmap = signatureView.getSignatureBitmap();
                image.setImageBitmap(admin_bitmap);
                submit.setVisibility(View.VISIBLE);
              //  approve_paa(admin_bitmap,gatePassstaffBean);
            }

        });

        warningDialog.show();

    }
    private void assignData() {

        if (admin_bitmap == null) {
            Toast.makeText(AddSignatureActivity.this, "Kindly add Digital Signature", Toast.LENGTH_SHORT).show();
            //errorMsg = "Please enter Title";
        } else {
            commonProgress.show();
            HashMap<String, String> params = new HashMap<>();
            params.put("id", User.getCurrentUser().getId());
            params.put("check", "sign");
            if (admin_bitmap != null) {
                String encoded = Utils.encodeToBase64(admin_bitmap, Bitmap.CompressFormat.JPEG, 90);
                encoded = String.format("data:image/jpeg;base64,%s", encoded);
                params.put("logo", encoded);
            }

            ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.UPDATE_SCHOOL_INFO, new ApiHandler.ApiCallback() {
                @Override
                public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                    ErpProgressVert.cancelProgressBar();
                    commonProgress.dismiss();
                    if (error == null) {
                        Toast.makeText(getApplicationContext(), "Added Successfully", Toast.LENGTH_SHORT).show();
                        finish();
                    } else {
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            }, this, params);
        }
    }
}