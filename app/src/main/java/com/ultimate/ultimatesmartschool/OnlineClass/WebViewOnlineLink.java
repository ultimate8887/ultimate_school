package com.ultimate.ultimatesmartschool.OnlineClass;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;

import butterknife.BindView;
import butterknife.ButterKnife;

public class WebViewOnlineLink extends AppCompatActivity {
    final Context context = this;

    @BindView(R.id.webviewabout)
    WebView webView;
    private WebChromeClient.CustomViewCallback customViewCallback;
    OnlineclsBean hostlroombean;
    boolean edit = false;
    ImageView imgback;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    boolean loadingFinished = true;
    boolean redirect = false;
    String link;
    Activity activity ;
    private ProgressDialog progDailog;

//    private MyWebChromeClient mWebChromeClient = null;
//    private View mCustomView;
//    private RelativeLayout mContentView;
    private FrameLayout mCustomViewContainer;
    private WebChromeClient.CustomViewCallback mCustomViewCallback;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view_online_link);
        ButterKnife.bind(this);
        if (getIntent().getExtras() != null) {
            if (getIntent().getExtras().containsKey("syllabus_data")) {
                Gson gson = new Gson();
                hostlroombean = gson.fromJson(getIntent().getExtras().getString("syllabus_data"), OnlineclsBean.class);
                edit = true;

            }
        }
        activity = this;

        link=hostlroombean.getS_file();
//        progDailog = ProgressDialog.show(activity, "Loading","Please wait...", true);
//        progDailog.setCancelable(false);
        Log.e("link","https://www.youtube.com/watch?v="+link);
        txtTitle.setText(hostlroombean.getEs_title());
        imgback = (ImageView) findViewById(R.id.imgBack);
        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setDomStorageEnabled(true);
        webView.loadUrl("https://www.youtube.com/channel/UCLmI8_hVUqA251PzK7OSvQw");
        webView.setWebViewClient(new loadWebView());
    }
    private class loadWebView extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }
//        webView.setWebViewClient(new Browser_Home());
//        webView.setWebChromeClient(new ChromeClient());
//        WebSettings webSettings = webView.getSettings();
//
//        webSettings.setJavaScriptEnabled(true);
//        webSettings.setAllowFileAccess(true);
//        webSettings.setAppCacheEnabled(true);
//        loadWebSite();
//        ErpProgress.showProgressBar(WebViewOnlineLink.this,"Please wait...");
//
//        webView.setWebViewClient(new WebViewClient(){
//            @Override
//            public void onPageFinished(WebView view, String url) {
//                if(!redirect){
//                    loadingFinished = true;
//                }
//
//                if(loadingFinished && !redirect){
//                    ErpProgress.cancelProgressBar();
//                } else{
//                    redirect = false;
//                }
//
//            }
//        });

   // }

//    @Override
//    public void onConfigurationChanged(Configuration newConfig) {
//        super.onConfigurationChanged(newConfig);
//    }
//
//    @Override
//    public void onSaveInstanceState(Bundle outState) {
//        super.onSaveInstanceState(outState);
//        webView.saveState(outState);
//    }
//
//    @Override
//    protected void onRestoreInstanceState(Bundle savedInstanceState) {
//        super.onRestoreInstanceState(savedInstanceState);
//        webView.restoreState(savedInstanceState);
//
//    }
//
//    private void loadWebSite() {
//        webView.loadUrl(link);
//    }
//
//    private class Browser_Home extends WebViewClient {
//        Browser_Home(){}
//
//        @Override
//        public void onPageStarted(WebView view, String url, Bitmap favicon) {
//            super.onPageStarted(view, url, favicon);
//        }
//
//        @Override
//        public void onPageFinished(WebView view, String url) {
//            super.onPageFinished(view, url);
//        }
//    }
//
//    private class ChromeClient extends WebChromeClient {
//        private View mCustomView;
//        private WebChromeClient.CustomViewCallback mCustomViewCallback;
//        protected FrameLayout mFullscreenContainer;
//        private int mOriginalOrientation;
//        private int mOriginalSystemUiVisibility;
//
//        ChromeClient() {
//        }
//
//        public Bitmap getDefaultVideoPoster() {
//            if (mCustomView == null) {
//                return null;
//            }
//            return BitmapFactory.decodeResource(getApplicationContext().getResources(), 2130837573);
//        }
//
//        public void onHideCustomView() {
//            ((FrameLayout) getWindow().getDecorView()).removeView(this.mCustomView);
//            this.mCustomView = null;
//            getWindow().getDecorView().setSystemUiVisibility(this.mOriginalSystemUiVisibility);
//            setRequestedOrientation(this.mOriginalOrientation);
//            this.mCustomViewCallback.onCustomViewHidden();
//            this.mCustomViewCallback = null;
//        }
//
//        public void onShowCustomView(View paramView, WebChromeClient.CustomViewCallback paramCustomViewCallback) {
//            if (this.mCustomView != null) {
//                onHideCustomView();
//                return;
//            }
//            this.mCustomView = paramView;
//            this.mOriginalSystemUiVisibility = getWindow().getDecorView().getSystemUiVisibility();
//            this.mOriginalOrientation = getRequestedOrientation();
//            this.mCustomViewCallback = paramCustomViewCallback;
//            ((FrameLayout) getWindow().getDecorView()).addView(this.mCustomView, new FrameLayout.LayoutParams(-1, -1));
//            getWindow().getDecorView().setSystemUiVisibility(3846 | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
//        }
//    }
}
