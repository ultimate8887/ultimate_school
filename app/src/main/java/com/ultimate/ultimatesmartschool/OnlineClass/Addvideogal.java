package com.ultimate.ultimatesmartschool.OnlineClass;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;

import com.ultimate.ultimatesmartschool.BeanModule.User;
import com.ultimate.ultimatesmartschool.Gallery.Albumlistbean;
import com.ultimate.ultimatesmartschool.Gallery.SpinAlbumlistadapter;
import com.ultimate.ultimatesmartschool.Login.LoginActivity;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.CommonProgress;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;
import com.ultimate.ultimatesmartschool.Utility.ErpProgressVert;
import com.ultimate.ultimatesmartschool.Utility.FileUtils;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class Addvideogal extends AppCompatActivity implements View.OnClickListener{
    String fileBase64 = null;
    private static final int SELECT_VIDEO = 3;
    private String selectedPath;
    private TextView textView;
    TextView addimage;
    Button add_photo_btn;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.edtphotonm)
    EditText photoname;
    @BindView(R.id.parent)
    RelativeLayout parent;
    @BindView(R.id.spinnerselectalbm)
    Spinner spinerAlbum;
    ArrayList<Albumlistbean> albmlist = new ArrayList<>();
    SpinAlbumlistadapter adapter;
    String album_id;
    String album_name;
    float  length;
    String size;
    CommonProgress commonProgress;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_addvideogal);
        ButterKnife.bind(this);
        commonProgress=new CommonProgress(this);
      //  parent.setAnimation(AnimationUtils.loadAnimation(getApplicationContext(),R.anim.fade_scale_animation));
        txtTitle.setText("Add Video");
        fetchalbumlist();
        addimage = (TextView) findViewById(R.id.addimage);
        add_photo_btn = (Button) findViewById(R.id.add_photo_btn);
        textView = (TextView) findViewById(R.id.textView);
        addimage.setOnClickListener(this);
        add_photo_btn.setOnClickListener(this);
    }
    @OnClick(R.id.imgBack)
    public void onback() {
        finish();
    }

    public void fetchalbumlist(){
        commonProgress.show();
        HashMap<String, String> params = new HashMap<String, String>();
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.ALBUMLIST_URL, albmapiCallback, this, params);

    }

    ApiHandler.ApiCallback albmapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            commonProgress.dismiss();
            if (error == null) {
                try {
                    albmlist = Albumlistbean.parseALBMlistarray(jsonObject.getJSONArray("admin_data"));
                    adapter = new SpinAlbumlistadapter(Addvideogal.this, albmlist);
                    spinerAlbum.setAdapter(adapter);
                    spinerAlbum.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if (i > 0) {
                                album_id = albmlist.get(i-1).getAlbum_id();
                                album_name=albmlist.get(i-1).getAlbum_name();
                                Log.e("classsid",album_id);

                            }else{
                                album_id="";
                            }

                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Utils.showSnackBar(error.getMessage(), parent);
            }
        }
    };


    private void chooseVideo() {
        Intent intent = new Intent();
        intent.setType("video/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivityForResult(Intent.createChooser(intent, "Select a Video "), SELECT_VIDEO);


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == SELECT_VIDEO) {
                System.out.println("SELECT_VIDEO");
                Uri selectedImageUri = data.getData();
                // selectedPath = getPath(selectedImageUri);
                selectedPath = FileUtils.getRealPath(this, selectedImageUri);
                File file = new File(selectedPath);
                length = file.length();
                length = length/1000000;
                size = formatSize(file.length());
                try {
                    if((float)length >30.0f){
                        Toast.makeText(getApplicationContext(),"Sorry, Your selected file size too large("+size+"), Kindly compress your file first to upload!" +
                                "(Max Size of file uploading should be less than 25MB)",Toast.LENGTH_LONG).show();                    }else {
                        fileBase64 = Utils.encodeFileToBase64Binary(selectedPath);
                        Bitmap bitmap = ThumbnailUtils.createVideoThumbnail(selectedPath, MediaStore.Video.Thumbnails.MICRO_KIND);
                        ByteArrayOutputStream stream = new ByteArrayOutputStream();
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 10, stream);
                        ((ImageView)findViewById(R.id.imageView6)).setImageBitmap(bitmap);
                      //  textView.setText(selectedPath+length+"mb");
                        textView.setVisibility(View.VISIBLE);
                        textView.setText("Path: " + selectedPath + "\nSize: " + size);
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }


            }
        }else {
            Toast.makeText(this, "Selection Cancelled", Toast.LENGTH_SHORT).show();
        }
    }
    public static String formatSize(long size) {
        String suffix = null;

        if (size >= 1024) {
            suffix = " Bytes";
            size /= 1024;
            if (size >= 1024) {
                suffix = " MB";
                size /= 1024;
            }
        }
        StringBuilder resultBuffer = new StringBuilder(Long.toString(size));

        int commaOffset = resultBuffer.length() - 3;
        while (commaOffset > 0) {
            resultBuffer.insert(commaOffset, ',');
            commaOffset -= 3;
        }
        if (suffix != null) resultBuffer.append(suffix);
        return resultBuffer.toString();
    }

    @Override
    public void onClick(View v) {
        if (v == addimage) {
            chooseVideo();
        }
        if (v == add_photo_btn) {
            uploadVideo2();
        }
    }


    public void uploadVideo2() {

        if (checkValid()) {

            commonProgress.show();
            HashMap<String, String> params = new HashMap<String, String>();
            if (fileBase64 != null) {
                params.put("image", fileBase64);
            }
            params.put("album_id", album_id);
            // params.put("album_name",album_name);
            params.put("p_title", photoname.getText().toString());
            params.put("type","video");
            ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.ADDVIDEO_URL, addphoapiCallback, this, params);
        }

    }


    ApiHandler.ApiCallback addphoapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgressVert.cancelProgressBar();
            commonProgress.dismiss();
            if (error == null) {
                  //  Utils.showSnackBar(jsonObject.getString("msg"), parent);
                    Toast.makeText(getApplicationContext(), "Video Uploaded Successfully!", Toast.LENGTH_SHORT).show();
                    finish();
            } else {
                Log.e("error", error.getMessage() + "");
                Utils.showSnackBar(error.getMessage(), parent);
                if (error.getStatusCode() == 405) {
                    User.logout();
                    startActivity(new Intent(Addvideogal.this, LoginActivity.class));
                    finish();
                }
            }
        }
    };

    private boolean checkValid() {
        boolean valid = true;
        String errorMsg = null;
        if (album_id.isEmpty()) {
            valid = false;
            errorMsg = "Please Select Album";
        } else if (photoname.getText().toString().trim().length() <= 0) {
            valid = false;
            errorMsg = "Please Enter Photo Title";
        } else if (fileBase64 == null) {
            valid = false;
            errorMsg = "Please Put your video";
        }else if((float)length >30.0f){
            Toast.makeText(getApplicationContext(),"Sorry,Your file size too large!(Max Size 25MB)",Toast.LENGTH_LONG).show();
            valid = false;
            errorMsg = "Sorry,Your file size too large!(Max Size of file will be 25MB) ";
        }
        if (!valid) {
            Utils.showSnackBar(errorMsg, parent);
        }
        return valid;
    }
}