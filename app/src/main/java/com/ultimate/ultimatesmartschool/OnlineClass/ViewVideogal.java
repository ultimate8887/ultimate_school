package com.ultimate.ultimatesmartschool.OnlineClass;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.google.gson.Gson;
import com.ultimate.ultimatesmartschool.BeanModule.SubjectBeancls;
import com.ultimate.ultimatesmartschool.BeanModule.User;
import com.ultimate.ultimatesmartschool.Gallery.GalleryPhoto_bean;
import com.ultimate.ultimatesmartschool.Gallery.ViewGallery;
import com.ultimate.ultimatesmartschool.Homework.AdapterOfSubjectList;
import com.ultimate.ultimatesmartschool.Login.LoginActivity;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.CommonProgress;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;
import com.ultimate.ultimatesmartschool.Utility.Utils;
import com.ultimate.ultimatesmartschool.VideoPlayer.VideoPlayerActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ViewVideogal extends AppCompatActivity implements GalleryvidAdapter.Mycallback{
    @BindView(R.id.parent)
    RelativeLayout parent;
    private LinearLayoutManager layoutManager;
    CommonProgress commonProgress;
    private int loaded = 0;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    private GalleryvidAdapter adapter;
    ArrayList<GalleryPhoto_bean> hwList = new ArrayList<>();
    @BindView(R.id.totalRecord)
    TextView totalRecord;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerview;
    @BindView(R.id.textNorecord)
    TextView txtNorecord;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_gallery);
        ButterKnife.bind(this);
        commonProgress=new CommonProgress(this);
        txtTitle.setText("Gallery Video");
        layoutManager = new LinearLayoutManager(ViewVideogal.this);
        recyclerview.setLayoutManager(layoutManager);
        adapter = new GalleryvidAdapter(hwList, ViewVideogal.this, this);
        recyclerview.setAdapter(adapter);
        fetchVIDEO();
    }
    @OnClick(R.id.imgBack)
    public void onback() {
        finish();
    }
    public void fetchVIDEO() {
        commonProgress.show();
        HashMap<String, String> params = new HashMap<String, String>();
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.viewgalleryvideourl, apiCallback, this, params);
    }

    ApiHandler.ApiCallback apiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            commonProgress.dismiss();
            if (error == null) {
                try {
                    if (hwList != null) {
                        hwList.clear();
                    }
                    JSONArray jsonArray = jsonObject.getJSONArray("syllabus_data");
                    hwList = GalleryPhoto_bean.parseEVENTArray(jsonArray);
                    if (hwList.size() > 0) {
                        adapter.setSyllabusList(hwList);
                        adapter.notifyDataSetChanged();
                        txtNorecord.setVisibility(View.GONE);
                        totalRecord.setText("Total Entries:- "+String.valueOf(hwList.size()));
                    } else {
                        totalRecord.setText("Total Entries:- 0");
                        adapter.setSyllabusList(hwList);
                        adapter.notifyDataSetChanged();
                        txtNorecord.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                totalRecord.setText("Total Entries:- 0");
                txtNorecord.setVisibility(View.VISIBLE);
                hwList.clear();
                adapter.setSyllabusList(hwList);
                adapter.notifyDataSetChanged();
            }
        }
    };

    @Override
    public void onMethodCallback(GalleryPhoto_bean eventlist_bean) {
        commonProgress.show();
        HashMap<String, String> params = new HashMap<>();
        params.put("event_id", eventlist_bean.getEvent_id());
        params.put("check", "video");
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.DELETEGALARY_URL, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                commonProgress.dismiss();
                if (error == null) {
                    int pos = hwList.indexOf(eventlist_bean);
                    hwList.remove(pos);
                    adapter.setSyllabusList(hwList);
                    adapter.notifyDataSetChanged();
                    totalRecord.setText("Total Entries:- "+String.valueOf(hwList.size()));
                    Utils.showSnackBar("Deleted Successfully!", parent);
                } else {
                    Log.e("error", error.getMessage() + "");
                    Utils.showSnackBar(error.getMessage(), parent);
                    Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_SHORT).show();
                    if (error.getStatusCode() == 405) {
                        User.logout();
                        startActivity(new Intent(ViewVideogal.this, LoginActivity.class));
                        finish();
                    }
                }
            }
        }, this, params);
    }

    @Override
    public void viewPdf(GalleryPhoto_bean syllabusBean) {
        if(syllabusBean.getType().equalsIgnoreCase("photo")) {
//            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.youtube.com/watch?v=" + syllabusBean.getS_file())));
        }else {
//            Gson gson = new Gson();
//            String ebookdata = gson.toJson(syllabusBean, GalleryPhoto_bean.class);
            Intent intent = new Intent(ViewVideogal.this, VideoPlayerActivity.class);
            intent.putExtra("video_data", syllabusBean.getS_file());
            intent.putExtra("video_name", syllabusBean.getTitle());
            startActivity(intent);
        }
    }
}