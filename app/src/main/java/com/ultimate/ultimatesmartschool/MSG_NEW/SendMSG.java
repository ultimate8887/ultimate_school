package com.ultimate.ultimatesmartschool.MSG_NEW;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.tabs.TabLayout;
import com.ultimate.ultimatesmartschool.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SendMSG extends AppCompatActivity {
    @BindView(R.id.imgBack)
    ImageView imgBackks;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    private Animation animation;

    @BindView(R.id.viewpager)
    ViewPager viewpager;
    @BindView(R.id.tab)
    TabLayout tablayout;
    String id="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_m_s_g);
        ButterKnife.bind(this);
       if (getIntent().getExtras() !=null){
           id = getIntent().getExtras().getString("id");
       }
        animation = AnimationUtils.loadAnimation(SendMSG.this, R.anim.btn_blink_animation);

        setupTabPager();
    }
    private void setupTabPager() {
        FilterTabPager adapter;
        if (id.equalsIgnoreCase("inbox")){
            txtTitle.setText("Inbox");
             adapter = new FilterTabPager(getSupportFragmentManager(),1,this);
        }else {
            txtTitle.setText("Sent Messages");
             adapter = new FilterTabPager(getSupportFragmentManager(),2,this);
        }
        viewpager.setAdapter(adapter);
        tablayout.setupWithViewPager(viewpager);
    }

    @OnClick(R.id.imgBack)
    public void imgBackks()  {
        imgBackks.startAnimation(animation);
        finish();
    }
}