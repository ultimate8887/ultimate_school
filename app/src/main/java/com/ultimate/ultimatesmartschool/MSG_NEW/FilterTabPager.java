package com.ultimate.ultimatesmartschool.MSG_NEW;

import android.content.Context;
import android.util.Log;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.ultimate.ultimatesmartschool.MSG_NEW.Fragments.Send_AdminFragment;
import com.ultimate.ultimatesmartschool.MSG_NEW.Fragments.Send_StaffFragment;
import com.ultimate.ultimatesmartschool.MSG_NEW.Fragments.Send_StdFragment;

public class FilterTabPager extends FragmentStatePagerAdapter {
    int v;
    Context context;
    public FilterTabPager(FragmentManager fm,int v,Context context) {
        super(fm);
        this.context = context;
        this.v=v;
        //Log.e("Send_StdFragment",String.valueOf(v));
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new Send_StdFragment(v,context);
            case 1:
                return new Send_StaffFragment(v,context);
            case 2:
                return new Send_AdminFragment(v,context);
            default:
                break;
        }
        return null;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        // Generate title based on item position
        switch (position) {
            case 0:
                return "Student";
            case 1:
                return "Staff";
            case 2:
                return "Admin";
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return 3;
    }
}
