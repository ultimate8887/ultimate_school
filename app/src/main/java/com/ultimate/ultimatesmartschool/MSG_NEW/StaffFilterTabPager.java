package com.ultimate.ultimatesmartschool.MSG_NEW;

import android.content.Context;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.ultimate.ultimatesmartschool.MSG_NEW.Fragments.Send_AdminFragment;
import com.ultimate.ultimatesmartschool.MSG_NEW.Fragments.Send_StaffFragment;
import com.ultimate.ultimatesmartschool.MSG_NEW.Fragments.Send_StdFragment;
import com.ultimate.ultimatesmartschool.MSG_NEW.Fragments.Stafftostu_ClassWise;
import com.ultimate.ultimatesmartschool.MSG_NEW.Fragments.Stafftostu_StaffWise;

public class StaffFilterTabPager extends FragmentStatePagerAdapter {

    public StaffFilterTabPager(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new Stafftostu_ClassWise();
            case 1:
                return new Stafftostu_StaffWise();
            default:
                break;
        }
        return null;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        // Generate title based on item position
        switch (position) {
            case 0:
                return "View Class Wise";
            case 1:
                return "View Staff Wise";
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return 2;
    }
}
