package com.ultimate.ultimatesmartschool.MSG_NEW.Fragments;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.os.Handler;
import android.text.ClipboardManager;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;
import com.android.volley.Request;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.ultimate.ultimatesmartschool.BeanModule.ClassBean;
import com.ultimate.ultimatesmartschool.BeanModule.User;
import com.ultimate.ultimatesmartschool.ClassWork.CWViewPagerAdapter;
import com.ultimate.ultimatesmartschool.Ebook.WebViewActivity;
import com.ultimate.ultimatesmartschool.Homework.StaffWiseHWActivity;
import com.ultimate.ultimatesmartschool.Login.LoginActivity;
import com.ultimate.ultimatesmartschool.MSG_NEW.ClassAdapter_New;
import com.ultimate.ultimatesmartschool.Message.InboxActivity;
import com.ultimate.ultimatesmartschool.Message.Message_AdapterNew;
import com.ultimate.ultimatesmartschool.Message.Message_Bean;
import com.ultimate.ultimatesmartschool.Message.Sent_message_bean;
import com.ultimate.ultimatesmartschool.Message.Sent_msg_Adapter;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.CommonProgress;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;
import com.ultimate.ultimatesmartschool.Utility.PaginationListener;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.relex.circleindicator.CircleIndicator;

public class Send_StdFragment extends Fragment implements Sent_msg_Adapter.Mycallback,Message_AdapterNew.Mycallback {

    @BindView(R.id.adddate)TextView adddate;
    ClassAdapter_New classAdapter_new;
    @BindView(R.id.cal_img)
    ImageView cal_img;
    @BindView(R.id.filelayout)
    LinearLayout filelayout;
    @BindView(R.id.parent)
    RelativeLayout parent;
    @BindView(R.id.recyclerViewmsg)
    RecyclerView recyclerView;
    @BindView(R.id.spinner)
    Spinner spinnerClass;
    LinearLayoutManager layoutManager;
    ArrayList<Sent_message_bean> sentmsglist = new ArrayList<>();
    Sent_msg_Adapter adapter;

    ArrayList<Message_Bean> messagelist=new ArrayList<>();
    private Message_AdapterNew msg_adapter;


    String type="2",name="student",date="",view_type="student",filter_id="",tdate="",ydate="";

    CommonProgress commonProgress;
    Animation animation;
    String id;
    @BindView(R.id.totalRecord)
    TextView totalRecord;
    int loaded = 0;
    public ArrayList<ClassBean> classList = new ArrayList<>();
    @BindView(R.id.textNorecord)
    TextView txtNorecord;

    ImageView imageView6;
    EditText subject;
    EditText message;
    TextView sendername,txtdate;
    Context context;
    int value;

    @BindView(R.id.main_progress)
    ProgressBar main_progress;
    int  limit=20,page_limit=0,total_pages=0;
    private static final int PAGE_START = 1;
    private int currentPage = PAGE_START;

    private boolean isLastPage = false;
    private int totalPage = 10;
    private boolean isLoading = false;
    int itemCount = 0;

    public Send_StdFragment(int value,Context context) {
        this.context=context;
        this.value=value;
        //  Log.e("Send_StdFragment_std",String.valueOf(value));
//        Toast.makeText(getContext(),String.valueOf(value),Toast.LENGTH_LONG).show();
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_send__common, container, false);
        ButterKnife.bind(this,view);
        commonProgress=new CommonProgress(getActivity());
        getTodayDate();
        animation = AnimationUtils.loadAnimation(getActivity(), R.anim.btn_blink_animation);
        id = User.getCurrentUser().getId();
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);

        if (value==1){
            msg_adapter = new Message_AdapterNew(messagelist, getActivity(),this);
            recyclerView.setAdapter(msg_adapter);
            fetnewmsg();
            fetchClass(limit);
        }else {
            adapter = new Sent_msg_Adapter(sentmsglist, getActivity(),this);
            recyclerView.setAdapter(adapter);
            fetchClass(limit);
        }
        // recyclerView.setNestedScrollingEnabled(false);
        // adding on scroll change listener method for our nested scroll view.

        /**
         * add scroll listener while user reach in bottom load more will call
         */
        recyclerView.addOnScrollListener(new PaginationListener(layoutManager) {
            @Override
            protected void loadMoreItems() {
                // isLoading = true;
                //  Utils.showSnackBar("No more messages found in message list.", parent);
                //  currentPage++;
                // doApiCall();
                if (total_pages>=page_limit) {
                    main_progress.setVisibility(View.VISIBLE);
                    currentPage += 10;
                    page_limit = currentPage + limit;

                    if (filter_id.equalsIgnoreCase("")){
                        if (value==1){
                            fetchsentmsglist_inbox(date,filter_id,0,page_limit,"no");
                        }else {
                            fetchsentmsglist(date,filter_id,0,page_limit,"no");
                        }
                    }else{
                        if (value==1){
                            fetchsentmsglist_inbox(date,filter_id,5,page_limit,"no");
                        }else {
                            fetchsentmsglist(date,filter_id,5,page_limit,"no");
                        }
                    }
                }else{
                    // main_progress.setVisibility(View.GONE);
                    //  Utils.showSnackBar("No more messages found in message list.", parent);
                }


            }
            @Override
            public boolean isLastPage() {
                return isLastPage;
            }
            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });

//        nestedSV.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
//            @Override
//            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
//                // on scroll change we are checking when users scroll as bottom.
//                if (scrollY == v.getChildAt(0).getMeasuredHeight() - v.getMeasuredHeight()) {
//                    main_progress.setVisibility(View.VISIBLE);
//                    // in this method we are incrementing page number,
//                    // making progress bar visible and calling get data method.
//                    if (total_pages>=page_limit) {
//
//                        currentPage += 1;
//                        page_limit = currentPage * limit;
//
//                        if (filter_id.equalsIgnoreCase("")){
//                            if (value==1){
//                                fetchsentmsglist_inbox(date,filter_id,0,page_limit,"no");
//                            }else {
//                                fetchsentmsglist(date,filter_id,0,page_limit,"no");
//                            }
//                        }else{
//                            if (value==1){
//                                fetchsentmsglist_inbox(date,filter_id,5,page_limit,"no");
//                            }else {
//                                fetchsentmsglist(date,filter_id,5,page_limit,"no");
//                            }
//                        }
//                    }else{
//                        main_progress.setVisibility(View.GONE);
//                        Utils.showSnackBar("No more messages found in message list.", parent);
//                    }
//
//                }
//            }
//        });

        filelayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                adddate.startAnimation(animation);
                java.util.Calendar cal = java.util.Calendar.getInstance(TimeZone.getDefault()); // Get current date
                DatePickerDialog datePicker = new DatePickerDialog(getActivity(),
                        R.style.MyDatePickerDialogTheme, ondate,
                        cal.get(java.util.Calendar.YEAR),
                        cal.get(java.util.Calendar.MONTH),
                        cal.get(java.util.Calendar.DAY_OF_MONTH));
                datePicker.getDatePicker().setMaxDate(new java.util.Date().getTime());
                datePicker.setCancelable(false);
                datePicker.show();
            }
        });
        return view;
    }

    public void getTodayDate() {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.HOUR_OF_DAY, 00);
        c.set(Calendar.MINUTE, 00);
        c.set(Calendar.SECOND, 00);
        Date setdate = c.getTime();
        //  SimpleDateFormat fmtOut = new SimpleDateFormat("dd MMM, yyyy");
        SimpleDateFormat fmtOut = new SimpleDateFormat("MMMM, yyyy");
        String dateString = fmtOut.format(setdate);
        // adddate.setText(dateString);
        SimpleDateFormat dateFrmOut = new SimpleDateFormat("yyyy-MM-dd");
        tdate = dateFrmOut.format(setdate);
        //for Yesterday
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);
        ydate= dateFormat.format(cal.getTime());
    }

    @Override
    public void onResume() {
        super.onResume();
        //fetchsentmsglist();
        // fetchClass();


    }

    DatePickerDialog.OnDateSetListener ondate = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            java.util.Calendar calendar = java.util.Calendar.getInstance(TimeZone.getDefault());
            calendar.set(year, monthOfYear, dayOfMonth);
            calendar.set(Calendar.HOUR_OF_DAY, 00);
            calendar.set(Calendar.MINUTE, 00);
            calendar.set(Calendar.SECOND, 00);
            java.util.Date setdate = calendar.getTime();
            SimpleDateFormat fmtOut = new SimpleDateFormat("dd MMM, yyyy");
            String dateString = fmtOut.format(setdate);
            // adddate.setText(dateString);
            SimpleDateFormat dateFrmOut = new SimpleDateFormat("yyyy-MM-dd");
            date = dateFrmOut.format(setdate);
            if (tdate.equalsIgnoreCase(date)){
                adddate.setText("Today");
                adddate.setTextColor(ContextCompat.getColor(getActivity(), R.color.light_red));
            } else if (ydate.equalsIgnoreCase(date)){
                adddate.setText("Yesterday");
                adddate.setTextColor(ContextCompat.getColor(getActivity(), R.color.green));
            }else {
                adddate.setText(dateString);
                adddate.setTextColor(ContextCompat.getColor(getActivity(), R.color.white));
            }
            page_limit=0;
            fetchClass(limit);
        }
    };

    private void fetchClass(int limit) {
        HashMap<String, String> params = new HashMap<String, String>();
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.CLASSLIST_URL, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                if (error == null) {
                    try {
                        classList = ClassBean.parseClassArray(jsonObject.getJSONArray(Constants.CLASSDATA));

                        if (getActivity()!=null){
                            classAdapter_new = new ClassAdapter_New(getActivity(), classList);
                            spinnerClass.setAdapter(classAdapter_new);
                            spinnerClass.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                    filter_id="";
                                    if (value==1){
                                        if (i > 0) {
                                            filter_id = classList.get(i - 1).getId();
                                            fetchsentmsglist_inbox(date,filter_id,5,limit,"yes");
                                        }else {
                                            filter_id="";
                                            fetchsentmsglist_inbox(date,filter_id,0,limit,"yes");
                                        }


                                    }else {
                                        if (i > 0) {
                                            filter_id = classList.get(i - 1).getId();
                                            fetchsentmsglist(date,filter_id,5,limit,"yes");
                                        }else {
                                            filter_id="";
                                            fetchsentmsglist(date,filter_id,0,limit,"yes");
                                        }


                                    }

                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> adapterView) {

                                }
                            });
                        }  else{
                            // Toast.makeText(getActivity(),"Std_nullllll",Toast.LENGTH_LONG).show();
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
//                Utils.showSnackBar(error.getMessage(), parent);
                }
            }
        }, getActivity(), params);
    }

    private void fetnewmsg() {
        HashMap<String, String> params = new HashMap<>();
        params.put("user_id",User.getCurrentUser().getId());
        params.put("check","");
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.NEW_MSG, apicallback5, getActivity(), params);
    }

    ApiHandler.ApiCallback apicallback5 = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();

            if (error == null) {

                try {
                    Log.e("USERDATA", jsonObject.getJSONObject("doc_data").toString());

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            } else {

                //  Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_LONG).show();
                //Utils.showSnackBar(error.getMessage(), parent);
            }
        }
    };


    private void fetchsentmsglist_inbox(String date,String filter_id,int i,int limit,String progress) {
        if (progress.equalsIgnoreCase("yes")){
            commonProgress.show();
        }
        HashMap<String, String> params = new HashMap<>();
        if (i==0){
            ApiHandler.apiHit(Request.Method.GET, Constants.getBaseURL() + Constants.MESSAGE+Constants.MSGID+id+Constants.MSGTYPE+"1"
                    +Constants.NAME1+name+Constants.DATE+date+Constants.VIEW_TYPE+view_type+Constants.FILTER_ID+""
                    +Constants.PAGE_LIMIT+limit,apicallback_inbox,getActivity(),params);
        }else {
            ApiHandler.apiHit(Request.Method.GET, Constants.getBaseURL() + Constants.MESSAGE+Constants.MSGID+id+Constants.MSGTYPE+"1"
                    +Constants.NAME1+name+Constants.DATE+date+Constants.VIEW_TYPE+view_type+Constants.FILTER_ID+filter_id
                    +Constants.PAGE_LIMIT+limit,apicallback_inbox,getActivity(),params);
        }

    }
    ApiHandler.ApiCallback apicallback_inbox = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            commonProgress.dismiss();
            if (error == null) {
                try {
                    if (messagelist != null) {
                        messagelist.clear();
                    }
                    JSONArray jsonArray = jsonObject.getJSONArray("msg_data");
                    ArrayList<Message_Bean> list = Message_Bean.parseMessageArray(jsonArray);
                    messagelist.addAll(list);
                    total_pages= Integer.parseInt(messagelist.get(0).getRowcount());
                    msg_adapter.notifyDataSetChanged();
                    txtNorecord.setVisibility(View.GONE);
                    totalRecord.setText("Total Entries:- "+String.valueOf(total_pages));
                    commonProgress.dismiss();
                    main_progress.setVisibility(View.GONE);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                totalRecord.setText("Total Entries:- 0");
                messagelist.clear();
                txtNorecord.setVisibility(View.VISIBLE);
                //  msg_adapter.setmessageList(messagelist);
                msg_adapter.notifyDataSetChanged();
                //  Utils.showSnackBar(error.getMessage(), parent);
            }
        }
    };


    private void fetchsentmsglist(String date,String filter_id,int i,int limit,String progress) {
        if (progress.equalsIgnoreCase("yes")){
            commonProgress.show();
        }
        HashMap<String, String> params = new HashMap<>();
        if (i==0){
            ApiHandler.apiHit(Request.Method.GET, Constants.getBaseURL() + Constants.MESSAGE+Constants.MSGID+id+Constants.MSGTYPE+type
                    +Constants.NAME1+name+Constants.DATE+date+Constants.VIEW_TYPE+view_type+Constants.FILTER_ID+""
                    +Constants.PAGE_LIMIT+limit,apicallback,getActivity(),params);
        }else {
            ApiHandler.apiHit(Request.Method.GET, Constants.getBaseURL() + Constants.MESSAGE+Constants.MSGID+id+Constants.MSGTYPE+type
                    +Constants.NAME1+name+Constants.DATE+date+Constants.VIEW_TYPE+view_type+Constants.FILTER_ID+filter_id
                    +Constants.PAGE_LIMIT+limit,apicallback,getActivity(),params);
        }

    }

    ApiHandler.ApiCallback apicallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            //  commonProgress.dismiss();
            if (error == null) {
                try {
                    if (sentmsglist != null) {
                        sentmsglist.clear();
                    }
                    JSONArray jsonArray = jsonObject.getJSONArray("msg_data");
                    ArrayList<Sent_message_bean> list = Sent_message_bean.parsesntMessageArray(jsonArray);
                    sentmsglist.addAll(list);
                    total_pages= Integer.parseInt(sentmsglist.get(0).getRowcount());
                    //   adapter.setsntmessageList(sentmsglist);
                    adapter.notifyDataSetChanged();
                    txtNorecord.setVisibility(View.GONE);
                    totalRecord.setText("Total Entries:- "+String.valueOf(sentmsglist.size()));
                    commonProgress.dismiss();
                    main_progress.setVisibility(View.GONE);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                commonProgress.dismiss();
                totalRecord.setText("Total Entries:- 0");
                txtNorecord.setVisibility(View.VISIBLE);
                sentmsglist.clear();
                // adapter.setsntmessageList(sentmsglist);
                adapter.notifyDataSetChanged();
//                Utils.showSnackBar(error.getMessage(), parent);
                //   Toast.makeText(getActivity(),error.getMessage(),Toast.LENGTH_LONG).show();
            }
        }
    };



    @Override
    public void onMethod_Image_callback(Sent_message_bean message_bean) {
        Dialog mBottomSheetDialog = new Dialog(getActivity());
        final View sheetView = getLayoutInflater().inflate(R.layout.view_msgimage_lyt, null);
        mBottomSheetDialog.setContentView(sheetView);
        mBottomSheetDialog.setCancelable(true);

        Animation animation;

        animation = AnimationUtils.loadAnimation(getActivity(), R.anim.btn_blink_animation);
        imageView6 = (ImageView) sheetView.findViewById(R.id.imageView6);
        subject = (EditText) sheetView.findViewById(R.id.txtsubject);
        message = (EditText) sheetView.findViewById(R.id.message_body);
        sendername = (TextView) sheetView.findViewById(R.id.txtsendername);
        txtdate = (TextView) sheetView.findViewById(R.id.txtdate);
        setData(message_bean);
        ImageView copy = (ImageView) sheetView.findViewById(R.id.copy);
        copy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                copy.startAnimation(animation);
                ClipboardManager cm = (ClipboardManager)getActivity().getSystemService(Context.CLIPBOARD_SERVICE);
                cm.setText(message.getText().toString());
                Toast.makeText(getActivity(), "Text copied to clipboard", Toast.LENGTH_SHORT).show();
            }
        });
        imageView6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                downloadfile(message_bean);
                //  Toast.makeText(GDSTest.this, "Downloaded successfully", Toast.LENGTH_LONG).show();
            }
        });
        RelativeLayout btnNo= (RelativeLayout) sheetView.findViewById(R.id.btnNo);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnNo.startAnimation(animation);
                mBottomSheetDialog.dismiss();
            }
        });

        mBottomSheetDialog.show();
        Window window = mBottomSheetDialog.getWindow();
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    }
    private void setData(Sent_message_bean sentmsglist) {
        subject.setEnabled(false);
        message.setEnabled(false);
        subject.setText(sentmsglist.getSubject());
       // message.setText(Html.fromHtml(sentmsglist.getMessage()));

        if (User.getCurrentUser().getSchoolData().getFi_school_id().equalsIgnoreCase("MSS2024")
                || User.getCurrentUser().getSchoolData().getFi_school_id().equalsIgnoreCase("NMS2024")){
            message.setText(Html.fromHtml(sentmsglist.getMessage()));
        }else{
            message.setText(sentmsglist.getMessage());
        }

        txtdate.setText(Utils.getTimeHr(sentmsglist.getCreated_on()));
        if (sentmsglist.getTo_name() != null) {
            String title = getColoredSpanned("To: ", "#e31e25");
            String Name = getColoredSpanned(sentmsglist.getTo_name(), "#7D7D7D");
            String l_Name = getColoredSpanned("["+ sentmsglist.getTo_type()+"]", "#5A5C59");
            sendername.setText(Html.fromHtml(title + " " + Name + " " + l_Name));
        }
        if (sentmsglist.getImage() != null) {
            imageView6.setVisibility(View.VISIBLE);
            Utils.progressImg_two(sentmsglist.getImage(),imageView6,getActivity(),"");
        }  else {
            imageView6.setVisibility(View.GONE);
        }
    }

    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }

    private void downloadfile(Sent_message_bean message_bean) {

        Dialog mBottomSheetDialog = new Dialog(getActivity());
        final View sheetView = getLayoutInflater().inflate(R.layout.image_lyt, null);
        mBottomSheetDialog.setContentView(sheetView);
        mBottomSheetDialog.setCancelable(true);

        Animation animation;
        animation = AnimationUtils.loadAnimation(getActivity(), R.anim.btn_blink_animation);
        ImageView imgShow = (ImageView) sheetView.findViewById(R.id.imgShow);


        ImageView imgDownload = (ImageView) sheetView.findViewById(R.id.imgDownload);
        imgDownload.setVisibility(View.GONE);
        Utils.progressImg_two(message_bean.getImage(),imgShow,getActivity(),"");
        //  Picasso.with(this).load(message_bean.getImage()).into(imgShow);
        imgDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imgDownload.startAnimation(animation);
                // ErpProgress.showProgressBar(InboxActivity.this, "downloading...");
                //Picasso.with(InboxActivity.this).load(message_bean.getImage()).into(imgTraget);
                //  Toast.makeText(GDSTest.this, "Downloaded successfully", Toast.LENGTH_LONG).show();
            }
        });


        mBottomSheetDialog.show();
        Window window = mBottomSheetDialog.getWindow();
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

    }

    @Override
    public void onMethod_Image_callback(Message_Bean message_bean) {
        Dialog mBottomSheetDialog = new Dialog(getActivity());
        final View sheetView = getLayoutInflater().inflate(R.layout.view_msgimage_lyt, null);
        mBottomSheetDialog.setContentView(sheetView);
        mBottomSheetDialog.setCancelable(true);

        Animation animation;

        animation = AnimationUtils.loadAnimation(getActivity(), R.anim.btn_blink_animation);
        imageView6 = (ImageView) sheetView.findViewById(R.id.imageView6);
        subject = (EditText) sheetView.findViewById(R.id.txtsubject);
        message = (EditText) sheetView.findViewById(R.id.message_body);
        sendername = (TextView) sheetView.findViewById(R.id.txtsendername);
        txtdate = (TextView) sheetView.findViewById(R.id.txtdate);
        setData_new(message_bean);

        ImageView copy = (ImageView) sheetView.findViewById(R.id.copy);
        copy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                copy.startAnimation(animation);
                ClipboardManager cm = (ClipboardManager)getActivity().getSystemService(Context.CLIPBOARD_SERVICE);
                cm.setText(message.getText().toString());
                Toast.makeText(getActivity(), "Text copied to clipboard", Toast.LENGTH_SHORT).show();
            }
        });

        imageView6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                downloadfile_new(message_bean);
                //  Toast.makeText(GDSTest.this, "Downloaded successfully", Toast.LENGTH_LONG).show();
            }
        });
        RelativeLayout btnNo= (RelativeLayout) sheetView.findViewById(R.id.btnNo);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnNo.startAnimation(animation);
                mBottomSheetDialog.dismiss();
            }
        });

        mBottomSheetDialog.show();
        Window window = mBottomSheetDialog.getWindow();
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    }



    private void downloadfile_new(Message_Bean message_bean) {
        Dialog mBottomSheetDialog = new Dialog(getActivity());
        final View sheetView = getLayoutInflater().inflate(R.layout.image_lyt, null);
        mBottomSheetDialog.setContentView(sheetView);
        mBottomSheetDialog.setCancelable(true);

        Animation animation;
        animation = AnimationUtils.loadAnimation(getActivity(), R.anim.btn_blink_animation);
        ImageView imgShow = (ImageView) sheetView.findViewById(R.id.imgShow);


        ImageView imgDownload = (ImageView) sheetView.findViewById(R.id.imgDownload);
        imgDownload.setVisibility(View.GONE);
        Utils.progressImg_two(message_bean.getImage(),imgShow,getActivity(),"");
        //Picasso.with(this).load(message_bean.getImage()).into(imgShow);
        imgDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imgDownload.startAnimation(animation);
                // ErpProgress.showProgressBar(InboxActivity.this, "downloading...");
                //Picasso.with(InboxActivity.this).load(message_bean.getImage()).into(imgTraget);
                //  Toast.makeText(GDSTest.this, "Downloaded successfully", Toast.LENGTH_LONG).show();
            }
        });


        mBottomSheetDialog.show();
        Window window = mBottomSheetDialog.getWindow();
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    }

    private void setData_new(Message_Bean sentmsglist) {
        subject.setEnabled(false);
        message.setEnabled(false);
        subject.setText(sentmsglist.getSubject());
        txtdate.setText(Utils.getTimeHr(sentmsglist.getCreated_on()));
//        message.setText(Html.fromHtml(sentmsglist.getMessage()));
        if (User.getCurrentUser().getSchoolData().getFi_school_id().equalsIgnoreCase("MSS2024")
                || User.getCurrentUser().getSchoolData().getFi_school_id().equalsIgnoreCase("NMS2024")){
            message.setText(Html.fromHtml(sentmsglist.getMessage()));
        }else{
            message.setText(sentmsglist.getMessage());
        }
        if (sentmsglist.getFrom_name() != null) {
            String title = getColoredSpanned("From: ", "#e31e25");
            String Name = getColoredSpanned(sentmsglist.getFrom_name(), "#7D7D7D");
            String l_Name = getColoredSpanned("["+ sentmsglist.getFrom_type()+"]", "#5A5C59");
            sendername.setText(Html.fromHtml(title + " " + Name + " " + l_Name));
        }
        if (sentmsglist.getImage() != null) {
            imageView6.setVisibility(View.VISIBLE);
            Utils.progressImg_two(sentmsglist.getImage(),imageView6, getActivity(),"");
            // Picasso.with(this).load(sentmsglist.getImage()).placeholder(this.getResources().getDrawable(R.drawable.logo)).into(imageView6);
        }  else {
            imageView6.setVisibility(View.GONE);
        }
    }


    @Override
    public void onDelecallback(Sent_message_bean homeworkbean) {
        //Toast.makeText(getActivity(), homeworkbean.getEs_messagesid(), Toast.LENGTH_LONG).show();
        commonProgress.show();
        HashMap<String, String> params = new HashMap<>();
        params.put("h_id", homeworkbean.getEs_messagesid());
        params.put("check", "msg");
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.DELETEHW, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                ErpProgress.cancelProgressBar();
                commonProgress.dismiss();
                if (error == null) {
                    int pos = sentmsglist.indexOf(homeworkbean);
                    sentmsglist.remove(pos);
                    adapter.setsntmessageList(sentmsglist);
                    adapter.notifyDataSetChanged();
                    Utils.showSnackBar("Deleted Successfully!", parent);
                    totalRecord.setText("Total Entries:- "+String.valueOf(sentmsglist.size()));
                } else {
                    Log.e("error", error.getMessage() + "");
                    Utils.showSnackBar(error.getMessage(), parent);
                    //  Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_LONG).show();
                    if (error.getStatusCode() == 405) {
                        User.logout();
                        startActivity(new Intent(getActivity(), LoginActivity.class));
                        getActivity().finish();
                    }
                }
            }
        }, getActivity(), params);
    }

    @Override
    public void onMethod_Image_callback_new(Message_Bean message_bean) {
         openCommonMultiImage(message_bean.getSubject(),message_bean.getMulti_image());
    }

    public ViewPager intro_images;
    LinearLayout pager_indicator;
    public CircleIndicator indicator;
    public TextView textView;
    String viewwwww = "";

    private void openCommonMultiImage(String subject, ArrayList<String> multiImage) {
        final Dialog warningDialog = new Dialog(getActivity());
        warningDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        warningDialog.setCancelable(true);

        warningDialog.setContentView(R.layout.class_img_dialog);
        warningDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        RelativeLayout btnClose = (RelativeLayout) warningDialog.findViewById(R.id.btnNo);

        indicator = (CircleIndicator) warningDialog.findViewById(R.id.indicator);
        textView = (TextView) warningDialog.findViewById(R.id.clsswrktexttt);
        intro_images = (ViewPager) warningDialog.findViewById(R.id.pager_introduction);

        textView.setText(subject);
        CWViewPagerAdapter mAdapter = new CWViewPagerAdapter(getActivity(), multiImage, "Message");
        intro_images.setAdapter(mAdapter);
        intro_images.setCurrentItem(0);
        indicator.setViewPager(intro_images);

        TextView tap_count = (TextView) warningDialog.findViewById(R.id.tap_count);
        String title1 = getColoredSpanned("<b>" + "1" + "</b>" + "", "#F4212C");
        String l_Name1 = getColoredSpanned(" / " + String.valueOf(multiImage.size()), "#000000");
        tap_count.setText(Html.fromHtml(title1 + " " + l_Name1 + " "));

        intro_images.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int arg0) {
                // TODO Auto-generated method stub
                // textView.setText((arg0+1)+" of "+ data.getRep_image().size());
                int c = arg0 + 1;
                String title1 = getColoredSpanned("<b>" + String.valueOf(c) + "</b>" + "", "#F4212C");
                String l_Name1 = getColoredSpanned(" / " + String.valueOf(multiImage.size()), "#000000");
                tap_count.setText(Html.fromHtml(title1 + " " + l_Name1 + " "));
            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
                // TODO Auto-generated method stub

                System.out.println("onPageScrolled");
            }

            @Override
            public void onPageScrollStateChanged(int num) {
                // TODO Auto-generated method stub

            }
        });
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                warningDialog.dismiss();
            }
        });
        warningDialog.show();
        Window window = warningDialog.getWindow();
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    }

    @Override
    public void onMethod_recording_callback(Message_Bean message_bean) {
        openCommonVoice(message_bean.getRecordingsfile());
    }

    private MediaPlayer mediaPlayer;
    private Button playButton, pauseButton;
    ImageView play_img, pause_img;
    RelativeLayout play_lyt;
    private SeekBar seekBar;
    private Handler handler = new Handler();
    private TextView currentDurationTextView, totalDurationTextView;
    LottieAnimationView loti_record_play;
    private Button btnRetake, btnPick,btnSave;
    BottomSheetDialog mBottomSheetDialog_play, mBottomSheetDialog;
    private void openCommonVoice(String recordingsfile) {

        mBottomSheetDialog_play = new BottomSheetDialog(getActivity());
        final View sheetView = getLayoutInflater().inflate(R.layout.activity_audio, null);
        mBottomSheetDialog_play.setContentView(sheetView);
        mBottomSheetDialog_play.setCancelable(false);

        btnRetake = sheetView.findViewById(R.id.btnRetake);
        btnPick = sheetView.findViewById(R.id.btnPick);
        btnSave = sheetView.findViewById(R.id.btnSave);
        play_lyt = sheetView.findViewById(R.id.play_lyt);
        play_img = sheetView.findViewById(R.id.play_img);
        pause_img = sheetView.findViewById(R.id.pause_img);
        playButton = sheetView.findViewById(R.id.playButton);
        pauseButton = sheetView.findViewById(R.id.pauseButton);
        seekBar = sheetView.findViewById(R.id.seekBar);
        loti_record_play = sheetView.findViewById(R.id.loti_play);
        currentDurationTextView = sheetView.findViewById(R.id.currentDurationTextView);
        totalDurationTextView = sheetView.findViewById(R.id.totalDurationTextView);


        btnSave.setVisibility(View.GONE);
        btnPick.setVisibility(View.GONE);

        mediaPlayer = new MediaPlayer();
        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                play_img.setVisibility(View.GONE);
                pause_img.setVisibility(View.VISIBLE);
                mediaPlayer.seekTo(0);
                seekBar.setProgress(0);
                loti_record_play.pauseAnimation();
            }
        });
        String url=recordingsfile;
        Log.d("selectedImageUri",url);
        Uri selectedImageUri = Uri.parse(url);

        try {
            mediaPlayer.setDataSource(getActivity(), selectedImageUri);

            mediaPlayer.prepare();
            mediaPlayer.start();
            updateSeekBar();
        } catch (IOException e) {
            e.printStackTrace();
        }

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


            }
        });

        play_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                play_img.setVisibility(View.GONE);
                pause_img.setVisibility(View.VISIBLE);
                mediaPlayer.pause();
                loti_record_play.pauseAnimation();
            }
        });

        pause_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                play_img.setVisibility(View.VISIBLE);
                pause_img.setVisibility(View.GONE);
                mediaPlayer.start();
                updateSeekBar();
            }
        });

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (fromUser) {
                    mediaPlayer.seekTo(progress);
                    updateDurationTextView(progress);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });

        totalDurationTextView.setText(formatDuration(mediaPlayer.getDuration()));

        btnRetake.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                commonCodeForDssmiss();
            }
        });

        // Dismiss the dialog and reset the MediaPlayer when the dialog is closed
        mBottomSheetDialog_play.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                //  Toast.makeText(getApplicationContext(), "Dismiss", Toast.LENGTH_SHORT).show();
            }
        });

        mBottomSheetDialog_play.show();
    }

    int starts = 0;

    private void commonCodeForDssmiss() {
        if (mediaPlayer != null) {
            mediaPlayer.release();
            mediaPlayer = null;
        }
        mBottomSheetDialog_play.dismiss();
    }

    private void updateSeekBar() {
        loti_record_play.playAnimation();
        seekBar.setMax(mediaPlayer.getDuration());
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (mediaPlayer != null) {
                    int currentPosition = mediaPlayer.getCurrentPosition();
                    seekBar.setProgress(currentPosition);
                    updateDurationTextView(currentPosition);
                    handler.postDelayed(this, 100);
                }
            }
        }, 100);
    }

    private void updateDurationTextView(int duration) {
        currentDurationTextView.setText(formatDuration(duration));
    }

    private String formatDuration(int milliseconds) {
        int seconds = (milliseconds / 1000) % 60;
        int minutes = (milliseconds / (1000 * 60)) % 60;
        return String.format(Locale.getDefault(), "%02d:%02d", minutes, seconds);
    }


    @Override
    public void onMethod_pdf_call_back(Message_Bean message_bean) {
        openCommonPdf(message_bean.getPdf_file());
    }

    private void openCommonPdf(String pdfFile) {
        Intent intent = new Intent(getActivity(), WebViewActivity.class);
        intent.putExtra("MY_kEY", pdfFile);
        startActivity(intent);
    }

    @Override
    public void onMethod_Image_callback_new_sent(Sent_message_bean message_bean) {
        openCommonMultiImage(message_bean.getSubject(),message_bean.getMulti_image());
    }

    @Override
    public void onMethod_recording_callback_sent(Sent_message_bean message_bean) {
        openCommonVoice(message_bean.getRecordingsfile());
    }

    @Override
    public void onMethod_pdf_call_back_sent(Sent_message_bean message_bean) {
        openCommonPdf(message_bean.getPdf_file());
    }


}