package com.ultimate.ultimatesmartschool.QRScanner;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.Result;
import com.ultimate.ultimatesmartschool.MainActivity;
import com.ultimate.ultimatesmartschool.R;

import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class QRScannerActivity extends AppCompatActivity {
    @BindView(R.id.lightButton)
    ImageView flashImageView;

    //Variables
    Intent i;
   // HistoryORM h = new HistoryORM();
//    private ZXingScannerView mScannerView;
    private boolean flashState = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_q_r_scanner);
        ButterKnife.bind(this);
        ActivityCompat.requestPermissions(QRScannerActivity.this,
                new String[]{Manifest.permission.CAMERA},
                1);

        ViewGroup contentFrame = (ViewGroup) findViewById(R.id.content_frame);
//        mScannerView = new ZXingScannerView(QRScannerActivity.this);
//        contentFrame.addView(mScannerView);

//       flashImageView.setOnClickListener(new View.OnClickListener() {
//           @Override
//           public void onClick(View v) {
//               mScannerView.setFlash(true);
//               if(flashState==false) {
//                   v.setBackgroundResource(R.drawable.ic_flash_off);
//                   Toast.makeText(getApplicationContext(), "Flashlight turned on", Toast.LENGTH_SHORT).show();
//                   mScannerView.setFlash(true);
//                   flashState = true;
//               }else if(flashState) {
//                   v.setBackgroundResource(R.drawable.ic_flash_on);
//                   Toast.makeText(getApplicationContext(), "Flashlight turned off", Toast.LENGTH_SHORT).show();
//                   mScannerView.setFlash(false);
//                   flashState = false;
//               }
//           }
//       });
    }
//
//    @Override
//    public void handleResult(Result rawResult) {
//        String mydate = java.text.DateFormat.getDateTimeInstance().format(Calendar.getInstance().getTime());
////        History history = new History();
////        history.setContext(rawResult.getText());
////        history.setDate(mydate);
////        h.add(getApplicationContext(), history);
//
//        // show custom alert dialog
//        final Dialog dialog = new Dialog(this);
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialog.setCancelable(true);
//        dialog.setContentView(R.layout.custom_dialogqrscan);
//        View v = dialog.getWindow().getDecorView();
//        v.setBackgroundResource(android.R.color.transparent);
//        TextView text = (TextView) dialog.findViewById(R.id.someText);
//        text.setText(rawResult.getText());
//        ImageView img = (ImageView) dialog.findViewById(R.id.imgOfDialog);
//        img.setImageResource(R.drawable.ic_done_gr);
//        Button webSearch = (Button) dialog.findViewById(R.id.searchButton);
////        Button copy = (Button) dialog.findViewById(R.id.copyButton);
////        Button share = (Button) dialog.findViewById(R.id.shareButton);
//        webSearch.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                String url;
//                if(Patterns.WEB_URL.matcher(rawResult.getText()).matches()) {
//                    url = rawResult.getText();
//                }else {
//                    url = "http://www.google.com/#q=" + rawResult.getText();
//                }
//                Intent intent= new Intent(Intent.ACTION_VIEW, Uri.parse(url));
//                startActivity(intent);
//                dialog.dismiss();
//                mScannerView.resumeCameraPreview(QRScannerActivity.this);
//            }
//        });
//
//
//        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
//            @Override
//            public void onCancel(DialogInterface dialog) {
//                mScannerView.resumeCameraPreview(QRScannerActivity.this);
//            }
//        });
//        dialog.show();
//    }

//    @Override
//    public void onResume() {
//        super.onResume();
//        mScannerView.setResultHandler(this);
//        mScannerView.startCamera();
//    }
//
//    @Override
//    public void onPause() {
//        super.onPause();
//        mScannerView.stopCamera();
//    }

//    @OnClick
//    void mainActivityOnClickEvents(View v) {
//
//        switch (v.getId()) {
//
//            case R.id.lightButton:
//                if(flashState==false) {
//                    v.setBackgroundResource(R.drawable.ic_flash_off);
//                    Toast.makeText(getApplicationContext(), "Flashlight turned on", Toast.LENGTH_SHORT).show();
//                    mScannerView.setFlash(true);
//                    flashState = true;
//                }else if(flashState) {
//                    v.setBackgroundResource(R.drawable.ic_flash_on);
//                    Toast.makeText(getApplicationContext(), "Flashlight turned off", Toast.LENGTH_SHORT).show();
//                    mScannerView.setFlash(false);
//                    flashState = false;
//                }
//                break;
//        }
//
//    }
}