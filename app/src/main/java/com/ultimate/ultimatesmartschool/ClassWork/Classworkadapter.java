package com.ultimate.ultimatesmartschool.ClassWork;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.daimajia.swipe.SimpleSwipeListener;
import com.daimajia.swipe.SwipeLayout;
import com.squareup.picasso.Picasso;
import com.ultimate.ultimatesmartschool.Banner.ViewPagerAdapter;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.relex.circleindicator.CircleIndicator;

public class Classworkadapter extends RecyclerView.Adapter<Classworkadapter.Viewholder>{
    private final Context listner;
    ArrayList<Classworkbean> hq_list;
    Eventcallback mEventcallback;
    Animation animation;
    public Classworkadapter(ArrayList<Classworkbean> hq_list, Context listner,Eventcallback mEventcallback) {
        this.hq_list=hq_list;
        this.listner=listner;
        this.mEventcallback=mEventcallback;
    }

    @Override
    public Classworkadapter.Viewholder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.homeada_lay_new, viewGroup, false);
        Classworkadapter.Viewholder viewholder = new Classworkadapter.Viewholder(view);
        return viewholder;
    }

    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }

    @Override
    public void onBindViewHolder(final Classworkadapter.Viewholder holder, @SuppressLint("RecyclerView") int position) {

        animation = AnimationUtils.loadAnimation(listner, R.anim.btn_blink_animation);
        if (hq_list.get(position).getSubject_name() != null){
            holder.sub.setText(hq_list.get(position).getSubject_name());
        }else{
            holder.sub.setText("Not Mentioned");
        }
        if (hq_list.get(position).getClass_name() != null) {
            String title = getColoredSpanned("Class-", "#000000");
            String Name="";
            if (hq_list.get(position).getSection_name() != null) {
                Name = getColoredSpanned(hq_list.get(position).getClass_name() + "(" + hq_list.get(position).getSection_name() + ")", "#5A5C59");
            }else {
                Name = getColoredSpanned(hq_list.get(position).getClass_name(), "#5A5C59");
            }
            holder.classess.setText(Html.fromHtml(title + " " + Name));
        }else {
            holder.classess.setVisibility(View.GONE);
        }

        if (hq_list.get(position).getId() != null) {
            String title = getColoredSpanned("ID:- ", "#000000");
            String Name = getColoredSpanned("cw-"+hq_list.get(position).getId(), "#5A5C59");
            holder.id.setText(Html.fromHtml(title + " " + Name));
        }

        if (hq_list.get(position).getStaff_name() != null) {
            String title = getColoredSpanned("Added by-", "#000000");
            String Name = getColoredSpanned(""+hq_list.get(position).getStaff_name()+"("+hq_list.get(position).getPost_name()+")", "#5A5C59");
            holder.addbyid.setText(Html.fromHtml(title + " " + Name));
        }

        holder.homeTopic.setText(hq_list.get(position).getInformation());

        // holder.u_date.setText(Utils.getDateFormated(hq_list.get(position).getDate()));

        //for Today
        java.util.Calendar calendar = java.util.Calendar.getInstance(TimeZone.getDefault());
        Date setdate = calendar.getTime();
        SimpleDateFormat fmtOut = new SimpleDateFormat("dd MMM, yyyy");
        String dateString = fmtOut.format(setdate);
        //for Yesterday
        DateFormat dateFormat = new SimpleDateFormat("dd MMM, yyyy");
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);
        String dateString1= dateFormat.format(cal.getTime());

        String check= Utils.getDateFormated(hq_list.get(position).getDate());
        if (check.equalsIgnoreCase(dateString)){
            //  holder.lytLine.setVisibility(View.VISIBLE);
            // holder.txtDT.setText("Today");
            String title = getColoredSpanned("Today", "#e31e25");
            //String Name = getColoredSpanned(messagelist.get(position).getFrom_type(), "#7D7D7D");
            String l_Name = getColoredSpanned("", "#5A5C59");
            holder.u_date.setText(Html.fromHtml(title + " " + l_Name));


            // holder.txtDT.setText("Today at "+time);
            //  holder.vieww.setBackground(mContext.getResources().getDrawable(R.drawable.a_card_lyt2));
        }else if (check.equalsIgnoreCase(dateString1)){

            String title = getColoredSpanned("Yesterday", "#1C8B3B");
            //String Name = getColoredSpanned(messagelist.get(position).getFrom_type(), "#7D7D7D");
            String l_Name = getColoredSpanned("", "#5A5C59");
            holder.u_date.setText(Html.fromHtml(title + " " + l_Name));

            //  holder.lytLine.setVisibility(View.GONE);
            //  holder.vieww.setBackground(mContext.getResources().getDrawable(R.drawable.card_lyt2));
        }else {
            holder.u_date.setText(Utils.getDateFormated(hq_list.get(position).getDate()));
        }

//        if (hq_list.get(position).getImage() != null) {
//            holder.nofile.setVisibility(View.VISIBLE);
//            holder.image_file.setVisibility(View.VISIBLE);
//            holder.audio_file.setVisibility(View.GONE);
//            holder.pdf_file.setVisibility(View.GONE);
//            holder.empty_file.setVisibility(View.GONE);
//            Picasso.with(listner).load(hq_list.get(position).getImage().get(0)).placeholder(listner.getResources().getDrawable(R.color.transparent)).into(holder.image_file);
//            holder.image_file.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    holder.tap_view_pic.startAnimation(animation);
//                    if(mEventcallback!= null){
//                        mEventcallback.DisplayImage(hq_list.get(position));
//                    }
//                }
//            });
//        }else {
//            holder.nofile.setVisibility(View.VISIBLE);
//            holder.image_file.setVisibility(View.GONE);
//            holder.audio_file.setVisibility(View.GONE);
//            holder.pdf_file.setVisibility(View.GONE);
//            holder.empty_file.setVisibility(View.VISIBLE);
//            Picasso.with(listner).load(R.drawable.classwork).into(holder.empty_file);
//        }

        holder.nofile.setVisibility(View.VISIBLE);
        holder.audio_file.setVisibility(View.GONE);
        holder.pdf_file.setVisibility(View.GONE);

        if (hq_list.get(position).getImage_tag().equalsIgnoreCase("images")){
            holder.empty_file.setVisibility(View.GONE);
            holder.image_file.setVisibility(View.VISIBLE);
            Picasso.get().load(hq_list.get(position).getImage().get(0)).placeholder(listner.getResources().getDrawable(R.color.transparent)).into(holder.image_file);
            holder.image_file.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    holder.image_file.startAnimation(animation);
                    if(mEventcallback!= null){
                        mEventcallback.onMethod_Image_callback(hq_list.get(position));
                    }
                }
            });
        } else if (hq_list.get(position).getPdf_file() != null){
            holder.nofile.setVisibility(View.VISIBLE);
            holder.image_file.setVisibility(View.GONE);
            holder.audio_file.setVisibility(View.GONE);
            holder.pdf_file.setVisibility(View.VISIBLE);
            holder.empty_file.setVisibility(View.GONE);
            holder.pdf_file.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    holder.tap_view_pdf.startAnimation(animation);
                    if(mEventcallback!= null){
                        mEventcallback.onMethod_pdf_call_back(hq_list.get(position));
                    }
                }
            });
        }else{
            //   Picasso.with(listner).load(hq_list.get(position).getImage().get(0)).placeholder(listner.getResources().getDrawable(R.drawable.classwork)).into(holder.image_file);
            Picasso.get().load(R.drawable.classwork).into(holder.empty_file);
            holder.empty_file.setVisibility(View.VISIBLE);
            holder.image_file.setVisibility(View.GONE);
        }

        holder.swipeLayout.setShowMode(SwipeLayout.ShowMode.PullOut);
        holder.swipeLayout.addSwipeListener(new SimpleSwipeListener() {
            @Override
            public void onOpen(SwipeLayout layout) {

            }

            @Override
            public void onStartClose(SwipeLayout layout) {
                holder.swipeLayout.close();
            }
        });

        // mAdaptercall.onMethodCallback();
        holder.update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEventcallback.onUpdateCallback(hq_list.get(position));
            }
        });
        holder.trash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder1 = new AlertDialog.Builder(listner);
                builder1.setMessage("Do you want to delete? ");
                builder1.setCancelable(false);
                builder1.setPositiveButton(
                        "Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                                if (mEventcallback != null) {
                                    mEventcallback.onDelecallback(hq_list.get(position));
//                                    Toast.makeText(listner, "deleted", Toast.LENGTH_SHORT).show();
                                }
                                // mAdapterCallback.deleteNotification(notificationList.get(postion));
                            }
                        });
                builder1.setNegativeButton(
                        "No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        });
                AlertDialog alert11 = builder1.create();
                alert11.show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return hq_list.size();
    }

    public void setHQList(ArrayList<Classworkbean> hq_list) {
        this.hq_list = hq_list;
    }

    public class Viewholder extends RecyclerView.ViewHolder {
        public TextView u_date,homeTopic,sub;
        public ImageView image_file, audio_file, pdf_file;
        public RelativeLayout withfile;
        @BindView(R.id.nofile)
        RelativeLayout nofile;
        @BindView(R.id.d_lyt)
        RelativeLayout d_lyt;
        @BindView(R.id.empty_file)
        ImageView empty_file;
        @BindView(R.id.classess)
        TextView classess;
        @BindView(R.id.id)
        TextView id;
        SwipeLayout swipeLayout;
        public ImageView trash, update;
        @BindView(R.id.tap_view_pdf)
        TextView tap_view_pdf;
        @BindView(R.id.tap_view_pic)
        TextView tap_view_pic;
        @BindView(R.id.tap_view_audio)
        TextView tap_view_audio;
        @BindView(R.id.addbyid)
        TextView addbyid;

        public Viewholder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            sub = (TextView) itemView.findViewById(R.id.subject);
            u_date = (TextView) itemView.findViewById(R.id.u_date);
            homeTopic = (TextView) itemView.findViewById(R.id.homeTopic);
            image_file = (ImageView) itemView.findViewById(R.id.image_file);
            pdf_file = (ImageView) itemView.findViewById(R.id.pdf_file);
            audio_file = (ImageView) itemView.findViewById(R.id.audio_file);
            withfile = (RelativeLayout) itemView.findViewById(R.id.head);
            swipeLayout = (SwipeLayout) itemView.findViewById(R.id.swipe);
            trash = (ImageView) itemView.findViewById(R.id.trash);
            update = (ImageView) itemView.findViewById(R.id.update);
        }
    }

    public interface Eventcallback {
        public void onUpdateCallback(Classworkbean classworkbean);

        public void onDelecallback(Classworkbean classworkbean);

        public void onMethod_pdf_call_back(Classworkbean classworkbean);
        public void onMethod_Image_callback(Classworkbean homeworkbean);

        void DisplayImage(final Classworkbean obj);
    }
}
