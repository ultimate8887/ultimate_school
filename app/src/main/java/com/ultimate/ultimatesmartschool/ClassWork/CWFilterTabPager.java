package com.ultimate.ultimatesmartschool.ClassWork;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.ultimate.ultimatesmartschool.ClassWork.FragmentsCw.StaffCW_DateWise;
import com.ultimate.ultimatesmartschool.ClassWork.FragmentsCw.StaffCW_MonthWise;
import com.ultimate.ultimatesmartschool.Homework.Fragments.StaffHW_DateWise;
import com.ultimate.ultimatesmartschool.Homework.Fragments.StaffHW_MonthWise;

public class CWFilterTabPager extends FragmentStatePagerAdapter {

    public CWFilterTabPager(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new StaffCW_DateWise();
            case 1:
                return new StaffCW_MonthWise();
            default:
                break;
        }
        return null;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        // Generate title based on item position
        switch (position) {
            case 0:
                return "View Date Wise";
            case 1:
                return "View Month Wise";
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return 2;
    }
}
