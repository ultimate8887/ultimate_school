package com.ultimate.ultimatesmartschool.ClassWork;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;
import androidx.viewpager.widget.ViewPager;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.text.Html;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewSwitcher;


import com.android.volley.Request;
import com.android.volley.toolbox.ImageLoader;
import com.bumptech.glide.Glide;
import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetSequence;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.Gson;
import com.kroegerama.imgpicker.BottomSheetImagePicker;
import com.kroegerama.imgpicker.ButtonType;
import com.ultimate.ultimatesmartschool.BeanModule.ClassBean;
import com.ultimate.ultimatesmartschool.BeanModule.SectionBean;
import com.ultimate.ultimatesmartschool.BeanModule.SubjectBeancls;
import com.ultimate.ultimatesmartschool.BeanModule.User;
import com.ultimate.ultimatesmartschool.Ebook.WebViewActivity;
import com.ultimate.ultimatesmartschool.Examination.ClassAdapter;
import com.ultimate.ultimatesmartschool.Homework.AdapterOfSubjectList;
import com.ultimate.ultimatesmartschool.Homework.AdapterOfSubjectListN;
import com.ultimate.ultimatesmartschool.Homework.GETHomeworkActivity;
import com.ultimate.ultimatesmartschool.Homework.Homeworkadapter;
import com.ultimate.ultimatesmartschool.Homework.HomeworkadapterNew;
import com.ultimate.ultimatesmartschool.Homework.Homeworkbean;
import com.ultimate.ultimatesmartschool.Homework.SectionnewAdapter;
import com.ultimate.ultimatesmartschool.Login.LoginActivity;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.CommonProgress;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;
import com.ultimate.ultimatesmartschool.Utility.PaginationListener;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.apptik.widget.multiselectspinner.BaseMultiSelectSpinner;
import io.apptik.widget.multiselectspinner.MultiSelectSpinner;
import me.relex.circleindicator.CircleIndicator;

public class ViewClassWork extends AppCompatActivity implements Classworkadapter.Eventcallback{
    ArrayList<ClassBean> classList = new ArrayList<>();
    @BindView(R.id.parent)
    RelativeLayout parent;

    public ViewPager intro_images;
    LinearLayout pager_indicator;
    public CircleIndicator indicator;
    public TextView textView;
    //@BindView(R.id.spinner) Spinner spinnerClass;
//    @BindView(R.id.multiselectSpinnerclass)
//    MultiSelectSpinner multiselectSpinnerclass;
    Spinner spinnerClass;
    String image_url="",school="",className = "";
    String classid = "";
    private Classworkadapter adapter;
    ArrayList<Classworkbean> hwList = new ArrayList<>();
    private LinearLayoutManager layoutManager;

    CommonProgress commonProgress;
    private int loaded = 0;
    int check=0;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.totalRecord)
    TextView totalRecord;
    List<String> classidsel;
    //    @BindView(R.id.textView7)
//    TextView textView7;
    Spinner spinnersection;
    ArrayList<SectionBean> sectionList = new ArrayList<>();
    String sectionid="";
    String sectionname;
    @BindView(R.id.recyclerViewmsg)
    RecyclerView recyclerview;
    @BindView(R.id.textNorecord)
    TextView txtNorecord;
    ArrayList<SubjectBeancls> subjectList = new ArrayList<>();
    AdapterOfSubjectListN adaptersub;
    String sub_id, sub_name;
    @BindView(R.id.spinner)
    Spinner spinnersubject;
    Dialog mBottomSheetDialog;
    @BindView(R.id.adddate)TextView adddate;
    String type="1",name="",date="",view_type="class",filter_id="",tdate="",ydate="";

    @BindView(R.id.dialog)
    ImageView dialog;
    SharedPreferences sharedPreferences;

    @BindView(R.id.main_progress)
    ProgressBar main_progress;
    int  limit=100,page_limit=0,total_pages=0;
    private static final int PAGE_START = 1;
    private int currentPage = PAGE_START;
    private boolean isLastPage = false;
    private boolean isLoading = false;
    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_g_e_t_homework);

        ButterKnife.bind(this);
        commonProgress=new CommonProgress(this);
        classidsel = new ArrayList<>();
        recyclerview.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(ViewClassWork.this);
        recyclerview.setLayoutManager(layoutManager);
        adapter = new Classworkadapter(hwList, ViewClassWork.this, this);
        recyclerview.setAdapter(adapter);
        getTodayDate();

        if (!restorePrefData()){
            setShowcaseView();
        }else {
            openDialog();
        }

        recyclerview.addOnScrollListener(new PaginationListener(layoutManager) {
            @Override
            protected void loadMoreItems() {
                // isLoading = true;
                //  Utils.showSnackBar("No more messages found in message list.", parent);
                //  currentPage++;
                // doApiCall();

                if (total_pages>=page_limit) {
                    main_progress.setVisibility(View.VISIBLE);
                    currentPage += 10;
                    page_limit = currentPage + limit;

                    if (sub_id.equalsIgnoreCase("")){
                        fetchClasswork(date,0,sub_id, page_limit, "no");
                    }else{
                        fetchClasswork(date,5,sub_id, page_limit, "no");
                    }
                }else{
                    // main_progress.setVisibility(View.GONE);
                    //  Utils.showSnackBar("No more messages found in message list.", parent);
                }


            }
            @Override
            public boolean isLastPage() {
                return isLastPage;
            }
            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });

        // fetchSection();
        //  txtTitle.setText("Homework");
        school=User.getCurrentUser().getSchoolData().getName();
    }

//    private void fetchClass() {
//        if (loaded == 0) {
//            ErpProgress.showProgressBar(this, "Please wait...");
//        }
//        loaded++;
//
//        HashMap<String, String> params = new HashMap<String, String>();
//        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.CLASSLIST_URL, classapiCallback, this, params);
//    }
//
//    ApiHandler.ApiCallback classapiCallback = new ApiHandler.ApiCallback() {
//        @Override
//        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
//            ErpProgress.cancelProgressBar();
//            if (error == null) {
//                try {
//                    if (classList != null) {
//                        classList.clear();
//                    }
//                    classList = ClassBean.parseClassArray(jsonObject.getJSONArray(Constants.CLASSDATA));
//                    List<String> dstring = new ArrayList<>();
//                    for (ClassBean cobj : classList) {
//                        dstring.add(cobj.getName());
//                        Log.e("getclassid",cobj.getId());
//                    }
//
//                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(ViewClassWork.this, android.R.layout.simple_list_item_multiple_choice, dstring);
//
//                    multiselectSpinnerclass.setListAdapter(adapter).setListener(new BaseMultiSelectSpinner.MultiSpinnerListener() {
//                        @Override
//                        public void onItemsSelected(boolean[] selected) {
//
//                            Log.e("dcfcds", "fdsf");
//                            String value = multiselectSpinnerclass.getSpinnerText();
//                            List<String> clas = new ArrayList<>();
//                            classidsel.clear();
//                            if (!value.isEmpty()) {
//                                clas = Arrays.asList(value.split("\\s*,\\s*"));
//                            }
//                            for (String dval : clas) {
//                                for (ClassBean obj : classList) {
//                                    if (obj.getName().equalsIgnoreCase(dval)) {
//                                        classidsel.add(obj.getId());
//                                        //  textView7.setVisibility(View.GONE);
//
//                                        break;
//                                    }
//                                }
//                            }
//                            fetchSubject();
//                            //fetchHomework();
//                            Log.e("dcfcds", value + "  ,  classid:  " + classidsel);
//                            // }
//                        }
//                    }).setSelectAll(false)
//                            .setSpinnerItemLayout(R.layout.support_simple_spinner_dropdown_item)
//                            .setTitle("Select Class")
//                            .setMaxSelectedItems(1)
//                    ;
//
//
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//
//            } else {
//                Utils.showSnackBar(error.getMessage(), parent);
//                if (error.getStatusCode() == 405) {
//                    User.logout();
//                    startActivity(new Intent(ViewClassWork.this, LoginActivity.class));
//                    finish();
//                }
//            }
//        }
//    };

    public void getTodayDate() {
        java.util.Calendar c = java.util.Calendar.getInstance();
        c.set(java.util.Calendar.HOUR_OF_DAY, 00);
        c.set(java.util.Calendar.MINUTE, 00);
        c.set(java.util.Calendar.SECOND, 00);
        Date setdate = c.getTime();
        java.text.SimpleDateFormat fmtOut = new java.text.SimpleDateFormat("dd MMM, yyyy");
        String dateString = fmtOut.format(setdate);
        //  adddate.setText(dateString);
        java.text.SimpleDateFormat dateFrmOut = new java.text.SimpleDateFormat("yyyy-MM-dd");
        tdate = dateFrmOut.format(setdate);
        //for Yesterday
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);
        ydate= dateFormat.format(cal.getTime());
    }

    @OnClick(R.id.filelayout)
    public void fetchDate() {
        java.util.Calendar cal = java.util.Calendar.getInstance(TimeZone.getDefault()); // Get current date
        DatePickerDialog datePicker = new DatePickerDialog(ViewClassWork.this,
                R.style.MyDatePickerDialogTheme, ondate,
                cal.get(java.util.Calendar.YEAR),
                cal.get(java.util.Calendar.MONTH),
                cal.get(java.util.Calendar.DAY_OF_MONTH));
        datePicker.getDatePicker().setMaxDate(new java.util.Date().getTime());
        datePicker.setCancelable(false);
        datePicker.show();
    }

    DatePickerDialog.OnDateSetListener ondate = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            java.util.Calendar calendar = java.util.Calendar.getInstance(TimeZone.getDefault());
            calendar.set(year, monthOfYear, dayOfMonth);
            calendar.set(Calendar.HOUR_OF_DAY, 00);
            calendar.set(Calendar.MINUTE, 00);
            calendar.set(Calendar.SECOND, 00);
            java.util.Date setdate = calendar.getTime();
            SimpleDateFormat fmtOut = new SimpleDateFormat("dd MMM, yyyy");
            String dateString = fmtOut.format(setdate);
            //adddate.setText(dateString);
            SimpleDateFormat dateFrmOut = new SimpleDateFormat("yyyy-MM-dd");
            date = dateFrmOut.format(setdate);
            if (tdate.equalsIgnoreCase(date)){
                adddate.setText("Today");
                adddate.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.light_red));
            } else if (ydate.equalsIgnoreCase(date)){
                adddate.setText("Yesterday");
                adddate.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.green));
            }else {
                adddate.setText(dateString);
                adddate.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
            }
            page_limit=0;
            fetchSubject(limit);
        }
    };


    @RequiresApi(api = Build.VERSION_CODES.N)
    @OnClick(R.id.dialog)
    public void dialogggg() {
        openDialog();
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void openDialog() {

        mBottomSheetDialog = new BottomSheetDialog(this);
        final View sheetView = getLayoutInflater().inflate(R.layout.search_dialog, null);
        mBottomSheetDialog.setContentView(sheetView);
        mBottomSheetDialog.setCancelable(false);

        spinnerClass=(Spinner) sheetView.findViewById(R.id.spinnerClass);
        spinnersection=(Spinner) sheetView.findViewById(R.id.spinnersection);
        ImageView close=(ImageView) sheetView.findViewById(R.id.close);
        TextView txtSetup=(TextView) sheetView.findViewById(R.id.txtSetup);
        txtSetup.setText("View Class-Work");
        //txtTitle.setText("Mark Attendance");
        fetchClass();
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //assignData();
                if (check==0) {
                    finish();
                    mBottomSheetDialog.dismiss();
                }else {
                    mBottomSheetDialog.dismiss();
                }
            }
        });


        Button btnYes= (Button) sheetView.findViewById(R.id.btnYes);


        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (classid.equalsIgnoreCase(""))  {
                    Toast.makeText(ViewClassWork.this,"Kindly Select Class!", Toast.LENGTH_SHORT).show();
                }  else if (sectionid.equalsIgnoreCase("")){
                    Toast.makeText(ViewClassWork.this,"Kindly Select Class Section!", Toast.LENGTH_SHORT).show();
                }else {
                    check++;
                    page_limit=0;
                    fetchSubject(limit);
                    setCommonData();
                }

            }
        });
        mBottomSheetDialog.show();

    }

    private void setCommonData() {
        txtTitle.setText(className+"("+sectionname+")" +" class");
        //  txtSub.setText(sub_name);
    }

    private void setShowcaseView() {
        new TapTargetSequence(this)
                .targets(TapTarget.forView(dialog,"Search Button!","Tap the Search button to Search Class-Wise Homework.")
                        .outerCircleColor(R.color.light)
                        .outerCircleAlpha(0.96f)
                        .targetCircleColor(R.color.white)
                        .titleTextSize(20)
                        .titleTextColor(R.color.white)
                        .descriptionTextSize(14)
                        .descriptionTextColor(R.color.black)
                        .textColor(R.color.black)
                        .textTypeface(Typeface.SANS_SERIF)
                        .dimColor(R.color.black)
                        .drawShadow(true)
                        .cancelable(false)
                        .tintTarget(true)
                        .transparentTarget(true)
                        .targetRadius(60)).listener(new TapTargetSequence.Listener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onSequenceFinish() {
                // Toast.makeText(getApplicationContext(),"Sequence Finished",Toast.LENGTH_SHORT).show();
                savePrefData();
                openDialog();
            }

            @Override
            public void onSequenceStep(TapTarget lastTarget, boolean targetClicked) {
                // Toast.makeText(getActivity(),"GREAT!",Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSequenceCanceled(TapTarget lastTarget) {

            }
        }).start();
    }

    private void savePrefData(){
        sharedPreferences=this.getSharedPreferences("boarding_pref_add_attend_search",MODE_PRIVATE);
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putBoolean("IsFirstTimeVisit_add_attend_search",true);
        editor.apply();
    }

    private boolean restorePrefData(){
        sharedPreferences = this.getSharedPreferences("boarding_pref_add_attend_search",MODE_PRIVATE);
        return sharedPreferences.getBoolean("IsFirstTimeVisit_add_attend_search",false);
    }

    private void fetchClass() {
        if (loaded == 0) {
            commonProgress.show();
        }
        loaded++;

        HashMap<String, String> params = new HashMap<String, String>();
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.CLASSLIST_URL, classapiCallback,getApplicationContext(), params);
    }


    ApiHandler.ApiCallback classapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            commonProgress.dismiss();
            if (error == null) {
                try {
                    classList = ClassBean.parseClassArray(jsonObject.getJSONArray(Constants.CLASSDATA));
                    ClassAdapter adapter = new ClassAdapter(getApplicationContext(), classList);
                    spinnerClass.setAdapter(adapter);
                    spinnerClass.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if (i > 0) {
                                sub_id= "";
                                sub_name= "" ;
                                sectionid="";
                                sectionname="";
                                classid = "";
                                className = "";
                                classid = classList.get(i - 1).getId();
                                className= classList.get(i - 1).getName();
                                fetchSection();
                            }

                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_LONG).show();
                // Utils.showSnackBar(error.getMessage(), view.findViewById(R.id.parent));
            }
        }
    };

    private void fetchSubject(int limit) {
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("id", classid);
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.SubjectList, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                mBottomSheetDialog.dismiss();
                if (error == null) {
                    try {

                        subjectList =   SubjectBeancls.parseClassArray(jsonObject.getJSONArray("sub_data"));

                        adaptersub = new AdapterOfSubjectListN(ViewClassWork.this, subjectList);
                        spinnersubject.setAdapter(adaptersub);
                        spinnersubject.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                sub_id="";
                                if (i > 0) {
                                    sub_name = subjectList.get(i - 1).getName();
                                    sub_id = subjectList.get(i - 1).getId();
                                    fetchClasswork(date,5,sub_id, limit, "yes");
                                }else {
                                    fetchClasswork(date,0,sub_id, limit, "yes");
                                }

                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> adapterView) {

                            }
                        });

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }, this, params);

    }

    private void fetchSection() {
        HashMap<String, String> params = new HashMap<String, String>();
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.SECTIONLIST_URL, sectionapiCallback, this, params);
    }

    ApiHandler.ApiCallback sectionapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            if (error == null) {
                try {
                    sectionList = SectionBean.parseCommonArray(jsonObject.getJSONArray("hostelroom_data"));
                    SectionnewAdapter adapter = new SectionnewAdapter(ViewClassWork.this, sectionList);
                    spinnersection.setAdapter(adapter);
                    spinnersection.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if (i > 0) {
                                sectionid = sectionList.get(i - 1).getSection_id();
                                sectionname = sectionList.get(i - 1).getSection_name();

                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Utils.showSnackBar(error.getMessage(), parent);
                if (error.getStatusCode() == 405) {
                    User.logout();
                    startActivity(new Intent(ViewClassWork.this, LoginActivity.class));
                    finish();
                }
            }
        }
    };


    public void fetchClasswork(String date, int i,String sub_id,int limit,String progress) {
        if (progress.equalsIgnoreCase("yes")){
            commonProgress.show();
        }
        HashMap<String, String> params = new HashMap<String, String>();

        if (i == 0){
            params.put("class_id", classid);
            params.put("sectionid", sectionid);
        } else {
            params.put("class_id", classid);
            params.put("sectionid", sectionid);
            params.put("sub_id", sub_id);
        }
        params.put("today",date);
        params.put("view_type",view_type);
        params.put("page_limit",String.valueOf(limit));

        //params.put("class_id", classid);
//        params.put("class_id", String.valueOf(classidsel));
//        params.put("sectionid", sectionid);
//        params.put("sub_id", sub_id);
//        Log.e("whatclassidsel",String.valueOf(classidsel));

        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.classworkGDS, apiCallback, this, params);
    }
    ApiHandler.ApiCallback apiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            commonProgress.dismiss();
            if (error == null) {
                try {
                    if (hwList != null) {
                        hwList.clear();
                    }
                    JSONArray jsonArray = jsonObject.getJSONArray("cw_data");
                    hwList = Classworkbean.parseHWArray(jsonArray);
                    total_pages= Integer.parseInt(hwList.get(0).getRowcount());
                    if (hwList.size() > 0) {
                        adapter.setHQList(hwList);
                        adapter.notifyDataSetChanged();
                        txtNorecord.setVisibility(View.GONE);
                        totalRecord.setText("Total Entries:- "+String.valueOf(hwList.size()));
                    } else {
                        totalRecord.setText("Total Entries:- 0");
                        adapter.setHQList(hwList);
                        adapter.notifyDataSetChanged();
                        txtNorecord.setVisibility(View.VISIBLE);
                    }
                    main_progress.setVisibility(View.GONE);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                totalRecord.setText("Total Entries:- 0");
                txtNorecord.setVisibility(View.VISIBLE);
                hwList.clear();
                adapter.setHQList(hwList);
                adapter.notifyDataSetChanged();
            }
        }
    };

    @Override
    public void onUpdateCallback(Classworkbean classworkbean) {
//        Gson gson = new Gson();
//        String classworkdata = gson.toJson(classworkbean, Classworkbean.class);
//        Intent intent = new Intent(ViewClassWork.this, UpdateClasswork.class);
//        intent.putExtra("classworkdata", classworkdata);
//        startActivity(intent);
    }

    @Override
    public void onDelecallback(Classworkbean classworkbean) {
        commonProgress.show();
        HashMap<String, String> params = new HashMap<>();
        params.put("classworkid", classworkbean.getId());
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.DELETECWGDSURL_URL, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                ErpProgress.cancelProgressBar();
                commonProgress.dismiss();
                if (error == null) {
                    int pos = hwList.indexOf(classworkbean);
                    hwList.remove(pos);
                    adapter.setHQList(hwList);
                    adapter.notifyDataSetChanged();
                    Utils.showSnackBar("Deleted Successfully!", parent);
                    totalRecord.setText("Total Entries:- "+String.valueOf(hwList.size()));
                } else {
                    Log.e("error", error.getMessage() + "");
                    Utils.showSnackBar(error.getMessage(), parent);

                    if (error.getStatusCode() == 405) {
                        User.logout();
                        startActivity(new Intent(ViewClassWork.this, LoginActivity.class));
                        finish();
                    }
                }
            }
        }, this, params);
    }

    @Override
    public void onMethod_pdf_call_back(Classworkbean classworkbean) {
        Intent intent = new Intent(ViewClassWork.this, WebViewActivity.class);
        intent.putExtra("MY_kEY",classworkbean.getPdf_file());
        startActivity(intent);
    }

    @Override
    public void onMethod_Image_callback(Classworkbean data) {
        final Dialog warningDialog = new Dialog(this);
        warningDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        warningDialog.setCancelable(true);

        warningDialog.setContentView(R.layout.class_img_dialog);
        warningDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        RelativeLayout btnClose = (RelativeLayout) warningDialog.findViewById(R.id.btnNo);

        indicator = (CircleIndicator) warningDialog.findViewById(R.id.indicator);
        textView=(TextView) warningDialog.findViewById(R.id.clsswrktexttt);
        intro_images=(ViewPager) warningDialog.findViewById(R.id.pager_introduction);

        TextView tap_count=(TextView) warningDialog.findViewById(R.id.tap_count);
        String title1 = getColoredSpanned("<b>"+"1"+"</b>"+"", "#F4212C");
        String l_Name1 = getColoredSpanned(" / "+String.valueOf(data.getImage().size()), "#000000");
        tap_count.setText(Html.fromHtml(title1 + " " + l_Name1+ " "));

        textView.setText(data.getInformation());
        CWViewPagerAdapter mAdapter = new  CWViewPagerAdapter(this,data.getImage(),data.getMsg());
        intro_images.setAdapter(mAdapter);
        intro_images.setCurrentItem(0);
        indicator.setViewPager(intro_images);

        intro_images.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int arg0) {
                // TODO Auto-generated method stub
                int c=arg0+1;
                String title1 = getColoredSpanned("<b>"+String.valueOf(c)+"</b>"+"", "#F4212C");
                String l_Name1 = getColoredSpanned(" / "+String.valueOf(data.getImage().size()), "#000000");
                tap_count.setText(Html.fromHtml(title1 + " " + l_Name1+ " "));
            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
                // TODO Auto-generated method stub

                System.out.println("onPageScrolled");
            }

            @Override
            public void onPageScrollStateChanged(int num) {
                // TODO Auto-generated method stub

            }
        });


        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                warningDialog.dismiss();
            }
        });


        warningDialog.show();
        Window window = warningDialog.getWindow();
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    }

    @Override
    public void DisplayImage(Classworkbean data) {


    }
    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }

    @OnClick(R.id.imgBack)
    public void onback() {
        finish();
    }
}
