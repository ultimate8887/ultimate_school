package com.ultimate.ultimatesmartschool.ClassWork;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;
import com.ultimate.ultimatesmartschool.Homework.HW_ReportAdapter;
import com.ultimate.ultimatesmartschool.Homework.Homeworkbean;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.relex.circleindicator.CircleIndicator;

public class CW_ReportAdapter extends RecyclerSwipeAdapter<CW_ReportAdapter.Viewholder> {
    ArrayList<Classworkbean> hq_list;
    Context listner;
    int value;

    public CW_ReportAdapter(ArrayList<Classworkbean> hq_list, Context listener, int value) {
        this.value = value;
        this.hq_list = hq_list;
        this.listner = listener;
    }


    @Override
    public CW_ReportAdapter.Viewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.report_hw_lay, parent, false);
        CW_ReportAdapter.Viewholder viewholder = new CW_ReportAdapter.Viewholder(view);
        return viewholder;

    }

    @Override
    public void onBindViewHolder(final CW_ReportAdapter.Viewholder holder, final int position) {
        final Classworkbean data = hq_list.get(position);
        holder.txtRoll.setText(data.getClass_name()+"("+data.getSection_name()+")");
        holder.txtsno.setText(data.getId());
        holder.txtName.setText(data.getStaff_name());

        if (value==1){
          //  holder.txtAttend.setText(Utils.getMonthFormated(data.getDate()));
            holder.txtAttend.setText(Utils.getDateTimeFormatedWithAMPM(data.getDate()));
        }else {
            holder.txtAttend.setText(Utils.getDateTimeFormatedWithAMPM(data.getDate()));
        }


    }

    @Override
    public int getItemCount() {
        return hq_list.size();
    }

    public void setHQList(ArrayList<Classworkbean> hq_list) {
        this.hq_list = hq_list;
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe;
    }


    public class Viewholder extends RecyclerView.ViewHolder {
        @BindView(R.id.txtAttend)
        TextView txtAttend;
        @BindView(R.id.txtName)
        TextView txtName;
        @BindView(R.id.txtRoll)
        TextView txtRoll;
        @BindView(R.id.txtFName)
        TextView txtFName;
        @BindView(R.id.txtsno)
        TextView txtsno;
        public Viewholder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
