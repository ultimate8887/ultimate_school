package com.ultimate.ultimatesmartschool.ClassWork;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import com.android.volley.Request;
import com.bumptech.glide.Glide;
import com.kroegerama.imgpicker.BottomSheetImagePicker;
import com.ultimate.ultimatesmartschool.BeanModule.ClassBean;
import com.ultimate.ultimatesmartschool.BeanModule.SectionBean;
import com.ultimate.ultimatesmartschool.BeanModule.SubjectBeancls;
import com.ultimate.ultimatesmartschool.BeanModule.User;
import com.ultimate.ultimatesmartschool.Homework.AdapterOfSubjectList;
import com.ultimate.ultimatesmartschool.Homework.AddHomeworkActivity;
import com.ultimate.ultimatesmartschool.Homework.SectionnewAdapter;
import com.ultimate.ultimatesmartschool.Login.LoginActivity;
import com.ultimate.ultimatesmartschool.MainActivity;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;
import com.ultimate.ultimatesmartschool.Utility.Utils;


import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.apptik.widget.multiselectspinner.BaseMultiSelectSpinner;
import io.apptik.widget.multiselectspinner.MultiSelectSpinner;

public class AddClassworkActivity extends AppCompatActivity implements BottomSheetImagePicker.OnImagesSelectedListener{
    ArrayList<ClassBean> classList = new ArrayList<>();
    @BindView(R.id.parent)
    RelativeLayout parent;
    //@BindView(R.id.spinner) Spinner spinnerClass;
    @BindView(R.id.multiselectSpinnerclass)
    MultiSelectSpinner multiselectSpinnerclass;
    String classid = "";
    private ViewGroup imageContainer;
    ArrayList<String> image;
int abc;
    private int loaded = 0;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    List<String> classidsel;
    @BindView(R.id.textView7)TextView textView7;
    @BindView(R.id.spinnersection)
    Spinner spinnersection;
    ArrayList<SectionBean> sectionList = new ArrayList<>();
    String sectionid="";
    String sectionname;
    private int type;
    @BindView(R.id.textnote)TextView textnote;
    private Bitmap hwbitmap;
    @BindView(R.id.spinnersubject)Spinner spinnersubject;
    ArrayList<SubjectBeancls> subjectList = new ArrayList<>();
    AdapterOfSubjectList adaptersub;
    String sub_id, sub_name;
    @BindView(R.id.commenthmwrk)
    EditText commenthmwrk;
    String encoded;
    @BindView(R.id.imgBack)ImageView back;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_classwork);
        ButterKnife.bind(this);
        parent.setAnimation(AnimationUtils.loadAnimation(getApplicationContext(),R.anim.fade_scale_animation));
        imageContainer = findViewById(R.id.imageContainer);
        classidsel = new ArrayList<>();
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        fetchSection();
        fetchClass();
        txtTitle.setText("Classwork");
    }
    private void fetchClass() {
        if (loaded == 0) {
            ErpProgress.showProgressBar(this, "Please wait...");
        }
        loaded++;

        HashMap<String, String> params = new HashMap<String, String>();
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.CLASSLIST_URL, classapiCallback, this, params);
    }

    ApiHandler.ApiCallback classapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            if (error == null) {
                try {
                    if (classList != null) {
                        classList.clear();
                    }
                    classList = ClassBean.parseClassArray(jsonObject.getJSONArray(Constants.CLASSDATA));
                    List<String> dstring = new ArrayList<>();
                    for (ClassBean cobj : classList) {
                        dstring.add(cobj.getName());
                        Log.e("getclassid",cobj.getId());
                    }

                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(AddClassworkActivity.this, android.R.layout.simple_list_item_multiple_choice, dstring);

                    multiselectSpinnerclass.setListAdapter(adapter).setListener(new BaseMultiSelectSpinner.MultiSpinnerListener() {
                        @Override
                        public void onItemsSelected(boolean[] selected) {

                            Log.e("dcfcds", "fdsf");
                            String value = multiselectSpinnerclass.getSpinnerText();
                            List<String> clas = new ArrayList<>();
                            classidsel.clear();
                            if (!value.isEmpty()) {
                                clas = Arrays.asList(value.split("\\s*,\\s*"));
                            }
                            for (String dval : clas) {
                                for (ClassBean obj : classList) {
                                    if (obj.getName().equalsIgnoreCase(dval)) {
                                        classidsel.add(obj.getId());
                                        //  textView7.setVisibility(View.GONE);

                                        break;
                                    }
                                }
                            }
                            fetchSubject();
                            //fetchHomework();
                            Log.e("dcfcds", value + "  ,  classid:  " + classidsel);
                            // }
                        }
                    }).setSelectAll(false)
                            .setSpinnerItemLayout(androidx.appcompat.R.layout.support_simple_spinner_dropdown_item)
                            .setTitle("Select Class")

                            .setMaxSelectedItems(1)
                    ;


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            } else {
                Utils.showSnackBar(error.getMessage(), parent);
                if (error.getStatusCode() == 405) {
                    User.logout();
                    startActivity(new Intent(AddClassworkActivity.this, LoginActivity.class));
                    finish();
                }
            }
        }
    };

    private void fetchSubject() {
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("id", String.valueOf(classidsel));
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.SubjectList, subapiCallback, this, params);

    }

    ApiHandler.ApiCallback subapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            /* */

            if (error == null) {
                try {

                    subjectList =   SubjectBeancls.parseClassArray(jsonObject.getJSONArray("sub_data"));

                    adaptersub = new AdapterOfSubjectList(AddClassworkActivity.this, subjectList);
                    spinnersubject.setAdapter(adaptersub);
                    spinnersubject.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if (i != 0) {
                                sub_name = subjectList.get(i - 1).getName();
                                sub_id = subjectList.get(i - 1).getId();

                            }

                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    };
    private void fetchSection() {
        HashMap<String, String> params = new HashMap<String, String>();
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.SECTIONLIST_URL, sectionapiCallback, this, params);
    }

    ApiHandler.ApiCallback sectionapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            if (error == null) {
                try {
                    sectionList = SectionBean.parseCommonArray(jsonObject.getJSONArray("hostelroom_data"));
                    SectionnewAdapter adapter = new SectionnewAdapter(AddClassworkActivity.this, sectionList);
                    spinnersection.setAdapter(adapter);
                    spinnersection.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if (i > 0) {
                                sectionid = sectionList.get(i - 1).getSection_id();
                                sectionname = sectionList.get(i - 1).getSection_name();
                                // fetchHomework();
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Utils.showSnackBar(error.getMessage(), parent);
                if (error.getStatusCode() == 405) {
                    User.logout();
                    startActivity(new Intent(AddClassworkActivity.this, LoginActivity.class));
                    finish();
                }
            }
        }
    };
       @SuppressLint("ResourceType")
    @OnClick(R.id.addimage)
    public void click() {
        new BottomSheetImagePicker.Builder(getString(R.xml.provider_paths))
                .multiSelect(2, 6)
                .multiSelectTitles(
                        R.plurals.pick_multi,
                        R.plurals.pick_multi_more,
                        R.string.pick_multi_limit
                )
                .peekHeight(R.dimen.peekHeight)
                .columnSize(R.dimen.columnSize)
                .requestTag("multi")
                .show(getSupportFragmentManager(), null);
    }


    @Override
    public void onImagesSelected(List<? extends Uri> list, String s) {
        imageContainer.removeAllViews();
        image= new ArrayList<>();
        for (Uri uri : list) {
            ImageView iv = (ImageView) LayoutInflater.from(this).inflate(R.layout.scrollitem_image, imageContainer, false);
            imageContainer.addView(iv);
            Glide.with(this).load(uri).into(iv);
            hwbitmap = Utils.getBitmapFromUri(AddClassworkActivity.this, uri, 2048);
            encoded = Utils.encodeToBase64(hwbitmap, Bitmap.CompressFormat.JPEG, 50);
            // encoded = String.format("data:image/jpeg;base64,%s", encoded);
            image.add(encoded);

        }
        textnote.setText("Image: Your selected images shown above." );

    }
    @OnClick(R.id.button3)
    public void save() {

        ErpProgress.showProgressBar(AddClassworkActivity.this,"Please wait...");
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("image", String.valueOf(image));
        Log.e("image", String.valueOf(image));
        params.put("msg", "msg");
        params.put("info", commenthmwrk.getText().toString());
        params.put("class_id", String.valueOf(classidsel));
        params.put("sectionid", sectionid);
        params.put("sub_id", sub_id);
        params.put("user_id", User.getCurrentUser().getId());
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.addclassworkGDS, apiCallback, this, params);

    }
    ApiHandler.ApiCallback apiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            if (error == null) {
                try {
                    Utils.showSnackBar(jsonObject.getString("msg"), parent);
                    finish();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("error", error.getMessage() + "");
                Utils.showSnackBar(error.getMessage(), parent);
                if (error.getStatusCode() == 405) {
                    User.logout();
                    startActivity(new Intent(AddClassworkActivity.this, LoginActivity.class));
                    finish();
                }
            }
        }
    };

}
