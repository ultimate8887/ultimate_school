package com.ultimate.ultimatesmartschool.Utility;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Handler;
import android.os.Message;



public class ErpProgressVert {

    private static ProgressDialog progressDialog;

    public static void showProgressBar(Context context, String msg) {
        Handler handle = new Handler() {
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                progressDialog.incrementProgressBy(2); // Incremented By Value 2
            }
        };

        progressDialog = new ProgressDialog(context);
        progressDialog.setMax(100);
        progressDialog.setMessage("Please wait! till uploading...");
        progressDialog.setTitle("Uploading in progress");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progressDialog.show();
        progressDialog.setCancelable(false);
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    while (progressDialog.getProgress() <= progressDialog
                            .getMax()) {
                        Thread.sleep(300);
                        handle.sendMessage(handle.obtainMessage());
                        if (progressDialog.getProgress() == progressDialog
                                .getMax()) {
//                            progressDialog.dismiss();
                            progressDialog.setTitle("Kindly Keep Patience");
                            progressDialog.setMessage("Uploading almost done....!");
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();



}


    public static void cancelProgressBar() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }
}
