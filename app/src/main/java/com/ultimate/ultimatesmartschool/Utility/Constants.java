package com.ultimate.ultimatesmartschool.Utility;


import com.ultimate.ultimatesmartschool.BeanModule.User;
import androidx.multidex.BuildConfig;

public class Constants {
    public static final int LOCATION_PERMISSION_CODE = 102;
    public static final String USERDATA = "user_data";
    public static final String ADDHOLIDAY_URL = "addholiday.php";
    public static final String NOTI_URL = "noti_tesing.php";
    public static final String INSTRUCTION = "instruction.php";
    public static String VIDEO_PATH = "office_admin/upload/ebooks/";
    public static final String UPDATE_STAFFURL = "update_staff.php";
    public static final String ADD_DOC = "add_document.php";
    public static final String STUDENT_ATTEND_URL = "student_attend";
    public static final String STUDENT_ATTEND_LIST = "StudAttendList";
    public static final String FEEDBACK_URL = "feedback";
    public static final String DOC_URL ="office_admin/images/";
    public static final String APPROVE_STAFFPASS ="approve_staff_gatepass.php";
    public static final String APPROVE_STDPASS ="approve_std_gatepass.php";
    public static final String STDGATEPASS_URL ="student_gatepass.php?";
    public static final String INCHARGE ="inchargelist.php";
    public static final String ADDINCHRGE ="assign_incharge.php";
    public static final String ASSIGNSUBJECT ="assign_subjectlist.php";
    public static final String ADDSUB ="assign_subject.php";
    public static final String NEWS_URL ="news.php";
    public static final String EXPENSE_URL = "add_expense.php";
    public static final String UPDATE_IMG ="update_image.php";
    public static final String UPDATE_MOB_INFO ="update_mobile_info.php";
    public static final String CHECK_MONTHLY ="check_monthly_fees.php";
    public static final String CHECK_FEES_CLASS ="fetch_class_fees.php";
    public static final String ADDFEES ="pay_fees.php";
    public static final String ADDFEESN ="pay_fees_new.php";
    public static final String FEE_PAID_STUDENT ="feepaidlist.php";
    public static final String FEE_PAID_STUDENT_NEW ="feepaidlist_new.php";
    public static final String STUDENT_ATTEND_LISTGDS_SUB ="StudAttendListgds_sub.php";
    public static final String USER_PERMISSION ="admin_permission.php";
    public static final String USER_LIST ="userlist.php";
    public static final String ADD_USER ="add_user.php";
    public static final String UPDATE_SCHOOL_INFO ="update_schoolinfo.php";
    public static final String DATE = "&today=";

    public static String sea="SEA";
    public static String sea_d="Subject Enrichment Activity";

    public static final String ACTIVITY = "School Activity";
    public static String questions = "Question Paper";
    public static final String EDIT_CATEGORY_URL = "updategroup.php";
    public static final String ADD_CATEGORY_URL = "add_activity_groups.php";
    public static final String CATEGORY_LIST_URL = "get_activity_groups.php";
    public static final String DEL_ACT_URL = "deletegroup.php";
    public static final String GET_ACT_URL = "get_activity_students.php";
    public static String Activity_URL="get_group_name.php";

    public static String health = "Health";
    public static String performance = "Performance";
    public static String post = "Social Post";

    public static final String ADD_HEALTH = "add_health.php";

    public static final String HEALTH = "get_health.php";
    public static final String ADDPOST = "add_post.php";
    public static final String POST = "get_post.php";

    public static final String ADD_COMMUNICATION = "add_communication.php";
    public static final String ADD_CLEANLINESS = "add_cleanliness.php";
    public static final String COMMUNICATION = "get_communication.php";
    public static final String CLEANLINESS = "get_cleanliness.php";

    public static final String VIEW_TYPE = "&view_type=";
    public static final String FILTER_ID = "&filter_id=";
    public static final String PAGE_LIMIT = "&page_limit=";
    public static final String NAME1 ="&name=";
    public static final String RESULT_URL = "result_permission.php";
    public static final String ADD_RESULT_URL ="add_result_permission.php";
    public static final String QRSTAFF_ATTEND_URLNEW = "qrstaff_attendnew.php";
    public static String PASS_RESET_URL="passwordreset";
    public static String HOLIDAY_URL="holidays.php";
    public static String HOSTEL= "Hostel";
    public static String galfoldername="XYZGallery";
    public static String frontoffice= "FrontOffice";
    public static final String SCHOOL_ROUTE_URL = "school_route.php";
    public static final String ROUTE_TITLE = "route_title";
    public static final String VEHICLE_CAP = "&capacity=";
    public static final String ROUTE_ID = "&route_id=";
    public static final String ROUTES_ID = "?route_id=";
    public static final String ROUTES_NAME = "route_name";
    public static final String CAPACITY = "capacity";
    public static final String PICKUP_POINT_URL = "pickup_point.php";
    public static final String PICKUP_POINT_NAME = "pickup_point_name";
    public static final String PICKUP_POINTS_NAME = "?pickup_point_name=";
    public static final String PICKUP_POINT_ID = "&pickup_point_id=";
    public static final String PICKUP_POINTS_ID = "?pickup_point_id=";
    public static final String VEHICLE_URL = "vehical.php";
    public static final String INSU_DATE = "ins_date";
    public static final String RENE_DATE = "ins_re_date";
    public static final String PURCH_DATE = "date";
    public static final String CAPAICITY = "passanger";
    public static final String REGIS_NO = "reg";
    public static final String TRANS_NAME = "name";
    public static final String TYPE = "type";
    public static final String INSUS_DATE = "?ins_date=";
    public static final String RENES_DATE = "&ins_re_date=";
    public static final String PURCHS_DATE = "&date=";
    public static final String CAPACITYS = "&passanger=";
    public static final String REGIS_NOS = "&reg=";
    public static final String TRANS_NAMES = "&name=";
    public static final String TYPES = "&type=";
    public static final String IMG = "&image=";
    public static final String IDS = "&id=";
    public static final String DRIVERLIST_URL = "driver.php";
    public static final String ADDRESS = "?address=";
    public static final String NAMES = "&name=";
    public static final String MOBILE = "&mobile=";
    public static final String DLNO = "&dlno=";
    public static final String ISSUE_AUTH = "&issue_auth=";
    public static final String VALID = "&valid=";
    public static final String ALOT_VICLE_ROUTE_URL = "alot_route_vehical.php";
    public static final String ALOT_DRIVR_VICLE_URL = "alot_driver_vehical.php";
    public static String LEAVELIST = "staffleavelist.php";
    public static String LEAVELISTID = "?tag=";
    public static String VEHICLEID = "?vehicle_id=";
    public static String TAGGG = "?tag=";

    public static String AMOUNTS = "&amount=";
    public static String syllabus = "Syllabus";
    public static String month_plan="Monthly Planner";
    public static String syllabusgds = "Syllabus.";
    public static String Synopsis = "Synopsis.";
    public static final String NOTICE_URL = "noticeboard.php";
    public static final String ADDNOTICE_URL = "addnotice.php";
    public static final String ADDBANNER_URL = "add_banner.php";
    public static final String SCHOOL_ID = "school_id";
    public static String HQ_URL = "headquater";
    public static String HQ_post = "addhq";

    public static String LAST_NAME = "lastname";
    public static String API_BASE_EXT = "api/";
    public static String API_BASE_EXT_I = "includes/api/";
//    public static String API_BASE_EXT_I = "includes/api_testing/";
    //    public static String Dev_Base_Url = "http://androidtest.smartedgedigitalschool.com/includes/";
//    public static String Prod_Base_Url = "http://ict.smartedgedigitalschool.com/old/includes/";
//    public static String Dev_Base_Url = "http://androidtest.smartedgedigitalschool.com/";
//    public static String Prod_Base_Url = "http://ict.smartedgedigitalschool.com/";
//    public static String Dev_Base_Url = "https://ultimatesolutiongroup.com/";
//    public static String Prod_Base_Url = "https://ultimatesolutiongroup.com/";
    public static String Dev_Base_Url = "https://ultimatesolutiongroup.com/";
    public static String Prod_Base_Url = "https://ultimatesolutiongroup.com/";
    public static int READ_PERMISSION = 1;
    public static String admission_form = "Admission Form";
    public static String colgadmission_form = "  Admission Form";
    public static String admission_enq = "Enquiry List";
    public static String Fee_manage = "Fee Payment";
    public static String CollegeFee_manage = "Fee Payment";
    public static String ChssclFee_manage = "   Fee ";
    public static String GDSsclFee_manage = "School Fee Payment ";
    public static String setup = "Settings";
    public static String admin = "Admin";
    public static String transport = "Transport";
    public static String attendance = "Attendance";
    public static String sattendance = "Staff Attendance";
    public static String gdsattendance = "Stu.Attendance";
    public static String collegeattendance = " Attendance";
    public static String gensisattendance = "Student Attendance";
    public static String exam = "Examination";
    public static String gdsexam = "Examination .";
    public static String timetable = "Timetable";
    public static String timetablesecwise = "Time-table.";
    public static String USER_NAME = "user_name";
    public static String PASSWORD = "password";
    public static String USER_TYPE = "user_type";
    public static String ADMIN = "admin";
    public static String Login_URL = "login.php/";
    public static String PREF_NAME = "ERPApp_Pref";
    public static String CASTELIST_URL = "castelist.php/";
    public static String CASTEDATA = "caste_data";
    public static String CLASSLIST_URL = "classlist.php/";
    public static String CLASSLIST_FEES_URL = "classlist_fees.php/";
    public static String STAFF_ATTEND_COUNT_URL = "staff_attend_count.php/";
    public static String CLASSDATA = "class_data";
    public static String PICKPOINTLIST_URL = "pickuppointlist.php/";
    public static String PICKUP_POINT = "pick_point";
    public static String ROUTE_URL = "transroutelist.php/";
    public static String ROUTEDATA = "route_data";
    public static String SESSION_URL = "sessionlist.php/";
    public static String SESSIONDATA = "session_data";
    public static String REGNO_URL = "fetchregno.php/";
    public static String FEE_LIST_URL = "feecategorylist.php/";
    public static String ADMISSION_URL = "admissionform.php/";
    public static String TITLE = "title";
    public static String FIRST_NAME = "firstname";
    public static String FROM_DATE = "fromdate";
    public static String TO_DATE = "todate";
    public static String GENDER = "gender";
    public static String DOB = "dateofbirth";
    public static String RELIGION = "religion";
    public static String NATIONALITY = "nationality";
    public static String MEDIUM = "medium";
    public static String CATEGORY = "category";
    public static String CLASSID = "classid";
    public static String BLOOD_GRP = "blood_group";
    public static String AADHAR = "aadhar";
    public static String PRE_ADDRESS = "pre_address";
    public static String PRE_CITY = "pre_city";
    public static String PRE_SATTE = "pre_state";
    public static String PRE_PIN_CODE = "pre_pincode";
    public static String PRE_EMAIL = "pre_email";
    public static String SMS_CONTACT = "pre__sms_contact";
    public static String FATHER_NAME = "father_name";
    public static String FATHER_AGE = "father_age";
    public static String FATHER_QUALI = "father_edu_qualification";
    public static String FATHER_OCC = "father_occupation";
    public static String FATHER_CONTACT = "father_contact";
    public static String MOTHER_NAME = "mother_name";
    public static String MOTH_AGE = "mother_age";
    public static String MOTHER_QUALI = "mother_edu_qualification";
    public static String MOTHER_OCC = "mother_occupation";
    public static String MOTHER_CONTACT = "pre_mother_contact";
    public static String PICKUPPOINT = "pickuppoint";
    public static String OTHER = "other";
    public static String FEE_CAT = "fee_category";
    public static String ROUTE = "route";
    public static String NAME = "name";
    public static String ADD_FEECAT_URL = "addfeecat.php/";
    public static String EDIT_FEECAT_URL = "editfeecat.php/";
    public static String ID = "id";
    public static String TRID = "?id=";
    public static String DEL_FEECAT_URL = "delfeecat.php/";
    public static String FEE_DATA = "fee_data";
    public static String GROUP_LIST_URL = "groups.php/";
    public static String GROUP_DATA = "group_data";
    public static String ADD_GROUP_URL = "addgroup.php/";
    public static String EDIT_GROUP_URL = "updategroup.php/";
    public static String DEL_GROUP_URL = "deletegroup.php/";
    public static String ADD_Class_URL = "addclass.php/";
    public static String EDIT_Class_URL = "updateclass.php/";
    public static String DEL_CLASS_URL = "deleteclass.php/";
    public static String GROUPCLASSLIST_URL = "groupclass.php/";
    public static String SUB_LIST_URL = "subjectlist.php/";
    public static String COLG_SUB_LIST_URL = "colgsubjectlist/.php";
    public static String FREE_STAFF_URL = "freeStaff.php/";
    public static String SUB_DATA = "sub_data";
    public static String ADD_SUB_URL = "addsub.php/";
    public static String EDIT_SUB_URL = "editsub.php/";
    public static String DEL_SUB_URL = "delsub.php/";
    public static String PARTICULAR = "particular";
    public static String AMOUNT = "amount";
    public static String CAT_ID = "categoryid";
    public static String ADD_FEE_PART_URL = "addfeeparticular.php/";
    //    public static String FEE_TO_PAY_BY_STUDENT_URL = "feepartstudent/";  //old url
    public static String FEE_TO_PAY_BY_STUDENT_URL = "feeToPayStud.php/";
    public static String STUDENT_URL = "student.php/";
    public static String STUDENT_URLGDS = "studentgds.php/";
    public static String COLLEGESTUDENT_URL = "collegestudent.php";
    public static String PAY_FEE_URL = "payfeeNew.php";
    public static String setting = "Account";
    public static String ATTEND_DATA = "attend_data";
    public static String ADD_ST_ATTEND_URL = "addstudattendance.php";
    public static String DEPARTMENT_LIST_URL = "departmentlist.php";
    public static String STAFF_ATTEND_URL = "staff_attend.php";
    public static String ADD_STAFF_ATTEND_URL = "addstaffattend.php";
    public static String DEVICE_TOKEN = "device_token";
    public static String UPDATE_TOKEN = "updated_device_id";
    public static String test = "Class Test";
    public static String testgds = "Test.";
    public static String SubjectList = "subjectlist.php";
    public static String TestList = "test.php";
    public static String CretateTest = "createtest.php";
    public static String notice = "Notice board";
    public static String ASSIGNMENT_URL = "assignment.php";
    public static String ADDASSIGNMENT_URL = "addassignment.php";
    public static String ASSIGNMENT_DATA = "assign_data";
    public static String assign = "Assignment";
    public static String querry = "Querry";
    public static String resultupload = "resultupload.php";
    public static String StudentList = "studentlistclass.php";
    public static String UPLOAD_EXAM = "uploadexam.php";
    public static String dmr = "DMR";
    //public static String student = "student.php";
    public static String student = "Students";
    public static String homework = "Homework";
    public static String gdshomework = "Home work";
    public static String addexam = "addexam.php";
    public static String addhomework = "addhomework.php";
    public static String classwork = "Classwork";
  //  public static String classwork = "classwork.php";
    public static String gdsclasswork = "Class work";
    public static String addclasswork = "addcw.php";
    public static String studentlistclass = "studentlistclass.php";
    public static String TIMETABLE_URL = "timetable.php";
    public static String ROUTELIST_URL = "routelist.php";
    public static String RHQ_POST = "addroute.php";
    public static String School_list = "schoolname.php";
    public static String ADD_SCHOOL = "addschool.php";
    public static String ROUTE_DATA = "route_data";
    public static String STAFF_DATA = "staff_data";
    public static String HQDATA = "headquater_data";
    public static String DEL_SCLLIST = "delschoollist.php";
    public static String addupdateexam = "addupdateexam.php";
    public static String querry1 = "querry.php";
    public static String ResponseQuery = "replyquerry.php";
    public static String Responsemsg = "queryres.php";
    public static String delexam = "delexam.php";
    public static String Examdetail = "examdetail.php";
    public static String  Examdetailbyclassstu="examdetailbyclassstu.php";
    public static String UPDATE_SCHOOL = "upschooldsr.php";
    public static String APPROVE_SCHOOL = "schoolapprove.php";
    public static String DMR_LIST_URL = "dmrlist.php";
    public static String STAFF_IN_OUT_URL = "staffinout.php";
    public static String STAFF_LIST_URL = "stafflist.php";
    public static String Route_NO_URL = "routeno.php";
    public static String SYLLABUS = "Syllabus.php";
    public static String ADD_SYLLABUS = "addsyllabus.php";
    public static String STAFF_TIMETABLE_URL = "timetablestaff.php";
    public static String TIME_URL = "schoolTime.php";
    public static String GET_TIME_URL = "schoolTiming.php";
    public static String UPDATE_TT_URL = "updateTT.php";
    public static String PERMISSION_LIST = "permissionlist.php";
    public static String addadmin = "addadmin.php";
    public static String adminlist = "adminlist.php";
    public static String SECTION_URL = "section.php?";
    public static String SECTION_DATA = "section_data";
    public static String SECTION_NAME = "section_name";
    public static String SECTION_ID = "section_id";
    public static String CATEGORY_NAME = "category_name";
    public static String CATEGORY_ID = "category_id";
    public static String CATEGORY_URL = "category.php?";
    public static String CATEGORY_DATA = "category_data";
    public static String GATEPASS_URL = "gatepass.php?";
    public static final String ADDSTDGATEPASS ="add_studentgatepass.php";
    public static String GATEPASS_DATA = "gatepass_data";
    public static String gatepass = "Gate Pass";
    public static String collegegatepass = "Gate-Pass.";
    public static String staffgatepass = "Gate Pass";
    public static String SCHOOL_DETAIL_URL = "schooldetail.php";
    public static String FEE_DETAIL_URL = "feedetail.php?";
    public static String FEE_PAID_BY_STUDENT = "feepaidByStudent.php";
    public static String ROUTE_NAME = "?route_name=";
    public static String DRIVER_V_LOC = "driverloc.php";
    public static String staff = "Staff";
    public static String STAFFURL = "staff.php";
    public static String deparmrnt = "department.php";
    public static String dpartment_id = "?d_id=";
    public static String dpartment_name = "&d_name=";
    public static String Postname_name = "&p_name=";
    public static String Postname_id = "&p_id=";
    public static String Post = "post.php";

    public static String STAFF_DETAIL = "staff_detail.php";
    public static String STAFF_ID = "staffid.php";
    public static String messages = "Messages";
    public static String MESSAGE = "message.php";
    public static String MESSAGE_ADMIN = "message_admin.php";
    public static String MSGID = "?id=";
    public static String MSGTYPE = "&type=";
    public static String AT_DATE = "&mdate=";
    public static String STUDENTLIST = "studentlist.php";
    public static String ALLSTUDENTLIST = "allstudentlist.php";
    public static String security="Security";
    public static String VISITOR_LIST="visitor_list.php";
    public static String  STUATTENDANCE_URL="student_attendance.php";
    public static String  Staff_LIST_URL="stafflistdetail.php";
    public static String  STAFFATTENDANCE_URL  ="staffattendance.php";
    public static String  STAFF_DETAIL_POST  ="stafflistbypost.php";
    public static String StudentLEAVELIST="studentleavelist.php";
    public static String leave="Leave";
    public static String sendnotice="Notice";
    public static String NOTICEURL="notice.php";
    public static String dietchart="Diet Chart";
    public static String DAILYTIPSURL="dailytips.php";
    public static String UPDATEDAILYPOST="adddailytips.php";
    public static String DIETCHARTURL="dietchart.php";
    public static String UPDTDIETCHARTURL="updatedietchart.php" ;
    public static String APPROVELEAVEURL="approveleave.php" ;
    public static String DECLINELEAVEURL="declineleave.php" ;
    public static String APPROVESTULEAVEURL="approvestudentleave.php" ;
    public static String APPROVESTULEAVEURL_SUB="approvestudentleave_sub.php" ;
    public static String DECLINESTDNTLEAVEURL="declinestudentleave.php" ;
    public static String DECLINESTDNTLEAVEURL_SUB="declinestudentleave_sub.php" ;
    public static String UPDATEHW="updatehw.php";
    public static String DELETEHW="deleteuploadhw.php";
    public static String notification="Notification";
    public static String NOTIFICATIONURL="notification.php";
    public static String DELETENOTIFICATIONURL = "deletenotification.php";
    public static String TESTRESULTLIST = "testresult.php";
    public static String FEE_TYPE = "fee_type.php";
    public static String STAFF_ATTEND_LIST="staffAttendList.php";
    public static String DEVICE_TOKEN_UPDATE="device_token.php";
    public static String HW_CW_STAFF="hw_cw_pending.php";
    public static String folder_main="XYZUltimate";
    public static String DATE_SHEET_URL="datesheet.php";
    public static String DATE_SHEET_URLGDS="datesheetgds.php";
    public static String ds="Datesheet";
    public static String STAFFTOSTAFFMSGLIST_URL="stafftostaffmsg.php";
    public static String STAFFTOSTUMSGLIST_URL="stafftostumsg.php";

    public static String ADD_QUE_GROUP_URL="add_que_name.php";
    public static String QUE_URL="get_que_name.php";
    public static String EDIT_QUE_URL="update_que_name.php";
    public static final String ADD_QUE_URL = "add_que_groups.php";
    public static final String QUE_LIST_URL = "get_que_groups.php";
    public static String EDIT_QUE_GROUP_URL="update_que_groups.php";
    public static final String DEL_QUE_URL = "delete_que_group.php";

    public static String STDTOSTAFFMSGLIST_URL="getStdToStaffMsg.php";
    public static String ADDEVENT_URL="addevent.php";
    public static String EVENTLIST_URL="eventlist.php";
    public static String UPDATEEVENT_URL="updateevent.php";
    public static String DELETEEVENT_URL="deleteevent.php";
    public static String  ADDEVNTPARTICIPATE_URL="addeventparticipate.php";
    public static String event_id = "?event_id=";
    public static String  EVNTLISTLASTDATEPARTICIPATE_URL="eventparticipatelastdate.php";
    public static String UPDTPRTCPTLSTURL="updateeventparticipatelist.php";
    public static String DELPRTCPLST_URL="delevntprtcptlist.php";
    public static String GATESTAFFPASS_URL="addstaffgatepas.php";
    public static final String STAFFGATEPASS_URL ="staff_gatepass.php?";
    public static String UPDTGATESTAFFPASS_URL="updatestaffgatepas.php";
    public static String  ADDALBUM_URL="addalbum.php";
    public static String ALBUMLIST_URL="albumlist.php";
    public static String ADDPHOTO_URL="addphoto.php";
    public static String GALLERY_URL="gallery.php";
    public static String DELETEPHOTO_URL="delgal_pic.php";
    public static String DELETEALBUM_URL ="deletealbum.php";
    public static String participatedetail_url="getparticipatebyclass.php";
    public static String ADDACHIEVMENT_URL="addachievement.php";
    public static String GETACHIVMNT_URL="getAchievementlist.php";
    public static String EVENTLISTBYTODATE_URL="eventlistbytodate.php";
    public static String EVENTLISTBYSTUDENT_URL="eventlistbystudent.php";
    public static String ebook="E-book";
    public static String elibrary="E-library";
    public static String stureport="Student Report";
    public static String stureportgds="StudentReport";
    public static String GETEVENTSTUNYCLASS="geteventstudent.php";
    public static String STUPERMISSION_LIST = "getstudntpermsn.php";
    public static String ADDNEWMAGZINEURL="addnewsmagzine.php";
    public static String GETNEWSMAGZINE_url="getnewsmagzine.php";
    public static String DELETNEWSMAGZN_URL="delnewsmagzine.php";
    public static String UPDATENEWSMAGZIN ="updatenewsmagzine.php";
    public static String ADDHOSTEL="hostel.php";
    public static String GETHOSTEL="getHostel.php";
    public static String DELETHOSTL_URL="delhostel.php";
    public static String UPDATEHOSTL="updatehostel.php";
    public static String ADDROOM_URL ="addhostelroom.php";
    public static String GETHOSTELROOM="gethostelroom.php";
    public static String DELETHOSTLROOM_URL="delhostelroom.php";
    public static String UPDATEROOM_URL="updatehostelroom.php";
    public static String GETWARDEN_URL="wardenlist.php";
    public static String ADDWARDEN_URL="addwardenhostel.php";
    public static String GETWARDENHOSTEL_URL="getwardenhostel.php";
    public static String DELETHOSTLWARDEN_URL="delwardenhostel.php";
    public static String UPDATEWARDEN_URL="updatewardenhostel.php";
    public static String GETHOSTELROOMBYHOSTELID_URL ="getroomnobyhostlid.php";
    public static String ADDROOMALLOTMENT="addhostlroomallotment.php";
    public static String GETALLOTEDROOMLIST_URL="gethostlroomallotment.php";
    public static String  UPDATEROOMALLOTMENTURL="updatehostlroomallotment.php";
    public static String DELETROOMALLOT_URL="delhostlroomallotment.php";
    public static String  ADDBOYHOSTELTIMIMG="addboyHostlTimimg.php";
    public static String GETHOSTELTIMIMG_URL="gethosteltimimg.php";
    public static String UPDATEHOSTELTIMIMG="updatehosteltiming.php";
    public static String STUBYROOMALLOT="getstubyroomallot.php";
    public static String ADDHOSTELGATEPASS="addhostelgatepass.php";
    public static String GETHOSTELGATEPASSLIST="gethostelgetpasslist.php";
    public static String ADDHOSTELPAYFE_URL="addpayhostelfee.php";
    public static String GETHOSTELFEESTATUS_URL="gethostelfeestatus.php";
    public static String STUNAMEBYPAYFEEss_URL="getstubyhostelfeepay.php";
    public static String GETHOSTLPAYFEELIST_URL="gethostelpayfeelist.php";
    public static String ADD_COLG_ST_ATTEND_URL ="addcolgstudattendance.php";
    public static final String COLG_STUDENT_ATTEND_LIST = "newColgStudAttendList.php";

    public static final String GETCOLGSTUDENT_ATTEND_URL = "getcolgStudentAttendance.php";
    public static final String COLLEGESTUATTENDANCE_URL="college_student_attendance.php";

    public static String ADD_COLG_FEE_PART_URL = "addcollegefeeparticular.php/";
    public static String COLLEGE_FEE_DETAIL_URL = "feedetail.php?";
    public static String  COLGFEEDUEDATE_URL="collegefeeduedate.php";
    public static String ADDFEEXTENSION_URL="extendcollegefeedate.php";
    public static String EXTENSION_LIST_URL="colgfeeextensionlist.php";
    public static String UPDATEFEEXTENSION_URL="updatecolgfeeextnsn.php";
    public static String   COLGACADEMICYAER_URL="colgacademicyear.php";
    public static String   EXTENSION_LIST_byclssstu_URL= "colgfeeextnsnbtclsatu.php";
    public static String FEE_TO_PAY_BY_COLLEGESTUDENT_URL = "feeToPayCollegeStud.php/";
    public static String NO_OFINTALLMENT_FEE_URL = "numberoffeeinstalmnt.php";
    public static String PAYCOLLEGEFEE_URL = "paycollegefee.php";
    public static String FEE_PAID_BY_CollegeSTUDENT = "feepaidBycollegeStudent.php";
    public static String MULTISTAFF_DEPARTARRAY="staffdetailbydepartarray.php";
    public static String MULTISTAFFMESSAGE = "messagetomultistaff.php";
    public static String COLLEGEGATEPASS_URL = "colleggatepass.php";
    public static String GETCOLLEGEGATEPASSLIST_URL = "getcollegegatepasslist.php";
    public static String  UPDATECOLLEGEGATEPASS_URL="updatecollegegatepass.php";
    public static String UPDATEHOSTELGATEPASS_URL="updatehostelgatepass.php";
    public static String COLLEGEADMISSION_URL="collegeAdmissionform.php";
    public static String UPDATECOLLEGEADMISSION_URL="updatecollegeAdmissionform.php";
    public static String STUDENTALLSUBJECTREPORT="studentallsubjectreportattend.php";
    public static String CLASSWISESTUDENTFEESTATS_URL="classwisestudentfeestatus.php";
    public static String  STUDENTLISTBYALLOTHOSTEL  ="studentlistbyallothostel.php";
    public static String ADDNOOFSUBINTIMETABLE_URL ="numofsubjecttimetable.php";
    public static String  GETNOOFSUBJECTTIMETABLE="getnoofsubjecttimetble.php";
    public static String MULTICLASSSTUDENTMESSAGE = "messagetomulticlassstudnt.php";
    public static String CHSSCHOOLPAYFEE_URL ="chssclPayfee.php";
    public static String FEE_TO_PAY_BY_CHSSTUDENT_URL = "feeToPayCHSStud.php/";
    public static String STUDENTLISTROLLNOWISE_URL ="studentlistrollnowise.php";
    public static String CHSFEEBALDETAIL_URL="chsfeebalancedetail.php";
    public static String SCHOOLDETAILBYSCLNAM_URL ="schooldetailbysclname.php";
    public static String UPLOAD_EXAM_MARKS = "uploadexammarks.php";
    public static String UPDATETRAVELEXPENSEURL = "updatetravelexpanse.php";
    public static String APPROVEEXPENSEAVEURL="approveexpense.php" ;
    public static String StudentListbyexamurl = "studentlistclassbyexam.php";
    public static String  marksuploadsubwiseurl="exmmarksuploadsubwise.php";
    public static String GNSISGATESTAFFPASS_URL="addgensisstaffgatepas.php";
    public static String GENSISSECTION_URL = "gensissection.php?";
    public static String GENSIADMISSION_URL = "gensadmissionform.php/";
    public static String DELHOSGATPEPASS_URL="delhosgatepass.php";
    public static String ADDCALANDEREVNT_URL="addcalevent.php";
    public static String GETCALEVENT="getcalevnt.php";
    public static String GETCALEVENTbydate="getcalevntbydate.php";
    public static final String STUDENT_BEHAV_URL = "student_behavior.php";
    public static String ADD_ST_BEHAV_URL = "add_studntbehav.php";
    public static String GET_ST_BEHAV_URL = "getStuBehavior";
    public static String ADD_ST_CLEAN_URL = "add_studntclean.php";
    public static String GET_ST_CLEAN_URL = "getStuclean.php";
    public static String ADD_ST_PUNC_URL = "add_studntpunc.php";
    public static String GET_ST_PUNC_URL = "getStupunc.php";
    public static String ADD_ST_COMMUNI_URL = "add_studntCommuni.php";
    public static String GET_ST_COMMUNI_URL = "getStuCommuni.php";
    public static final String CREATEAPPLICANT_URL ="front_office.php";
    public static final String FETECH_FRRONTOFFICE_LIST ="get_frontoffice_list.php";
    public static String  STAFF_LIST_TEACHER  ="getStaffListteacher.php";
    public static String ADDLESSONPLAN="addlessonplan.php";
    public static String GETLESSONPLANLIST="getlessonplanlist.php";
    public static String UPDATELESSONPLAN="updlessonplan.php";
    public static String DELELESSONPLAN_URL="deletelessonplan.php";
    public static final String STAFFLIST_LOC_URL = "getstaffforloc.php";
    public static String STAFF_V_LOC = "staffloc.php";
    public static String ADD_COMMUNI_listen_URL = "add_comlisten.php";
    public static String ADD_COMMUNI_SPEAK_URL = "add_comspeaking.php";
    public static String STU_LIST_FOR_REPORT = "studentlistforreport.php";
    public static String GET_VIEW_REPORT = "getviewreport.php";
    public static String GET_STU_LISTEN = "getStulisting.php";
    public static String APPROVELESNPLNURL="aprovelessonplan.php";
    public static String DECLINELESNPLNURL="declinelessonplan.php";
    public static String STAFF_ATTEND_LISTExport="staffAttendListExport";
    public static String  ADDPENDINGERKURL="addpendingwork.php";
    public static String staffappraisal = "Staff Appraisal";
    public static String STAFF_appraisal_list = "staffappraisallist.php";
    public static String sports= "Sports";
    public static String ADDSPORT_URL="addsport.php";
    public static String SPORTLIST_URL="sportlist.php";
    public static String UPDATESPORT_URL="updatesport.php";
    public static String DELETESPORT_URL="deletesport.php";
    public static String SPORTLISTLASTDATEPARTICIPATE_URL="sportparticipatelastdate.php";
    public static String ADDSPORTPARTICIPATE_URL="addSportparticipate.php";
    public static String SPORTUPDTPRTCPTLSTURL="updateSportPartcpt.php";
    public static String DELETESPORTPARCPT_URL="deleteSportPartcpt.php";
    public static String SPORTLISTBYSTUDENT_URL="sportlistbystudent.php";
    public static String sportparticipatedetail_url="getsportparticipatebyclass.php";
    public static String ADDSPORTACHIEVMENT_URL="addsportachievement.php";
    public static String GETSPORTACHIVMNT_URL="getSportAchievementlist.php";
    public static String childcare= "Child Care";
    public static String ADDCHILDCAREURL= "addchildcare.php";
    public static String UPDATECHILDCAREURL= "updatechildcare.php";
    public static String DELETCHILDCAREURL="delchildcareinfo.php";
    public static String summercamp= "Summer Camp";
    public static String addsummercamp= "addsummercamp.php";
    public static String getSummerCamp="getSummerCamp.php";
    public static String updateSummerCamp="updateSummerCamp.php";
    public static String delSummerCamp="delSummerCamp.php";
    public static String regSummerCamp="regSummerCamp.php";
    public static String updateregSummerCamp="updateregSummerCamp.php";
    public static String delregSummerCamp="delregSummerCamp.php";
    public static String trainingmgt= "Training Mgt.";
    public static String addtraining= "addtraining.php";
    public static String gettraining="gettraining.php";
    public static String updatetraining="updatetraining.php";
    public static String deltraining="deltraining.php";
    public static String regTraining="regTraining.php";
    public static final String ADDDRIVERTOVEHICLE_URL = "adddrivertovehicle.php";
    public static final String ADDROUTETOVEHICLE_URL = "addroutetovehicle.php";
    public static String GDSADD_FEE_PART_URL = "addGDSfeeparticular.php/";
    public static String GDSFEE_TO_PAY_BY_STUDENT_URL = "feeToPayStudGDS.php/";
    public static String GDSFEE_TO_TRANS_BY_STUDENT_URL = "feeTransportToPayStudGDS.php/";
    public static String PAY_FEE_URL_GDS = "payfeeNewGDS.php";
    public static String FEE_PAID_BY_STUDENTGDS = "feepaidByStudentGDS.php";
    public static String  GDSPENDINGFEEDETAIL_URL="pendingfeedetailgds.php";
    public static String  CHSStudentfeeStatus_URL="chsstudentfeestatus.php";
    public static String STUDENTLISTREGNOWISE_URL ="studentlistregnowise.php";
    public static String SECTIONLIST_URL = "sectionlist.php/";
    public static String STUDENTLISTCLSSECWISE_URL ="studentlistclssecwise.php";
    public static final String GDSSTUDENT_ATTEND_URL = "gdsstudent_attend.php";
    public static String ADD_ST_ATTEND_URLGDS = "addstudattendancegds.php";
    public static final String STUDENT_ATTEND_LISTGDS = "StudAttendListgds.php";
    public static String StudentListbyexamurlgds = "studentlistclassbyexamgds.php";
    public static String GDSGALLERYLIST_URL="gdsgallerylist.php";
    public static String gdshomeworkurl = "gdshomework.php";
    public static String addhomeworkGDS = "addhomeworkgds.php";
    public static String classworkGDS = "classworkGDS.php";
    public static String addclassworkGDS = "addcwGDS.php";
    public static String updateclassworkGDS = "updatecwGDS.php";
    public static String DELETECWGDSURL_URL="deletecwgds.php";
    public static String ADDFEEDBACKMAIL="addfeedbackmail.php";
    public static String FETCHFEEDBOOK="fetchfeedback.php";
    public static String CretateTestgds = "createtestGDS.php";
    public static String TestListgds = "testgds.php";
    public static String resultuploadgds = "resultuploadgds.php";
    public static String TESTRESULTLISTGDS = "testresultGDS.php";
    public static String ADD_SYLLABUSGDS = "addsyllabusGDS.php";
    public static String SYLLABUSGDS = "syllabusGDS.php";
    public static String UPDATE_SYLLABUSGDS = "updatesyllabusGDS.php";
    public static String gdshomeworkbydatreurl = "gdshomeworkbydate.php";
    public static String CLASSLISTSECWISE_URL = "classlistsecwise.php/";
    public static String UPDATE_SYNOPSIS = "updatesynopsis.php";
    public static String ADD_SYNOPSIS = "addsynopsis.php";
    public static String SYNOPSIS = "getsynopsis.php";
    public static String TIMETABLESEC_URL = "timetablesection.php";
    public static String UPDATE_TTSECTIONWISE_URL = "updateTTsectionwise.php";
    public static final String STUDENT_BEHAVSEC_URL = "student_behaviorsec.php";
    public static String ADD_ST_BEHAVSEC_URL = "add_studntbehavsec.php";
    public static String ADD_ST_CLEANSEC_URL = "add_studntcleansec.php";
    public static String ADD_ST_PUNCSEC_URL = "add_studntpuncsec.php";
    public static String ADD_ST_COMMUNISEC_URL = "add_studntCommunisec.php";
    public static String ADD_COMMUNI_SPEAKSEC_URL = "add_comspeakingsec.php";
    public static String ADD_COMMUNI_listenSEC_URL = "add_comlistensec.php";
    public static String STU_LIST_FOR_REPORTSEC = "studentlistforreportsec.php";
    public static String ADDAUTHORISEPERSONGP= "addauthriseprsngp.php";
    public static String GETAUTHORISEPRSNGP="getauthoriseprsngp.php";
    public static String  DELEAUTHPRSNGATEPASS_URL="delauthprsngp.php";
    public static String schlchkinout= "Check In-Out";
    public static String studentcheckInOut= "studentcheckInOut.php";
    public static String ocr= "OCR";
    public static String newstureport="Student-Report";
    public static String STU_REP_CAT_URL = "stureportcategorylist.php/";
    public static String ADD_STU_REPCAT_URL = "addStuReportcat.php/";
    public static String EDIT_STU_REPCAT_URL = "editStuReportcat.php/";
    public static String DEL_STU_REPCAT_URL = "delStuReportcat.php/";
    public static String STU_REP_GRADE_URL = "stureportgradelist.php/";
    public static String ADD_STU_REPgrade_URL = "addStuReportgrade.php/";
    public static String EDIT_STU_REPGRADE_URL = "editStuReportgrade.php/";
    public static String DEL_STU_REPGRADE_URL = "delStuReportgrade.php/";
    public static String GIDSRSTAFF_LIST_URL = "stafflistGIDSR.php";
    public static final String GETGENSISSTUDENT_ATTEND_URL = "getGensisStudentAttendance.php";
    public static String ADD_GIDSR_ST_ATTEND_URL ="addgidsrstudattendance.php";
    public static final String GIDSR_STUDENT_ATTEND_LIST = "gidsrStudAttendList.php";
    public static String birthdaylist = "birthdaylist.php";
    public static String BDAYSNAME ="?schoolname=";
    public static String staffbirthdaylist = "staffbirthdaylist.php ";
    public static String STUDENTLISTVANWISE_URL ="studentlistvanwise.php";
    public static String STUDENTLISTVILLWISE_URL ="studentlistvilagwise.php";
    public static String STAFFCHKINOUTWISE_URL ="ofcstaffchkinoutlist.php";
    public static String DRIVERNOTICEURL="noticedriver.php";
    public static String block = "block.php";
    public static String  STAFF_DETAIL_BLOCK  ="stafflistbyBlock.php";
    public static String addhomeworkSUBWISE = "addhomeworksubwise.php";
    public static String gethomeworksubwise = "getHWsubwise.php";
    public static String ADD_EBOOK = "addebook.php";
    public static String get_EBOOK = "getebook.php";
    public static String Update_EBOOK = "updateebook.php";

    public static String onlineclass= "E-Learning";
    public static String ADD_ONLINELINK = "addonlinelink.php";
    public static String VIEW_ONLINELINK = "viewonlinelink.php";
    public static String DELETONLINELINK="delonlinelink.php";
    public static String AddNewTimetableURL = "addnewtimetable.php";
    public static String GETNewTimetableURL =  "getNewTimeTable.php";
    public static String DELETETIMETABLEurl="deletetimetable.php";
    public static String DEL_EBOOKURL= "deleteebook.php";
    public static String DEL_SYLLABUSURL= "deletesyllabus.php";
    public static String gdsclassworkbydatreurl = "gdsclassworkbydate.php";
    public static String REPLYASSIGNMENT_URL = "replyassignmentlist.php";
    public static String DELETEGALARY_URL="deletegalery.php";
    public static String DELETEnoticeboard="deletenoticenrd.php";
    public static String DELETETest="deleteclasstest.php";
    public static String DELETEdatesheet="deletedatesheet.php";
    public static String OnlineQuiz="OnlineQuiz";
    public static String getonlinequiztest= "getonlinequiz.php" ;
    public static String getonlinequizresult= "getonlinequizresult.php" ;
    public static String ADDTHOUGHT = "addthought.php";
    public static String ADDNEWTHOUGHT = "addnewthought.php";
    public static String VIEWTHOUGHT = "viewthought.php";
    public static String ADDVIDEOlearn_URL = "addvideolearn.php";
    public static String ADDVIDEO_URL="addvideogal.php";
    public static String viewgalleryvideourl = "viewgalleryvideo.php";
    public static String QRSCANNNER="QR Scanner";
    public static final String GDSSTUDENT_ATTEND_URLNEW = "gdsstudent_attendnew.php";
    public static final String GDSSTUDENT_ATTEND_URLTRANS = "gdsstudent_attendtrans.php";
    public static String ADD_ST_ATTEND_URLTRANSPORT = "addstudattendancegdstrans.php";
    public static final String GDSSTUDENT_ATTENDBYVEHI_URL = "gdsstudent_attendByvehi.php";
    public static final String EVENGDSSTUDENT_ATTENDBYVEHI_URL = "gdsstudent_attendByvehieven.php";
    public static String updatecheckouttransurl="updatecheckouttran.php";
    public static final String CHECKOUT_ATTENDBYVEHI_URL = "checkout_attendByvehi.php";
    public static String ADD_ST_ATTEND_URLTRANSPORTEVE = "addstudattendancegdstranseve.php";
    public static String updatecheckouttransurleven="updatecheckouttraneven.php";
    public static final String EVENCHECKOUT_ATTENDBYVEHI_URL = "evencheckout_attendByvehi.php";
    public static final String GDSSTUDENT_ATTEND_URLTRANSEVE = "gdsstudent_attendtranseve.php";
    public static final String MORNING_DEFAULTER_LIST_URL = "morningdefaulterlist.php";
    public static final String EVENNING_DEFAULTER_LIST_URL = "eveningdefaulterlist.php";
    public static String BLOCKSTUDENTURL="blockstudent.php";
    public static String BLOCKLISTSTUDENTURL="blockliststudent.php";
    public static String UNBLOCKSTUDENTURL="unblockstudent.php";
    public static final String STUDENTBYSCANNING_URLNEW = "student_detailbyscan.php";

    public static String s_key= "?s_key=";
    public static String db_user = "&db_user=";
    public static String db_pass = "&db_pass=";
    public static String db_host = "&db_host=";
    public static String db_name = "&db_name=";
    public static String from_finance = "&from_finance=";
    public static String to_finance = "&to_finance=";
    public static String feepaidns = "feepaid.php";


    public static String OnlineClass="Online Class";
    public static String live_classurl="live_class.php";
    public static String getliveclassteacherdeturl = "getliveclassteacherdetail.php";
    public static final String STUDENTBYid = "student_detailbyid.php";
    public static String gateattendance="Gate Attendance";
    public static String gatestafflistclass="gate_staffattendence.php";
    public static String gatestudentlistclass="gate_attendence.php";
    public static String inchage="Assign Incharge";
    public static String subject="Assign Subject";
    public static String SECURITYcheckin_LIST="securitycheckinlist.php";
    public static String SECURITYcheckOUT_LIST="securitycheckOUTlist.php";
    public static String schoolsetup="School SetUp";
    public static String thought="Today's Thought";
    public static String banner="Banners";

    public static String NoticeBoard="Notice Board";
    public static String Message="Message";
    public static String Gallery="Gallery";
    public static final String NEW_MSG ="msg_noti.php";
    public static final String NEW_MSG_NOTI ="noti_toggle.php";

    public static String Transport="Transport";
    public static String Visitor="Visitor Detail";
    public static String feedback="Feedback";
    public static String Notification="Notification";
    public static String fuel_expense="Fuel Expense";
    public static final String GETEXPENSE_URL ="getExpenseList.php";
    public static String ImageToPdf="Image To Pdf";
    public static String OCR="OCR";
    public static String userlist="User List";
    public static String school="School Detail";
    public static String signature="Add Signature";
    public static String birthday="Birthday";
    public static String holiday="Holiday";


    public static String getBaseURL() {
        if(User.getCurrentUser()!= null && User.getCurrentUser().getSchoolData().getUrl() !=null){
            return User.getCurrentUser().getSchoolData().getUrl()+ API_BASE_EXT_I;
            //return  User.getCurrentUser().getSchoolData().getBaseUrl()+ API_BASE_EXT_I;
        }else {
            if (BuildConfig.BUILD_TYPE.equalsIgnoreCase("release")) {
                return Prod_Base_Url + API_BASE_EXT_I;
            } else {
                return Dev_Base_Url + API_BASE_EXT_I;
            }
        }
    }



//    public static String getBaseURL() {
//        if(User.getCurrentUser()!= null){
//            if(User.getCurrentUser().getSchoolData().getUrl().contains("includes")){
//                return  User.getCurrentUser().getSchoolData().getUrl()+ API_BASE_EXT;
//            }else {
//                return  User.getCurrentUser().getSchoolData().getUrl()+ API_BASE_EXT_I;
//            }
//        }else {
//            if (BuildConfig.BUILD_TYPE.equalsIgnoreCase("release")) {
//                return Prod_Base_Url + API_BASE_EXT;
//            } else {
//                return Dev_Base_Url + API_BASE_EXT;
//            }
//        }
//    }

    //gitsetup_new
    //ATBBqKFRp2scc49UweKGw5kSLncEC056EE14

    public static String getImageBaseURL() {
        if (User.getCurrentUser() != null && User.getCurrentUser().getSchoolData().getUrl() != null) {
            if(User.getCurrentUser().getSchoolData().getUrl().contains("includes")){
                String[] separated = User.getCurrentUser().getSchoolData().getUrl().split("includes");
                return separated[0];
            }else{
                return User.getCurrentUser().getSchoolData().getUrl();
            }
        } else {
            if (BuildConfig.BUILD_TYPE.equalsIgnoreCase("release")) {
                return Prod_Base_Url;
            } else {
                return Dev_Base_Url;
            }

        }

    }

    public static String[] mColors = {
            "FFEBEE", "FFCDD2", "EF9A9A", "E57373", "EF5350", "F44336", "E53935",        //reds
            "D32F2F", "C62828", "B71C1C", "FF8A80", "FF5252", "FF1744", "D50000",
            "FCE4EC", "F8BBD0", "F48FB1", "F06292", "EC407A", "E91E63", "D81B60",        //pinks
            "C2185B", "AD1457", "880E4F", "FF80AB", "FF4081", "F50057", "C51162",
            "F3E5F5", "E1BEE7", "CE93D8", "BA68C8", "AB47BC", "9C27B0", "8E24AA",        //purples
            "7B1FA2", "6A1B9A", "4A148C", "EA80FC", "E040FB", "D500F9", "AA00FF",
            "EDE7F6", "D1C4E9", "B39DDB", "9575CD", "7E57C2", "673AB7", "5E35B1",        //deep purples
            "512DA8", "4527A0", "311B92", "B388FF", "7C4DFF", "651FFF", "6200EA",
            "E8EAF6", "C5CAE9", "9FA8DA", "7986CB", "5C6BC0", "3F51B5", "3949AB",        //indigo
            "303F9F", "283593", "1A237E", "8C9EFF", "536DFE", "3D5AFE", "304FFE",
            "E3F2FD", "BBDEFB", "90CAF9", "64B5F6", "42A5F5", "2196F3", "1E88E5",        //blue
            "1976D2", "1565C0", "0D47A1", "82B1FF", "448AFF", "2979FF", "2962FF",
            "E1F5FE", "B3E5FC", "81D4fA", "4fC3F7", "29B6FC", "03A9F4", "039BE5",        //light blue
            "0288D1", "0277BD", "01579B", "80D8FF", "40C4FF", "00B0FF", "0091EA",
            "E0F7FA", "B2EBF2", "80DEEA", "4DD0E1", "26C6DA", "00BCD4", "00ACC1",        //cyan
            "0097A7", "00838F", "006064", "84FFFF", "18FFFF", "00E5FF", "00B8D4",
            "E0F2F1", "B2DFDB", "80CBC4", "4DB6AC", "26A69A", "009688", "00897B",        //teal
            "00796B", "00695C", "004D40", "A7FFEB", "64FFDA", "1DE9B6", "00BFA5",
            "E8F5E9", "C8E6C9", "A5D6A7", "81C784", "66BB6A", "4CAF50", "43A047",        //green
            "388E3C", "2E7D32", "1B5E20", "B9F6CA", "69F0AE", "00E676", "00C853",
            "F1F8E9", "DCEDC8", "C5E1A5", "AED581", "9CCC65", "8BC34A", "7CB342",        //light green
            "689F38", "558B2F", "33691E", "CCFF90", "B2FF59", "76FF03", "64DD17",
            "F9FBE7", "F0F4C3", "E6EE9C", "DCE775", "D4E157", "CDDC39", "C0CA33",        //lime
            "A4B42B", "9E9D24", "827717", "F4FF81", "EEFF41", "C6FF00", "AEEA00",
            "FFFDE7", "FFF9C4", "FFF590", "FFF176", "FFEE58", "FFEB3B", "FDD835",        //yellow
            "FBC02D", "F9A825", "F57F17", "FFFF82", "FFFF00", "FFEA00", "FFD600",
            "FFF8E1", "FFECB3", "FFE082", "FFD54F", "FFCA28", "FFC107", "FFB300",        //amber
            "FFA000", "FF8F00", "FF6F00", "FFE57F", "FFD740", "FFC400", "FFAB00",
            "FFF3E0", "FFE0B2", "FFCC80", "FFB74D", "FFA726", "FF9800", "FB8C00",        //orange
            "F57C00", "EF6C00", "E65100", "FFD180", "FFAB40", "FF9100", "FF6D00",
            "FBE9A7", "FFCCBC", "FFAB91", "FF8A65", "FF7043", "FF5722", "F4511E",        //deep orange
            "E64A19", "D84315", "BF360C", "FF9E80", "FF6E40", "FF3D00", "DD2600",
            "EFEBE9", "D7CCC8", "BCAAA4", "A1887F", "8D6E63", "795548", "6D4C41",        //brown
            "5D4037", "4E342E", "3E2723",
            "FAFAFA", "F5F5F5", "EEEEEE", "E0E0E0", "BDBDBD", "9E9E9E", "757575",        //grey
            "616161", "424242", "212121",
            "ECEFF1", "CFD8DC", "B0BBC5", "90A4AE", "78909C", "607D8B", "546E7A",        //blue grey
            "455A64", "37474F", "263238"
    };
}
