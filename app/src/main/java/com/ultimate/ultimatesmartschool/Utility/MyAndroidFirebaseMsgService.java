package com.ultimate.ultimatesmartschool.Utility;

import android.annotation.SuppressLint;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.media.AudioAttributes;
import android.net.Uri;
import android.os.Build;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import com.android.volley.Request;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.ultimate.ultimatesmartschool.BeanModule.User;
import com.ultimate.ultimatesmartschool.MainActivity;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.UltimateSchoolApp;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class MyAndroidFirebaseMsgService extends FirebaseMessagingService {

    @Override
    public void onNewToken(String mToken) {
        super.onNewToken(mToken);
        saveInSharedPref(mToken);
        if (User.getCurrentUser() != null && User.getCurrentUser().getId() != null)
            saveOnServer(mToken);
        //Log.e("TOKEN",mToken);
    }
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

       // Log.e("here", "notify");
        if (remoteMessage.getData() != null) {
            //Log.e("noti", "Message data payload: " + remoteMessage.getData());
            Map<String, String> data = remoteMessage.getData();
            JSONObject object = new JSONObject(data);
            try {
                // Log.e("checck: ", User.getCurrentUser().getId().equals(object.getString("user_id")) + "");
                User currentUser = User.getCurrentUser();
                if (currentUser != null && currentUser.getId() != null) {
                    //        Log.e("TAG", "Notification Body 3: " + currentUser.getId().equals(object.getString("user_id")));
                    if (currentUser.getId().equals(object.getString("user_id"))) {
                        createNotification(object.getString("body"), object.getString("title"));
                    }
                } else {
                    //        Log.e("TAG", "CurrentUser is null in onMessageReceived");
                }
//                if (User.getCurrentUser() != null && User.getCurrentUser().getId() != null
//                        && User.getCurrentUser().getId().equals(object.getString("user_id"))) {
//                    createNotification(object.getString("body"), object.getString("title"));
//                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        } else if (remoteMessage.getNotification() != null) {
          //  Log.e("noti", "Message Notification Body: " + remoteMessage.getNotification().getBody());
            if (User.getCurrentUser() != null && User.getCurrentUser().getId() != null) {
                createNotification(remoteMessage.getNotification().getBody(), remoteMessage.getNotification().getTitle());
            }
        }

    }


    private void createNotification(String msg, String title) {
        Intent intent = null;
        PendingIntent resultIntent = null;
        int m = 0;
        m = new Random().nextInt();

        intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        resultIntent = PendingIntent.getActivity(this, m, intent,
                PendingIntent.FLAG_ONE_SHOT | PendingIntent.FLAG_IMMUTABLE);

        Uri alarmSound = Uri.parse("android.resource://"
                + getApplication().getPackageName() + "/" + R.raw.good);
        String channelId = "UltimateSchool";
        CharSequence channelName = "UltimateSchool";
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this, channelId)
                .setSmallIcon(R.mipmap.ultillogo)
                .setContentTitle(title)
                .setContentText(msg)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                // Set the intent that will fire when the user taps the notification
                .setContentIntent(resultIntent)
                .setSound(alarmSound)
                .setPriority(NotificationManager.IMPORTANCE_HIGH)
                .setAutoCancel(true);
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_HIGH;
            @SuppressLint("WrongConstant") NotificationChannel channel = new NotificationChannel(channelId, channelName, importance);
            channel.enableLights(true);
            channel.setLightColor(Color.RED);
            channel.enableVibration(true);
            channel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
            AudioAttributes audioAttributes = new AudioAttributes.Builder()
                    .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                    .setUsage(AudioAttributes.USAGE_NOTIFICATION_RINGTONE)
                    .build();
            channel.setSound(alarmSound, audioAttributes);
            channel.setDescription(msg);
            notificationManager.createNotificationChannel(channel);

        }
        notificationManager.notify(m, mBuilder.build());
    }
    private static void saveOnServer(String deviceToken) {
        if (User.getCurrentUser() != null && User.getCurrentUser().getId() != null) {
            HashMap<String, String> params = new HashMap<String, String>();
            params.put("user_id", User.getCurrentUser().getId());
            params.put("device_token", deviceToken);
            ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.DEVICE_TOKEN_UPDATE, new ApiHandler.ApiCallback() {
                @Override
                public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {

                }
            }, UltimateSchoolApp.getInstance(), params);
        }else{

        }
    }

    public static void saveInSharedPref(String deviceToken) {
        SharedPreferences preferences = UltimateSchoolApp.getInstance().getSharedPreferences(Constants.PREF_NAME, Context.MODE_PRIVATE);
        preferences.edit().putString(Constants.DEVICE_TOKEN, deviceToken).commit();
    }
}
