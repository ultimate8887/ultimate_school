package com.ultimate.ultimatesmartschool.Utility;

import android.content.Context;
import android.content.SharedPreferences;

import com.android.volley.Request;

import com.ultimate.ultimatesmartschool.BeanModule.User;
import com.ultimate.ultimatesmartschool.UltimateSchoolApp;

import org.json.JSONObject;

import java.util.HashMap;

public class MyAndroidFirebaseInstanceIdService  {

    String deviceToken;


    private static void saveOnServer(String deviceToken) {
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("user_id", User.getCurrentUser().getId());
        params.put("device_token", deviceToken);
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.DEVICE_TOKEN_UPDATE, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {

            }
        }, UltimateSchoolApp.getInstance(), params);

    }

    public static void saveInSharedPref(String deviceToken) {
        SharedPreferences preferences = UltimateSchoolApp.getInstance().getSharedPreferences(Constants.PREF_NAME, Context.MODE_PRIVATE);
        preferences.edit().putString(Constants.DEVICE_TOKEN, deviceToken).commit();
    }
}
