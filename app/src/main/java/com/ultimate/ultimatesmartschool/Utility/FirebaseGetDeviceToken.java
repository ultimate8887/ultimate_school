package com.ultimate.ultimatesmartschool.Utility;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import androidx.annotation.NonNull;

import com.android.volley.Request;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import com.google.firebase.messaging.FirebaseMessaging;
import com.ultimate.ultimatesmartschool.BeanModule.User;
import com.ultimate.ultimatesmartschool.UltimateSchoolApp;

import org.json.JSONObject;

import java.util.HashMap;

public class FirebaseGetDeviceToken {

    private static final String TAG = FirebaseGetDeviceToken.class.getName();
    static String deviceToken;

    public static String getDeviceToken() {
//                deviceToken = FirebaseInstanceId.getInstance().getToken();
//        saveInSharedPref(deviceToken);
//        if (User.getCurrentUser() != null)
//            saveOnServer(deviceToken);
        FirebaseMessaging.getInstance().getToken()
                .addOnCompleteListener(new OnCompleteListener<String>() {
                    @Override
                    public void onComplete(@NonNull Task<String> task) {
                        if (!task.isSuccessful()) {
                            Log.w("TAG", "Fetching FCM registration token failed", task.getException());
                            return;
                        }

                        // Get new FCM registration token
                        String token = task.getResult();
                        deviceToken=token;
                        saveInSharedPref(token);
                        if (User.getCurrentUser() != null){
                            saveOnServer(deviceToken);
                        }
                        // Log and send the token to your server or update the UI if needed
                        Log.e("mytoken", token);
                    }
                });

        return (deviceToken);
    }

    private static void saveOnServer(String deviceToken) {
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("user_id", User.getCurrentUser().getId());
        params.put("device_token", deviceToken);
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.DEVICE_TOKEN_UPDATE, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {

            }
        }, UltimateSchoolApp.getInstance(), params);

    }

    public static void saveInSharedPref(String deviceToken) {
        SharedPreferences preferences = UltimateSchoolApp.getInstance().getSharedPreferences(Constants.PREF_NAME, Context.MODE_PRIVATE);
        preferences.edit().putString(Constants.DEVICE_TOKEN, deviceToken).commit();
    }
}
