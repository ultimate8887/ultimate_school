package com.ultimate.ultimatesmartschool.Fee;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;

import com.github.siyamed.shapeimageview.CircularImageView;
import com.squareup.picasso.Picasso;
import com.ultimate.ultimatesmartschool.Activity.SchoolInfo;
import com.ultimate.ultimatesmartschool.Banner.BannerActivity;
import com.ultimate.ultimatesmartschool.BeanModule.User;
import com.ultimate.ultimatesmartschool.ForCamera.IPickResult;
import com.ultimate.ultimatesmartschool.ForCamera.PickImageDialog;
import com.ultimate.ultimatesmartschool.ForCamera.PickResult;
import com.ultimate.ultimatesmartschool.ForCamera.PickSetup;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.CommonProgress;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;
import com.ultimate.ultimatesmartschool.Utility.ErpProgressVert;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AddFeesQRActivity extends AppCompatActivity implements IPickResult {


    private int selection;
    private Bitmap userBitmap=null, userBitmapProf=null;
    private int VERIFY_NUMBER = 1000;
    Animation animation;
    @BindView(R.id.circleimgrecepone)
    ImageView image;
    @BindView(R.id.imgBackmsg)
    ImageView back;
    @BindView(R.id.edit_profile)
    ImageView edit_profile;
    @BindView(R.id.submit)
    TextView submit;

    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.default_card)
    RelativeLayout default_card;



    String image_url="",key="";
    CommonProgress commonProgress;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_fees_q_r);
        ButterKnife.bind(this);
        commonProgress = new CommonProgress(this);

        if (getIntent().getExtras() != null) {
            key = getIntent().getExtras().getString("key");
        }

        animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.btn_blink_animation);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                back.startAnimation(animation);
                finish();
            }
        });
        edit_profile.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {
                edit_profile.startAnimation(animation);
                selectImg();
            }
        });
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submit.startAnimation(animation);
               if (key.equalsIgnoreCase("qr")){
                   assignData();
               }else{
                   assignData2();
               }


            }
        });

        if (key.equalsIgnoreCase("qr")){
            txtTitle.setText("Add Online Fees QR");
            userprofile();
        }else{
            txtTitle.setText("Add Birthday Card");
            userprofile();
           // userprofile();
        }



       // selectImg();

    }



    private void userprofile() {
        // UltimateProgress.showProgressBar(getActivity(),"Please Wait........!");
        commonProgress.show();
        HashMap<String, String> params = new HashMap<>();
        params.put("user_id", User.getCurrentUser().getId());
        params.put("check", "fetch");


        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.UPDATE_SCHOOL_INFO, new ApiHandler.ApiCallback() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error ) {
                commonProgress.dismiss();
                ErpProgress.cancelProgressBar();
                if (error == null) {
                    try {

                        if (key.equalsIgnoreCase("qr")){
                            default_card.setVisibility(View.GONE);
                            image_url = jsonObject.getJSONObject(Constants.USERDATA).getString("qr_img");
                            if (!image_url.equalsIgnoreCase("https://ultimatesolutiongroup.com/office_admin/images/school_logo/")) {
                                Utils.progressImg_two(image_url,image,AddFeesQRActivity.this,"doc");
                                // Picasso.with(AddFeesQRActivity.this).load(image_url).placeholder(R.color.transparent).into(image);
                            } else {
                                default_card.setVisibility(View.GONE);
                                Picasso.get().load(R.color.transparent).into(image);
                                Toast.makeText(getApplicationContext(), "QR Image File not found, kindly select QR Image File to upload", Toast.LENGTH_SHORT).show();
                                selectImg();
                            }
                        }else{
                            image_url = jsonObject.getJSONObject(Constants.USERDATA).getString("birthday_card");
                            if (!image_url.equalsIgnoreCase("https://ultimatesolutiongroup.com/office_admin/images/school_logo/")) {
                                Utils.progressImg_two(image_url,image,AddFeesQRActivity.this,"doc");
                                // Picasso.with(AddFeesQRActivity.this).load(image_url).placeholder(R.color.transparent).into(image);
                            } else {
                                Picasso.get().load(R.color.transparent).into(image);
                                Toast.makeText(getApplicationContext(), "Birthday Card not found, kindly select Birthday Card Image File to upload", Toast.LENGTH_SHORT).show();
                                default_card.setVisibility(View.VISIBLE);
                               // selectImg();
                            }
                            // userprofile();
                        }




                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }, this, params);
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    @SuppressLint("ResourceType")
    private void selectImg() {

        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.S) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_MEDIA_IMAGES)
                    != PackageManager.PERMISSION_GRANTED) {

                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_MEDIA_IMAGES)) {
                    Toast.makeText(this, "Please provide storage permission from app settings", Toast.LENGTH_LONG).show();
                } else {

                    ActivityCompat.requestPermissions(this,
                            new String[]{Manifest.permission.READ_MEDIA_IMAGES},
                            1);
                }
            } else {

                onImageViewClick();
            }

        } else {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {

                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    Toast.makeText(this, "Please provide storage permission from app settings", Toast.LENGTH_LONG).show();
                } else {

                    ActivityCompat.requestPermissions(this,
                            new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                            1);
                }
            } else {
                onImageViewClick();

            }
        }
    }

    private void onImageViewClick() {
        //  type = 1;
        PickSetup setup = new PickSetup();
        // super.customize(setup);

        PickImageDialog.build(setup)
                //.setOnClick(this)
                .show(this);
    }

    @Override
    public void onPickResult(PickResult r) {
        if (r.getError() == null) {
            //If you want the Bitmap.
            image.setImageBitmap(r.getBitmap());
            userBitmap = r.getBitmap();
            default_card.setVisibility(View.GONE);
            submit.setVisibility(View.VISIBLE);

        } else {
            //Handle possible errors
            //TODO: do what you have to do with r.getError();
            Toast.makeText(this, r.getError().getMessage(), Toast.LENGTH_LONG).show();
            default_card.setVisibility(View.GONE);
        }
    }

//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        if (resultCode == Activity.RESULT_OK) {
//            // Uri object will not be null for RESULT_OK
//            Uri imageUri = data.getData();
//            try {
//                userBitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), imageUri);
//                image.setImageBitmap(userBitmap);
//                default_card.setVisibility(View.GONE);
//                submit.setVisibility(View.VISIBLE);
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//
//        }else {
//            default_card.setVisibility(View.GONE);
//            Toast.makeText(this, "Selection Cancelled", Toast.LENGTH_SHORT).show();
//        }
//    }


    private void assignData2() {
        if (userBitmap == null) {
            Toast.makeText(AddFeesQRActivity.this, "Kindly add Birthday Card", Toast.LENGTH_SHORT).show();
            //errorMsg = "Please enter Title";
        }  else{
            commonProgress.show();
            HashMap<String, String> params = new HashMap<>();
            params.put("check", "birthday_card");
            if (userBitmap != null) {
                String encoded = Utils.encodeToBase64(userBitmap, Bitmap.CompressFormat.JPEG, 50);
                encoded = String.format("data:image/jpeg;base64,%s", encoded);
                params.put("logo", encoded);
            }

            ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.UPDATE_SCHOOL_INFO, new ApiHandler.ApiCallback() {
                @Override
                public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                    ErpProgressVert.cancelProgressBar();
                    commonProgress.dismiss();
                    if (error == null) {
                             finish();
                            Toast.makeText(AddFeesQRActivity.this, "Update Successfully", Toast.LENGTH_SHORT).show();


                    } else {
                        Toast.makeText(AddFeesQRActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            }, this, params);

        }
    }

    private void assignData() {

        if (userBitmap == null) {
            Toast.makeText(AddFeesQRActivity.this, "Kindly add OR Image", Toast.LENGTH_SHORT).show();
            //errorMsg = "Please enter Title";
        }  else{
            commonProgress.show();
            HashMap<String, String> params = new HashMap<>();
            params.put("check", "qr");
            if (userBitmap != null) {
                String encoded = Utils.encodeToBase64(userBitmap, Bitmap.CompressFormat.JPEG, 50);
                encoded = String.format("data:image/jpeg;base64,%s", encoded);
                params.put("image", encoded);
            }


            ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.ADDBANNER_URL, new ApiHandler.ApiCallback() {
                @Override
                public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                    ErpProgressVert.cancelProgressBar();
                    commonProgress.dismiss();
                    if (error == null) {
                        try {
                            Toast.makeText(AddFeesQRActivity.this, jsonObject.getString("msg"), Toast.LENGTH_SHORT).show();
                            finish();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else {
                        Toast.makeText(AddFeesQRActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            }, this, params);

        }
    }

}