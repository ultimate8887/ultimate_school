package com.ultimate.ultimatesmartschool.Fee;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.ultimate.ultimatesmartschool.AddmissionForm.FeeCatBean;
import com.ultimate.ultimatesmartschool.AddmissionForm.SessionAdapter;
import com.ultimate.ultimatesmartschool.AddmissionForm.SessionBean;
import com.ultimate.ultimatesmartschool.BeanModule.ClassBean;
import com.ultimate.ultimatesmartschool.BeanModule.CommonBean;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AddFee extends AppCompatActivity {
    @BindView(R.id.parent)
    LinearLayout parent;
    @BindView(R.id.spinnerGroup)
    Spinner spinnerGroup;
    @BindView(R.id.spinnerClass)
    Spinner spinnerClass;
    @BindView(R.id.spinnerCategory)
    Spinner spinnerCategory;
    @BindView(R.id.spinnerFinancialYear)
    Spinner spinnerFinancialYear;
    @BindView(R.id.edtAmount)
    EditText edtAmount;
    @BindView(R.id.edtParticular)
    EditText edtParticular;
    @BindView(R.id.btnSubmit)
    Button btnSubmit;
    private ArrayList<FeeCatBean> feeList;
    private String feeid = "";
    private ArrayList<ClassBean> classList;
    private String classid = "0";
    private ArrayList<CommonBean> groupList;
    private ClassAdapterfee classadapter;
    private String groupid = "0";
    private ArrayList<SessionBean> sessionlist;
    private SessionBean session;
    private boolean noClass = false;
    RadioGroup feepaytype;
    String radiobuttonvalue ="yearly";
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.edtLateFine)EditText edtLateFine;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_fee);
        ButterKnife.bind(this);
        feepaytype = (RadioGroup) findViewById(R.id.radiofeepdtype);
        feeList = new ArrayList<>();
        groupList = new ArrayList<>();
        txtTitle.setText("Fee Particular");

        sessionlist = new ArrayList<>();
        classList = new ArrayList<>();
        classadapter = new ClassAdapterfee(AddFee.this, classList);
        spinnerClass.setAdapter(classadapter);
        spinnerClass.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i == 0) {
                    noClass = false;
                    classid = "0";
                }
                if (i > 0) {
                    classid = classList.get(i - 1).getId();
                    noClass = true;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        fetchCategory();
        fetchGroup();
        fetchSession();


        feepaytype.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.radioonetime:
                        radiobuttonvalue = "yearly";
                        break;


                }
            }
        });
    }


    @OnClick(R.id.imgBack)
    public void callBack() {
        finish();
    }
    private void fetchCategory() {
        HashMap<String, String> params = new HashMap<String, String>();
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.FEE_LIST_URL, catapiCallback, this, params);
    }
    ApiHandler.ApiCallback catapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            if (error == null) {
                try {
                    feeList = FeeCatBean.parseFeeArray(jsonObject.getJSONArray(Constants.FEE_DATA));
                    FeeModAdapter adapter = new FeeModAdapter(AddFee.this, feeList);
                    spinnerCategory.setAdapter(adapter);
                    spinnerCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            feeid = feeList.get(i).getFeecategory_id();
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Utils.showSnackBar(error.getMessage(), parent);
            }
        }
    };

    private void fetchGroup() {
        ErpProgress.showProgressBar(AddFee.this, "Please wait...");
        HashMap<String, String> params = new HashMap<String, String>();
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.GROUP_LIST_URL, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                ErpProgress.cancelProgressBar();
                if (error == null) {
                    try {
                        groupList.clear();
                        groupList = CommonBean.parseCommonArray(jsonObject.getJSONArray(Constants.GROUP_DATA));
                        GroupAdapternew adapter = new GroupAdapternew(AddFee.this, groupList);
                        spinnerGroup.setAdapter(adapter);
                        spinnerGroup.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                if (i == 0) {
                                    if (classadapter != null) {
                                        groupid = "0";
                                        noClass = false;
                                        fetcAllClass();
                                    }
                                }
                                if (i > 0) {
                                    classList.clear();
                                    classid = "";
                                    groupid = groupList.get(i - 1).getId();
                                    fetchClass(groupid);
                                }
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> adapterView) {

                            }
                        });
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    Utils.showSnackBar(error.getMessage(), parent);
                }
            }
        }, AddFee.this, params);
    }

    private void fetcAllClass() {
        HashMap<String, String> params = new HashMap<String, String>();
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.CLASSLIST_URL, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                try {
                    classList.clear();
                    classadapter.notifyDataSetChanged();
                    classList = ClassBean.parseClassArray(jsonObject.getJSONArray(Constants.CLASSDATA));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, AddFee.this, params);

    }

    private void fetchClass(String id) {
        HashMap<String, String> params = new HashMap<String, String>();
        params.put(Constants.ID, id);
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.GROUPCLASSLIST_URL, classapiCallback, AddFee.this, params);
    }

    ApiHandler.ApiCallback classapiCallback = new ApiHandler.ApiCallback() {

        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            if (error == null) {
                try {
                    classList.clear();
                    classList = ClassBean.parseClassArray(jsonObject.getJSONArray(Constants.CLASSDATA));
                    classadapter.setClassData(classList);
                    spinnerClass.setSelection(0);
                    classadapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Utils.showSnackBar(error.getMessage(), parent);
            }
        }
    };

    private void fetchSession() {
        HashMap<String, String> params = new HashMap<String, String>();
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.SESSION_URL, sessionapiCallback, this, params);
    }


    ApiHandler.ApiCallback sessionapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            if (error == null) {
                try {
                    sessionlist = SessionBean.parseSessionArray(jsonObject.getJSONArray(Constants.SESSIONDATA));
                    SessionAdapter adapter = new SessionAdapter(AddFee.this, sessionlist);
                    spinnerFinancialYear.setAdapter(adapter);
                    spinnerFinancialYear.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            session = sessionlist.get(i);
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Utils.showSnackBar(error.getMessage(), parent);
            }
        }
    };

    @OnClick(R.id.btnSubmit)
    public void saveFeeDetails() {
        if (checkValid()) {
            ArrayList<String> class_id = new ArrayList<>();
            if (!noClass) {
                for (int i = 0; i < classList.size(); i++) {
                    class_id.add(classList.get(i).getId());
                }
            } else {
                class_id.add(classid);
            }

            Log.e("classidkyahai", String.valueOf(class_id));

            HashMap<String, String> params = new HashMap<String, String>();
            params.put(Constants.PARTICULAR, edtParticular.getText().toString());
            params.put(Constants.AMOUNT, edtAmount.getText().toString());
            params.put(Constants.CAT_ID, feeid);
            params.put(Constants.CLASSID, String.valueOf(class_id));
            params.put(Constants.FROM_DATE, session.getStart_date());
            params.put(Constants.TO_DATE, session.getEnd_date());
            params.put(Constants.FEE_TYPE, radiobuttonvalue);
            params.put("latefine",edtLateFine.getText().toString());
            ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.GDSADD_FEE_PART_URL, apiCallback, this, params);

        }
    }

    ApiHandler.ApiCallback apiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            if (error == null) {
                try {
                    Toast.makeText(AddFee.this, jsonObject.getString("msg"), Toast.LENGTH_SHORT).show();
                    finish();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("error", error.getMessage() + "");
                Utils.showSnackBar(error.getMessage(), parent);
            }
        }
    };

    private boolean checkValid() {
        boolean valid = true;
        String errorMsg = null;
        if (edtParticular.getText().toString().trim().length() <= 0) {
            valid = false;
            errorMsg = "Please enter Particular";
        } else if (edtAmount.getText().toString().trim().length() <= 0) {
            valid = false;
            errorMsg = "Please enter Amount";
        } else if (edtLateFine.getText().toString().trim().length() <= 0) {
            valid = false;
            errorMsg = "Please enter Latefine";
        }
        if (!valid) {
            Utils.showSnackBar(errorMsg, parent);
        }
        return valid;
    }
}
