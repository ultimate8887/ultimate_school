package com.ultimate.ultimatesmartschool.Fee;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.Request;
import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetSequence;
import com.ultimate.ultimatesmartschool.AddmissionForm.SessionAdapter;
import com.ultimate.ultimatesmartschool.AddmissionForm.SessionBean;
import com.ultimate.ultimatesmartschool.Examination.CheckReportCard;
import com.ultimate.ultimatesmartschool.Examination.ReportCard;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Staff.CategoryAdapter;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.CommonProgress;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class FeeDetail extends AppCompatActivity {
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.spinnerFinancialYear)
    Spinner spinnerFinancialYear;
    @BindView(R.id.parent)
    RelativeLayout parent;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    private CategoryAdapter adapter;
    private LinearLayoutManager layoutManager;
    ArrayList<FeeDetailBean> list;
    private FeeDetaildapter mAdapter;
    Animation animation;
    @BindView(R.id.dialog)
    ImageView dialog;
    SharedPreferences sharedPreferences;
    @BindView(R.id.totalRecord)
    TextView totalRecord;
    @BindView(R.id.textNorecord)
    TextView txtNorecord;
    CommonProgress commonProgress;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fee_detail);
        list = new ArrayList<>();
        ButterKnife.bind(this);
        commonProgress=new CommonProgress(this);
        animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.btn_blink_animation);
        txtTitle.setText("Fee Detail");
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        mAdapter = new FeeDetaildapter(list, this);
        recyclerView.setAdapter(mAdapter);

//        if (!restorePrefData()){
//            setShowcaseView();
//        }

        fetchSession();
    }

    private void setShowcaseView() {
        new TapTargetSequence(this)
                .targets(TapTarget.forView(dialog,"Add Online Fees QR Image!","Tap the QR button to add online fees QR image.")
                        .outerCircleColor(R.color.light)
                        .outerCircleAlpha(0.96f)
                        .targetCircleColor(R.color.white)
                        .titleTextSize(20)
                        .titleTextColor(R.color.white)
                        .descriptionTextSize(14)
                        .descriptionTextColor(R.color.black)
                        .textColor(R.color.black)
                        .textTypeface(Typeface.SANS_SERIF)
                        .dimColor(R.color.black)
                        .drawShadow(true)
                        .cancelable(false)
                        .tintTarget(true)
                        .transparentTarget(true)
                        .targetRadius(60)).listener(new TapTargetSequence.Listener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onSequenceFinish() {
                // Toast.makeText(getApplicationContext(),"Sequence Finished",Toast.LENGTH_SHORT).show();
                savePrefData();
                commonIntent();
            }

            @Override
            public void onSequenceStep(TapTarget lastTarget, boolean targetClicked) {
                // Toast.makeText(getActivity(),"GREAT!",Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSequenceCanceled(TapTarget lastTarget) {

            }
        }).start();
    }

    private void commonIntent() {
        dialog.startAnimation(animation);

        Intent intent = new Intent(FeeDetail.this, AddFeesQRActivity.class);
        intent.putExtra("key", "qr");
        startActivity(intent);

       // startActivity(new Intent(FeeDetail.this,AddFeesQRActivity.class));
    }

    private void savePrefData(){
        sharedPreferences=this.getSharedPreferences("boarding_pref_add_qrrrrrrr",MODE_PRIVATE);
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putBoolean("IsFirstTimeVisit_add_qrrrrrrr",true);
        editor.apply();
    }

    private boolean restorePrefData(){
        sharedPreferences = this.getSharedPreferences("boarding_pref_add_qrrrrrrr",MODE_PRIVATE);
        return sharedPreferences.getBoolean("IsFirstTimeVisit_add_qrrrrrrr",false);
    }

    private void fetchSession() {
        commonProgress.show();
        HashMap<String, String> params = new HashMap<String, String>();
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.SESSION_URL, sessionapiCallback, this, params);
    }

    ApiHandler.ApiCallback sessionapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            commonProgress.dismiss();
            if (error == null) {
                try {
                    final ArrayList<SessionBean> sessionlist = SessionBean.parseSessionArray(jsonObject.getJSONArray(Constants.SESSIONDATA));
                    SessionAdapter adapter = new SessionAdapter(FeeDetail.this, sessionlist);
                    spinnerFinancialYear.setAdapter(adapter);
                    spinnerFinancialYear.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            SessionBean session = sessionlist.get(i);
                            fetchFeeDetail(session);
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Utils.showSnackBar(error.getMessage(), parent);
            }
        }
    };

    private void fetchFeeDetail(SessionBean session) {
        commonProgress.show();
        HashMap<String, String> params = new HashMap<String, String>();
        String url = Constants.getBaseURL() + Constants.FEE_DETAIL_URL + "sd=" + session.getStart_date() + "&ed=" + session.getEnd_date();
        ApiHandler.apiHit(Request.Method.GET, url, apiCallback, this, params);
    }

    ApiHandler.ApiCallback apiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            commonProgress.dismiss();
            if (error == null) {
                try {
                    if (list != null)
                        list.clear();
                    list = FeeDetailBean.pareFeeDetail(jsonObject.getJSONArray("fee_data"));
                    mAdapter.setFeeList(list);
                    mAdapter.notifyDataSetChanged();
                    txtNorecord.setVisibility(View.GONE);
                    totalRecord.setText("Total Entries:- "+String.valueOf(list.size()));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                totalRecord.setText("Total Entries:- 0");
                txtNorecord.setVisibility(View.VISIBLE);
                if (list != null)
                    list.clear();
                mAdapter.setFeeList(list);
                mAdapter.notifyDataSetChanged();
                Utils.showSnackBar(error.getMessage(), parent);
            }
        }
    };

    @OnClick(R.id.dialog)
    public void dialog() {
        commonIntent();
    }

    @OnClick(R.id.imgBack)
    public void backClick() {
        finish();
    }
}
