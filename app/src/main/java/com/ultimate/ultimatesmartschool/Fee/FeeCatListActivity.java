package com.ultimate.ultimatesmartschool.Fee;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.ultimate.ultimatesmartschool.AddmissionForm.FeeCatBean;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class FeeCatListActivity extends AppCompatActivity implements  FeeCatAdapter.AddNewGroup{
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.lytInst)
    LinearLayout lytInst;
    ArrayList<FeeCatBean> dataList = new ArrayList<>();
    private FeeCatAdapter adapter;
    @BindView(R.id.parent)
    LinearLayout parent;
    Dialog dialog;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fee_cat_list);
        ButterKnife.bind(this);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new FeeCatAdapter(dataList, this);
        recyclerView.setAdapter(adapter);
        updateInst();
        fetchFeeList();
        txtTitle.setText("Fee Category");
    }

    private void fetchFeeList() {
        ErpProgress.showProgressBar(this,"Please wait...");
        HashMap<String, String> params = new HashMap<String, String>();
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.FEE_LIST_URL, feeapiCallback, this, params);

    }

    ApiHandler.ApiCallback feeapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            if (error == null) {
                try {
                    ArrayList<FeeCatBean> feeList = FeeCatBean.parseFeeArray(jsonObject.getJSONArray(Constants.FEE_DATA));
                    dataList.clear();
                    dataList.addAll(feeList);
                    adapter.notifyDataSetChanged();
                    updateInst();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Utils.showSnackBar(error.getMessage(), parent);
            }
        }
    };



    private void updateInst() {
        if (dataList.size() > 0) {
            lytInst.setVisibility(View.VISIBLE);
        } else {
            lytInst.setVisibility(View.GONE);
        }
    }

    @SuppressLint("NewApi")
    @Override
    public void addEditNewGroup(final FeeCatBean data, final int pos) {


        dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.add_fee_dialog);
        final EditText edtTitle = (EditText) dialog.findViewById(R.id.edtTitle);
        Button btnSubmit = (Button) dialog.findViewById(R.id.btnSubmit);;
        if (pos != -1) {
            edtTitle.setText(dataList.get(pos).getFeecategory());

            btnSubmit.setText("Update");
        }
//        final Button

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Utils.hideKeyboard(FeeCatListActivity.this);
                if (edtTitle.getText().toString().length() > 0) {
                    if (pos == -1) {
                        addNewcat(edtTitle.getText().toString());

                    } else {
                        editNewcat(data, edtTitle.getText().toString());

                    }


                } else {
                    Utils.showSnackBar("Please enter Category name", parent);
                }

            }
        });
        dialog.show();

    }



    private void editNewcat(FeeCatBean data, String name) {
        HashMap<String, String> params = new HashMap<String, String>();
        params.put(Constants.NAME, name);
        params.put(Constants.ID, data.getFeecategory_id());
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.EDIT_FEECAT_URL, apiCallback, this, params);
    }

    private void addNewcat(String cat) {
        HashMap<String, String> params = new HashMap<String, String>();
        params.put(Constants.NAME, cat);
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.ADD_FEECAT_URL, apiCallback, this, params);
    }

    ApiHandler.ApiCallback apiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            if (error == null) {
                try {
                    Toast.makeText(FeeCatListActivity.this,jsonObject.getString("msg"),Toast.LENGTH_SHORT).show();
                    ArrayList<FeeCatBean> feelist = FeeCatBean.parseFeeArray(jsonObject.getJSONArray(Constants.FEE_DATA));
                    dataList.clear();
                    dataList.addAll(feelist);
                    adapter.notifyDataSetChanged();
                    dialog.dismiss();
                    updateInst();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("error", error.getMessage() + "");
                Utils.showSnackBar(error.getMessage(), parent);
            }
        }
    };

    @Override
    public void deleteGroup(final FeeCatBean data, final int position) {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:
                        HashMap<String, String> params = new HashMap<String, String>();
                        params.put(Constants.ID, data.getFeecategory_id());
                        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.DEL_FEECAT_URL, new ApiHandler.ApiCallback() {
                            @Override
                            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                                if (error == null) {
                                    Toast.makeText(FeeCatListActivity.this, "Deleted Successfully", Toast.LENGTH_SHORT).show();
                                    dataList.remove(position);
                                    adapter.notifyDataSetChanged();
                                    updateInst();
                                } else {
                                    Utils.showSnackBar(error.getMessage(), parent);
                                }
                            }
                        }, FeeCatListActivity.this, params);
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        //No button clicked
                        break;
                }
            }
        };
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Are you sure you want to delete?").setPositiveButton("Yes", dialogClickListener)
                .setNegativeButton("No", dialogClickListener).show();
    }

    @OnClick(R.id.imgBack)
    public void callBack() {
        finish();
    }
}
