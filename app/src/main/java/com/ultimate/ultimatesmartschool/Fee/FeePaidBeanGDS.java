package com.ultimate.ultimatesmartschool.Fee;

import com.ultimate.ultimatesmartschool.Utility.DebugLog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class FeePaidBeanGDS {


    private String id;
    private String orgnl_fee;
    private String monthname;
    private String  paymentdate;
    private String  prev_paid_bal;
    private String prev_total_bal;

    private String  remark;

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getReceiptno() {
        return receiptno;
    }

    public void setReceiptno(String receiptno) {
        this.receiptno = receiptno;
    }

    public String getConcessionamountgds() {
        return concessionamountgds;
    }

    public void setConcessionamountgds(String concessionamountgds) {
        this.concessionamountgds = concessionamountgds;
    }

    private String  receiptno;
    private String concessionamountgds;

    public String getPrev_total_bal() {
        return prev_total_bal;
    }

    public void setPrev_total_bal(String prev_total_bal) {
        this.prev_total_bal = prev_total_bal;
    }

    public String getPrev_paid_bal() {
        return prev_paid_bal;
    }

    public void setPrev_paid_bal(String prev_paid_bal) {
        this.prev_paid_bal = prev_paid_bal;
    }

    public String getMonthname() {
        return monthname;
    }

    public void setMonthname(String monthname) {
        this.monthname = monthname;
    }

    public String getPaymentdate() {
        return paymentdate;
    }

    public void setPaymentdate(String paymentdate) {
        this.paymentdate = paymentdate;
    }

    public String getOrgnl_fee() {
        return orgnl_fee;
    }

    public void setOrgnl_fee(String orgnl_fee) {
        this.orgnl_fee = orgnl_fee;
    }

    private String orgnl_transfee;
    private String particularname;
    private String paid_particular_amnt;
    private String paid_transportfee;
    private String orgnl_fine;
    private String paid_fine;
    private String fee_bal;
    private String totalamount;
    private String paidamount;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOrgnl_transfee() {
        return orgnl_transfee;
    }

    public void setOrgnl_transfee(String orgnl_transfee) {
        this.orgnl_transfee = orgnl_transfee;
    }

    public String getParticularname() {
        return particularname;
    }

    public void setParticularname(String particularname) {
        this.particularname = particularname;
    }

    public String getPaid_particular_amnt() {
        return paid_particular_amnt;
    }

    public void setPaid_particular_amnt(String paid_particular_amnt) {
        this.paid_particular_amnt = paid_particular_amnt;
    }

    public String getPaid_transportfee() {
        return paid_transportfee;
    }

    public void setPaid_transportfee(String paid_transportfee) {
        this.paid_transportfee = paid_transportfee;
    }

    public String getOrgnl_fine() {
        return orgnl_fine;
    }

    public void setOrgnl_fine(String orgnl_fine) {
        this.orgnl_fine = orgnl_fine;
    }

    public String getPaid_fine() {
        return paid_fine;
    }

    public void setPaid_fine(String paid_fine) {
        this.paid_fine = paid_fine;
    }

    public String getFee_bal() {
        return fee_bal;
    }

    public void setFee_bal(String fee_bal) {
        this.fee_bal = fee_bal;
    }

    public String getTotalamount() {
        return totalamount;
    }

    public void setTotalamount(String totalamount) {
        this.totalamount = totalamount;
    }

    public String getPaidamount() {
        return paidamount;
    }

    public void setPaidamount(String paidamount) {
        this.paidamount = paidamount;
    }

    public static ArrayList<FeePaidBeanGDS> parseHOSTLRoomArray(JSONArray arrayObj) {
        ArrayList<FeePaidBeanGDS> list = new ArrayList<FeePaidBeanGDS>();
        try {

            for (int i = 0; i < arrayObj.length(); i++) {
                FeePaidBeanGDS p = parseHOSTLSRoomObject(arrayObj.getJSONObject(i));
                if (p != null) {
                    list.add(p);
                }
            }
        } catch (Exception e) {
            DebugLog.printLog("exp",e.getMessage());
            e.printStackTrace();
        }
        return list;
    }

    public static FeePaidBeanGDS parseHOSTLSRoomObject(JSONObject jsonObject) {
        FeePaidBeanGDS casteObj = new FeePaidBeanGDS();
        try {

            if (jsonObject.has("orgnl_transfee") && !jsonObject.getString("orgnl_transfee").isEmpty() && !jsonObject.getString("orgnl_transfee").equalsIgnoreCase("null")) {
                casteObj.setOrgnl_transfee(jsonObject.getString("orgnl_transfee"));
            }
            if (jsonObject.has("particularname") && !jsonObject.getString("particularname").isEmpty() && !jsonObject.getString("particularname").equalsIgnoreCase("null")) {
                casteObj.setParticularname(jsonObject.getString("particularname"));
            }
            if (jsonObject.has("paid_particular_amnt") && !jsonObject.getString("paid_particular_amnt").isEmpty() && !jsonObject.getString("paid_particular_amnt").equalsIgnoreCase("null")) {
                casteObj.setPaid_particular_amnt(jsonObject.getString("paid_particular_amnt"));
            }
            if (jsonObject.has("orgnl_fee") && !jsonObject.getString("orgnl_fee").isEmpty() && !jsonObject.getString("orgnl_fee").equalsIgnoreCase("null")) {
                casteObj.setOrgnl_fee(jsonObject.getString("orgnl_fee"));
            }
            if (jsonObject.has("paid_transportfee") && !jsonObject.getString("paid_transportfee").isEmpty() && !jsonObject.getString("paid_transportfee").equalsIgnoreCase("null")) {
                casteObj.setPaid_transportfee(jsonObject.getString("paid_transportfee"));
            }

            if (jsonObject.has("orgnl_fine") && !jsonObject.getString("orgnl_fine").isEmpty() && !jsonObject.getString("orgnl_fine").equalsIgnoreCase("null")) {
                casteObj.setOrgnl_fine(jsonObject.getString("orgnl_fine"));
            }
            if (jsonObject.has("paid_fine") && !jsonObject.getString("paid_fine").isEmpty() && !jsonObject.getString("paid_fine").equalsIgnoreCase("null")) {
                casteObj.setPaid_fine(jsonObject.getString("paid_fine"));
            }
            if (jsonObject.has("fee_bal") && !jsonObject.getString("fee_bal").isEmpty() && !jsonObject.getString("fee_bal").equalsIgnoreCase("null")) {
                casteObj.setFee_bal(jsonObject.getString("fee_bal"));
            }

            if (jsonObject.has("totalamount") && !jsonObject.getString("totalamount").isEmpty() && !jsonObject.getString("totalamount").equalsIgnoreCase("null")) {
                casteObj.setTotalamount(jsonObject.getString("totalamount"));
            }
            if (jsonObject.has("paidamount") && !jsonObject.getString("paidamount").isEmpty() && !jsonObject.getString("paidamount").equalsIgnoreCase("null")) {
                casteObj.setPaidamount(jsonObject.getString("paidamount"));
            }

            if (jsonObject.has("monthname") && !jsonObject.getString("monthname").isEmpty() && !jsonObject.getString("monthname").equalsIgnoreCase("null")) {
                casteObj.setMonthname(jsonObject.getString("monthname"));
            }
            if (jsonObject.has("paymentdate") && !jsonObject.getString("paymentdate").isEmpty() && !jsonObject.getString("paymentdate").equalsIgnoreCase("null")) {
                casteObj.setPaymentdate(jsonObject.getString("paymentdate"));
            }
            if (jsonObject.has("prev_paid_bal") && !jsonObject.getString("prev_paid_bal").isEmpty() && !jsonObject.getString("prev_paid_bal").equalsIgnoreCase("null")) {
                casteObj.setPrev_paid_bal(jsonObject.getString("prev_paid_bal"));
            }
            if (jsonObject.has("prev_total_bal") && !jsonObject.getString("prev_total_bal").isEmpty() && !jsonObject.getString("prev_total_bal").equalsIgnoreCase("null")) {
                casteObj.setPrev_total_bal(jsonObject.getString("prev_total_bal"));
            }

            if (jsonObject.has("remark") && !jsonObject.getString("remark").isEmpty() && !jsonObject.getString("remark").equalsIgnoreCase("null")) {
                casteObj.setRemark(jsonObject.getString("remark"));
            }
            if (jsonObject.has("receiptno") && !jsonObject.getString("receiptno").isEmpty() && !jsonObject.getString("receiptno").equalsIgnoreCase("null")) {
                casteObj.setReceiptno(jsonObject.getString("receiptno"));
            }
            if (jsonObject.has("concessionamountgds") && !jsonObject.getString("concessionamountgds").isEmpty() && !jsonObject.getString("concessionamountgds").equalsIgnoreCase("null")) {
                casteObj.setConcessionamountgds(jsonObject.getString("concessionamountgds"));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return casteObj;
    }
}
