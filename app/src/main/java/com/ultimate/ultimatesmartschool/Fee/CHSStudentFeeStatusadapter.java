package com.ultimate.ultimatesmartschool.Fee;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.ultimate.ultimatesmartschool.R;

import java.util.ArrayList;

public class CHSStudentFeeStatusadapter extends RecyclerView.Adapter<CHSStudentFeeStatusadapter.Viewholder> {
    ArrayList<CHSStudentFeeStatusBean> hq_list;

    Context listner;

    public CHSStudentFeeStatusadapter(ArrayList<CHSStudentFeeStatusBean> hq_list, Context listener) {

        this.hq_list = hq_list;
        this.listner = listener;
    }


    @Override
    public CHSStudentFeeStatusadapter.Viewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.classwisestudfee_lay, parent, false);
        CHSStudentFeeStatusadapter.Viewholder viewholder = new CHSStudentFeeStatusadapter.Viewholder(view);
        return viewholder;

    }

    @Override
    public void onBindViewHolder(final CHSStudentFeeStatusadapter.Viewholder holder, final int position) {
        holder.textViewid.setText("Class : "+hq_list.get(position).getClass_name());
        holder.name.setText("Name: "+hq_list.get(position).getName());
        holder.rollno.setText("Roll No: "+hq_list.get(position).getStudent_id());
        holder.totalfee.setText(hq_list.get(position).getEs_prev_bal());
        holder.amountpaid.setText(hq_list.get(position).getEs_totalamount());
        holder.con_amt.setText(hq_list.get(position).getEs_concessionamount());
        holder.fine.setText(hq_list.get(position).getEs_fine());
        holder.fine_bal.setText(hq_list.get(position).getEs_particularamount());
        holder.fine_paid.setText(hq_list.get(position).getEs_fee_bal());
        holder.fee_bal.setText(hq_list.get(position).getEs_annual_bal());
        holder.totalfee_bal.setVisibility(View.GONE);


    }



    @Override
    public int getItemCount() {
        return hq_list.size();
    }

    public void setHQList(ArrayList<CHSStudentFeeStatusBean> hq_list) {
        this.hq_list = hq_list;
    }



    public class Viewholder extends RecyclerView.ViewHolder {
        public TextView textViewid,rollno,totalfee,amountpaid,con_amt,fine,fine_bal,fine_paid,fee_bal,totalfee_bal,transportfee;
        RecyclerView recList;
        public TextView name;

        public Viewholder(View itemView) {
            super(itemView);
            textViewid = (TextView) itemView.findViewById(R.id.textViewid);
            name = (TextView) itemView.findViewById(R.id.name);
            rollno = (TextView) itemView.findViewById(R.id.rollno);
            totalfee = (TextView) itemView.findViewById(R.id.totalfee);
            amountpaid = (TextView) itemView.findViewById(R.id.amount);
            con_amt = (TextView) itemView.findViewById(R.id.con_amt);
            fine= (TextView) itemView.findViewById(R.id.fine);
            fine_bal= (TextView) itemView.findViewById(R.id.fine_bal);
            fine_paid= (TextView) itemView.findViewById(R.id.fine_paid);
            fee_bal= (TextView) itemView.findViewById(R.id.fee_bal);
            totalfee_bal=(TextView)itemView.findViewById(R.id.totalfee_bal);
            transportfee=(TextView)itemView.findViewById(R.id.transportfee);
        }
    }

}
