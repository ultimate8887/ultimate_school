package com.ultimate.ultimatesmartschool.Fee;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;
import com.ultimate.ultimatesmartschool.BeanModule.User;
import com.ultimate.ultimatesmartschool.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FeeDetaildapter extends RecyclerView.Adapter<FeeDetaildapter.Viewholder> {
    private final Context mContext;
    ArrayList<FeeDetailBean> list;

    public FeeDetaildapter(ArrayList<FeeDetailBean> list, Context mContext) {
        this.list = list;
        this.mContext = mContext;

    }

    @Override
    public FeeDetaildapter.Viewholder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.fee_detail_lyt, viewGroup, false);
        FeeDetaildapter.Viewholder viewholder = new FeeDetaildapter.Viewholder(view);
        return viewholder;
    }

    @Override
    public void onBindViewHolder(final FeeDetaildapter.Viewholder viewholder, int i) {
        viewholder.txtClass.setText(list.get(i).getClass_data().getName());

        FeeDetailListAdapter mAdapter = new FeeDetailListAdapter(list.get(i).getFee(), mContext);
        viewholder.recyclerview.setAdapter(mAdapter);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void setFeeList(ArrayList<FeeDetailBean> list) {
        this.list = list;
    }

    public static class Viewholder extends RecyclerView.ViewHolder {
        @BindView(R.id.txtClass)
        TextView txtClass;
        @BindView(R.id.recyclerview)
        RecyclerView recyclerview;

        public Viewholder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }

}
