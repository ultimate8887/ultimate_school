package com.ultimate.ultimatesmartschool.Fee.New_Fees;

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;
import com.ultimate.ultimatesmartschool.Fee.FeeListAdapter;
import com.ultimate.ultimatesmartschool.Fee.Fee_Paid_Bean;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PaidFeeListAdapter extends RecyclerView.Adapter<PaidFeeListAdapter.ViewHolder> {

    Context context;
    ArrayList<Fee_Paid_Bean> list;
    private PaidFeeListAdapter.ProdMethodCallBack prodMethodCallBack;
    private Animation animation;
    private int view_value;

    public PaidFeeListAdapter(ArrayList<Fee_Paid_Bean> list, Context context, PaidFeeListAdapter.ProdMethodCallBack prodMethodCallBack, int view_value) {
        this.list=list;
        this.context=context;
        this.prodMethodCallBack=prodMethodCallBack;
        this.view_value=view_value;
    }

    public void setMerchantBeans(ArrayList<Fee_Paid_Bean> list) {
        this.list = list;
    }

    public interface ProdMethodCallBack{
        void clickMethod(Fee_Paid_Bean fee_paid_bean);
        void clickRECMethod(Fee_Paid_Bean fee_paid_bean);
    }


    @NonNull
    @Override
    public PaidFeeListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.transaction_layout_paid_new, parent, false);
        PaidFeeListAdapter.ViewHolder viewHolder= new PaidFeeListAdapter.ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull PaidFeeListAdapter.ViewHolder holder, @SuppressLint("RecyclerView") int position) {

        Fee_Paid_Bean data= list.get(position);

        if (data.getS_id() != null) {
            String title = getColoredSpanned(data.getName(), "#000000");
            String Name = getColoredSpanned("("+data.getS_id()+"-"+data.getClass_name()+")", "#5A5C59");
            holder.name.setText(Html.fromHtml(title + " " + Name));
        }else {
            holder.name.setText("Not Mentioned");
        }



        if (data.getScreenshot()!=null) {
            //holder.product_img.setVisibility(View.GONE);
            Picasso.get().load(data.getScreenshot()).placeholder(context.getResources().getDrawable(R.color.transparent)).into(holder.product_img_receipt);
            holder.bg.setVisibility(View.VISIBLE);
        }else {
            holder.bg.setVisibility(View.GONE);
            // holder.product_img.setVisibility(View.VISIBLE);
        }

        if (view_value==2){
            holder.v_status.setVisibility(View.VISIBLE);
            holder.track_order.setVisibility(View.GONE);

            String title5 = getColoredSpanned("Fee Status:- ", "#5A5C59");
            String Name5 = "";

            if (data.getPayment_status().equalsIgnoreCase("pending")){
                Name5 = getColoredSpanned("<b>" +"Pending"+"</b>", "#F9A602");
            }else if (data.getPayment_status().equalsIgnoreCase("add")){
                Name5 = getColoredSpanned("<b>" +"Complete"+"</b>", "#1C8B3B");
            }else {
                Name5 = getColoredSpanned("<b>" +"Decline"+"</b>", "#F4212C");
            }

            holder.testDate.setText(Html.fromHtml(title5 + " " + Name5));

        }else {
            holder.v_status.setVisibility(View.GONE);
            holder.track_order.setVisibility(View.VISIBLE);
        }



        if (data.getId() != null) {
            String title = getColoredSpanned("" +"ID:- "+"", "#000000");
            String Name = getColoredSpanned("fee-"+data.getId() , "#5A5C59");
            holder.orderid.setText(Html.fromHtml(title + " " + Name));
        }else {
            holder.orderid.setText("ID:- ");
        }


        int jan_m_fee = 0, feb_m_fee = 0, mar_m_fee = 0, april_m_fee = 0, may_m_fee = 0, june_m_fee = 0, july_m_fee = 0, aug_m_fee = 0, sep_m_fee = 0, oct_m_fee = 0,
                nov_m_fee = 0, dec_m_fee = 0;

        int jan_m_fee_fine = 0, feb_m_fee_fine = 0, mar_m_fee_fine = 0, april_m_fee_fine = 0, may_m_fee_fine = 0, june_m_fee_fine = 0, july_m_fee_fine = 0,
                aug_m_fee_fine = 0, sep_m_fee_fine = 0, oct_m_fee_fine = 0,
                nov_m_fee_fine = 0, dec_m_fee_fine = 0;

        if (data.getJan_month().equalsIgnoreCase("no")){
            holder.jan_view.setVisibility(View.GONE);
            jan_m_fee = 0;
            jan_m_fee_fine = 0;
        }else{
            holder.jan_view.setVisibility(View.VISIBLE);
            holder.jan_p.setText(data.getJan_particular());
            holder.jan_m.setText("'"+context.getString(R.string.jan)+"'");
            holder.jan_f.setText(data.getJan_fine());
            holder.jan_a.setText(data.getJan_paid());
            jan_m_fee = Integer.parseInt(data.getJan_paid());
            jan_m_fee_fine = Integer.parseInt(data.getJan_fine());
        }

        if (data.getFeb_month().equalsIgnoreCase("no")){
            holder.feb_view.setVisibility(View.GONE);
            feb_m_fee = 0;
            feb_m_fee_fine = 0;
        }else{
            holder.feb_view.setVisibility(View.VISIBLE);
            holder.feb_p.setText(data.getFeb_particular());
            holder.feb_m.setText("'"+context.getString(R.string.feb)+"'");
            holder.feb_f.setText(data.getFeb_fine());
            holder.feb_a.setText(data.getFeb_paid());
            feb_m_fee = Integer.parseInt(data.getFeb_paid());
            feb_m_fee_fine = Integer.parseInt(data.getFeb_fine());
        }

        if (data.getMar_month().equalsIgnoreCase("no")){
            holder.mar_view.setVisibility(View.GONE);
            mar_m_fee = 0;
            mar_m_fee_fine = 0;
        }else{
            holder.mar_view.setVisibility(View.VISIBLE);
            holder.mar_p.setText(data.getMar_particular());
            holder.mar_m.setText("'"+context.getString(R.string.mar)+"'");
            holder.mar_f.setText(data.getMar_fine());
            holder.mar_a.setText(data.getMar_paid());
            mar_m_fee = Integer.parseInt(data.getMar_paid());
            mar_m_fee_fine = Integer.parseInt(data.getMar_fine());
        }

        if (data.getApr_month().equalsIgnoreCase("no")){
            holder.apr_view.setVisibility(View.GONE);
            april_m_fee = 0;
            april_m_fee_fine = 0;
        }else{
            holder.apr_view.setVisibility(View.VISIBLE);
            holder.apr_p.setText(data.getApr_particular());
            holder.apr_m.setText("'"+context.getString(R.string.april)+"'");
            holder.apr_f.setText(data.getApr_fine());
            holder.apr_a.setText(data.getApr_paid());
            april_m_fee = Integer.parseInt(data.getApr_paid());
            april_m_fee_fine = Integer.parseInt(data.getApr_fine());
        }

        if (data.getMay_month().equalsIgnoreCase("no")){
            holder.may_view.setVisibility(View.GONE);
            may_m_fee = 0;
            may_m_fee_fine = 0;
        }else{
            holder.may_view.setVisibility(View.VISIBLE);
            holder.may_p.setText(data.getMay_particular());
            holder.may_m.setText("'"+context.getString(R.string.may)+"'");
            holder.may_f.setText(data.getMay_fine());
            holder.may_a.setText(data.getMay_paid());
            may_m_fee = Integer.parseInt(data.getMay_paid());
            may_m_fee_fine = Integer.parseInt(data.getMay_fine());
        }

        if (data.getJune_month().equalsIgnoreCase("no")){
            holder.jun_view.setVisibility(View.GONE);
            june_m_fee = 0;
            june_m_fee_fine = 0;
        }else{
            holder.jun_view.setVisibility(View.VISIBLE);
            holder.jun_p.setText(data.getJune_particular());
            holder.jun_m.setText("'"+context.getString(R.string.jun)+"'");
            holder.jun_f.setText(data.getJune_fine());
            holder.jun_a.setText(data.getJune_paid());
            june_m_fee = Integer.parseInt(data.getJune_paid());
            june_m_fee_fine = Integer.parseInt(data.getJune_fine());
        }

        if (data.getJuly_month().equalsIgnoreCase("no")){
            holder.jul_view.setVisibility(View.GONE);
            july_m_fee = 0;
            july_m_fee_fine = 0;
        }else{
            holder.jul_view.setVisibility(View.VISIBLE);
            holder.jul_p.setText(data.getJuly_particular());
            holder.jul_m.setText("'"+context.getString(R.string.jul)+"'");
            holder.jul_f.setText(data.getJuly_fine());
            holder.jul_a.setText(data.getJuly_paid());
            july_m_fee = Integer.parseInt(data.getJuly_paid());
            july_m_fee_fine = Integer.parseInt(data.getJuly_fine());
        }

        if (data.getAug_month().equalsIgnoreCase("no")){
            holder.aug_view.setVisibility(View.GONE);
            aug_m_fee = 0;
            aug_m_fee_fine = 0;
        }else{
            holder.aug_view.setVisibility(View.VISIBLE);
            holder.aug_p.setText(data.getAug_particular());
            holder.aug_m.setText("'"+context.getString(R.string.aug)+"'");
            holder.aug_f.setText(data.getAug_fine());
            holder.aug_a.setText(data.getAug_paid());
            aug_m_fee = Integer.parseInt(data.getAug_paid());
            aug_m_fee_fine = Integer.parseInt(data.getAug_fine());
        }

        if (data.getSep_month().equalsIgnoreCase("no")){
            holder.sep_view.setVisibility(View.GONE);
            sep_m_fee = 0;
            sep_m_fee_fine = 0;

        }else{
            holder.sep_view.setVisibility(View.VISIBLE);
            holder.sep_p.setText(data.getSep_particular());
            holder.sep_m.setText("'"+context.getString(R.string.sep)+"'");
            holder.sep_f.setText(data.getSep_fine());
            holder.sep_a.setText(data.getSep_paid());
            sep_m_fee = Integer.parseInt(data.getSep_paid());
            sep_m_fee_fine = Integer.parseInt(data.getSep_fine());
        }

        if (data.getOct_month().equalsIgnoreCase("no")){
            holder.oct_view.setVisibility(View.GONE);
            oct_m_fee = 0;
            oct_m_fee_fine = 0;

        }else{
            holder.oct_view.setVisibility(View.VISIBLE);
            holder.oct_p.setText(data.getOct_particular());
            holder.oct_m.setText("'"+context.getString(R.string.oct)+"'");
            holder.oct_f.setText(data.getOct_fine());
            holder.oct_a.setText(data.getOct_paid());
            oct_m_fee = Integer.parseInt(data.getOct_paid());
            oct_m_fee_fine = Integer.parseInt(data.getOct_fine());
        }

        if (data.getNov_month().equalsIgnoreCase("no")){
            holder.nov_view.setVisibility(View.GONE);
            nov_m_fee = 0;
            nov_m_fee_fine = 0;
        }else{
            holder.nov_view.setVisibility(View.VISIBLE);
            holder.nov_p.setText(data.getNov_particular());
            holder.nov_m.setText("'"+context.getString(R.string.nov)+"'");
            holder.nov_f.setText(data.getNov_fine());
            holder.nov_a.setText(data.getNov_paid());
            nov_m_fee = Integer.parseInt(data.getNov_paid());
            nov_m_fee_fine = Integer.parseInt(data.getNov_fine());
        }

        if (data.getDec_month().equalsIgnoreCase("no")){
            holder.dec_view.setVisibility(View.GONE);
            dec_m_fee = 0;
            dec_m_fee_fine = 0;
        }else{
            holder.dec_view.setVisibility(View.VISIBLE);
            holder.dec_p.setText(data.getDec_particular());
            holder.dec_m.setText("'"+context.getString(R.string.dec)+"'");
            holder.dec_f.setText(data.getDec_fine());
            holder.dec_a.setText(data.getDec_paid());
            dec_m_fee = Integer.parseInt(data.getDec_paid());
            dec_m_fee_fine = Integer.parseInt(data.getDec_fine());
        }

        if (data.getPaymentdate() != null) {
            String title = getColoredSpanned("<b>" +"Payment Date:- "+"</b>", "#000000");
            String Name = getColoredSpanned(Utils.getDateFormated(data.getPaymentdate()) , "#f85164");
            holder.date.setText(Html.fromHtml(title + " " + Name));
        }else {
            holder.date.setText("Payment Date:- Not mentioned");
        }

        //  holder.price.setText(String.valueOf(getTotalPrice()));
        int new_totalPrice = 0,p_balance= 0,d_charge= 0,sub_totalPrice=0,total_fee_fine=0;

//        for (int i = 0; i < data.getProd_data().size(); i++) {
//            totalPrice += Integer.parseInt(data.getProd_data().get(i).getFee_amount());
//            total_fee_fine += Integer.parseInt(data.getProd_data().get(i).getEs_fine());
//        }

        total_fee_fine = jan_m_fee_fine + feb_m_fee_fine + mar_m_fee_fine + april_m_fee_fine + may_m_fee_fine + june_m_fee_fine + july_m_fee_fine +
                aug_m_fee_fine + sep_m_fee_fine + oct_m_fee_fine + nov_m_fee_fine + dec_m_fee_fine;


        sub_totalPrice = jan_m_fee + feb_m_fee + mar_m_fee + april_m_fee + may_m_fee +
                june_m_fee + july_m_fee + aug_m_fee + sep_m_fee + oct_m_fee + nov_m_fee + dec_m_fee;

        new_totalPrice = total_fee_fine + p_balance + sub_totalPrice;


        // item_price=Integer.parseInt(data.getGrand_total());
        d_charge=Integer.parseInt(data.getLast_fee_bal());
        holder.delivery_price.setText(String.valueOf(d_charge) + ".00");

        holder.new_balance.setText(String.valueOf(data.getFee_bal()) + ".00");

        new_totalPrice=sub_totalPrice+total_fee_fine+d_charge;

        holder.a_sub.setText(String.valueOf(sub_totalPrice) + "");
        holder.f_sub.setText(String.valueOf(total_fee_fine) + "");
        holder.totals.setText(String.valueOf(data.getEs_particularamount()) + ".00");
        holder.gross_total.setText(String.valueOf(new_totalPrice) + ".00");

        holder.price.setText(String.valueOf(data.getEs_particularamount()) + ".00");
        //str_price = data.getTotal_price();
        String Nameeee="";
        if (data.getEs_months() != null) {

            String str = data.getEs_months();
            int n = str.length();
            char last = str.charAt(n - 1);
            String value=String.valueOf(last);
            if (value.equalsIgnoreCase(",")){
                String substring = str.substring(0, str.length() - 1); // AB
                String replaced = substring + "";
                Nameeee = getColoredSpanned("" +replaced+".", "#1C8B3B");
            }else{
                Nameeee = getColoredSpanned("" +str+".", "#1C8B3B");
            }
            String title = getColoredSpanned("<b>" +"Fee of: "+"</b>", "#000000");

            holder.mon.setText(Html.fromHtml(title + " " + Nameeee));
        }else {
            holder.mon.setText("Not Mentioned");
        }

        if (data.getGender().equalsIgnoreCase("Male")) {


            if (data.getFather_name() != null) {
                String title = getColoredSpanned("<b>" +"S/O "+"</b>", "#000000");
                String Name = getColoredSpanned("" +data.getFather_name()+"", "#5A5C59");
                holder.add.setText(Html.fromHtml(title + " " + Name));
            }else {
                holder.add.setText("Not Mentioned");
            }

            if (data.getProfile() != null) {
                Utils.progressImg(Constants.getImageBaseURL()+data.getProfile() ,holder.product_img,context);
                // Picasso.with(getActivity()).load(stuList.get(i-1).getProfile()).placeholder(R.drawable.stud).into(dp);
            } else {
                Picasso.get().load(R.drawable.stud).into(holder.product_img);
            }
        }else{

            if (data.getFather_name() != null) {
                String title = getColoredSpanned("<b>" +"D/O "+"</b>", "#000000");
                String Name = getColoredSpanned("" +data.getFather_name()+"", "#5A5C59");
                holder.add.setText(Html.fromHtml(title + " " + Name));
            }else {
                holder.add.setText("Not Mentioned");
            }

            if (data.getProfile() != null) {
                Utils.progressImg(Constants.getImageBaseURL()+data.getProfile(),holder.product_img,context);
                //  Picasso.with(getActivity()).load(stuList.get(i-1).getProfile()).placeholder(R.drawable.f_student).into(dp);
            } else {
                Picasso.get().load(R.drawable.f_student).into(holder.product_img);
            }
        }



        holder.d_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.d_close.setVisibility(View.VISIBLE);
                holder.bottom_price.setVisibility(View.GONE);
                holder.d_add.setVisibility(View.GONE);
            }
        });

        holder.d_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.d_close.setVisibility(View.GONE);
                holder.bottom_price.setVisibility(View.VISIBLE);
                holder.d_add.setVisibility(View.VISIBLE);
            }
        });

        holder.track_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                   prodMethodCallBack.clickMethod(list.get(position));
            }
        });

        holder.bg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Animation animation = AnimationUtils.loadAnimation(context, R.anim.btn_blink_animation);
                holder.bg.startAnimation(animation);
                prodMethodCallBack.clickRECMethod(list.get(position));
            }
        });

    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }
    @Override
    public int getItemCount() {
        if (list != null) {
            return list.size();
        }
        return 0;

    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.jan_view)
        LinearLayout jan_view;
        @BindView(R.id.feb_view)
        LinearLayout feb_view;
        @BindView(R.id.mar_view)
        LinearLayout mar_view;
        @BindView(R.id.apr_view)
        LinearLayout apr_view;
        @BindView(R.id.may_view)
        LinearLayout may_view;
        @BindView(R.id.jun_view)
        LinearLayout jun_view;
        @BindView(R.id.jul_view)
        LinearLayout jul_view;
        @BindView(R.id.aug_view)
        LinearLayout aug_view;
        @BindView(R.id.sep_view)
        LinearLayout sep_view;
        @BindView(R.id.oct_view)
        LinearLayout oct_view;
        @BindView(R.id.nov_view)
        LinearLayout nov_view;
        @BindView(R.id.dec_view)
        LinearLayout dec_view;


        @BindView(R.id.TestDate)
        TextView testDate;

        @BindView(R.id.product_img)
        ImageView product_img;

        @BindView(R.id.v_status)
        LinearLayout v_status;
        @BindView(R.id.waoo)
        RelativeLayout bg;
        @BindView(R.id.gross_total)
        TextView gross_total;

        @BindView(R.id.jan_a)
        TextView jan_a;
        @BindView(R.id.feb_a)
        TextView feb_a;
        @BindView(R.id.mar_a)
        TextView mar_a;
        @BindView(R.id.apr_a)
        TextView apr_a;
        @BindView(R.id.may_a)
        TextView may_a;
        @BindView(R.id.jun_a)
        TextView jun_a;
        @BindView(R.id.jul_a)
        TextView jul_a;
        @BindView(R.id.aug_a)
        TextView aug_a;
        @BindView(R.id.sep_a)
        TextView sep_a;
        @BindView(R.id.oct_a)
        TextView oct_a;
        @BindView(R.id.nov_a)
        TextView nov_a;
        @BindView(R.id.dec_a)
        TextView dec_a;

        @BindView(R.id.jan_f)
        TextView jan_f;
        @BindView(R.id.feb_f)
        TextView feb_f;
        @BindView(R.id.mar_f)
        TextView mar_f;
        @BindView(R.id.apr_f)
        TextView apr_f;
        @BindView(R.id.may_f)
        TextView may_f;
        @BindView(R.id.jun_f)
        TextView jun_f;
        @BindView(R.id.jul_f)
        TextView jul_f;
        @BindView(R.id.aug_f)
        TextView aug_f;
        @BindView(R.id.sep_f)
        TextView sep_f;
        @BindView(R.id.oct_f)
        TextView oct_f;
        @BindView(R.id.nov_f)
        TextView nov_f;
        @BindView(R.id.dec_f)
        TextView dec_f;


        @BindView(R.id.jan_m)
        TextView jan_m;
        @BindView(R.id.feb_m)
        TextView feb_m;
        @BindView(R.id.mar_m)
        TextView mar_m;
        @BindView(R.id.apr_m)
        TextView apr_m;
        @BindView(R.id.may_m)
        TextView may_m;
        @BindView(R.id.jun_m)
        TextView jun_m;
        @BindView(R.id.jul_m)
        TextView jul_m;
        @BindView(R.id.aug_m)
        TextView aug_m;
        @BindView(R.id.sep_m)
        TextView sep_m;
        @BindView(R.id.oct_m)
        TextView oct_m;
        @BindView(R.id.nov_m)
        TextView nov_m;
        @BindView(R.id.dec_m)
        TextView dec_m;

        @BindView(R.id.jan_p)
        TextView jan_p;
        @BindView(R.id.feb_p)
        TextView feb_p;
        @BindView(R.id.mar_p)
        TextView mar_p;
        @BindView(R.id.apr_p)
        TextView apr_p;
        @BindView(R.id.may_p)
        TextView may_p;
        @BindView(R.id.jun_p)
        TextView jun_p;
        @BindView(R.id.jul_p)
        TextView jul_p;
        @BindView(R.id.aug_p)
        TextView aug_p;
        @BindView(R.id.sep_p)
        TextView sep_p;
        @BindView(R.id.oct_p)
        TextView oct_p;
        @BindView(R.id.nov_p)
        TextView nov_p;
        @BindView(R.id.dec_p)
        TextView dec_p;


        @BindView(R.id.name)
        TextView name;

        @BindView(R.id.orderid)
        TextView orderid;

        @BindView(R.id.date)
        TextView date;

        @BindView(R.id.mon)
        TextView mon;

        @BindView(R.id.price)
        TextView price;

        @BindView(R.id.totals)
        TextView totals;

        @BindView(R.id.d_add)
        TextView d_add;

        @BindView(R.id.product_img_receipt)
        ImageView product_img_receipt;

        @BindView(R.id.d_close)
        LinearLayout d_close;

        @BindView(R.id.track_order)
        LinearLayout track_order;

        @BindView(R.id.linearlayout)
        RelativeLayout linearlayout;

        @BindView(R.id.bottom_price)
        RelativeLayout bottom_price;

        @BindView(R.id.layt)
        RelativeLayout layt;

        @BindView(R.id.gift)
        TextView add;

        @BindView(R.id.status)
        TextView status;

        @BindView(R.id.a_sub)
        TextView a_sub;
        @BindView(R.id.f_sub)
        TextView f_sub;

        @BindView(R.id.balance)
        TextView delivery_price;

        @BindView(R.id.new_balance)
        TextView new_balance;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}
