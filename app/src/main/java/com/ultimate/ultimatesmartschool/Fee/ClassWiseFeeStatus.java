package com.ultimate.ultimatesmartschool.Fee;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.Request;
import com.ultimate.ultimatesmartschool.AddmissionForm.SessionAdapter;
import com.ultimate.ultimatesmartschool.AddmissionForm.SessionBean;
import com.ultimate.ultimatesmartschool.BeanModule.ClassBean;
import com.ultimate.ultimatesmartschool.BeanModule.User;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.StudentAttendance.ClassAdapterAttend;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ClassWiseFeeStatus extends AppCompatActivity {
    @BindView(R.id.txtfromdate)
    TextView txtfromdate;
    @BindView(R.id.txttodate)TextView txttodate;
    @BindView(R.id.spinnerClass)
    Spinner spinnerClass;
    @BindView(R.id.parent)
    RelativeLayout parent;
    @BindView(R.id.textNorecord)TextView textNorecord;
    @BindView(R.id.spinnerSession)
    Spinner spinnerSession;
    private SessionBean session;
    View view;
    private LinearLayoutManager layoutManager;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    String fromDate;
    String toDate;
    ArrayList<CHSStudentFeeStatusBean> hwList = new ArrayList<>();
    private CHSStudentFeeStatusadapter adapter;
    @BindView(R.id.recyclerview)
    RecyclerView recyclerview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_class_wise_fee_status);
        ButterKnife.bind(this);
        txtTitle.setText("Class Wise  Fee status");
        txtTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ClassWiseFeeStatus.this,WebViewFeeStat.class));
            }
        });
        layoutManager = new LinearLayoutManager(ClassWiseFeeStatus.this);
        recyclerview.setLayoutManager(layoutManager);
        adapter = new CHSStudentFeeStatusadapter(hwList, ClassWiseFeeStatus.this);
        recyclerview.setAdapter(adapter);

        fetchClass();
        fetchcollegeacadmictear();


    }


    private void fetchClass() {
        ErpProgress.showProgressBar(ClassWiseFeeStatus.this, "Please wait...");
        HashMap<String, String> params = new HashMap<String, String>();
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.CLASSLIST_URL, classapiCallback, ClassWiseFeeStatus.this, params);
    }


    private ArrayList<ClassBean> classList;
    private String classid;
    ApiHandler.ApiCallback classapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            if (error == null) {
                try {
                    classList = ClassBean.parseClassArray(jsonObject.getJSONArray(Constants.CLASSDATA));
                    ClassAdapterAttend adapter = new ClassAdapterAttend(ClassWiseFeeStatus.this, classList,0);
                    spinnerClass.setAdapter(adapter);
                    spinnerClass.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if (i > 0) {
                                classid = classList.get(i - 1).getId();
                                // fetchStudentList();
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {
                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Utils.showSnackBar(error.getMessage(), view.findViewById(R.id.parent));
            }
        }
    };

    private void fetchcollegeacadmictear() {

        HashMap<String, String> params = new HashMap<String, String>();
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.COLGACADEMICYAER_URL, sessionapiCallback, this, params);

    }
    ApiHandler.ApiCallback sessionapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            if (error == null) {
                try {
                    final ArrayList<SessionBean> sessionlist = SessionBean.parseSessionArray(jsonObject.getJSONArray(Constants.SESSIONDATA));
                    SessionAdapter adapter = new SessionAdapter(ClassWiseFeeStatus.this, sessionlist);
                    spinnerSession.setAdapter(adapter);
                    spinnerSession.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            session = sessionlist.get(i);
                            String my_url=Constants.Prod_Base_Url+Constants.feepaidns+Constants.s_key+User.getCurrentUser().getSchoolData().getFi_school_id()+Constants.db_user+User.getCurrentUser().getSchoolData().getDbuser()+Constants.db_host+User.getCurrentUser().getSchoolData().getDbhost()+Constants.db_pass+User.getCurrentUser().getSchoolData().getDbpass()+Constants.db_name+User.getCurrentUser().getSchoolData().getDbname()+Constants.from_finance+session.getStart_date()+Constants.to_finance+session.getEnd_date();
                            Log.e("my_urlnew",my_url);
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Utils.showSnackBar(error.getMessage(), parent);
            }
        }
    };


    @OnClick(R.id.imgBack)
    public void onback() {
        finish();
    }
    @OnClick(R.id.txtfromdate)
    public void openDatePicker() {
        java.util.Calendar cal = java.util.Calendar.getInstance(TimeZone.getDefault()); // Get current date
        DatePickerDialog datePicker = new DatePickerDialog(this,
                R.style.MyDatePickerDialogTheme, onfromdate,
                cal.get(java.util.Calendar.YEAR),
                cal.get(java.util.Calendar.MONTH),
                cal.get(java.util.Calendar.DAY_OF_MONTH));
        datePicker.getDatePicker();
        //.setMinDate(new Date().getTime());
        datePicker.setCancelable(false);
        datePicker.show();
    }


    DatePickerDialog.OnDateSetListener onfromdate = new DatePickerDialog.OnDateSetListener() {


        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

            java.util.Calendar calendar = java.util.Calendar.getInstance(TimeZone.getDefault());
            calendar.set(year, monthOfYear, dayOfMonth);
            Date sub_date = calendar.getTime();
            Calendar value_datePicker = calendar;
            SimpleDateFormat fmtOut = new SimpleDateFormat("dd MMM, yyyy");
            String dateString = fmtOut.format(sub_date);
            txtfromdate.setText("From :"+dateString);
            SimpleDateFormat fmtOut1 = new SimpleDateFormat("yyyy-MM-dd");
            fromDate = fmtOut1.format(sub_date);
        }
    };

    @OnClick(R.id.txttodate)
    public void txttodate() {
        java.util.Calendar cal = java.util.Calendar.getInstance(TimeZone.getDefault()); // Get current date
        DatePickerDialog datePicker = new DatePickerDialog(this,
                R.style.MyDatePickerDialogTheme, ontodate,
                cal.get(java.util.Calendar.YEAR),
                cal.get(java.util.Calendar.MONTH),
                cal.get(java.util.Calendar.DAY_OF_MONTH));
        datePicker.getDatePicker().setMaxDate(new Date().getTime());;
        //
        datePicker.setCancelable(false);
        datePicker.show();
    }


    DatePickerDialog.OnDateSetListener ontodate = new DatePickerDialog.OnDateSetListener() {


        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

            java.util.Calendar calendar = java.util.Calendar.getInstance(TimeZone.getDefault());
            calendar.set(year, monthOfYear, dayOfMonth);
            Date sub_date = calendar.getTime();
            Calendar value_datePicker = calendar;
            SimpleDateFormat fmtOut = new SimpleDateFormat("dd MMM, yyyy");
            String dateString = fmtOut.format(sub_date);
            txttodate.setText("To: "+dateString);
            SimpleDateFormat fmtOut1 = new SimpleDateFormat("yyyy-MM-dd");
            toDate = fmtOut1.format(sub_date);
        }
    };


    @OnClick(R.id.search)public void search(){
        // if(checkValid()){
        ErpProgress.showProgressBar(ClassWiseFeeStatus.this, "Please wait...");
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("class_id", classid);
        params.put("fromdate", fromDate);
        params.put("todate", toDate);

        params.put("s_key", User.getCurrentUser().getSchoolData().getFi_school_id());
        params.put("db_user", User.getCurrentUser().getSchoolData().getDbuser());
        params.put("db_pass", User.getCurrentUser().getSchoolData().getDbpass());
        params.put("db_host", User.getCurrentUser().getSchoolData().getDbhost());
        params.put("db_name", User.getCurrentUser().getSchoolData().getDbname());

        Log.e("s_key", User.getCurrentUser().getSchoolData().getFi_school_id());
        Log.e("db_user", User.getCurrentUser().getSchoolData().getDbuser());
        Log.e("db_pass", User.getCurrentUser().getSchoolData().getDbpass());
        Log.e("db_host", User.getCurrentUser().getSchoolData().getDbhost());
        Log.e("db_name", User.getCurrentUser().getSchoolData().getDbname());

        //params.put("type", selected + "");
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.CHSStudentfeeStatus_URL, studentallapiCallback,ClassWiseFeeStatus.this, params);
        //  }



    }

    ApiHandler.ApiCallback studentallapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            if (error == null) {
                try {
                    if (hwList != null) {
                        hwList.clear();
                    }
                    JSONArray jsonArray = jsonObject.getJSONArray("hostelroom_data");
                    hwList = CHSStudentFeeStatusBean.parseClassArray(jsonArray);
                    if (hwList.size() > 0) {
                        adapter.setHQList(hwList);
                        adapter.notifyDataSetChanged();
                        textNorecord.setVisibility(View.GONE);
                    } else {
                        adapter.setHQList(hwList);
                        adapter.notifyDataSetChanged();
                        textNorecord.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                textNorecord.setVisibility(View.VISIBLE);
                hwList.clear();
                adapter.setHQList(hwList);
                adapter.notifyDataSetChanged();
            }
        }
    };
}
