package com.ultimate.ultimatesmartschool.Fee;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.ultimate.ultimatesmartschool.BeanModule.ClassBean;
import com.ultimate.ultimatesmartschool.R;

import java.util.ArrayList;

public class ClassAdapterfee extends BaseAdapter {
    Context context;
    LayoutInflater inflter;
    private ArrayList<ClassBean> classData;

    public ClassAdapterfee(Context applicationContext, ArrayList<ClassBean> classData) {
        this.context = applicationContext;
        this.classData = classData;
        inflter = (LayoutInflater.from(applicationContext));
    }

    @Override
    public int getCount() {
        return classData.size() + 1;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        view = inflter.inflate(R.layout.spinner_lyt, null);
        TextView label = (TextView) view.findViewById(R.id.txtText);
        if (i == 0) {
            // Default selected Spinner item
            label.setText("All");
        } else {
            ClassBean classObj = classData.get(i - 1);
            label.setText(classObj.getName());
        }

        return view;
    }

    public void setClassData(ArrayList<ClassBean> classData) {
        this.classData = classData;
    }
}
