package com.ultimate.ultimatesmartschool.Fee;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;
import com.ultimate.ultimatesmartschool.BeanModule.User;
import com.ultimate.ultimatesmartschool.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FeePaidAdapterGDS extends RecyclerSwipeAdapter<FeePaidAdapterGDS.Viewholder> {

    ArrayList<FeePaidBeanGDS> hq_list;

    Context listner;

    public FeePaidAdapterGDS(ArrayList<FeePaidBeanGDS> hq_list, Context listner) {
        this.hq_list = hq_list;
        this.listner = listner;



    }


    @Override
    public FeePaidAdapterGDS.Viewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.feepaidlistgds, parent, false);
        FeePaidAdapterGDS.Viewholder viewholder = new FeePaidAdapterGDS.Viewholder(view);
        return viewholder;
    }

    @Override
    public void onBindViewHolder(final FeePaidAdapterGDS.Viewholder holder, final int position) {
        holder.partcularname.setText(hq_list.get(position).getParticularname());
        holder.totaltutionfee.setText(hq_list.get(position).getOrgnl_fee());
        holder.paidtutionfee.setText(hq_list.get(position).getPaid_particular_amnt());
        holder.totaltransfee.setText(hq_list.get(position).getOrgnl_transfee());
        holder.paidtransfee.setText(hq_list.get(position).getPaid_transportfee());
        holder.totalfine.setText(hq_list.get(position).getOrgnl_fine());
        holder.paidfine.setText(hq_list.get(position).getPaid_fine());
        holder.totalamount.setText(hq_list.get(position).getTotalamount());
        holder.totalpaidamnt.setText(hq_list.get(position).getPaidamount());
        holder.balanceAmount.setText(hq_list.get(position).getFee_bal());
        holder.paymentDate.setText(hq_list.get(position).getPaymentdate());
        holder.txtmonths.setText(hq_list.get(position).getMonthname());
        holder.paidprevbal.setText(hq_list.get(position).getPrev_paid_bal());
        holder.totalprevbal.setText(hq_list.get(position).getPrev_total_bal());
        holder.concessiontext.setText(hq_list.get(position).getConcessionamountgds());
        holder.remarks.setText(hq_list.get(position).getRemark());
        holder.txtReceiptno.setText("Receipt No. "+ User.getCurrentUser().getSchoolData().getFi_school_id()+hq_list.get(position).getReceiptno());
    }

    @Override
    public int getItemCount() {
        return hq_list.size();
    }








    public void setroomList(ArrayList<FeePaidBeanGDS> hq_list) {
        this.hq_list = hq_list;
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return 0;
    }


    public class Viewholder extends RecyclerView.ViewHolder {
        @BindView(R.id.partcularname)
        TextView partcularname;
        @BindView(R.id.totaltutionfee)TextView totaltutionfee;
        @BindView(R.id.paidtutionfee)TextView paidtutionfee;
        @BindView(R.id.totaltransfee)TextView totaltransfee;

        @BindView(R.id.paidtransfee)TextView paidtransfee;
        @BindView(R.id.totalfine)TextView totalfine;
        @BindView(R.id.paidfine)TextView paidfine;
        @BindView(R.id.totalamount)TextView totalamount;

        @BindView(R.id.totalpaidamnt)TextView totalpaidamnt;
        @BindView(R.id.balanceAmount)TextView balanceAmount;

        @BindView(R.id.txtmonths)TextView txtmonths;
        @BindView(R.id.txtDate)TextView paymentDate;
        @BindView(R.id.paidprevbal)TextView paidprevbal;
        @BindView(R.id.totalprevbal)TextView totalprevbal;
        @BindView(R.id.concessiontext)TextView concessiontext;
        @BindView(R.id.remarks)TextView remarks;
        @BindView(R.id.txtReceiptno)TextView txtReceiptno;

        public Viewholder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}
