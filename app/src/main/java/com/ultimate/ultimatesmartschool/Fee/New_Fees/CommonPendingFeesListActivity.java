package com.ultimate.ultimatesmartschool.Fee.New_Fees;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.icu.text.SimpleDateFormat;
import android.icu.util.Calendar;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.ultimate.ultimatesmartschool.AddmissionForm.SessionBean;
import com.ultimate.ultimatesmartschool.Fee.FeeListAdapter;
import com.ultimate.ultimatesmartschool.Fee.Fee_Paid_Bean;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.CommonProgress;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;
import com.ultimate.ultimatesmartschool.Utility.PaginationListener;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CommonPendingFeesListActivity extends AppCompatActivity implements FeeListAdapter.ProdMethodCallBack {

    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.imgBack)
    ImageView imgBackmsg;
    @BindView(R.id.recyclerview)
    RecyclerView recyclerView;
    @BindView(R.id.lyttt1)
    RelativeLayout lyttt;

    private LinearLayoutManager layoutManager;
    ArrayList<Fee_Paid_Bean> list;
    private PendingFeeListAdapter mAdapter;
    Animation animation;
    @BindView(R.id.textNorecord)
    TextView txtNorecord;
    @BindView(R.id.totalRecord)
    TextView totalRecord;
    @BindView(R.id.spinnerMonth)
    Spinner spinnerMonth;
    // ArrayList<String> monthList = new ArrayList<>();
    CommonProgress commonProgress;
    @BindView(R.id.main_progress)
    ProgressBar main_progress;
    int  limit=100,page_limit=0,total_pages=0;
    private static final int PAGE_START = 1;
    private int currentPage = PAGE_START;
    private boolean isLastPage = false;
    private boolean isLoading = false;
    String selectMonth = "", name = "", f_name = "", roll = "",
            image = "", gender = "", regist = "", classid = "",
            fee_cate_id = "", fromdate = "", todate = "", class_name = "", place_id = "", route_id = "", tag = "", month_name = "";
    private YearOptionBean yearOptionBean;
    ArrayList<YearOptionBean> monthList = new ArrayList<>();
    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_common_fees_list);
        ButterKnife.bind(this);
        if (getIntent().getExtras() != null) {
            name = getIntent().getExtras().getString("name");
            f_name = getIntent().getExtras().getString("f_name");
            image = getIntent().getExtras().getString("dp");
            gender = getIntent().getExtras().getString("gender");
            roll = getIntent().getExtras().getString("roll");
            regist = getIntent().getExtras().getString("regist");
            classid = getIntent().getExtras().getString("class");
            fee_cate_id = getIntent().getExtras().getString("fee_cate_id");
            class_name = getIntent().getExtras().getString("class_name");
            fromdate = getIntent().getExtras().getString("fromdate");
            todate = getIntent().getExtras().getString("todate");
            place_id = getIntent().getExtras().getString("place_id");
            route_id = getIntent().getExtras().getString("route_id");
            tag = getIntent().getExtras().getString("tag");

        }
        commonProgress=new CommonProgress(this);
        list = new ArrayList<>();

        animation = AnimationUtils.loadAnimation(this, R.anim.btn_blink_animation);


        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        mAdapter = new PendingFeeListAdapter(list, this, this,1);
        recyclerView.setAdapter(mAdapter);
        //fetchFeeDetail(selectMonth);

        if (tag.equalsIgnoreCase("student_wise")) {
            lyttt.setVisibility(View.GONE);
            // fetchFeeDetail(selectMonth);
            fetchFeeDetail(0,selectMonth,limit, "yes");
            txtTitle.setText("Paid Fees List");
        }else if (tag.equalsIgnoreCase("class_wise")) {
            lyttt.setVisibility(View.VISIBLE);
            fetchmonthList();
            txtTitle.setText(class_name+"-class");
        }else {
            lyttt.setVisibility(View.VISIBLE);
            fetchmonthList();
            txtTitle.setText("Pending Fees List");
        }

        recyclerView.addOnScrollListener(new PaginationListener(layoutManager) {
            @Override
            protected void loadMoreItems() {
                // isLoading = true;
                //  Utils.showSnackBar("No more messages found in message list.", parent);
                //  currentPage++;
                // doApiCall();

                if (total_pages>=page_limit) {
                    main_progress.setVisibility(View.VISIBLE);
                    currentPage += 10;
                    page_limit = currentPage + limit;
                    fetchFeeDetail(0,selectMonth,page_limit, "no");
                }else{
                    // main_progress.setVisibility(View.GONE);
                    //  Utils.showSnackBar("No more messages found in message list.", parent);
                }


            }
            @Override
            public boolean isLastPage() {
                return isLastPage;
            }
            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });


    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void fetchmonthList() {
        String start_year="",end_year="";
        start_year= Utils.getYearOnlyNEW(fromdate);
        end_year= Utils.getYearOnlyNEW(todate);
//
//        monthList.add("April"+" ("+start_year+")");
//        monthList.add("May"+" ("+start_year+")");
//        monthList.add("June"+" ("+start_year+")");
//        monthList.add("July"+" ("+start_year+")");
//        monthList.add("August"+" ("+start_year+")");
//        monthList.add("September"+" ("+start_year+")");
//        monthList.add("October"+" ("+start_year+")");
//        monthList.add("November"+" ("+start_year+")");
//        monthList.add("December"+" ("+start_year+")");
//        monthList.add("January"+" ("+end_year+")");
//        monthList.add("February"+" ("+end_year+")");
//        monthList.add("March"+" ("+end_year+")");

        monthList.add(new YearOptionBean("April",start_year));
        monthList.add(new YearOptionBean("May",start_year));
        monthList.add(new YearOptionBean("June",start_year));
        monthList.add(new YearOptionBean("July",start_year));
        monthList.add(new YearOptionBean("August",start_year));
        monthList.add(new YearOptionBean("September",start_year));
        monthList.add(new YearOptionBean("October",start_year));
        monthList.add(new YearOptionBean("November",start_year));
        monthList.add(new YearOptionBean("December",start_year));
        monthList.add(new YearOptionBean("January",end_year));
        monthList.add(new YearOptionBean("February",end_year));
        monthList.add(new YearOptionBean("March",end_year));

        //   Toast.makeText(getApplicationContext(),month_name,Toast.LENGTH_SHORT).show();


//        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
//                android.R.layout.simple_spinner_item, monthList);
//        //set the view for the Drop down list
//        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        //set the ArrayAdapter to the spinner
//        spinnerMonth.setAdapter(dataAdapter);

//        // setting here spinner item selected with position..
//        for(int i=0;i<monthList.size();i++) {
//
//            if (monthList.get(i).equalsIgnoreCase(month_name)) {
//                selectMonth = monthList.get(i);
//                spinnerMonth.setSelection(i);
//
//            }
//        }
//
//        //attach the listener to the spinner
//        spinnerMonth.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long l) {
//                selectMonth = monthList.get(pos);
////                if (tag.equalsIgnoreCase("student_wise")) {
////                     fetchFeesDetails(selectMonth);
////                }else){
////                     fetchFeesDetails(selectMonth);
////                }
//                fetchFeeDetail(0,selectMonth,limit, "yes");
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> adapterView) {
//
//            }
//        });


        YearOptionAdapter dataAdapter = new YearOptionAdapter(CommonPendingFeesListActivity.this, monthList);
        spinnerMonth.setAdapter(dataAdapter);

        Calendar cal=Calendar.getInstance();
        SimpleDateFormat month_date = new SimpleDateFormat("MMMM");
        month_name = month_date.format(cal.getTime());

        // setting here spinner item selected with position..
        for(int i=0;i<monthList.size();i++) {
            if (monthList.get(i).getMonth().equalsIgnoreCase(month_name)) {
                selectMonth = monthList.get(i).getMonth();
                spinnerMonth.setSelection(i);
            }
        }

        //attach the listener to the spinner
        spinnerMonth.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                //  if (i != 0) {
                selectMonth= monthList.get(i).getMonth();
//                    yearOptionBean=monthList.get(i);
//                }
                fetchFeeDetail(0,selectMonth,limit, "yes");
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }


        });


    }


    private void fetchFeeDetail(int i, String selectMonth, int limit, String progress) {


        if (progress.equalsIgnoreCase("yes")){
            commonProgress.show();
        }

        HashMap<String, String> params = new HashMap<String, String>();
        params.put("student_id", regist);
        if (tag.equalsIgnoreCase("student_wise")) {
            params.put("tag", "student_wise");
        }else if (tag.equalsIgnoreCase("class_wise")) {
            params.put("tag", "class_wise");
        }else {
            params.put("tag", "all");
        }
        params.put("page_limit",String.valueOf(limit));
        params.put("month", selectMonth);
        params.put("check", "pending");
        params.put("fee_cate_id", fee_cate_id);
        params.put("classid", classid);
        params.put("todate", todate);
        params.put("fromdate", fromdate);
        String url = Constants.getBaseURL() + Constants.FEE_PAID_STUDENT_NEW;
        ApiHandler.apiHit(Request.Method.POST, url, apiCallback, this, params);
    }

    ApiHandler.ApiCallback apiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            commonProgress.dismiss();
            if (error == null) {
                if (list != null)
                    list.clear();

                JSONArray jsonArray = null;
                try {
                    jsonArray = jsonObject.getJSONArray("fee_data");
                    list = Fee_Paid_Bean.parsFee_Paid_BeanArray(jsonArray);
                    total_pages= Integer.parseInt(list.get(0).getRowcount());
                    if (list.size() > 0) {
                        mAdapter.setMerchantBeans(list);
                        //setanimation on adapter...
                        recyclerView.getAdapter().notifyDataSetChanged();
                        recyclerView.scheduleLayoutAnimation();
                        totalRecord.setVisibility(View.VISIBLE);
                        totalRecord.setText("Total Entries:- "+String.valueOf(list.size()));
                        //-----------end------------
                        txtNorecord.setVisibility(View.GONE);
                    } else {
                        totalRecord.setText("Total Entries:- 0");
                        mAdapter.setMerchantBeans(list);
                        mAdapter.notifyDataSetChanged();
                        txtNorecord.setVisibility(View.VISIBLE);
                    }
                    main_progress.setVisibility(View.GONE);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            } else {
                totalRecord.setText("Total Entries:- 0");
                txtNorecord.setVisibility(View.VISIBLE);
                list.clear();
                mAdapter.setMerchantBeans(list);
                mAdapter.notifyDataSetChanged();
                Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_SHORT).show();
            }
        }
    };

    @OnClick(R.id.imgBack)
    public void imgBackssss() {
        imgBackmsg.startAnimation(animation);
        finish();
    }

    @Override
    public void clickMethod(Fee_Paid_Bean fee_paid_bean) {


    }
}