package com.ultimate.ultimatesmartschool.Fee.New_Fees;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.text.Html;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetSequence;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;
import com.ultimate.ultimatesmartschool.BeanModule.User;
import com.ultimate.ultimatesmartschool.ClassTest.ViewClassWiseTest;
import com.ultimate.ultimatesmartschool.Fee.Fee_Paid_Bean;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.CommonProgress;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;
import com.ultimate.ultimatesmartschool.Utility.PaginationListener;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class FeePaymentHistory extends AppCompatActivity implements FeePaymentAdapter.ProdMethodCallBack {

    SharedPreferences sharedPreferences;

    @BindView(R.id.submit)
    TextView export;

    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.imgBack)
    ImageView imgBackmsg;
    @BindView(R.id.recyclerview)
    RecyclerView recyclerView;

    @BindView(R.id.oneDay)
    RelativeLayout oneDay;
    @BindView(R.id.multiDay)
    RelativeLayout multiDay;
    String apply="full";

    @BindView(R.id.multiple)
    LinearLayout multiple;

    @BindView(R.id.top)
    LinearLayout top;

    @BindView(R.id.bottom)
    RelativeLayout bottom;

    @BindView(R.id.start)
    LinearLayout start;
    @BindView(R.id.end)
    LinearLayout end;
    @BindView(R.id.today)
    LinearLayout today;

    @BindView(R.id.arrow)
    ImageView arrow;

    @BindView(R.id.cal_img)
    ImageView cal_img;

    @BindView(R.id.img1)
    ImageView img1;
    @BindView(R.id.img2)
    ImageView img2;

    int fee_patricular=0;
    @BindView(R.id.a)
    TextView fee_patricular_ammount;
    @BindView(R.id.amount_in_word)
    TextView amount_in_word;
    @BindView(R.id.today_date)
    TextView today_date;
    @BindView(R.id.start_date)
    TextView start_date;
    @BindView(R.id.end_date)
    TextView end_date;

    private LinearLayoutManager layoutManager;
    ArrayList<Fee_Paid_Bean> list;
    private FeePaymentAdapter mAdapter;
    Animation animation;
    @BindView(R.id.textNorecord)
    TextView txtNorecord;
    @BindView(R.id.totalRecord)
    TextView totalRecord;

    CommonProgress commonProgress;
    @BindView(R.id.main_progress)
    ProgressBar main_progress;
    int  limit=250,page_limit=0,total_pages=0;
    private static final int PAGE_START = 1;
    private int currentPage = PAGE_START;
    private boolean isLastPage = false;
    private boolean isLoading = false;
    String start_year="",end_year="";
    String selectMonth = "", name = "", f_name = "", roll = "",
            image = "", gender = "", regist = "", classid = "",
            fee_cate_id = "", fromdate = "", todate = "", class_name = "", place_id = "", route_id = "", tag = ""
            , filter_start_date = "",filter_end_date = "",filter_today_date = "";

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fee_payment_history);
        ButterKnife.bind(this);
        if (getIntent().getExtras() != null) {
            fromdate = getIntent().getExtras().getString("fromdate");
            todate = getIntent().getExtras().getString("todate");
        }
        commonProgress=new CommonProgress(this);
        list = new ArrayList<>();

        animation = AnimationUtils.loadAnimation(this, R.anim.btn_blink_animation);

        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        mAdapter = new FeePaymentAdapter(list, this, this,1);
        recyclerView.setAdapter(mAdapter);
        //fetchFeeDetail(selectMonth);

        if (!restorePrefData_aday()){
            setShowcaseView_aday();
        }else{
            getTodayDate();
        }

        txtTitle.setText("Fees Payment Report");


        recyclerView.addOnScrollListener(new PaginationListener(layoutManager) {
            @Override
            protected void loadMoreItems() {
                // isLoading = true;
                //  Utils.showSnackBar("No more messages found in message list.", parent);
                //  currentPage++;
                // doApiCall();

                if (total_pages>=page_limit) {
                    main_progress.setVisibility(View.VISIBLE);
                    currentPage += 10;
                    page_limit = currentPage + limit;
                    fetchFeeDetail(0,filter_today_date,filter_start_date,filter_end_date,page_limit, "no");
                }else{
                    // main_progress.setVisibility(View.GONE);
                    //  Utils.showSnackBar("No more messages found in message list.", parent);
                }


            }
            @Override
            public boolean isLastPage() {
                return isLastPage;
            }
            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });


    }


    private void setShowcaseViewP() {
        new TapTargetSequence(this)
                .targets(TapTarget.forView(export,"Export Excel!","Tap the Export button to export report.")
                        .outerCircleColor(R.color.light)
                        .outerCircleAlpha(0.96f)
                        .targetCircleColor(R.color.white)
                        .titleTextSize(20)
                        .titleTextColor(R.color.white)
                        .descriptionTextSize(14)
                        .descriptionTextColor(R.color.black)
                        .textColor(R.color.black)
                        .textTypeface(Typeface.SANS_SERIF)
                        .dimColor(R.color.black)
                        .drawShadow(true)
                        .cancelable(false)
                        .tintTarget(true)
                        .transparentTarget(true)
                        .targetRadius(40)).listener(new TapTargetSequence.Listener() {
                    @RequiresApi(api = Build.VERSION_CODES.N)
                    @Override
                    public void onSequenceFinish() {
                        // Toast.makeText(getApplicationContext(),"Sequence Finished",Toast.LENGTH_SHORT).show();
                        savePrefDataP();

                    }

                    @Override
                    public void onSequenceStep(TapTarget lastTarget, boolean targetClicked) {
                        // Toast.makeText(getActivity(),"GREAT!",Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onSequenceCanceled(TapTarget lastTarget) {

                    }
                }).start();
    }

    private void savePrefDataP(){
        sharedPreferences=this.getSharedPreferences("boarding_pref_view_export1",MODE_PRIVATE);
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putBoolean("IsFirstTimeVisit_view_export1",true);
        editor.apply();
    }

    private boolean restorePrefDataP(){
        sharedPreferences = this.getSharedPreferences("boarding_pref_view_export1",MODE_PRIVATE);
        return sharedPreferences.getBoolean("IsFirstTimeVisit_view_export1",false);
    }

    private void setShowcaseView_aday() {
        new TapTargetSequence(this)
                .targets(TapTarget.forView(cal_img,"Date Button!","Tap the Date button to View Day-Wise.")
                        .outerCircleColor(R.color.light)
                        .outerCircleAlpha(0.96f)
                        .targetCircleColor(R.color.white)
                        .titleTextSize(20)
                        .titleTextColor(R.color.white)
                        .descriptionTextSize(14)
                        .descriptionTextColor(R.color.black)
                        .textColor(R.color.black)
                        .textTypeface(Typeface.SANS_SERIF)
                        .dimColor(R.color.black)
                        .drawShadow(true)
                        .cancelable(false)
                        .tintTarget(true)
                        .transparentTarget(true)
                        .targetRadius(40)).listener(new TapTargetSequence.Listener() {
                    @RequiresApi(api = Build.VERSION_CODES.N)
                    @Override
                    public void onSequenceFinish() {
                        // Toast.makeText(getApplicationContext(),"Sequence Finished",Toast.LENGTH_SHORT).show();
                        savePrefData_aday();
                        //openDialog();
                        getTodayDate();
                    }

                    @Override
                    public void onSequenceStep(TapTarget lastTarget, boolean targetClicked) {
                        // Toast.makeText(getActivity(),"GREAT!",Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onSequenceCanceled(TapTarget lastTarget) {

                    }
                }).start();
    }

    private void savePrefData_aday(){
        sharedPreferences=this.getSharedPreferences("boarding_pref_add_attend_vday1",MODE_PRIVATE);
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putBoolean("IsFirstTimeVisit_add_attend_vday1",true);
        editor.apply();
    }

    private boolean restorePrefData_aday(){
        sharedPreferences = this.getSharedPreferences("boarding_pref_add_attend_vday1",MODE_PRIVATE);
        return sharedPreferences.getBoolean("IsFirstTimeVisit_add_attend_vday1",false);
    }




    public void getTodayDate() {
        java.util.Calendar c = java.util.Calendar.getInstance();
        c.set(java.util.Calendar.HOUR_OF_DAY, 00);
        c.set(java.util.Calendar.MINUTE, 00);
        c.set(Calendar.SECOND, 00);
        Date setdate = c.getTime();
        java.text.SimpleDateFormat fmtOut = new java.text.SimpleDateFormat("dd MMM, yyyy");
        String dateString = fmtOut.format(setdate);
        //end_date.setText(dateString);
        today_date.setText(dateString);
        java.text.SimpleDateFormat dateFrmOut = new SimpleDateFormat("yyyy-MM-dd");
        filter_today_date = dateFrmOut.format(setdate);
        filter_end_date = dateFrmOut.format(setdate);

        String title = getColoredSpanned("" +dateString+"", "#000000");
        String Name = getColoredSpanned("(" +"Today"+")", "#f4685b");
        end_date.setText(Html.fromHtml(title + " " + Name));


        fetchFeeDetail(0,filter_today_date,filter_start_date,filter_end_date,limit, "yes");

        //for Yesterday
//        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
//        Calendar cal = Calendar.getInstance();
//        cal.add(Calendar.DATE, -1);
//        ydate= dateFormat.format(cal.getTime());
    }

    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }


    @OnClick(R.id.end)
    public void end() {
        Toast.makeText(getApplicationContext(),"You can't change end date",Toast.LENGTH_SHORT).show();

    }


    @OnClick(R.id.start)
    public void start() {
        java.util.Calendar cal = java.util.Calendar.getInstance(TimeZone.getDefault()); // Get current date
        DatePickerDialog datePicker = new DatePickerDialog(FeePaymentHistory.this,
                R.style.MyDatePickerDialogTheme, ondate1,
                cal.get(java.util.Calendar.YEAR),
                cal.get(java.util.Calendar.MONTH),
                cal.get(java.util.Calendar.DAY_OF_MONTH)-1);
        datePicker.getDatePicker().setMaxDate(new java.util.Date().getTime());
        datePicker.setCancelable(false);
        datePicker.show();
    }

    DatePickerDialog.OnDateSetListener ondate1 = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            java.util.Calendar calendar = java.util.Calendar.getInstance(TimeZone.getDefault());
            calendar.set(year, monthOfYear, dayOfMonth);
            calendar.set(Calendar.HOUR_OF_DAY, 00);
            calendar.set(Calendar.MINUTE, 00);
            calendar.set(Calendar.SECOND, 00);
            java.util.Date setdate = calendar.getTime();
            SimpleDateFormat fmtOut = new SimpleDateFormat("dd MMM, yyyy");
            String dateString = fmtOut.format(setdate);
            start_date.setText(dateString);
            SimpleDateFormat dateFrmOut = new SimpleDateFormat("yyyy-MM-dd");
            filter_start_date = dateFrmOut.format(setdate);
            fetchFeeDetail(0,filter_today_date,filter_start_date,filter_end_date,limit, "yes");
        }
    };



    @OnClick(R.id.today)
    public void today() {
        java.util.Calendar cal = java.util.Calendar.getInstance(TimeZone.getDefault()); // Get current date
        DatePickerDialog datePicker = new DatePickerDialog(FeePaymentHistory.this,
                R.style.MyDatePickerDialogTheme, ondate,
                cal.get(java.util.Calendar.YEAR),
                cal.get(java.util.Calendar.MONTH),
                cal.get(java.util.Calendar.DAY_OF_MONTH));
        datePicker.getDatePicker().setMaxDate(new java.util.Date().getTime());
        datePicker.setCancelable(false);
        datePicker.show();
    }

    DatePickerDialog.OnDateSetListener ondate = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            java.util.Calendar calendar = java.util.Calendar.getInstance(TimeZone.getDefault());
            calendar.set(year, monthOfYear, dayOfMonth);
            calendar.set(Calendar.HOUR_OF_DAY, 00);
            calendar.set(Calendar.MINUTE, 00);
            calendar.set(Calendar.SECOND, 00);
            java.util.Date setdate = calendar.getTime();
            SimpleDateFormat fmtOut = new SimpleDateFormat("dd MMM, yyyy");
            String dateString = fmtOut.format(setdate);
            today_date.setText(dateString);
            SimpleDateFormat dateFrmOut = new SimpleDateFormat("yyyy-MM-dd");
            filter_today_date = dateFrmOut.format(setdate);
            fetchFeeDetail(0,filter_today_date,filter_start_date,filter_end_date,limit, "yes");

        }
    };


    @OnClick(R.id.oneDay)
    public void oneDay() {
        apply="full";
        img1.setVisibility(View.VISIBLE);
        img2.setVisibility(View.GONE);
        today.setVisibility(View.VISIBLE);
        multiple.setVisibility(View.GONE);
        multiDay.setBackgroundColor(Color.parseColor("#00000000"));
        oneDay.setBackgroundColor(Color.parseColor("#66000000"));
        if (!filter_start_date.equalsIgnoreCase("")) {
            today_date.setText(Utils.getDateFormated(filter_start_date));
            filter_today_date=filter_start_date;
            fetchFeeDetail(0, filter_today_date, filter_start_date, filter_end_date, limit, "yes");
        }else{
            fetchFeeDetail(0,filter_today_date,filter_start_date,filter_end_date,limit, "yes");
        }
    }

    @OnClick(R.id.multiDay)
    public void multiDay() {
        apply="multi";
        img2.setVisibility(View.VISIBLE);
        today.setVisibility(View.GONE);
        multiple.setVisibility(View.VISIBLE);
        img1.setVisibility(View.GONE);
        oneDay.setBackgroundColor(Color.parseColor("#00000000"));
        multiDay.setBackgroundColor(Color.parseColor("#66000000"));
//        if (!filter_start_date.equalsIgnoreCase("")) {
//            fetchFeeDetail(0, filter_today_date, filter_start_date, filter_end_date, limit, "yes");
//        }else{
        start_date.setText(Utils.getDateFormated(filter_today_date));
        filter_start_date=filter_today_date;
        fetchFeeDetail(0, filter_today_date, filter_start_date, filter_end_date, limit, "yes");
        //  }
    }


    private void fetchFeeDetail(int i, String filter_today_date,String filter_start_date,String filter_end_date, int limit, String progress) {
        fee_patricular=0;
        if (progress.equalsIgnoreCase("yes")){
            commonProgress.show();
        }

        HashMap<String, String> params = new HashMap<String, String>();
        params.put("student_id", regist);
        if (tag.equalsIgnoreCase("student_wise")) {
            params.put("tag", "student_wise");
        }else if (tag.equalsIgnoreCase("class_wise")) {
            params.put("tag", "class_wise");
        }else {
            params.put("tag", "all");
        }
        params.put("page_limit",String.valueOf(limit));
        params.put("month", selectMonth);
        params.put("filter_today_date", filter_today_date);
        params.put("filter_start_date", filter_start_date);
        params.put("filter_end_date", filter_end_date);
        params.put("classid", classid);
        params.put("check", "paid_history");
        params.put("check_view", apply);
        params.put("todate", todate);
        params.put("fromdate", fromdate);
        String url = Constants.getBaseURL() + Constants.FEE_PAID_STUDENT_NEW;
        ApiHandler.apiHit(Request.Method.POST, url, apiCallback, this, params);
    }

    ApiHandler.ApiCallback apiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            commonProgress.dismiss();
            if (error == null) {
                if (list != null)
                    list.clear();

                JSONArray jsonArray = null;
                try {
                    jsonArray = jsonObject.getJSONArray("fee_data");
                    list = Fee_Paid_Bean.parsFee_Paid_BeanArray(jsonArray);
                    total_pages= Integer.parseInt(list.get(0).getRowcount());
                    if (list.size() > 0) {
                        mAdapter.setMerchantBeans(list);
                        //setanimation on adapter...
                        recyclerView.getAdapter().notifyDataSetChanged();
                        recyclerView.scheduleLayoutAnimation();
                        totalRecord.setVisibility(View.VISIBLE);
                        top.setVisibility(View.VISIBLE);
                        bottom.setVisibility(View.VISIBLE);
                        totalRecord.setText("Total Entries:- "+String.valueOf(total_pages));
                        //-----------end------------
                        txtNorecord.setVisibility(View.GONE);

                        for (int i = 0; i < list.size(); i++) {
                            fee_patricular += Integer.parseInt(list.get(i).getEs_particularamount());
                        }
                        settotal(fee_patricular);


                    } else {
                        fee_patricular=0;
                        totalRecord.setText("Total Entries:- 0");
                        mAdapter.setMerchantBeans(list);
                        mAdapter.notifyDataSetChanged();
                        txtNorecord.setVisibility(View.VISIBLE);
                        top.setVisibility(View.GONE);
                        bottom.setVisibility(View.GONE);
                        export.setVisibility(View.GONE);
                    }
                    main_progress.setVisibility(View.GONE);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            } else {
                export.setVisibility(View.GONE);
                fee_patricular=0;
                totalRecord.setText("Total Entries:- 0");
                txtNorecord.setVisibility(View.VISIBLE);
                list.clear();
                mAdapter.setMerchantBeans(list);
                mAdapter.notifyDataSetChanged();
                top.setVisibility(View.GONE);
                bottom.setVisibility(View.GONE);
                Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_SHORT).show();
            }
        }
    };

    private void settotal(int fee_patricular) {
        fee_patricular_ammount.setText(String.valueOf(fee_patricular)+".00/-");

        String return_val_in_english =   EnglishNumberToWords.convert(fee_patricular).toUpperCase();
        if (fee_patricular != 0) {
            String title = getColoredSpanned("" +"Amount in word: "+"", "#ffffff");
            String Name = getColoredSpanned("" + return_val_in_english +" RUPEES.", "#ffffff");
            amount_in_word.setText(Html.fromHtml(title + " " + Name));
        }else {
            amount_in_word.setText("Not Mentioned");
        }

        export.setVisibility(View.VISIBLE);
        if (!restorePrefDataP()){
            setShowcaseViewP();
        }

    }


    @SuppressLint("RestrictedApi")
    @OnClick(R.id.submit)
    public void submit() {
        export.startAnimation(animation);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.S) {
            importData();
        } else {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {

                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    Toast.makeText(this, "Please provide storage permission from app settings", Toast.LENGTH_LONG).show();
                } else {

                    ActivityCompat.requestPermissions(this,
                            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            1);
                }
            } else {
                importData();

            }
        }
    }

    @SuppressLint("RestrictedApi")
    private void importData() {
        // File filePath = new File(Environment.getExternalStorageDirectory() + "/Demo.xls");
        Workbook wb = new HSSFWorkbook();

        Cell cell = null;

        String ndate="",sub_staff="",sub_date="";

        if (apply.equalsIgnoreCase("full")){
            sub_staff= Utils.getDateFormated(filter_today_date);
        }else {
            sub_staff=Utils.getDateFormated(filter_start_date)+"_to_"+Utils.getDateFormated(filter_end_date);
        }

        Sheet sheet = null;
        sheet = wb.createSheet(sub_staff+" Report");
        //Now column and row
        Row row = sheet.createRow(0);

        cell = row.createCell(0);
        cell.setCellValue("S No");

        cell = row.createCell(1);
        cell.setCellValue("Receipt No");

        cell = row.createCell(2);
        cell.setCellValue("Admission No");

        cell = row.createCell(3);
        cell.setCellValue("Class/Section");

        cell = row.createCell(4);
        cell.setCellValue("Student Name");

        cell = row.createCell(5);
        cell.setCellValue("Father Name");

        cell = row.createCell(6);
        cell.setCellValue("Total Amount");


        cell = row.createCell(7);
        cell.setCellValue("Amount Paid");

        cell = row.createCell(8);
        cell.setCellValue("Due Balance");


        //column width
        sheet.setColumnWidth(0, (20 * 100));
        sheet.setColumnWidth(1, (20 * 200));
        sheet.setColumnWidth(2, (30 * 150));
        sheet.setColumnWidth(3, (30 * 250));
        sheet.setColumnWidth(4, (30 * 200));
        sheet.setColumnWidth(5, (30 * 200));
        sheet.setColumnWidth(6, (30 * 200));
        sheet.setColumnWidth(7, (30 * 200));
        sheet.setColumnWidth(8, (30 * 200));

        for (int i = 0; i < list.size(); i++) {

            Row row1 = sheet.createRow(i + 1);

            cell = row1.createCell(0);
            cell.setCellValue(i+1);

            cell = row1.createCell(1);
            cell.setCellValue(User.getCurrentUser().getSchoolData().getFi_school_id()+"-"+list.get(i).getId());

            cell = row1.createCell(2);
            cell.setCellValue(list.get(i).getS_id());

            cell = row1.createCell(3);
            cell.setCellValue((list.get(i).getClass_name()+"-"+list.get(i).getSection_name()));
            //  cell.setCellStyle(cellStyle);

            cell = row1.createCell(4);
            cell.setCellValue(list.get(i).getName());

            cell = row1.createCell(5);
            cell.setCellValue(list.get(i).getFather_name());

            cell = row1.createCell(6);
            cell.setCellValue(list.get(i).getEs_totalamount()+".00");

            cell = row1.createCell(7);
            cell.setCellValue(list.get(i).getEs_particularamount()+".00");

            cell = row1.createCell(8);
            cell.setCellValue(list.get(i).getFee_bal()+".00");


            sheet.setColumnWidth(0, (20 * 100));
            sheet.setColumnWidth(1, (20 * 200));
            sheet.setColumnWidth(2, (30 * 150));
            sheet.setColumnWidth(3, (30 * 250));
            sheet.setColumnWidth(4, (30 * 200));
            sheet.setColumnWidth(5, (30 * 200));
            sheet.setColumnWidth(6, (30 * 200));
            sheet.setColumnWidth(7, (30 * 200));
            sheet.setColumnWidth(8, (30 * 200));


        }
        String fileName;
        if (sub_staff.equalsIgnoreCase("")){
            fileName = sub_date+"_" +System.currentTimeMillis() + ".xls";
        }else {
            fileName = sub_staff+"_"+"_"+"_" + System.currentTimeMillis() + ".xls";
        }


        // File file = new File(getActivity().getExternalFilesDir(null), fileName);

        File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS), fileName);

        FileOutputStream fileOutputStream = null;

        try {
            fileOutputStream = new FileOutputStream(file);
            wb.write(fileOutputStream);
            Utils.openSuccessDialog("Success!","Export file in "+file,"",this);
        } catch (IOException e) {
            Utils.openErrorDialog("Error writing Exception: "+ e,this);
        } catch (Exception e) {
            Utils.openErrorDialog("Failed to save file due to Exception: "+ e,this);
        } finally {
            try {
                if (null != fileOutputStream) {
                    fileOutputStream.close();
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

    }

    @SuppressLint("RestrictedApi")
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == 1 && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            importData();
        } else {
            Toast.makeText(getApplicationContext(), "Permission Denied", Toast.LENGTH_SHORT).show();
        }
    }

    @OnClick(R.id.imgBack)
    public void imgBackssss() {
        imgBackmsg.startAnimation(animation);
        finish();
    }

    @Override
    public void clickMethod(Fee_Paid_Bean fee_paid_bean) {
        start_year= Utils.getYearOnlyNEW(fromdate);
        end_year= Utils.getYearOnlyNEW(todate);
        Gson gson = new Gson();
        String prod_data = gson.toJson(fee_paid_bean, Fee_Paid_Bean.class);
        Intent intent = new Intent(FeePaymentHistory.this, FeeDetailsActivity_New.class);
        intent.putExtra("fee_data", prod_data);
        intent.putExtra("fromdate", start_year);
        intent.putExtra("todate", end_year);
        startActivity(intent);

    }
}