package com.ultimate.ultimatesmartschool.Fee;

import com.ultimate.ultimatesmartschool.Utility.DebugLog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ParticularBean {

    private String id;
    private String fee_particular;

    public String getFee_particular() {
        return fee_particular;
    }

    public void setFee_particular(String fee_particular) {
        this.fee_particular = fee_particular;
    }

    public String getFee_amount() {
        return fee_amount;
    }

    public void setFee_amount(String fee_amount) {
        this.fee_amount = fee_amount;
    }

    public String getEs_fine() {
        return es_fine;
    }

    public void setEs_fine(String es_fine) {
        this.es_fine = es_fine;
    }

    private String fee_amount;
    private String  es_fine;
//    private String  prev_paid_bal;
//    private String prev_total_bal;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public static ArrayList<ParticularBean> parseParticularBeanArray(JSONArray arrayObj) {
        ArrayList<ParticularBean> list = new ArrayList<ParticularBean>();
        try {

            for (int i = 0; i < arrayObj.length(); i++) {
                ParticularBean p = parseParticularBeanObject(arrayObj.getJSONObject(i));
                if (p != null) {
                    list.add(p);
                }
            }
        } catch (Exception e) {
            DebugLog.printLog("exp",e.getMessage());
            e.printStackTrace();
        }
        return list;
    }

    public static ParticularBean parseParticularBeanObject(JSONObject jsonObject) {
        ParticularBean casteObj = new ParticularBean();
        try {

            if (jsonObject.has("id") && !jsonObject.getString("id").isEmpty() && !jsonObject.getString("id").equalsIgnoreCase("null")) {
                casteObj.setId(jsonObject.getString("id"));
            }
            if (jsonObject.has("fee_particular") && !jsonObject.getString("fee_particular").isEmpty() && !jsonObject.getString("fee_particular").equalsIgnoreCase("null")) {
                casteObj.setFee_particular(jsonObject.getString("fee_particular"));
            }
            if (jsonObject.has("fee_amount") && !jsonObject.getString("fee_amount").isEmpty() && !jsonObject.getString("fee_amount").equalsIgnoreCase("null")) {
                casteObj.setFee_amount(jsonObject.getString("fee_amount"));
            }
            if (jsonObject.has("es_fine") && !jsonObject.getString("es_fine").isEmpty() && !jsonObject.getString("es_fine").equalsIgnoreCase("null")) {
                casteObj.setEs_fine(jsonObject.getString("es_fine"));
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }
        return casteObj;
    }
}
