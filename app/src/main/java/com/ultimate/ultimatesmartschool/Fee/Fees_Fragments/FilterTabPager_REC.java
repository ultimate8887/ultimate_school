package com.ultimate.ultimatesmartschool.Fee.Fees_Fragments;

import android.content.Context;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.ultimate.ultimatesmartschool.MSG_NEW.Fragments.Send_AdminFragment;
import com.ultimate.ultimatesmartschool.MSG_NEW.Fragments.Send_StaffFragment;
import com.ultimate.ultimatesmartschool.MSG_NEW.Fragments.Send_StdFragment;

public class FilterTabPager_REC extends FragmentStatePagerAdapter {
    String regist = "", classid = "", fromdate = "", todate = "", tag = "";
    Context context;
    public FilterTabPager_REC(FragmentManager fm, String tag,String regist,String classid,String fromdate,String todate, Context context) {
        super(fm);
        this.context = context;
        this.tag=tag;
        this.regist=regist;
        this.classid=classid;
        this.fromdate=fromdate;
        this.todate=todate;
        //Log.e("Send_StdFragment",String.valueOf(v));
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new Pending_Fees(tag,regist,classid,fromdate,todate,context);
            case 1:
                return new Complete_Fees(tag,regist,classid,fromdate,todate,context);
            case 2:
                return new Cancel_Fees(tag,regist,classid,fromdate,todate,context);
            default:
                break;
        }
        return null;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        // Generate title based on item position
        switch (position) {
            case 0:
                return "Pending";
            case 1:
                return "Complete";
            case 2:
                return "Cancel";
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return 3;
    }
}
