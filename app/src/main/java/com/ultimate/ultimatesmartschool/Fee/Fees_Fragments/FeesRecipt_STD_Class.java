package com.ultimate.ultimatesmartschool.Fee.Fees_Fragments;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.tabs.TabLayout;
import com.ultimate.ultimatesmartschool.MSG_NEW.FilterTabPager;
import com.ultimate.ultimatesmartschool.MSG_NEW.SendMSG;
import com.ultimate.ultimatesmartschool.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class FeesRecipt_STD_Class extends AppCompatActivity {
    @BindView(R.id.imgBack)
    ImageView imgBackks;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    private Animation animation;

    @BindView(R.id.viewpager)
    ViewPager viewpager;
    @BindView(R.id.tab)
    TabLayout tablayout;
    String selectMonth = "", name = "", f_name = "", roll = "",
            image = "", gender = "", regist = "", classid = "",
            fee_cate_id = "", fromdate = "", todate = "", class_name = "", place_id = "", route_id = "", tag = "", month_name = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_m_s_g);
        ButterKnife.bind(this);

        if (getIntent().getExtras() != null) {
            name = getIntent().getExtras().getString("name");
            f_name = getIntent().getExtras().getString("f_name");
            image = getIntent().getExtras().getString("dp");
            gender = getIntent().getExtras().getString("gender");
            roll = getIntent().getExtras().getString("roll");
            regist = getIntent().getExtras().getString("regist");
            classid = getIntent().getExtras().getString("class");
            fee_cate_id = getIntent().getExtras().getString("fee_cate_id");
            class_name = getIntent().getExtras().getString("class_name");
            fromdate = getIntent().getExtras().getString("fromdate");
            todate = getIntent().getExtras().getString("todate");
            place_id = getIntent().getExtras().getString("place_id");
            route_id = getIntent().getExtras().getString("route_id");
            tag = getIntent().getExtras().getString("tag");

        }

        animation = AnimationUtils.loadAnimation(FeesRecipt_STD_Class.this, R.anim.btn_blink_animation);

        setupTabPager();
    }
    private void setupTabPager() {

        viewpager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tablayout));

        tablayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(viewpager) {

            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                super.onTabSelected(tab);
                String value=String.valueOf(tab.getText());

                if (value.equalsIgnoreCase("Pending")){
                    tablayout.setTabTextColors(

                            // Unselected Tab Text Color
                            ContextCompat.getColor(FeesRecipt_STD_Class.this, R.color.d_light),
                            // Selected Tab Text Color
                            ContextCompat.getColor(FeesRecipt_STD_Class.this, R.color.leave)
                    );
                    // Selected Tab Indicator Color
                 //   tablayout.setSelectedTabIndicatorColor(getResources().getColor(R.color.leave));
                    //  tablayout.setSelectedTabIndicatorHeight(5);
                }else if (value.equalsIgnoreCase("Complete")) {
                    tablayout.setTabTextColors(

                            // Unselected Tab Text Color
                            ContextCompat.getColor(FeesRecipt_STD_Class.this, R.color.d_light),
                            // Selected Tab Text Color
                            ContextCompat.getColor(FeesRecipt_STD_Class.this, R.color.green)
                    );
                    // Selected Tab Indicator Color
                  //  tablayout.setSelectedTabIndicatorColor(getResources().getColor(R.color.present));
                    //tablayout.setSelectedTabIndicatorHeight(5);
                }else {
                    tablayout.setTabTextColors(

                            // Unselected Tab Text Color
                            ContextCompat.getColor(FeesRecipt_STD_Class.this, R.color.d_light),
                            // Selected Tab Text Color
                            ContextCompat.getColor(FeesRecipt_STD_Class.this, R.color.light_red)
                    );
                    // Selected Tab Indicator Color
                  //  tablayout.setSelectedTabIndicatorColor(getResources().getColor(R.color.absent));
                    //tablayout.setSelectedTabIndicatorHeight(5);
                }

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                super.onTabUnselected(tab);
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                super.onTabReselected(tab);
            }
        });


        FilterTabPager_REC adapter;
        if (tag.equalsIgnoreCase("stu_wise_rec")){
            txtTitle.setText(name);
            adapter = new FilterTabPager_REC(getSupportFragmentManager(),tag,regist,classid,fromdate,todate,this);
        }else if (tag.equalsIgnoreCase("fees_receipt_all")){
            txtTitle.setText("Online Fees Paid List");
            adapter = new FilterTabPager_REC(getSupportFragmentManager(),tag,regist,classid,fromdate,todate,this);
        }else{
            txtTitle.setText(class_name+"-class");
            adapter = new FilterTabPager_REC(getSupportFragmentManager(),tag,regist,classid,fromdate,todate,this);
        }
        viewpager.setAdapter(adapter);
        tablayout.setupWithViewPager(viewpager);
    }

    @OnClick(R.id.imgBack)
    public void imgBackks()  {
        imgBackks.startAnimation(animation);
        finish();
    }
}