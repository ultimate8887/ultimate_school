package com.ultimate.ultimatesmartschool.Fee.New_Fees;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.ultimate.ultimatesmartschool.Fee.Fee_Paid_Bean;
import com.ultimate.ultimatesmartschool.Fee.ParticularBean;
import com.ultimate.ultimatesmartschool.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ParticularItemAdapter extends RecyclerView.Adapter<ParticularItemAdapter.Viewholder> {
    private final Context mContext;
    String month="";
    ArrayList<Fee_Paid_Bean.MonthDataBean> list;

    public ParticularItemAdapter(ArrayList<Fee_Paid_Bean.MonthDataBean> list, Context mContext) {
        this.list = list;
        this.mContext = mContext;

    }

    @Override
    public ParticularItemAdapter.Viewholder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.pay_fee_adapter_new, viewGroup, false);
        ParticularItemAdapter.Viewholder viewholder = new ParticularItemAdapter.Viewholder(view);
        return viewholder;
    }

    @Override
    public void onBindViewHolder(final ParticularItemAdapter.Viewholder viewholder, int i) {


        if (list.get(i).getFee_particular() !=null) {
            viewholder.p.setText(list.get(i).getFee_particular());
        }
        if (list.get(i).getFee_amount() !=null) {
            viewholder.a.setText(list.get(i).getFee_amount());
        }

        if (list.get(i).getEs_fine() !=null) {
            viewholder.f.setText(list.get(i).getEs_fine());
        }

        if (list.get(i).getFee_month() !=null) {
            viewholder.m.setText("'"+list.get(i).getFee_month()+"'");
        }

    }

    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void setFeeList(ArrayList<Fee_Paid_Bean.MonthDataBean> list) {
        this.list = list;
    }

    public static class Viewholder extends RecyclerView.ViewHolder {
        @BindView(R.id.p)
        TextView p;
        @BindView(R.id.a)
        TextView a;
        @BindView(R.id.m)
        TextView m;
        @BindView(R.id.f)
        TextView f;
        public Viewholder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}
