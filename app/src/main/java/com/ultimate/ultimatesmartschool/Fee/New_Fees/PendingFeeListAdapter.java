package com.ultimate.ultimatesmartschool.Fee.New_Fees;

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.github.siyamed.shapeimageview.CircularImageView;
import com.squareup.picasso.Picasso;
import com.ultimate.ultimatesmartschool.Fee.FeeListAdapter;
import com.ultimate.ultimatesmartschool.Fee.Fee_Paid_Bean;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PendingFeeListAdapter extends RecyclerView.Adapter<PendingFeeListAdapter.ViewHolder> {

    Context context;
    ArrayList<Fee_Paid_Bean> list;
    private FeeListAdapter.ProdMethodCallBack prodMethodCallBack;
    private Animation animation;
    private int view_value;

    public PendingFeeListAdapter(ArrayList<Fee_Paid_Bean> list, Context context, FeeListAdapter.ProdMethodCallBack prodMethodCallBack, int view_value) {
        this.list=list;
        this.context=context;
        this.prodMethodCallBack=prodMethodCallBack;
        this.view_value=view_value;
    }

    public void setMerchantBeans(ArrayList<Fee_Paid_Bean> list) {
        this.list = list;
    }

    public interface ProdMethodCallBack{
        void clickMethod(Fee_Paid_Bean fee_paid_bean);
    }


    @NonNull
    @Override
    public PendingFeeListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.transaction_layout_new, parent, false);
        PendingFeeListAdapter.ViewHolder viewHolder= new PendingFeeListAdapter.ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull PendingFeeListAdapter.ViewHolder holder, @SuppressLint("RecyclerView") int position) {

        Fee_Paid_Bean data= list.get(position);

        if (data.getName() != null) {
            String title = getColoredSpanned(data.getName(), "#000000");
            String Name = getColoredSpanned("("+data.getClass_name()+")", "#5A5C59");
            holder.name.setText(Html.fromHtml(title + " " + Name));
        }else {
            holder.name.setText("Not Mentioned");
        }

        if (data.getS_id() != null) {
            String title = getColoredSpanned("" +"Roll.No: "+"", "#000000");
            String Name = getColoredSpanned(data.getS_id() , "#5A5C59");
            holder.orderid.setText(Html.fromHtml(title + " " + Name));
        }else {
            holder.orderid.setText("Roll.No:");
        }

        ParticularItemAdapter mAdapter = new ParticularItemAdapter(data.getProd_data(), context);
        holder.recycleFirst.setAdapter(mAdapter);

        if (data.getProd_data().get(0).getEs_duedate() != null) {
            String title = getColoredSpanned("<b>" +"Last Date:- "+"</b>", "#000000");
            String Name = getColoredSpanned(data.getProd_data().get(0).getEs_duedate() , "#f85164");
            holder.date.setText(Html.fromHtml(title + " " + Name));
        }else {
            holder.date.setText("Last Date:- Not mentioned");
        }

        //  holder.price.setText(String.valueOf(getTotalPrice()));
        int total = 0,item_price= 0,d_charge= 0,totalPrice=0,total_fee_fine=0;

        for (int i = 0; i < data.getProd_data().size(); i++) {
            totalPrice += Integer.parseInt(data.getProd_data().get(i).getFee_amount());
            total_fee_fine += Integer.parseInt(data.getProd_data().get(i).getEs_fine());
        }

       // item_price=Integer.parseInt(data.getGrand_total());
        d_charge=Integer.parseInt(data.getFee_bal());
        holder.delivery_price.setText(String.valueOf(d_charge) + ".00");
        total=totalPrice+total_fee_fine+d_charge;

        holder.a_sub.setText(String.valueOf(totalPrice) + "");
        holder.f_sub.setText(String.valueOf(total_fee_fine) + "");
        holder.totals.setText(String.valueOf(total) + ".00");

        holder.price.setText(String.valueOf(total) + ".00");
        //str_price = data.getTotal_price();


        if (data.getGender().equalsIgnoreCase("Male")) {


            if (data.getS_id() != null) {
                String title = getColoredSpanned("<b>" +"S/O "+"</b>", "#000000");
                String Name = getColoredSpanned("" +data.getFather_name()+"", "#5A5C59");
                holder.add.setText(Html.fromHtml(title + " " + Name));
            }else {
                holder.add.setText("Not Mentioned");
            }

            if (data.getProfile() != null) {
                Utils.progressImg(Constants.getImageBaseURL()+data.getProfile() ,holder.product_img,context);
                // Picasso.with(getActivity()).load(stuList.get(i-1).getProfile()).placeholder(R.drawable.stud).into(dp);
            } else {
                Picasso.get().load(R.drawable.stud).into(holder.product_img);
            }
        }else{

            if (data.getS_id() != null) {
                String title = getColoredSpanned("<b>" +"D/O "+"</b>", "#000000");
                String Name = getColoredSpanned("" +data.getFather_name()+"", "#5A5C59");
                holder.add.setText(Html.fromHtml(title + " " + Name));
            }else {
                holder.add.setText("Not Mentioned");
            }

            if (data.getProfile() != null) {
                Utils.progressImg(Constants.getImageBaseURL()+data.getProfile(),holder.product_img,context);
                //  Picasso.with(getActivity()).load(stuList.get(i-1).getProfile()).placeholder(R.drawable.f_student).into(dp);
            } else {
                Picasso.get().load(R.drawable.f_student).into(holder.product_img);
            }
        }



        holder.d_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.d_close.setVisibility(View.VISIBLE);
                holder.bottom_price.setVisibility(View.GONE);
                holder.d_add.setVisibility(View.GONE);
            }
        });

        holder.d_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.d_close.setVisibility(View.GONE);
                holder.bottom_price.setVisibility(View.VISIBLE);
                holder.d_add.setVisibility(View.VISIBLE);
            }
        });

        holder.layt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


            }
        });



    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }
    @Override
    public int getItemCount() {
        if (list != null) {
            return list.size();
        }
        return 0;

    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.recycleDeliFirst)
        RecyclerView recycleFirst;

        @BindView(R.id.product_img)
        ImageView product_img;

        @BindView(R.id.name)
        TextView name;

        @BindView(R.id.orderid)
        TextView orderid;

        @BindView(R.id.date)
        TextView date;

        @BindView(R.id.price)
        TextView price;

        @BindView(R.id.totals)
        TextView totals;

        @BindView(R.id.d_add)
        TextView d_add;

        @BindView(R.id.d_close)
        LinearLayout d_close;

        @BindView(R.id.track_order)
        LinearLayout track_order;

        @BindView(R.id.linearlayout)
        RelativeLayout linearlayout;

        @BindView(R.id.bottom_price)
        RelativeLayout bottom_price;

        @BindView(R.id.layt)
        RelativeLayout layt;

        @BindView(R.id.gift)
        TextView add;

        @BindView(R.id.status)
        TextView status;

        @BindView(R.id.a_sub)
        TextView a_sub;
        @BindView(R.id.f_sub)
        TextView f_sub;

        @BindView(R.id.balance)
        TextView delivery_price;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }

}
