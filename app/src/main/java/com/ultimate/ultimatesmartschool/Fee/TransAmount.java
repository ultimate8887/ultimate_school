package com.ultimate.ultimatesmartschool.Fee;

import com.ultimate.ultimatesmartschool.Utility.DebugLog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class TransAmount {

    private String id;
    private String trans_amount;

    public String getTrans_amount() {
        return trans_amount;
    }

    public void setTrans_amount(String trans_amount) {
        this.trans_amount = trans_amount;
    }

    public String getPlace_name() {
        return place_name;
    }

    public void setPlace_name(String place_name) {
        this.place_name = place_name;
    }

    private String place_name;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public static ArrayList<TransAmount> parseParticularBeanArray(JSONArray arrayObj) {
        ArrayList<TransAmount> list = new ArrayList<TransAmount>();
        try {

            for (int i = 0; i < arrayObj.length(); i++) {
                TransAmount p = parseParticularBeanObject(arrayObj.getJSONObject(i));
                if (p != null) {
                    list.add(p);
                }
            }
        } catch (Exception e) {
            DebugLog.printLog("exp",e.getMessage());
            e.printStackTrace();
        }
        return list;
    }

    public static TransAmount parseParticularBeanObject(JSONObject jsonObject) {
        TransAmount casteObj = new TransAmount();
        try {

            if (jsonObject.has("id") && !jsonObject.getString("id").isEmpty() && !jsonObject.getString("id").equalsIgnoreCase("null")) {
                casteObj.setId(jsonObject.getString("id"));
            }
            if (jsonObject.has("place_name") && !jsonObject.getString("place_name").isEmpty() && !jsonObject.getString("place_name").equalsIgnoreCase("null")) {
                casteObj.setPlace_name(jsonObject.getString("place_name"));
            }
            if (jsonObject.has("trans_amount") && !jsonObject.getString("trans_amount").isEmpty() && !jsonObject.getString("trans_amount").equalsIgnoreCase("null")) {
                casteObj.setTrans_amount(jsonObject.getString("trans_amount"));
            }



        } catch (JSONException e) {
            e.printStackTrace();
        }
        return casteObj;
    }
}
