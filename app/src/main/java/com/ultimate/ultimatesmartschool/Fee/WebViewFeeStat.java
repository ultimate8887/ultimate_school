package com.ultimate.ultimatesmartschool.Fee;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.Request;
import com.ultimate.ultimatesmartschool.AddmissionForm.SessionAdapter;
import com.ultimate.ultimatesmartschool.AddmissionForm.SessionBean;
import com.ultimate.ultimatesmartschool.BeanModule.User;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class WebViewFeeStat extends AppCompatActivity {
    @BindView(R.id.spinnerSession)
    Spinner spinnerSession;
    WebView mywebview;
    private SessionBean session;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view_fee_stat);
        ButterKnife.bind(this);
         mywebview = (WebView) findViewById(R.id.webView);
        mywebview.getSettings().setJavaScriptEnabled(true);
        txtTitle.setText("Staff Attendance");
        fetchcollegeacadmictear();
    }
    @OnClick(R.id.imgBack)
    public void onback() {
        finish();
    }
    private void fetchcollegeacadmictear() {
        ErpProgress.showProgressBar(this, "Please wait...");
        HashMap<String, String> params = new HashMap<String, String>();
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.COLGACADEMICYAER_URL, sessionapiCallback, this, params);

    }
    ApiHandler.ApiCallback sessionapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            if (error == null) {
                try {
                    final ArrayList<SessionBean> sessionlist = SessionBean.parseSessionArray(jsonObject.getJSONArray(Constants.SESSIONDATA));
                    SessionAdapter adapter = new SessionAdapter(WebViewFeeStat.this, sessionlist);
                    spinnerSession.setAdapter(adapter);
                    spinnerSession.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            session = sessionlist.get(i);
                            String my_url=Constants.Prod_Base_Url+Constants.feepaidns+Constants.s_key+ User.getCurrentUser().getSchoolData().getFi_school_id()+Constants.db_user+User.getCurrentUser().getSchoolData().getDbuser()+Constants.db_host+User.getCurrentUser().getSchoolData().getDbhost()+Constants.db_pass+User.getCurrentUser().getSchoolData().getDbpass()+Constants.db_name+User.getCurrentUser().getSchoolData().getDbname()+Constants.from_finance+session.getStart_date()+Constants.to_finance+session.getEnd_date();
                            Log.e("my_urlnew",my_url);
                            mywebview.loadUrl(my_url);
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {

            }
        }
    };

}