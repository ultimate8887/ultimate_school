package com.ultimate.ultimatesmartschool.Fee;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ultimate.ultimatesmartschool.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FeeDetailListAdapter extends RecyclerView.Adapter<FeeDetailListAdapter.Viewholder> {
    private final Context mContext;
    ArrayList<FeeDetailBean.FeeBean> list;

    public FeeDetailListAdapter(ArrayList<FeeDetailBean.FeeBean> list, Context mContext) {
        this.list = list;
        this.mContext = mContext;

    }

    @Override
    public FeeDetailListAdapter.Viewholder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.fee_part_amount, viewGroup, false);
        FeeDetailListAdapter.Viewholder viewholder = new FeeDetailListAdapter.Viewholder(view);
        return viewholder;
    }

    @Override
    public void onBindViewHolder(@NonNull FeeDetailListAdapter.Viewholder viewholder, int i) {

        viewholder.txtCategory.setText(list.get(i).getFee_cat_name());
        viewholder.txtParticular.setText(list.get(i).getFee_particular());
        viewholder.txtAmount.setText(list.get(i).getFee_amount());

        viewholder.txtFeetype.setText(list.get(i).getFee_type());

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void setFeeList(ArrayList<FeeDetailBean.FeeBean> list) {
        this.list = list;
    }

    public class Viewholder extends RecyclerView.ViewHolder {
        @BindView(R.id.txtCategory)
        TextView txtCategory;
        @BindView(R.id.txtParticular)
        TextView txtParticular;
        @BindView(R.id.txtAmount)
        TextView txtAmount;
        @BindView(R.id.txtfeetype)
        TextView txtFeetype;

        public Viewholder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}
