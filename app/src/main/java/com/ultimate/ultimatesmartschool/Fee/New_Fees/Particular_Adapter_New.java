package com.ultimate.ultimatesmartschool.Fee.New_Fees;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.ultimate.ultimatesmartschool.Fee.ParticularBean;
import com.ultimate.ultimatesmartschool.Fee.Particular_Adapter;
import com.ultimate.ultimatesmartschool.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Particular_Adapter_New  extends RecyclerView.Adapter<Particular_Adapter_New.Viewholder> {
    private final Context mContext;
    String month="";
    ArrayList<ParticularBean> list;

    public Particular_Adapter_New(ArrayList<ParticularBean> list, Context mContext,String month) {
        this.list = list;
        this.mContext = mContext;
        this.month = month;
    }

    @Override
    public Particular_Adapter_New.Viewholder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.pay_fee_adapter_new, viewGroup, false);
        Particular_Adapter_New.Viewholder viewholder = new Particular_Adapter_New.Viewholder(view);
        return viewholder;
    }

    @Override
    public void onBindViewHolder(final Particular_Adapter_New.Viewholder viewholder, int i) {

        if (list.get(i).getFee_particular() !=null) {
            viewholder.p.setText(list.get(i).getFee_particular());
        }
        if (list.get(i).getFee_amount() !=null) {
            viewholder.a.setText(list.get(i).getFee_amount());
        }

        if (list.get(i).getEs_fine() !=null) {
            viewholder.f.setText(list.get(i).getEs_fine());
        }

        if (!month.equalsIgnoreCase("")) {
            viewholder.m.setText("'"+month+"'");
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void setFeeList(ArrayList<ParticularBean> list) {
        this.list = list;
    }

    public static class Viewholder extends RecyclerView.ViewHolder {
        @BindView(R.id.p)
        TextView p;
        @BindView(R.id.a)
        TextView a;
        @BindView(R.id.m)
        TextView m;
        @BindView(R.id.f)
        TextView f;
        public Viewholder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}
