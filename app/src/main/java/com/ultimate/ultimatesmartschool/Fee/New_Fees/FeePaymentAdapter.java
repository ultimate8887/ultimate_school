package com.ultimate.ultimatesmartschool.Fee.New_Fees;

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;
import com.ultimate.ultimatesmartschool.BeanModule.User;
import com.ultimate.ultimatesmartschool.Fee.Fee_Paid_Bean;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FeePaymentAdapter extends RecyclerView.Adapter<FeePaymentAdapter.ViewHolder> {

    Context context;
    ArrayList<Fee_Paid_Bean> list;
    private FeePaymentAdapter.ProdMethodCallBack prodMethodCallBack;
    private Animation animation;
    private int view_value;

    public FeePaymentAdapter(ArrayList<Fee_Paid_Bean> list, Context context, FeePaymentAdapter.ProdMethodCallBack prodMethodCallBack, int view_value) {
        this.list=list;
        this.context=context;
        this.prodMethodCallBack=prodMethodCallBack;
        this.view_value=view_value;
    }

    public void setMerchantBeans(ArrayList<Fee_Paid_Bean> list) {
        this.list = list;
    }

    public interface ProdMethodCallBack{
        void clickMethod(Fee_Paid_Bean fee_paid_bean);
    }


    @NonNull
    @Override
    public FeePaymentAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.payment_fee_adapter, parent, false);
        FeePaymentAdapter.ViewHolder viewHolder= new FeePaymentAdapter.ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull FeePaymentAdapter.ViewHolder holder, @SuppressLint("RecyclerView") int position) {
        Animation animation;
        animation = AnimationUtils.loadAnimation(context, R.anim.btn_blink_animation);
        Fee_Paid_Bean data= list.get(position);

        if (data.getEs_particularamount() !=null) {
            holder.amount.setText(data.getEs_particularamount()+".00/-");
        }
        if (data.getPaymentdate() !=null) {
            holder.date.setText(Utils.getDateFormated(data.getPaymentdate()));
        }
        if (data.getS_id() !=null) {
            holder.admission.setText(data.getS_id());
        }

        if (data.getPayment_mode() !=null) {


          //  String title = getColoredSpanned("" +dateString+"", "#383737");
            String Name ="",title ="";
            if (data.getPayment_mode().equalsIgnoreCase("online")){
                title = getColoredSpanned("" +data.getPayment_mode()+"", "#1C8B3B");
                Name = getColoredSpanned("" +" Payment"+"", "#1C8B3B");
            }else{
                title = getColoredSpanned("" +data.getPayment_mode()+"", "#f85164");
                Name = getColoredSpanned("" +" Payment"+"", "#f85164");
            }

            holder.mode.setText(Html.fromHtml(title + " " + Name));


           // holder.mode.setText(data.getPayment_mode()+"Payment");
        }

        if (data.getId() !=null) {


            String Name ="",title ="";
            title = getColoredSpanned("" +User.getCurrentUser().getSchoolData().getFi_school_id()+"", "#cc7722");
            Name = getColoredSpanned("-" +data.getId()+"", "#383737");
            holder.receipt.setText(Html.fromHtml(title + " " + Name));


          //  holder.receipt.setText(User.getCurrentUser().getSchoolData().getFi_school_id() +"-"+data.getId());
        }

        holder.view_action.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.view_action.startAnimation(animation);
                prodMethodCallBack.clickMethod(list.get(position));
            }
        });



    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }
    @Override
    public int getItemCount() {
        if (list != null) {
            return list.size();
        }
        return 0;

    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.p)
        TextView admission;
        @BindView(R.id.a)
        TextView amount;
        @BindView(R.id.m)
        TextView receipt;
        @BindView(R.id.f)
        TextView view_action;
        @BindView(R.id.date)
        TextView date;
        @BindView(R.id.mode)
        TextView mode;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}
