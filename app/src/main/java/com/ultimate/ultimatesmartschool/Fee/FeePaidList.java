package com.ultimate.ultimatesmartschool.Fee;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.Request;
import com.ultimate.ultimatesmartschool.AddmissionForm.SessionAdapter;
import com.ultimate.ultimatesmartschool.AddmissionForm.SessionBean;
import com.ultimate.ultimatesmartschool.BeanModule.ClassBean;
import com.ultimate.ultimatesmartschool.BeanModule.Studentbean;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.StudentAttendance.ClassAdapterAttend;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class FeePaidList extends AppCompatActivity {
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.spinnerFinancialYear)
    Spinner spinnerFinancialYear;
    @BindView(R.id.parent)
    RelativeLayout parent;
    @BindView(R.id.recyclerview)
    RecyclerView recyclerView;
    private LinearLayoutManager layoutManager;
    ArrayList<FeePaidBeanGDS> list;
    private FeePaidAdapterGDS adapter;
    ArrayList<ClassBean> classList = new ArrayList<>();
    private ArrayList<Studentbean> stuList = new ArrayList<>();
    //    @BindView(R.id.txtTotal)
//    TextView txtTotal;
    @BindView(R.id.spinnerStudent)
    Spinner spinnerStudent;
    @BindView(R.id.spinnerClass)
    Spinner spinnerClass;
    @BindView(R.id.listLyt)
    LinearLayout listLyt;
    @BindView(R.id.textNorecord)TextView textNorecord;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fee_paid_list);
        list = new ArrayList<>();
        ButterKnife.bind(this);
        txtTitle.setText("Fee Paid List");
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new FeePaidAdapterGDS(list, this);
        recyclerView.setAdapter(adapter);
        fetchSession();
        fetchClass();
    }

    private void fetchClass() {
        HashMap<String, String> params = new HashMap<String, String>();
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.CLASSLIST_URL, classapiCallback, this, params);
    }

    ApiHandler.ApiCallback classapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            if (error == null) {
                try {
                    classList = ClassBean.parseClassArray(jsonObject.getJSONArray(Constants.CLASSDATA));
                    ClassAdapterAttend adapter = new ClassAdapterAttend(FeePaidList.this, classList,0);
                    spinnerClass.setAdapter(adapter);
                    spinnerClass.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if (i > 0) {
                                String classid = classList.get(i - 1).getId();
                                fetchStudent(classid);
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Utils.showSnackBar(error.getMessage(), parent);
            }
        }
    };

    private void fetchStudent(String classid) {
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("class_id", classid);
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.studentlistclass, stapiCallback, this, params);
    }

    ApiHandler.ApiCallback stapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            if (error == null) {
                try {
                    stuList = Studentbean.parseHWArray(jsonObject.getJSONArray("student_list"));
                    StudentAdapter adapter = new StudentAdapter(FeePaidList.this, stuList);
                    spinnerStudent.setAdapter(adapter);
                    spinnerStudent.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if (i > 0) {
                                String studentid = stuList.get(i - 1).getId();
                                fetchFeeDetail(session, studentid);
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Utils.showSnackBar(error.getMessage(), parent);
            }
        }
    };

    private void fetchSession() {
        ErpProgress.showProgressBar(this, "Please wait..");
        HashMap<String, String> params = new HashMap<String, String>();
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.SESSION_URL, sessionapiCallback, this, params);
    }

    private SessionBean session;
    ApiHandler.ApiCallback sessionapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            if (error == null) {
                try {
                    final ArrayList<SessionBean> sessionlist = SessionBean.parseSessionArray(jsonObject.getJSONArray(Constants.SESSIONDATA));
                    SessionAdapter adapter = new SessionAdapter(FeePaidList.this, sessionlist);
                    spinnerFinancialYear.setAdapter(adapter);
                    spinnerFinancialYear.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            session = sessionlist.get(i);
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Utils.showSnackBar(error.getMessage(), parent);
            }
        }
    };

    private void fetchFeeDetail(SessionBean session, String id) {
        ErpProgress.showProgressBar(this, "Please wait..");
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("todate", session.getEnd_date());
        params.put("fromdate", session.getStart_date());
        params.put("student_id", id);
        String url = Constants.getBaseURL() + Constants.FEE_PAID_BY_STUDENTGDS;
        ApiHandler.apiHit(Request.Method.POST, url, apiCallback, this, params);
    }

    ApiHandler.ApiCallback apiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            if (error == null) {
                try {
                    if (list != null) {
                        list.clear();
                    }
                    JSONArray jsonArray = jsonObject.getJSONArray("hostelroom_data");
                    list = FeePaidBeanGDS.parseHOSTLRoomArray(jsonArray);
                    if (list.size() > 0) {
                        adapter.setroomList(list);
                        adapter.notifyDataSetChanged();
                        textNorecord.setVisibility(View.GONE);
                    } else {
                        adapter.setroomList(list);
                        adapter.notifyDataSetChanged();
                        textNorecord.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                textNorecord.setVisibility(View.VISIBLE);
                list.clear();
                adapter.setroomList(list);

                adapter.notifyDataSetChanged();
            }
        }
    };

    @OnClick(R.id.imgBack)
    public void backClick() {
        finish();
    }
}
