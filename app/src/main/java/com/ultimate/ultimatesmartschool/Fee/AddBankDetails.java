package com.ultimate.ultimatesmartschool.Fee;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetSequence;
import com.squareup.picasso.Picasso;
import com.ultimate.ultimatesmartschool.Activity.SchoolInfoActivity;
import com.ultimate.ultimatesmartschool.BeanModule.User;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.CommonProgress;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AddBankDetails extends AppCompatActivity {
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.ano)
    EditText ano;
    @BindView(R.id.aname)
    EditText aname;
    @BindView(R.id.bname)
    EditText bname;
    @BindView(R.id.visitaddress)
    EditText address;
    @BindView(R.id.upi)
    EditText upi;
    Animation animation;
    @BindView(R.id.code)
    EditText ifsc;
    @BindView(R.id.f_note)
    EditText f_note;
    @BindView(R.id.dialog)
    ImageView dialog;
    SharedPreferences sharedPreferences;
    CommonProgress commonProgress;
    String bank_n="",acc_n="",acc_no="",ifsc_code="",bank_add="",upi_name="",fees_note="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_bank_details);
        ButterKnife.bind(this);
        commonProgress=new CommonProgress(this);
        animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.btn_blink_animation);
        txtTitle.setText("Update Bank Details");
        if (!restorePrefData()){
            setShowcaseView();
        }
        userprofile();
    }



    private void userprofile() {
        // UltimateProgress.showProgressBar(getActivity(),"Please Wait........!");
        commonProgress.show();
        HashMap<String, String> params = new HashMap<>();
        params.put("user_id", User.getCurrentUser().getId());
        params.put("check", "fetch");

        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.UPDATE_SCHOOL_INFO, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error ) {
                commonProgress.dismiss();
                ErpProgress.cancelProgressBar();
                if (error == null) {
                    try {

                        bank_n=jsonObject.getJSONObject(Constants.USERDATA).getString("bank_name");
                        acc_n=jsonObject.getJSONObject(Constants.USERDATA).getString("acc_name");
                        acc_no=jsonObject.getJSONObject(Constants.USERDATA).getString("acc_no");
                        ifsc_code=jsonObject.getJSONObject(Constants.USERDATA).getString("ifsc_code");
                        bank_add=jsonObject.getJSONObject(Constants.USERDATA).getString("bank_address");
                        upi_name=jsonObject.getJSONObject(Constants.USERDATA).getString("upi");
                        fees_note=jsonObject.getJSONObject(Constants.USERDATA).getString("fees_note");

                            bname.setText(bank_n);
                            aname.setText(acc_n);
                            ano.setText(acc_no);
                            address.setText(bank_add);
                            ifsc.setText(ifsc_code);
                            upi.setText(upi_name);
                            f_note.setText(fees_note);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }, this, params);
    }

    private void setShowcaseView() {
        new TapTargetSequence(this)
                .targets(TapTarget.forView(dialog,"Add Online Fees QR Image!","Tap the QR button to add online fees QR image.")
                        .outerCircleColor(R.color.light)
                        .outerCircleAlpha(0.96f)
                        .targetCircleColor(R.color.white)
                        .titleTextSize(20)
                        .titleTextColor(R.color.white)
                        .descriptionTextSize(14)
                        .descriptionTextColor(R.color.black)
                        .textColor(R.color.black)
                        .textTypeface(Typeface.SANS_SERIF)
                        .dimColor(R.color.black)
                        .drawShadow(true)
                        .cancelable(false)
                        .tintTarget(true)
                        .transparentTarget(true)
                        .targetRadius(60)).listener(new TapTargetSequence.Listener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onSequenceFinish() {
                // Toast.makeText(getApplicationContext(),"Sequence Finished",Toast.LENGTH_SHORT).show();
                savePrefData();
                commonIntent();
            }

            @Override
            public void onSequenceStep(TapTarget lastTarget, boolean targetClicked) {
                // Toast.makeText(getActivity(),"GREAT!",Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSequenceCanceled(TapTarget lastTarget) {

            }
        }).start();
    }

    private void commonIntent() {
        dialog.startAnimation(animation);

        Intent intent = new Intent(AddBankDetails.this, AddFeesQRActivity.class);
        intent.putExtra("key", "qr");
        startActivity(intent);

        // startActivity(new Intent(FeeDetail.this,AddFeesQRActivity.class));
    }

    private void savePrefData(){
        sharedPreferences=this.getSharedPreferences("boarding_pref_add_qrrrrrrr",MODE_PRIVATE);
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putBoolean("IsFirstTimeVisit_add_qrrrrrrr",true);
        editor.apply();
    }

    private boolean restorePrefData(){
        sharedPreferences = this.getSharedPreferences("boarding_pref_add_qrrrrrrr",MODE_PRIVATE);
        return sharedPreferences.getBoolean("IsFirstTimeVisit_add_qrrrrrrr",false);
    }

    @OnClick(R.id.submit)
    public void submit() {

     if (bname.getText().toString().isEmpty() || ifsc.getText().toString().isEmpty()
                || ano.getText().toString().isEmpty() || aname.getText().toString().isEmpty() ) {
        Toast.makeText(AddBankDetails.this, "Kindly fill complete Bank details", Toast.LENGTH_LONG).show();
    } else {
        HashMap<String, String> params = new HashMap<String, String>();
        commonProgress.show();
        params.put("bank_name", bname.getText().toString());
        params.put("acc_name", aname.getText().toString());
        params.put("acc_no",ano.getText().toString() );
        params.put("bank_address",address.getText().toString() );
        params.put("ifsc_code",ifsc.getText().toString() );
         params.put("upi",upi.getText().toString() );
        params.put("check","qr" );
         params.put("fees_note",f_note.getText().toString() );

        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.ADDBANNER_URL, checkinclassapiCallback, this, params);
    }
}
    ApiHandler.ApiCallback checkinclassapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            commonProgress.dismiss();
            if (error == null) {
                Toast.makeText(AddBankDetails.this,"Update Successfully!",Toast.LENGTH_LONG).show();
                finish();
            }
            else {
                Toast.makeText(AddBankDetails.this, error.getMessage(), Toast.LENGTH_SHORT).show();

            }
        }
    };

    @OnClick(R.id.dialog)
    public void dialog() {
        commonIntent();
    }

    @OnClick(R.id.imgBack)
    public void backClick() {
        finish();
    }

}