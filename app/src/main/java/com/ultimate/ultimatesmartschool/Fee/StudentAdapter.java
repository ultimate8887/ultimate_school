package com.ultimate.ultimatesmartschool.Fee;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.ultimate.ultimatesmartschool.BeanModule.Studentbean;
import com.ultimate.ultimatesmartschool.BeanModule.User;
import com.ultimate.ultimatesmartschool.R;

import java.util.ArrayList;

public class StudentAdapter extends BaseAdapter {
    private final ArrayList<Studentbean> casteList;
    Context context;
    LayoutInflater inflter;

    public StudentAdapter(Context applicationContext, ArrayList<Studentbean> casteList) {
        this.context = applicationContext;
        this.casteList = casteList;
        inflter = (LayoutInflater.from(applicationContext));
    }

    @Override
    public int getCount() {
        return casteList.size() + 1;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        view = inflter.inflate(R.layout.spinner_lyt, null);
        TextView label = (TextView) view.findViewById(R.id.txtText);
        if (i == 0) {
            // Default selected Spinner item
            label.setText("Student");
        } else {
            Studentbean classObj = casteList.get(i - 1);
            if(User.getCurrentUser().getSchoolData().getName().equalsIgnoreCase("Guru Nanak Dev Dental College and Research Institute")) {
                label.setText("(" + classObj.getCroll_no() + ")" + classObj.getName());
            }else{
                label.setText("(" + classObj.getId() + ")" + classObj.getName());
            }
        }

        return view;
    }
}
