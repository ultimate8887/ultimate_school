package com.ultimate.ultimatesmartschool.Fee.New_Fees;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import java.util.ArrayList;

public class YearOptionAdapter extends BaseAdapter {
    LayoutInflater inflter;
    Context context;
    private ArrayList<YearOptionBean> vehicleList;

    public YearOptionAdapter(Context context, ArrayList<YearOptionBean> vehicleList) {
        this.context = context;
        this.vehicleList = vehicleList;
        inflter = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return vehicleList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup parent) {
        view = inflter.inflate(R.layout.spinner_lyt, null);
        TextView label = (TextView) view.findViewById(R.id.txtText);
            YearOptionBean classObj = vehicleList.get(i);
          //  label.setText(classObj.getMonth()+" ("+classObj.getYear()+")");

        String title = getColoredSpanned(classObj.getMonth(), "#000000");
        String Name = getColoredSpanned(" ("+classObj.getYear()+")", "#f4685b");
        label.setText(Html.fromHtml(title + " " + Name));

        return view;
    }
    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }
}
