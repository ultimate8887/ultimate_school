package com.ultimate.ultimatesmartschool.Fee;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.google.zxing.Result;
import com.squareup.picasso.Picasso;
import com.ultimate.ultimatesmartschool.AddmissionForm.SessionAdapter;
import com.ultimate.ultimatesmartschool.AddmissionForm.SessionBean;
import com.ultimate.ultimatesmartschool.BeanModule.Studentbean;
import com.ultimate.ultimatesmartschool.Fee.New_Fees.CommonPendingFeesListActivity;
import com.ultimate.ultimatesmartschool.Fee.New_Fees.CommonSTDPendingFeesListActivity;
import com.ultimate.ultimatesmartschool.Fee.New_Fees.PayFee_New;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Student.NewsAdapter;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
//import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class QRScanner extends AppCompatActivity {
    String edtFather= "",className="";
    String edtName= "";
    String edtRegistration = "",fromdate = "",std_dp = "",todate= "",gender="",regist="",fee_cate_id="",place_id="",route_id="",selectMonth="";
    private ArrayList<SessionBean> sessionlist;
    private SessionBean session;
    @BindView(R.id.spinnerFinancialYear)
    Spinner spinnerFinancialYear;
    @BindView(R.id.lightButton)
    ImageView flashImageView;
    @BindView(R.id.parent)
    LinearLayout parent;
    //@BindView(R.id.imgBack)ImageView imgBack;
    public String date,classid,sectionid;
    //Variables
    Intent i;
    String tag="";
    // HistoryORM h = new HistoryORM();
//    private ZXingScannerView mScannerView;
    private boolean flashState = false;

    TextView name,fname,text ;
    Dialog dialog;
    String value="";
    CircularImageView img;

    private NewsAdapter adapter;

    ArrayList<Studentbean> mData = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_q_r_scanner2);
        ButterKnife.bind(this);
//        fetchSession();
//        if (getIntent().getExtras()!=null){
//            tag = getIntent().getExtras().getString("tag");
//        }
//
//        ActivityCompat.requestPermissions(QRScanner.this,
//                new String[]{Manifest.permission.CAMERA},
//                1);
//
//        ViewGroup contentFrame = (ViewGroup) findViewById(R.id.content_frame);
//        mScannerView = new ZXingScannerView(QRScanner.this);
//        contentFrame.addView(mScannerView);
//
//        java.util.Calendar calendar = java.util.Calendar.getInstance(TimeZone.getDefault());
//        Date setdate = calendar.getTime();
//        SimpleDateFormat fmtOut = new SimpleDateFormat("dd MMM, yyyy");
//        String dateString = fmtOut.format(setdate);
//        //adddate.setText(dateString);
//        SimpleDateFormat dateFrmOut = new SimpleDateFormat("yyyy-MM-dd");
//        date = dateFrmOut.format(setdate);
//
//        flashImageView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                mScannerView.setFlash(true);
//                if(flashState==false) {
//                    v.setBackgroundResource(R.drawable.ic_flash_off);
//                    Toast.makeText(getApplicationContext(), "Flashlight turned on", Toast.LENGTH_SHORT).show();
//                    mScannerView.setFlash(true);
//                    flashState = true;
//                }else if(flashState) {
//                    v.setBackgroundResource(R.drawable.ic_flash_on);
//                    Toast.makeText(getApplicationContext(), "Flashlight turned off", Toast.LENGTH_SHORT).show();
//                    mScannerView.setFlash(false);
//                    flashState = false;
//                }
//            }
//        });
    }

//    private void fetchSession() {
//        HashMap<String, String> params = new HashMap<String, String>();
//        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.SESSION_URL, sessionapiCallback, this, params);
//    }
//
//
//    ApiHandler.ApiCallback sessionapiCallback = new ApiHandler.ApiCallback() {
//        @Override
//        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
//            if (error == null) {
//                try {
//                    if (sessionlist != null) {
//                        sessionlist.clear();
//                    }
//                    sessionlist = SessionBean.parseSessionArray(jsonObject.getJSONArray(Constants.SESSIONDATA));
//                    SessionAdapter adapter = new SessionAdapter(QRScanner.this, sessionlist);
//                    spinnerFinancialYear.setAdapter(adapter);
//                    spinnerFinancialYear.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//                        @Override
//                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
//                            session = sessionlist.get(i);
//                        }
//
//                        @Override
//                        public void onNothingSelected(AdapterView<?> adapterView) {
//
//                        }
//                    });
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            } else {
//                //   Utils.showSnackBar(error.getMessage(), parent);
//            }
//        }
//    };
//    @Override
//    public void handleResult(Result rawResult) {
//        searchStudent(rawResult.getText());
//        mScannerView.stopCamera();
//    }
//
//    @Override
//    public void onResume() {
//        super.onResume();
//        mScannerView.setResultHandler(this);
//        mScannerView.startCamera();
//    }
//
//    @Override
//    public void onPause() {
//        super.onPause();
//        mScannerView.stopCamera();
//    }
//
//
//    public void searchStudent(String student_id) {
//        ErpProgress.showProgressBar(this, "Please wait...");
//        HashMap<String, String> params = new HashMap<String, String>();
//        params.put("stu_id",student_id);
//        if (date != null) {
//            params.put("mdate", String.valueOf(date));
//        }
//        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.STUDENTBYSCANNING_URLNEW, studentapiCallback, this, params);
//
//    }
//
//    ApiHandler.ApiCallback studentapiCallback = new ApiHandler.ApiCallback() {
//        @Override
//        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
//
//            if (error == null) {
//                ErpProgress.cancelProgressBar();
//                try {
//                    if (mData != null) {
//                        mData.clear();
//                    }
//
//                    if (mData != null){
//                        mData.clear();
//                        mData=Studentbean.parseHWArray(jsonObject.getJSONArray("student_list"));
////                        name.setText(mData.get(0).getName()+"("+mData.get(0).getClass_name()+")"+mData.get(0).getSection_name());
////                        fname.setText(mData.get(0).getFather_name());
//
//                        openDialoggg();
//                        fee_cate_id=mData.get(0).getFee_cat_id();
//                        regist=mData.get(0).getId();
//                        gender=mData.get(0).getGender();
//                        std_dp=mData.get(0).getProfile();
//                        classid=mData.get(0).getClass_id();
//                        className=mData.get(0).getClass_name();
//
////                        classid=mData.get(0).getClass_id();
////                        className=mData.get(0).getClass_name();
//
//                        place_id=mData.get(0).getPlace_id();
//                        route_id=mData.get(0).getRoute_id();
//                        edtFather=mData.get(0).getFather_name();
//                        edtName=mData.get(0).getName();
//                        edtRegistration=mData.get(0).getClass_name()+"("+mData.get(0).getId()+")";
//
//                        text.setText(mData.get(0).getId());
//                        fname.setText(mData.get(0).getClass_name());
//
//                        if (mData.get(0).getGender().equalsIgnoreCase("male")){
//                            name.setText(mData.get(0).getName()+"\nS/O\n"+mData.get(0).getFather_name());
//                            if (mData.get(0).getProfile() != null) {
//                                Picasso.get().load(mData.get(0).getProfile()).placeholder(R.drawable.stud).into(img);
//                            } else {
//                                Picasso.get().load(R.drawable.stud).into(img);
//                            }
//                        }else {
//                            name.setText(mData.get(0).getName()+"\nD/O\n"+mData.get(0).getFather_name());
//                            if (mData.get(0).getProfile() != null) {
//                                Picasso.get().load(mData.get(0).getProfile()).placeholder(R.drawable.f_student).into(img);
//                            } else {
//                                Picasso.get().load(R.drawable.f_student).into(img);
//                            }
//                        }
//                       // Picasso.with(getApplicationContext()).load(mData.get(0).getProfile()).placeholder(getResources().getDrawable(R.drawable.stud)).into(img);
////                        adapter.setHQList(mData);
////                        adapter.notifyDataSetChanged();
//
//                    }
//
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            } else {
//
//                mData.clear();
////                adapter.setHQList(mData);
////                adapter.notifyDataSetChanged();
//            }
//        }
//    };
//
//    private void openDialoggg() {
//        dialog = new Dialog(this);
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialog.setCancelable(true);
//        dialog.setContentView(R.layout.new_dialogqrscan);
//        View v = dialog.getWindow().getDecorView();
//        v.setBackgroundResource(android.R.color.transparent);
//        text = (TextView) dialog.findViewById(R.id.txtRegNo);
//        // text.setText(rawResult.getText());
//        img = (CircularImageView) dialog.findViewById(R.id.img);
//        // img.setImageResource(R.drawable.ic_done_gr);
//        name = (TextView) dialog.findViewById(R.id.nametxtone);
//        fname = (TextView) dialog.findViewById(R.id.classtxtone);
//
//        Button webSearch = (Button) dialog.findViewById(R.id.searchButton);
//
//        webSearch.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                mScannerView.resumeCameraPreview(QRScanner.this);
//                       Intent intent;
//                      if (tag.equalsIgnoreCase("stu_wise")) {
//                         intent = new Intent(QRScanner.this, CommonFeesListActivity.class);
//                         intent.putExtra("tag", "student_wise");
//                      } else if (tag.equalsIgnoreCase("pending_stu_wise")){
//                          intent = new Intent(QRScanner.this, CommonSTDPendingFeesListActivity.class);
//                          intent.putExtra("tag", "student_wise");
//                      } else {
//                            intent = new Intent(QRScanner.this, PayFee_New.class);
//                        }
//                        intent.putExtra("name", edtName);
//                        intent.putExtra("f_name", edtFather);
//                        intent.putExtra("roll", edtRegistration);
//                        intent.putExtra("dp", std_dp);
//                        intent.putExtra("regist", regist);
//                        intent.putExtra("class", classid);
//                        intent.putExtra("class_name", className);
//                        intent.putExtra("gender", gender);
//                        intent.putExtra("fee_cate_id", fee_cate_id);
//                        intent.putExtra("route_id", route_id);
//                        intent.putExtra("place_id", place_id);
//                        intent.putExtra("fromdate", session.getStart_date());
//                        intent.putExtra("todate", session.getEnd_date());
//                        startActivity(intent);
//                         dialog.dismiss();
//                         finish();
//                    }
//
//
//        });
//
//
//        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
//            @Override
//            public void onCancel(DialogInterface dialog) {
//                mScannerView.resumeCameraPreview(QRScanner.this);
//            }
//        });
//        dialog.show();
//    }
}