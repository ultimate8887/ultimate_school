package com.ultimate.ultimatesmartschool.Fee.Fees_Fragments;

import android.app.Dialog;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.icu.text.SimpleDateFormat;
import android.icu.util.Calendar;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.squareup.picasso.Picasso;
import com.ultimate.ultimatesmartschool.BeanModule.User;
import com.ultimate.ultimatesmartschool.Fee.FeeListAdapter;
import com.ultimate.ultimatesmartschool.Fee.Fee_Paid_Bean;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.CommonProgress;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;
import com.ultimate.ultimatesmartschool.Utility.PaginationListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Complete_Fees extends Fragment implements FeeListAdapter.ProdMethodCallBack {

    Dialog mBottomSheetDialog;
    @BindView(R.id.recyclerview)
    RecyclerView recyclerView;

    private LinearLayoutManager layoutManager;
    ArrayList<Fee_Paid_Bean> list;
    private FeeListAdapter mAdapter;
    Animation animation;
    @BindView(R.id.textNorecord)
    TextView txtNorecord;
    @BindView(R.id.totalRecord)
    TextView totalRecord;
    @BindView(R.id.spinnerMonth)
    Spinner spinnerMonth;
    ArrayList<String> monthList = new ArrayList<>();
    CommonProgress commonProgress;

    @BindView(R.id.main_progress)
    ProgressBar main_progress;
    int  limit=100,page_limit=0,total_pages=0;
    private static final int PAGE_START = 1;
    private int currentPage = PAGE_START;
    private boolean isLastPage = false;
    private boolean isLoading = false;

    String regist = "", classid = "", fromdate = "", todate = "", tag = "",selectMonth = "",month_name="";
    Context context;

    public Complete_Fees(String tag, String regist, String classid, String fromdate, String todate, Context context) {
        this.context = context;
        this.tag=tag;
        this.regist=regist;
        this.classid=classid;
        this.fromdate=fromdate;
        this.todate=todate;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_common__fees, container, false);
        ButterKnife.bind(this,view);
        commonProgress=new CommonProgress(getActivity());
        list = new ArrayList<>();
        animation = AnimationUtils.loadAnimation(getActivity(), R.anim.btn_blink_animation);
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        mAdapter = new FeeListAdapter(list, getActivity(), this,0);
        recyclerView.setAdapter(mAdapter);
        fetchmonthList();


        recyclerView.addOnScrollListener(new PaginationListener(layoutManager) {
            @Override
            protected void loadMoreItems() {
                // isLoading = true;
                //  Utils.showSnackBar("No more messages found in message list.", parent);
                //  currentPage++;
                // doApiCall();

                if (total_pages>=page_limit) {
                    main_progress.setVisibility(View.VISIBLE);
                    currentPage += 10;
                    page_limit = currentPage + limit;
                    fetchFeeDetail(0,selectMonth,page_limit, "no");
                }else{
                    // main_progress.setVisibility(View.GONE);
                    //  Utils.showSnackBar("No more messages found in message list.", parent);
                }

            }
            @Override
            public boolean isLastPage() {
                return isLastPage;
            }
            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });

        return view;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void fetchmonthList() {

        monthList.add("April");
        monthList.add("May");
        monthList.add("June");
        monthList.add("July");
        monthList.add("August");
        monthList.add("September");
        monthList.add("October");
        monthList.add("November");
        monthList.add("December");
        monthList.add("January");
        monthList.add("February");
        monthList.add("March");

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_item, monthList);
        //set the view for the Drop down list
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //set the ArrayAdapter to the spinner
        spinnerMonth.setAdapter(dataAdapter);

        Calendar cal=Calendar.getInstance();
        SimpleDateFormat month_date = new SimpleDateFormat("MMMM");
        month_name = month_date.format(cal.getTime());

        // setting here spinner item selected with position..
        for(int i=0;i<monthList.size();i++) {
            if (monthList.get(i).equalsIgnoreCase(month_name)) {
                selectMonth = monthList.get(i);
                spinnerMonth.setSelection(i);

            }
        }

        //attach the listener to the spinner
        spinnerMonth.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long l) {
                selectMonth = monthList.get(pos);
//                if (tag.equalsIgnoreCase("student_wise")) {
//                     fetchFeesDetails(selectMonth);
//                }else){
//                     fetchFeesDetails(selectMonth);
//                }
                fetchFeeDetail(0,selectMonth,limit, "yes");

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }


    private void fetchFeeDetail(int i, String selectMonth, int limit, String progress) {
        // Toast.makeText(getActivity(),tag,Toast.LENGTH_SHORT).show();
        if (progress.equalsIgnoreCase("yes")){
            commonProgress.show();
        }

        HashMap<String, String> params = new HashMap<String, String>();
        params.put("student_id", regist);
        if (tag.equalsIgnoreCase("stu_wise_rec")) {
            params.put("tag", "student_wise");
        }else if (tag.equalsIgnoreCase("class_wise_rec")) {
        params.put("tag", "class_wise");
         }else {
        params.put("tag", "all");
         }
        params.put("page_limit",String.valueOf(limit));
        params.put("month", selectMonth);
        params.put("check", "receipt");
        params.put("status", "complete");
        params.put("classid", classid);
        params.put("todate", todate);
        params.put("fromdate", fromdate);
        String url = Constants.getBaseURL() + Constants.FEE_PAID_STUDENT;
        ApiHandler.apiHit(Request.Method.POST, url, apiCallback, getActivity(), params);
    }

    ApiHandler.ApiCallback apiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            commonProgress.dismiss();
            if (error == null) {
                if (list != null)
                    list.clear();

                JSONArray jsonArray = null;
                try {
                    jsonArray = jsonObject.getJSONArray("fee_data");
                    list = Fee_Paid_Bean.parsFee_Paid_BeanArray(jsonArray);
                    total_pages= Integer.parseInt(list.get(0).getRowcount());
                    if (list.size() > 0) {
                        mAdapter.setMerchantBeans(list);
                        //setanimation on adapter...
                        recyclerView.getAdapter().notifyDataSetChanged();
                        recyclerView.scheduleLayoutAnimation();
                        totalRecord.setVisibility(View.VISIBLE);
                        totalRecord.setText("Total Entries:- "+String.valueOf(list.size()));
                        //-----------end------------
                        txtNorecord.setVisibility(View.GONE);
                    } else {
                        totalRecord.setText("Total Entries:- 0");
                        mAdapter.setMerchantBeans(list);
                        mAdapter.notifyDataSetChanged();
                        txtNorecord.setVisibility(View.VISIBLE);
                    }
                    main_progress.setVisibility(View.GONE);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            } else {
                totalRecord.setText("Total Entries:- 0");
                txtNorecord.setVisibility(View.VISIBLE);
                list.clear();
                mAdapter.setMerchantBeans(list);
                mAdapter.notifyDataSetChanged();
            //    Toast.makeText(getActivity(),error.getMessage(),Toast.LENGTH_SHORT).show();
            }
        }
    };

    @Override
    public void clickMethod(Fee_Paid_Bean fee_paid_bean) {
//        Gson gson = new Gson();
//        String prod_data = gson.toJson(fee_paid_bean, Fee_Paid_Bean.class);
//        Intent intent = new Intent(getActivity(), FeeDetailsActivity.class);
//        intent.putExtra("fee_data", prod_data);
//        startActivity(intent);

        mBottomSheetDialog = new Dialog(getActivity());
        final View sheetView = getLayoutInflater().inflate(R.layout.image_lyt, null);
        mBottomSheetDialog.setContentView(sheetView);
        ImageView imgShow = (ImageView) mBottomSheetDialog.findViewById(R.id.imgShow);

        ImageView imgDownload = (ImageView) mBottomSheetDialog.findViewById(R.id.imgDownload);
        Picasso.get().load(fee_paid_bean.getScreenshot()).placeholder(getResources().getDrawable(R.drawable.logo)).into(imgShow);
        imgDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                ErpProgress.showProgressBar(GETHomeworkActivity.this, "downloading...");
//                Log.e("Ultimate1","Ultimate1");
//                Picasso.with(GETHomeworkActivity.this).load(homeworkbean.getImage()).into(imgTraget);
//                Log.e("Ultimate2","Ultimate2");
//                Toast.makeText(GETHomeworkActivity.this, "Downloaded successfully", Toast.LENGTH_LONG).show();

                BitmapDrawable bitmapDrawable = (BitmapDrawable) imgShow.getDrawable();
                Bitmap bitmap = bitmapDrawable.getBitmap();
                saveImageToGallery(bitmap);


            }
        });
        mBottomSheetDialog.show();
        Window window = mBottomSheetDialog.getWindow();
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);


    }

    private void saveImageToGallery(Bitmap bitmap){
        String school= User.getCurrentUser().getSchoolData().getName();
        OutputStream fos;

        try{

            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q){

                ContentResolver resolver = getActivity().getContentResolver();
                ContentValues contentValues =  new ContentValues();
                contentValues.put(MediaStore.MediaColumns.DISPLAY_NAME, "fees_img_saved" + ".jpg");
                contentValues.put(MediaStore.MediaColumns.MIME_TYPE, "image/jpeg");
                contentValues.put(MediaStore.MediaColumns.RELATIVE_PATH, Environment.DIRECTORY_PICTURES + File.separator + school);
                Uri imageUri = resolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, contentValues);

                fos = resolver.openOutputStream(Objects.requireNonNull(imageUri));


                Toast.makeText(getActivity(), "Image Saved", Toast.LENGTH_SHORT).show();
            }
            else{

                //with folder name
                String imagesDir = Environment.getExternalStoragePublicDirectory(
                        Environment.DIRECTORY_PICTURES).toString() + File.separator + school;

                File file = new File(imagesDir);

                if (!file.exists()) {
                    file.mkdir();
                }

                File image = new File(imagesDir, "fees_img_saved" + ".jpg");
                fos = new FileOutputStream(image);

                Toast.makeText(getActivity(), "Image saved to internal!!", Toast.LENGTH_SHORT).show();
                //resetOpTimes();

            }

            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            Objects.requireNonNull(fos);

        }catch(Exception e){

            Toast.makeText(getActivity(), "Image not saved \n" + e.toString(), Toast.LENGTH_SHORT).show();
        }

    }

}