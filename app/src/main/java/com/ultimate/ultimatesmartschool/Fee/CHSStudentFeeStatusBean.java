package com.ultimate.ultimatesmartschool.Fee;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class CHSStudentFeeStatusBean {

    private String name;
    private String father_name;
    private String class_name;
    private String es_months;
    private String es_fee_bal;
    private String es_prev_bal;
    private String es_totalamount;
    private String es_particularamount;
    private String es_concessionamount;
    private String es_fine;

    public String getEs_annual_bal() {
        return es_annual_bal;
    }

    public void setEs_annual_bal(String es_annual_bal) {
        this.es_annual_bal = es_annual_bal;
    }

    private String es_annual_bal;

    public String getEs_fine() {
        return es_fine;
    }

    public void setEs_fine(String es_fine) {
        this.es_fine = es_fine;
    }

    private String student_id;

    public String getStudent_id() {
        return student_id;
    }

    public void setStudent_id(String student_id) {
        this.student_id = student_id;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFather_name() {
        return father_name;
    }

    public void setFather_name(String father_name) {
        this.father_name = father_name;
    }

    public String getClass_name() {
        return class_name;
    }

    public void setClass_name(String class_name) {
        this.class_name = class_name;
    }

    public String getEs_months() {
        return es_months;
    }

    public void setEs_months(String es_months) {
        this.es_months = es_months;
    }

    public String getEs_fee_bal() {
        return es_fee_bal;
    }

    public void setEs_fee_bal(String es_fee_bal) {
        this.es_fee_bal = es_fee_bal;
    }

    public String getEs_prev_bal() {
        return es_prev_bal;
    }

    public void setEs_prev_bal(String es_prev_bal) {
        this.es_prev_bal = es_prev_bal;
    }

    public String getEs_totalamount() {
        return es_totalamount;
    }

    public void setEs_totalamount(String es_totalamount) {
        this.es_totalamount = es_totalamount;
    }

    public String getEs_particularamount() {
        return es_particularamount;
    }

    public void setEs_particularamount(String es_particularamount) {
        this.es_particularamount = es_particularamount;
    }

    public String getEs_concessionamount() {
        return es_concessionamount;
    }

    public void setEs_concessionamount(String es_concessionamount) {
        this.es_concessionamount = es_concessionamount;
    }

    public static ArrayList<CHSStudentFeeStatusBean> parseClassArray(JSONArray arrayObj) {
        ArrayList list = new ArrayList<CHSStudentFeeStatusBean>();
        try {
            for (int i = 0; i < arrayObj.length(); i++) {
                CHSStudentFeeStatusBean p = parseClassObject(arrayObj.getJSONObject(i));
                if (p != null) {
                    list.add(p);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public static CHSStudentFeeStatusBean parseClassObject(JSONObject jsonObject) {
        CHSStudentFeeStatusBean casteObj = new CHSStudentFeeStatusBean();
        try {

            if (jsonObject.has("name") && !jsonObject.getString("name").isEmpty() && !jsonObject.getString("name").equalsIgnoreCase("null")) {
                casteObj.setName(jsonObject.getString("name"));
            }
            if (jsonObject.has("father_name") && !jsonObject.getString("father_name").isEmpty() && !jsonObject.getString("father_name").equalsIgnoreCase("null")) {
                casteObj.setFather_name(jsonObject.getString("father_name"));
            }
            if (jsonObject.has("class_name") && !jsonObject.getString("class_name").isEmpty() && !jsonObject.getString("class_name").equalsIgnoreCase("null")) {
                casteObj.setClass_name(jsonObject.getString("class_name"));
            }
            if (jsonObject.has("es_months") && !jsonObject.getString("es_months").isEmpty() && !jsonObject.getString("es_months").equalsIgnoreCase("null")) {
                casteObj.setEs_months(jsonObject.getString("es_months"));
            }
            if (jsonObject.has("es_fee_bal") && !jsonObject.getString("es_fee_bal").isEmpty() && !jsonObject.getString("es_fee_bal").equalsIgnoreCase("null")) {
                casteObj.setEs_fee_bal(jsonObject.getString("es_fee_bal"));
            }
            if (jsonObject.has("es_prev_bal") && !jsonObject.getString("es_prev_bal").isEmpty() && !jsonObject.getString("es_prev_bal").equalsIgnoreCase("null")) {
                casteObj.setEs_prev_bal(jsonObject.getString("es_prev_bal"));
            }

            if (jsonObject.has("es_totalamount") && !jsonObject.getString("es_totalamount").isEmpty() && !jsonObject.getString("es_totalamount").equalsIgnoreCase("null")) {
                casteObj.setEs_totalamount(jsonObject.getString("es_totalamount"));
            }
            if (jsonObject.has("es_particularamount") && !jsonObject.getString("es_particularamount").isEmpty() && !jsonObject.getString("es_particularamount").equalsIgnoreCase("null")) {
                casteObj.setEs_particularamount(jsonObject.getString("es_particularamount"));
            }
            if (jsonObject.has("es_concessionamount") && !jsonObject.getString("es_concessionamount").isEmpty() && !jsonObject.getString("es_concessionamount").equalsIgnoreCase("null")) {
                casteObj.setEs_concessionamount(jsonObject.getString("es_concessionamount"));
            }
            if (jsonObject.has("student_id") && !jsonObject.getString("student_id").isEmpty() && !jsonObject.getString("student_id").equalsIgnoreCase("null")) {
                casteObj.setStudent_id(jsonObject.getString("student_id"));
            }
            if (jsonObject.has("es_fine") && !jsonObject.getString("es_fine").isEmpty() && !jsonObject.getString("es_fine").equalsIgnoreCase("null")) {
                casteObj.setEs_fine(jsonObject.getString("es_fine"));
            }
            if (jsonObject.has("es_annual_bal") && !jsonObject.getString("es_annual_bal").isEmpty() && !jsonObject.getString("es_annual_bal").equalsIgnoreCase("null")) {
                casteObj.setEs_annual_bal(jsonObject.getString("es_annual_bal"));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return casteObj;
    }
}
