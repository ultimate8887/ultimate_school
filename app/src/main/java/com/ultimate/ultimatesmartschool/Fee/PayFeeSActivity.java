package com.ultimate.ultimatesmartschool.Fee;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.icu.text.SimpleDateFormat;
import android.icu.util.Calendar;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetSequence;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Staff.AssignIncharge;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PayFeeSActivity extends AppCompatActivity implements FeeListAdapter.ProdMethodCallBack {


    int totalPrice = 0,trans_amount= 0, p_balance=0, p_fine_balance=0, new_totalPrice=0,check=0, totalPrice_withoutTrans=0,selection = 0;
    int p_value=0,t_value=0,c_value=0,total_value=0,paid_fine_value=0,today_add_fine=0,new_f_balance=0,new_total_fine=0,new_feess_balance=0;

    // All EditText
    @BindView(R.id.edtName)
    EditText edtName;
    @BindView(R.id.edtFather)
    EditText edtFather;
    @BindView(R.id.edtRegistration)
    EditText edtRegistration;

    @BindView(R.id.fine_balance)
    EditText fine_balance;
    @BindView(R.id.today_fine)
    EditText today_fine;
    @BindView(R.id.paid_fine)
    EditText paid_fine;

    @BindView(R.id.p_amount)
    EditText p_amount;
    @BindView(R.id.t_amount)
    EditText t_amount;
    @BindView(R.id.c_amount)
    EditText c_amount;
    @BindView(R.id.g_total)
    EditText g_total;


    @BindView(R.id.total_fine)
    EditText total_fine;
    @BindView(R.id.new_fine_balance)
    EditText new_fine_balance;
    @BindView(R.id.new_fee_balance)
    EditText new_fee_balance;

    @BindView(R.id.edtReason)
    EditText edtReason;


    @BindView(R.id.b_name)
    EditText b_name;
    @BindView(R.id.b_a_no)
    EditText b_a_no;
    @BindView(R.id.cheque)
    EditText cheque;

    // All ImageView
    @BindView(R.id.dp)
    ImageView dp;


    // All TextView
    @BindView(R.id.view_std)
    TextView view_std;
    @BindView(R.id.balance)
    TextView balance;
    @BindView(R.id.totals)
    TextView totals;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.submit)
    RelativeLayout submit;
//    @BindView(R.id.textNorecord)
    TextView txtNorecord;

    @BindView(R.id.fee_view)
    TextView fee_view;
    @BindView(R.id.fine_view)
    TextView fine_view;
    @BindView(R.id.pay_view)
    TextView pay_view;
    @BindView(R.id.f_details)
    TextView f_details;
    @BindView(R.id.paying_amount)
    TextView paying_amount;

//    @BindView(R.id.txt_list)
    TextView txt_list;

    // All LinearLayout
    @BindView(R.id.bank_lyt)
    LinearLayout bank_lyt;
    @BindView(R.id.student_lyt)
    LinearLayout student_lyt;
    @BindView(R.id.top)
    LinearLayout top;
    @BindView(R.id.bottom)
    LinearLayout bottom;
//
//    @BindView(R.id.list)
//    RelativeLayout list_view;

    @BindView(R.id.pay_close)
    LinearLayout pay_close;
    @BindView(R.id.fine_close)
    LinearLayout fine_close;
    @BindView(R.id.fee_close)
    LinearLayout fee_close;


    @BindView(R.id.spinnerMonth)
    Spinner spinnerMonth;
    @BindView(R.id.spinner)
    Spinner bank_spinner;

    ArrayList<String> monthList = new ArrayList<>();
    ArrayList<String> bankList = new ArrayList<>();
    String selectMonth ="";
    String selectBankType ="",name="",f_name="",roll="",
            image="",gender="",regist="",classid="",
            fee_cate_id="",fromdate="",todate="",class_name="",place_id="",route_id="",month_name="";

    @BindView(R.id.contact_support)
    ImageView contact_support;
    SharedPreferences sharedPreferences;

    //TransAmount
    @BindView(R.id.recycler_trans)
    RecyclerView recycler_trans;
    private TransAmountAdap transAmountAdap;
    ArrayList<TransAmount> transAmounts = new ArrayList<>();


    //FeeListAdapter
    RecyclerView recyclerView;
    ArrayList<Fee_Paid_Bean> paid_beanslist;
    private FeeListAdapter mAdapter;
    ImageView close;
    TextView txtSetup;

    //ParticularBean
    @BindView(R.id.recycler_view)
    RecyclerView recycler_view;
    RecyclerView.LayoutManager layoutManager;
    private Particular_Adapter newsAdapter;
    ArrayList<ParticularBean> classList = new ArrayList<>();


    ArrayList<FeeBalanceBean> list = new ArrayList<>();

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pay_fee_s);
        ButterKnife.bind(this);

        edtName.setEnabled(false);
        edtFather.setEnabled(false);
        edtRegistration.setEnabled(false);

        if (getIntent().getExtras()!=null){
            name=getIntent().getExtras().getString("name");
            f_name=getIntent().getExtras().getString("f_name");
            image=getIntent().getExtras().getString("dp");
            gender=getIntent().getExtras().getString("gender");
            roll=getIntent().getExtras().getString("roll");
            regist=getIntent().getExtras().getString("regist");
            classid=getIntent().getExtras().getString("class");
            fee_cate_id=getIntent().getExtras().getString("fee_cate_id");
            class_name=getIntent().getExtras().getString("class_name");
            fromdate=getIntent().getExtras().getString("fromdate");
            todate=getIntent().getExtras().getString("todate");
            place_id=getIntent().getExtras().getString("place_id");
            route_id=getIntent().getExtras().getString("route_id");
            route_id=getIntent().getExtras().getString("route_id");
            //TransAmount
            layoutManager = new LinearLayoutManager(PayFeeSActivity.this);
            recycler_trans.setLayoutManager(layoutManager);
            // newsAdapter = new NewsAdapter(this,mData);
            transAmountAdap = new TransAmountAdap(transAmounts,PayFeeSActivity.this);
            recycler_trans.setAdapter(transAmountAdap);

            //ParticularBean
            layoutManager = new LinearLayoutManager(PayFeeSActivity.this);
            recycler_view.setLayoutManager(layoutManager);
            // newsAdapter = new NewsAdapter(this,mData);
            newsAdapter = new Particular_Adapter(classList,PayFeeSActivity.this);
            recycler_view.setAdapter(newsAdapter);

           // Toast.makeText(getApplicationContext(),"fee_cate_id"+fee_cate_id+"\nplace_id"+place_id+"\nroute_id"+route_id,Toast.LENGTH_LONG).show();

            setData();
        }


        fetchmonthList();
        fetchBankType();

        if (!restorePrefData()){
            setShowcaseView();
        }



        txtTitle.setText(class_name+" Class Fees");
    }


    private void savePrefData(){
        sharedPreferences= PayFeeSActivity.this.getSharedPreferences("boarding_pref_list",MODE_PRIVATE);
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putBoolean("IsFirstTimeVisit_list",true);
        editor.apply();
    }

    private boolean restorePrefData(){
        sharedPreferences = PayFeeSActivity.this.getSharedPreferences("boarding_pref_list",MODE_PRIVATE);
        return sharedPreferences.getBoolean("IsFirstTimeVisit_list",false);
    }

    private void setShowcaseView() {
        new TapTargetSequence(this)
                .targets(TapTarget.forView(contact_support,"View Student Fees History!","Tap the View button to view Student Fees History.")
                        .outerCircleColor(R.color.light)
                        .outerCircleAlpha(0.96f)
                        .targetCircleColor(R.color.white)
                        .titleTextSize(20)
                        .titleTextColor(R.color.white)
                        .descriptionTextSize(14)
                        .descriptionTextColor(R.color.black)
                        .textColor(R.color.black)
                        .textTypeface(Typeface.SANS_SERIF)
                        .dimColor(R.color.black)
                        .drawShadow(true)
                        .cancelable(false)
                        .tintTarget(true)
                        .transparentTarget(true)
                        .targetRadius(60)).listener(new TapTargetSequence.Listener() {
            @Override
            public void onSequenceFinish() {
                // Toast.makeText(getApplicationContext(),"Sequence Finished",Toast.LENGTH_SHORT).show();
                savePrefData();
                commonIntent();
            }

            @Override
            public void onSequenceStep(TapTarget lastTarget, boolean targetClicked) {
                // Toast.makeText(getActivity(),"GREAT!",Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSequenceCanceled(TapTarget lastTarget) {

            }
        }).start();


    }

    private void commonIntent() {
        Intent intent = new Intent(PayFeeSActivity.this, CommonFeesListActivity.class);
        intent.putExtra("name", edtName.getText().toString());
        intent.putExtra("f_name", edtFather.getText().toString());
        intent.putExtra("roll", edtRegistration.getText().toString());
        intent.putExtra("dp", image);
        intent.putExtra("regist", regist);
        intent.putExtra("class", classid);
        intent.putExtra("class_name", class_name);
        intent.putExtra("gender", gender);
        intent.putExtra("fee_cate_id", fee_cate_id);
        intent.putExtra("route_id", route_id);
        intent.putExtra("place_id", place_id);
        intent.putExtra("fromdate", fromdate);
        intent.putExtra("todate", todate);
        intent.putExtra("month", selectMonth);
        intent.putExtra("tag", "student_wise");
        startActivity(intent);
    }

    private void setData() {


        fine_balance.setEnabled(false);
        g_total.setEnabled(false);

        student_lyt.setVisibility(View.VISIBLE);

        if (gender.equalsIgnoreCase("Male")) {
            if (image != null) {
                Utils.progressImg(image,dp,PayFeeSActivity.this);
                // Picasso.with(getActivity()).load(stuList.get(i-1).getProfile()).placeholder(R.drawable.stud).into(dp);
            } else {
                Picasso.get().load(R.drawable.stud).into(dp);
            }
        }else{
            if (image != null) {
                Utils.progressImg(image,dp,PayFeeSActivity.this);
                //  Picasso.with(getActivity()).load(stuList.get(i-1).getProfile()).placeholder(R.drawable.f_student).into(dp);
            } else {
                Picasso.get().load(R.drawable.f_student).into(dp);
            }
        }

        edtFather.setText(name);
        edtName.setText(f_name);
        edtRegistration.setText(roll);

        p_amount.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (!p_amount.getText().toString().isEmpty()) {
                    p_value= Integer.parseInt(p_amount.getText().toString());
                }else {
                    p_value=0;
                    p_amount.setHint("0");
                    if (check==0){
                        showOnce();
                    }
               }


                 setTotalvalues("p_amount");
            }
        });

        t_amount.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (!t_amount.getText().toString().isEmpty()) {
                    t_value= Integer.parseInt(t_amount.getText().toString());
                }else {
                    t_value=0;
                    t_amount.setHint("0");
                    if (check==0){
                        showOnce();
                    }

                }


                setTotalvalues("t_amount");
            }
        });

        c_amount.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {


                if (!c_amount.getText().toString().isEmpty()) {
                    c_value = Integer.parseInt(c_amount.getText().toString());
                }else {
                    c_value=0;
                    c_amount.setHint("0");
                    if (check==0){
                        showOnce();
                    }
                }
                setTotalvalues("c_amount");
            }
        });



        today_fine.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {


                if (!today_fine.getText().toString().isEmpty()) {
                    today_add_fine = Integer.parseInt(today_fine.getText().toString());
                }else {
                    today_add_fine=0;
                    today_fine.setHint("0");
                    if (check==0){
                        showOnce();
                    }
                }
                setTotalfinevalues();
            }
        });


        paid_fine.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {


                if (!paid_fine.getText().toString().isEmpty()) {
                    paid_fine_value = Integer.parseInt(paid_fine.getText().toString());
                }else {
                    paid_fine_value=0;
                    paid_fine.setHint("0");
                    if (check==0){
                        showOnce();
                    }

                }
                setTotalfinevalues();
            }
        });


    }

    private void showOnce() {
        check++;
        Toast.makeText(getApplicationContext(),"Kindly enter valid amount\nThank You!",Toast.LENGTH_LONG).show();
    }

    private void setTotalfinevalues() {

        new_total_fine=p_fine_balance+today_add_fine;
        total_fine.setText(String.valueOf(new_total_fine));

        if (new_total_fine >= paid_fine_value) {
            new_f_balance=new_total_fine-paid_fine_value;
            new_fine_balance.setText(String.valueOf(new_f_balance));

        }else {
            Toast.makeText(getApplicationContext(),"Kindly enter valid amount, You are putting an amount\n more than the actual fine\nThank You!",Toast.LENGTH_LONG).show();
            paid_fine.setText("0");
            new_fine_balance.setText(String.valueOf(new_total_fine));
        }

        int fees=Integer.parseInt(g_total.getText().toString());
        int paying=paid_fine_value+fees;
        paying_amount.setText(String.valueOf(paying));

    }

    private void setTotalvalues(String tag) {


        if (tag.equalsIgnoreCase("p_amount")){
            total_value=p_value+t_value-c_value;
            if (totalPrice_withoutTrans >= p_value) {
                g_total.setText(String.valueOf(total_value));
                new_feess_balance=new_totalPrice-p_value+t_value;
                new_fee_balance.setText(String.valueOf(new_feess_balance));
            }else {
                Toast.makeText(getApplicationContext(),"Kindly enter valid amount, You are putting an amount\n more than the actual Class fee\nThank You!",Toast.LENGTH_LONG).show();
                this.p_amount.setText("0");
                new_fee_balance.setText(String.valueOf(new_totalPrice));
            }
        }else if (tag.equalsIgnoreCase("t_amount")){
            total_value=p_value+t_value-c_value;
            if (trans_amount >= t_value) {
                g_total.setText(String.valueOf(total_value));
                int check=p_value+t_value;
                new_feess_balance=new_totalPrice-check;
                new_fee_balance.setText(String.valueOf(new_feess_balance));
            }else {
                Toast.makeText(getApplicationContext(),"Kindly enter valid amount, You are putting an amount\n more than the actual Transport fee\nThank You!",Toast.LENGTH_LONG).show();
                this.t_amount.setText("0");
                new_fee_balance.setText(String.valueOf(new_totalPrice));
            }
        }else {
            int tot_value=p_value+t_value;
            total_value=p_value+t_value-c_value;
            g_total.setText(String.valueOf(total_value));
            if (tot_value >= c_value) {
                int check=p_value+t_value;
                new_feess_balance=new_totalPrice-check;
                new_fee_balance.setText(String.valueOf(new_feess_balance));
            }else {
                Toast.makeText(getApplicationContext(),"Kindly enter valid amount, You are putting an amount\n more than the actual total fee\nThank You!",Toast.LENGTH_LONG).show();
                this.c_amount.setText("0");
                new_fee_balance.setText(String.valueOf(new_totalPrice));
            }
        }


        int fees=Integer.parseInt(g_total.getText().toString());
        int paying=paid_fine_value+fees;
        paying_amount.setText(String.valueOf(paying));


    }

    @OnClick(R.id.fee_close)
    public void fee_close() {
        //  Toast.makeText(getApplicationContext(),"Select view_std",Toast.LENGTH_SHORT).show();
        fee_close.setVisibility(View.GONE);
        fee_view.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.fee_view)
    public void fee_view() {
        // Toast.makeText(getApplicationContext(),"Select student_lyt",Toast.LENGTH_SHORT).show();
        fee_close.setVisibility(View.VISIBLE);
        fee_view.setVisibility(View.GONE);
    }

    @OnClick(R.id.contact_support)
    public void contact_support() {
        // Toast.makeText(getApplicationContext(),"Select student_lyt",Toast.LENGTH_SHORT).show();
       commonIntent();
    }


    @OnClick(R.id.pay_close)
    public void pay_close() {
        //  Toast.makeText(getApplicationContext(),"Select view_std",Toast.LENGTH_SHORT).show();
        pay_close.setVisibility(View.GONE);
        pay_view.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.pay_view)
    public void pay_view() {
        // Toast.makeText(getApplicationContext(),"Select student_lyt",Toast.LENGTH_SHORT).show();
        pay_close.setVisibility(View.VISIBLE);
        pay_view.setVisibility(View.GONE);
    }



    @OnClick(R.id.fine_close)
    public void fine_close() {
        //  Toast.makeText(getApplicationContext(),"Select view_std",Toast.LENGTH_SHORT).show();
        fine_close.setVisibility(View.GONE);
        fine_view.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.fine_view)
    public void fine_view() {
        // Toast.makeText(getApplicationContext(),"Select student_lyt",Toast.LENGTH_SHORT).show();
        fine_close.setVisibility(View.VISIBLE);
        fine_view.setVisibility(View.GONE);
    }


    @OnClick(R.id.view_std)
    public void view_stddd() {
      //  Toast.makeText(getApplicationContext(),"Select view_std",Toast.LENGTH_SHORT).show();
        view_std.setVisibility(View.GONE);
        student_lyt.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.student_lyt)
    public void student_lyttt() {
       // Toast.makeText(getApplicationContext(),"Select student_lyt",Toast.LENGTH_SHORT).show();
        view_std.setVisibility(View.VISIBLE);
        student_lyt.setVisibility(View.GONE);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        commonBack();
    }

    @OnClick(R.id.imgBack)
    public void imgBackssss() {
        commonBack();

    }

    private void commonBack() {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(PayFeeSActivity.this);
        builder1.setMessage("Are you sure, you want to exit? ");
        builder1.setCancelable(false);
        builder1.setPositiveButton(
                "Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finish();
                    }
                });
        builder1.setNegativeButton(
                "No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

    private void fetchBankType() {
        bankList.add("Cash");
        bankList.add("Cheque");
        bankList.add("DD");

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, bankList);
        //set the view for the Drop down list
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //set the ArrayAdapter to the spinner
        bank_spinner.setAdapter(dataAdapter);
        //attach the listener to the spinner
        bank_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long l) {

                selectBankType = bankList.get(pos);
                if (selectBankType.equalsIgnoreCase("Cash")){
                    bank_lyt.setVisibility(View.GONE);
                }else {
                    bank_lyt.setVisibility(View.VISIBLE);
                    Toast.makeText(getApplicationContext(),"Kindly Add Bank Details \n",Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void fetchmonthList() {

        monthList.add("April");
        monthList.add("May");
        monthList.add("June");
        monthList.add("July");
        monthList.add("August");
        monthList.add("September");
        monthList.add("October");
        monthList.add("November");
        monthList.add("December");
        monthList.add("January");
        monthList.add("February");
        monthList.add("March");


       //   Toast.makeText(getApplicationContext(),month_name,Toast.LENGTH_SHORT).show();


        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, monthList);
        //set the view for the Drop down list
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //set the ArrayAdapter to the spinner
        spinnerMonth.setAdapter(dataAdapter);

        Calendar cal=Calendar.getInstance();
        SimpleDateFormat month_date = new SimpleDateFormat("MMMM");
        month_name = month_date.format(cal.getTime());

        // setting here spinner item selected with position..
        for(int i=0;i<monthList.size();i++) {
            if (monthList.get(i).equalsIgnoreCase(month_name)) {
                    selectMonth = monthList.get(i);
                    spinnerMonth.setSelection(i);

            }
        }

        //attach the listener to the spinner
        spinnerMonth.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long l) {
                    selectMonth = monthList.get(pos);
                    fetchFeesDetails(selectMonth);

                if (!selectMonth.equalsIgnoreCase("")) {
                    String title = getColoredSpanned(selectMonth, "#F55B53");
                    String Name = getColoredSpanned(" MONTH FEES DETAILS", "#000000");
                    f_details.setText(Html.fromHtml(title + " " + Name));
                   // txt_list.setText(Html.fromHtml(title + " " + Name));
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }
    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }

    private void fetchFeesDetails(String month) {


        fine_balance.setText("0");
        balance.setText("0");
        totals.setText("0");
        p_amount.setText("0");
        t_amount.setText("0");
        c_amount.setText("0");
        g_total.setText("0");
        new_fee_balance.setText("0");
        fine_balance.setText("0");
        new_fine_balance.setText("0");
        total_fine.setText("0");
        today_fine.setText("0");
        paid_fine.setText("0");


        ErpProgress.showProgressBar(PayFeeSActivity.this, "Please wait...");
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("month", month);
        params.put("c_id", classid);
        params.put("s_id", regist);
        params.put("fromdate", fromdate);
        params.put("todate", todate);
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.CHECK_MONTHLY, apiCallback, this, params);
    }

    ApiHandler.ApiCallback apiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            if (error == null) {
                ErpProgress.cancelProgressBar();
                openDialog("start");
                Log.i("jsonObjectsssssssss", String.valueOf(jsonObject));
                Toast.makeText(getApplicationContext(),"Student "+name+ " already paid "+selectMonth+" Month Fees \n kindly select another Month",Toast.LENGTH_LONG).show();

            } else {
                ErpProgress.cancelProgressBar();
              //  Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_SHORT).show();
                fetchFeesAmount();
                fetchPreviousbalance();
                fetTranAmount();
            }
        }
    };



    private void openDialog(String value) {

        BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(this);
        final View sheetView = getLayoutInflater().inflate(R.layout.fee_success_dialog, null);
        mBottomSheetDialog.setContentView(sheetView);
        mBottomSheetDialog.setCancelable(false);

        view_std.setVisibility(View.GONE);
        student_lyt.setVisibility(View.VISIBLE);
        top.setVisibility(View.GONE);
        bottom.setVisibility(View.GONE);

        recyclerView=(RecyclerView) sheetView.findViewById(R.id.recyclerview_list);
        close=(ImageView) sheetView.findViewById(R.id.close);
        txtSetup=(TextView) sheetView.findViewById(R.id.txtSetup);
        txtNorecord=(TextView) sheetView.findViewById(R.id.textNorecord);
        txt_list=(TextView) sheetView.findViewById(R.id.txt_list);

        if (value.equalsIgnoreCase("start")){
            txtSetup.setText("This Month Fees \nAlready Paid!");
        }else{
            txtSetup.setText("Fees Paid Successfully \nThank You!");
        }

        //FeeListAdapter
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        mAdapter = new FeeListAdapter(paid_beanslist,this, this,1);
        recyclerView.setAdapter(mAdapter);

        if (!selectMonth.equalsIgnoreCase("")) {
            String title = getColoredSpanned(selectMonth, "#F55B53");
            String Name = getColoredSpanned(" MONTH FEES DETAILS", "#000000");
            txt_list.setText(Html.fromHtml("'"+title+"'" + " " + Name));
        }

        fetchFeeDetail();

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //assignData();
                if (value.equalsIgnoreCase("start")){
                    mBottomSheetDialog.dismiss();
                }else{
                    mBottomSheetDialog.dismiss();
                    finish();
                }
            }
        });


        Button btnYes= (Button) sheetView.findViewById(R.id.btnYes);
        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //assignData();
                if (value.equalsIgnoreCase("start")){
                    mBottomSheetDialog.dismiss();
                }else{
                    mBottomSheetDialog.dismiss();
                    finish();
                }
               // finish();
            }
        });
        mBottomSheetDialog.show();

    }






    private void fetchFeeDetail() {
//        view_std.setVisibility(View.GONE);
//        student_lyt.setVisibility(View.VISIBLE);
//        list_view.setVisibility(View.VISIBLE);
//        top.setVisibility(View.GONE);
//        bottom.setVisibility(View.GONE);

        ErpProgress.showProgressBar(this, "Please wait..");
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("student_id", regist);
        params.put("month", selectMonth);
        params.put("todate", todate);
        params.put("fromdate", fromdate);
        String url = Constants.getBaseURL() + Constants.FEE_PAID_STUDENT;
        ApiHandler.apiHit(Request.Method.POST, url, apiCallback6, this, params);
    }

    ApiHandler.ApiCallback apiCallback6 = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            if (error == null) {
                if (paid_beanslist != null)
                    paid_beanslist.clear();


                JSONArray jsonArray = null;
                try {
                    jsonArray = jsonObject.getJSONArray("fee_data");
                    paid_beanslist = Fee_Paid_Bean.parsFee_Paid_BeanArray(jsonArray);
                    if (paid_beanslist.size() > 0) {
                        mAdapter.setMerchantBeans(paid_beanslist);
                        //setanimation on adapter...
                        recyclerView.getAdapter().notifyDataSetChanged();
                        recyclerView.scheduleLayoutAnimation();
                        //-----------end------------
                        txtNorecord.setVisibility(View.GONE);
                    } else {
                        mAdapter.setMerchantBeans(paid_beanslist);
                        mAdapter.notifyDataSetChanged();
                        txtNorecord.setVisibility(View.VISIBLE);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                txtNorecord.setVisibility(View.VISIBLE);
                Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_SHORT).show();
            }
        }
    };

    private void fetTranAmount() {
        trans_amount=0;
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("s_id", regist);
        params.put("place_id", place_id);
        params.put("route_id", route_id);
        params.put("month", "trans");
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.CHECK_FEES_CLASS, apiCall, this, params);
    }

    ApiHandler.ApiCallback apiCall = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            if (error == null) {
                ErpProgress.cancelProgressBar();
                Log.i("jsonObjectsssssssss", String.valueOf(jsonObject));
                  t_amount.setEnabled(true);
                view_std.setVisibility(View.VISIBLE);
                student_lyt.setVisibility(View.GONE);
               // list_view.setVisibility(View.GONE);
                top.setVisibility(View.VISIBLE);
                bottom.setVisibility(View.VISIBLE);

                try {
                    if (transAmounts != null) {
                        transAmounts.clear();
                    }

                    if (transAmounts != null){
                        transAmounts.clear();
                        transAmounts=TransAmount.parseParticularBeanArray(jsonObject.getJSONArray("month_data"));
                        transAmountAdap.setFeeList(transAmounts);
                        transAmountAdap.notifyDataSetChanged();

                        for (int i = 0; i<transAmounts.size(); i++)
                        {
                            trans_amount += Integer.parseInt(transAmounts.get(i).getTrans_amount());
                        }
                         setTotals();
                        // Toast.makeText(getApplicationContext(),"totals "+totalPrice,Toast.LENGTH_SHORT).show();

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            } else {
                ErpProgress.cancelProgressBar();
                t_amount.setEnabled(false);
                t_amount.setTextColor(ContextCompat.getColor(PayFeeSActivity.this, R.color.grey));
                //  Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_SHORT).show();
                transAmounts.clear();
                transAmountAdap.setFeeList(transAmounts);
                transAmountAdap.notifyDataSetChanged();
            }
        }
    };

    private void setTotals() {
        totalPrice_withoutTrans=totalPrice+p_balance;
        new_totalPrice=p_balance+totalPrice+trans_amount;
        totals.setText(String.valueOf(new_totalPrice));
    }

    private void fetchFeesAmount() {

       // ErpProgress.showProgressBar(PayFeeSActivity.this, "Please wait...");
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("month", "all");
        params.put("c_id", classid);
        params.put("s_id", regist);
        params.put("fromdate", fromdate);
        params.put("todate", todate);
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.CHECK_MONTHLY, apiCallback2, this, params);
    }

    ApiHandler.ApiCallback apiCallback2 = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            if (error == null) {
                ErpProgress.cancelProgressBar();
                Log.i("jsonObjectsssssssss", String.valueOf(jsonObject));
              //  Toast.makeText(getApplicationContext(),"select another Month",Toast.LENGTH_SHORT).show();

                try {
                    list=FeeBalanceBean.parseParticularBeanArray(jsonObject.getJSONArray("month_data"));

                    p_balance= Integer.parseInt(list.get(0).getFee_bal());
                    p_fine_balance= Integer.parseInt(list.get(0).getBalance_fine());

//                    Toast.makeText(getApplicationContext(),new_totalPrice+p_balance+p_fine_balance,Toast.LENGTH_LONG).show();


                } catch (JSONException e) {
                    e.printStackTrace();
                }
                setFeeData(p_balance,p_fine_balance);

            } else {
                ErpProgress.cancelProgressBar();

                if (p_fine_balance==0){
                    fine_close.setVisibility(View.GONE);
                    fine_view.setVisibility(View.VISIBLE);
                }
             //    Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_SHORT).show();
//                fetchFeesAmount();
//                fetchPreviousbalance();
            }
        }
    };

    private void setFeeData(int p_balance, int p_fine_balance) {



        Log.e("hahaha", String.valueOf(p_balance));
        Log.e("hahaha", String.valueOf(p_fine_balance));
        Log.e("hahaha", String.valueOf(totalPrice));
        Log.e("hahaha", String.valueOf(new_totalPrice));

        if (p_fine_balance==0){
            fine_close.setVisibility(View.GONE);
            fine_view.setVisibility(View.VISIBLE);
        }

        fine_balance.setText(String.valueOf(p_fine_balance));
        total_fine.setText(String.valueOf(p_fine_balance));
        balance.setText(String.valueOf(p_balance));


     //   totals.setText(String.valueOf(new_totalPrice));

    }

    private void fetchPreviousbalance() {
        totalPrice=0;
        ErpProgress.showProgressBar(PayFeeSActivity.this, "Please wait...");
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("month", selectMonth);
        params.put("c_id", classid);
        params.put("fee_cate_id", fee_cate_id);
        params.put("fromdate", fromdate);
        params.put("todate", todate);
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.CHECK_FEES_CLASS, apiCallback1, this, params);
    }

    ApiHandler.ApiCallback apiCallback1 = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            if (error == null) {
                ErpProgress.cancelProgressBar();
                Log.i("jsonObjectsssssssss", String.valueOf(jsonObject));

                view_std.setVisibility(View.VISIBLE);
                student_lyt.setVisibility(View.GONE);
                top.setVisibility(View.VISIBLE);
                bottom.setVisibility(View.VISIBLE);

                try {
                    if (classList != null) {
                        classList.clear();
                    }

                    if (classList != null){
                        classList.clear();
                        classList=ParticularBean.parseParticularBeanArray(jsonObject.getJSONArray("month_data"));
                        newsAdapter.setFeeList(classList);
                        newsAdapter.notifyDataSetChanged();
                        for (int i = 0; i<classList.size(); i++)
                        {
                            totalPrice += Integer.parseInt(classList.get(i).getFee_amount());
                        }
                       // Toast.makeText(getApplicationContext(),"totals "+totalPrice,Toast.LENGTH_SHORT).show();
                        setTotals();
//                        new_totalPrice=p_balance+totalPrice;
//                      totals.setText(String.valueOf(new_totalPrice));
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            } else {
                ErpProgress.cancelProgressBar();
              //  Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_SHORT).show();
                classList.clear();
                newsAdapter.setFeeList(classList);
                newsAdapter.notifyDataSetChanged();
            }
        }
    };


    @OnClick(R.id.submit)
    public void submit() {
            if (selectMonth.equalsIgnoreCase("")) {
                Toast.makeText(getApplicationContext(),"Kindly Select Fees Pay Month",Toast.LENGTH_LONG).show();
            }
            else{
                ErpProgress.showProgressBar(this, "Please wait...");
                HashMap<String, String> params = new HashMap<>();

                if (p_amount.getText().toString().isEmpty()) {
                    params.put("particular_amount", "0");
                }else {
                    params.put("particular_amount", p_amount.getText().toString());
                }

                params.put("last_fee_bal", balance.getText().toString());
                params.put("total_amount", totals.getText().toString());
                params.put("fee_bal", new_fee_balance.getText().toString());

                if (t_amount.getText().toString().isEmpty()) {
                    params.put("es_transportfee", "0");
                }else {
                    params.put("es_transportfee", t_amount.getText().toString());
                }



                params.put("es_orgnl_transfee", String.valueOf(trans_amount));

                if (c_amount.getText().toString().isEmpty()) {
                    params.put("concession_amount", "0");
                }else {
                    params.put("concession_amount", c_amount.getText().toString());
                }

                if (paid_fine.getText().toString().isEmpty()) {
                    params.put("fine_pay", "0");
                }else {
                    params.put("fine_pay", paid_fine.getText().toString());
                }

                params.put("fine", total_fine.getText().toString());
                params.put("fine_bal", new_fine_balance.getText().toString());



                params.put("classid", classid);

                params.put("payment_mode", selectBankType);

                params.put("bank_name", b_name.getText().toString());
                params.put("acc_no", b_a_no.getText().toString());
                params.put("dd_chk_no", cheque.getText().toString());


                if (edtReason.getText().toString().isEmpty()) {
                    params.put("note", "Done");
                }else {
                    params.put("note", edtReason.getText().toString());
                }

                params.put("todate", todate);
                params.put("fromdate", fromdate);

                params.put("es_months", selectMonth);
                params.put("student_id", regist);

                ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.ADDFEES, apisendmsgCallback, this, params);
            }
        }

    private void commonToast(String s) {
        Toast.makeText(getApplicationContext(),s,Toast.LENGTH_LONG).show();
    }

    ApiHandler.ApiCallback apisendmsgCallback = new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                ErpProgress.cancelProgressBar();
                if (error == null) {
                    try {
                        Toast.makeText(getApplicationContext(),jsonObject.getString("msg"),Toast.LENGTH_LONG).show();
                        openDialog("pay");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    Log.e("error", error.getMessage() + "");
                    // Utils.showSnackBar(error.getMessage(), parent);
                    Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_LONG).show();

                }
            }
        };


    @Override
    public void clickMethod(Fee_Paid_Bean fee_paid_bean) {
        Gson gson = new Gson();
        String prod_data = gson.toJson(fee_paid_bean, Fee_Paid_Bean.class);
        Intent intent = new Intent(PayFeeSActivity.this, FeeDetailsActivity.class);
        intent.putExtra("fee_data", prod_data);
        startActivity(intent);
    }
}