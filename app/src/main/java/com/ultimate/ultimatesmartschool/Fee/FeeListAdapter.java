package com.ultimate.ultimatesmartschool.Fee;

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.github.siyamed.shapeimageview.CircularImageView;
import com.squareup.picasso.Picasso;
import com.ultimate.ultimatesmartschool.BeanModule.User;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.Utils;


import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FeeListAdapter extends RecyclerView.Adapter<FeeListAdapter.ViewHolder> {

    Context context;
    ArrayList<Fee_Paid_Bean> list;
    private ProdMethodCallBack prodMethodCallBack;
    private Animation animation;
    private int view_value;

    public FeeListAdapter(ArrayList<Fee_Paid_Bean> list, Context context, ProdMethodCallBack prodMethodCallBack,int view_value) {
        this.list=list;
        this.context=context;
        this.prodMethodCallBack=prodMethodCallBack;
        this.view_value=view_value;
    }

    public void setMerchantBeans(ArrayList<Fee_Paid_Bean> list) {
        this.list = list;
    }

    public interface ProdMethodCallBack{
        void clickMethod(Fee_Paid_Bean fee_paid_bean);
    }


    @NonNull
    @Override
    public FeeListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.transaction_layout, parent, false);
        ViewHolder viewHolder= new FeeListAdapter.ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull FeeListAdapter.ViewHolder holder, @SuppressLint("RecyclerView") int position) {

        Fee_Paid_Bean data= list.get(position);

        int add=0,sub=0,value=0;

//         add =Integer.parseInt(data.getEs_particularamount()) + Integer.parseInt(data.getEs_prospect()) +
//                Integer.parseInt(data.getEs_exam()) + Integer.parseInt(data.getEs_counsel()) +
//                Integer.parseInt(data.getEs_srf()) + Integer.parseInt(data.getEs_student())
//                +Integer.parseInt(data.getEs_other())+Integer.parseInt(data.getEs_transportfee())+
//                Integer.parseInt(data.getPaid_fine());

        add =Integer.parseInt(data.getEs_particularamount()) +Integer.parseInt(data.getEs_transportfee())+
                Integer.parseInt(data.getPaid_fine());


        // add = Integer.parseInt(data.getEs_particularamount())+Integer.parseInt(data.getEs_transportfee());

         sub=Integer.parseInt(data.getEs_concessionamount());

         value=add-sub;

        Log.i("add", String.valueOf(add));
        Log.i("add", String.valueOf(sub));
        Log.i("add", String.valueOf(value));

        holder.name.setText(data.getName()+"("+data.getS_id()+"-"+data.getClass_name()+")");

        if (view_value==0){
            holder.status.setVisibility(View.VISIBLE);

            String title5 = getColoredSpanned("Status:- ", "#5A5C59");
            String Name5 = "";

            if (data.getPayment_status().equalsIgnoreCase("pending")){
                Name5 = getColoredSpanned("Pending", "#F4D00C");
            }else if (data.getPayment_status().equalsIgnoreCase("complete")){
                Name5 = getColoredSpanned("Complete", "#1C8B3B");
            }else {
                Name5 = getColoredSpanned("Cancel", "#F4212C");
            }

            holder.status.setText(Html.fromHtml(title5 + " " + Name5));

            String title = getColoredSpanned("Payment Mode:- ", "#5A5C59");
//            String Name = getColoredSpanned("<b>" +data.getPayment_mode()+"</b>", "#000000");
            String Name = getColoredSpanned(data.getPayment_mode(), "#000000");
            holder.gift.setText(Html.fromHtml(title + " " + Name));

            if (data.getScreenshot()!=null) {
                holder.image_file_lyt.setVisibility(View.VISIBLE);
                Picasso.get().load(data.getScreenshot()).placeholder(context.getResources().getDrawable(R.color.transparent)).into(holder.image_file);
            }else {
                holder.image_file_lyt.setVisibility(View.GONE);
            }

            holder.linearlayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    animation = AnimationUtils.loadAnimation(context, R.anim.btn_blink_animation);
                    holder.image_file.startAnimation(animation);
                    prodMethodCallBack.clickMethod(list.get(position));
                }
            });

        }else {
            holder.status.setVisibility(View.GONE);
            holder.image_file_lyt.setVisibility(View.GONE);

            String title = getColoredSpanned("Payment Mode:- ", "#5A5C59");
            String Name = getColoredSpanned(data.getPayment_mode(), "#f85164");
            holder.gift.setText(Html.fromHtml(title + " " + Name));

            holder.linearlayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    animation = AnimationUtils.loadAnimation(context, R.anim.btn_blink_animation);
                    holder.linearlayout.startAnimation(animation);
                    prodMethodCallBack.clickMethod(list.get(position));
                }
            });
        }

//        if (data.getEs_totalamount() != null) {
            holder.price.setText(value+".00");
//        }else {
//            holder.price.setText("Not Mentioned");
//        }

        if (data.getGender().equalsIgnoreCase("Male")) {
            if (data.getProfile() != null) {
                Utils.progressImg(Constants.getImageBaseURL()+data.getProfile() ,holder.product_img,context);
                // Picasso.with(getActivity()).load(stuList.get(i-1).getProfile()).placeholder(R.drawable.stud).into(dp);
            } else {
                Picasso.get().load(R.drawable.stud).into(holder.product_img);
            }
        }else{
            if (data.getProfile() != null) {
                Utils.progressImg(Constants.getImageBaseURL()+data.getProfile(),holder.product_img,context);
                //  Picasso.with(getActivity()).load(stuList.get(i-1).getProfile()).placeholder(R.drawable.f_student).into(dp);
            } else {
                Picasso.get().load(R.drawable.f_student).into(holder.product_img);
            }
        }

//       if (User.getCurrentUser().getSchoolData().getLogo() != null) {
//            Picasso.with(context).load(User.getCurrentUser().getSchoolData().getLogo()).placeholder(context.getResources().getDrawable(R.drawable.logo)).into(holder.product_img);
//       }
        int bal=0;
        if (data.getBalance_fine().equalsIgnoreCase("0")) {
            bal = Integer.parseInt(data.getFee_bal()) + Integer.parseInt(data.getTotal_fine());
        }else {
            bal = Integer.parseInt(data.getFee_bal()) + Integer.parseInt(data.getBalance_fine());
        }
            holder.balance.setText(bal+".00");
          //  holder.tagline.setText("Payment Mode:- "+data.getPayment_mode());
       if (data.getPaymentdate()!=null) {
           String title = getColoredSpanned("<b>" +"Billing Date:- "+ "</b>", "#5A5C59");
           String Name = getColoredSpanned( Utils.getDateFormated(data.getPaymentdate()), "#5A5C59");
           holder.date.setText(Html.fromHtml(title + " " + Name));
       }


            //   name2.setText(user.getFamily2name());

            // holder.gift.setTextColor(Color.parseColor("#f85164"));
            //holder.gift.setText("Payment Mode:-"+data.getPayment_mode());


    }

    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }
    @Override
    public int getItemCount() {
        if (list != null) {
            return list.size();
        }
        return 0;

    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.product_img)
        CircularImageView product_img;
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.gift)
        TextView gift;
        @BindView(R.id.date)
        TextView date;
        @BindView(R.id.balance)
        TextView balance;
        @BindView(R.id.image_file_lyt)
        RelativeLayout image_file_lyt;
        @BindView(R.id.image_file)
        ImageView image_file;
        @BindView(R.id.status)
        TextView status;
        @BindView(R.id.price)
        TextView price;
        @BindView(R.id.linearlayout)
        RelativeLayout linearlayout;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}
