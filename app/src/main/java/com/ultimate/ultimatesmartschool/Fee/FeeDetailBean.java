package com.ultimate.ultimatesmartschool.Fee;

import com.ultimate.ultimatesmartschool.BeanModule.ClassBean;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class FeeDetailBean {

    public ClassBean class_data;
    public ArrayList<FeeBean> fee;

    public ClassBean getClass_data() {
        return class_data;
    }

    public void setClass_data(ClassBean class_data) {
        this.class_data = class_data;
    }

    public ArrayList<FeeBean> getFee() {
        return fee;
    }

    public void setFee(ArrayList<FeeBean> fee) {
        this.fee = fee;
    }


    public static ArrayList<FeeDetailBean> pareFeeDetail(JSONArray arrayObj) {
        ArrayList list = new ArrayList<FeeDetailBean>();
        try {
            for (int i = 0; i < arrayObj.length(); i++) {
                FeeDetailBean p = parseFeeDetail(arrayObj.getJSONObject(i));
                if (p != null) {
                    list.add(p);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    private static FeeDetailBean parseFeeDetail(JSONObject jsonObject) {
        FeeDetailBean data = new FeeDetailBean();
        try {
            if (jsonObject.has("class_data")) {
                data.setClass_data(ClassBean.parseClassObject(jsonObject.getJSONObject("class_data")));
            }
            if (jsonObject.has("fee")) {
                data.setFee(FeeBean.parseFee(jsonObject.getJSONArray("fee")));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return data;
    }

    public static class FeeBean {
        /**
         * es_feemasterid : 202
         * fee_cat_id : 23
         * fee_particular : Jan add
         * fee_class : 2
         * fee_amount : 200
         * fee_instalments : 0
         * fee_extra1 :
         * fee_extra2 :
         * fee_fromdate : 2018-04-01
         * fee_todate : 2019-03-31
         * fee_cat_name : monthly
         */

        private String es_feemasterid;
        private String fee_cat_id;
        private String fee_particular;
        private String fee_class;
        private String fee_amount;
        private String fee_instalments;
        private String fee_extra1;
        private String fee_extra2;
        private String fee_fromdate;
        private String fee_todate;
        private String fee_cat_name;
        private String fee_type;
        private String es_fine;

        public String getEs_fine() {
            return es_fine;
        }

        public void setEs_fine(String es_fine) {
            this.es_fine = es_fine;
        }

        public String getEs_duedate() {
            return es_duedate;
        }

        public void setEs_duedate(String es_duedate) {
            this.es_duedate = es_duedate;
        }

        private String es_duedate;

        public String getFee_type() {
            return fee_type;
        }

        public void setFee_type(String fee_type) {
            this.fee_type = fee_type;
        }


        public String getEs_feemasterid() {
            return es_feemasterid;
        }

        public void setEs_feemasterid(String es_feemasterid) {
            this.es_feemasterid = es_feemasterid;
        }

        public String getFee_cat_id() {
            return fee_cat_id;
        }

        public void setFee_cat_id(String fee_cat_id) {
            this.fee_cat_id = fee_cat_id;
        }

        public String getFee_particular() {
            return fee_particular;
        }

        public void setFee_particular(String fee_particular) {
            this.fee_particular = fee_particular;
        }

        public String getFee_class() {
            return fee_class;
        }

        public void setFee_class(String fee_class) {
            this.fee_class = fee_class;
        }

        public String getFee_amount() {
            return fee_amount;
        }

        public void setFee_amount(String fee_amount) {
            this.fee_amount = fee_amount;
        }

        public String getFee_instalments() {
            return fee_instalments;
        }

        public void setFee_instalments(String fee_instalments) {
            this.fee_instalments = fee_instalments;
        }

        public String getFee_extra1() {
            return fee_extra1;
        }

        public void setFee_extra1(String fee_extra1) {
            this.fee_extra1 = fee_extra1;
        }

        public String getFee_extra2() {
            return fee_extra2;
        }

        public void setFee_extra2(String fee_extra2) {
            this.fee_extra2 = fee_extra2;
        }

        public String getFee_fromdate() {
            return fee_fromdate;
        }

        public void setFee_fromdate(String fee_fromdate) {
            this.fee_fromdate = fee_fromdate;
        }

        public String getFee_todate() {
            return fee_todate;
        }

        public void setFee_todate(String fee_todate) {
            this.fee_todate = fee_todate;
        }

        public String getFee_cat_name() {
            return fee_cat_name;
        }

        public void setFee_cat_name(String fee_cat_name) {
            this.fee_cat_name = fee_cat_name;
        }

        public static ArrayList<FeeBean> parseFee(JSONArray arrayObj) {
            ArrayList list = new ArrayList<FeeBean>();
            try {
                for (int i = 0; i < arrayObj.length(); i++) {
                    FeeBean p = parseFeeObj(arrayObj.getJSONObject(i));
                    if (p != null) {
                        list.add(p);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return list;
        }

        private static FeeBean parseFeeObj(JSONObject jsonObject) {
            FeeBean data = new FeeBean();
            try {
                if (jsonObject.has("es_feemasterid") && !jsonObject.getString("es_feemasterid").isEmpty() && !jsonObject.getString("es_feemasterid").equalsIgnoreCase("null")) {
                    data.setEs_feemasterid(jsonObject.getString("es_feemasterid"));
                }
                if (jsonObject.has("fee_particular") && !jsonObject.getString("fee_particular").isEmpty() && !jsonObject.getString("fee_particular").equalsIgnoreCase("null")) {
                    data.setFee_particular(jsonObject.getString("fee_particular"));
                }
                if (jsonObject.has("fee_amount") && !jsonObject.getString("fee_amount").isEmpty() && !jsonObject.getString("fee_amount").equalsIgnoreCase("null")) {
                    data.setFee_amount(jsonObject.getString("fee_amount"));
                }
                if (jsonObject.has("fee_cat_name") && !jsonObject.getString("fee_cat_name").isEmpty() && !jsonObject.getString("fee_cat_name").equalsIgnoreCase("null")) {
                    data.setFee_cat_name(jsonObject.getString("fee_cat_name"));
                }
                if (jsonObject.has("fee_type") && !jsonObject.getString("fee_type").isEmpty() && !jsonObject.getString("fee_type").equalsIgnoreCase("null")) {
                    data.setFee_type(jsonObject.getString("fee_type"));
                }
                if (jsonObject.has("es_duedate") && !jsonObject.getString("es_duedate").isEmpty() && !jsonObject.getString("fee_type").equalsIgnoreCase("null")) {
                    data.setEs_duedate(jsonObject.getString("es_duedate"));
                }
                if (jsonObject.has("es_fine") && !jsonObject.getString("es_fine").isEmpty() && !jsonObject.getString("es_fine").equalsIgnoreCase("null")) {
                    data.setEs_fine(jsonObject.getString("es_fine"));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return data;
        }
    }
}
