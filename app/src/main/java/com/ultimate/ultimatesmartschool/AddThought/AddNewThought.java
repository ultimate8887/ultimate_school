package com.ultimate.ultimatesmartschool.AddThought;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.ultimate.ultimatesmartschool.BeanModule.User;
import com.ultimate.ultimatesmartschool.Login.LoginActivity;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AddNewThought extends AppCompatActivity {
    @BindView(R.id.edtAddmessageadmn)
    EditText edtAddmessageadmn;
    @BindView(R.id.parent)
    RelativeLayout parent;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.imgBack)
    ImageView back;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_thought);
        ButterKnife.bind(this);
        txtTitle.setText("Add New Thought");
    }

    @OnClick(R.id.imgBack)
    public void onback() {
        finish();
    }
    @OnClick(R.id.send_messageadmin)
    public void send_message(){
        if(checkValid()) {
            ErpProgress.showProgressBar(this, "Please wait...");
            HashMap<String, String> params = new HashMap<>();
            params.put("msg", edtAddmessageadmn.getText().toString());

            ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.ADDNEWTHOUGHT, apisendmsgCallback, this, params);

        }
    }
    ApiHandler.ApiCallback apisendmsgCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            if (error == null) {
                try {
                    Toast.makeText(getApplicationContext(),jsonObject.getString("msg"),Toast.LENGTH_LONG).show();
                    Utils.showSnackBar(jsonObject.getString("msg"), parent);
                    finish();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("error", error.getMessage() + "");
                Utils.showSnackBar(error.getMessage(), parent);
                // Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_LONG).show();
                if (error.getStatusCode() == 405) {
                    User.logout();
                    startActivity(new Intent(AddNewThought.this, LoginActivity.class));
                    finish();
                }
            }
        }
    };

    private boolean checkValid() {

        boolean valid = true;
        String errorMsg = null;
        if(edtAddmessageadmn.getText().toString().trim().length()<=0){
            valid=false;
            errorMsg="Please enter the Messages";
        }

        if (!valid) {
            Utils.showSnackBar(errorMsg, parent);
        }
        return valid;
    }
}