package com.ultimate.ultimatesmartschool.AddThought;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.google.gson.Gson;
import com.ultimate.ultimatesmartschool.BeanModule.User;
import com.ultimate.ultimatesmartschool.Login.LoginActivity;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Transport.TransBeanMod.DriverListBean;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.CommonProgress;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AddThought extends AppCompatActivity {
@BindView(R.id.edtAddmessageadmn)
    EditText edtAddmessageadmn;
    @BindView(R.id.parent)
    RelativeLayout parent;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.imgBack)
    ImageView back;
    Thought_bean thought_data;
    CommonProgress commonProgress;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_thought);
        ButterKnife.bind(this);
        commonProgress =new CommonProgress(this);
        txtTitle.setText("Update Thought");
        Bundle intent_value = getIntent().getExtras();
        if (intent_value == null) {
            return;
        }
        if (intent_value.containsKey("evntdata")) {
            Gson gson = new Gson();
            thought_data = gson.fromJson(intent_value.getString("evntdata"), Thought_bean.class);

        } else {
            return;
        }
        edtAddmessageadmn.setText(thought_data.getMessage());
    }
    @OnClick(R.id.imgBack)
    public void onback() {
        finish();
    }
    @OnClick(R.id.send_messageadmin)
    public void send_message(){
        if(checkValid()) {
           commonProgress.show();
            HashMap<String, String> params = new HashMap<>();
            params.put("msg", edtAddmessageadmn.getText().toString());
            params.put("thought_id", thought_data.getId());
            ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.ADDTHOUGHT, apisendmsgCallback, this, params);

        }
    }
    ApiHandler.ApiCallback apisendmsgCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            commonProgress.dismiss();
            if (error == null) {
                try {
                    Toast.makeText(getApplicationContext(),jsonObject.getString("msg"),Toast.LENGTH_LONG).show();
                    Utils.showSnackBar(jsonObject.getString("msg"), parent);
                    finish();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("error", error.getMessage() + "");
                Utils.showSnackBar(error.getMessage(), parent);
                // Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_LONG).show();
                if (error.getStatusCode() == 405) {
                    User.logout();
                    startActivity(new Intent(AddThought.this, LoginActivity.class));
                    finish();
                }
            }
        }
    };

    private boolean checkValid() {

        boolean valid = true;
        String errorMsg = null;
         if(edtAddmessageadmn.getText().toString().trim().length()<=0){
            valid=false;
            errorMsg="Please enter the Messages";
        }

        if (!valid) {
            Utils.showSnackBar(errorMsg, parent);
        }
        return valid;
    }
}
