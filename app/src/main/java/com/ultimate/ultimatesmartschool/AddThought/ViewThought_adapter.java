package com.ultimate.ultimatesmartschool.AddThought;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.ClipboardManager;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;
import com.ultimate.ultimatesmartschool.Gallery.AlbmlistAdapter;
import com.ultimate.ultimatesmartschool.Gallery.Albumlistbean;
import com.ultimate.ultimatesmartschool.NoticeMod.NoticeAdapter;
import com.ultimate.ultimatesmartschool.NoticeMod.NoticeBean;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import java.util.ArrayList;

public class ViewThought_adapter extends RecyclerSwipeAdapter<ViewThought_adapter.Viewholder> {

    ArrayList<Thought_bean> hq_list;
    private final Mycallback mMycallback;
    Context listner;

    public ViewThought_adapter(ArrayList<Thought_bean> hq_list, Context listner, Mycallback mMycallback) {
        this.hq_list = hq_list;
        this.listner = listner;
        this.mMycallback = mMycallback;
    }


    @Override
    public ViewThought_adapter.Viewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.albm_ada_lay, parent, false);
        ViewThought_adapter.Viewholder viewholder = new ViewThought_adapter.Viewholder(view);
        return viewholder;
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(final ViewThought_adapter.Viewholder holder, final int position) {
        //holder.albm_id.setText("Id: "+hq_list.get(position).getId());

        if (hq_list.get(position).getStatus().equalsIgnoreCase("active")){
            holder.status.setText("Currently Active");
            holder.createdon.setTextColor(ContextCompat.getColor(listner, R.color.present));
            holder.albm_name.setTextColor(ContextCompat.getColor(listner, R.color.present));
            holder.status.setTextColor(ContextCompat.getColor(listner, R.color.present));
            // holder.status.setTextColor((R.color.present));
        }else {
            holder.status.setText("Inactive");
            holder.createdon.setTextColor(ContextCompat.getColor(listner, R.color.dark_grey));
            holder.albm_name.setTextColor(ContextCompat.getColor(listner, R.color.light_black));
            holder.status.setTextColor(ContextCompat.getColor(listner, R.color.grey));
            // holder.status.setTextColor((R.color.grey));
        }

        holder.albm_name.setText(hq_list.get(position).getMessage());
        holder.createdon.setText(Utils.getDateTimeFormatedWithAMPM(hq_list.get(position).getDate()));
        holder.albm_name.setEnabled(false);



        if (hq_list.get(position).getId() != null) {
            String title = getColoredSpanned("ID: ", "#000000");
            String Name = getColoredSpanned(hq_list.get(position).getId(), "#5A5C59");
            holder.albm_id.setText(Html.fromHtml(title + " " + Name));
        }



//        holder.trash.setVisibility(View.GONE);

        holder.update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (hq_list.get(position).getStatus().equalsIgnoreCase("active")){
                    Toast.makeText(listner,"Unable to delete active thought",Toast.LENGTH_SHORT).show();

                }else {
                    AlertDialog.Builder builder1 = new AlertDialog.Builder(listner);
                    builder1.setMessage("Do you want to delete? ");
                    builder1.setCancelable(false);
                    builder1.setPositiveButton(
                            "Yes", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {

                                    if (mMycallback != null) {
                                        mMycallback.onDelecallback(hq_list.get(position));

                                    }
                                    // mAdapterCallback.deleteNotification(notificationList.get(postion));
                                }
                            });
                    builder1.setNegativeButton(
                            "No", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();
                                }
                            });
                    AlertDialog alert11 = builder1.create();
                    alert11.show();
                }
            }

        });

        holder.copy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Animation animation;
                animation = AnimationUtils.loadAnimation(listner, R.anim.btn_blink_animation);
                holder.copy.startAnimation(animation);
                ClipboardManager cm = (ClipboardManager)listner.getSystemService(Context.CLIPBOARD_SERVICE);
                cm.setText(holder.albm_name.getText().toString());
                Toast.makeText(listner, "Text copied to clipboard", Toast.LENGTH_SHORT).show();
            }
        });

        holder.trash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder1 = new AlertDialog.Builder(listner);
                builder1.setMessage("Do you want to use this thought again? ");
                builder1.setCancelable(false);
                builder1.setPositiveButton(
                        "Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                                if (mMycallback != null) {
                                    mMycallback.onUseThisThought(hq_list.get(position));

                                }
                                // mAdapterCallback.deleteNotification(notificationList.get(postion));
                            }
                        });
                builder1.setNegativeButton(
                        "No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        });
                AlertDialog alert11 = builder1.create();
                alert11.show();
            }
        });
    }

    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }

    @Override
    public int getItemCount() {
        return hq_list.size();
    }


    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe;
    }

    public void setalbmList(ArrayList<Thought_bean> hq_list) {
        this.hq_list = hq_list;
    }



    public class Viewholder extends RecyclerView.ViewHolder {
        public TextView status,albm_id;
        public EditText albm_name;
        ImageView copy;
        public TextView createdon;
        public ImageView iv2, trash, update;
        SwipeLayout swipeLayout;
        Button usethisthought;
        public Viewholder(View itemView) {
            super(itemView);
            usethisthought=itemView.findViewById(R.id.usethisthought);
            albm_id = itemView.findViewById(R.id.albm_id);
            status = itemView.findViewById(R.id.status);
            albm_name = itemView.findViewById(R.id.albm_name);
            createdon = itemView.findViewById(R.id.createdon);
//            iv2 = itemView.findViewById(R.id.imagefoto);
            trash = (ImageView) itemView.findViewById(R.id.trash);
           update = (ImageView) itemView.findViewById(R.id.update);
             copy = (ImageView) itemView.findViewById(R.id.copy);
        }
    }
    public interface Mycallback {
        public void onMethodCallback(Thought_bean obj);
        public void onDelecallback(Thought_bean obj);
        public void onUseThisThought(Thought_bean obj);
    }


}
