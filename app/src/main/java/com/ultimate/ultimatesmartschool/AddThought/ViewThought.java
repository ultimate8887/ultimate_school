package com.ultimate.ultimatesmartschool.AddThought;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetSequence;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.ultimate.ultimatesmartschool.BeanModule.User;
import com.ultimate.ultimatesmartschool.Gallery.ViewGallery;
import com.ultimate.ultimatesmartschool.Login.LoginActivity;
import com.ultimate.ultimatesmartschool.OnlineClass.ViewVideogal;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.CommonProgress;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;
import com.ultimate.ultimatesmartschool.Utility.ErpProgressVert;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ViewThought extends AppCompatActivity implements ViewThought_adapter.Mycallback{
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.contact_support)
    ImageView contact_support;
    @BindView(R.id.imgBack)
    ImageView back;
    @BindView(R.id.parent)
    RelativeLayout parent;
    @BindView(R.id.totalRecord)
    TextView totalRecord;
    EditText edtAddmessageadmn,edtAddmessageadmn1;
    Button btnYes,btnYes1;
    @BindView(R.id.textNorecord)TextView txtNorecord;
    private ViewThought_adapter adapter;
    ArrayList<Thought_bean> eventList = new ArrayList<>();
    private LinearLayoutManager layoutManager;
    @BindView(R.id.button5)
    Button addnewthought;
    SharedPreferences sharedPreferences;
    BottomSheetDialog mBottomSheetDialog,mBottomSheetDialog1;
    CommonProgress commonProgress;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_thought);
        ButterKnife.bind(this);
        commonProgress =new CommonProgress(this);
        recyclerView.setNestedScrollingEnabled(false);
        txtTitle.setText("Today's Thought");
        layoutManager = new LinearLayoutManager(ViewThought.this);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new ViewThought_adapter(eventList, ViewThought.this,this);
        recyclerView.setAdapter(adapter);
        if (!restorePrefData()){
            setShowcaseView();
        }
        addnewthought.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ViewThought.this,AddNewThought.class));
            }
        });
    }

    private void fetchEventlist() {
        commonProgress.show();
        HashMap<String, String> params = new HashMap<String, String>();
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.VIEWTHOUGHT, apiCallback, this, params);

    }
    ApiHandler.ApiCallback apiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            commonProgress.dismiss();
            if (error == null) {
                try {
                    if (eventList != null) {
                        eventList.clear();
                    }
                    JSONArray jsonArray = jsonObject.getJSONArray("gallery_data");
                    eventList = Thought_bean.parseALBMlistarray(jsonArray);
                    if (eventList.size() > 0) {
                        adapter.setalbmList(eventList);
                        adapter.notifyDataSetChanged();
                        totalRecord.setVisibility(View.VISIBLE);
                        totalRecord.setText("Total Entries:- "+String.valueOf(eventList.size()));
                        txtNorecord.setVisibility(View.GONE);
                    } else {
                        adapter.setalbmList(eventList);
                        adapter.notifyDataSetChanged();
                        totalRecord.setText("Total Entries:- 0");
                        txtNorecord.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                totalRecord.setText("Total Entries:- 0");
                txtNorecord.setVisibility(View.VISIBLE);
                eventList.clear();
                adapter.setalbmList(eventList);

                adapter.notifyDataSetChanged();
            }
        }
    };
    @OnClick(R.id.imgBack)
    public void onback() {
        finish();
    }
    @Override
    protected void onResume() {
        super.onResume();
        fetchEventlist();
    }

    private void savePrefData(){
        sharedPreferences= ViewThought.this.getSharedPreferences("boarding_pref4",MODE_PRIVATE);
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putBoolean("IsFirstTimeVisit4",true);
        editor.apply();
    }

    private boolean restorePrefData(){
        sharedPreferences = ViewThought.this.getSharedPreferences("boarding_pref4",MODE_PRIVATE);
        return sharedPreferences.getBoolean("IsFirstTimeVisit4",false);
    }

    private void setShowcaseView() {
        new TapTargetSequence(this)
                .targets(TapTarget.forView(contact_support,"Add Thought Button!","Tap the Add Thought button to add new Thought.")
                        .outerCircleColor(R.color.light)
                        .outerCircleAlpha(0.96f)
                        .targetCircleColor(R.color.white)
                        .titleTextSize(20)
                        .titleTextColor(R.color.white)
                        .descriptionTextSize(14)
                        .descriptionTextColor(R.color.black)
                        .textColor(R.color.black)
                        .textTypeface(Typeface.SANS_SERIF)
                        .dimColor(R.color.black)
                        .drawShadow(true)
                        .cancelable(false)
                        .tintTarget(true)
                        .transparentTarget(true)
                        .targetRadius(60)).listener(new TapTargetSequence.Listener() {
            @Override
            public void onSequenceFinish() {
                // Toast.makeText(getApplicationContext(),"Sequence Finished",Toast.LENGTH_SHORT).show();
                savePrefData();
                openDialog();
               // startActivity(new Intent(ViewThought.this,AddNewThought.class));
            }

            @Override
            public void onSequenceStep(TapTarget lastTarget, boolean targetClicked) {
                // Toast.makeText(getActivity(),"GREAT!",Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSequenceCanceled(TapTarget lastTarget) {

            }
        }).start();


    }

    private void openDialog() {

        mBottomSheetDialog = new BottomSheetDialog(this);
        final View sheetView = getLayoutInflater().inflate(R.layout.add_thought_dialog, null);
        mBottomSheetDialog.setContentView(sheetView);
        mBottomSheetDialog.setCancelable(true);

        edtAddmessageadmn=(EditText) sheetView.findViewById(R.id.edtAddmessageadmn);
        btnYes=(Button) sheetView.findViewById(R.id.btnYes);
//        spinnerGroup=(Spinner) sheetView.findViewById(R.id.spinnerGroup);

        Button btnYes= (Button) sheetView.findViewById(R.id.btnYes);
        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (edtAddmessageadmn.getText().toString().trim().length()<=0) {
                    Toast.makeText(getApplicationContext(),"Kindly add thought text",Toast.LENGTH_LONG).show();
                }else {
                    assignData();
                }
            }
        });
        mBottomSheetDialog.show();

    }

    private void assignData() {

           commonProgress.show();
            HashMap<String, String> params = new HashMap<>();
            params.put("msg", edtAddmessageadmn.getText().toString());

            ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.ADDNEWTHOUGHT, apisendmsgCallback, this, params);


    }
    ApiHandler.ApiCallback apisendmsgCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgressVert.cancelProgressBar();
            commonProgress.dismiss();
            if (error == null) {
                try {
                    Toast.makeText(getApplicationContext(),jsonObject.getString("msg"),Toast.LENGTH_LONG).show();
                    //Utils.showSnackBar(jsonObject.getString("msg"), parent);
                    //finish();
                    mBottomSheetDialog.dismiss();
                    fetchEventlist();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("error", error.getMessage() + "");
              //  Utils.showSnackBar(error.getMessage(), parent);
                 Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_LONG).show();
//                if (error.getStatusCode() == 405) {
//                    User.logout();
//                    startActivity(new Intent(ViewThought.this, LoginActivity.class));
//                    finish();
//                }
            }
        }
    };

    @OnClick(R.id.contact_support)
    public void contact_support() {
        openDialog();
    }



    @Override
    public void onMethodCallback(Thought_bean obj) {

    }


    @Override
    public void onDelecallback(Thought_bean eventlist_bean) {
//        Gson gson = new Gson();
//        String eventdata = gson.toJson(obj, Thought_bean.class);
//        Log.e("evntdata", eventdata);
//        Intent intent = new Intent(ViewThought.this,AddThought.class);
//        intent.putExtra("evntdata", eventdata);
//        startActivity(intent);
     //   openDialog1(obj, "update");

        HashMap<String, String> params = new HashMap<>();
        params.put("event_id", eventlist_bean.getId());
        params.put("check", "thought");
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.DELETEGALARY_URL, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                ErpProgress.cancelProgressBar();
                if (error == null) {
                    Toast.makeText(getApplicationContext(),"Deleted successfully",Toast.LENGTH_SHORT).show();
                    int pos = eventList.indexOf(eventlist_bean);
                    eventList.remove(pos);
                    adapter.setalbmList(eventList);
                    adapter.notifyDataSetChanged();
                    totalRecord.setText("Total Entries:- "+String.valueOf(eventList.size()));
                } else {
                    Log.e("error", error.getMessage() + "");
                    Utils.showSnackBar(error.getMessage(), parent);
                    Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_SHORT).show();

                    if (error.getStatusCode() == 405) {
                        User.logout();
                        startActivity(new Intent(ViewThought.this, LoginActivity.class));
                        finish();
                    }
                }
            }
        }, this, params);

    }


    @Override
    public void onUseThisThought(Thought_bean obj) {
//        Gson gson = new Gson();
//        String eventdata = gson.toJson(obj, Thought_bean.class);
//        Log.e("evntdata", eventdata);
//        Intent intent = new Intent(ViewThought.this,AddThought.class);
//        intent.putExtra("evntdata", eventdata);
//        startActivity(intent);
        openDialog1(obj,"use");
    }

    private void openDialog1(Thought_bean obj, String use) {

        mBottomSheetDialog1 = new BottomSheetDialog(this);
        final View sheetView = getLayoutInflater().inflate(R.layout.add_thought_dialog, null);
        mBottomSheetDialog1.setContentView(sheetView);
        mBottomSheetDialog1.setCancelable(true);

        edtAddmessageadmn1=(EditText) sheetView.findViewById(R.id.edtAddmessageadmn);
        TextView txtSetup=(TextView) sheetView.findViewById(R.id.txtSetup);
        edtAddmessageadmn1.setText(obj.getMessage());
        btnYes1=(Button) sheetView.findViewById(R.id.btnYes);
        if (use.equalsIgnoreCase("use")){
            edtAddmessageadmn1.setEnabled(false);
            txtSetup.setText("Use Thought");
            btnYes1.setText("Use Again");
        }else {
            txtSetup.setText("Update Thought");
            btnYes1.setText("Update");
        }

//        spinnerGroup=(Spinner) sheetView.findViewById(R.id.spinnerGroup);

        btnYes1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (edtAddmessageadmn1.getText().toString().trim().length()<=0) {
                    Toast.makeText(getApplicationContext(),"Kindly add thought text",Toast.LENGTH_LONG).show();
                }else {

                if (use.equalsIgnoreCase("use")){
                    updateData(obj,"use");
                }else {

                    updateData(obj,"update");
                }
                }
            }
        });
        mBottomSheetDialog1.show();

    }

    private void updateData(Thought_bean obj, String use) {

            commonProgress.show();
            HashMap<String, String> params = new HashMap<>();
            params.put("msg", edtAddmessageadmn1.getText().toString());
            params.put("thought_id", obj.getId());
            if (use.equalsIgnoreCase("use")){
            params.put("check", "use");
             }else {
             params.put("check", "update");
             }
            ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.ADDTHOUGHT, apisendmsgCallback1, this, params);

    }
    ApiHandler.ApiCallback apisendmsgCallback1 = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgressVert.cancelProgressBar();
            commonProgress.dismiss();
            if (error == null) {
                try {
                    Toast.makeText(getApplicationContext(),jsonObject.getString("msg"),Toast.LENGTH_LONG).show();
                    mBottomSheetDialog1.dismiss();
                    fetchEventlist();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
//                Log.e("error", error.getMessage() + "");
                  Utils.showSnackBar(error.getMessage(), parent);
                 Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_LONG).show();
//                if (error.getStatusCode() == 405) {
//                    User.logout();
//                    startActivity(new Intent(ViewThought.this, LoginActivity.class));
//                    finish();
//                }
            }
        }
    };

}
