package com.ultimate.ultimatesmartschool.ClassTest;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.ultimate.ultimatesmartschool.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TestResultAdapter extends RecyclerView.Adapter<TestResultAdapter.Viewholder> {
    ArrayList<TestResultBean> testresultList;
    Context context;

    public TestResultAdapter(ArrayList<TestResultBean> testresultList, Context context) {
        this.context=context;
        this.testresultList=testresultList;


    }

    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.test_result_layout, parent, false);

        TestResultAdapter.Viewholder viewHolder = new TestResultAdapter.Viewholder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull Viewholder holder, int position) {
        holder.txtsno.setText(String.valueOf(position+1));
        holder.studentid.setText(testresultList.get(position).getStudentid());
        // holder.className.setText("Class : "+testresultList.get(position).getClass_name());
        holder.studentname.setText(testresultList.get(position).getStudentname());
        // holder.subjectname.setText("Subject Name: "+testresultList.get(position).getSub_name());
        //  holder.marks.setText(testresultList.get(position).getMarks());
        // holder.testdate.setText("Test Date: "+Utils.getDateFormated(testresultList.get(position).getTest_date()));

        String attend="N/A";
        if(testresultList.get(position).getMarks() !=null){
            attend =testresultList.get(position).getMarks();
            holder.marks.setBackground(context.getResources().getDrawable(R.drawable.present_border));
            holder.marks.setTextColor(ContextCompat.getColor(context, R.color.present));

        }else {
            attend="N/A";
            holder.marks.setBackground(context.getResources().getDrawable(R.drawable.white_bg));
            holder.marks.setTextColor(ContextCompat.getColor(context, R.color.dark_black));
        }
        holder.marks.setText(attend);
    }

    @Override
    public int getItemCount() {
        return testresultList.size();
    }

    public void settestresultList(ArrayList<TestResultBean> testresultList) {
        this.testresultList = testresultList;
    }

    public class Viewholder extends RecyclerView.ViewHolder {
        @BindView(R.id.txtRoll)
        TextView studentid;
        // @BindView(R.id.textclassName)TextView className;
        @BindView(R.id.txtName)TextView studentname;
        @BindView(R.id.txtsno)TextView txtsno;
        //  @BindView(R.id.subjectname)TextView subjectname;
        @BindView(R.id.txtAttend)TextView marks;

        public Viewholder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}
