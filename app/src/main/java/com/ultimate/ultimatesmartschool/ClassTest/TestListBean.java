package com.ultimate.ultimatesmartschool.ClassTest;

import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.DebugLog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class TestListBean {
    private static String ID = "id";
    private static String SUB_ID = "sub_id";
    private static String SUB_NAME = "sub_name";
    private static String CLASS_ID = "class_id";
    private static String TEST_DATE = "test_date";
    private static String DURATION = "dur";
    private static String MARKS = "marks";
    private static String TOPIC = "topic";
    private static String CLASS_NAME = "classname";
    private static String TEST_FILE = "test_file";
    private static String STATUS = "status";
    private static String EXAM = "exam";


    public String getRowcount() {
        return rowcount;
    }

    public void setRowcount(String rowcount) {
        this.rowcount = rowcount;
    }

    private String rowcount;

    public String getStaff_name() {
        return staff_name;
    }

    public void setStaff_name(String staff_name) {
        this.staff_name = staff_name;
    }

    public String getStaff_image() {
        return staff_image;
    }

    public void setStaff_image(String staff_image) {
        this.staff_image = staff_image;
    }

    public String getPost_name() {
        return post_name;
    }

    public void setPost_name(String post_name) {
        this.post_name = post_name;
    }

    private String staff_name;
    private String staff_image;
    private String post_name;


    /**
     * id : 2
     * sub_id : 1
     * sub_name : ENGLISH
     * class_id : 2
     * test_date : 2018-04-04
     * dur : 40
     * marks : 50
     * topic : fifty
     * classname : NURSERY1
     * test_file : office_admin/documents/test_04042018_100445.jpg
     */

    private String id;
    private String sub_id;
    private String sub_name;
    private String class_id;
    private String test_date;
    private String dur;
    private String marks;
    private String topic;
    private String classname;
//    private String test_file;

    private ArrayList<String>  test_file;

    private String sectionname;
    private String sectionid;

    public String getSectionname() {
        return sectionname;
    }

    public void setSectionname(String sectionname) {
        this.sectionname = sectionname;
    }

    public String getSectionid() {
        return sectionid;
    }

    public void setSectionid(String sectionid) {
        this.sectionid = sectionid;
    }

    /**
     * status : inactive
     */

    private String status;

    /**
     * status : active
     */
    public String getExam() {
        return exam;
    }

    public void setExam(String exam) {
        this.exam = exam;
    }

    private String exam;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    private String title;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSub_id() {
        return sub_id;
    }

    public void setSub_id(String sub_id) {
        this.sub_id = sub_id;
    }

    public String getSub_name() {
        return sub_name;
    }

    public void setSub_name(String sub_name) {
        this.sub_name = sub_name;
    }

    public String getClass_id() {
        return class_id;
    }

    public void setClass_id(String class_id) {
        this.class_id = class_id;
    }

    public String getTest_date() {
        return test_date;
    }

    public void setTest_date(String test_date) {
        this.test_date = test_date;
    }

    public String getDur() {
        return dur;
    }

    public void setDur(String dur) {
        this.dur = dur;
    }

    public String getMarks() {
        return marks;
    }

    public void setMarks(String marks) {
        this.marks = marks;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getClassname() {
        return classname;
    }

    public void setClassname(String classname) {
        this.classname = classname;
    }

    public String getImage_tag() {
        return image_tag;
    }

    public void setImage_tag(String image_tag) {
        this.image_tag = image_tag;
    }

    private String image_tag;

//    public String getTest_file() {
//        return test_file;
//    }
//
//    public void setTest_file(String test_file) {
//        this.test_file = test_file;
//    }

    public static ArrayList<TestListBean> parseTestlistArray(JSONArray jsonArray) {
        ArrayList<TestListBean> list = new ArrayList<TestListBean>();
        try {

            for (int i = 0; i < jsonArray.length(); i++) {
                TestListBean p = parsetestlistObject(jsonArray.getJSONObject(i));
                if (p != null) {
                    list.add(p);
                }
            }
        } catch (Exception e) {
            DebugLog.printLog("exp", e.getMessage());
            e.printStackTrace();
        }
        return list;

    }

    private static TestListBean parsetestlistObject(JSONObject jsonObject) {

        TestListBean msgObj = new TestListBean();
        try {
            if (jsonObject.has(ID) && !jsonObject.getString(ID).isEmpty() && !jsonObject.getString(ID).equalsIgnoreCase("null")) {
                msgObj.setId(jsonObject.getString(ID));
            }

            if (jsonObject.has(SUB_ID) && !jsonObject.getString(SUB_ID).isEmpty() && !jsonObject.getString(SUB_ID).equalsIgnoreCase("null")) {
                msgObj.setSub_id(jsonObject.getString(SUB_ID));
            }
            if (jsonObject.has(SUB_NAME) && !jsonObject.getString(SUB_NAME).isEmpty() && !jsonObject.getString(SUB_NAME).equalsIgnoreCase("null")) {
                msgObj.setSub_name(jsonObject.getString(SUB_NAME));
            }
            if (jsonObject.has(CLASS_ID) && !jsonObject.getString(CLASS_ID).isEmpty() && !jsonObject.getString(CLASS_ID).equalsIgnoreCase("null")) {
                msgObj.setClass_id(jsonObject.getString(CLASS_ID));
            }
            if (jsonObject.has(TEST_DATE) && !jsonObject.getString(TEST_DATE).isEmpty() && !jsonObject.getString(TEST_DATE).equalsIgnoreCase("null")) {
                msgObj.setTest_date(jsonObject.getString(TEST_DATE));
            }

            if (jsonObject.has(DURATION) && !jsonObject.getString(DURATION).isEmpty() && !jsonObject.getString(DURATION).equalsIgnoreCase("null")) {
                msgObj.setDur(jsonObject.getString(DURATION));
            }

            if (jsonObject.has(MARKS) && !jsonObject.getString(MARKS).isEmpty() && !jsonObject.getString(MARKS).equalsIgnoreCase("null")) {
                msgObj.setMarks(jsonObject.getString(MARKS));
            }

            if (jsonObject.has(TOPIC) && !jsonObject.getString(TOPIC).isEmpty() && !jsonObject.getString(TOPIC).equalsIgnoreCase("null")) {
                msgObj.setTopic(jsonObject.getString(TOPIC));
            }
            if (jsonObject.has(CLASS_NAME) && !jsonObject.getString(CLASS_NAME).isEmpty() && !jsonObject.getString(CLASS_NAME).equalsIgnoreCase("null")) {
                msgObj.setClassname(jsonObject.getString(CLASS_NAME));
            }

            if (jsonObject.has(EXAM) && !jsonObject.getString(EXAM).isEmpty() && !jsonObject.getString(EXAM).equalsIgnoreCase("null")) {
                msgObj.setExam(jsonObject.getString(EXAM));
            }

            if (jsonObject.has("title") && !jsonObject.getString("title").isEmpty() && !jsonObject.getString("title").equalsIgnoreCase("null")) {
                msgObj.setTitle(jsonObject.getString("title"));
            }

//            if (jsonObject.has(TEST_FILE) && !jsonObject.getString(TEST_FILE).isEmpty() && !jsonObject.getString(TEST_FILE).equalsIgnoreCase("null")) {
//                msgObj.setTest_file(Constants.getImageBaseURL() + jsonObject.getString(TEST_FILE));
//            }

            if (jsonObject.has(TEST_FILE) && !jsonObject.getString(TEST_FILE).isEmpty() && !jsonObject.getString(TEST_FILE).equalsIgnoreCase("null")) {
                JSONArray img_arr = jsonObject.getJSONArray(TEST_FILE);
                ArrayList<String> img_arrs = new ArrayList<>();
                for(int i=0;i<img_arr.length();i++){
                    img_arrs.add(Constants.getImageBaseURL()+img_arr.getString(i));
                }
                msgObj.setTest_file(img_arrs);
            }

            if (jsonObject.has("image_tag") && !jsonObject.getString("image_tag").isEmpty() && !jsonObject.getString("image_tag").equalsIgnoreCase("null")) {
                msgObj.setImage_tag(jsonObject.getString("image_tag"));
            }

            if (jsonObject.has(STATUS) && !jsonObject.getString(STATUS).isEmpty() && !jsonObject.getString(STATUS).equalsIgnoreCase("null")) {
                msgObj.setStatus(jsonObject.getString(STATUS));
            }

            if (jsonObject.has("staff_name") && !jsonObject.getString("staff_name").isEmpty() && !jsonObject.getString("staff_name").equalsIgnoreCase("null")) {
                msgObj.setStaff_name(jsonObject.getString("staff_name"));
            }
            if (jsonObject.has("staff_image") && !jsonObject.getString("staff_image").isEmpty() && !jsonObject.getString("staff_image").equalsIgnoreCase("null")) {
                msgObj.setStaff_image(Constants.getImageBaseURL() + jsonObject.getString("staff_image"));
            }
            if (jsonObject.has("post_name") && !jsonObject.getString("post_name").isEmpty() && !jsonObject.getString("post_name").equalsIgnoreCase("null")) {
                msgObj.setPost_name(jsonObject.getString("post_name"));
            }
            if (jsonObject.has("rowcount") && !jsonObject.getString("rowcount").isEmpty() && !jsonObject.getString("rowcount").equalsIgnoreCase("null")) {
                msgObj.setRowcount(jsonObject.getString("rowcount"));
            }

            if (jsonObject.has("sectionname") && !jsonObject.getString("sectionname").isEmpty() && !jsonObject.getString("sectionname").equalsIgnoreCase("null")) {
                msgObj.setSectionname(jsonObject.getString("sectionname"));
            }

            if (jsonObject.has("sectionid") && !jsonObject.getString("sectionid").isEmpty() && !jsonObject.getString("sectionid").equalsIgnoreCase("null")) {
                msgObj.setSectionid(jsonObject.getString("sectionid"));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return msgObj;


    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ArrayList<String> getTest_file() {
        return test_file;
    }

    public void setTest_file(ArrayList<String> test_file) {
        this.test_file = test_file;
    }
}
