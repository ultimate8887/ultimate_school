package com.ultimate.ultimatesmartschool.ClassTest;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetSequence;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.CommonProgress;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TestResultGDS extends AppCompatActivity {
    String test_id, topic, subject, date, classss,section_id,sectionname,staff_name;
    @BindView(R.id.tstrsltreclrvw)
    RecyclerView recyclerView;
    @BindView(R.id.parent)
    RelativeLayout parent;
    @BindView(R.id.txttopicname)
    TextView topicname;
    @BindView(R.id.txtsubjectname)
    TextView subjectname;
    @BindView(R.id.txtdates)
    TextView datename;
    SharedPreferences sharedPreferences;
    ArrayList<TestResultBean> testresultList = new ArrayList<>();
    TestResultAdapter adapter;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.totalRecord)
    TextView totalRecord;
    CommonProgress commonProgress;
    @BindView(R.id.export)
    FloatingActionButton export;
    String tag="",exam="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_result_g_d_s);
        ButterKnife.bind(this);
        commonProgress=new CommonProgress(this);

        if (getIntent().getExtras() != null) {
            tag = getIntent().getExtras().getString("name");
            exam=getIntent().getExtras().getString("exam");
            test_id = getIntent().getExtras().getString("test_id");
            staff_name = getIntent().getExtras().getString("staff_name");
            topic = getIntent().getExtras().getString("topic");
            subject = getIntent().getExtras().getString("subName");
            date = getIntent().getExtras().getString("date");
            classss = getIntent().getExtras().getString("classname");
            section_id = getIntent().getExtras().getString("section_id");
            sectionname = getIntent().getExtras().getString("sectionname");

            if (tag.equalsIgnoreCase("sea")) {
                topicname.setText(exam);
                txtTitle.setText("SEA Result");
                // stulayout.setVisibility(View.GONE);
            } else {
                txtTitle.setText("Test Result");
                topicname.setText(topic);
                //  stulayout.setVisibility(View.VISIBLE);
            }
        }

      //  txtTitle.setText("Test Result");

      //  topicname.setText(topic);
        subjectname.setText(subject+" ("+classss+"-"+""+sectionname+")");
        datename.setText(Utils.getDateFormated(date));
        recyclerView.setLayoutManager(new LinearLayoutManager(TestResultGDS.this));
        adapter = new TestResultAdapter(testresultList, TestResultGDS.this);
        recyclerView.setAdapter(adapter);
        fetchtestresult();
    }

    private void fetchtestresult() {
        commonProgress.show();
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("test_id", test_id);
        params.put("type", tag);
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.TESTRESULTLISTGDS, testresultCallback, this, params);
    }

    ApiHandler.ApiCallback testresultCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            commonProgress.dismiss();
            if (error == null) {
                try {
                    if (testresultList != null) {
                        testresultList.clear();
                    }
                    JSONArray jsonArray = jsonObject.getJSONArray("testresult_data");
                    testresultList = TestResultBean.parsetestrsltArray(jsonArray);
                    if (testresultList.size() > 0) {
                        adapter.settestresultList(testresultList);
                        adapter.notifyDataSetChanged();
                        export.setVisibility(View.VISIBLE);
                        totalRecord.setText("Total Entries:- "+String.valueOf(testresultList.size()));
                        if (!restorePrefDataP()){
                            setShowcaseViewP();
                        }
                    } else {
                        adapter.settestresultList(testresultList);
                        adapter.notifyDataSetChanged();
                        totalRecord.setText("Total Entries:- 0");
                        export.setVisibility(View.GONE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                totalRecord.setText("Total Entries:- 0");
                export.setVisibility(View.GONE);
                testresultList.clear();
                adapter.settestresultList(testresultList);
                adapter.notifyDataSetChanged();
                Utils.showSnackBar(error.getMessage(), parent);
            }
        }
    };
    private void setShowcaseViewP() {
        new TapTargetSequence(this)
                .targets(TapTarget.forView(export,"Export Excel!","Tap the Export button to export Test report.")
                .outerCircleColor(R.color.light)
                .outerCircleAlpha(0.96f)
                .targetCircleColor(R.color.white)
                .titleTextSize(20)
                .titleTextColor(R.color.white)
                .descriptionTextSize(14)
                .descriptionTextColor(R.color.black)
                .textColor(R.color.black)
                .textTypeface(Typeface.SANS_SERIF)
                .dimColor(R.color.black)
                .drawShadow(true)
                .cancelable(false)
                .tintTarget(true)
                .transparentTarget(true)
                .targetRadius(40)).listener(new TapTargetSequence.Listener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onSequenceFinish() {
                // Toast.makeText(getApplicationContext(),"Sequence Finished",Toast.LENGTH_SHORT).show();
                savePrefDataP();

            }

            @Override
            public void onSequenceStep(TapTarget lastTarget, boolean targetClicked) {
                // Toast.makeText(getActivity(),"GREAT!",Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSequenceCanceled(TapTarget lastTarget) {

            }
        }).start();
    }

    private void savePrefDataP(){
        sharedPreferences=this.getSharedPreferences("boarding_pref_view_export_up",MODE_PRIVATE);
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putBoolean("IsFirstTimeVisit_view_export_up",true);
        editor.apply();
    }

    private boolean restorePrefDataP(){
        sharedPreferences = this.getSharedPreferences("boarding_pref_view_export_up",MODE_PRIVATE);
        return sharedPreferences.getBoolean("IsFirstTimeVisit_view_export_up",false);
    }




    @SuppressLint("RestrictedApi")
    @OnClick(R.id.export)
    public void submit() {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.S) {
            importData();
        } else {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {

                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    Toast.makeText(getApplicationContext(), "Please provide storage permission from app settings", Toast.LENGTH_LONG).show();
                } else {

                    ActivityCompat.requestPermissions(this,
                            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            1);
                }
            } else {
                importData();

            }
        }
    }

    @SuppressLint("RestrictedApi")
    private void importData() {
        // File filePath = new File(Environment.getExternalStorageDirectory() + "/Demo.xls");
        Workbook wb = new HSSFWorkbook();

        Cell cell = null;

        String ndate="",sub_staff="",sub_date="";
        ndate= Utils.getDateOnlyNEW(date)+","+Utils.getMonthFormated(date);
        sub_date=ndate.substring(0,3);



        if (tag.equalsIgnoreCase("sea")){

//        if (className.equalsIgnoreCase("")){
            sub_staff=classss+"-"+sectionname+"("+subject+")";
//        }else {
//            sub_staff=className+"_"+sectionname;
//        }

            Sheet sheet = null;
            sheet = wb.createSheet(sub_staff+", "+Utils.getDateFormated(date)+" SEA Marks Report");
            //Now column and row
            Row row = sheet.createRow(0);

            cell = row.createCell(0);
            cell.setCellValue("S No");

            cell = row.createCell(1);
            cell.setCellValue("Roll No");

            cell = row.createCell(2);
            cell.setCellValue("Class/Section");

            cell = row.createCell(3);
            cell.setCellValue("Student Name");

            cell = row.createCell(4);
            cell.setCellValue("Subject");

            cell = row.createCell(5);
            cell.setCellValue("Marks");

            cell = row.createCell(6);
            cell.setCellValue("Teacher Name");


            cell = row.createCell(7);
            cell.setCellValue("Date");

            cell = row.createCell(8);
            cell.setCellValue("Examination");


            //column width
            sheet.setColumnWidth(0, (20 * 100));
            sheet.setColumnWidth(1, (20 * 150));
            sheet.setColumnWidth(2, (30 * 180));
            sheet.setColumnWidth(3, (30 * 220));
            sheet.setColumnWidth(4, (30 * 200));
            sheet.setColumnWidth(5, (20 * 200));
            sheet.setColumnWidth(6, (30 * 200));
            sheet.setColumnWidth(7, (30 * 200));
            sheet.setColumnWidth(8, (30 * 250));

            for (int i = 0; i < testresultList.size(); i++) {

                Row row1 = sheet.createRow(i + 1);

                cell = row1.createCell(0);
                cell.setCellValue(i+1);

                cell = row1.createCell(1);
                cell.setCellValue(testresultList.get(i).getStudentid());

                cell = row1.createCell(2);
                cell.setCellValue(testresultList.get(i).getClass_name()+" ("+sectionname+")");

                cell = row1.createCell(3);
                cell.setCellValue((testresultList.get(i).getStudentname()));
                //  cell.setCellStyle(cellStyle);

                cell = row1.createCell(4);
                cell.setCellValue(testresultList.get(i).getSub_name());

                String attend="";
                if (testresultList.get(i).getMarks()!=null) {
                    attend = testresultList.get(i).getMarks();
                }else{
                    attend="N/A";
                }

                cell = row1.createCell(5);
                cell.setCellValue(attend);


                cell = row1.createCell(6);
                cell.setCellValue(staff_name);

                cell = row1.createCell(7);
                cell.setCellValue(ndate);

                cell = row1.createCell(8);
                cell.setCellValue(exam);

                sheet.setColumnWidth(0, (20 * 100));
                sheet.setColumnWidth(1, (20 * 150));
                sheet.setColumnWidth(2, (30 * 180));
                sheet.setColumnWidth(3, (30 * 220));
                sheet.setColumnWidth(4, (30 * 200));
                sheet.setColumnWidth(5, (20 * 200));
                sheet.setColumnWidth(6, (30 * 200));
                sheet.setColumnWidth(7, (30 * 200));
                sheet.setColumnWidth(8, (30 * 250));


            }
            String fileName;
            if (sub_staff.equalsIgnoreCase("")){
                fileName = sub_date+"_" +System.currentTimeMillis() + ".xls";
            }else {
                fileName = sub_staff+"_"+Utils.getDateOnlyNEW(date)+"_"+Utils.getMonthFormated(date)+"_" + System.currentTimeMillis() + ".xls";
            }

            // File file = new File(getActivity().getExternalFilesDir(null), fileName);

            File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS), fileName);

            FileOutputStream fileOutputStream = null;

            try {
                fileOutputStream = new FileOutputStream(file);
                wb.write(fileOutputStream);
                Utils.openSuccessDialog("Success!","Export file in "+file,"",this);
            } catch (IOException e) {
                Utils.openErrorDialog("Error writing Exception: "+ e,this);
            } catch (Exception e) {
                Utils.openErrorDialog("Failed to save file due to Exception: "+ e,this);
            } finally {
                try {
                    if (null != fileOutputStream) {
                        fileOutputStream.close();
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

        }else {

//        if (className.equalsIgnoreCase("")){
            sub_staff=classss+"-"+sectionname+"("+subject+")";
//        }else {
//            sub_staff=className+"_"+sectionname;
//        }

            Sheet sheet = null;
            sheet = wb.createSheet(sub_staff+", "+Utils.getDateFormated(date)+" Test Marks Report");
            //Now column and row
            Row row = sheet.createRow(0);

            cell = row.createCell(0);
            cell.setCellValue("S No");

            cell = row.createCell(1);
            cell.setCellValue("Roll No");

            cell = row.createCell(2);
            cell.setCellValue("Class/Section");

            cell = row.createCell(3);
            cell.setCellValue("Student Name");

            cell = row.createCell(4);
            cell.setCellValue("Subject");

            cell = row.createCell(5);
            cell.setCellValue("Marks");

            cell = row.createCell(6);
            cell.setCellValue("Teacher Name");


            cell = row.createCell(7);
            cell.setCellValue("Date");


            //column width
            sheet.setColumnWidth(0, (20 * 100));
            sheet.setColumnWidth(1, (20 * 150));
            sheet.setColumnWidth(2, (30 * 180));
            sheet.setColumnWidth(3, (30 * 220));
            sheet.setColumnWidth(4, (30 * 200));
            sheet.setColumnWidth(5, (20 * 200));
            sheet.setColumnWidth(6, (30 * 200));
            sheet.setColumnWidth(7, (30 * 200));

            for (int i = 0; i < testresultList.size(); i++) {

                Row row1 = sheet.createRow(i + 1);

                cell = row1.createCell(0);
                cell.setCellValue(i+1);

                cell = row1.createCell(1);
                cell.setCellValue(testresultList.get(i).getStudentid());

                cell = row1.createCell(2);
                cell.setCellValue(testresultList.get(i).getClass_name()+" ("+sectionname+")");

                cell = row1.createCell(3);
                cell.setCellValue((testresultList.get(i).getStudentname()));
                //  cell.setCellStyle(cellStyle);

                cell = row1.createCell(4);
                cell.setCellValue(testresultList.get(i).getSub_name());

                String attend="";
                if (testresultList.get(i).getMarks()!=null) {
                    attend = testresultList.get(i).getMarks();
                }else{
                    attend="N/A";
                }

                cell = row1.createCell(5);
                cell.setCellValue(attend);


                cell = row1.createCell(6);
                cell.setCellValue(staff_name);

                cell = row1.createCell(7);
                cell.setCellValue(ndate);

                sheet.setColumnWidth(0, (20 * 100));
                sheet.setColumnWidth(1, (20 * 150));
                sheet.setColumnWidth(2, (30 * 180));
                sheet.setColumnWidth(3, (30 * 220));
                sheet.setColumnWidth(4, (30 * 200));
                sheet.setColumnWidth(5, (20 * 200));
                sheet.setColumnWidth(6, (30 * 200));
                sheet.setColumnWidth(7, (30 * 200));


            }
            String fileName;
            if (sub_staff.equalsIgnoreCase("")){
                fileName = sub_date+"_" +System.currentTimeMillis() + ".xls";
            }else {
                fileName = sub_staff+"_"+Utils.getDateOnlyNEW(date)+"_"+Utils.getMonthFormated(date)+"_" + System.currentTimeMillis() + ".xls";
            }

            // File file = new File(getActivity().getExternalFilesDir(null), fileName);

            File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS), fileName);

            FileOutputStream fileOutputStream = null;

            try {
                fileOutputStream = new FileOutputStream(file);
                wb.write(fileOutputStream);
                Utils.openSuccessDialog("Success!","Export file in "+file,"",this);
            } catch (IOException e) {
                Utils.openErrorDialog("Error writing Exception: "+ e,this);
            } catch (Exception e) {
                Utils.openErrorDialog("Failed to save file due to Exception: "+ e,this);
            } finally {
                try {
                    if (null != fileOutputStream) {
                        fileOutputStream.close();
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }



    }

    @SuppressLint("RestrictedApi")
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 1 && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            importData();
        } else {
            Toast.makeText(getApplicationContext(), "Permission Denied", Toast.LENGTH_SHORT).show();
        }
    }


    @OnClick(R.id.imgBack)
    public void backClick() {
        finish();
    }
}
