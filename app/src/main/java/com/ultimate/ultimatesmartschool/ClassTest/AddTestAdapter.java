package com.ultimate.ultimatesmartschool.ClassTest;

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AddTestAdapter extends RecyclerView.Adapter<AddTestAdapter.ViewHolder> {
    Context context;
    private Adaptercall mAdaptercall;
    private ArrayList<TestListBean> MainImageUploadInfoList;
    int value=0;

    public AddTestAdapter(Context context, ArrayList<TestListBean> MainImageUploadInfoList,
                          Adaptercall mAdaptercall,int value) {

        this.MainImageUploadInfoList = MainImageUploadInfoList;
        this.context = context;
        this.mAdaptercall=mAdaptercall;
        this.value=value;
    }
    @Override
    public AddTestAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.test_list_lyt, parent, false);

        AddTestAdapter.ViewHolder viewHolder = new AddTestAdapter.ViewHolder(view);

        return viewHolder;
    }


    @Override
    public void onBindViewHolder(final AddTestAdapter.ViewHolder holder, @SuppressLint("RecyclerView") final int position) {


        //holder.card1.setAnimation(AnimationUtils.loadAnimation(context,R.anim.fade_scale_animation));

        if(MainImageUploadInfoList.get(position).getStatus().equalsIgnoreCase("inactive")){

            holder.upldreslttext.setVisibility(View.INVISIBLE);
            holder.viewresult.setVisibility(View.VISIBLE);
        }else{

            holder.upldreslttext.setVisibility(View.VISIBLE);
            holder.viewresult.setVisibility(View.INVISIBLE);

        }

        holder.viewresult.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAdaptercall.methodcalltestresult(MainImageUploadInfoList.get(position));
            }
        });


        //withfile
        holder.upldreslttext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mAdaptercall.methodcall(MainImageUploadInfoList.get(position));
            }
        });

        if (value==1){
            holder.title.setVisibility(View.VISIBLE);
            holder.title1.setVisibility(View.VISIBLE);

            if (MainImageUploadInfoList.get(position).getId() != null) {
                String title = getColoredSpanned("ID:- ", "#000000");
                String Name = getColoredSpanned("sea-"+MainImageUploadInfoList.get(position).getId(), "#5A5C59");
                holder.id.setText(Html.fromHtml(title + " " + Name));
            }


            holder.Topic.setText(MainImageUploadInfoList.get(position).getExam());

            if (MainImageUploadInfoList.get(position).getTitle() != null) {
                String title = getColoredSpanned("<b>"+"Title:- "+"</b>", "#000000");
                String Name = getColoredSpanned(MainImageUploadInfoList.get(position).getTitle(), "#5A5C59");
                holder.title.setText(Html.fromHtml(title + " " + Name));
            }

            if (MainImageUploadInfoList.get(position).getTopic() != null) {
                String title = getColoredSpanned("<b>"+"Topic:- "+"</b>", "#000000");
                String Name = getColoredSpanned(MainImageUploadInfoList.get(position).getTopic(), "#5A5C59");
                holder.title1.setText(Html.fromHtml(title + " " + Name));
            }

            if (MainImageUploadInfoList.get(position).getMarks() != null) {
                String title = getColoredSpanned("<b>"+"Total marks:- "+"</b>", "#000000");
                String Name = getColoredSpanned(MainImageUploadInfoList.get(position).getMarks(), "#5A5C59");
                holder.Dur.setText(Html.fromHtml(title + " " + Name));
            }

            if (MainImageUploadInfoList.get(position).getDur() != null) {
                String title = getColoredSpanned("<b>"+"Duration: "+"</b>", "#000000");
                String Name = getColoredSpanned(MainImageUploadInfoList.get(position).getDur()+" min.", "#5A5C59");
                holder.maxMarks.setText(Html.fromHtml(title + " " + Name));
            }

            if (MainImageUploadInfoList.get(position).getTest_date() != null) {

            }

            //for Today
            java.util.Calendar calendar = java.util.Calendar.getInstance(TimeZone.getDefault());
            Date setdate = calendar.getTime();
            SimpleDateFormat fmtOut = new SimpleDateFormat("dd MMM, yyyy");
            String dateString = fmtOut.format(setdate);
            //for Yesterday
            DateFormat dateFormat = new SimpleDateFormat("dd MMM, yyyy");
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.DATE, -1);
            String dateString1= dateFormat.format(cal.getTime());

            String check= Utils.getDateFormated(MainImageUploadInfoList.get(position).getTest_date());
            if (check.equalsIgnoreCase(dateString)){
                //  holder.lytLine.setVisibility(View.VISIBLE);
                // holder.txtDT.setText("Today");
                String title = getColoredSpanned("Date:-", "#000000");
                //String Name = getColoredSpanned(messagelist.get(position).getFrom_type(), "#7D7D7D");
                String l_Name = getColoredSpanned("<b>"+"Today"+"</b>", "#e31e25");
                holder.Date.setText(Html.fromHtml(title + " " + l_Name));


                // holder.txtDT.setText("Today at "+time);
                //  holder.vieww.setBackground(mContext.getResources().getDrawable(R.drawable.a_card_lyt2));
            }else if (check.equalsIgnoreCase(dateString1)){

                String title = getColoredSpanned("Date:-", "#000000");
                //String Name = getColoredSpanned(messagelist.get(position).getFrom_type(), "#7D7D7D");
                String l_Name = getColoredSpanned("<b>"+"Yesterday"+"</b>", "#1C8B3B");
                holder.Date.setText(Html.fromHtml(title + " " + l_Name));

                //  holder.lytLine.setVisibility(View.GONE);
                //  holder.vieww.setBackground(mContext.getResources().getDrawable(R.drawable.card_lyt2));
            }else {
                String title = getColoredSpanned("Date:- ", "#000000");
                String Name = getColoredSpanned("<b>"+Utils.getDateFormated(MainImageUploadInfoList.get(position).getTest_date())+"</b>", "#5A5C59");
                holder.Date.setText(Html.fromHtml(title + " " + Name));
            }


//            if(MainImageUploadInfoList.get(position).getTest_file()!= null){
//                Picasso.with(context).load(MainImageUploadInfoList.get(position).getTest_file().get(0)).placeholder(context.getResources().getDrawable(R.color.transparent)).into(holder.pdf);
//                holder.pdf.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        mAdaptercall.methodcallImg(MainImageUploadInfoList.get(position));
//                    }
//                });
//            }else{
//                Picasso.with(context).load(R.drawable.exam_sea).into(holder.pdf);
//
//            }

            if(MainImageUploadInfoList.get(position).getImage_tag().equalsIgnoreCase("images")){
                holder.imageEmpty.setVisibility(View.GONE);
                holder.pdf.setVisibility(View.VISIBLE);
                Picasso.get().load(MainImageUploadInfoList.get(position).getTest_file().get(0)).placeholder(context.getResources().getDrawable(R.drawable.test)).into(holder.pdf);
                holder.pdf.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mAdaptercall.methodcallImg(MainImageUploadInfoList.get(position));
                    }
                });
            }else{
                holder.imageEmpty.setVisibility(View.GONE);
                Picasso.get().load(R.drawable.exam_sea).into(holder.pdf);
            }

        }else{

            holder.title.setVisibility(View.GONE);
            holder.title1.setVisibility(View.GONE);


            if (MainImageUploadInfoList.get(position).getId() != null) {
                String title = getColoredSpanned("ID:- ", "#000000");
                String Name = getColoredSpanned("ts-"+MainImageUploadInfoList.get(position).getId(), "#5A5C59");
                holder.id.setText(Html.fromHtml(title + " " + Name));
            }

            holder.Topic.setText(MainImageUploadInfoList.get(position).getTopic());

            if (MainImageUploadInfoList.get(position).getMarks() != null) {
                String title = getColoredSpanned("<b>"+"Total marks:- "+"</b>", "#000000");
                String Name = getColoredSpanned(MainImageUploadInfoList.get(position).getMarks(), "#5A5C59");
                holder.maxMarks.setText(Html.fromHtml(title + " " + Name));
            }

            if (MainImageUploadInfoList.get(position).getDur() != null) {
                String title = getColoredSpanned("<b>"+"Duration: "+"</b>", "#000000");
                String Name = getColoredSpanned(MainImageUploadInfoList.get(position).getDur()+" min.", "#5A5C59");
                holder.Dur.setText(Html.fromHtml(title + " " + Name));
            }

            if (MainImageUploadInfoList.get(position).getTest_date() != null) {

            }

            //for Today
            java.util.Calendar calendar = java.util.Calendar.getInstance(TimeZone.getDefault());
            Date setdate = calendar.getTime();
            SimpleDateFormat fmtOut = new SimpleDateFormat("dd MMM, yyyy");
            String dateString = fmtOut.format(setdate);
            //for Yesterday
            DateFormat dateFormat = new SimpleDateFormat("dd MMM, yyyy");
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.DATE, -1);
            String dateString1= dateFormat.format(cal.getTime());

            String check= Utils.getDateFormated(MainImageUploadInfoList.get(position).getTest_date());
            if (check.equalsIgnoreCase(dateString)){
                //  holder.lytLine.setVisibility(View.VISIBLE);
                // holder.txtDT.setText("Today");
                String title = getColoredSpanned("TestDate:-", "#000000");
                //String Name = getColoredSpanned(messagelist.get(position).getFrom_type(), "#7D7D7D");
                String l_Name = getColoredSpanned("<b>"+"Today"+"</b>", "#e31e25");
                holder.Date.setText(Html.fromHtml(title + " " + l_Name));


                // holder.txtDT.setText("Today at "+time);
                //  holder.vieww.setBackground(mContext.getResources().getDrawable(R.drawable.a_card_lyt2));
            }else if (check.equalsIgnoreCase(dateString1)){

                String title = getColoredSpanned("TestDate:-", "#000000");
                //String Name = getColoredSpanned(messagelist.get(position).getFrom_type(), "#7D7D7D");
                String l_Name = getColoredSpanned("<b>"+"Yesterday"+"</b>", "#1C8B3B");
                holder.Date.setText(Html.fromHtml(title + " " + l_Name));

                //  holder.lytLine.setVisibility(View.GONE);
                //  holder.vieww.setBackground(mContext.getResources().getDrawable(R.drawable.card_lyt2));
            }else {
                String title = getColoredSpanned("TestDate:- ", "#000000");
                String Name = getColoredSpanned("<b>"+Utils.getDateFormated(MainImageUploadInfoList.get(position).getTest_date())+"</b>", "#5A5C59");
                holder.Date.setText(Html.fromHtml(title + " " + Name));
            }

            if(MainImageUploadInfoList.get(position).getImage_tag().equalsIgnoreCase("images")){
                holder.imageEmpty.setVisibility(View.GONE);
                holder.pdf.setVisibility(View.VISIBLE);
                Picasso.get().load(MainImageUploadInfoList.get(position).getTest_file().get(0)).placeholder(context.getResources().getDrawable(R.drawable.test)).into(holder.pdf);
                holder.pdf.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mAdaptercall.methodcallImg(MainImageUploadInfoList.get(position));
                    }
                });
            }else{
                holder.imageEmpty.setVisibility(View.GONE);
                Picasso.get().load(R.drawable.test).into(holder.pdf);
            }

        }



        if (MainImageUploadInfoList.get(position).getSub_name() != null){
            holder.sub.setText(MainImageUploadInfoList.get(position).getSub_name()+" ("+MainImageUploadInfoList.get(position).getClassname()+"-"
                    +MainImageUploadInfoList.get(position).getSectionname()+")");
        }else{
            holder.sub.setText("Not Mentioned");
        }

        if (!MainImageUploadInfoList.get(position).getStaff_name().equalsIgnoreCase("")) {
            String title = getColoredSpanned("Added by-", "#000000");
            String Name = getColoredSpanned(""+MainImageUploadInfoList.get(position).getStaff_name()+"("+MainImageUploadInfoList.get(position).getPost_name()+")", "#5A5C59");
            holder.addbyid.setText(Html.fromHtml(title + " " + Name));
        }



    }

    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }

    @Override
    public int getItemCount() {

        return MainImageUploadInfoList.size();
    }



    public void settestList(ArrayList<TestListBean> MainImageUploadInfoList) {
        this.MainImageUploadInfoList = MainImageUploadInfoList;
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.addbyid)
        TextView addbyid;
        public TextView Topic,maxMarks,Dur,Date,upldreslttext,upldsuccmsg,fupldsuccmsg,viewresult, sub;
        public ImageView imageEmpty;
        ImageView pdf;
        public ConstraintLayout withfile;
        CardView card1;
        TextView id,title,title1;
        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            sub = (TextView) itemView.findViewById(R.id.subject);
            id = (TextView) itemView.findViewById(R.id.id);
            title = (TextView) itemView.findViewById(R.id.title);
            title1 = (TextView) itemView.findViewById(R.id.title1);
            card1 = (CardView) itemView.findViewById(R.id.card1);
            Topic = (TextView) itemView.findViewById(R.id.TestTopic);
            maxMarks= (TextView) itemView.findViewById(R.id.txtMarks);
            Dur = (TextView) itemView.findViewById(R.id.TestDuration);
            Date = (TextView) itemView.findViewById(R.id.TestDate);
            pdf=(ImageView)itemView.findViewById(R.id.imagefoto);
            imageEmpty=(ImageView)itemView.findViewById(R.id.imageEmpty);
            withfile=(ConstraintLayout)itemView.findViewById(R.id.withfile);
            upldreslttext=(TextView)itemView.findViewById(R.id.upldreslttext);
            viewresult=(TextView)itemView.findViewById(R.id.viewresult);

        }


    }
    public interface Adaptercall{
        public void methodcall(TestListBean obj);
        public void methodcallImg(TestListBean obj);
        public void methodcalltestresult(TestListBean obj);
    }
}
