package com.ultimate.ultimatesmartschool.ClassTest;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.ultimate.ultimatesmartschool.BeanModule.SectionBean;
import com.ultimate.ultimatesmartschool.R;

import java.util.ArrayList;

public class SectionnewAdapterblk extends BaseAdapter {
    private final ArrayList<SectionBean> casteList;
    Context context;
    LayoutInflater inflter;

    public SectionnewAdapterblk(Context applicationContext, ArrayList<SectionBean> casteList) {
        this.context = applicationContext;
        this.casteList = casteList;
        inflter = (LayoutInflater.from(applicationContext));
    }

    @Override
    public int getCount() {
        return casteList.size() + 1;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        view = inflter.inflate(R.layout.spinner_lyt_msg, null);
        TextView label = (TextView) view.findViewById(R.id.txtText);
        if (i == 0) {
            // Default selected Spinner item
            label.setText("Choose Section");
        } else {
            SectionBean classObj = casteList.get(i - 1);
            label.setText(classObj.getSection_name());
        }

        return view;
    }
}
