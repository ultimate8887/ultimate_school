package com.ultimate.ultimatesmartschool.ClassTest;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.Request;

import com.ultimate.ultimatesmartschool.BeanModule.ClassBean;
import com.ultimate.ultimatesmartschool.BeanModule.SectionBean;
import com.ultimate.ultimatesmartschool.BeanModule.SubjectBeancls;
import com.ultimate.ultimatesmartschool.BeanModule.User;
import com.ultimate.ultimatesmartschool.Homework.AdapterOfSubjectList;
import com.ultimate.ultimatesmartschool.Homework.SectionnewAdapter;
import com.ultimate.ultimatesmartschool.Login.LoginActivity;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.StuGatePass.ClassBeans;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.apptik.widget.multiselectspinner.MultiSelectSpinner;

public class AddTest extends AppCompatActivity {
    Spinner spinnerClass;
    Spinner SubjectSpinner;
    @BindView(R.id.testadd)
    RelativeLayout parent;
    ArrayList<ClassBeans> classList = new ArrayList<>();
    ArrayList<SubjectBeancls> subjectList = new ArrayList<>();
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.spinnersection)Spinner spinnersection;
    ArrayList<SectionBean> sectionList = new ArrayList<>();
    ImageView imgback;
    AdapterOfSubjectListblk adaptersub;
    String class_id, class_name, sub_id, sub_name;
    String sectionid="";
    String sectionname;
    TextView  Date, Upload;
    EditText Time, Marks, Topics;
    Button save;
    private Bitmap userBitmap;
    String TestTime, TestTopics, TestMarks;
    String testDate = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_test);
        ButterKnife.bind(this);
        txtTitle.setText("Class Test");

        parent.setAnimation(AnimationUtils.loadAnimation(getApplicationContext(),R.anim.fade_scale_animation));
        Time = (EditText) findViewById(R.id.edtTime);
        Marks = (EditText) findViewById(R.id.edtTotalMarks);
        Topics = (EditText) findViewById(R.id.TestTopics);
        Date = (TextView) findViewById(R.id.Date);
        Upload = (TextView) findViewById(R.id.upload);
        save = (Button) findViewById(R.id.save);
        Upload.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View view) {
                showFileChooser();
            }
        });
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SendData();
            }
        });
        Date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openDatePicker();

            }
        });


        imgback = (ImageView) findViewById(R.id.imgBack);
        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finish();
            }
        });
        spinnerClass = (Spinner) findViewById(R.id.spinner);
        SubjectSpinner = (Spinner) findViewById(R.id.spinner2);


        fetchClass();
        fetchsection();
    }


    private void fetchClass() {
        ErpProgress.showProgressBar(this, "Please wait...");
        HashMap<String, String> params = new HashMap<String, String>();
        ApiHandler.apiHit(Request.Method.GET, Constants.getBaseURL() + Constants.CLASSLIST_URL, classapiCallback, this, params);
    }

    ApiHandler.ApiCallback classapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            if (error == null) {
                try {
                    classList = ClassBeans.parseClassArray(jsonObject.getJSONArray(Constants.CLASSDATA));
                    Messageclass_adpter adapter = new Messageclass_adpter(AddTest.this, classList);
                    spinnerClass.setAdapter(adapter);
                    spinnerClass.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                            if (i == 0) {

                                if (adaptersub != null) {
                                    subjectList.clear();
                                    adaptersub.notifyDataSetChanged();
                                }
                            }
                            if (i > 0) {
                                class_id = classList.get(i - 1).getId();
                                class_name = classList.get(i - 1).getName();
                                sub_id = "";
                                sub_name = "";

                                Log.e("classsid", class_id);

                                getSubjectofClass(classList.get(i - 1));
                            } else {
                                class_id = "";
                            }

                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Utils.showSnackBar(error.getMessage(), parent);
            }
        }
    };
    private void fetchsection() {
        HashMap<String, String> params = new HashMap<String, String>();
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.SECTIONLIST_URL, sectionapiCallback, this, params);
    }

    ApiHandler.ApiCallback sectionapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            if (error == null) {
                try {
                    sectionList = SectionBean.parseCommonArray(jsonObject.getJSONArray("hostelroom_data"));
                    SectionnewAdapterblk adapter = new SectionnewAdapterblk(AddTest.this, sectionList);
                    spinnersection.setAdapter(adapter);
                    spinnersection.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if (i > 0) {
                                sectionid = sectionList.get(i - 1).getSection_id();
                                sectionname = sectionList.get(i - 1).getSection_name();

                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Utils.showSnackBar(error.getMessage(), parent);
                if (error.getStatusCode() == 405) {
                    User.logout();
                    startActivity(new Intent(AddTest.this, LoginActivity.class));
                    finish();
                }
            }
        }
    };

    private void getSubjectofClass(ClassBeans classList) {
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("id", classList.getId());
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.SubjectList, subapiCallback, this, params);

    }

    ApiHandler.ApiCallback subapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            /* */

            if (error == null) {
                try {

                    subjectList =   SubjectBeancls.parseClassArray(jsonObject.getJSONArray("sub_data"));

                    adaptersub = new AdapterOfSubjectListblk(AddTest.this, subjectList);
                    SubjectSpinner.setAdapter(adaptersub);
                    SubjectSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if (i != 0) {
                                sub_name = subjectList.get(i - 1).getName();
                                sub_id = subjectList.get(i - 1).getId();

                            }

                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    };

    public void openDatePicker() {
        java.util.Calendar cal = java.util.Calendar.getInstance(TimeZone.getDefault()); // Get current date
        DatePickerDialog datePicker = new DatePickerDialog(this,
                R.style.MyDatePickerDialogTheme, ondate,
                cal.get(java.util.Calendar.YEAR),
                cal.get(java.util.Calendar.MONTH),
                cal.get(java.util.Calendar.DAY_OF_MONTH));
        datePicker.getDatePicker().setMinDate(new Date().getTime());
        datePicker.setCancelable(false);
        datePicker.show();
    }

    DatePickerDialog.OnDateSetListener ondate = new DatePickerDialog.OnDateSetListener() {


        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

            java.util.Calendar calendar = java.util.Calendar.getInstance(TimeZone.getDefault());
            calendar.set(year, monthOfYear, dayOfMonth);
            Date sub_date = calendar.getTime();
            Calendar value_datePicker = calendar;
            SimpleDateFormat fmtOut = new SimpleDateFormat("dd MMM, yyyy");
            String dateString = fmtOut.format(sub_date);
            Date.setText(dateString);
            SimpleDateFormat fmtOut1 = new SimpleDateFormat("yyyy-MM-dd");
            testDate = fmtOut1.format(sub_date);
        }
    };

    private void SendData() {
        ErpProgress.showProgressBar(this, "Please wait...");
        TestTime = Time.getText().toString();
        TestTopics = Topics.getText().toString();
        TestMarks = Marks.getText().toString();
        if (TestTime.isEmpty() || TestTopics.isEmpty() || TestMarks.isEmpty()) {
            Utils.showSnackBar("Please fill the credential", parent);
        } else {

            HashMap<String, String> params = new HashMap<String, String>();
            params.put("topic", TestTopics);
            params.put("class_id", class_id);
            params.put("class_name", class_name);
            params.put("sub_id", sub_id);
            params.put("sub_name", sub_name);
            params.put("date", testDate);
            params.put("dur", TestTime);
            params.put("marks", TestMarks);
            params.put("sectionid", sectionid);
            params.put("sectionname", sectionname);
            if (userBitmap != null) {
                String encoded = Utils.encodeToBase64(userBitmap, Bitmap.CompressFormat.JPEG, 90);
                encoded = String.format("data:image/jpeg;base64,%s", encoded);
                params.put("file", encoded);
                Log.d("cvbnm", encoded);
            }


            ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.CretateTestgds, createtestapiCallback, this, params);
        }
    }

    ApiHandler.ApiCallback createtestapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            if (error == null) {
                Utils.showSnackBar("Upload Succesfully", parent);
                finish();
            } else {
                Utils.showSnackBar("Fail To Upload", parent);
                Log.e("16955", error.getMessage() + "");
            }
        }
    };

    //method to show file chooser
    @RequiresApi(api = Build.VERSION_CODES.M)
    private void showFileChooser() {
//        if (CropImage.isExplicitCameraPermissionRequired(AddTest.this)) {
//            requestPermissions(new String[]{Manifest.permission.CAMERA}, CropImage.CAMERA_CAPTURE_PERMISSIONS_REQUEST_CODE);
//        } else {
//            if (!Utils.checkPermission(AddTest.this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
//                AddTest.this.requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, Constants.READ_PERMISSION);
//            } else {
//                CropImage.startPickImageActivity(AddTest.this);
//            }
//        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
//                case CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE:
//                    Uri imageUri = CropImage.getPickImageResultUri(AddTest.this, data);
//                    CropImage.activity(imageUri).setCropShape(CropImageView.CropShape.RECTANGLE).setAspectRatio(1, 1)
//                            .start(AddTest.this);
//                    break;
//
//                case CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE:
//                    CropImage.ActivityResult result = CropImage.getActivityResult(data);
//                    Uri resultUri = result.getUri();
//                    Upload.setText("File is selected");
//                    userBitmap = Utils.getBitmapFromUri(AddTest.this, resultUri, 2048);

            }
        }
    }


}
