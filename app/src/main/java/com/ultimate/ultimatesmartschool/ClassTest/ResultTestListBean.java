package com.ultimate.ultimatesmartschool.ClassTest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ResultTestListBean {

    private static String ID = "id";
    private static String NAME = "name";
    private static String EXAM_DATE = "exam_date";
    private static String EXAM_DUR = "exam_dur";
    private static String EXAM_MARKS=" exam_markS";




    /**
     * id : 1
     * name : Yadhvi Rajput
     * class_name : NURSERY1
     * father_name : Sandeep Kumar
     * mobile : 7889267816
     * profile : office_admin/images/st_02012018_080227_download.jpg
     */

    private String id;
    private String name;
    private String class_name;
    private String father_name;
    private String mobile;
    private String profile;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getClass_name() {
        return class_name;
    }

    public void setClass_name(String class_name) {
        this.class_name = class_name;
    }

    public String getFather_name() {
        return father_name;
    }

    public void setFather_name(String father_name) {
        this.father_name = father_name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }

    public static ArrayList<ResultTestListBean> parsestunamemrkArray(JSONArray jsonArray) {
        ArrayList list = new ArrayList<ResultTestListBean>();
        try {
            for (int i = 0; i < jsonArray.length(); i++) {
                ResultTestListBean p = parsestunamemrObject(jsonArray.getJSONObject(i));
                if (p != null) {
                    list.add(p);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        return list;
    }

    private static ResultTestListBean parsestunamemrObject(JSONObject jsonObject) {
        ResultTestListBean casteObj=new ResultTestListBean();
        try {
            if (jsonObject.has(ID)) {
                casteObj.setId(jsonObject.getString(ID));
            }
            if (jsonObject.has(NAME) && !jsonObject.getString(NAME).isEmpty() && !jsonObject.getString(NAME).equalsIgnoreCase("null")) {
                casteObj.setName(jsonObject.getString(NAME));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return casteObj;
    }
    //    String name;
//    String id;
//
    public String getMarks() {
        return marks;
    }
    //
    public void setMarks(String marks) {
        this.marks = marks;
    }
    //
    String marks;
//
//    public ResultTestListBean(String id, String name) {
//        this.id = id;
//        this.name = name;
//    }
//
//    public String getName() {
//        return name;
//    }
//
//    public String getId() {
//        return id;
//    }

}
