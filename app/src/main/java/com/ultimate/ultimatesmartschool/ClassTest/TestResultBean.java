package com.ultimate.ultimatesmartschool.ClassTest;

import com.ultimate.ultimatesmartschool.Utility.DebugLog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class TestResultBean {

    private static String ID="id";
    private static String SUB_ID="sub_id";
    private static String SUB_NAME="sub_name";
    private static String TEST_DATE="test_date";
    private static String CLASS_ID="class_id";
    private static String CLASS_NAME="class_name";
    private static String MARKS="marks";
    private static String STUDENT_ID="studentid";
    private static String STUDENT_NAME="studentname";


    /**
     * id : 96
     * sub_id : 1
     * sub_name : ENGLISH
     * class_id : 2
     * class_name : NURSERY1
     * test_date : 2018-10-25
     * status : active
     * marks : 7
     * studentid : 1
     * studentname : Yadhvi Rajput
     */

    private String id;
    private String sub_id;
    private String sub_name;
    private String class_id;
    private String class_name;
    private String test_date;
    private String status;
    private String marks;
    private String studentid;
    private String studentname;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSub_id() {
        return sub_id;
    }

    public void setSub_id(String sub_id) {
        this.sub_id = sub_id;
    }

    public String getSub_name() {
        return sub_name;
    }

    public void setSub_name(String sub_name) {
        this.sub_name = sub_name;
    }

    public String getClass_id() {
        return class_id;
    }

    public void setClass_id(String class_id) {
        this.class_id = class_id;
    }

    public String getClass_name() {
        return class_name;
    }

    public void setClass_name(String class_name) {
        this.class_name = class_name;
    }

    public String getTest_date() {
        return test_date;
    }

    public void setTest_date(String test_date) {
        this.test_date = test_date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMarks() {
        return marks;
    }

    public void setMarks(String marks) {
        this.marks = marks;
    }

    public String getStudentid() {
        return studentid;
    }

    public void setStudentid(String studentid) {
        this.studentid = studentid;
    }

    public String getStudentname() {
        return studentname;
    }

    public void setStudentname(String studentname) {
        this.studentname = studentname;
    }



    public static ArrayList<TestResultBean> parsetestrsltArray(JSONArray arrayObj) {
        ArrayList<TestResultBean> list = new ArrayList<TestResultBean>();
        try {

            for (int i = 0; i < arrayObj.length(); i++) {
                TestResultBean p = parsetestrsltObject(arrayObj.getJSONObject(i));
                if (p != null) {
                    list.add(p);
                }
            }
        } catch (Exception e) {
            DebugLog.printLog("exp",e.getMessage());
            e.printStackTrace();
        }
        return list;
    }

    public static TestResultBean parsetestrsltObject(JSONObject jsonObject) {
        TestResultBean casteObj = new TestResultBean();
        try {
            if (jsonObject.has(ID)) {
                casteObj.setId(jsonObject.getString(ID));
            }

            if (jsonObject.has(SUB_ID) && !jsonObject.getString(SUB_ID).isEmpty() && !jsonObject.getString(SUB_ID).equalsIgnoreCase("null")) {
                casteObj.setSub_id(jsonObject.getString(SUB_ID));
            }
            if (jsonObject.has(SUB_NAME) && !jsonObject.getString(SUB_NAME).isEmpty() && !jsonObject.getString(SUB_NAME).equalsIgnoreCase("null")) {
                casteObj.setSub_name(jsonObject.getString(SUB_NAME));
            }
            if (jsonObject.has(TEST_DATE) && !jsonObject.getString(TEST_DATE).isEmpty() && !jsonObject.getString(TEST_DATE).equalsIgnoreCase("null")) {
                casteObj.setTest_date(jsonObject.getString(TEST_DATE));
            }
            if (jsonObject.has(CLASS_ID) && !jsonObject.getString(CLASS_ID).isEmpty() && !jsonObject.getString(CLASS_ID).equalsIgnoreCase("null")) {
                casteObj.setClass_id(jsonObject.getString(CLASS_ID));
            }

            if (jsonObject.has(CLASS_NAME) && !jsonObject.getString(CLASS_NAME).isEmpty() && !jsonObject.getString(CLASS_NAME).equalsIgnoreCase("null")) {
                casteObj.setClass_name(jsonObject.getString(CLASS_NAME));
            }
            if (jsonObject.has(MARKS) && !jsonObject.getString(MARKS).isEmpty() && !jsonObject.getString(MARKS).equalsIgnoreCase("null")) {
                casteObj.setMarks(jsonObject.getString(MARKS));
            }

            if (jsonObject.has(STUDENT_ID) && !jsonObject.getString(STUDENT_ID).isEmpty() && !jsonObject.getString(STUDENT_ID).equalsIgnoreCase("null")) {
                casteObj.setStudentid(jsonObject.getString(STUDENT_ID));
            }
            if (jsonObject.has(STUDENT_NAME) && !jsonObject.getString(STUDENT_NAME).isEmpty() && !jsonObject.getString(STUDENT_NAME).equalsIgnoreCase("null")) {
                casteObj.setStudentname(jsonObject.getString(STUDENT_NAME));
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }
        return casteObj;
    }
}
