package com.ultimate.ultimatesmartschool.ClassTest;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.squareup.picasso.Picasso;
import com.ultimate.ultimatesmartschool.BeanModule.SectionBean;
import com.ultimate.ultimatesmartschool.BeanModule.SubjectBeancls;
import com.ultimate.ultimatesmartschool.BeanModule.User;
import com.ultimate.ultimatesmartschool.ClassWork.CWViewPagerAdapter;
import com.ultimate.ultimatesmartschool.Homework.AdapterOfSubjectListN;
import com.ultimate.ultimatesmartschool.Homework.HomeworkadapterNew;
import com.ultimate.ultimatesmartschool.Homework.Homeworkbean;
import com.ultimate.ultimatesmartschool.Homework.StaffListAdapter;
import com.ultimate.ultimatesmartschool.Homework.StaffWiseHWActivity;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Staff.View_staff_bean;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.CommonProgress;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;
import com.ultimate.ultimatesmartschool.Utility.PaginationListener;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.relex.circleindicator.CircleIndicator;

public class ViewStaffWiseTest extends AppCompatActivity implements AddTestAdapter.Adaptercall {

    ArrayList<View_staff_bean> classList = new ArrayList<>();
    @BindView(R.id.parent)
    RelativeLayout parent;
    //@BindView(R.id.spinner) Spinner spinnerClass;
    @BindView(R.id.spinner)
    Spinner spinner;
    @BindView(R.id.spinner1)
    Spinner spinnerClass;
    String image_url="",school="",className = "";
    String classid = "";
    private AddTestAdapter adapter;
    ArrayList<TestListBean> hwList = new ArrayList<>();
    private LinearLayoutManager layoutManager;
    CommonProgress commonProgress;
    private int loaded = 0;
    int check=0;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.totalRecord)
    TextView totalRecord;
    List<String> classidsel;
    //    @BindView(R.id.textView7)
//    TextView textView7;
    Spinner spinnersection;
    ArrayList<SectionBean> sectionList = new ArrayList<>();
    String sectionid="";
    String sectionname;
    @BindView(R.id.recyclerViewmsg)
    RecyclerView recyclerview;
    @BindView(R.id.textNorecord)
    TextView txtNorecord;
    ArrayList<SubjectBeancls> subjectList = new ArrayList<>();
    AdapterOfSubjectListN adaptersub;
    String sub_id, sub_name;


    public ViewPager intro_images;
    LinearLayout pager_indicator;
    public CircleIndicator indicator;
    public TextView textView;

    Dialog mBottomSheetDialog;
    @BindView(R.id.adddate)TextView adddate;
    String type="1",name="",date="",view_type="staff",filter_id="",tdate="",ydate="";
    StaffListAdapter staffListAdapter;

    @BindView(R.id.dialog)
    ImageView dialog;
    SharedPreferences sharedPreferences;
    String tag="";

    @BindView(R.id.main_progress)
    ProgressBar main_progress;
    int  limit=100,page_limit=0,total_pages=0;
    private static final int PAGE_START = 1;
    private int currentPage = PAGE_START;
    private boolean isLastPage = false;
    private boolean isLoading = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_g_e_t_homework);
        ButterKnife.bind(this);
        commonProgress=new CommonProgress(this);

        if (getIntent().getExtras() != null) {
            tag = getIntent().getExtras().getString("name");
            if (tag.equalsIgnoreCase("sea")) {
                txtTitle.setText("Subject Enrichment Activity");
            } else {
                txtTitle.setText("Test List");
            }
        }

        classidsel = new ArrayList<>();
        dialog.setVisibility(View.GONE);
        spinner.setVisibility(View.GONE);
        spinnerClass.setVisibility(View.VISIBLE);
        recyclerview.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(ViewStaffWiseTest.this);
        recyclerview.setLayoutManager(layoutManager);
        if (tag.equalsIgnoreCase("sea")) {
            adapter = new AddTestAdapter(ViewStaffWiseTest.this, hwList, this,1);
        } else {
            adapter = new AddTestAdapter(ViewStaffWiseTest.this, hwList, this,0);
        }
        recyclerview.setAdapter(adapter);
        fetchClass(limit);
        getTodayDate();



       // txtTitle.setText("Test");

        recyclerview.addOnScrollListener(new PaginationListener(layoutManager) {
            @Override
            protected void loadMoreItems() {
                // isLoading = true;
                //  Utils.showSnackBar("No more messages found in message list.", parent);
                //  currentPage++;
                // doApiCall();

                if (total_pages>=page_limit) {
                    main_progress.setVisibility(View.VISIBLE);
                    currentPage += 10;
                    page_limit = currentPage + limit;

                    if (sub_id.equalsIgnoreCase("")){
                        fetchClasswork(date,0,sub_id, page_limit, "no");
                    }else{
                        fetchClasswork(date,5,sub_id, page_limit, "no");
                    }
                }else{
                    // main_progress.setVisibility(View.GONE);
                    //  Utils.showSnackBar("No more messages found in message list.", parent);
                }


            }
            @Override
            public boolean isLastPage() {
                return isLastPage;
            }
            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });

        // fetchSection();
        //  txtTitle.setText("Homework");
        school= User.getCurrentUser().getSchoolData().getName();
    }

    public void getTodayDate() {
        java.util.Calendar c = java.util.Calendar.getInstance();
        c.set(java.util.Calendar.HOUR_OF_DAY, 00);
        c.set(java.util.Calendar.MINUTE, 00);
        c.set(java.util.Calendar.SECOND, 00);
        Date setdate = c.getTime();
        java.text.SimpleDateFormat fmtOut = new java.text.SimpleDateFormat("dd MMM, yyyy");
        String dateString = fmtOut.format(setdate);
        //  adddate.setText(dateString);
        java.text.SimpleDateFormat dateFrmOut = new java.text.SimpleDateFormat("yyyy-MM-dd");
        tdate = dateFrmOut.format(setdate);
        //for Yesterday
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);
        ydate= dateFormat.format(cal.getTime());
    }

    @OnClick(R.id.filelayout)
    public void fetchDate() {
        java.util.Calendar cal = java.util.Calendar.getInstance(TimeZone.getDefault()); // Get current date
        DatePickerDialog datePicker = new DatePickerDialog(ViewStaffWiseTest.this,
                R.style.MyDatePickerDialogTheme, ondate,
                cal.get(java.util.Calendar.YEAR),
                cal.get(java.util.Calendar.MONTH),
                cal.get(java.util.Calendar.DAY_OF_MONTH));
        datePicker.getDatePicker().setMaxDate(new java.util.Date().getTime());
        datePicker.setCancelable(false);
        datePicker.show();
    }

    DatePickerDialog.OnDateSetListener ondate = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            java.util.Calendar calendar = java.util.Calendar.getInstance(TimeZone.getDefault());
            calendar.set(year, monthOfYear, dayOfMonth);
            calendar.set(Calendar.HOUR_OF_DAY, 00);
            calendar.set(Calendar.MINUTE, 00);
            calendar.set(Calendar.SECOND, 00);
            java.util.Date setdate = calendar.getTime();
            SimpleDateFormat fmtOut = new SimpleDateFormat("dd MMM, yyyy");
            String dateString = fmtOut.format(setdate);
            //adddate.setText(dateString);
            SimpleDateFormat dateFrmOut = new SimpleDateFormat("yyyy-MM-dd");
            date = dateFrmOut.format(setdate);
            if (tdate.equalsIgnoreCase(date)){
                adddate.setText("Today");
                adddate.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.light_red));
            } else if (ydate.equalsIgnoreCase(date)){
                adddate.setText("Yesterday");
                adddate.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.green));
            }else {
                adddate.setText(dateString);
                adddate.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
            }
            page_limit=0;
            fetchClass(limit);
        }
    };

    private void fetchClass(int limit) {

        HashMap<String, String> params = new HashMap<String, String>();
        params.put("check", "list");
        params.put("d_id", "");
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.STAFF_DETAIL, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                if (error == null) {
                    try {
                        JSONArray jsonArray = jsonObject.getJSONArray("staff_detail");
                        classList = View_staff_bean.parseviewstaffArray(jsonArray);
                        staffListAdapter = new StaffListAdapter(ViewStaffWiseTest.this, classList);
                        spinnerClass.setAdapter(staffListAdapter);
                        spinnerClass.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                sub_id="";
                                if (i > 0) {
                                    sub_id = classList.get(i - 1).getId();
                                    fetchClasswork(date,5,sub_id, limit, "yes");
                                } else {
                                    fetchClasswork(date,0,sub_id, limit, "yes");
                                }
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> adapterView) {

                            }
                        });

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
//                Utils.showSnackBar(error.getMessage(), parent);
                }
            }
        }, this, params);
    }

    @OnClick(R.id.imgBack)
    public void onback() {
        finish();
    }


    public void fetchClasswork(String date, int i,String sub_id,int limit,String progress) {
        if (progress.equalsIgnoreCase("yes")){
            commonProgress.show();
        }
        HashMap<String, String> params = new HashMap<String, String>();

        if (i == 0){
            params.put("class_id", classid);
            params.put("section_id", sectionid);
        } else {
            params.put("class_id", classid);
            params.put("section_id", sectionid);
            params.put("sub_id", sub_id);
        }
        params.put("today",date);
        params.put("type", tag);
        params.put("view_type",view_type);
        params.put("page_limit",String.valueOf(limit));
        params.put("new", "new");
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.TestListgds, apiCallback, this, params);
    }
    ApiHandler.ApiCallback apiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            commonProgress.dismiss();
            if (error == null) {
                try {
                    if (hwList != null) {
                        hwList.clear();
                    }
                    JSONArray jsonArray = jsonObject.getJSONArray("test_data");
                    hwList = TestListBean.parseTestlistArray(jsonArray);
                    total_pages= Integer.parseInt(hwList.get(0).getRowcount());
                    if (hwList.size() > 0) {
                        adapter.settestList(hwList);
                        adapter.notifyDataSetChanged();
                        txtNorecord.setVisibility(View.GONE);
                        totalRecord.setText("Total Entries:- "+String.valueOf(hwList.size()));
                    } else {
                        totalRecord.setText("Total Entries:- 0");
                        adapter.settestList(hwList);
                        adapter.notifyDataSetChanged();
                        txtNorecord.setVisibility(View.VISIBLE);
                    }
                    main_progress.setVisibility(View.GONE);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                totalRecord.setText("Total Entries:- 0");
                txtNorecord.setVisibility(View.VISIBLE);
                hwList.clear();
                adapter.settestList(hwList);
                adapter.notifyDataSetChanged();
            }
        }
    };

    @Override
    public void methodcall(TestListBean obj) {
        Toast.makeText(getApplicationContext(),"Test marks not available!",Toast.LENGTH_SHORT).show();
    }

    @Override
    public void methodcallImg(TestListBean data) {

//        mBottomSheetDialog = new Dialog(this);
//        final View sheetView = getLayoutInflater().inflate(R.layout.image_lyt, null);
//        mBottomSheetDialog.setContentView(sheetView);
//        ImageView imgShow = (ImageView) mBottomSheetDialog.findViewById(R.id.imgShow);
//
//        ImageView imgDownload = (ImageView) mBottomSheetDialog.findViewById(R.id.imgDownload);
//        Picasso.with(this).load(homeworkbean.getTest_file()).placeholder(getResources().getDrawable(R.drawable.test)).into(imgShow);
//        imgDownload.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                BitmapDrawable bitmapDrawable = (BitmapDrawable) imgShow.getDrawable();
//                Bitmap bitmap = bitmapDrawable.getBitmap();
//                saveImageToGallery(bitmap);
//            }
//        });
//        mBottomSheetDialog.show();
//        Window window = mBottomSheetDialog.getWindow();
//        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        final Dialog warningDialog = new Dialog(this);
        warningDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        warningDialog.setCancelable(true);

        warningDialog.setContentView(R.layout.class_img_dialog);
        warningDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        RelativeLayout btnClose = (RelativeLayout) warningDialog.findViewById(R.id.btnNo);

        indicator = (CircleIndicator) warningDialog.findViewById(R.id.indicator);
        textView=(TextView) warningDialog.findViewById(R.id.clsswrktexttt);
        intro_images=(ViewPager) warningDialog.findViewById(R.id.pager_introduction);

        TextView tap_count=(TextView) warningDialog.findViewById(R.id.tap_count);
        String title1 = getColoredSpanned("<b>"+"1"+"</b>"+"", "#F4212C");
        String l_Name1 = getColoredSpanned(" / "+String.valueOf(data.getTest_file().size()), "#000000");
        tap_count.setText(Html.fromHtml(title1 + " " + l_Name1+ " "));

        textView.setText(data.getTopic());
        CWViewPagerAdapter mAdapter = new  CWViewPagerAdapter(this,data.getTest_file(),data.getTopic());
        intro_images.setAdapter(mAdapter);
        intro_images.setCurrentItem(0);
        indicator.setViewPager(intro_images);

        intro_images.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int arg0) {
                // TODO Auto-generated method stub
                int c=arg0+1;
                String title1 = getColoredSpanned("<b>"+String.valueOf(c)+"</b>"+"", "#F4212C");
                String l_Name1 = getColoredSpanned(" / "+String.valueOf(data.getTest_file().size()), "#000000");
                tap_count.setText(Html.fromHtml(title1 + " " + l_Name1+ " "));
            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
                // TODO Auto-generated method stub

                System.out.println("onPageScrolled");
            }

            @Override
            public void onPageScrollStateChanged(int num) {
                // TODO Auto-generated method stub

            }
        });


        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                warningDialog.dismiss();
            }
        });


        warningDialog.show();
        Window window = warningDialog.getWindow();
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    }

    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }

    private void saveImageToGallery(Bitmap bitmap){

        OutputStream fos;

        try{

            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q){

                ContentResolver resolver = getContentResolver();
                ContentValues contentValues =  new ContentValues();
                contentValues.put(MediaStore.MediaColumns.DISPLAY_NAME, "ts_img_saved" + ".jpg");
                contentValues.put(MediaStore.MediaColumns.MIME_TYPE, "image/jpeg");
                contentValues.put(MediaStore.MediaColumns.RELATIVE_PATH, Environment.DIRECTORY_PICTURES + File.separator + school);
                Uri imageUri = resolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, contentValues);

                fos = resolver.openOutputStream(Objects.requireNonNull(imageUri));


                Toast.makeText(this, "Image Saved", Toast.LENGTH_SHORT).show();
            }
            else{

//                // Save image to gallery
//                String savedImageURL = MediaStore.Images.Media.insertImage(getContentResolver(), bitmap, "Bird", "Image of bird");
//
//                // Parse the gallery image url to uri
//                Uri savedImageURI = Uri.parse(savedImageURL);

                //with folder name
                String imagesDir = Environment.getExternalStoragePublicDirectory(
                        Environment.DIRECTORY_PICTURES).toString() + File.separator + school;

                File file = new File(imagesDir);

                if (!file.exists()) {
                    file.mkdir();
                }

                File image = new File(imagesDir, "ts_img_saved" + ".jpg");
                fos = new FileOutputStream(image);

                Toast.makeText(this, "Image saved to internal!!", Toast.LENGTH_SHORT).show();
                //resetOpTimes();

            }

            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            Objects.requireNonNull(fos);

        }catch(Exception e){

            Toast.makeText(this, "Image not saved \n" + e.toString(), Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void methodcalltestresult(TestListBean obj) {
        String subName = obj.getSub_name();
        String test_id = obj.getId();
        String topic = obj.getTopic();
        String date = obj.getTest_date();
        String classname = obj.getClassname();
        String section_id=obj.getSectionid();
        String sectionname=obj.getSectionname();
        String staff_name=obj.getStaff_name();
        Intent i = new Intent(ViewStaffWiseTest.this, TestResultGDS.class);
        i.putExtra("test_id", test_id);
        i.putExtra("topic", topic);
        i.putExtra("subName", subName);
        i.putExtra("date", date);
        i.putExtra("classname", classname);
        i.putExtra("section_id", section_id);
        i.putExtra("sectionname", sectionname);
        i.putExtra("staff_name", staff_name);
        if (tag.equalsIgnoreCase("sea")){
            i.putExtra("name", tag);
            i.putExtra("exam", obj.getExam());
        }else{
            i.putExtra("name", tag);
            i.putExtra("exam", "");
        }
        startActivity(i);
    }
}
