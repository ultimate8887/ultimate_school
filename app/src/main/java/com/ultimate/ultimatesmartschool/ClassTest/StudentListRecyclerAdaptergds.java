package com.ultimate.ultimatesmartschool.ClassTest;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.ultimate.ultimatesmartschool.R;

import java.util.ArrayList;

import butterknife.ButterKnife;

public class StudentListRecyclerAdaptergds extends RecyclerView.Adapter<StudentListRecyclerAdaptergds.ViewHolder> {
    Context context;
    static String k;
    private ArrayList<ResultTestListBean> MainImageUploadInfoList;
    private ad m;

    public StudentListRecyclerAdaptergds(Context context, ArrayList<ResultTestListBean> MainImageUploadInfoList, ad m1) {

        this.MainImageUploadInfoList = MainImageUploadInfoList;
        this.context = context;
        this.m = m1;
    }

    @Override
    public StudentListRecyclerAdaptergds.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.stud_list_lyt, parent, false);

        StudentListRecyclerAdaptergds.ViewHolder viewHolder = new StudentListRecyclerAdaptergds.ViewHolder(view,new StudentListRecyclerAdaptergds.MyCstmEditTextListener());

        return viewHolder;
    }


    @Override
    public void onBindViewHolder(final StudentListRecyclerAdaptergds.ViewHolder holder, final int position) {
        holder.parent.setAnimation(AnimationUtils.loadAnimation(context,R.anim.fade_scale_animation));
        holder.studentsnames.setText(MainImageUploadInfoList.get(position).getName());
        holder.myCustomEditTextListener.updatePosition(holder.getAdapterPosition());

//        if(MainImageUploadInfoList.size()>0) {
//            holder.studentsnumber.setText(MainImageUploadInfoList.get(holder.getAdapterPosition()).getMarks());
//        }
        holder.studentsnumber.setText(MainImageUploadInfoList.get(holder.getAdapterPosition()).getMarks());
        //holder.studentsnumber.setText((CharSequence) MainImageUploadInfoList.get(position));
        holder.studentsnumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                m.methodcall(MainImageUploadInfoList.get(position), k);
            }
        });
    }

    @Override
    public int getItemCount() {

        return MainImageUploadInfoList.size();
    }

    public void setStudentList(ArrayList<ResultTestListBean> MainImageUploadInfoList) {
        this.MainImageUploadInfoList = MainImageUploadInfoList;
    }

    public ArrayList<ResultTestListBean> getStudentList() {
        return MainImageUploadInfoList;
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView studentsnames;
        public EditText studentsnumber;
        ConstraintLayout parent;
        public StudentListRecyclerAdaptergds.MyCstmEditTextListener myCustomEditTextListener;
        public ViewHolder(View itemView, StudentListRecyclerAdaptergds.MyCstmEditTextListener myCustomEditTextListener) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            parent= (ConstraintLayout) itemView.findViewById(R.id.parent);
            studentsnames = (TextView) itemView.findViewById(R.id.studentnames);
            studentsnumber = (EditText) itemView.findViewById(R.id.studentsnumber);
            this.myCustomEditTextListener = myCustomEditTextListener;
            this.studentsnumber.addTextChangedListener(myCustomEditTextListener);

        }

    }

    public interface ad {
        public void methodcall(ResultTestListBean obj, String k);
    }

    private class MyCstmEditTextListener implements TextWatcher {
        private int position;

        public void updatePosition(int position) {
            this.position = position;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
//         if(count==0){
//             MainImageUploadInfoList.get(position).setMarks("0");
//         }else{
//             MainImageUploadInfoList.get(position).setMarks(String.valueOf(s));
//         }
//
//            if(count==0){
//                MainImageUploadInfoList.get(position).setMarks("0");
//            }else{
//
//                MainImageUploadInfoList.get(position).setMarks(String.valueOf(s));
//            }


            MainImageUploadInfoList.get(position).setMarks(String.valueOf(s));
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    }
}
