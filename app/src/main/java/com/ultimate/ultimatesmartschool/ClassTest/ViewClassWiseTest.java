package com.ultimate.ultimatesmartschool.ClassTest;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.Html;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetSequence;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.squareup.picasso.Picasso;
import com.ultimate.ultimatesmartschool.BeanModule.ClassBean;
import com.ultimate.ultimatesmartschool.BeanModule.SectionBean;
import com.ultimate.ultimatesmartschool.BeanModule.SubjectBeancls;
import com.ultimate.ultimatesmartschool.BeanModule.User;
import com.ultimate.ultimatesmartschool.ClassWork.CWViewPagerAdapter;
import com.ultimate.ultimatesmartschool.Examination.ClassAdapter;
import com.ultimate.ultimatesmartschool.Homework.AdapterOfSubjectListN;
import com.ultimate.ultimatesmartschool.Homework.SectionnewAdapter;
import com.ultimate.ultimatesmartschool.Login.LoginActivity;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.CommonProgress;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;
import com.ultimate.ultimatesmartschool.Utility.PaginationListener;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.relex.circleindicator.CircleIndicator;

public class ViewClassWiseTest extends AppCompatActivity implements AddTestAdapter.Adaptercall{
    ArrayList<ClassBean> classList = new ArrayList<>();
    @BindView(R.id.parent)
    RelativeLayout parent;

    public ViewPager intro_images;
    LinearLayout pager_indicator;
    public CircleIndicator indicator;
    public TextView textView;
    //@BindView(R.id.spinner) Spinner spinnerClass;
//    @BindView(R.id.multiselectSpinnerclass)
//    MultiSelectSpinner multiselectSpinnerclass;
    Spinner spinnerClass;
    String image_url="",school="",className = "";
    String classid = "";
    private AddTestAdapter adapter;
    ArrayList<TestListBean> hwList = new ArrayList<>();
    private LinearLayoutManager layoutManager;

    CommonProgress commonProgress;
    private int loaded = 0;
    int check=0;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.totalRecord)
    TextView totalRecord;
    List<String> classidsel;
    //    @BindView(R.id.textView7)
//    TextView textView7;
    Spinner spinnersection;
    ArrayList<SectionBean> sectionList = new ArrayList<>();
    String sectionid="";
    String sectionname;
    @BindView(R.id.recyclerViewmsg)
    RecyclerView recyclerview;
    @BindView(R.id.textNorecord)
    TextView txtNorecord;
    ArrayList<SubjectBeancls> subjectList = new ArrayList<>();
    AdapterOfSubjectListN adaptersub;
    String sub_id, sub_name;
    @BindView(R.id.spinner)
    Spinner spinnersubject;
    Dialog mBottomSheetDialog;
    @BindView(R.id.adddate)TextView adddate;
    String type="1",name="",date="",view_type="class",filter_id="",tdate="",ydate="";
    @BindView(R.id.dialog)
    ImageView dialog;
    String tag="";
    SharedPreferences sharedPreferences;
    @BindView(R.id.main_progress)
    ProgressBar main_progress;
    int  limit=100,page_limit=0,total_pages=0;
    private static final int PAGE_START = 1;
    private int currentPage = PAGE_START;
    private boolean isLastPage = false;
    private boolean isLoading = false;
    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_g_e_t_homework);

        ButterKnife.bind(this);
        commonProgress=new CommonProgress(this);

        if (getIntent().getExtras() != null) {
            tag = getIntent().getExtras().getString("name");
        }

        classidsel = new ArrayList<>();
        recyclerview.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(ViewClassWiseTest.this);
        recyclerview.setLayoutManager(layoutManager);
        if (tag.equalsIgnoreCase("sea")) {
            adapter = new AddTestAdapter(ViewClassWiseTest.this, hwList, this,1);
        } else {
            adapter = new AddTestAdapter(ViewClassWiseTest.this, hwList, this,0);
        }

        recyclerview.setAdapter(adapter);
        getTodayDate();

        if (!restorePrefData()){
            setShowcaseView();
        }else {
            openDialog();
        }

        recyclerview.addOnScrollListener(new PaginationListener(layoutManager) {
            @Override
            protected void loadMoreItems() {
                // isLoading = true;
                //  Utils.showSnackBar("No more messages found in message list.", parent);
                //  currentPage++;
                // doApiCall();

                if (total_pages>=page_limit) {
                    main_progress.setVisibility(View.VISIBLE);
                    currentPage += 10;
                    page_limit = currentPage + limit;

                    if (sub_id.equalsIgnoreCase("")){
                        fetchClasswork(date,0,sub_id, page_limit, "no");
                    }else{
                        fetchClasswork(date,5,sub_id, page_limit, "no");
                    }
                }else{
                    // main_progress.setVisibility(View.GONE);
                    //  Utils.showSnackBar("No more messages found in message list.", parent);
                }


            }
            @Override
            public boolean isLastPage() {
                return isLastPage;
            }
            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });

        // fetchSection();
        //  txtTitle.setText("Homework");
        school=User.getCurrentUser().getSchoolData().getName();
    }

    public void getTodayDate() {
        java.util.Calendar c = java.util.Calendar.getInstance();
        c.set(java.util.Calendar.HOUR_OF_DAY, 00);
        c.set(java.util.Calendar.MINUTE, 00);
        c.set(java.util.Calendar.SECOND, 00);
        Date setdate = c.getTime();
        java.text.SimpleDateFormat fmtOut = new java.text.SimpleDateFormat("dd MMM, yyyy");
        String dateString = fmtOut.format(setdate);
        //  adddate.setText(dateString);
        java.text.SimpleDateFormat dateFrmOut = new java.text.SimpleDateFormat("yyyy-MM-dd");
        tdate = dateFrmOut.format(setdate);
        //for Yesterday
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);
        ydate= dateFormat.format(cal.getTime());
    }

    @OnClick(R.id.filelayout)
    public void fetchDate() {
        java.util.Calendar cal = java.util.Calendar.getInstance(TimeZone.getDefault()); // Get current date
        DatePickerDialog datePicker = new DatePickerDialog(ViewClassWiseTest.this,
                R.style.MyDatePickerDialogTheme, ondate,
                cal.get(java.util.Calendar.YEAR),
                cal.get(java.util.Calendar.MONTH),
                cal.get(java.util.Calendar.DAY_OF_MONTH));
        datePicker.getDatePicker().setMaxDate(new java.util.Date().getTime());
        datePicker.setCancelable(false);
        datePicker.show();
    }

    DatePickerDialog.OnDateSetListener ondate = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            java.util.Calendar calendar = java.util.Calendar.getInstance(TimeZone.getDefault());
            calendar.set(year, monthOfYear, dayOfMonth);
            calendar.set(Calendar.HOUR_OF_DAY, 00);
            calendar.set(Calendar.MINUTE, 00);
            calendar.set(Calendar.SECOND, 00);
            java.util.Date setdate = calendar.getTime();
            SimpleDateFormat fmtOut = new SimpleDateFormat("dd MMM, yyyy");
            String dateString = fmtOut.format(setdate);
            //adddate.setText(dateString);
            SimpleDateFormat dateFrmOut = new SimpleDateFormat("yyyy-MM-dd");
            date = dateFrmOut.format(setdate);
            if (tdate.equalsIgnoreCase(date)){
                adddate.setText("Today");
                adddate.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.light_red));
            } else if (ydate.equalsIgnoreCase(date)){
                adddate.setText("Yesterday");
                adddate.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.green));
            }else {
                adddate.setText(dateString);
                adddate.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
            }
            page_limit=0;
            fetchSubject(limit);
        }
    };


    @RequiresApi(api = Build.VERSION_CODES.N)
    @OnClick(R.id.dialog)
    public void dialogggg() {
        openDialog();
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void openDialog() {

        mBottomSheetDialog = new BottomSheetDialog(this);
        final View sheetView = getLayoutInflater().inflate(R.layout.search_dialog, null);
        mBottomSheetDialog.setContentView(sheetView);
        mBottomSheetDialog.setCancelable(false);

        spinnerClass=(Spinner) sheetView.findViewById(R.id.spinnerClass);
        spinnersection=(Spinner) sheetView.findViewById(R.id.spinnersection);
        ImageView close=(ImageView) sheetView.findViewById(R.id.close);
        TextView txtSetup=(TextView) sheetView.findViewById(R.id.txtSetup);
        if (tag.equalsIgnoreCase("sea")) {
            //  txtTitle.setText("SEA List");
            txtSetup.setText("View SEA List");
            // stulayout.setVisibility(View.GONE);
        } else {
            txtSetup.setText("View Class-Test");
            //  stulayout.setVisibility(View.VISIBLE);
        }
        //txtTitle.setText("Mark Attendance");
        fetchClass();
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //assignData();
                if (check==0) {
                    finish();
                    mBottomSheetDialog.dismiss();
                }else {
                    mBottomSheetDialog.dismiss();
                }
            }
        });


        Button btnYes= (Button) sheetView.findViewById(R.id.btnYes);


        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (classid.equalsIgnoreCase(""))  {
                    Toast.makeText(ViewClassWiseTest.this,"Kindly Select Class!", Toast.LENGTH_SHORT).show();
                }  else if (sectionid.equalsIgnoreCase("")){
                    Toast.makeText(ViewClassWiseTest.this,"Kindly Select Class Section!", Toast.LENGTH_SHORT).show();
                }else {
                    check++;
                    page_limit=0;
                    fetchSubject(limit);
                    setCommonData();
                }

            }
        });
        mBottomSheetDialog.show();

    }

    private void setCommonData() {
        txtTitle.setText(className+"("+sectionname+")" +" class");
        //  txtSub.setText(sub_name);
    }

    private void setShowcaseView() {
        new TapTargetSequence(this)
                .targets(TapTarget.forView(dialog,"Search Button!","Tap the Search button to Search Class-Wise Homework.")
                        .outerCircleColor(R.color.light)
                        .outerCircleAlpha(0.96f)
                        .targetCircleColor(R.color.white)
                        .titleTextSize(20)
                        .titleTextColor(R.color.white)
                        .descriptionTextSize(14)
                        .descriptionTextColor(R.color.black)
                        .textColor(R.color.black)
                        .textTypeface(Typeface.SANS_SERIF)
                        .dimColor(R.color.black)
                        .drawShadow(true)
                        .cancelable(false)
                        .tintTarget(true)
                        .transparentTarget(true)
                        .targetRadius(60)).listener(new TapTargetSequence.Listener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onSequenceFinish() {
                // Toast.makeText(getApplicationContext(),"Sequence Finished",Toast.LENGTH_SHORT).show();
                savePrefData();
                openDialog();
            }

            @Override
            public void onSequenceStep(TapTarget lastTarget, boolean targetClicked) {
                // Toast.makeText(getActivity(),"GREAT!",Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSequenceCanceled(TapTarget lastTarget) {

            }
        }).start();
    }

    private void savePrefData(){
        sharedPreferences=this.getSharedPreferences("boarding_pref_add_attend_search",MODE_PRIVATE);
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putBoolean("IsFirstTimeVisit_add_attend_search",true);
        editor.apply();
    }

    private boolean restorePrefData(){
        sharedPreferences = this.getSharedPreferences("boarding_pref_add_attend_search",MODE_PRIVATE);
        return sharedPreferences.getBoolean("IsFirstTimeVisit_add_attend_search",false);
    }

    private void fetchClass() {
        if (loaded == 0) {
            commonProgress.show();
        }
        loaded++;

        HashMap<String, String> params = new HashMap<String, String>();
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.CLASSLIST_URL, classapiCallback,getApplicationContext(), params);
    }


    ApiHandler.ApiCallback classapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            commonProgress.dismiss();
            if (error == null) {
                try {
                    classList = ClassBean.parseClassArray(jsonObject.getJSONArray(Constants.CLASSDATA));
                    ClassAdapter adapter = new ClassAdapter(getApplicationContext(), classList);
                    spinnerClass.setAdapter(adapter);
                    spinnerClass.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if (i > 0) {
                                sub_id= "";
                                sub_name= "" ;
                                sectionid="";
                                sectionname="";
                                classid = "";
                                className = "";
                                classid = classList.get(i - 1).getId();
                                className= classList.get(i - 1).getName();
                                fetchSection();
                            }

                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_LONG).show();
                // Utils.showSnackBar(error.getMessage(), view.findViewById(R.id.parent));
            }
        }
    };

    private void fetchSubject(int limit) {
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("id", classid);
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.SubjectList, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                mBottomSheetDialog.dismiss();
                if (error == null) {
                    try {

                        subjectList =   SubjectBeancls.parseClassArray(jsonObject.getJSONArray("sub_data"));

                        adaptersub = new AdapterOfSubjectListN(ViewClassWiseTest.this, subjectList);
                        spinnersubject.setAdapter(adaptersub);
                        spinnersubject.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                sub_id="";
                                if (i > 0) {
                                    sub_name = subjectList.get(i - 1).getName();
                                    sub_id = subjectList.get(i - 1).getId();
                                    fetchClasswork(date,5,sub_id, limit, "yes");
                                }else {
                                    fetchClasswork(date,0,sub_id, limit, "yes");
                                }

                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> adapterView) {

                            }
                        });

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }, this, params);

    }

    private void fetchSection() {
        HashMap<String, String> params = new HashMap<String, String>();
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.SECTIONLIST_URL, sectionapiCallback, this, params);
    }

    ApiHandler.ApiCallback sectionapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            if (error == null) {
                try {
                    sectionList = SectionBean.parseCommonArray(jsonObject.getJSONArray("hostelroom_data"));
                    SectionnewAdapter adapter = new SectionnewAdapter(ViewClassWiseTest.this, sectionList);
                    spinnersection.setAdapter(adapter);
                    spinnersection.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if (i > 0) {
                                sectionid = sectionList.get(i - 1).getSection_id();
                                sectionname = sectionList.get(i - 1).getSection_name();

                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Utils.showSnackBar(error.getMessage(), parent);
                if (error.getStatusCode() == 405) {
                    User.logout();
                    startActivity(new Intent(ViewClassWiseTest.this, LoginActivity.class));
                    finish();
                }
            }
        }
    };

    @OnClick(R.id.imgBack)
    public void onback() {
        finish();
    }


    public void fetchClasswork(String date, int i,String sub_id,int limit,String progress) {
        if (progress.equalsIgnoreCase("yes")){
            commonProgress.show();
        }
        HashMap<String, String> params = new HashMap<String, String>();

        if (i == 0){
            params.put("class_id", classid);
            params.put("section_id", sectionid);
        } else {
            params.put("class_id", classid);
            params.put("section_id", sectionid);
            params.put("sub_id", sub_id);
        }
        params.put("today",date);
        params.put("view_type",view_type);
        params.put("type", tag);
        params.put("page_limit",String.valueOf(limit));
        params.put("new", "new");
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.TestListgds, apiCallback, this, params);
    }
    ApiHandler.ApiCallback apiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            commonProgress.dismiss();
            if (error == null) {
                try {
                    if (hwList != null) {
                        hwList.clear();
                    }
                    JSONArray jsonArray = jsonObject.getJSONArray("test_data");
                    hwList = TestListBean.parseTestlistArray(jsonArray);
                    total_pages= Integer.parseInt(hwList.get(0).getRowcount());
                    if (hwList.size() > 0) {
                        adapter.settestList(hwList);
                        adapter.notifyDataSetChanged();
                        txtNorecord.setVisibility(View.GONE);
                        totalRecord.setText("Total Entries:- "+String.valueOf(hwList.size()));
                    } else {
                        totalRecord.setText("Total Entries:- 0");
                        adapter.settestList(hwList);
                        adapter.notifyDataSetChanged();
                        txtNorecord.setVisibility(View.VISIBLE);
                    }
                    main_progress.setVisibility(View.GONE);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                totalRecord.setText("Total Entries:- 0");
                txtNorecord.setVisibility(View.VISIBLE);
                hwList.clear();
                adapter.settestList(hwList);
                adapter.notifyDataSetChanged();
            }
        }
    };

    @Override
    public void methodcall(TestListBean obj) {
       Toast.makeText(getApplicationContext(),"Test marks not available!",Toast.LENGTH_SHORT).show();
    }



    @Override
    public void methodcallImg(TestListBean data) {
//        mBottomSheetDialog = new Dialog(this);
//        final View sheetView = getLayoutInflater().inflate(R.layout.image_lyt, null);
//        mBottomSheetDialog.setContentView(sheetView);
//        ImageView imgShow = (ImageView) mBottomSheetDialog.findViewById(R.id.imgShow);
//
//        ImageView imgDownload = (ImageView) mBottomSheetDialog.findViewById(R.id.imgDownload);
//
//        Picasso.with(this).load(homeworkbean.getTest_file()).placeholder(getResources().getDrawable(R.drawable.test)).into(imgShow);
//
//        imgDownload.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                BitmapDrawable bitmapDrawable = (BitmapDrawable) imgShow.getDrawable();
//                Bitmap bitmap = bitmapDrawable.getBitmap();
//                saveImageToGallery(bitmap);
//            }
//        });
//        mBottomSheetDialog.show();
//        Window window = mBottomSheetDialog.getWindow();
//        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        final Dialog warningDialog = new Dialog(this);
        warningDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        warningDialog.setCancelable(true);

        warningDialog.setContentView(R.layout.class_img_dialog);
        warningDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        RelativeLayout btnClose = (RelativeLayout) warningDialog.findViewById(R.id.btnNo);

        indicator = (CircleIndicator) warningDialog.findViewById(R.id.indicator);
        textView=(TextView) warningDialog.findViewById(R.id.clsswrktexttt);
        intro_images=(ViewPager) warningDialog.findViewById(R.id.pager_introduction);

        TextView tap_count=(TextView) warningDialog.findViewById(R.id.tap_count);
        String title1 = getColoredSpanned("<b>"+"1"+"</b>"+"", "#F4212C");
        String l_Name1 = getColoredSpanned(" / "+String.valueOf(data.getTest_file().size()), "#000000");
        tap_count.setText(Html.fromHtml(title1 + " " + l_Name1+ " "));

        textView.setText(data.getTopic());
        CWViewPagerAdapter mAdapter = new  CWViewPagerAdapter(this,data.getTest_file(),data.getTopic());
        intro_images.setAdapter(mAdapter);
        intro_images.setCurrentItem(0);
        indicator.setViewPager(intro_images);

        intro_images.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int arg0) {
                // TODO Auto-generated method stub
                int c=arg0+1;
                String title1 = getColoredSpanned("<b>"+String.valueOf(c)+"</b>"+"", "#F4212C");
                String l_Name1 = getColoredSpanned(" / "+String.valueOf(data.getTest_file().size()), "#000000");
                tap_count.setText(Html.fromHtml(title1 + " " + l_Name1+ " "));
            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
                // TODO Auto-generated method stub

                System.out.println("onPageScrolled");
            }

            @Override
            public void onPageScrollStateChanged(int num) {
                // TODO Auto-generated method stub

            }
        });


        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                warningDialog.dismiss();
            }
        });


        warningDialog.show();
        Window window = warningDialog.getWindow();
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    }

    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }


    private void saveImageToGallery(Bitmap bitmap){

        OutputStream fos;

        try{

            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q){

                ContentResolver resolver = getContentResolver();
                ContentValues contentValues =  new ContentValues();
                contentValues.put(MediaStore.MediaColumns.DISPLAY_NAME, "ts_img_saved" + ".jpg");
                contentValues.put(MediaStore.MediaColumns.MIME_TYPE, "image/jpeg");
                contentValues.put(MediaStore.MediaColumns.RELATIVE_PATH, Environment.DIRECTORY_PICTURES + File.separator + school);
                Uri imageUri = resolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, contentValues);

                fos = resolver.openOutputStream(Objects.requireNonNull(imageUri));


                Toast.makeText(this, "Image Saved", Toast.LENGTH_SHORT).show();
            }
            else{

//                // Save image to gallery
//                String savedImageURL = MediaStore.Images.Media.insertImage(getContentResolver(), bitmap, "Bird", "Image of bird");
//
//                // Parse the gallery image url to uri
//                Uri savedImageURI = Uri.parse(savedImageURL);

                //with folder name
                String imagesDir = Environment.getExternalStoragePublicDirectory(
                        Environment.DIRECTORY_PICTURES).toString() + File.separator + school;

                File file = new File(imagesDir);

                if (!file.exists()) {
                    file.mkdir();
                }

                File image = new File(imagesDir, "ts_img_saved" + ".jpg");
                fos = new FileOutputStream(image);

                Toast.makeText(this, "Image saved to internal!!", Toast.LENGTH_SHORT).show();
                //resetOpTimes();

            }

            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            Objects.requireNonNull(fos);

        }catch(Exception e){

            Toast.makeText(this, "Image not saved \n" + e.toString(), Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void methodcalltestresult(TestListBean obj) {
        String subName = obj.getSub_name();
        String test_id = obj.getId();
        String topic = obj.getTopic();
        String date = obj.getTest_date();
        String classname = obj.getClassname();
        String section_id=obj.getSectionid();
        String sectionname=obj.getSectionname();
        String staff_name=obj.getStaff_name();
        Intent i = new Intent(ViewClassWiseTest.this, TestResultGDS.class);
        i.putExtra("test_id", test_id);
        i.putExtra("topic", topic);
        i.putExtra("subName", subName);
        i.putExtra("date", date);
        i.putExtra("classname", classname);
        i.putExtra("section_id", section_id);
        i.putExtra("sectionname", sectionname);
        i.putExtra("staff_name", staff_name);
        if (tag.equalsIgnoreCase("sea")){
            i.putExtra("name", tag);
            i.putExtra("exam", obj.getExam());
        }else{
            i.putExtra("name", tag);
            i.putExtra("exam", "");
        }
        startActivity(i);
    }
}
