package com.ultimate.ultimatesmartschool.Transport.DriverNotice;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.Environment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.android.volley.Request;
import com.google.android.material.textfield.TextInputLayout;
import com.ultimate.ultimatesmartschool.BeanModule.User;
import com.ultimate.ultimatesmartschool.Login.LoginActivity;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Transport.TransBeanMod.DriverListBean;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;
import com.ultimate.ultimatesmartschool.Utility.Utils;


import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.Manifest.permission.RECORD_AUDIO;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class ComposeNoticeFrag extends Fragment {
    @BindView(R.id.spinnerselectstff)
    Spinner spinnerstaff;
    @BindView(R.id.edtsubstaff)
    EditText editsubject;
    @BindView(R.id.edtAddmessagestaff)EditText editmessage;
    @BindView(R.id.send_messagestaff)
    Button sendmessage;
    @BindView(R.id.parent)
    RelativeLayout parent;
    @BindView(R.id.inputmessage)
    TextInputLayout inputmessage;
    private int loaded = 0;
    View view;
    String from_id;
    String staff_id;
    ArrayList<DriverListBean> arrayList= new ArrayList<>();
    String to_type="driver";

    Button buttonStart, send, buttonStop, buttonPlayLastRecordAudio, buttonStopPlayingRecording ;
    String AudioSavePathInDevice = null;
    MediaRecorder mediaRecorder ;
    Random random ;
    String RandomAudioFileName = "ABCDEFGHIJKLMNOP";
    public static final int RequestPermissionCode = 1;
    MediaPlayer mediaPlayer ;
    private JSONObject user_obj;

    private final int LOAD_FILE_RESULTS = 1000;
    String fileBase64 = null;
    private Animation animation;
    ImageView imageView;
    CheckBox audioNotice;
    @BindView(R.id.recordlayout)RelativeLayout recordlayout;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        view = inflater.inflate(R.layout.activity_sent_driver_notice, container, false);
        ButterKnife.bind(this, view);

        from_id= User.getCurrentUser().getId();
        fetchdriver();
        audioNotice=(CheckBox)view.findViewById(R.id.checkBoxallstud);

        imageView = (ImageView) view.findViewById(R.id.imageView);
        buttonStart = (Button) view.findViewById(R.id.button);
        buttonStop = (Button) view.findViewById(R.id.button2);
        buttonPlayLastRecordAudio = (Button) view.findViewById(R.id.button3);
        buttonStopPlayingRecording = (Button)view.findViewById(R.id.button4);

        buttonStop.setEnabled(false);
        buttonPlayLastRecordAudio.setEnabled(false);
        buttonStopPlayingRecording.setEnabled(false);

        random = new Random();
        audioNotice.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (audioNotice.isChecked()) {
                    inputmessage.setVisibility(View.GONE);
                    recordlayout.setVisibility(View.VISIBLE);

                } else {

                    inputmessage.setVisibility(View.VISIBLE);
                    recordlayout.setVisibility(View.GONE);
                }

            }
        });
        buttonStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                animation.setRepeatCount(Animation.INFINITE);
                imageView.startAnimation(animation);
                if(checkPermission()) {

                    AudioSavePathInDevice = Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + CreateRandomAudioFileName(5) + "AudioRecording.mp3";

                    MediaRecorderReady();

                    try {
                        mediaRecorder.prepare();
                        mediaRecorder.start();
                    } catch (IllegalStateException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    } catch (IOException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }

                    buttonStart.setEnabled(false);
                    buttonStop.setEnabled(true);

                    Toast.makeText(getContext(), "Recording started", Toast.LENGTH_LONG).show();
                }
                else {

                    requestPermission();

                }

            }
        });

        buttonStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imageView.clearAnimation();
                animation.cancel();
                animation.reset();
                mediaRecorder.stop();

                buttonStop.setEnabled(false);
                buttonPlayLastRecordAudio.setEnabled(true);
                buttonStart.setEnabled(true);
                buttonStopPlayingRecording.setEnabled(false);

                Toast.makeText(getContext(), "Recording Completed", Toast.LENGTH_LONG).show();

            }
        });



        buttonPlayLastRecordAudio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) throws IllegalArgumentException, SecurityException, IllegalStateException {
                animation.setRepeatCount(Animation.INFINITE);
                imageView.startAnimation(animation);
                buttonStop.setEnabled(false);
                buttonStart.setEnabled(false);
                buttonStopPlayingRecording.setEnabled(true);

                mediaPlayer = new MediaPlayer();

                try {
                    mediaPlayer.setDataSource(AudioSavePathInDevice);
                    mediaPlayer.prepare();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                mediaPlayer.start();

                Toast.makeText(getContext(), "Recording Playing", Toast.LENGTH_LONG).show();

            }
        });

        buttonStopPlayingRecording.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imageView.clearAnimation();
                animation.cancel();
                animation.reset();
                buttonStop.setEnabled(false);
                buttonStart.setEnabled(true);
                buttonStopPlayingRecording.setEnabled(false);
                buttonPlayLastRecordAudio.setEnabled(true);

                if(mediaPlayer != null){

                    mediaPlayer.stop();
                    mediaPlayer.release();

                    MediaRecorderReady();

                }

            }
        });
        return view;
    }
    public void MediaRecorderReady(){

        mediaRecorder=new MediaRecorder();

        mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);

        // mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);

        //mediaRecorder.setAudioEncoder(MediaRecorder.OutputFormat.AMR_NB);
        mediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);

        mediaRecorder.setOutputFile(AudioSavePathInDevice);

        if (AudioSavePathInDevice != null) {
            try {
                fileBase64 = Utils.encodeFileToBase64Binary(AudioSavePathInDevice);
            } catch (IOException e) {
                e.printStackTrace();
            }
            Toast.makeText(getActivity(), "File Picked.", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(getActivity(), "Can't Pick this file.", Toast.LENGTH_SHORT).show();
        }

    }

    public String CreateRandomAudioFileName(int string){

        StringBuilder stringBuilder = new StringBuilder( string );

        int i = 0 ;
        while(i < string ) {

            stringBuilder.append(RandomAudioFileName.charAt(random.nextInt(RandomAudioFileName.length())));

            i++ ;
        }
        return stringBuilder.toString();

    }

    private void requestPermission() {

        ActivityCompat.requestPermissions(getActivity(), new String[]{WRITE_EXTERNAL_STORAGE, RECORD_AUDIO}, RequestPermissionCode);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case RequestPermissionCode:
                if (grantResults.length > 0) {

                    boolean StoragePermission = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    boolean RecordPermission = grantResults[1] == PackageManager.PERMISSION_GRANTED;

                    if (StoragePermission && RecordPermission) {

                        Toast.makeText(getActivity(), "Permission Granted", Toast.LENGTH_LONG).show();
                    }
                    else {
                        Toast.makeText(getActivity(),"Permission Denied",Toast.LENGTH_LONG).show();

                    }
                }

                break;
        }
    }
    private void fetchdriver() {
        HashMap<String,String> params=new HashMap<>();
        ApiHandler.apiHit(Request.Method.GET, Constants.getBaseURL() + Constants.DRIVERLIST_URL,apiCallBack,getContext(),null);
    }
    ApiHandler.ApiCallback apiCallBack=new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            if (error==null){
                try{
                    if ( arrayList!= null) {
                        arrayList.clear();
                        arrayList = DriverListBean.parseDriverListArray(jsonObject.getJSONArray("driver_data"));
                        Spinner_drivlist_adapter     adapter = new Spinner_drivlist_adapter(getContext(), arrayList);
                        spinnerstaff.setAdapter(adapter);
                        spinnerstaff.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                if (i > 0) {
                                    staff_id = arrayList.get(i-1).getId();
                                    Log.e("studentss1",staff_id);
                                }else{
                                    staff_id="";
                                }

                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> adapterView) {

                            }
                        });
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    };



    @OnClick(R.id.send_messagestaff)
    public void send_message(){
        if(checkValid()){
            ErpProgress.showProgressBar(getContext(),"Please wait...");
            HashMap<String,String> params=new HashMap<>();
            params.put("from_id",from_id);
            params.put("to_id",staff_id);
            params.put("to_type",to_type);
            params.put("sub",editsubject.getText().toString());

            if (audioNotice.isChecked()) {
                params.put("msg","audio message");
            } else {
                params.put("msg",editmessage.getText().toString());
            }

            if(fileBase64 != null) {
                params.put("recording", fileBase64);
            }
            ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.DRIVERNOTICEURL, apisendmsgCallback, getContext(), params);

        }
    }
    ApiHandler.ApiCallback apisendmsgCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            if (error == null) {
                try {

                    Toast.makeText(getContext(),jsonObject.getString("msg"),Toast.LENGTH_LONG).show();
                    Utils.showSnackBar(jsonObject.getString("msg"), parent);
                    getActivity().finish();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("error", error.getMessage() + "");
                Utils.showSnackBar(error.getMessage(), parent);
                // Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_LONG).show();
                if (error.getStatusCode() == 405) {
                    User.logout();
                    startActivity(new Intent(getContext(), LoginActivity.class));
                    getActivity().finish();
                }
            }
        }
    };



    private boolean checkValid() {

        boolean valid = true;
        String errorMsg = null;
        if (staff_id.isEmpty() || staff_id == "") {
            valid = false;
            errorMsg = "Please select driver";

        }
        else if (editsubject.getText().toString().trim().length() <= 0) {
            valid = false;
            errorMsg = "Please enter subject";
        }

        if (!valid) {
            Utils.showSnackBar(errorMsg, parent);
        }
        return valid;
    }

    public boolean checkPermission() {

        int result = ContextCompat.checkSelfPermission(getContext(), WRITE_EXTERNAL_STORAGE);
        int result1 = ContextCompat.checkSelfPermission(getContext(), RECORD_AUDIO);

        return result == PackageManager.PERMISSION_GRANTED && result1 == PackageManager.PERMISSION_GRANTED;
    }
}
