package com.ultimate.ultimatesmartschool.Transport;

import android.content.Context;
import android.graphics.Typeface;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.ultimate.ultimatesmartschool.BeanModule.CommonBean;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Transport.TransBeanMod.DriverListBean;

import java.util.ArrayList;

public class DriverSpinnerAdapter extends BaseAdapter{

        private final ArrayList<DriverListBean> casteList;
        Context context;
        LayoutInflater inflter;

        public DriverSpinnerAdapter(Context applicationContext, ArrayList<DriverListBean> casteList) {
            this.context = applicationContext;
            this.casteList = casteList;
            inflter = (LayoutInflater.from(applicationContext));
        }

        @Override
        public int getCount() {
            return casteList.size() + 1;
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(final int i, View view, ViewGroup viewGroup) {
            view = inflter.inflate(R.layout.spinner_lyt_group, null);
            TextView label = (TextView) view.findViewById(R.id.txtText);
            if (i == 0) {
                // Default selected Spinner item
                label.setText("Select Vehicle");
                // Make the text bold
                label.setTypeface(null, Typeface.BOLD);
            } else {
                DriverListBean classObj = casteList.get(i - 1);
               // String details=classObj.getV_type()+"-"+classObj.getV_no()+"("+classObj.getDriver_name()+")";

                String title = getColoredSpanned(""+classObj.getV_type().toUpperCase()+"-", "#3e2936");
                String l_Name = getColoredSpanned(""+classObj.getV_no().toUpperCase(), "#1C8B3B");
                String l_Name1 = getColoredSpanned("("+classObj.getDriver_name()+")", "#3e2936");

                label.setText(Html.fromHtml(title + "" + l_Name+ " "+ l_Name1+ ""));
               // label.setText(details);
            }

            return view;
        }
    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }

}
