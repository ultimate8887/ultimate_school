package com.ultimate.ultimatesmartschool.Transport.AllotVicleToRouteMod;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;

import android.os.Bundle;


import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.google.android.material.snackbar.Snackbar;
import com.ultimate.ultimatesmartschool.BeanModule.User;
import com.ultimate.ultimatesmartschool.Login.LoginActivity;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Transport.PickUpMod.PickUpActivity;
import com.ultimate.ultimatesmartschool.Transport.TransBeanMod.AlotVicleToRouteBean;
import com.ultimate.ultimatesmartschool.Transport.TransBeanMod.RouteBean;
import com.ultimate.ultimatesmartschool.Transport.TransBeanMod.VehicleBean;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;
import com.ultimate.ultimatesmartschool.Utility.Utils;


import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class VicleToRouteActivity extends AppCompatActivity implements VicleToRouteAdap.MethodCallback {

    @BindView(R.id.tapedit)
    TextView tapedit;
    Dialog dialog;
    Spinner spinerVehicleNo;
    TextView txtRoutes;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.parentss)
    RelativeLayout parent;
    @BindView(R.id.recycle)
    RecyclerView recycleview;
    RecyclerView.LayoutManager layoutManager;
    private VicleToRouteAdap adap;
    VehicleBean vehicleBean;

    ArrayList<AlotVicleToRouteBean> arrayList = new ArrayList<>();
    ArrayList<VehicleBean> vehicleList = new ArrayList<>();
    ArrayList<RouteBean> routrarrayList = new ArrayList<>();
    Spinner spinevehicle;
    Spinner spinerroute;
    String vehicle_id;
    String route_id;
    String alotroutetovehicle_id;
    AlotVicleToRouteBean alotVicleToRouteBean2;
    @BindView(R.id.totalRecord)
    TextView totalRecord;
    @BindView(R.id.ggg)
    TextView ggg;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_common_lyt);
        ButterKnife.bind(this);
        txtTitle.setText("Allot Vehicle To Route");
        ggg.setText("Tap to Allot Vehicle to Route");
        layoutManager = new LinearLayoutManager(this);
        recycleview.setLayoutManager(layoutManager);
        adap = new VicleToRouteAdap(this, arrayList, this);
        recycleview.setAdapter(adap);
    }

    @Override
    protected void onResume() {
        super.onResume();
        fetchList();
    }

    private void fetchList() {
        ErpProgress.showProgressBar(this,"Please wait...");
        HashMap<String, String> params = new HashMap<>();
        ApiHandler.apiHit(Request.Method.GET, Constants.getBaseURL() + Constants.ALOT_VICLE_ROUTE_URL, apiCallBack, this, null);
    }

    ApiHandler.ApiCallback apiCallBack = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            if (error == null) {
                try {
                    if (arrayList != null) {
                        arrayList.clear();
                        arrayList = AlotVicleToRouteBean.parseVicleToRouteArray(jsonObject.getJSONArray("alotrv_data"));
                        adap.setArrayList(arrayList);
                        totalRecord.setText("Total Entries:- "+String.valueOf(arrayList.size()));
                        adap.notifyDataSetChanged();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            else {
                totalRecord.setText("Total Entries:- 0");
                Toast.makeText(VicleToRouteActivity.this,error.getMessage(),Toast.LENGTH_SHORT).show();
            }
        }
    };

    @OnClick(R.id.imgBack)
    public void imgBackssss() {
        finish();
    }

    @Override
    public void alotVicleRouteCallBack(final AlotVicleToRouteBean vicleToRouteBean,int pos) {
        alotVicleToRouteBean2=vicleToRouteBean;
        Toast.makeText(VicleToRouteActivity.this, "Allot Vehicle to Route", Toast.LENGTH_SHORT).show();

        dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);

        dialog.setContentView(R.layout.add_drivertovehicle_dialog);
        Button btnSubmit = (Button) dialog.findViewById(R.id.btnPickUpSubmit);
        btnSubmit.setText("Update");
        spinevehicle= (Spinner) dialog.findViewById(R.id.spinevehicle);
        spinerroute= (Spinner) dialog.findViewById(R.id.spinerdriver);
        fetchvehicleNoListnewupdt();
        fetchRouteListupdte();
        alotroutetovehicle_id=vicleToRouteBean.getVicltorouteid();
        Toast.makeText(getApplicationContext(),alotroutetovehicle_id,Toast.LENGTH_LONG).show();
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Utils.hideKeyboard(VicleToRouteActivity.this);
                if (checkValidation()) {

                    updateDetail(alotroutetovehicle_id);
                    //updateDetail(alotDrivrToVicleBean);
                    dialog.dismiss();
                    //Toast.makeText(VicleToRouteActivity.this,"Updated Successfully",Toast.LENGTH_SHORT).show();

                }
            }
        });
//        dialog.setContentView(R.layout.alot_common_dialog);
//        txtRoutes = (TextView) dialog.findViewById(R.id.txtTitles);
//        Button btnUpdate = (Button) dialog.findViewById(R.id.btnAlotUpdate);
//        spinerVehicleNo = (Spinner) dialog.findViewById(R.id.spinerDetails);
//        fetchvehicleNoList();
//
//        String title = getColoredSpanned("Route: ", "#7D7D7D");
//        String Name = getColoredSpanned(vicleToRouteBean.getTitle(),"#9c9da1");
//        txtRoutes.setText(Html.fromHtml(title+" "+Name));
//
//        // txtRoutes.setText("Route: "+vicleToRouteBean.getTitle());
//
//        btnUpdate.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Utils.hideKeyboard(VicleToRouteActivity.this);
//                updateDetail(vicleToRouteBean);
//            }
//        });

        dialog.show();
    }

    @Override
    public void addVicleRouteCallBack(AlotVicleToRouteBean vicleToRouteBean, final int pos) {

        final Dialog  dialogs = new Dialog(this);
        dialogs.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogs.setCancelable(true);
        dialogs.setContentView(R.layout.add_drivertovehicle_dialog);

        Button btnSubmit = (Button) dialogs.findViewById(R.id.btnPickUpSubmit);
        spinevehicle= (Spinner) dialogs.findViewById(R.id.spinevehicle);
        spinerroute= (Spinner) dialogs.findViewById(R.id.spinerdriver);
        fetchvehicleNoListnew();
        fetchRouteList();
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Utils.hideKeyboard(VicleToRouteActivity.this);
                if (checkValidation()) {
                    if (pos == -1) {

                        addNewRoutetoVehicle();
                        dialogs.dismiss();
                      //  Toast.makeText(VicleToRouteActivity.this,"Add Successfully",Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
        dialogs.show();


    }

    private void addNewRoutetoVehicle() {
        HashMap<String,String> params=new HashMap<String, String>();

        params.put("v_id",vehicle_id);
        params.put("r_id",route_id);

        ApiHandler.apiHit(Request.Method.POST,Constants.getBaseURL() + Constants.ALOT_VICLE_ROUTE_URL,apiCallBackadtv,VicleToRouteActivity.this,params);

    }

    ApiHandler.ApiCallback apiCallBackadtv = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            if (error == null) {
                try {
                    Toast.makeText(getApplicationContext(),jsonObject.getString("msg"),Toast.LENGTH_LONG).show();
                    Utils.showSnackBar(jsonObject.getString("msg"), parent);
                    finish();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("error", error.getMessage() + "");
                Utils.showSnackBar(error.getMessage(), parent);
                // Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_LONG).show();
                if (error.getStatusCode() == 405) {
                    User.logout();
                    startActivity(new Intent(VicleToRouteActivity.this, LoginActivity.class));
                    finish();
                }
            }
        }
    };
    private boolean checkValidation() {

        boolean valid = true;
        String errorMsg = null;
        if (vehicle_id == null || vehicle_id.isEmpty()) {
            errorMsg = "Please select vehicle";
            valid = false;
        } else if (route_id == null || route_id.isEmpty()) {
            valid = false;
            errorMsg = "Please enter route";
        }
        if (!valid) {
            showSnackBar(errorMsg);
        }
        return valid;
    }
    private void showSnackBar(String errorMsg) {

        Snackbar snack = Snackbar.make(parent, errorMsg, Snackbar.LENGTH_LONG);
        View view = snack.getView();
//        TextView tv = (TextView) view.findViewById(R.id.snackbar_text);
//        tv.setTextColor(Color.WHITE);
        snack.show();
    }
    // it is used for set multiple color on single textview acc user requirments.....
    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }

    private void updateDetail(String alotroutetovehicle_id) {

        HashMap<String, String> params = new HashMap<>();

        params.put("alotroutetovehicle_id", alotroutetovehicle_id);
        params.put("r_id", route_id);
        params.put("v_id", vehicle_id);
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.ALOT_VICLE_ROUTE_URL, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                if (error == null) {
                    fetchList();

                } else {
                    Utils.showSanckBar(parent, error.getMessage());
                }
            }
        }, this, params);
       // ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.ALOT_VICLE_ROUTE_URL, apiCall, VicleToRouteActivity.this, params);
      }

//     ApiHandler.ApiCallback apiCall = new ApiHandler.ApiCallback() {
//        @Override
//        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
//            if (error == null) {
//                dialog.dismiss();
//                try {
//                    if (arrayList != null) {
//                        arrayList.clear();
//                        arrayList = AlotVicleToRouteBean.parseVicleToRouteArray(jsonObject.getJSONArray("alotrv_data"));
//                        adap.setArrayList(arrayList);
//                        adap.notifyDataSetChanged();
//                        Toast.makeText(VicleToRouteActivity.this,"Alloted Successfully",Toast.LENGTH_SHORT).show();
//                    }
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//
//            } else {
//                Toast.makeText(VicleToRouteActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
//            }
//        }
//     };

    private void fetchvehicleNoList() {

        HashMap<String, String> params = new HashMap<>();
        ApiHandler.apiHit(Request.Method.GET, Constants.getBaseURL() + Constants.VEHICLE_URL, apicallback, this, null);
    }

    ApiHandler.ApiCallback apicallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            if (error == null) {
                try {
                    vehicleList = VehicleBean.parseVehicleArray(jsonObject.getJSONArray("vehical_data"));
                    VehicleNoSpinnrAdap adap1 = new VehicleNoSpinnrAdap(VicleToRouteActivity.this, vehicleList);
                    spinerVehicleNo.setAdapter(adap1);
                    spinerVehicleNo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                            if (i > 0) {
                                vehicleBean = vehicleList.get(i - 1);
                            }
                          }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    };
    private void fetchvehicleNoListnew() {

        HashMap<String, String> params = new HashMap<>();
        ApiHandler.apiHit(Request.Method.GET, Constants.getBaseURL() + Constants.VEHICLE_URL, apicallbacknew, this, null);
    }

    ApiHandler.ApiCallback apicallbacknew = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            if (error == null) {
                try {
                    vehicleList = VehicleBean.parseVehicleArray(jsonObject.getJSONArray("vehical_data"));
                    VehicleNoSpinnrAdap adap1 = new VehicleNoSpinnrAdap(VicleToRouteActivity.this, vehicleList);
                    spinevehicle.setAdapter(adap1);
                    spinevehicle.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                            if (i > 0) {
                                vehicleBean = vehicleList.get(i - 1);
                                vehicle_id=vehicleList.get(i-1).getEs_transportid();
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    };

    private void fetchRouteList() {

        HashMap<String, String> params = new HashMap<>();
        ApiHandler.apiHit(Request.Method.GET, Constants.getBaseURL() + Constants.SCHOOL_ROUTE_URL, apiCallBacknew, this, null);
    }

    ApiHandler.ApiCallback apiCallBacknew = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            if (error == null) {
                try {
                    if (routrarrayList != null)
                        routrarrayList.clear();
                    routrarrayList = RouteBean.parseTransportArray(jsonObject.getJSONArray("school_route_data"));
                    RouteNoSpinnrAdap adap1 = new RouteNoSpinnrAdap(VicleToRouteActivity.this, routrarrayList);
                    spinerroute.setAdapter(adap1);
                    spinerroute.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                            if (i > 0) {
                                //vehicleBean = vehicleList.get(i - 1);
                                route_id=routrarrayList.get(i-1).getId();
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            } else {
                Toast.makeText(VicleToRouteActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
    };
    private void fetchvehicleNoListnewupdt() {

        HashMap<String, String> params = new HashMap<>();
        ApiHandler.apiHit(Request.Method.GET, Constants.getBaseURL() + Constants.VEHICLE_URL, apicallbacknewupdt, this, null);
    }

    ApiHandler.ApiCallback apicallbacknewupdt = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            if (error == null) {
                try {
                    vehicleList = VehicleBean.parseVehicleArray(jsonObject.getJSONArray("vehical_data"));
                    VehicleNoSpinnrAdap adap1 = new VehicleNoSpinnrAdap(VicleToRouteActivity.this, vehicleList);
                    spinevehicle.setAdapter(adap1);
                    spinevehicle.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                            if (i > 0) {
                                vehicleBean = vehicleList.get(i - 1);
                                vehicle_id=vehicleList.get(i-1).getEs_transportid();
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });
                    int i = 0;
                    for ( VehicleBean obj : vehicleList) {
                        if (obj.getTr_vehicle_no().equals(alotVicleToRouteBean2.getV_no())) {
                            spinevehicle.setSelection(i + 1);
                            break;
                        }
                        i++;
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    };

    private void fetchRouteListupdte() {

        HashMap<String, String> params = new HashMap<>();
        ApiHandler.apiHit(Request.Method.GET, Constants.getBaseURL() + Constants.SCHOOL_ROUTE_URL, apiCallBacknewupdate, this, null);
    }

    ApiHandler.ApiCallback apiCallBacknewupdate = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            if (error == null) {
                try {
                    if (routrarrayList != null)
                        routrarrayList.clear();
                    routrarrayList = RouteBean.parseTransportArray(jsonObject.getJSONArray("school_route_data"));
                    RouteNoSpinnrAdap adap1 = new RouteNoSpinnrAdap(VicleToRouteActivity.this, routrarrayList);
                    spinerroute.setAdapter(adap1);
                    spinerroute.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                            if (i > 0) {
                                //vehicleBean = vehicleList.get(i - 1);
                                route_id=routrarrayList.get(i-1).getId();
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                    int i = 0;
                    for ( RouteBean obj : routrarrayList) {
                        if (obj.getRoute_title().equals(alotVicleToRouteBean2.getRoute_title())) {
                            spinerroute.setSelection(i + 1);
                            break;
                        }
                        i++;
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            } else {
                Toast.makeText(VicleToRouteActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
    };
}
