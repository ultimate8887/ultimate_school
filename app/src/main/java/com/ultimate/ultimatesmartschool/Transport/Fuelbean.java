package com.ultimate.ultimatesmartschool.Transport;


import com.ultimate.ultimatesmartschool.Utility.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Fuelbean {

    private String d_id;
    private String id;
    private String date;

    private String image;

    private String d_name;


    public String getD_id() {
        return d_id;
    }

    public void setD_id(String d_id) {
        this.d_id = d_id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getD_name() {
        return d_name;
    }

    public void setD_name(String d_name) {
        this.d_name = d_name;
    }

    public String getRoute_no() {
        return route_no;
    }

    public void setRoute_no(String route_no) {
        this.route_no = route_no;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getFuel_price() {
        return fuel_price;
    }

    public void setFuel_price(String fuel_price) {
        this.fuel_price = fuel_price;
    }

    public String getMorn_reading() {
        return morn_reading;
    }

    public void setMorn_reading(String morn_reading) {
        this.morn_reading = morn_reading;
    }

    public String getMorn_reading_img() {
        return morn_reading_img;
    }

    public void setMorn_reading_img(String morn_reading_img) {
        this.morn_reading_img = morn_reading_img;
    }

    public String getEvng_reading() {
        return evng_reading;
    }

    public void setEvng_reading(String evng_reading) {
        this.evng_reading = evng_reading;
    }

    public String getEvng_reading_img() {
        return evng_reading_img;
    }

    public void setEvng_reading_img(String evng_reading_img) {
        this.evng_reading_img = evng_reading_img;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreate() {
        return create;
    }

    public void setCreate(String create) {
        this.create = create;
    }

    private String route_no;

    public String getRoute_name() {
        return route_name;
    }

    public void setRoute_name(String route_name) {
        this.route_name = route_name;
    }

    private String route_name;

    private String amount;

    private String fuel_price;
    private String morn_reading;

    private String morn_reading_img;

    private String evng_reading;
    private String evng_reading_img;

    private String status;

    private String create;


//    Now there is implementing all the ApiHandler working.............................

    public static ArrayList<Fuelbean> parseNoticetArray(JSONArray arrayObj) {
        ArrayList list = new ArrayList<Fuelbean>();
        try {
            for (int i = 0; i < arrayObj.length(); i++) {
                Fuelbean p = parseNoticeObject(arrayObj.getJSONObject(i));
                if (p != null) {
                    list.add(p);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public static Fuelbean parseNoticeObject(JSONObject jsonObject) {
        Fuelbean casteObj = new Fuelbean();
        try {



            if (jsonObject.has("id") && !jsonObject.getString("id").isEmpty() && !jsonObject.getString("id").equalsIgnoreCase("null")) {
                casteObj.setId(jsonObject.getString("id"));
            }

            if (jsonObject.has("route_name") && !jsonObject.getString("route_name").isEmpty() && !jsonObject.getString("route_name").equalsIgnoreCase("null")) {
                casteObj.setRoute_name(jsonObject.getString("route_name"));
            }


            if (jsonObject.has("d_id") && !jsonObject.getString("d_id").isEmpty() && !jsonObject.getString("d_id").equalsIgnoreCase("null")) {
                casteObj.setD_id(jsonObject.getString("d_id"));
            }
            if (jsonObject.has("image") && !jsonObject.getString("image").isEmpty() && !jsonObject.getString("image").equalsIgnoreCase("null")) {
                casteObj.setImage(Constants.getImageBaseURL()+jsonObject.getString("image"));
            }

            if (jsonObject.has("d_name") && !jsonObject.getString("d_name").isEmpty() && !jsonObject.getString("d_name").equalsIgnoreCase("null")) {
                casteObj.setD_name(jsonObject.getString("d_name"));
            }

            if (jsonObject.has("route_no") && !jsonObject.getString("route_no").isEmpty() && !jsonObject.getString("route_no").equalsIgnoreCase("null")) {
                casteObj.setRoute_no(jsonObject.getString("route_no"));
            }

            if (jsonObject.has("amount") && !jsonObject.getString("amount").isEmpty() && !jsonObject.getString("amount").equalsIgnoreCase("null")) {
                casteObj.setAmount(jsonObject.getString("amount"));
            }

            if (jsonObject.has("fuel_price") && !jsonObject.getString("fuel_price").isEmpty() && !jsonObject.getString("fuel_price").equalsIgnoreCase("null")) {
                casteObj.setFuel_price(jsonObject.getString("fuel_price"));
            }



            if (jsonObject.has("morn_reading") && !jsonObject.getString("morn_reading").isEmpty() && !jsonObject.getString("morn_reading").equalsIgnoreCase("null")) {
                casteObj.setMorn_reading(jsonObject.getString("morn_reading"));
            }

            if (jsonObject.has("morn_reading_img") && !jsonObject.getString("morn_reading_img").isEmpty() && !jsonObject.getString("morn_reading_img").equalsIgnoreCase("null")) {
                casteObj.setMorn_reading_img(Constants.getImageBaseURL()+jsonObject.getString("morn_reading_img"));
            }


            if (jsonObject.has("evng_reading") && !jsonObject.getString("evng_reading").isEmpty() && !jsonObject.getString("evng_reading").equalsIgnoreCase("null")) {
                casteObj.setEvng_reading(jsonObject.getString("evng_reading"));
            }

            if (jsonObject.has("evng_reading_img") && !jsonObject.getString("evng_reading_img").isEmpty() && !jsonObject.getString("evng_reading_img").equalsIgnoreCase("null")) {
                casteObj.setEvng_reading_img(Constants.getImageBaseURL()+jsonObject.getString("evng_reading_img"));
            }

            if (jsonObject.has("status") && !jsonObject.getString("status").isEmpty() && !jsonObject.getString("status").equalsIgnoreCase("null")) {
                casteObj.setStatus(jsonObject.getString("status"));
            }

            if (jsonObject.has("create") && !jsonObject.getString("create").isEmpty() && !jsonObject.getString("create").equalsIgnoreCase("null")) {
                casteObj.setCreate(jsonObject.getString("create"));
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }
        return casteObj;
    }

}
