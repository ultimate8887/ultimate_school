package com.ultimate.ultimatesmartschool.Transport.DriverListMod;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;

import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.android.volley.Request;

import com.google.android.material.snackbar.Snackbar;

import com.ultimate.ultimatesmartschool.ForCamera.IPickResult;
import com.ultimate.ultimatesmartschool.ForCamera.PickImageDialog;
import com.ultimate.ultimatesmartschool.ForCamera.PickResult;
import com.ultimate.ultimatesmartschool.ForCamera.PickSetup;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Transport.TransBeanMod.DriverListBean;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class AddDriverActivity extends AppCompatActivity implements IPickResult {


    @BindView(R.id.edit_image)
    ImageView edtProfImage;
    @BindView(R.id.edit_image1)
    ImageView edit_image1;
    @BindView(R.id.doc_image)
    ImageView edtImageDoc;
    @BindView(R.id.addImg)
    ImageView addImg;
    @BindView(R.id.imgCal3)
    RelativeLayout imgValidUpto;
    @BindView(R.id.edtDriverName)
    EditText edtDriverName;
    @BindView(R.id.edtDriverld)
    EditText edtDriverld;
    @BindView(R.id.edtDriverpassword)
    EditText edtDriverpassword;
    @BindView(R.id.edtDriverAdd)
    EditText edtDriverAdd;
    @BindView(R.id.edtMobile)
    EditText edtMobile;
    public String validDate="";
    @BindView(R.id.edtDriverLicense)
    EditText edtDriverLicense;
    @BindView(R.id.edtIssuing)
    EditText edtIssuing;
    @BindView(R.id.edtValidUpto)
    TextView edtValidUpto;
    @BindView(R.id.roots)
    RelativeLayout parent;
    @BindView(R.id.btnDriverSubmit)
    Button btnDriverSubmit;
    private int selection;
    private Bitmap userBitmap, userBitmapProf;
    ArrayList<DriverListBean> arrayList=new ArrayList<>();
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    Animation animation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_edit_driver);
        ButterKnife.bind(this);
        edtValidUpto.setEnabled(false);
        txtTitle.setText("Add Driver");
    }
    @OnClick(R.id.imgBack)
    public void imgBackssss() {
        finish();
    }
    @OnClick(R.id.imgCal3)
    public void imgReneDate() {

        java.util.Calendar cal = java.util.Calendar.getInstance(TimeZone.getDefault()); // Get current date
        DatePickerDialog datePicker = new DatePickerDialog(this,
                R.style.MyDatePickerDialogTheme, ondateRene,
                cal.get(java.util.Calendar.YEAR),
                cal.get(java.util.Calendar.MONTH),
                cal.get(java.util.Calendar.DAY_OF_MONTH));
        datePicker.getDatePicker();
        datePicker.setCancelable(false);
        datePicker.show();
    }
    DatePickerDialog.OnDateSetListener ondateRene = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            java.util.Calendar calendar = java.util.Calendar.getInstance(TimeZone.getDefault());
            calendar.set(year, monthOfYear, dayOfMonth);
            Date dbirthdate = calendar.getTime();
            Calendar value_datePicker = calendar;
            SimpleDateFormat fmtOut = new SimpleDateFormat("yyyy-MM-dd");
            String dateString = fmtOut.format(dbirthdate);
            validDate=dateString;
            edtValidUpto.setText(Utils.getDateFormated(dateString));
        }
    };


    @RequiresApi(api = Build.VERSION_CODES.M)
    @OnClick(R.id.edit_image1)
    public void addDoc() {

        animation = AnimationUtils.loadAnimation(this, R.anim.btn_blink_animation);
        edit_image1.startAnimation(animation);

        selection=1;
        pick_img();

    }

        @RequiresApi(api = Build.VERSION_CODES.M)
   @OnClick(R.id.addImg)
    public void addImg() {

            animation = AnimationUtils.loadAnimation(this, R.anim.btn_blink_animation);
            addImg.startAnimation(animation);
       selection=2;

      pick_img();
   }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @SuppressLint("ResourceType")
    private void pick_img() {

            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.S) {
                if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_MEDIA_IMAGES)
                        != PackageManager.PERMISSION_GRANTED) {

                    if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_MEDIA_IMAGES)) {
                        Toast.makeText(this, "Please provide storage permission from app settings", Toast.LENGTH_LONG).show();
                    } else {

                        ActivityCompat.requestPermissions(this,
                                new String[]{Manifest.permission.READ_MEDIA_IMAGES},
                                1);
                    }
                } else {

                    onImageViewClick();
                }

            } else {
                if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {

                    if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                        Toast.makeText(this, "Please provide storage permission from app settings", Toast.LENGTH_LONG).show();
                    } else {

                        ActivityCompat.requestPermissions(this,
                                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                                1);
                    }
                } else {
                    onImageViewClick();

                }

        }
    }

    private void onImageViewClick() {
        PickSetup setup = new PickSetup();
        // super.customize(setup);
        PickImageDialog.build(setup)
                //.setOnClick(this)
                .show(this);
    }

    @Override
    public void onPickResult(PickResult r) {
        if (r.getError() == null) {
            //If you want the Bitmap.
            if (selection==1) {
                edtImageDoc.setImageBitmap(r.getBitmap());
                userBitmap = r.getBitmap();
            }

            if (selection==2) {
                edtProfImage.setImageBitmap(r.getBitmap());
                userBitmapProf = r.getBitmap();
                //  encoded_father = Utility.encodeToBase64(profileBitmap_father, Bitmap.CompressFormat.JPEG, 50);
            }


            //r.getPath();
        } else {
            //Handle possible errors
            //TODO: do what you have to do with r.getError();
            Toast.makeText(this, r.getError().getMessage(), Toast.LENGTH_LONG).show();
        }


    }


//    @Override
//    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        if (resultCode == Activity.RESULT_OK) {
//            switch (requestCode) {
//                case CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE:
//                    Uri imageUri = CropImage.getPickImageResultUri(AddDriverActivity.this, data);
//                    CropImage.activity(imageUri).setCropShape(CropImageView.CropShape.RECTANGLE).setAspectRatio(1, 1)
//                            .start(AddDriverActivity.this);
//                    break;
//
//                case CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE:
//                    CropImage.ActivityResult result = CropImage.getActivityResult(data);
//                    Uri resultUri = result.getUri();
//                    if (selection == 1) {
//                        userBitmap = Utils.getBitmapFromUri(AddDriverActivity.this, resultUri, 2048);
//                        edtImageDoc.setImageBitmap(userBitmap);
//                    }
//                    if (selection == 2) {
//
//                        userBitmapProf = Utils.getBitmapFromUri(AddDriverActivity.this, resultUri, 2048);
//                        edtProfImage.setImageBitmap(userBitmapProf);
//                    }
//            }
//        }
//    }

   @OnClick(R.id.btnDriverSubmit)
    public void btnDriSubmit(){
       if (checkValidation()){
           ErpProgress.showProgressBar(this,"Please wait...");
           HashMap<String,String> params=new HashMap<String, String>();
           params.put("address",edtDriverAdd.getText().toString());
           params.put("name",edtDriverName.getText().toString());
           params.put("check", "add");
           params.put("mobile",edtMobile.getText().toString());
           params.put("dlno",edtDriverLicense.getText().toString());
           params.put("issue_auth",edtIssuing.getText().toString());
           params.put("username",edtDriverld.getText().toString());
           params.put("password",edtDriverpassword.getText().toString());
           params.put("valid",validDate);

           if (userBitmap != null) {
               String encoded = Utils.encodeToBase64(userBitmap, Bitmap.CompressFormat.JPEG, 90);
               encoded = String.format("data:image/jpeg;base64,%s", encoded);
               params.put("image", encoded);
           }
           if (userBitmapProf != null) {
               String encoded = Utils.encodeToBase64(userBitmapProf, Bitmap.CompressFormat.JPEG, 90);
               encoded = String.format("data:image/jpeg;base64,%s", encoded);
               params.put("dimage", encoded);
           }
           ApiHandler.apiHit(Request.Method.POST,Constants.getBaseURL() + Constants.DRIVERLIST_URL,apicallBack,this,params);
       }
   }

   ApiHandler.ApiCallback apicallBack = new ApiHandler.ApiCallback() {
       @Override
       public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
           ErpProgress.cancelProgressBar();
            if (error==null){
                try {
                    arrayList=DriverListBean.parseDriverListArray(jsonObject.getJSONArray("driver.php"));

                } catch (JSONException e) {
                    e.printStackTrace();
                } Toast.makeText(AddDriverActivity.this,"add successfully",Toast.LENGTH_SHORT).show();
                finish();
               } else {
                 Toast.makeText(AddDriverActivity.this,error.getMessage(),Toast.LENGTH_SHORT).show();
              }
            }
         };
    private boolean checkValidation() {

        boolean valid = true;
        String errorMsg = null;
        if (edtDriverName.getText().toString().trim().length() <= 0) {
            valid = false;
            errorMsg = "Please enter name";
        } else if (edtDriverld.getText().toString().trim().length() <= 0) {
            valid = false;
            errorMsg = "Please enter id";
        }
        else if (edtDriverAdd.getText().toString().trim().length() <= 0) {
            valid = false;
            errorMsg = "Please enter address";
        }
        if (edtDriverpassword.getText().toString().trim().length() <= 0) {
            valid = false;
            errorMsg = "Please enter password";
        } else if (edtMobile.getText().toString().trim().length() <= 0) {
            valid = false;
            errorMsg = "Please enter contact no.";
        }
        else if (edtDriverLicense.getText().toString().trim().length() <= 0) {
            valid = false;
            errorMsg = "Please enter liecense no.";
        }
        else if (edtIssuing.getText().toString().trim().length() <= 0) {
            valid = false;
            errorMsg = "Please enter issuing authority";
        }
        else if (userBitmap == null) {
            valid = false;
            errorMsg = "Please add liecense document image";
        }
        else if (userBitmapProf == null) {
            valid = false;
            errorMsg = "Please add Driver image";
        }
        else if (validDate == null) {
            valid = false;
            errorMsg = "Please enter valid Upto date";
         }

        if (!valid) {
            showSnackBar(errorMsg);
        }
        return valid;

    }
    private void showSnackBar(String errorMsg) {
        Snackbar snack = Snackbar.make(parent, errorMsg, Snackbar.LENGTH_LONG);
        View view = snack.getView();
//        TextView tv = (TextView) view.findViewById(R.id.snackbar_text);
//        tv.setTextColor(Color.WHITE);
        snack.show();
    }
}
