package com.ultimate.ultimatesmartschool.Transport.driverlocation

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Transformations.map
import com.example.easywaylocation.draw_path.DirectionUtil
import com.example.easywaylocation.draw_path.PolyLineDataBean
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.ultimate.ultimatesmartschool.R


class MapsActivity : AppCompatActivity(), OnMapReadyCallback , DirectionUtil.DirectionCallBack {
    override fun pathFindFinish(polyLineDetails: HashMap<String, PolyLineDataBean>) {
        for (i in polyLineDetails.keys){

        }
    }

    private lateinit var mMap: GoogleMap
    private var wayPoints:ArrayList<LatLng> = ArrayList()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
                .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)


    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID)
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(37.423669, -122.090168),16F))


        wayPoints.add(LatLng(37.423669, -122.090168))
        wayPoints.add(LatLng(37.420930, -122.085362))

        val directionUtil = DirectionUtil.Builder()
                .setDirectionKey("AIzaSyBiqpuSpvj04re0W6x14ZvOMKWEEMCzi7Q")
                .setOrigin(LatLng(37.421481, -122.092156))
                .setWayPoints(wayPoints)
                .setGoogleMap(mMap)
                .setPathAnimation(true)
                .setPolyLineWidth(12)
                .setCallback(this)
                .setDestination(LatLng(37.421519, -122.086809))
                .build()

        directionUtil.drawPath()
    }


}