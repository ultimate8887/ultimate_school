package com.ultimate.ultimatesmartschool.Transport.TransBeanMod;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by admin on 6/21/2018.
 */

public class DriverListBean {


    private static String ID = "id";
    private static String DRIVER_ADD = "driver_addrs";
    private static String DRIVER_MOBILE = "diver_mobile";
    private static String DRIVER_NAME = "driver_name";
    private static String ISSUE_AUTH = "issuing_authority";
    private static String DRIVER_LICENSE = "driver_license";
    private static String VALID_TO = "valid_date";
    private static String DRIVER_ID = "driver_id";
    private static String STATUS = "status";
    private static String LICENSE_DOC = "license_doc";
    private static String USERNAME = "username";
    private static String PASSWORD = "password";
    private static String IMG = "image";

    /**
     * id : 1
     * driver_name : nnnnnsdsfsdf
     * driver_addrs : dfsdfsdfsdfsdfsdf
     * diver_mobile : 9878987897
     * driver_license : dw5de5d
     * issuing_authority : ddekjdwjehdej
     * valid_date : 1970-01-01
     * driver_id : 0
     * status : Active
     * license_doc : office_admin/images/dirverdoc/driver_dl__1_mob.jpg
     * username : 0
     * password : 0
     */

    private String id;
    private String driver_name;
    private String driver_addrs;
    private String diver_mobile;
    private String driver_license;
    private String issuing_authority;
    private String valid_date;
    private String driver_id;
    private String status;
    private String license_doc;
    private String username;
    private String password;

    public String getV_no() {
        return v_no;
    }

    public void setV_no(String v_no) {
        this.v_no = v_no;
    }

    public String getV_titile() {
        return v_titile;
    }

    public void setV_titile(String v_titile) {
        this.v_titile = v_titile;
    }

    public String getV_type() {
        return v_type;
    }

    public void setV_type(String v_type) {
        this.v_type = v_type;
    }

    private String v_no;
    private String v_titile;
    private String v_type;

    public String getVehicle_id() {
        return vehicle_id;
    }

    public void setVehicle_id(String vehicle_id) {
        this.vehicle_id = vehicle_id;
    }

    private String vehicle_id;

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    private String image;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDriver_name() {
        return driver_name;
    }

    public void setDriver_name(String driver_name) {
        this.driver_name = driver_name;
    }

    public String getDriver_addrs() {
        return driver_addrs;
    }

    public void setDriver_addrs(String driver_addrs) {
        this.driver_addrs = driver_addrs;
    }

    public String getDiver_mobile() {
        return diver_mobile;
    }

    public void setDiver_mobile(String diver_mobile) {
        this.diver_mobile = diver_mobile;
    }

    public String getDriver_license() {
        return driver_license;
    }

    public void setDriver_license(String driver_license) {
        this.driver_license = driver_license;
    }

    public String getIssuing_authority() {
        return issuing_authority;
    }

    public void setIssuing_authority(String issuing_authority) {
        this.issuing_authority = issuing_authority;
    }

    public String getValid_date() {
        return valid_date;
    }

    public void setValid_date(String valid_date) {
        this.valid_date = valid_date;
    }

    public String getDriver_id() {
        return driver_id;
    }

    public void setDriver_id(String driver_id) {
        this.driver_id = driver_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getLicense_doc() {
        return license_doc;
    }

    public void setLicense_doc(String license_doc) {
        this.license_doc = license_doc;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public static ArrayList<DriverListBean> parseDriverListArray(JSONArray arrayObj) {
        ArrayList list = new ArrayList<DriverListBean>();
        try {
            for (int i = 0; i < arrayObj.length(); i++) {
                DriverListBean p = parseDriverListBeanObject(arrayObj.getJSONObject(i));
                if (p != null) {
                    list.add(p);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    private static DriverListBean parseDriverListBeanObject(JSONObject jsonObject) {

        DriverListBean casteObj= new DriverListBean();
        try {
            if (jsonObject.has(ID)) {
                casteObj.setId(jsonObject.getString(ID));
            }
            if (jsonObject.has(DRIVER_ADD) && !jsonObject.getString(DRIVER_ADD).isEmpty() && !jsonObject.getString(DRIVER_ADD).equalsIgnoreCase("null")) {
                casteObj.setDriver_addrs(jsonObject.getString(DRIVER_ADD));
            }
            if (jsonObject.has(DRIVER_MOBILE) && !jsonObject.getString(DRIVER_MOBILE).isEmpty() && !jsonObject.getString(DRIVER_MOBILE).equalsIgnoreCase("null")) {
                casteObj.setDiver_mobile(jsonObject.getString(DRIVER_MOBILE));
            }
            if (jsonObject.has(DRIVER_NAME) && !jsonObject.getString(DRIVER_NAME).isEmpty() && !jsonObject.getString(DRIVER_NAME).equalsIgnoreCase("null")) {
                casteObj.setDriver_name(jsonObject.getString(DRIVER_NAME));
            }
            if (jsonObject.has(ISSUE_AUTH) && !jsonObject.getString(ISSUE_AUTH).isEmpty() && !jsonObject.getString(ISSUE_AUTH).equalsIgnoreCase("null")) {
                casteObj.setIssuing_authority(jsonObject.getString(ISSUE_AUTH));
            }
            if (jsonObject.has(DRIVER_LICENSE) && !jsonObject.getString(DRIVER_LICENSE).isEmpty() && !jsonObject.getString(DRIVER_LICENSE).equalsIgnoreCase("null")) {
                casteObj.setDriver_license(jsonObject.getString(DRIVER_LICENSE));
            }
            if (jsonObject.has(DRIVER_ID) && !jsonObject.getString(DRIVER_ID).isEmpty() && !jsonObject.getString(DRIVER_ID).equalsIgnoreCase("null")) {
                casteObj.setDriver_id(jsonObject.getString(DRIVER_ID));
            }
            if (jsonObject.has(VALID_TO) && !jsonObject.getString(VALID_TO).isEmpty() && !jsonObject.getString(VALID_TO).equalsIgnoreCase("null")) {
                casteObj.setValid_date(jsonObject.getString(VALID_TO));
            }
            if (jsonObject.has(PASSWORD) && !jsonObject.getString(PASSWORD).isEmpty() && !jsonObject.getString(PASSWORD).equalsIgnoreCase("null")) {
                casteObj.setPassword(jsonObject.getString(PASSWORD));
            }
            if (jsonObject.has(IMG) && !jsonObject.getString(IMG).isEmpty() && !jsonObject.getString(IMG).equalsIgnoreCase("null")) {
                casteObj.setImage(jsonObject.getString(IMG));
            }

            if (jsonObject.has(LICENSE_DOC) && !jsonObject.getString(LICENSE_DOC).isEmpty() && !jsonObject.getString(LICENSE_DOC).equalsIgnoreCase("null")) {
                casteObj.setLicense_doc(jsonObject.getString(LICENSE_DOC));
            }
            if (jsonObject.has(USERNAME) && !jsonObject.getString(USERNAME).isEmpty() && !jsonObject.getString(USERNAME).equalsIgnoreCase("null")) {
                casteObj.setUsername(jsonObject.getString(USERNAME));
            }
            if (jsonObject.has(STATUS) && !jsonObject.getString(STATUS).isEmpty() && !jsonObject.getString(STATUS).equalsIgnoreCase("null")) {
                casteObj.setStatus(jsonObject.getString(STATUS));
            }
            if (jsonObject.has("vehicle_id") && !jsonObject.getString("vehicle_id").isEmpty() && !jsonObject.getString("vehicle_id").equalsIgnoreCase("null")) {
                casteObj.setVehicle_id(jsonObject.getString("vehicle_id"));
            }
            if (jsonObject.has("v_no") && !jsonObject.getString("v_no").isEmpty() && !jsonObject.getString("v_no").equalsIgnoreCase("null")) {
                casteObj.setV_no(jsonObject.getString("v_no"));
            }
            if (jsonObject.has("v_titile") && !jsonObject.getString("v_titile").isEmpty() && !jsonObject.getString("v_titile").equalsIgnoreCase("null")) {
                casteObj.setV_titile(jsonObject.getString("v_titile"));
            }
            if (jsonObject.has("v_type") && !jsonObject.getString("v_type").isEmpty() && !jsonObject.getString("v_type").equalsIgnoreCase("null")) {
                casteObj.setV_type(jsonObject.getString("v_type"));
            }else{
                casteObj.setV_type("Bus");
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return casteObj;
    }



}
