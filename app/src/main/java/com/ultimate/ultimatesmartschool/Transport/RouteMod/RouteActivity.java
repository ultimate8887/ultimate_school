package com.ultimate.ultimatesmartschool.Transport.RouteMod;

import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;

import android.os.Bundle;

import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.google.android.material.snackbar.Snackbar;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Transport.TransBeanMod.RouteBean;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;
import com.ultimate.ultimatesmartschool.Utility.Utils;


import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RouteActivity extends AppCompatActivity implements RouteAdap.CallBackMethod {

    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.recycle)
    RecyclerView recycleview;
    RecyclerView.LayoutManager layoutManager;
    ArrayList<RouteBean> arrayList = new ArrayList<>();
    private RouteAdap adap;
    @BindView(R.id.parentss)
    RelativeLayout parent;
    Dialog dialog;
    @BindView(R.id.imgBack)
    ImageView imageBack;
    EditText edtRouteTitle,edtRoteCap;
    @BindView(R.id.totalRecord)
    TextView totalRecord;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_common_lyt);
        ButterKnife.bind(this);
        txtTitle.setText("Route List");
        layoutManager = new LinearLayoutManager(this);
        recycleview.setLayoutManager(layoutManager);
        adap = new RouteAdap(arrayList, this, this);
        recycleview.setAdapter(adap);
        fetchRouteList();
    }

    @OnClick(R.id.imgBack)
    public void imgBackssss() {
        finish();
    }

    private void fetchRouteList() {
        ErpProgress.showProgressBar(this,"Please wait...");
        HashMap<String, String> params = new HashMap<>();
        ApiHandler.apiHit(Request.Method.GET, Constants.getBaseURL() + Constants.SCHOOL_ROUTE_URL, apiCallBack, this, null);
    }

    ApiHandler.ApiCallback apiCallBack = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            if (error == null) {
                try {
                    if (arrayList != null)
                        arrayList.clear();
                    arrayList = RouteBean.parseTransportArray(jsonObject.getJSONArray("school_route_data"));
                    adap.setArrayList(arrayList);
                    adap.notifyDataSetChanged();
                    totalRecord.setText("Total Entries:- "+String.valueOf(arrayList.size()));
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            } else {
                totalRecord.setText("Total Entries:- 0");
                Toast.makeText(RouteActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
    };

    @Override
    public void addRouteCallBack(final RouteBean routedata, final int pos) {

        dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.add_route_dialog);
         edtRouteTitle = (EditText) dialog.findViewById(R.id.edtRouteTitle);
         edtRoteCap = (EditText) dialog.findViewById(R.id.edtRoteCap);

        Button btnSubmit = (Button) dialog.findViewById(R.id.btnSubmits);
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Utils.hideKeyboard(RouteActivity.this);
                if (checkValidation()) {
                    if (pos == -1) {
                        String title = edtRouteTitle.getText().toString();
                        String capacity = edtRoteCap.getText().toString();
                        addNewRoute(title, capacity);
                        dialog.dismiss();
                        Toast.makeText(RouteActivity.this,"Add Successfully",Toast.LENGTH_SHORT).show();
                    }
                }
            }

        });

        dialog.show();
    }

    private boolean checkValidation() {

            boolean valid = true;
            String errorMsg = null;
            if (edtRouteTitle.getText().toString().trim().length() <= 0) {
                valid = false;
                errorMsg = "Please enter Route Name";
            } else if (edtRoteCap.getText().toString().trim().length() <= 0) {
                valid = false;
                errorMsg = "Please enter Vehicle Capacity";
            }
            if (!valid) {
                showSnackBar(errorMsg);
            }
            return valid;
        }

    private void showSnackBar(String errorMsg) {
            Snackbar snack = Snackbar.make(parent, errorMsg, Snackbar.LENGTH_LONG);
            View view = snack.getView();
//            TextView tv = (TextView) view.findViewById(R.id.snackbar_text);
//            tv.setTextColor(Color.WHITE);
            snack.show();

        }


    private void addNewRoute(String title, String capacity) {
        HashMap<String, String> params = new HashMap<String, String>();
        params.put(Constants.ROUTES_NAME, title);
        params.put(Constants.CAPACITY, capacity);
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.SCHOOL_ROUTE_URL, apiCallBack, this, params);

    }
    @Override
    public void deleteRouteCallback(final RouteBean routedata, int pos) {
        DialogInterface.OnClickListener onClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:

                        HashMap<String, String> params = new HashMap<String, String>();
                        String url = Constants.getBaseURL() + Constants.SCHOOL_ROUTE_URL + Constants.ROUTES_ID + routedata.getId();
                        ApiHandler.apiHit(Request.Method.DELETE, url, apiCallBack, RouteActivity.this, params);
                        Toast.makeText(RouteActivity.this,"Delete Successfully",Toast.LENGTH_SHORT).show();
                        break;
                    case DialogInterface.BUTTON_NEGATIVE:
                        //No button clicked
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Are you sure you want to delete?").setPositiveButton("Yes", onClickListener)
                .setNegativeButton("No", onClickListener).show();
    }

    @Override
    public void editRouteCallback(final RouteBean routedata, int pos) {

        dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.add_route_dialog);
         edtRouteTitle = (EditText) dialog.findViewById(R.id.edtRouteTitle);
         edtRoteCap = (EditText) dialog.findViewById(R.id.edtRoteCap);
        Button btnUpdate = (Button) dialog.findViewById(R.id.btnSubmits);
         TextView txtSetup=(TextView) dialog.findViewById(R.id.txtSetup);
           if (pos != -1) {
            edtRouteTitle.setText(arrayList.get(pos).getRoute_title());
            edtRoteCap.setText(arrayList.get(pos).getCapacity());
            txtSetup.setText("Update Route Details");
            btnUpdate.setText("Update");
            btnUpdate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (checkValidation()) {
                        String title = null;
                        try {
                            title = URLEncoder.encode(edtRouteTitle.getText().toString().trim(),"utf-8");
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                        String capacity = edtRoteCap.getText().toString();
                        EditNewRoute(title, capacity, routedata);
                        dialog.dismiss();
                        Toast.makeText(RouteActivity.this,"Update Successfully",Toast.LENGTH_SHORT).show();
                    }
                }
            });

            dialog.show();
        }
      }
     private void EditNewRoute(String title, String capacity, RouteBean routedata) {

//         String value = URLEncoder.encode(myEditBox.getText.toString().trim(), "utf-8");
//         String url = "http://xxxxx/xxx/xxx&message=%s" + value;


         String url = Constants.getBaseURL() + Constants.SCHOOL_ROUTE_URL + Constants.ROUTE_NAME + title + Constants.ROUTE_ID + routedata.getId() + Constants.VEHICLE_CAP + capacity ;



//         String url = null;
//         try {
//             url = URLEncoder.encode(Constants.getBaseURL() + Constants.SCHOOL_ROUTE_URL + Constants.ROUTE_NAME+title+Constants.ROUTE_ID+routedata.getId()+Constants.VEHICLE_CAP+capacity.trim(), "utf-8");
//         } catch (UnsupportedEncodingException e) {
//             e.printStackTrace();
//         }
         ApiHandler.apiHit(Request.Method.PUT, url, apiCallBack, this,null);
    }




//    ApiHandler.ApiCallback apiCallbacksec = new ApiHandler.ApiCallback() {
//        @Override
//        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
//            ErpProgress.cancelProgressBar();
//            if (error == null) {
//                try {
//                    Utils.showSnackBar(jsonObject.getString("msg"), parent);
//                    finish();
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            } else {
//                Log.e("error", error.getMessage() + "");
//                Utils.showSnackBar(error.getMessage(), parent);
//
//                if (error.getStatusCode() == 405) {
//                    User.logout();
//                    startActivity(new Intent(RouteActivity.this, LoginActivity.class));
//                    finish();
//                }
//            }
//        }
//    };

}
