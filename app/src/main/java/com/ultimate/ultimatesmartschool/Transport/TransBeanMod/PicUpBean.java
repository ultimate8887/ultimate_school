package com.ultimate.ultimatesmartschool.Transport.TransBeanMod;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by admin on 6/19/2018.
 */

public class PicUpBean {

    private static String ID = "id";
    private static String ROUTE_ID = "route_id";
    private static String PICKUP_NAME = "pickup_name";
    private static String AMOUNT = "amount";
    private static String ROUTE = "route_title";

    /**
     * id : 1
     * route_id : 1
     * pickup_name : bhucho
     * amount : 0
     */

    private String id;
    private String route_id;
    private String pickup_name;
    private String amount;

    public String getRoute_title() {
        return route_title;
    }

    public void setRoute_title(String route_title) {
        this.route_title = route_title;
    }

    private String route_title;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRoute_id() {
        return route_id;
    }

    public void setRoute_id(String route_id) {
        this.route_id = route_id;
    }

    public String getPickup_name() {
        return pickup_name;
    }

    public void setPickup_name(String pickup_name) {
        this.pickup_name = pickup_name;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public static ArrayList<PicUpBean> parsePicUpArray(JSONArray arrayObj) {
        ArrayList list = new ArrayList<RouteBean>();
        try {
            for (int i = 0; i < arrayObj.length(); i++) {
                PicUpBean p = parsePicUpBeanObject(arrayObj.getJSONObject(i));
                if (p != null) {
                    list.add(p);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    private static PicUpBean parsePicUpBeanObject(JSONObject jsonObject) {
        PicUpBean casteObj = new PicUpBean();
        try {
            if (jsonObject.has(ID)) {
                casteObj.setId(jsonObject.getString(ID));
            }
            if (jsonObject.has(ROUTE_ID) && !jsonObject.getString(ROUTE_ID).isEmpty() && !jsonObject.getString(ROUTE_ID).equalsIgnoreCase("null")) {
                casteObj.setRoute_id(jsonObject.getString(ROUTE_ID));
            }
            if (jsonObject.has(PICKUP_NAME) && !jsonObject.getString(PICKUP_NAME).isEmpty() && !jsonObject.getString(PICKUP_NAME).equalsIgnoreCase("null")) {
                casteObj.setPickup_name(jsonObject.getString(PICKUP_NAME));
            }
            if (jsonObject.has(AMOUNT) && !jsonObject.getString(AMOUNT).isEmpty() && !jsonObject.getString(AMOUNT).equalsIgnoreCase("null")) {
                casteObj.setAmount(jsonObject.getString(AMOUNT));
            }
            if (jsonObject.has(ROUTE) && !jsonObject.getString(ROUTE).isEmpty() && !jsonObject.getString(ROUTE).equalsIgnoreCase("null")) {
                casteObj.setRoute_title(jsonObject.getString(ROUTE));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return casteObj;


    }

}
