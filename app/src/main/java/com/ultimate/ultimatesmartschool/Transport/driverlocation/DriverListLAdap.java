package com.ultimate.ultimatesmartschool.Transport.driverlocation;

import android.content.Context;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;


import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Transport.TransBeanMod.AlotDrivrToVicleBean;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by admin on 6/21/2018.
 */


public class DriverListLAdap extends RecyclerView.Adapter<DriverListLAdap.ViewHolder> {

    ArrayList<AlotDrivrToVicleBean> arrayList;
    Context context;
    private MethodCallBack callBackMethod;

    public DriverListLAdap(ArrayList<AlotDrivrToVicleBean> arrayList, Context context, MethodCallBack callBackMethod) {
        this.arrayList = arrayList;
        this.context = context;
        this.callBackMethod = callBackMethod;
    }

    public void setArrayList(ArrayList<AlotDrivrToVicleBean> arrayList) {
        this.arrayList = arrayList;
    }

    public interface MethodCallBack {

        void ViewLocation(AlotDrivrToVicleBean driverListBean);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.mobile)
        TextView mobile;
        @BindView(R.id.drivermobilenums)TextView drivermobilenums;
        @BindView(R.id.drivershifta)TextView drivershifta;
        @BindView(R.id.lytAll)
        RelativeLayout lytAll;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.driveloclist, parent, false);
        ViewHolder viewHolder = new DriverListLAdap.ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        holder.name.setText(arrayList.get(position).getV_no());
        holder.mobile.setText(arrayList.get(position).getD_name());
        holder.drivermobilenums.setText(arrayList.get(position).getD_mobile());
       // holder.drivershifta.setText(arrayList.get(position).getEs_shift());
        holder.lytAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (callBackMethod != null) {
                    callBackMethod.ViewLocation(arrayList.get(position));
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }
}
