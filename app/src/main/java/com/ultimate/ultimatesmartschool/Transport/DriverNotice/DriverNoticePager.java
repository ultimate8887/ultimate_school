package com.ultimate.ultimatesmartschool.Transport.DriverNotice;


import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

public class DriverNoticePager extends FragmentPagerAdapter {

    int tabCount;
    // tab titles
    private String[] tabTitles = new String[]{"Compose Notice to Driver", "Notice List"};

    public DriverNoticePager(FragmentManager fm) {
        super(fm);
        this.tabCount =2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabTitles[position];
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                ComposeNoticeFrag tab1 = new ComposeNoticeFrag();
                return tab1;
            case 1:
                DriverNoticeList tab2 = new DriverNoticeList();
                return tab2;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return tabCount;
    }
}
