package com.ultimate.ultimatesmartschool.Transport.PickUpMod;

import android.content.Context;

import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Transport.TransBeanMod.PicUpBean;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by admin on 6/18/2018.
 */

public class PickUpPointAdap extends RecyclerView.Adapter<PickUpPointAdap.ViewHolder> {

    ArrayList<PicUpBean> arrayList;
    Context context;
    MethodCallBack methodCallBack;

    public PickUpPointAdap(ArrayList<PicUpBean> arrayList, Context context,MethodCallBack methodCallBack) {
        this.arrayList=arrayList;
        this.context=context;
        this.methodCallBack=methodCallBack;
    }

    public interface MethodCallBack {
        void addPicUpCallBack(PicUpBean routedata, int pos);
        void deletePicUpCallback(PicUpBean routedata, int pos);
        void editPicUpCallback(PicUpBean routedata, int pos);
    }

    public void setArrayList(ArrayList<PicUpBean> arrayList) {
        this.arrayList = arrayList;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.lytAddData)
        RelativeLayout lytAddData;
        @BindView(R.id.lytAll)
        RelativeLayout lytAll;
        @BindView(R.id.first)
        TextView first;
        @BindView(R.id.second)
        TextView second;
        @BindView(R.id.third)
        TextView third;
        @BindView(R.id.fetchFirst)
        TextView txtRouteName;
        @BindView(R.id.fetchSecond)
        TextView pickUpPoint;
        @BindView(R.id.fetchThid)
        TextView amoount;
        @BindView(R.id.trash)
        ImageView delete;
        @BindView(R.id.albm_id)
        TextView albm_id;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.common_adap_lyt, parent, false);
        ViewHolder viewHolder=new PickUpPointAdap.ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {

        if (arrayList.size() == position) {   //suppose data size is 4 then when position is 4 we have show add UI as it start count from o  then 4th position means  index 5
            holder.lytAll.setVisibility(View.GONE);
            holder.lytAddData.setVisibility(View.VISIBLE);
            holder.lytAddData.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (methodCallBack != null) {
                        methodCallBack.addPicUpCallBack(null,-1);
                    }
                }
            });
        } else {

            if (arrayList.get(position).getId() != null) {
                String title = getColoredSpanned("ID: ", "#000000");
                String Name = getColoredSpanned(arrayList.get(position).getId(), "#5A5C59");
                holder.albm_id.setText(Html.fromHtml(title + " " + Name));
            }

            holder.lytAll.setVisibility(View.VISIBLE);
            holder.lytAddData.setVisibility(View.GONE);
            holder.first.setText("Route:");
            holder.txtRouteName.setText(arrayList.get(position).getRoute_title()+"("+arrayList.get(position).getRoute_id()+")");
            holder.second.setText("PickUp Point:");
            holder.pickUpPoint.setText(arrayList.get(position).getPickup_name());
            holder.third.setText("Amount:");
            holder.amoount.setText(arrayList.get(position).getAmount());
            holder.lytAll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (methodCallBack != null) {
                        methodCallBack.editPicUpCallback(arrayList.get(position),position);
                    }
                }
            });
            holder.delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (methodCallBack != null) {
                        methodCallBack.deletePicUpCallback(arrayList.get(position),position);
                    }
                }
            });
        }
    }

    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }

    @Override
    public int getItemCount() {
        return (arrayList.size() + 1);
    }


}
