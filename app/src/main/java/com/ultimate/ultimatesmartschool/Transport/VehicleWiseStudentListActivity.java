package com.ultimate.ultimatesmartschool.Transport;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;
import com.ultimate.ultimatesmartschool.BeanModule.ClassBean;
import com.ultimate.ultimatesmartschool.BeanModule.SectionBean;
import com.ultimate.ultimatesmartschool.BeanModule.Studentbean;
import com.ultimate.ultimatesmartschool.BeanModule.User;
import com.ultimate.ultimatesmartschool.Homework.SectionnewAdapter;
import com.ultimate.ultimatesmartschool.Login.LoginActivity;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Student.StudentProfile;
import com.ultimate.ultimatesmartschool.Student.Studentadapter;
import com.ultimate.ultimatesmartschool.Student.UpdateStudentDetails;
import com.ultimate.ultimatesmartschool.StudentAttendance.ClassAdapterAttend;
import com.ultimate.ultimatesmartschool.Transport.TransBeanMod.DriverListBean;
import com.ultimate.ultimatesmartschool.Transport.TransBeanMod.RouteBean;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.CommonProgress;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class VehicleWiseStudentListActivity extends AppCompatActivity implements Studentadapter.StudentPro {
    RecyclerView.LayoutManager layoutManager;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @BindView(R.id.textNorecord)
    TextView txtNorecord;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.parent)
    RelativeLayout parent;
    ArrayList<Studentbean> hwList = new ArrayList<>();
    private int loaded = 0;

    @BindView(R.id.totalRecord)
    TextView totalRecord;
    @BindView(R.id.spinnerClass1)
    Spinner spinnerGroup;

    @BindView(R.id.spinnerClass)
    Spinner spinnerGroup1;

    @BindView(R.id.spinnersection)
    Spinner spinnerActivity;

    ArrayList<RouteBean> dataList = new ArrayList<>();
    private ArrayList<DriverListBean> groupList = new ArrayList<>();

    Studentadapter adapter;

    private String route_id = "";

    @BindView(R.id.imgBack)
    ImageView back;
    CommonProgress commonProgress;
    @BindView(R.id.export)
    FloatingActionButton export;

    @BindView(R.id.textView8)
    TextView textdriver;

    @BindView(R.id.textView9)
    TextView textroute;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_list_by_name_i_d);
        ButterKnife.bind(this);
        commonProgress = new CommonProgress(VehicleWiseStudentListActivity.this);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        //fetchStudent();
        layoutManager = new LinearLayoutManager(VehicleWiseStudentListActivity.this);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new Studentadapter(hwList, VehicleWiseStudentListActivity.this, this);
        recyclerView.setAdapter(adapter);
        spinnerGroup.setVisibility(View.VISIBLE);
        spinnerGroup1.setVisibility(View.GONE);
        txtTitle.setText("Vehicle Wise Student List");
        textdriver.setText("Vehicle");
        textroute.setText("Route");
        parent.setBackgroundColor(getResources().getColor(R.color.white));
        fetchDriver();

    }

    private void fetchDriver() {
        commonProgress.show();
        HashMap<String,String> params=new HashMap<>();ApiHandler.apiHit(Request.Method.GET, Constants.getBaseURL() + Constants.DRIVERLIST_URL+Constants.TAGGG+"new", new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                ErpProgress.cancelProgressBar();
                commonProgress.dismiss();
                if (error == null) {
                    try {
                        groupList.clear();
                        groupList = DriverListBean.parseDriverListArray(jsonObject.getJSONArray("driver_data"));
                        DriverSpinnerAdapter adapter1 = new DriverSpinnerAdapter(VehicleWiseStudentListActivity.this, groupList);
                        spinnerGroup.setAdapter(adapter1);
                        spinnerGroup.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                                if (i > 0) {
                                    driver_id = "";
                                    driver_name = "";
                                    driver_id = groupList.get(i - 1).getId();
                                    vehicle_id= groupList.get(i - 1).getVehicle_id();
                                    driver_name = groupList.get(i - 1).getDriver_name();
                                    vehicle_name = groupList.get(i - 1).getV_type()+"-"+groupList.get(i - 1).getV_no();
                                    fetchRoute(vehicle_id);
                                } else {
                                    driver_id = "";
                                    driver_name = "";
                                    vehicle_name = "";
                                    // fetchActivity(groupid);
                                    // recyclerView.setVisibility(View.GONE);
                                }
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> adapterView) {

                            }
                        });

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    Utils.showSnackBar(error.getMessage(), parent);
                    Toast.makeText(VehicleWiseStudentListActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }, this, params);
    }

    private String driver_id = "",vehicle_name = "",vehicle_id="";
    private String driver_name = "";
    private String route_name = "";

    private void fetchRoute(String vehicle_id) {
        commonProgress.show();
        HashMap<String, String> params = new HashMap<String, String>();
        // params.put("driver_id", groupid);
        ApiHandler.apiHit(Request.Method.GET, Constants.getBaseURL() + Constants.SCHOOL_ROUTE_URL+Constants.VEHICLEID+ vehicle_id, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                ErpProgress.cancelProgressBar();
                commonProgress.dismiss();
                if (error == null) {
                    try {
                        dataList.clear();
                        dataList = RouteBean.parseTransportArray(jsonObject.getJSONArray("school_route_data"));
                        RouteSpinnerAdapter adapter = new RouteSpinnerAdapter(VehicleWiseStudentListActivity.this, dataList);
                        spinnerActivity.setAdapter(adapter);
                        spinnerActivity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                if (i > 0) {
                                    route_id = "";
                                    route_name = "";
                                    route_id = dataList.get(i - 1).getId();
                                    route_name = dataList.get(i - 1).getRoute_title();
                                    //setActivityData(dataList.get(i - 1));
                                    fetchStudent();
                                } else {
                                    route_id = "";
                                    route_name = "";
                                    // p_fetchhwlist();
                                }
                            }
                            @Override
                            public void onNothingSelected(AdapterView<?> adapterView) {

                            }
                        });

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    Utils.showSnackBar(error.getMessage(), parent);
                    Toast.makeText(VehicleWiseStudentListActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }, this, params);
    }

    public void fetchStudent() {
        //  ErpProgress.showProgressBar(this,"please wait...");
//        if (loaded == 0) {
            commonProgress.show();
      //  }
        loaded++;
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("route_id",route_id);
        params.put("tag","trans");
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.STUDENTLISTCLSSECWISE_URL, apiCallback, this, params);

    }

    ApiHandler.ApiCallback apiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            commonProgress.dismiss();
            if (error == null) {
                ErpProgress.cancelProgressBar();
                try {
                    if (hwList != null) {
                        hwList.clear();
                    }
                    JSONArray jsonArray = jsonObject.getJSONArray("student_list");
                    hwList = Studentbean.parseHWArray(jsonArray);
                    if (hwList.size() > 0) {
                        adapter.setHQList(hwList);
                        adapter.notifyDataSetChanged();
                        txtNorecord.setVisibility(View.GONE);
                        totalRecord.setText("Total Entries:- " + String.valueOf(hwList.size()));
                        export.setVisibility(View.VISIBLE);
                    } else {
                        export.setVisibility(View.GONE);
                        totalRecord.setText("Total Entries:- 0");
                        hwList.clear();
                        adapter.setHQList(hwList);
                        adapter.notifyDataSetChanged();
                        txtNorecord.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                export.setVisibility(View.GONE);
                totalRecord.setText("Total Entries:- 0");
                txtNorecord.setVisibility(View.VISIBLE);
                hwList.clear();
                adapter.setHQList(hwList);
                adapter.notifyDataSetChanged();
            }
        }
    };


    @SuppressLint("RestrictedApi")
    @OnClick(R.id.export)
    public void submit() {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.S) {
            importData();
        } else {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {

                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    Toast.makeText(this, "Please provide storage permission from app settings", Toast.LENGTH_LONG).show();
                } else {

                    ActivityCompat.requestPermissions(this,
                            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            1);
                }
            } else {
                importData();

            }
        }
    }

    @SuppressLint("RestrictedApi")
    private void importData() {
        // File filePath = new File(Environment.getExternalStorageDirectory() + "/Demo.xls");
        Workbook wb = new HSSFWorkbook();

        Cell cell = null;

        String ndate = "", sub_staff = "", sub_date = "";

        sub_staff = "Vehicle Wise";

        Sheet sheet = null;
        sheet = wb.createSheet(sub_staff + " Students List");
        //Now column and row
        Row row = sheet.createRow(0);

        cell = row.createCell(0);
        cell.setCellValue("S No");

        cell = row.createCell(1);
        cell.setCellValue("Roll No");

        cell = row.createCell(2);
        cell.setCellValue("Class/Section");

        cell = row.createCell(3);
        cell.setCellValue("Student Name");

        cell = row.createCell(4);
        cell.setCellValue("Father Name");

        cell = row.createCell(5);
        cell.setCellValue("Phone");

        cell = row.createCell(6);
        cell.setCellValue("Class & Section");

        cell = row.createCell(7);
        cell.setCellValue("Driver");

        cell = row.createCell(8);
        cell.setCellValue("Route");

        cell = row.createCell(9);
        cell.setCellValue("Vehicle");


        //column width
        sheet.setColumnWidth(0, (20 * 100));
        sheet.setColumnWidth(1, (20 * 150));
        sheet.setColumnWidth(2, (30 * 150));
        sheet.setColumnWidth(3, (30 * 250));
        sheet.setColumnWidth(4, (30 * 200));
        sheet.setColumnWidth(5, (20 * 100));
        sheet.setColumnWidth(6, (30 * 250));
        sheet.setColumnWidth(7, (30 * 250));
        sheet.setColumnWidth(8, (30 * 250));
        sheet.setColumnWidth(8, (30 * 350));


        for (int i = 0; i < hwList.size(); i++) {

            Row row1 = sheet.createRow(i + 1);

            cell = row1.createCell(0);
            cell.setCellValue(i + 1);

            cell = row1.createCell(1);
            cell.setCellValue(hwList.get(i).getId());

            cell = row1.createCell(2);
            cell.setCellValue(hwList.get(i).getClass_name() + " (" + hwList.get(i).getSection_name() + ")");

            cell = row1.createCell(3);
            cell.setCellValue((hwList.get(i).getName()));
            //  cell.setCellStyle(cellStyle);

            cell = row1.createCell(4);
            cell.setCellValue(hwList.get(i).getFather_name());

            cell = row1.createCell(5);
            cell.setCellValue(hwList.get(i).getPhoneno());

            cell = row1.createCell(6);
            cell.setCellValue(hwList.get(i).getClass_name()+"-"+hwList.get(i).getSection_name());

            cell = row1.createCell(7);
            cell.setCellValue(driver_name);

            cell = row1.createCell(8);
            cell.setCellValue(route_name);

            cell = row1.createCell(9);
            cell.setCellValue(vehicle_name);


            sheet.setColumnWidth(0, (20 * 100));
            sheet.setColumnWidth(1, (20 * 150));
            sheet.setColumnWidth(2, (30 * 150));
            sheet.setColumnWidth(3, (30 * 250));
            sheet.setColumnWidth(4, (30 * 200));
            sheet.setColumnWidth(5, (20 * 100));
            sheet.setColumnWidth(6, (30 * 250));
            sheet.setColumnWidth(7, (30 * 250));
            sheet.setColumnWidth(8, (30 * 250));
            sheet.setColumnWidth(9, (30 * 350));

        }
        String fileName;

        fileName = sub_staff + "_students_list_" + System.currentTimeMillis() + ".xls";

        // File file = new File(getActivity().getExternalFilesDir(null), fileName);

        File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS), fileName);

        FileOutputStream fileOutputStream = null;

        try {
            fileOutputStream = new FileOutputStream(file);
            wb.write(fileOutputStream);
            Utils.openSuccessDialog("Success!", "Export file in " + file, "", this);
        } catch (IOException e) {
            Utils.openErrorDialog("Error writing Exception: " + e, this);
        } catch (Exception e) {
            Utils.openErrorDialog("Failed to save file due to Exception: " + e, this);
        } finally {
            try {
                if (null != fileOutputStream) {
                    fileOutputStream.close();
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

    }

    @SuppressLint("RestrictedApi")
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 1 && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            importData();
        } else {
            Toast.makeText(getApplicationContext(), "Permission Denied", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void StudentProfile(Studentbean std) {
        Gson gson = new Gson();
        String student = gson.toJson(std, Studentbean.class);
        Intent i = new Intent(VehicleWiseStudentListActivity.this, StudentProfile.class);
        i.putExtra("student", student);
        startActivity(i);
    }

    @Override
    public void onUpdateCallback(Studentbean std) {

    }

    @Override
    public void onBlockCallback(Studentbean std) {

    }


    @Override
    public void stdCallBack(Studentbean std) {
        String phoneNo = std.getPhoneno();
        Log.i("email", phoneNo);
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + phoneNo));
        startActivity(intent);
    }

    @Override
    public void stdEmailCallBack(Studentbean std) {

        String phoneNo = std.getPhoneno();
        Uri uri = Uri.parse("smsto:" + phoneNo);
        Intent i = new Intent(Intent.ACTION_SENDTO, uri);
        i.setPackage("com.whatsapp");
        startActivity(Intent.createChooser(i, "Chat Now!"));
    }

    @Override
    public void onResume() {
        super.onResume();
        //  fetchStudent();
    }

    @Override
    public void onDeleteCallback(Studentbean std) {

    }
}
