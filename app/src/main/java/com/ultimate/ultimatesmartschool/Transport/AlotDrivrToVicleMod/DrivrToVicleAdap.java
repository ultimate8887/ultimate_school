package com.ultimate.ultimatesmartschool.Transport.AlotDrivrToVicleMod;

import android.content.Context;

import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;


import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Transport.TransBeanMod.AlotDrivrToVicleBean;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by admin on 6/22/2018.
 */

public class DrivrToVicleAdap extends RecyclerView.Adapter<DrivrToVicleAdap.ViewHolder> {

    ArrayList<AlotDrivrToVicleBean> arrayList;
    Context context;
    private OnMethodCallBack methodCallBack;

    public DrivrToVicleAdap(ArrayList<AlotDrivrToVicleBean> arrayList, Context context ,OnMethodCallBack methodCallBack) {
        this.arrayList=arrayList;
        this.context=context;
        this.methodCallBack=methodCallBack;
    }

    public void setArrayList(ArrayList<AlotDrivrToVicleBean> arrayList) {
        this.arrayList = arrayList;
    }

    public interface OnMethodCallBack {

      void alotDrivrVehicleCallBack(AlotDrivrToVicleBean alotDrivrToVicleBean,int pos);
      void addDriverVehicleCallBack(AlotDrivrToVicleBean alotDrivrToVicleBean, int pos);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.lytAddData)
        RelativeLayout lytAddData;
        @BindView(R.id.lytAll)
        RelativeLayout lytAll;
        @BindView(R.id.first)
        TextView first;
        @BindView(R.id.second)
        TextView second;
        @BindView(R.id.third)
        TextView third;
        @BindView(R.id.forth)TextView forth;
        @BindView(R.id.fetchFirst)
        TextView vehicleNo;
        @BindView(R.id.fetchSecond)
        TextView driverName;
        @BindView(R.id.fetchThid)
        TextView driverId;
        @BindView(R.id.fetchForth)
        TextView drivershift;
        @BindView(R.id.r1)
        RelativeLayout delete;
        @BindView(R.id.albm_id)
        TextView albm_id;
        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
    @NonNull
    @Override
    public DrivrToVicleAdap.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.common_adap_lyt, parent, false);
        ViewHolder viewHolder=new DrivrToVicleAdap.ViewHolder(v);
        return viewHolder;
    }
    @Override
    public void onBindViewHolder(@NonNull DrivrToVicleAdap.ViewHolder holder, final int position) {


        if (arrayList.size() == position) {   //suppose data size is 4 then when position is 4 we have show add UI as it start count from o  then 4th position means  index 5
            holder.lytAll.setVisibility(View.GONE);
            holder.lytAddData.setVisibility(View.VISIBLE);
            holder.lytAddData.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (methodCallBack != null) {
                        methodCallBack.addDriverVehicleCallBack(null,-1);
                    }
                }
            });
        } else {
            final AlotDrivrToVicleBean data=arrayList.get(position);
            holder.first.setText("Vehicle No: ");
            holder.second.setText("Driver Name: ");
            holder.third.setText("Driver Id: ");
            holder.forth.setText("Driver Shift: ");
            holder.delete.setVisibility(View.GONE);
            holder.lytAll.setVisibility(View.VISIBLE);
            holder.lytAddData.setVisibility(View.GONE);
            if (data.getV_no() != null) {
                holder.vehicleNo.setText(data.getV_no());
            } else {
                holder.vehicleNo.setText("-");
            }


            if (arrayList.get(position).getV_id() != null) {
                String title = getColoredSpanned("ID: ", "#000000");
                String Name = getColoredSpanned(arrayList.get(position).getV_id(), "#5A5C59");
                holder.albm_id.setText(Html.fromHtml(title + " " + Name));
            }

            if (data.getD_name() != null) {
                holder.driverName.setText(data.getD_name());
            } else {
                holder.driverName.setText("-");

            }
            if (data.getD_id() != null) {
                holder.driverId.setText(data.getD_id());
            } else {
                holder.driverId.setText("-");
            }
//            if (data.getEs_shift() != null) {
//                holder.drivershift.setText(data.getEs_shift());
//            } else {
//                holder.drivershift.setText("-");
//            }

            holder.lytAll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    methodCallBack.alotDrivrVehicleCallBack(arrayList.get(position), position);
                }
            });
        }
    }

    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }

    @Override
    public int getItemCount() {
        return arrayList.size()+1;
    }
}
