package com.ultimate.ultimatesmartschool.Transport;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;

import com.ultimate.ultimatesmartschool.BeanModule.ClassBean;
import com.ultimate.ultimatesmartschool.Examination.ClassAdapter;
import com.ultimate.ultimatesmartschool.Examination.Exam_Detail_Activity;
import com.ultimate.ultimatesmartschool.ForCamera.IPickResult;
import com.ultimate.ultimatesmartschool.ForCamera.PickImageDialog;
import com.ultimate.ultimatesmartschool.ForCamera.PickResult;
import com.ultimate.ultimatesmartschool.ForCamera.PickSetup;
import com.ultimate.ultimatesmartschool.Homework.StaffListAdapter;
import com.ultimate.ultimatesmartschool.Homework.StaffWiseHWActivity;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Staff.View_staff_bean;
import com.ultimate.ultimatesmartschool.Transport.RouteMod.RouteAdapter;
import com.ultimate.ultimatesmartschool.Transport.TransBeanMod.RouteBean;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.CommonProgress;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.FileUtils;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AddFuelExpense extends AppCompatActivity implements IPickResult {
    ArrayList<View_staff_bean> classList = new ArrayList<>();

    ArrayList<RouteBean> routeBeans = new ArrayList<>();
    @BindView(R.id.spinner1)
    Spinner spinnerClass;

    String s_id="",s_name="";
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    StaffListAdapter staffListAdapter;
    RouteAdapter routeAdapter;
    private int type=0;

    @BindView(R.id.toMeet)
    EditText price;

    @BindView(R.id.vistname)
    EditText route;

    @BindView(R.id.vistphoneno)
    EditText amount;

    @BindView(R.id.pnumber)
    EditText morn;

    @BindView(R.id.pnumber1)
    EditText evng;

    @BindView(R.id.docimage)
    ImageView chooseimage;

    @BindView(R.id.docimage1)
    ImageView chooseimage1;
    private Bitmap userBitmap,userBitmap1;
    @BindView(R.id.checkIn)
    Button checkIn;
    CommonProgress commonProgress;
    String route_no="",route_name="";

    @BindView(R.id.spinnerClasssec)Spinner classspin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_fuel_expense);
        ButterKnife.bind(this);
        commonProgress=new CommonProgress(this);
        txtTitle.setText("Add Fuel Expense");
        fetchStaff();
        checkIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendcheckinrequest();
            }
        });
        fetchRoutelist();

    }

    private void fetchRoutelist() {

        HashMap<String, String> params = new HashMap<String, String>();
        ApiHandler.apiHit(Request.Method.GET, Constants.getBaseURL() + Constants.SCHOOL_ROUTE_URL, classapiCallback, this, params);
    }
    ApiHandler.ApiCallback classapiCallback=new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            if (error == null) {
                try {
                    routeBeans = RouteBean.parseTransportArray(jsonObject.getJSONArray("school_route_data"));
                    routeAdapter = new RouteAdapter(AddFuelExpense.this, routeBeans);
                    classspin.setAdapter(routeAdapter);
                    classspin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            if (position > 0) {
                                route_no = routeBeans.get(position - 1).getId();
                                route_name=routeBeans.get(position-1).getRoute_title();
                                //fetchsujectname(classid);
                            } else {
                                route_no = "";
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }else{

            }
        }
    };


    private void sendcheckinrequest() {
        if (s_id.equalsIgnoreCase("")) {
            Toast.makeText(getApplicationContext(), "Kindly select staff", Toast.LENGTH_LONG).show();
        }else if (route_no.equalsIgnoreCase("")) {
            Toast.makeText(getApplicationContext(), "Kindly select route no.", Toast.LENGTH_LONG).show();
        }else if (price.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(getApplicationContext(), "Kindly enter petrol/diesel price", Toast.LENGTH_LONG).show();
        }else if (amount.getText().toString().isEmpty()) {
            Toast.makeText(getApplicationContext(), "Kindly add amount", Toast.LENGTH_LONG).show();
        }else if (morn.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(getApplicationContext(), "Kindly add morning meter reading", Toast.LENGTH_LONG).show();
        }else if (userBitmap == null) {
            Toast.makeText(getApplicationContext(), "Kindly Add morning meter reading image", Toast.LENGTH_LONG).show();
        }else if (evng.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(getApplicationContext(), "Kindly add evening meter reading", Toast.LENGTH_LONG).show();
        }else if (userBitmap1 == null) {
            Toast.makeText(getApplicationContext(), "Kindly Add evening meter reading image", Toast.LENGTH_LONG).show();
        }else {
            HashMap<String, String> params = new HashMap<String, String>();
            commonProgress.show();
            params.put("s_id", s_id);
            params.put("name", s_name);
            params.put("route_no", route_no);
            params.put("amount", amount.getText().toString());
            params.put("fuel_price", price.getText().toString());
            params.put("morn_reading",morn.getText().toString() );
            params.put("evng_reading", evng.getText().toString());
            if (userBitmap != null) {
                String encoded = Utils.encodeToBase64(userBitmap, Bitmap.CompressFormat.JPEG, 90);
                encoded = String.format("data:image/jpeg;base64,%s", encoded);
                params.put("docimage", encoded);
            }
            if (userBitmap1 != null) {
                String encoded = Utils.encodeToBase64(userBitmap1, Bitmap.CompressFormat.JPEG, 90);
                encoded = String.format("data:image/jpeg;base64,%s", encoded);
                params.put("docimage1", encoded);
            }

            ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.EXPENSE_URL, checkinclassapiCallback, this, params);
        }
    }
    ApiHandler.ApiCallback checkinclassapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            commonProgress.dismiss();
            if (error == null) {
                Toast.makeText(AddFuelExpense.this,"Added Successfully!",Toast.LENGTH_LONG).show();
                finish();
            }
            else {
                Toast.makeText(AddFuelExpense.this, error.getMessage(), Toast.LENGTH_SHORT).show();

            }
        }
    };


    @OnClick(R.id.imgBack)
    public void onback() {
        finish();
    }
    private void fetchStaff() {

        HashMap<String, String> params = new HashMap<String, String>();
        params.put("check", "trans");
        params.put("d_id", "");
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.STAFF_DETAIL, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                if (error == null) {
                    try {
                        JSONArray jsonArray = jsonObject.getJSONArray("staff_detail");
                        classList = View_staff_bean.parseviewstaffArray(jsonArray);
                        staffListAdapter = new StaffListAdapter(AddFuelExpense.this, classList);
                        spinnerClass.setAdapter(staffListAdapter);
                        spinnerClass.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                s_id="";
                                if (i > 0) {
                                    s_id = classList.get(i - 1).getId();
                                    s_name = classList.get(i - 1).getName();
                                } else {
                                    s_id = "";
                                    s_name = "";
                                }
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> adapterView) {

                            }
                        });

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
//                Utils.showSnackBar(error.getMessage(), parent);
                }
            }
        }, this, params);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @OnClick(R.id.docclickimage)
    public void docclickimage() {
        type = 1;
        pick_img();
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @OnClick(R.id.docclickimage1)
    public void docclickimage11() {
        type = 2;
        pick_img();
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @SuppressLint("ResourceType")
    private void pick_img() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.UPSIDE_DOWN_CAKE) { // Android 14+ (API 34)
            onImageViewClick();

        } else if (Build.VERSION.SDK_INT > Build.VERSION_CODES.S) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_MEDIA_IMAGES)
                    != PackageManager.PERMISSION_GRANTED) {

                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_MEDIA_IMAGES)) {
                    Toast.makeText(this, "Please provide storage permission from app settings", Toast.LENGTH_LONG).show();
                } else {

                    ActivityCompat.requestPermissions(this,
                            new String[]{Manifest.permission.READ_MEDIA_IMAGES},
                            1);
                }
            } else {

                onImageViewClick();
            }

        } else {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {

                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    Toast.makeText(this, "Please provide storage permission from app settings", Toast.LENGTH_LONG).show();
                } else {

                    ActivityCompat.requestPermissions(this,
                            new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                            1);
                }
            } else {
                onImageViewClick();

            }
        }
    }

    private void onImageViewClick() {
        //  type = 1;
        PickSetup setup = new PickSetup();
        // super.customize(setup);

        PickImageDialog.build(setup)
                //.setOnClick(this)
                .show(this);
    }

    @Override
    public void onPickResult(PickResult r) {
        if (r.getError() == null) {
            //If you want the Bitmap.
//            chooseimage.setImageBitmap(r.getBitmap());
//            userBitmap = r.getBitmap();

            if (type == 1) {
                chooseimage.setImageBitmap(r.getBitmap());
                userBitmap = r.getBitmap();
            }
            if (type == 2){
                chooseimage1.setImageBitmap(r.getBitmap());
                userBitmap1 = r.getBitmap();
            }

        } else {
            //Handle possible errors
            //TODO: do what you have to do with r.getError();
            Toast.makeText(this, r.getError().getMessage(), Toast.LENGTH_LONG).show();
        }


    }




    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (type == 1) {
                Uri imageUri = data.getData();
                try {
                    String path = FileUtils.getRealPath(this, imageUri);
                    userBitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), imageUri);

                } catch (IOException e) {
                    e.printStackTrace();
                }
                chooseimage.setImageBitmap(userBitmap);
            }else if (type == 2){
                Uri imageUri = data.getData();
                try {
                    String path = FileUtils.getRealPath(this, imageUri);
                    userBitmap1 = MediaStore.Images.Media.getBitmap(this.getContentResolver(), imageUri);

                } catch (IOException e) {
                    e.printStackTrace();
                }
                chooseimage1.setImageBitmap(userBitmap1);
            }else{

            }


        }else {
            Toast.makeText(this, "Selection Cancelled", Toast.LENGTH_SHORT).show();
        }
    }


    public static boolean checkPermission(Context mContext, String Permission) {
        int result = ContextCompat.checkSelfPermission(mContext, Permission);
        return result == PackageManager.PERMISSION_GRANTED;
    }
}