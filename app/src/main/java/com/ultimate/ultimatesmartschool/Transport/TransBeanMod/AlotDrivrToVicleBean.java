package com.ultimate.ultimatesmartschool.Transport.TransBeanMod;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by admin on 6/25/2018.
 */

public class AlotDrivrToVicleBean {

    private static String D_ID = "d_id";
    private static String V_ID = "v_id";
    private static String D_NAME = "d_name";
    private static String V_NO = "v_no";

    /**
     * v_id : 8
     * v_no : 12345
     * d_id : 1
     * d_name : Nehu
     */

    private String v_id;
    private String v_no;
    private String d_id;
    private String d_name;
    private String allotdrivervicleid;
    private String es_shift;
    private String d_mobile;

    public String getD_mobile() {
        return d_mobile;
    }

    public void setD_mobile(String d_mobile) {
        this.d_mobile = d_mobile;
    }

    public String getEs_shift() {
        return es_shift;
    }

    public void setEs_shift(String es_shift) {
        this.es_shift = es_shift;
    }

    public String getAllotdrivervicleid() {
        return allotdrivervicleid;
    }

    public void setAllotdrivervicleid(String allotdrivervicleid) {
        this.allotdrivervicleid = allotdrivervicleid;
    }

    public String getV_id() {
        return v_id;
    }

    public void setV_id(String v_id) {
        this.v_id = v_id;
    }

    public String getV_no() {
        return v_no;
    }

    public void setV_no(String v_no) {
        this.v_no = v_no;
    }

    public String getD_id() {
        return d_id;
    }

    public void setD_id(String d_id) {
        this.d_id = d_id;
    }

    public String getD_name() {
        return d_name;
    }

    public void setD_name(String d_name) {
        this.d_name = d_name;
    }

    public static ArrayList<AlotDrivrToVicleBean> parseDrivrToVicleArray(JSONArray arrayObj) {
        ArrayList list = new ArrayList<AlotDrivrToVicleBean>();
        try {
            for (int i = 0; i < arrayObj.length(); i++) {
                AlotDrivrToVicleBean p = parseDrivrToVicleBeanObject(arrayObj.getJSONObject(i));
                if (p != null) {
                    list.add(p);
                }
            }
          } catch (Exception e) {
            e.printStackTrace();
         }
         return list;
      }

      private static AlotDrivrToVicleBean parseDrivrToVicleBeanObject(JSONObject jsonObject) {

        AlotDrivrToVicleBean casteObj= new AlotDrivrToVicleBean();

        try {

            if (jsonObject.has(D_ID) && !jsonObject.getString(D_ID).isEmpty() && !jsonObject.getString(D_ID).equalsIgnoreCase("null")) {
                casteObj.setD_id(jsonObject.getString(D_ID));
            }
            if (jsonObject.has(V_ID) && !jsonObject.getString(V_ID).isEmpty() && !jsonObject.getString(V_ID).equalsIgnoreCase("null")) {
                casteObj.setV_id(jsonObject.getString(V_ID));
            }
            if (jsonObject.has(D_NAME) && !jsonObject.getString(D_NAME).isEmpty() && !jsonObject.getString(D_NAME).equalsIgnoreCase("null")) {
                casteObj.setD_name(jsonObject.getString(D_NAME));
            }

            if (jsonObject.has(V_NO) && !jsonObject.getString(V_NO).isEmpty() && !jsonObject.getString(V_NO).equalsIgnoreCase("null")) {
                casteObj.setV_no(jsonObject.getString(V_NO));
            }
            if (jsonObject.has("allotdrivervicleid") && !jsonObject.getString("allotdrivervicleid").isEmpty() && !jsonObject.getString("allotdrivervicleid").equalsIgnoreCase("null")) {
                casteObj.setAllotdrivervicleid(jsonObject.getString("allotdrivervicleid"));
            }
            if (jsonObject.has("es_shift") && !jsonObject.getString("es_shift").isEmpty() && !jsonObject.getString("es_shift").equalsIgnoreCase("null")) {
                casteObj.setEs_shift(jsonObject.getString("es_shift"));
            }
            if (jsonObject.has("d_mobile") && !jsonObject.getString("d_mobile").isEmpty() && !jsonObject.getString("d_mobile").equalsIgnoreCase("null")) {
                casteObj.setD_mobile(jsonObject.getString("d_mobile"));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return casteObj;

    }


}
