package com.ultimate.ultimatesmartschool.Transport.DriverNotice;

import android.content.Context;

import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Driver_Notice_Adapter extends RecyclerView.Adapter<Driver_Notice_Adapter.Viewholder> {
    ArrayList<DriverNoticeBean> sentnoticelist;
    Context context;
    Replycallback replycallback;
    public Driver_Notice_Adapter(ArrayList<DriverNoticeBean> sentnoticelist, Context context,Replycallback replycallback) {
        this.context=context;
        this.sentnoticelist=sentnoticelist;
        this.replycallback=replycallback;
    }

    @NonNull
    @Override
    public Driver_Notice_Adapter.Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.sntnotice_adpt_layout,parent,false);
        Driver_Notice_Adapter.Viewholder viewholder=new Driver_Notice_Adapter.Viewholder(view);
        return viewholder;

    }

    @Override
    public void onBindViewHolder(@NonNull Driver_Notice_Adapter.Viewholder holder, final int position) {
        holder.subject.setText(sentnoticelist.get(position).getSub());
        holder.message.setText(Html.fromHtml(sentnoticelist.get(position).getMsg()));
        holder.date.setText(Utils.getDateFormated(sentnoticelist.get(position).getCreated_on()));
        holder.sendertoname.setText(sentnoticelist.get(position).getTo_name()+"["+ sentnoticelist.get(position).getTo_type()+"]");


        if (sentnoticelist.get(position).getRecordingsfile() != null) {

            if(sentnoticelist.get(position).getMsg().equalsIgnoreCase("audio message")){
                holder.imageView.setVisibility(View.VISIBLE);

                holder.imageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        replycallback.onMethodCallback(sentnoticelist.get(position));

                    }
                });
            }else{

                holder.imageView.setVisibility(View.GONE);
            }

        } else {
            holder.imageView.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return sentnoticelist.size();
    }

    public void setsntnoticeList(ArrayList<DriverNoticeBean> sentnoticelist) {
        this.sentnoticelist = sentnoticelist;
    }

    public class Viewholder extends RecyclerView.ViewHolder {
        @BindView(R.id.txtsubject)
        TextView subject;
        @BindView(R.id.txtdate)TextView date;
        @BindView(R.id.message_body)TextView message;
        @BindView(R.id.txtsendername)TextView sendertoname;

        @BindView(R.id.imageViewrecord)
        ImageView imageView;
        //        @BindView(R.id.txtsubject)TextView subject;
//        @BindView(R.id.txtdate)TextView date;
//        @BindView(R.id.txtwhatNotice)TextView message;
//        @BindView(R.id.txtfrom)TextView sendertoname;
        public Viewholder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
    public interface Replycallback{

        public void onMethodCallback(DriverNoticeBean driverNoticeBean);
    }

}
