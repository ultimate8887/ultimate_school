package com.ultimate.ultimatesmartschool.Transport.TransBeanMod;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by admin on 6/19/2018.
 */

public class RouteBean {

    private static String ID = "id";
    private static String ROUTE_TITLE = "route_title";
    private static String CAPACITY = "capacity";
    private static String STATUS = "status";
    private static String CREATED_ON = "created_on";

    /**
     * id : 1
     * route_title : abs
     * capacity : 20
     * status : Active
     * created_on : 2018-02-13 08:57:51
     */

    private String id;
    private String route_title;
    private String capacity;
    private String status;
    private String created_on;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRoute_title() {
        return route_title;
    }

    public void setRoute_title(String route_title) {
        this.route_title = route_title;
    }

    public String getCapacity() {
        return capacity;
    }

    public void setCapacity(String capacity) {
        this.capacity = capacity;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreated_on() {
        return created_on;
    }

    public void setCreated_on(String created_on) {
        this.created_on = created_on;
    }

    public static ArrayList<RouteBean> parseTransportArray(JSONArray arrayObj) {
        ArrayList list = new ArrayList<RouteBean>();
        try {
            for (int i = 0; i < arrayObj.length(); i++) {
                RouteBean p = parseTransportBeanObject(arrayObj.getJSONObject(i));
                if (p != null) {
                    list.add(p);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    private static RouteBean parseTransportBeanObject(JSONObject jsonObject) {

        RouteBean casteObj=new RouteBean();
        try {
            if (jsonObject.has(ID)) {
                casteObj.setId(jsonObject.getString(ID));
            }
            if (jsonObject.has(ROUTE_TITLE) && !jsonObject.getString(ROUTE_TITLE).isEmpty() && !jsonObject.getString(ROUTE_TITLE).equalsIgnoreCase("null")) {
                casteObj.setRoute_title(jsonObject.getString(ROUTE_TITLE));
            }
            if (jsonObject.has(CAPACITY) && !jsonObject.getString(CAPACITY).isEmpty() && !jsonObject.getString(CAPACITY).equalsIgnoreCase("null")) {
                casteObj.setCapacity(jsonObject.getString(CAPACITY));
            }
            if (jsonObject.has(STATUS) && !jsonObject.getString(STATUS).isEmpty() && !jsonObject.getString(STATUS).equalsIgnoreCase("null")) {
                casteObj.setStatus(jsonObject.getString(STATUS));
            }
            if (jsonObject.has(CREATED_ON) && !jsonObject.getString(CREATED_ON).isEmpty() && !jsonObject.getString(CREATED_ON).equalsIgnoreCase("null")) {
                casteObj.setCreated_on(jsonObject.getString(CREATED_ON));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return casteObj;

    }

}
