package com.ultimate.ultimatesmartschool.Transport.driverlocation;

import android.content.Intent;

import android.os.Bundle;

import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;

import com.google.android.gms.maps.MapView;
import com.google.gson.Gson;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Transport.TransBeanMod.AlotDrivrToVicleBean;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.Constants;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DriverLocActivity extends AppCompatActivity implements DriverListLAdap.MethodCallBack {

    @BindView(R.id.parentss)
    RelativeLayout parent;
    @BindView(R.id.imgBack)
    ImageView imageBack;
    @BindView(R.id.recycle)
    RecyclerView recycleview;
    RecyclerView.LayoutManager layoutManager;
    ArrayList<AlotDrivrToVicleBean> arrayList = new ArrayList<>();
    private DriverListLAdap adap;
    @BindView(R.id.txtTitle)
    TextView txtTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_driver_loc);
        ButterKnife.bind(this);
        txtTitle.setText("Driver List");
        layoutManager = new LinearLayoutManager(this);
        recycleview.setLayoutManager(layoutManager);
        adap = new DriverListLAdap(arrayList, this, this);
        recycleview.setAdapter(adap);

    }

    @Override
    protected void onResume() {
        super.onResume();
        fetechDriverList();
    }

    @OnClick(R.id.imgBack)
    public void imgBackssss() {
        finish();
    }

    private void fetechDriverList() {
        ApiHandler.apiHit(Request.Method.GET, Constants.getBaseURL() + Constants.ALOT_DRIVR_VICLE_URL, apiCallBack, this, null);
    }


    ApiHandler.ApiCallback apiCallBack = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            if (error == null) {
                try {
                    if (arrayList != null) {
                        arrayList.clear();
                        ArrayList<AlotDrivrToVicleBean> arrayList1 = AlotDrivrToVicleBean.parseDrivrToVicleArray(jsonObject.getJSONArray("alotdv_data"));
                        for (int i = 0; i < arrayList1.size(); i++) {
                            if (arrayList1.get(i).getD_id() != null) {
                                arrayList.add(arrayList1.get(i));
                            }
                        }
                        adap.setArrayList(arrayList);
                        adap.notifyDataSetChanged();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    };

    @Override
    public void ViewLocation(AlotDrivrToVicleBean driverListBean) {
        Gson gson = new Gson();
        String dv = gson.toJson(driverListBean, AlotDrivrToVicleBean.class);
        Intent i = new Intent(DriverLocActivity.this, ViewMap.class);
        i.putExtra("driverv", dv);
        startActivity(i);

    }
}
