package com.ultimate.ultimatesmartschool.Transport.DriverListMod;

import android.content.Context;

import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;


import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.github.siyamed.shapeimageview.CircularImageView;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Transport.TransBeanMod.DriverListBean;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by admin on 6/21/2018.
 */

public class DriverListAdap extends RecyclerView.Adapter<DriverListAdap.ViewHolder> {

      ArrayList<DriverListBean> arrayList;
      Context context;
      private MethodCallBack callBackMethod;

    public DriverListAdap(ArrayList<DriverListBean> arrayList, Context context,MethodCallBack callBackMethod) {
        this.arrayList=arrayList;
        this.context=context;
        this.callBackMethod=callBackMethod;
    }

    public void setArrayList(ArrayList<DriverListBean> arrayList) {
        this.arrayList = arrayList;
    }

    public interface MethodCallBack {

        void addDriverCallBack(DriverListBean driverListBean, int pos);
        void deleteDriverCallback(DriverListBean driverListBean, int pos);
        void editDriverCallback(DriverListBean driverListBean, int pos);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.address)
        TextView address;
        @BindView(R.id.mobile)
        TextView mobile;
        @BindView(R.id.liecense)
        TextView liecense;
        @BindView(R.id.validUpto)
        TextView validUpto;
        @BindView(R.id.issuing)
        TextView issuing;
        @BindView(R.id.lytAddData)
        RelativeLayout lytAddData;
        @BindView(R.id.lytAll)
        RelativeLayout lytAll;
        @BindView(R.id.trash)
        ImageView delete;

        @BindView(R.id.more)
        RelativeLayout more;
        @BindView(R.id.moree)
        RelativeLayout moree;
        @BindView(R.id.p_add)
        TextView p_add;
        @BindView(R.id.p_mini)
        TextView p_mini;
        @BindView(R.id.txtproof)
        EditText txtproof;
        @BindView(R.id.txtproofn)
        TextView txtproofn;
        @BindView(R.id.guardian_add)
        TextView guardian_add;
        @BindView(R.id.guardian_mini)
        TextView guardian_mini;

        @BindView(R.id.txtcontprsn)
        EditText txtcontprsn;
        @BindView(R.id.txtcontprsnnum)
        TextView txtcontprsnnum;
        @BindView(R.id.txtrelation)
        TextView txtrelation;
        @BindView(R.id.txtconfirmationnum)
        TextView txtconfirmationnum;
        @BindView(R.id.albm_id)
        TextView albm_id;
        @BindView(R.id.doc_image)
        ImageView edtImageDoc;
        @BindView(R.id.visitimage)
        CircularImageView edtProfImage;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.driver_list_adap, parent, false);
        ViewHolder viewHolder=new DriverListAdap.ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        if (arrayList.size() == position) {   //suppose data size is 4 then when position is 4 we have show add UI as it start count from o  then 4th position means  index 5
            holder.lytAll.setVisibility(View.GONE);
            holder.lytAddData.setVisibility(View.VISIBLE);
            holder.lytAddData.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (callBackMethod != null) {
                        callBackMethod.addDriverCallBack(null,-1);
                    }
                }
            });
        } else {

            DriverListBean mData=arrayList.get(position);


            if (arrayList.get(position).getId() != null) {
                String title = getColoredSpanned("ID: ", "#000000");
                String Name = getColoredSpanned(arrayList.get(position).getId(), "#5A5C59");
                holder.albm_id.setText(Html.fromHtml(title + " " + Name));
            }

            holder.p_add.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    holder.p_add.setVisibility(View.GONE);
                    holder.p_mini.setVisibility(View.VISIBLE);
                    holder.moree.setVisibility(View.VISIBLE);
                }
            });


            holder.moree.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    holder.p_add.setVisibility(View.VISIBLE);
                    holder.p_mini.setVisibility(View.GONE);
                    holder.moree.setVisibility(View.GONE);
                }
            });


            holder.guardian_add.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    holder.guardian_add.setVisibility(View.GONE);
                    holder.guardian_mini.setVisibility(View.VISIBLE);
                    holder.more.setVisibility(View.VISIBLE);
                }
            });

            holder.more.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    holder.guardian_add.setVisibility(View.VISIBLE);
                    holder.guardian_mini.setVisibility(View.GONE);
                    holder.more.setVisibility(View.GONE);
                }
            });



            holder.lytAll.setVisibility(View.VISIBLE);
            holder.lytAddData.setVisibility(View.GONE);
            holder.name.setText(arrayList.get(position).getDriver_name());
            holder.address.setText(arrayList.get(position).getDriver_addrs());
            holder.mobile.setText(arrayList.get(position).getDiver_mobile());
            holder.liecense.setText(arrayList.get(position).getDriver_license());
            holder.validUpto.setText(arrayList.get(position).getValid_date());
            holder.issuing.setText(arrayList.get(position).getIssuing_authority());
            holder.txtcontprsn.setEnabled(false);
            holder.txtproof.setEnabled(false);


            String image_url="https://ultimatesolutiongroup.com/office_admin/images/dirverdoc/"+mData.getImage();
            String doc_image_url="https://ultimatesolutiongroup.com/office_admin/images/dirverdoc/"+mData.getLicense_doc();


//            Log.e("image_url",image_url+mData.getImage());
//            Log.e("image_url_doc",image_url+mData.getLicense_doc());

            if (mData.getImage()!=null){
                //   Picasso.with(UpdateVehicleActivity.this).load(image_url).placeholder(R.drawable.boy).into(visitimage);
                Utils.progressImg_two(image_url,holder.edtProfImage,context,"prof");
            }

            if (mData.getLicense_doc()!=null){
                //   Picasso.with(UpdateVehicleActivity.this).load(image_url).placeholder(R.drawable.boy).into(visitimage);
                Utils.progressImg_two(doc_image_url,holder.edtImageDoc,context,"doc");
            }

            if (mData.getDriver_license() != null) {
                holder.txtcontprsn.setVisibility(View.VISIBLE);
                holder.guardian_add.setVisibility(View.VISIBLE);
                String title = getColoredSpanned("License No: ", "#5A5C59");
                String Name = getColoredSpanned(mData.getDriver_license(), "#7D7D7D");
                holder.txtcontprsn.setText(Html.fromHtml(title + " " + Name));
            }else {
                holder.txtcontprsn.setText("Not Mentioned");
            }

            if (mData.getIssuing_authority() != null) {
                holder.txtcontprsnnum.setVisibility(View.VISIBLE);
                String title = getColoredSpanned("Issuing Authority: ", "#5A5C59");
                String Name = getColoredSpanned(mData.getIssuing_authority(), "#7D7D7D");
                holder.txtcontprsnnum.setText(Html.fromHtml(title + " " + Name));
            } else {
                holder.txtcontprsnnum.setText("Not Mentioned");
            }

            if (mData.getValid_date() != null) {
                holder.txtrelation.setVisibility(View.VISIBLE);
                String title = getColoredSpanned("Valid UpTo: ", "#5A5C59");
                String Name = getColoredSpanned(Utils.getDateFormated(mData.getValid_date()), "#7D7D7D");
                holder.txtrelation.setText(Html.fromHtml(title + " " + Name));
            }else {
                holder.txtrelation.setText("Not Mentioned");
            }


            if (mData.getUsername() != null) {
                String title = getColoredSpanned("Username: ", "#5A5C59");
                String Name = getColoredSpanned(mData.getUsername(), "#7D7D7D");
                holder.txtproof.setText(Html.fromHtml(title + " " + Name));
            }

            if (mData.getPassword() != null) {
                String title = getColoredSpanned("Password: ", "#5A5C59");
                String Name = getColoredSpanned(mData.getPassword(), "#7D7D7D");
                holder.txtproofn.setText(Html.fromHtml(title + " " + Name));
            }

            holder.lytAll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (callBackMethod != null) {
                        callBackMethod.editDriverCallback(arrayList.get(position),position);
                    }
                }
            });
            holder.delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (callBackMethod != null) {

                        callBackMethod.deleteDriverCallback(arrayList.get(position),position);
                    }
                }
            });
        }

    }

    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }

    @Override
    public int getItemCount() {
        return arrayList.size() + 1;
    }
}
