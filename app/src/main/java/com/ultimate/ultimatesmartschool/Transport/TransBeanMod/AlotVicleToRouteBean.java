package com.ultimate.ultimatesmartschool.Transport.TransBeanMod;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by admin on 6/23/2018.
 */

public class AlotVicleToRouteBean {

    private static String ID = "id";
    private static String TITLE = "title";
    private static String V_ID = "v_id";
    private static String V_TITILE = "v_titile";
    private static String V_TYPE = "v_type";
    private static String V_NO = "v_no";

    /**
     * id : 18
     * title : green city
     * v_id : 10
     * v_titile : anuuuuuuu
     * v_type : bus
     */

    private String id;
    private String title;
    private String v_id;
    private String v_titile;
    private String v_type;
    private String  route_title;
    private String board_id;
    private String vicltorouteid;

    public String getRoute_title() {
        return route_title;
    }

    public void setRoute_title(String route_title) {
        this.route_title = route_title;
    }

    public String getBoard_id() {
        return board_id;
    }

    public void setBoard_id(String board_id) {
        this.board_id = board_id;
    }

    public String getVicltorouteid() {
        return vicltorouteid;
    }

    public void setVicltorouteid(String vicltorouteid) {
        this.vicltorouteid = vicltorouteid;
    }

    /**
     * v_no : 12312345555555
     */

    private String v_no;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getV_id() {
        return v_id;
    }

    public void setV_id(String v_id) {
        this.v_id = v_id;
    }

    public String getV_titile() {
        return v_titile;
    }

    public void setV_titile(String v_titile) {
        this.v_titile = v_titile;
    }

    public String getV_type() {
        return v_type;
    }

    public void setV_type(String v_type) {
        this.v_type = v_type;
    }

    public static ArrayList<AlotVicleToRouteBean> parseVicleToRouteArray(JSONArray arrayObj) {
        ArrayList list = new ArrayList<AlotVicleToRouteBean>();
        try {
            for (int i = 0; i < arrayObj.length(); i++) {
                AlotVicleToRouteBean p = parseVicleToRouteBeanObject(arrayObj.getJSONObject(i));
                if (p != null) {
                    list.add(p);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
      }

      private static AlotVicleToRouteBean parseVicleToRouteBeanObject(JSONObject jsonObject) {

        AlotVicleToRouteBean casteObj=new AlotVicleToRouteBean();

        try {
            if (jsonObject.has(ID)) {
                casteObj.setId(jsonObject.getString(ID));
            }
            if (jsonObject.has(TITLE) && !jsonObject.getString(TITLE).isEmpty() && !jsonObject.getString(TITLE).equalsIgnoreCase("null")) {
                casteObj.setTitle(jsonObject.getString(TITLE));
            }
            if (jsonObject.has(V_ID) && !jsonObject.getString(V_ID).isEmpty() && !jsonObject.getString(V_ID).equalsIgnoreCase("null")) {
                casteObj.setV_id(jsonObject.getString(V_ID));
            }
            if (jsonObject.has(V_TITILE) && !jsonObject.getString(V_TITILE).isEmpty() && !jsonObject.getString(V_TITILE).equalsIgnoreCase("null")) {
                casteObj.setV_titile(jsonObject.getString(V_TITILE));
            }
            if (jsonObject.has(V_TYPE) && !jsonObject.getString(V_TYPE).isEmpty() && !jsonObject.getString(V_TYPE).equalsIgnoreCase("null")) {
                casteObj.setV_type(jsonObject.getString(V_TYPE));
            }
            if (jsonObject.has(V_NO) && !jsonObject.getString(V_NO).isEmpty() && !jsonObject.getString(V_NO).equalsIgnoreCase("null")) {
                casteObj.setV_no(jsonObject.getString(V_NO));
            }
            if (jsonObject.has("route_title") && !jsonObject.getString("route_title").isEmpty() && !jsonObject.getString("route_title").equalsIgnoreCase("null")) {
                casteObj.setRoute_title(jsonObject.getString("route_title"));
            }
            if (jsonObject.has("board_id") && !jsonObject.getString("board_id").isEmpty() && !jsonObject.getString("board_id").equalsIgnoreCase("null")) {
                casteObj.setBoard_id(jsonObject.getString("board_id"));
            }
            if (jsonObject.has("vicltorouteid") && !jsonObject.getString("vicltorouteid").isEmpty() && !jsonObject.getString("vicltorouteid").equalsIgnoreCase("null")) {
                casteObj.setVicltorouteid(jsonObject.getString("vicltorouteid"));
            }
           } catch (JSONException e) {
            e.printStackTrace();
          }
            return casteObj;
        }

    public String getV_no() {
        return v_no;
    }

    public void setV_no(String v_no) {
        this.v_no = v_no;
    }
}


