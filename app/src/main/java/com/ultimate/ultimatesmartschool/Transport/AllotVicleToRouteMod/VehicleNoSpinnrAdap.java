package com.ultimate.ultimatesmartschool.Transport.AllotVicleToRouteMod;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;


import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Transport.TransBeanMod.VehicleBean;

import java.util.ArrayList;

/**
 * Created by admin on 6/23/2018.
 */

public class VehicleNoSpinnrAdap extends BaseAdapter {

    private final ArrayList<VehicleBean> vehicleList;
    Context contexts;
    LayoutInflater inflter;


    public VehicleNoSpinnrAdap (Context contexts, ArrayList<VehicleBean> vehicleList) {
        this.contexts=contexts;
        this.vehicleList=vehicleList;
        inflter= (LayoutInflater.from(contexts));

    }

    @Override
    public int getCount() {
        return vehicleList.size() + 1;
    }
    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }
    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        view=inflter.inflate(R.layout.spinner_prodvehi,null);
        TextView select=(TextView) view.findViewById(R.id.txtText);
        if (i == 0) {
            // Default selected Spinner item
            select.setText("Select Vehicle no.");

        } else {
            VehicleBean castObj=vehicleList.get(i-1);
            select.setText(castObj.getTr_vehicle_no() + " (" + castObj.getTr_transport_name() + ")");
        }
        return view;
    }
}
