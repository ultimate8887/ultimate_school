package com.ultimate.ultimatesmartschool.Transport.AlotDrivrToVicleMod;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;


import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Transport.TransBeanMod.DriverListBean;

import java.util.ArrayList;

/**
 * Created by admin on 6/25/2018.
 */

public class DrivrDetailSpinnrAdap extends BaseAdapter {

    private final ArrayList<DriverListBean> driverList;
    Context contexts;
    LayoutInflater inflter;

    public DrivrDetailSpinnrAdap(Context contexts, ArrayList<DriverListBean> driverList) {
        this.driverList = driverList;
        this.contexts=contexts;
        inflter= (LayoutInflater.from(contexts));
    }

    @Override
    public int getCount() {
        return driverList.size() + 1;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view=inflter.inflate(R.layout.spinner_prod,null);
        TextView select=(TextView) view.findViewById(R.id.txtText);
        if (i == 0) {
            // Default selected Spinner item
            select.setText("Select Driver");

        } else {
            DriverListBean castObj=driverList.get(i-1);
           // select.setText(castObj.getId() + " (" + castObj.getDriver_name() + ")");
            select.setText( castObj.getDriver_name() );
        }
        return view;
    }
}
