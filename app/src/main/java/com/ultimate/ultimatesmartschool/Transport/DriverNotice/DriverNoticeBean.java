package com.ultimate.ultimatesmartschool.Transport.DriverNotice;


import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.DebugLog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class DriverNoticeBean {

    private static String MSG_ID="id";
    private static String SUBJECT="sub";
    private static String MESSAGE="msg";
    private static String DATE="created_on";
    //private static String TO_TYPE="from_type";
    private static String TO_TYPE="to_type";
    private static String TO_NAME="to_name";
    private static String RE_MSG="re_msg";


    private String  recordingsfile;

    public String getRecordingsfile() {
        return recordingsfile;
    }

    public void setRecordingsfile(String recordingsfile) {
        this.recordingsfile = recordingsfile;
    }
    private String id;
    private String from_id;
    private String from_type;
    private String to_id;
    private String to_type;
    private String sub;

    public static String getMsgId() {
        return MSG_ID;
    }

    public static void setMsgId(String msgId) {
        MSG_ID = msgId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFrom_id() {
        return from_id;
    }

    public void setFrom_id(String from_id) {
        this.from_id = from_id;
    }

    public String getFrom_type() {
        return from_type;
    }

    public void setFrom_type(String from_type) {
        this.from_type = from_type;
    }

    public String getTo_id() {
        return to_id;
    }

    public void setTo_id(String to_id) {
        this.to_id = to_id;
    }

    public String getTo_type() {
        return to_type;
    }

    public void setTo_type(String to_type) {
        this.to_type = to_type;
    }

    public String getSub() {
        return sub;
    }

    public void setSub(String sub) {
        this.sub = sub;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getCreated_on() {
        return created_on;
    }

    public void setCreated_on(String created_on) {
        this.created_on = created_on;
    }

    public String getTo_name() {
        return to_name;
    }

    public void setTo_name(String to_name) {
        this.to_name = to_name;
    }

    private String msg;
    private String created_on;
    private String to_name;



    public static ArrayList<DriverNoticeBean> parseMessageArray(JSONArray jsonArray) {
        ArrayList<DriverNoticeBean> list = new ArrayList<DriverNoticeBean>();
        try {

            for (int i = 0; i < jsonArray.length(); i++) {
                DriverNoticeBean p = parsemessageObject(jsonArray.getJSONObject(i));
                if (p != null) {
                    list.add(p);
                }
            }
        } catch (Exception e) {
            DebugLog.printLog("exp",e.getMessage());
            e.printStackTrace();
        }
        return list;

    }

    private static DriverNoticeBean parsemessageObject(JSONObject jsonObject) {

        DriverNoticeBean msgObj = new DriverNoticeBean();
        try {


            if (jsonObject.has(SUBJECT) && !jsonObject.getString(SUBJECT).isEmpty() && !jsonObject.getString(SUBJECT).equalsIgnoreCase("null")) {
                msgObj.setSub(jsonObject.getString(SUBJECT));
            }
            if (jsonObject.has(DATE) && !jsonObject.getString(DATE).isEmpty() && !jsonObject.getString(DATE).equalsIgnoreCase("null")) {
                msgObj.setCreated_on(jsonObject.getString(DATE));
            }
            if (jsonObject.has(MESSAGE) && !jsonObject.getString(MESSAGE).isEmpty() && !jsonObject.getString(MESSAGE).equalsIgnoreCase("null")) {
                msgObj.setMsg(jsonObject.getString(MESSAGE));
            }
            if (jsonObject.has(TO_TYPE) && !jsonObject.getString(TO_TYPE).isEmpty() && !jsonObject.getString(TO_TYPE).equalsIgnoreCase("null")) {
                msgObj.setTo_type(jsonObject.getString(TO_TYPE));
            }

            if (jsonObject.has(TO_NAME) && !jsonObject.getString(TO_NAME).isEmpty() && !jsonObject.getString(TO_NAME).equalsIgnoreCase("null")) {
                msgObj.setTo_name(jsonObject.getString(TO_NAME));
            }

            if (jsonObject.has("recordingsfile") && !jsonObject.getString("recordingsfile").isEmpty() && !jsonObject.getString("recordingsfile").equalsIgnoreCase("null")) {
                msgObj.setRecordingsfile(Constants.getImageBaseURL()+jsonObject.getString("recordingsfile"));
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }
        return msgObj;


    }
}
