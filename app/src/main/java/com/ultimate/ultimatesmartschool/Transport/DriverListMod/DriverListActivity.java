package com.ultimate.ultimatesmartschool.Transport.DriverListMod;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;

import android.os.Bundle;

import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;

import com.google.gson.Gson;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Transport.PickUpMod.PickUpActivity;
import com.ultimate.ultimatesmartschool.Transport.TransBeanMod.DriverListBean;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DriverListActivity extends AppCompatActivity implements DriverListAdap.MethodCallBack {

    @BindView(R.id.parentss)
    RelativeLayout parent;
    Dialog dialog;
    @BindView(R.id.imgBack)
    ImageView imageBack;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.recycle)
    RecyclerView recycleview;
    RecyclerView.LayoutManager layoutManager;
    ArrayList<DriverListBean> arrayList= new ArrayList<>();
    private DriverListAdap adap;
    @BindView(R.id.totalRecord)
    TextView totalRecord;
    @BindView(R.id.ggg)
    TextView ggg;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_common_lyt);
        ButterKnife.bind(this);
        txtTitle.setText("Driver List");
        ggg.setText("Tap to edit Driver details!");
        layoutManager=new LinearLayoutManager(this);
        recycleview.setLayoutManager(layoutManager);
        adap=new DriverListAdap(arrayList,this,this);
        recycleview.setAdapter(adap);
    }

    @Override
    protected void onResume() {
        super.onResume();
        fetechDriverList();
    }
    @OnClick(R.id.imgBack)
    public void imgBackssss() {
        finish();
    }

    private void fetechDriverList() {
        ErpProgress.showProgressBar(this,"Please wait...");
        HashMap<String,String> params=new HashMap<>();
        ApiHandler.apiHit(Request.Method.GET, Constants.getBaseURL() + Constants.DRIVERLIST_URL,apiCallBack,this,null);
    }
    ApiHandler.ApiCallback apiCallBack=new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
        if (error==null){
            try{
                 if ( arrayList!= null) {
                     arrayList.clear();
                     arrayList = DriverListBean.parseDriverListArray(jsonObject.getJSONArray("driver_data"));
                     adap.setArrayList(arrayList);
                     totalRecord.setText("Total Entries:- "+String.valueOf(arrayList.size()));
                     adap.notifyDataSetChanged();
                 }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }else {
            totalRecord.setText("Total Entries:- 0");
            Toast.makeText(DriverListActivity.this,error.getMessage(),Toast.LENGTH_SHORT).show();
        }
        }
    };

    @Override
    public void addDriverCallBack(DriverListBean driverListBean, int pos) {

        Intent intent=new Intent(DriverListActivity.this,AddDriverActivity.class);
        startActivity(intent);

    }

    @Override
    public void deleteDriverCallback(final DriverListBean driverListBean, int pos) {
        DialogInterface.OnClickListener onClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:

                        HashMap<String, String> params = new HashMap<String, String>();
                        String url = Constants.getBaseURL() + Constants.DRIVERLIST_URL + Constants.TRID + driverListBean.getId();
                        ApiHandler.apiHit(Request.Method.DELETE, url, apiCallBack,DriverListActivity.this, params);
                        Toast.makeText(DriverListActivity.this,"Delete Successfully",Toast.LENGTH_SHORT).show();
                        break;
                    case DialogInterface.BUTTON_NEGATIVE:
                        //No button clicked
                        break;
                }
            }
        };
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Are you sure you want to delete?").setPositiveButton("Yes", onClickListener)
                .setNegativeButton("No", onClickListener).show();

    }

    @Override
    public void editDriverCallback(DriverListBean driverListBean, int pos) {
        Gson gson = new Gson();
        String driver_data = gson.toJson(driverListBean, DriverListBean.class);
        Intent intent = new Intent(DriverListActivity.this, UpdateDriverActivity.class);
        intent.putExtra("driver_data", driver_data);
        startActivity(intent);
    }
}
