package com.ultimate.ultimatesmartschool.Transport.AlotDrivrToVicleMod;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;

import android.os.Bundle;

import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.google.android.material.snackbar.Snackbar;
import com.ultimate.ultimatesmartschool.BeanModule.User;
import com.ultimate.ultimatesmartschool.Login.LoginActivity;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Transport.AllotVicleToRouteMod.VicleToRouteActivity;
import com.ultimate.ultimatesmartschool.Transport.TransBeanMod.AlotDrivrToVicleBean;
import com.ultimate.ultimatesmartschool.Transport.TransBeanMod.DriverListBean;
import com.ultimate.ultimatesmartschool.Transport.TransBeanMod.VehicleBean;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;
import com.ultimate.ultimatesmartschool.Utility.Utils;


import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DrivrToVicleActivity extends AppCompatActivity implements DrivrToVicleAdap.OnMethodCallBack{


    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.recycle)
    RecyclerView recycleview;
    @BindView(R.id.tapedit)
    TextView tapedit;
    Dialog dialog;
    TextView txtVehicleNo;
    Spinner spinerDriver;
    @BindView(R.id.parentss)
    RelativeLayout parent;
    RecyclerView.LayoutManager layoutManager;
    private DrivrToVicleAdap adap;
    DriverListBean driverListBean;

    ArrayList<AlotDrivrToVicleBean> arrayList=new ArrayList<>();
    ArrayList<DriverListBean> driverDetails= new ArrayList<>();
    ArrayList<VehicleBean> arrayListveh=new ArrayList<>();
    Spinner spinevehicle;
    Spinner spinerdriver2;
    VehicleBean vehicleBean;

    String vehicle_id;
    String driver_id;
    AlotDrivrToVicleBean alotDrivrToVicleBean2;
    String alotdrivertovehicle_id;
    Spinner spinerdriverShift;
    ArrayList<String> shifttype = new ArrayList<String>();
    String shifttypetime;

    @BindView(R.id.totalRecord)
    TextView totalRecord;
    @BindView(R.id.ggg)
    TextView ggg;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_common_lyt);
        ButterKnife.bind(this);
        txtTitle.setText("Allot Driver To Vehicle");
        ggg.setText("Tap to Allot Driver to Vehicle");
        layoutManager=new LinearLayoutManager(this);
        recycleview.setLayoutManager(layoutManager);
        adap=new DrivrToVicleAdap(arrayList,this,this);
        recycleview.setAdapter(adap);

        shifttype.add("Morning");
        shifttype.add("Evening");


    }

    @Override
    protected void onResume() {
        super.onResume();
        fetchList();
    }

    private void fetchList() {
        ErpProgress.showProgressBar(this,"Please wait...");
        HashMap<String,String> params=new HashMap<>();
        ApiHandler.apiHit(Request.Method.GET, Constants.getBaseURL() + Constants.ALOT_DRIVR_VICLE_URL, apiCallBack, this,null);
      }

      ApiHandler.ApiCallback apiCallBack=new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            if (error==null){
                try{
                    if (arrayList!=null){
                       arrayList.clear();
                       arrayList= AlotDrivrToVicleBean.parseDrivrToVicleArray(jsonObject.getJSONArray("alotdv_data"));
                       adap.setArrayList(arrayList);
                        totalRecord.setText("Total Entries:- "+String.valueOf(arrayList.size()));
                       adap.notifyDataSetChanged();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            else {
                totalRecord.setText("Total Entries:- 0");
                Toast.makeText(DrivrToVicleActivity.this,error.getMessage(),Toast.LENGTH_SHORT).show();
            }
        }
    };

    @OnClick(R.id.imgBack)
     public void imgBackssss() {
        finish();
    }
//here is now updating the drivertovehicle allotment
    @Override
    public void alotDrivrVehicleCallBack(final AlotDrivrToVicleBean alotDrivrToVicleBean,int pos) {


        alotDrivrToVicleBean2=alotDrivrToVicleBean;
        dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.add_drivertovehicle_dialog);
        Button btnSubmit = (Button) dialog.findViewById(R.id.btnPickUpSubmit);
        btnSubmit.setText("Update");
        spinevehicle= (Spinner) dialog.findViewById(R.id.spinevehicle);
        spinerdriver2= (Spinner) dialog.findViewById(R.id.spinerdriver);
        spinerdriverShift=(Spinner) dialog.findViewById(R.id.spinerdriverShift);
        fetchVicledetailsupdate();
        fetchDriverDetail2update();
         alotdrivertovehicle_id=alotDrivrToVicleBean.getAllotdrivervicleid();
        Toast.makeText(getApplicationContext(),alotdrivertovehicle_id,Toast.LENGTH_LONG).show();
        ArrayAdapter<String> foodAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, shifttype);
        foodAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinerdriverShift.setAdapter(foodAdapter);
        spinerdriverShift.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                shifttypetime=parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        for(int i=0;i<shifttype.size();i++) {
            if (shifttype.get(i).equalsIgnoreCase(alotDrivrToVicleBean.getEs_shift())) {
                shifttypetime = shifttype.get(i);
                spinerdriverShift.setSelection(i);
            }
        }

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Utils.hideKeyboard(DrivrToVicleActivity.this);
                if (checkValidation()) {

                    updateDetail(alotdrivertovehicle_id);
                    //updateDetail(alotDrivrToVicleBean);
                    dialog.dismiss();
                    Toast.makeText(DrivrToVicleActivity.this,"Updated Successfully",Toast.LENGTH_SHORT).show();

                }
            }
        });

       // dialog.setContentView(R.layout.alot_common_dialog);
//        txtVehicleNo = (TextView) dialog.findViewById(R.id.txtTitles);
//        TextView txtSetup = (TextView) dialog.findViewById(R.id.txtSetup);
//        Button btnUpdate = (Button) dialog.findViewById(R.id.btnAlotUpdate);
//        spinerDriver = (Spinner) dialog.findViewById(R.id.spinerDetails);
//        fetchDriverDetail();
//
//        txtSetup.setText("Allot Driver To Vehicle");
//
//        String title = getColoredSpanned("Vehicle no: ", "#7D7D7D");
//        String Name = getColoredSpanned(alotDrivrToVicleBean.getV_no(),"#9c9da1");
//        txtVehicleNo.setText(Html.fromHtml(title+" "+Name));
//
//        btnUpdate.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Utils.hideKeyboard(DrivrToVicleActivity.this);
//                updateDetail(alotDrivrToVicleBean);
//
//            }
//        });
        dialog.show();

    }

    @Override
    public void addDriverVehicleCallBack(AlotDrivrToVicleBean alotDrivrToVicleBean, final int pos) {
        final Dialog  dialogs = new Dialog(this);
        dialogs.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogs.setCancelable(true);
        dialogs.setContentView(R.layout.add_drivertovehicle_dialog);

        Button btnSubmit = (Button) dialogs.findViewById(R.id.btnPickUpSubmit);
        spinevehicle= (Spinner) dialogs.findViewById(R.id.spinevehicle);
        spinerdriver2= (Spinner) dialogs.findViewById(R.id.spinerdriver);
        spinerdriverShift=(Spinner) dialogs.findViewById(R.id.spinerdriverShift);
        fetchVicledetails();
        fetchDriverDetail2();
        ArrayAdapter<String> foodAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, shifttype);
        foodAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinerdriverShift.setAdapter(foodAdapter);
        spinerdriverShift.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                shifttypetime=parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Utils.hideKeyboard(DrivrToVicleActivity.this);
                if (checkValidation()) {
                    if (pos == -1) {

                        addNewDrivertoVehicle();
                        dialogs.dismiss();
                        Toast.makeText(DrivrToVicleActivity.this,"Add Successfully",Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
        dialogs.show();
//        Intent intent=new Intent(DrivrToVicleActivity.this, AddDrivertoVehicleActivity.class);
//        startActivity(intent);
    }

    private void addNewDrivertoVehicle() {
        HashMap<String,String> params=new HashMap<String, String>();

            params.put("vehicle_id",vehicle_id);
            params.put("driver_id",driver_id);
            //params.put("shift",shifttypetime);
            ApiHandler.apiHit(Request.Method.POST,Constants.getBaseURL() + Constants.ADDDRIVERTOVEHICLE_URL,apiCallBackadtv,DrivrToVicleActivity.this,params);

    }

    ApiHandler.ApiCallback apiCallBackadtv = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            if (error == null) {
                try {
                    Toast.makeText(getApplicationContext(),jsonObject.getString("msg"),Toast.LENGTH_LONG).show();
                    Utils.showSnackBar(jsonObject.getString("msg"), parent);
                    finish();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("error", error.getMessage() + "");
                Utils.showSnackBar(error.getMessage(), parent);
                // Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_LONG).show();
                if (error.getStatusCode() == 405) {
                    User.logout();
                    startActivity(new Intent(DrivrToVicleActivity.this, LoginActivity.class));
                    finish();
                }
            }
        }
    };
    private boolean checkValidation() {

        boolean valid = true;
        String errorMsg = null;
         if (vehicle_id == null || vehicle_id.isEmpty()) {
             errorMsg = "Please select vehicle";
             valid = false;
        } else if (driver_id == null || driver_id.isEmpty()) {
            valid = false;
            errorMsg = "Please enter driver";
        }
        if (!valid) {
            showSnackBar(errorMsg);
        }
        return valid;
    }
    private void showSnackBar(String errorMsg) {

        Snackbar snack = Snackbar.make(parent, errorMsg, Snackbar.LENGTH_LONG);
        View view = snack.getView();
//        TextView tv = (TextView) view.findViewById(R.id.snackbar_text);
//        tv.setTextColor(Color.WHITE);
        snack.show();
    }

    private void updateDetail(String alotdrivertovehicle_id) {

        HashMap<String, String> params = new HashMap<>();
        params.put("v_id",alotdrivertovehicle_id);
        params.put("d_id",driver_id);
        params.put("v_no",vehicle_id);
       // params.put("shift",shifttypetime);
        Log.e("v_id",alotdrivertovehicle_id);
        Log.e("d_id",driver_id);
        Log.e("v_no",vehicle_id);
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.ALOT_DRIVR_VICLE_URL, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                if (error == null) {
                    fetchList();

                } else {
                    Utils.showSanckBar(parent, error.getMessage());
                }
            }
        }, this, params);

      }

    // it is used for set multiple color on single textview acc user requirments.....
    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }

    private void fetchDriverDetail() {

        HashMap<String,String> params=new HashMap<>();
        ApiHandler.apiHit(Request.Method.GET, Constants.getBaseURL() + Constants.DRIVERLIST_URL,apiSpinnrCallBack,this,null);
    }
     ApiHandler.ApiCallback apiSpinnrCallBack=new ApiHandler.ApiCallback() {
         @Override
         public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
         if (error==null) {
             try {
                 driverDetails = DriverListBean.parseDriverListArray(jsonObject.getJSONArray("driver_data"));
                 DrivrDetailSpinnrAdap adap1=new DrivrDetailSpinnrAdap(DrivrToVicleActivity.this,driverDetails);
                 spinerDriver.setAdapter(adap1);
                 spinerDriver.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                     @Override
                     public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                         if (i > 0) {
                             driverListBean = driverDetails.get(i - 1);
                         }
                     }
                     @Override
                     public void onNothingSelected(AdapterView<?> adapterView) {
                     }
                 });
             } catch (JSONException e) {
                 e.printStackTrace();
             }
         }


         }
     };
    private void fetchDriverDetail2() {

        HashMap<String,String> params=new HashMap<>();
        ApiHandler.apiHit(Request.Method.GET, Constants.getBaseURL() + Constants.DRIVERLIST_URL,apiSpinnrCallBack2,this,null);
    }
    ApiHandler.ApiCallback apiSpinnrCallBack2=new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            if (error==null) {
                try {
                    driverDetails = DriverListBean.parseDriverListArray(jsonObject.getJSONArray("driver_data"));
                    DrivrDetailSpinnrAdap adap1=new DrivrDetailSpinnrAdap(DrivrToVicleActivity.this,driverDetails);
                    spinerdriver2.setAdapter(adap1);
                    spinerdriver2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if (i > 0) {
                                driverListBean = driverDetails.get(i - 1);
                                driver_id=driverDetails.get(i - 1).getId();
                            }
                        }
                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {
                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }


        }
    };



    private void fetchVicledetails() {
        HashMap<String,String> params=new HashMap<>();
        ApiHandler.apiHit(Request.Method.GET, Constants.getBaseURL() + Constants.VEHICLE_URL, apiCallBackvrh, this,null);
    }

    ApiHandler.ApiCallback apiCallBackvrh=new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            if (error==null){
                try{


                    arrayListveh= VehicleBean.parseVehicleArray(jsonObject.getJSONArray("vehical_data"));
                    VehicledetailSpinerAdap adap1=new VehicledetailSpinerAdap(DrivrToVicleActivity.this,arrayListveh);
                    spinevehicle.setAdapter(adap1);
                    spinevehicle.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if (i > 0) {
                                vehicleBean = arrayListveh.get(i - 1);
                                vehicle_id=arrayListveh.get(i-1).getEs_transportid();

                            }
                        }
                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {
                        }
                    });



                } catch (JSONException e) {
                    e.printStackTrace();
                }

            } else {
                Toast.makeText(DrivrToVicleActivity.this,error.getMessage(),Toast.LENGTH_SHORT).show();
            }
        }
    };

    private void fetchVicledetailsupdate() {
        HashMap<String,String> params=new HashMap<>();
        ApiHandler.apiHit(Request.Method.GET, Constants.getBaseURL() + Constants.VEHICLE_URL, apiCallBackvrhupdt, this,null);
    }

    ApiHandler.ApiCallback apiCallBackvrhupdt=new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            if (error==null){
                try{


                    arrayListveh= VehicleBean.parseVehicleArray(jsonObject.getJSONArray("vehical_data"));
                    VehicledetailSpinerAdap adap1=new VehicledetailSpinerAdap(DrivrToVicleActivity.this,arrayListveh);
                    spinevehicle.setAdapter(adap1);
                    spinevehicle.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if (i > 0) {
                                vehicleBean = arrayListveh.get(i - 1);
                                vehicle_id=arrayListveh.get(i-1).getEs_transportid();

                            }
                        }
                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {
                        }
                    });


                    int i = 0;
                    for ( VehicleBean obj : arrayListveh) {
                        if (obj.getTr_vehicle_no().equals(alotDrivrToVicleBean2.getV_no())) {
                            spinevehicle.setSelection(i + 1);
                            break;
                        }
                        i++;
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            } else {
                Toast.makeText(DrivrToVicleActivity.this,error.getMessage(),Toast.LENGTH_SHORT).show();
            }
        }
    };


    private void fetchDriverDetail2update() {

        HashMap<String,String> params=new HashMap<>();
        ApiHandler.apiHit(Request.Method.GET, Constants.getBaseURL() + Constants.DRIVERLIST_URL,apiSpinnrCallBack2updt,this,null);
    }
    ApiHandler.ApiCallback apiSpinnrCallBack2updt=new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            if (error==null) {
                try {
                    driverDetails = DriverListBean.parseDriverListArray(jsonObject.getJSONArray("driver_data"));
                    DrivrDetailSpinnrAdap adap1=new DrivrDetailSpinnrAdap(DrivrToVicleActivity.this,driverDetails);
                    spinerdriver2.setAdapter(adap1);
                    spinerdriver2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if (i > 0) {
                                driverListBean = driverDetails.get(i - 1);
                                driver_id=driverDetails.get(i - 1).getId();
                            }
                        }
                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {
                        }
                    });
                    int i = 0;
                    for ( DriverListBean obj : driverDetails) {
                        if (obj.getDriver_name().equals(alotDrivrToVicleBean2.getD_name())) {
                            spinerdriver2.setSelection(i + 1);
                            break;
                        }
                        i++;
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }


        }
    };


}

