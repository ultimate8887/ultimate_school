package com.ultimate.ultimatesmartschool.Transport.PickUpMod;

import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;

import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.google.android.material.snackbar.Snackbar;
import com.ultimate.ultimatesmartschool.BeanModule.CommonBean;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Transport.TransBeanMod.PicUpBean;
import com.ultimate.ultimatesmartschool.Transport.TransBeanMod.RouteBean;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;
import com.ultimate.ultimatesmartschool.Utility.Utils;


import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PickUpActivity extends AppCompatActivity implements PickUpPointAdap.MethodCallBack {

    @BindView(R.id.recycle)
    RecyclerView recyclerView;
    RecyclerView.LayoutManager layoutManager;
    ArrayList<PicUpBean> arrayList = new ArrayList<>();
    ArrayList<RouteBean> routeList = new ArrayList<>();
    private PickUpPointAdap adap;
    Dialog dialog;
    private String selectedRoute="";
    RouteBean routeBean;
    @BindView(R.id.parentss)
    RelativeLayout parent;
    EditText edtPicUpPoint,edtAmount;
    Spinner spinnerRoute;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.totalRecord)
    TextView totalRecord;
    @BindView(R.id.ggg)
    TextView ggg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_common_lyt);
        ButterKnife.bind(this);
        txtTitle.setText("Pickup point");
        ggg.setText("Tap to edit Pickup Point!");
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        adap = new PickUpPointAdap(arrayList, this,this);
        recyclerView.setAdapter(adap);

    }

    @Override
    protected void onResume() {
        super.onResume();
        fetechList();
    }

    private void fetechList() {
        ErpProgress.showProgressBar(this,"Please wait...");
        HashMap<String,String> params=new HashMap<>();
        ApiHandler.apiHit(Request.Method.GET, Constants.getBaseURL() + Constants.PICKUP_POINT_URL, apiCallBack,this,null);
    }
    ApiHandler.ApiCallback apiCallBack=new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            if (error==null){
             try{
                 if (arrayList != null){
                     arrayList.clear();
                     arrayList=PicUpBean.parsePicUpArray(jsonObject.getJSONArray("pickup_point_data"));
                     adap.setArrayList(arrayList);
                     adap.notifyDataSetChanged();
                     totalRecord.setText("Total Entries:- "+String.valueOf(arrayList.size()));
                 }

             } catch (JSONException e) {
                 e.printStackTrace();
             }
            } else {
                totalRecord.setText("Total Entries:- 0");
                Toast.makeText(PickUpActivity.this,error.getMessage(),Toast.LENGTH_SHORT).show();
            }
        }
    };

    @OnClick(R.id.imgBack)
    public void imgBackssss() {
        finish();
    }

    @Override
    public void addPicUpCallBack(PicUpBean routedata, final int pos) {
        dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.add_edit_pickup_dialog);
        edtPicUpPoint = (EditText) dialog.findViewById(R.id.edtPicUpPoint);
        edtAmount = (EditText) dialog.findViewById(R.id.edtAmount);
        Button btnSubmit = (Button) dialog.findViewById(R.id.btnPickUpSubmit);
        spinnerRoute= (Spinner) dialog.findViewById(R.id.spinerRoute);
        fetchRouteList(pos);
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Utils.hideKeyboard(PickUpActivity.this);
                if (checkValidation()) {
                    if (pos == -1) {
                        String pickUpPoint = edtPicUpPoint.getText().toString();
                        String amount = edtAmount.getText().toString();
                        addNewPicUpPoint(pickUpPoint, amount);
                        dialog.dismiss();
                        Toast.makeText(PickUpActivity.this,"Add Successfully",Toast.LENGTH_SHORT).show();
                    }
                }
        }
        });
           dialog.show();

    }

    private void addNewPicUpPoint(String pickUpPoint, String amount) {
        HashMap<String,String> params=new HashMap<String, String>();
        params.put(Constants.PICKUP_POINT_NAME,pickUpPoint);
        params.put(Constants.AMOUNT,amount);
        if (routeBean!=null){
        params.put("route_id",routeBean.getId());

        ApiHandler.apiHit(Request.Method.POST,Constants.getBaseURL() + Constants.PICKUP_POINT_URL,apiCallBack,PickUpActivity.this,params);
        }
    }

    private void fetchRouteList(int pos) {

    HashMap<String,String> params=new HashMap<>();
    ApiHandler.apiHit(Request.Method.GET, Constants.getBaseURL() + Constants.SCHOOL_ROUTE_URL, new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {

            if (error==null){
                try {
                    routeList = RouteBean.parseTransportArray(jsonObject.getJSONArray("school_route_data"));
                    RouteListSpinrAdap adap1=new RouteListSpinrAdap(PickUpActivity.this,routeList);
                    spinnerRoute.setAdapter(adap1);


                    spinnerRoute.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                            if (i > 0) {
                                routeBean = routeList.get(i - 1);
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                    // setting here spinner item selected with position..

                    if (pos != -1) {
                        int i = 0;
                        for (RouteBean obj : routeList) {
                            if (obj.getId().equals(selectedRoute)) {
                                spinnerRoute.setSelection(i + 1);
                                break;
                            }
                            i++;
                        }
                    }

//                    for(int i=0;i<routeList.size();i++) {
//                        if (routeList.get(i).equals(routeBean.getRoute_title())) {
//                            routeBean=routeList.get(i);
//                            spinnerRoute.setSelection(i);
//                        }
//                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Toast.makeText(PickUpActivity.this,error.getMessage(),Toast.LENGTH_SHORT).show();
            }

        }
    }, this, null);
    }


    private boolean checkValidation() {

        boolean valid = true;
        String errorMsg = null;
        if (edtPicUpPoint.getText().toString().trim().length() <= 0) {
            valid = false;
            errorMsg = "Please enter PickUp Point";
        } else if (edtAmount.getText().toString().trim().length() <= 0) {
            valid = false;
            errorMsg = "Please enter Amount";
        }
        if (!valid) {
            showSnackBar(errorMsg);
        }
        return valid;
    }

    private void showSnackBar(String errorMsg) {

        Snackbar snack = Snackbar.make(parent, errorMsg, Snackbar.LENGTH_LONG);
        View view = snack.getView();
//        TextView tv = (TextView) view.findViewById(R.id.snackbar_text);
//        tv.setTextColor(Color.WHITE);
        snack.show();
    }
    @Override
    public void deletePicUpCallback(final PicUpBean routedata, int pos) {

        DialogInterface.OnClickListener onClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:

                        HashMap<String, String> params = new HashMap<String, String>();
                        String url = Constants.getBaseURL() + Constants.PICKUP_POINT_URL + Constants.PICKUP_POINTS_ID + routedata.getId();
                        ApiHandler.apiHit(Request.Method.DELETE, url, apiCallBack, PickUpActivity.this, params);
                        Toast.makeText(PickUpActivity.this,"Delete Successfully",Toast.LENGTH_SHORT).show();
                        break;
                    case DialogInterface.BUTTON_NEGATIVE:
                        //No button clicked
                        break;
                }
            }
        };
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Are you sure you want to delete?").setPositiveButton("Yes", onClickListener)
                .setNegativeButton("No", onClickListener).show();
    }

    @Override
    public void editPicUpCallback(final PicUpBean routedata, int pos) {

        dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.add_edit_pickup_dialog);
        edtPicUpPoint = (EditText) dialog.findViewById(R.id.edtPicUpPoint);
        edtAmount = (EditText) dialog.findViewById(R.id.edtAmount);
        TextView txtSetup=(TextView) dialog.findViewById(R.id.txtSetup);
        Button btnUpdate = (Button) dialog.findViewById(R.id.btnPickUpSubmit);
        spinnerRoute= (Spinner) dialog.findViewById(R.id.spinerRoute);
        if (pos != -1){
            edtPicUpPoint.setText(arrayList.get(pos).getPickup_name());
            edtAmount.setText(arrayList.get(pos).getAmount());
            btnUpdate.setText("Update");
            txtSetup.setText("Update Pick_Up Point");


//
//                for (int i = 0; i < routeList.size(); i++) {
//                    if (routeList.get(i).toString().equals(routeBean.getRoute_title())) {
//
//                     spinnerRoute.setSelection(i);
//                    }
//                }
//
            selectedRoute= routedata.getRoute_id();

            fetchRouteList(pos);
            spinnerRoute.setSelection(pos);
            btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkValidation()) {
                    String pickUpPoint = null;
                    try {
                        pickUpPoint = URLEncoder.encode(edtPicUpPoint.getText().toString().trim(),"utf-8");
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                    String amount = edtAmount.getText().toString();
                    editNewPicUpPoint(pickUpPoint, amount, routedata);
                    dialog.dismiss();
                    dialog.dismiss();
                    Toast.makeText(PickUpActivity.this,"Update Successfully",Toast.LENGTH_SHORT).show();
                }
              }
          });
        }
      dialog.show();
    }

    private void editNewPicUpPoint(String pickUpPoint, String amount, PicUpBean routedata) {
    HashMap<String,String> params=new HashMap<>();
    String url=Constants.getBaseURL() + Constants.PICKUP_POINT_URL + Constants.PICKUP_POINTS_NAME + pickUpPoint + Constants.AMOUNTS + amount + Constants.ROUTE_ID + routedata.getRoute_id() + Constants.PICKUP_POINT_ID + routedata.getId();

    ApiHandler.apiHit(Request.Method.PUT,url,apiCallBack,PickUpActivity.this,null);

    }
}
