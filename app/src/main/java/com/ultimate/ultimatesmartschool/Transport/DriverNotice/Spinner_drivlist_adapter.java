package com.ultimate.ultimatesmartschool.Transport.DriverNotice;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;


import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Transport.TransBeanMod.DriverListBean;

import java.util.ArrayList;

public class Spinner_drivlist_adapter extends BaseAdapter {
    Context context;
    ArrayList<DriverListBean> stafflist;
    LayoutInflater inflter;

    public Spinner_drivlist_adapter(Context context, ArrayList<DriverListBean> stafflist) {
        this.context=context;
        this.stafflist=stafflist;
        inflter = (LayoutInflater.from(context));
    }


    @Override
    public int getCount() {
        return stafflist.size()+1;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        convertView = inflter.inflate(R.layout.spinner_lyt_msgnt, null);
        TextView label = (TextView) convertView.findViewById(R.id.txtText);
        if (position == 0) {
            // Default selected Spinner item
            label.setText("Choose Driver");

        } else {
            DriverListBean classObj = stafflist.get(position - 1);
            label.setText(classObj.getDriver_name());
        }

        return convertView;

    }
}
