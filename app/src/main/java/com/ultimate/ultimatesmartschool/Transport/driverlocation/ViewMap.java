package com.ultimate.ultimatesmartschool.Transport.driverlocation;

import android.Manifest;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Point;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Handler;
import android.os.SystemClock;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.android.volley.Request;

import com.example.easywaylocation.draw_path.DirectionUtil;
import com.example.easywaylocation.draw_path.PolyLineDataBean;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.ultimate.ultimatesmartschool.BeanModule.User;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Transport.TransBeanMod.AlotDrivrToVicleBean;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.Constants;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.google.android.gms.location.LocationServices.getFusedLocationProviderClient;

public class ViewMap extends AppCompatActivity
        implements OnMapReadyCallback, GoogleApiClient.OnConnectionFailedListener {

    private GoogleMap map;
    private SupportMapFragment mapFragment;
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 1000;
    private Marker myMarker = null;
    private AlotDrivrToVicleBean data;
    double lat, lng;
    MarkerOptions markerOpt;
  //  MarkerOptions markerOptsec;
    LatLng latLng;
    Handler handler = new Handler();
    Geocoder geo;
    List<Address> addresses = null;
    StringBuilder  str = null;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.imgBack)
    ImageView back;
    @BindView(R.id.errortext)TextView errortext;
    ArrayList<LatLng> wayPoints= new ArrayList();

    Location myLocation=null;
    Location destinationLocation=null;
    protected LatLng start=null;
    protected LatLng end=null;

    //to get location permissions.
    private final static int LOCATION_REQUEST_CODE = 23;
    boolean locationPermission=false;

    //polyline object
    private List<Polyline> polylines=null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_map);
        ButterKnife.bind(this);
        txtTitle.setText("Driver Location");
         geo = new Geocoder(getApplicationContext(), Locale.getDefault());
        handler.postDelayed(runLocation, 15000);
        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map_location);
        mapFragment.getMapAsync(this);//remember getMap() is deprecated!
        if (getIntent().getExtras().containsKey("driverv")) {
            Gson gson = new Gson();
            data = gson.fromJson(getIntent().getExtras().getString("driverv"), AlotDrivrToVicleBean.class);
        } else {
            return;
        }

    }
    @OnClick(R.id.imgBack)
    public void onback() {
        finish();
    }
    public Runnable runLocation = new Runnable(){

        @Override
        public void run() {

            fetchCurrentLocation();

            ViewMap.this.handler.postDelayed(ViewMap.this.runLocation, 15000);
        }
    };
    @Override
    public void onMapReady(GoogleMap googleMap) {


//try{
//    boolean isSuccess=googleMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(this,R.raw.my_map_style));
//
//    if(!isSuccess){
//        Log.e("ERROR","MAP STYLE NOT LOADED");
//    }
//}catch (Resources.NotFoundException ex){
//  ex.printStackTrace();
//}

        map = googleMap;
        map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        map.setMaxZoomPreference(18);
        map.setMinZoomPreference(14);


//        markerOptsec = new MarkerOptions()
//                .title(User.getCurrentUser().getSchoolData().getName())
//
//                .icon(BitmapDescriptorFactory.fromResource(R.drawable.chalk_board_icon))
//                .position(new LatLng(30.27877, 74.96394));


        fetchCurrentLocation();
//        setLocationOnMap(lat, lng);
//        LatLng mylocation = new LatLng(lat, lng);
//        map.moveCamera(CameraUpdateFactory.newLatLngZoom(mylocation, 10));
//        animateMarker(mylocation, false);


    }

    private void fetchCurrentLocation() {
        ApiHandler.apiHit(Request.Method.GET, Constants.getBaseURL() + Constants.DRIVER_V_LOC+"?v_id="+data.getV_id(), apiCallBack, this, null);

        Log.e("myurlerp",Constants.getBaseURL()+Constants.DRIVER_V_LOC+"?v_id="+data.getV_id());
    }

    ApiHandler.ApiCallback apiCallBack = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            if (error == null) {
                try {
                    if (jsonObject.has("loc_data")) {
                        JSONObject obj = jsonObject.getJSONObject("loc_data");
                        if (obj.has("lat")) {
                            lat = obj.getDouble("lat");
                        }
                        if (obj.has("lng")) {
                            lng = obj.getDouble("lng");
                        }
                        if (obj.has("logged_on")) {
                            String dt = obj.getString("logged_on");
                            try {
                                DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
                                Date date = (Date) formatter.parse(dt);
                                Log.e("diff",Math.abs(date.getTime() - System.currentTimeMillis()) +"");
                                if (Math.abs(date.getTime() - System.currentTimeMillis()) > 120000) {
                                    //server timestamp is within 5 minutes of current system time
                                } else {
                                    //server is not within 5 minutes of current system time
                                }
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                        }
                        setLocationOnMap(lat, lng);


                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                errortext.setVisibility(View.VISIBLE);
                errortext.setText(error.getMessage());
                //Toast.makeText(ViewMap.this, error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }
    };
//
    private void setLocationOnMap(double latitude, double longitude) {
         latLng = new LatLng(latitude, longitude);
        LatLng school = new LatLng(31.6407118, 74.8857875);




        map.moveCamera(CameraUpdateFactory.newLatLngZoom(
                new LatLng(latitude, longitude), 16));

        String ABCD;

        try {
            addresses = geo.getFromLocation(latitude, longitude, 1);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (addresses.isEmpty()) {


        } else {
            if (addresses.size() > 0) {
                Address returnAddress = addresses.get(0);
                String localityString = returnAddress.getLocality();

                str = new StringBuilder();

                str.append(addresses.get(0).getAddressLine(0) + " " + addresses.get(0).getAddressLine(1) + " ");
               Log.e("my loctinstring",str.toString() );

            }
        }
           if(str.toString()!=null){
               ABCD=str.toString();
           }else{
               ABCD="fetching location!";
           }


         markerOpt = new MarkerOptions()
                 .title(data.getD_name())
                 .snippet(ABCD)
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.school_bus_icon))
                .position(new LatLng(latitude, longitude));
        if (myMarker == null) {
            myMarker = map.addMarker(markerOpt);
            myMarker.showInfoWindow();
        } else {
            myMarker.setPosition(new LatLng(latitude, longitude));
        }
        animateMarker(latLng, false);

//        map.addPolyline((new PolylineOptions()).add(latLng, school).
//                // below line is use to specify the width of poly line.
//                        width(8)
//                // below line is use to add color to our poly line.
//                .color(Color.RED)
//                // below line is to make our poly line geodesic.
//                .geodesic(true));
        // on below line we will be starting the drawing of polyline.


        // map.moveCamera(CameraUpdateFactory.zoomTo(15.0f));
       // map.animateCamera(CameraUpdateFactory.newLatLngZoom(   new LatLng(latitude, longitude), 16));
    }

    public void animateMarker(final LatLng toPosition,final boolean hideMarke) {
        final Handler handler = new Handler();
        final long start = SystemClock.uptimeMillis();
        Projection proj = map.getProjection();
        Point startPoint = proj.toScreenLocation(latLng);
        final LatLng startLatLng = proj.fromScreenLocation(startPoint);
        final long duration = 5000;

        final Interpolator interpolator = new LinearInterpolator();

        handler.post(new Runnable() {
            @Override
            public void run() {
                long elapsed = SystemClock.uptimeMillis() - start;
                float t = interpolator.getInterpolation((float) elapsed
                        / duration);
                double lng = t * toPosition.longitude + (1 - t)
                        * startLatLng.longitude;
                double lat = t * toPosition.latitude + (1 - t)
                        * startLatLng.latitude;
                myMarker.setPosition(new LatLng(lat, lng));

                if (t < 1.0) {
                    // Post again 16ms later.
                    handler.postDelayed(this, 16);
                } else {
                    if (hideMarke) {
                        myMarker.setVisible(false);
                    } else {
                        myMarker.setVisible(true);
                    }
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
       // fetchCurrentLocation();
    }


    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }



}
