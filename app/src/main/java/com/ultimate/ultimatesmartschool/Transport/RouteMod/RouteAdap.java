package com.ultimate.ultimatesmartschool.Transport.RouteMod;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;


import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Transport.TransBeanMod.RouteBean;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by admin on 6/18/2018.
 */

public class RouteAdap extends RecyclerView.Adapter<RouteAdap.ViewHolder> {

       ArrayList<RouteBean> arrayList;
       Context context;
       private CallBackMethod callBackMethod;

    public RouteAdap(ArrayList<RouteBean> arrayList, Context context, CallBackMethod callBackMethod) {

        this.arrayList=arrayList;
        this.context=context;
        this.callBackMethod=callBackMethod;

    }

    public void setArrayList(ArrayList<RouteBean> arrayList) {
        this.arrayList = arrayList;
    }

    public interface CallBackMethod {
        void addRouteCallBack(RouteBean routedata, int pos);
        void deleteRouteCallback(RouteBean routedata, int pos);
        void editRouteCallback(RouteBean routedata, int pos);

    }

    public class ViewHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.lytAddData)
        RelativeLayout lytAddData;
        @BindView(R.id.lytAll)
        RelativeLayout lytAll;
        @BindView(R.id.txtRouteName)
        TextView txtRouteName;
        @BindView(R.id.txtRouteCap)
        TextView txtRouteCap;
        @BindView(R.id.albm_id)
        TextView albm_id;
        @BindView(R.id.trash)
        ImageView delete;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.route_adap_lyt, parent, false);
        ViewHolder viewHolder=new RouteAdap.ViewHolder(v);
        return viewHolder;

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {

        if (arrayList.size() == position) {   //suppose data size is 4 then when position is 4 we have show add UI as it start count from o  then 4th position means  index 5
            holder.lytAll.setVisibility(View.GONE);
            holder.lytAddData.setVisibility(View.VISIBLE);
            holder.lytAddData.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (callBackMethod != null) {
                        callBackMethod.addRouteCallBack(null,-1);
                    }
                }
            });
        } else {
            holder.lytAll.setVisibility(View.VISIBLE);
            holder.lytAddData.setVisibility(View.GONE);

            if (arrayList.get(position).getId() != null) {
                String title = getColoredSpanned("ID: ", "#000000");
                String Name = getColoredSpanned(arrayList.get(position).getId(), "#5A5C59");
                holder.albm_id.setText(Html.fromHtml(title + " " + Name));
            }

            String title = getColoredSpanned("Route title: ", "#F4212C");
            String Name = getColoredSpanned("<b>"+arrayList.get(position).getRoute_title()+"</b>","#000000");
            holder.txtRouteName.setText(Html.fromHtml(title+" "+Name));

            String captitle = getColoredSpanned("Route Capacity: ", "#F4212C");
            String capicity = getColoredSpanned("<b>"+arrayList.get(position).getCapacity()+"</b>","#000000");
            holder.txtRouteCap.setText(Html.fromHtml(captitle+" "+capicity));

            holder.lytAll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (callBackMethod != null) {
                        callBackMethod.editRouteCallback(arrayList.get(position),position);
                    }
                }
            });
            holder.delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (callBackMethod != null) {

                        callBackMethod.deleteRouteCallback(arrayList.get(position),position);
                    }
                }
             });
           }
        }

        private String getColoredSpanned(String text, String color) {
         String input = "<font color=" + color + ">" + text + "</font>";
         return input;
        }

    @Override
    public int getItemCount() {
        return (arrayList.size() + 1);
    }

    }
