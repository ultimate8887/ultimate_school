package com.ultimate.ultimatesmartschool.Transport;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Transport.TransBeanMod.DriverListBean;
import com.ultimate.ultimatesmartschool.Transport.TransBeanMod.RouteBean;

import java.util.ArrayList;

public class RouteSpinnerAdapter extends BaseAdapter {

    private final ArrayList<RouteBean> casteList;
    Context context;
    LayoutInflater inflter;

    public RouteSpinnerAdapter(Context applicationContext, ArrayList<RouteBean> casteList) {
        this.context = applicationContext;
        this.casteList = casteList;
        inflter = (LayoutInflater.from(applicationContext));
    }

    @Override
    public int getCount() {
        return casteList.size() + 1;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        view = inflter.inflate(R.layout.spinner_lyt_group, null);
        TextView label = (TextView) view.findViewById(R.id.txtText);
        if (i == 0) {
            // Default selected Spinner item
            label.setText("Select route");
        } else {
            RouteBean classObj = casteList.get(i - 1);
            label.setText(classObj.getRoute_title());
        }

        return view;
    }
}
