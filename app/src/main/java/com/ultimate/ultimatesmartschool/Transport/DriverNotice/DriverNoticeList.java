package com.ultimate.ultimatesmartschool.Transport.DriverNotice;

import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.ultimate.ultimatesmartschool.BeanModule.User;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;
import com.ultimate.ultimatesmartschool.Utility.Utils;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DriverNoticeList extends Fragment implements Driver_Notice_Adapter.Replycallback
{
    View view;
    @BindView(R.id.sntnotcrecyclerview)
    RecyclerView recyclerView;
    @BindView(R.id.parent)
    RelativeLayout parent;
    RecyclerView.LayoutManager layoutManager;
    ArrayList<DriverNoticeBean> sentnoticelist=new ArrayList<>();
    Driver_Notice_Adapter adapter;
    String id;
    String type="2";
    int loaded=0;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        view = inflater.inflate(R.layout.activity_driver_notice_lists, container, false);
        ButterKnife.bind(this, view);


        id= User.getCurrentUser().getId();
        layoutManager=new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        adapter=new Driver_Notice_Adapter(sentnoticelist, getContext(),this);
        recyclerView.setAdapter(adapter);
        fetchsentnoticelist();

        return view;
    }

    private void fetchsentnoticelist() {
        if (loaded == 0) {
            ErpProgress.showProgressBar(getContext(), "Please wait...");
        }
        loaded++;
        HashMap<String,String> params=new HashMap<>();
        ApiHandler.apiHit(Request.Method.GET, Constants.getBaseURL() + Constants.DRIVERNOTICEURL+Constants.MSGID+id+Constants.MSGTYPE+type,apicallback,getContext(),params);
    }
    ApiHandler.ApiCallback apicallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            if (error == null) {
                try {
                    if (sentnoticelist != null) {
                        sentnoticelist.clear();
                    }
                    JSONArray jsonArray = jsonObject.getJSONArray("msg_data");
                    //sentnoticelist = Sent_Notice_bean.parsesntMessageArray(jsonArray);
                    sentnoticelist = DriverNoticeBean.parseMessageArray(jsonArray);
                    if (sentnoticelist.size() > 0) {
                        adapter.setsntnoticeList(sentnoticelist);
                        adapter.notifyDataSetChanged();
                    } else {
                        adapter.setsntnoticeList(sentnoticelist);
                        adapter.notifyDataSetChanged();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                sentnoticelist.clear();
                adapter.setsntnoticeList(sentnoticelist);
                adapter.notifyDataSetChanged();
                Utils.showSnackBar(error.getMessage(), parent);
            }
        }
    };

    @Override
    public void onMethodCallback(DriverNoticeBean driverNoticeBean) {
        String mimeType = "audio/*";
        Intent intent = new Intent();
        intent.setAction(android.content.Intent.ACTION_VIEW);
        intent.setDataAndType(Uri.parse(driverNoticeBean.getRecordingsfile()), mimeType);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
       startActivity(intent);




    }
}
