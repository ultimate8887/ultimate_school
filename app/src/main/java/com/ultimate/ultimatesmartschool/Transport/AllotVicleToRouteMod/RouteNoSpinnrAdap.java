package com.ultimate.ultimatesmartschool.Transport.AllotVicleToRouteMod;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;


import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Transport.TransBeanMod.RouteBean;

import java.util.ArrayList;

public class RouteNoSpinnrAdap extends BaseAdapter {

    private final ArrayList<RouteBean> vehicleList;
    Context contexts;
    LayoutInflater inflter;


    public RouteNoSpinnrAdap (Context contexts, ArrayList<RouteBean> vehicleList) {
        this.contexts=contexts;
        this.vehicleList=vehicleList;
        inflter= (LayoutInflater.from(contexts));

    }

    @Override
    public int getCount() {
        return vehicleList.size() + 1;
    }
    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }
    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        view=inflter.inflate(R.layout.spinner_prod,null);
        TextView select=(TextView) view.findViewById(R.id.txtText);
        if (i == 0) {
            // Default selected Spinner item
            select.setText("Select Route");

        } else {
            RouteBean castObj=vehicleList.get(i-1);
            select.setText(castObj.getRoute_title() );
        }
        return view;
    }
}
