package com.ultimate.ultimatesmartschool.Transport.VehicleListMod;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.android.volley.Request;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.google.android.material.snackbar.Snackbar;

import com.ultimate.ultimatesmartschool.Activity.UpdateProfileActivity;
import com.ultimate.ultimatesmartschool.ForCamera.IPickResult;
import com.ultimate.ultimatesmartschool.ForCamera.PickImageDialog;
import com.ultimate.ultimatesmartschool.ForCamera.PickResult;
import com.ultimate.ultimatesmartschool.ForCamera.PickSetup;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Transport.DriverListMod.AddDriverActivity;
import com.ultimate.ultimatesmartschool.Transport.TransBeanMod.VehicleBean;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;
import com.ultimate.ultimatesmartschool.Utility.Utils;


import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AddVehicleActivity extends AppCompatActivity implements IPickResult {



    @BindView(R.id.spinerVehicletype)
    Spinner vehicleType;
    @BindView(R.id.edtRegNos)
    EditText edtRegNos;
    public String insuDate = "";
    public String purchDate = "";
    public String reneDate = "";
    String selectVehicleType ="";

    @BindView(R.id.edtCapacity)
    EditText edtCapacity;
    VehicleBean vehicleBean;
    @BindView(R.id.edtInsuranceDte)
    TextView edtInsuranceDte;
    @BindView(R.id.edtPurchaseDte)
    TextView edtPurchaseDte;
    @BindView(R.id.edtRenewalDte)
    TextView edtRenewalDte;
    @BindView(R.id.edtTransName)
    EditText edtTransName;
    @BindView(R.id.root)
    RelativeLayout parent;
    @BindView(R.id.btnVehicleSubmit)
    Button btnVehicleSubmit;

    @BindView(R.id.visitimage)
    CircularImageView visitimage;
    @BindView(R.id.clickimage)
    ImageView clickimage;
    Animation animation;
    private Bitmap userBitmap=null, userBitmapProf=null;

    ArrayList<String> vehicleList = new ArrayList<>();
@BindView(R.id.txtTitle)TextView txtTitle;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_edit_vehicle);
        ButterKnife.bind(this);
//        edtInsuranceDte.setEnabled(false);
//        edtPurchaseDte.setEnabled(false);
//        edtRenewalDte.setEnabled(false);
        fetchTranportType();
        txtTitle.setText("Add Vehicle");
    }

    @OnClick(R.id.imgBack)
    public void imgBackssss() {
        finish();
    }

    private void fetchTranportType() {

        vehicleList.add("Bus");
        vehicleList.add("Car(Manual)");
        vehicleList.add("Car(Auto)");
        vehicleList.add("Minibus");
        vehicleList.add("Van");
        vehicleList.add("Coach");
        vehicleList.add("Other");

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, vehicleList);
        //set the view for the Drop down list
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //set the ArrayAdapter to the spinner
        vehicleType.setAdapter(dataAdapter);
        //attach the listener to the spinner
        vehicleType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long l) {

                selectVehicleType = vehicleList.get(pos);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @OnClick(R.id.clickimage)
    public void edit_profile() {
        animation = AnimationUtils.loadAnimation(this, R.anim.btn_blink_animation);
        clickimage.startAnimation(animation);

        pick_img();
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @SuppressLint("ResourceType")
    private void pick_img() {

            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.S) {
                if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_MEDIA_IMAGES)
                        != PackageManager.PERMISSION_GRANTED) {

                    if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_MEDIA_IMAGES)) {
                        Toast.makeText(this, "Please provide storage permission from app settings", Toast.LENGTH_LONG).show();
                    } else {

                        ActivityCompat.requestPermissions(this,
                                new String[]{Manifest.permission.READ_MEDIA_IMAGES},
                                1);
                    }
                } else {

                    onImageViewClick();
                }

            } else {
                if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {

                    if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                        Toast.makeText(this, "Please provide storage permission from app settings", Toast.LENGTH_LONG).show();
                    } else {

                        ActivityCompat.requestPermissions(this,
                                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                                1);
                    }
                } else {
                    onImageViewClick();

                }
        }
    }

    private void onImageViewClick() {
        PickSetup setup = new PickSetup();
        // super.customize(setup);
        PickImageDialog.build(setup)
                //.setOnClick(this)
                .show(this);
    }

    @Override
    public void onPickResult(PickResult r) {
        if (r.getError() == null) {
            //If you want the Bitmap.
                visitimage.setImageBitmap(r.getBitmap());
                userBitmapProf = r.getBitmap();
                //  encoded_father = Utility.encodeToBase64(profileBitmap_father, Bitmap.CompressFormat.JPEG, 50);
            //r.getPath();
        } else {
            //Handle possible errors
            //TODO: do what you have to do with r.getError();
            Toast.makeText(this, r.getError().getMessage(), Toast.LENGTH_LONG).show();
        }


    }

//
//    @SuppressLint("MissingSuperCall")
//    @Override
//    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//
//        if (resultCode == Activity.RESULT_OK) {
//            switch (requestCode) {
//                case CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE:
//                    Uri imageUri = CropImage.getPickImageResultUri(AddVehicleActivity.this, data);
//                    CropImage.activity(imageUri).setCropShape(CropImageView.CropShape.RECTANGLE).setAspectRatio(1, 1)
//                            .start(AddVehicleActivity.this);
//                    break;
//
//                case CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE:
//                    CropImage.ActivityResult result = CropImage.getActivityResult(data);
//                    Uri resultUri = result.getUri();
//                    userBitmapProf = Utils.getBitmapFromUri(AddVehicleActivity.this, resultUri, 2048);
//                    visitimage.setImageBitmap(userBitmapProf);
////                    submit.setVisibility(View.VISIBLE);
//
//            }
//        }
//
//    }

    @OnClick(R.id.edtInsuranceDte)
    public void imgInsuDate() {
        java.util.Calendar cal = java.util.Calendar.getInstance(TimeZone.getDefault()); // Get current date
        DatePickerDialog datePicker = new DatePickerDialog(this,
                R.style.MyDatePickerDialogTheme, ondateInsu,
                cal.get(java.util.Calendar.YEAR),
                cal.get(java.util.Calendar.MONTH),
                cal.get(java.util.Calendar.DAY_OF_MONTH));
        datePicker.getDatePicker().setMaxDate(new Date().getTime());
        datePicker.setCancelable(false);
        datePicker.show();
    }
    @OnClick(R.id.edtPurchaseDte)
    public void imgPurchDate() {
        java.util.Calendar cal = java.util.Calendar.getInstance(TimeZone.getDefault()); // Get current date
        DatePickerDialog datePicker = new DatePickerDialog(this,
                R.style.MyDatePickerDialogTheme, ondatePurch,
                cal.get(java.util.Calendar.YEAR),
                cal.get(java.util.Calendar.MONTH),
                cal.get(java.util.Calendar.DAY_OF_MONTH));
        datePicker.getDatePicker().setMaxDate(new Date().getTime());
        datePicker.setCancelable(false);
        datePicker.show();
    }
    @OnClick(R.id.edtRenewalDte)
    public void imgReneDate() {
        java.util.Calendar cal = java.util.Calendar.getInstance(TimeZone.getDefault()); // Get current date
        DatePickerDialog datePicker = new DatePickerDialog(this,
                R.style.MyDatePickerDialogTheme, ondateRene,
                cal.get(java.util.Calendar.YEAR),
                cal.get(java.util.Calendar.MONTH),
                cal.get(java.util.Calendar.DAY_OF_MONTH));
        datePicker.getDatePicker().setMinDate(new Date().getTime());
        datePicker.setCancelable(false);
        datePicker.show();
    }
    DatePickerDialog.OnDateSetListener ondateInsu = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

            java.util.Calendar calendar = java.util.Calendar.getInstance(TimeZone.getDefault());
            calendar.set(year, monthOfYear, dayOfMonth);
            Date dbirthdate = calendar.getTime();
            Calendar value_datePicker = calendar;
            SimpleDateFormat fmtOut = new SimpleDateFormat("yyyy-MM-dd");
            String dateString = fmtOut.format(dbirthdate);
            insuDate=dateString;
            edtInsuranceDte.setText(Utils.getDateFormated(dateString));
        }
    };
    DatePickerDialog.OnDateSetListener ondatePurch = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

            java.util.Calendar calendar = java.util.Calendar.getInstance(TimeZone.getDefault());
            calendar.set(year, monthOfYear, dayOfMonth);
            Date dbirthdate = calendar.getTime();
            Calendar value_datePicker = calendar;
            SimpleDateFormat fmtOut = new SimpleDateFormat("yyyy-MM-dd");
            String dateString = fmtOut.format(dbirthdate);
            purchDate=dateString;
            edtPurchaseDte.setText(Utils.getDateFormated(dateString));

        }
    };
    DatePickerDialog.OnDateSetListener ondateRene = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

            java.util.Calendar calendar = java.util.Calendar.getInstance(TimeZone.getDefault());
            calendar.set(year, monthOfYear, dayOfMonth);
            Date dbirthdate = calendar.getTime();
            Calendar value_datePicker = calendar;
            SimpleDateFormat fmtOut = new SimpleDateFormat("yyyy-MM-dd");
            String dateString = fmtOut.format(dbirthdate);
            reneDate=dateString;
            edtRenewalDte.setText(Utils.getDateFormated(dateString));
        }
    };

    @OnClick(R.id.btnVehicleSubmit)
   public void btnSuu() {
        if (checkValidation()) {
            ErpProgress.showProgressBar(this,"Please wait...");
            HashMap<String, String> params = new HashMap<>();
            params.put(Constants.INSU_DATE, insuDate);
            params.put(Constants.RENE_DATE, reneDate);
            params.put("check", "add");
            params.put(Constants.PURCH_DATE, purchDate);
            params.put(Constants.CAPAICITY, edtCapacity.getText().toString());
            params.put(Constants.REGIS_NO, edtRegNos.getText().toString());
            params.put(Constants.TRANS_NAME, edtTransName.getText().toString());
                params.put(Constants.TYPE, selectVehicleType);
            if (userBitmapProf != null) {
                String encoded = Utils.encodeToBase64(userBitmapProf, Bitmap.CompressFormat.JPEG, 90);
                encoded = String.format("data:image/jpeg;base64,%s", encoded);
                params.put("image", encoded);
            }else{
                params.put("image", "");
            }

            ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.VEHICLE_URL, apicallback, this, params);
        }
    }

    private boolean checkValidation() {
        boolean valid = true;
        String errorMsg = null;
        if (edtTransName.getText().toString().trim().length() <= 0) {
            valid = false;
            errorMsg = "Please enter transport name";
        } else if (edtRegNos.getText().toString().trim().length() <= 0) {
            valid = false;
            errorMsg = "Please enter registration no.";
        }
        else if (edtCapacity.getText().toString().trim().length() <= 0) {
            valid = false;
            errorMsg = "Please enter vehicle capacity";
        }
        else if (selectVehicleType == null) {
            valid = false;
            errorMsg = "Please select vehicle type";
        }
         else if (edtPurchaseDte.getText().toString().trim().length() <= 0) {
            valid = false;
            errorMsg = "Please enter purchase date";
        } else if (edtRenewalDte.getText().toString().trim().length() <= 0) {
            valid = false;
            errorMsg = "Please enter renewal date";
        } else if (edtInsuranceDte.getText().toString().trim().length() <= 0) {
            valid = false;
            errorMsg = "Please enter insurance date ";
        }
        if (!valid) {
            showSnackBar(errorMsg);
        }
        return valid;
    }

    private void showSnackBar(String errorMsg) {
        Snackbar snack = Snackbar.make(parent, errorMsg, Snackbar.LENGTH_LONG);
        View view = snack.getView();
//        TextView tv = (TextView) view.findViewById(R.id.snackbar_text);
//        tv.setTextColor(Color.WHITE);
        snack.show();
    }

    ApiHandler.ApiCallback apicallback = new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                ErpProgress.cancelProgressBar();
                if (error == null) {
                    Toast.makeText(AddVehicleActivity.this,"Add Successfully", Toast.LENGTH_SHORT).show();
                        finish();
                } else {

                    Toast.makeText(AddVehicleActivity.this,error.getMessage(),Toast.LENGTH_SHORT).show();
                }
            }
        };

}