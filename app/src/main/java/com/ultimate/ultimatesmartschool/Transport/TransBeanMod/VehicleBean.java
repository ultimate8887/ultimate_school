package com.ultimate.ultimatesmartschool.Transport.TransBeanMod;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by admin on 6/20/2018.
 */

public class VehicleBean {

    private static String ID = "es_transportid";
    private static String TRANSPORT_TYPE = "tr_transport_type";
    private static String PURCHASE_DATE = "tr_purchase_date";
    private static String TRANSPORT_NAME = "tr_transport_name";
    private static String VEHICLE_NO = "tr_vehicle_no";
    private static String INSURANCE_DATE = "tr_insurance_date";
    private static String RENEWAL_DATE = "tr_ins_renewal_date";
    private static String SEATING_CAPACITY = "tr_seating_capacity";
    private static String STATUS = "status";
    private static String IMG = "image";


    /**
     * es_transportid : 7
     * tr_transport_type :
     * tr_purchase_date : 0000-00-00
     * tr_transport_name : fdgbhjg
     * tr_vehicle_no : 345356
     * tr_insurance_date : 0000-00-00
     * tr_ins_renewal_date : 0000-00-00
     * tr_seating_capacity : 235426
     * status : Active
     */

    private String es_transportid;
    private String tr_transport_type;
    private String tr_purchase_date;
    private String tr_transport_name;
    private String tr_vehicle_no;
    private String tr_insurance_date;
    private String tr_ins_renewal_date;
    private String tr_seating_capacity;
    private String status;

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    private String image;

    public String getEs_transportid() {
        return es_transportid;
    }

    public void setEs_transportid(String es_transportid) {
        this.es_transportid = es_transportid;
    }

    public String getTr_transport_type() {
        return tr_transport_type;
    }

    public void setTr_transport_type(String tr_transport_type) {
        this.tr_transport_type = tr_transport_type;
    }

    public String getTr_purchase_date() {
        return tr_purchase_date;
    }

    public void setTr_purchase_date(String tr_purchase_date) {
        this.tr_purchase_date = tr_purchase_date;
    }

    public String getTr_transport_name() {
        return tr_transport_name;
    }

    public void setTr_transport_name(String tr_transport_name) {
        this.tr_transport_name = tr_transport_name;
    }

    public String getTr_vehicle_no() {
        return tr_vehicle_no;
    }

    public void setTr_vehicle_no(String tr_vehicle_no) {
        this.tr_vehicle_no = tr_vehicle_no;
    }

    public String getTr_insurance_date() {
        return tr_insurance_date;
    }

    public void setTr_insurance_date(String tr_insurance_date) {
        this.tr_insurance_date = tr_insurance_date;
    }

    public String getTr_ins_renewal_date() {
        return tr_ins_renewal_date;
    }

    public void setTr_ins_renewal_date(String tr_ins_renewal_date) {
        this.tr_ins_renewal_date = tr_ins_renewal_date;
    }

    public String getTr_seating_capacity() {
        return tr_seating_capacity;
    }

    public void setTr_seating_capacity(String tr_seating_capacity) {
        this.tr_seating_capacity = tr_seating_capacity;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public static ArrayList<VehicleBean> parseVehicleArray(JSONArray arrayObj) {
        ArrayList list = new ArrayList<VehicleBean>();
        try {
            for (int i = 0; i < arrayObj.length(); i++) {
                VehicleBean p = parseVehicleBeanObject(arrayObj.getJSONObject(i));
                if (p != null) {
                    list.add(p);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    private static VehicleBean parseVehicleBeanObject(JSONObject jsonObject) {
        VehicleBean casteObj= new VehicleBean();
        try {
            if (jsonObject.has(ID)) {
                casteObj.setEs_transportid(jsonObject.getString(ID));
            }
            if (jsonObject.has(TRANSPORT_TYPE) && !jsonObject.getString(TRANSPORT_TYPE).isEmpty() && !jsonObject.getString(TRANSPORT_TYPE).equalsIgnoreCase("null")) {
                casteObj.setTr_transport_type(jsonObject.getString(TRANSPORT_TYPE));
            }
            if (jsonObject.has(PURCHASE_DATE) && !jsonObject.getString(PURCHASE_DATE).isEmpty() && !jsonObject.getString(PURCHASE_DATE).equalsIgnoreCase("null")) {
                casteObj.setTr_purchase_date(jsonObject.getString(PURCHASE_DATE));
            }
            if (jsonObject.has(TRANSPORT_NAME) && !jsonObject.getString(TRANSPORT_NAME).isEmpty() && !jsonObject.getString(TRANSPORT_NAME).equalsIgnoreCase("null")) {
                casteObj.setTr_transport_name(jsonObject.getString(TRANSPORT_NAME));
            }
            if (jsonObject.has(VEHICLE_NO) && !jsonObject.getString(VEHICLE_NO).isEmpty() && !jsonObject.getString(VEHICLE_NO).equalsIgnoreCase("null")) {
                casteObj.setTr_vehicle_no(jsonObject.getString(VEHICLE_NO));
            }
            if (jsonObject.has(INSURANCE_DATE) && !jsonObject.getString(INSURANCE_DATE).isEmpty() && !jsonObject.getString(INSURANCE_DATE).equalsIgnoreCase("null")) {
                casteObj.setTr_insurance_date(jsonObject.getString(INSURANCE_DATE));
            }
            if (jsonObject.has(RENEWAL_DATE) && !jsonObject.getString(RENEWAL_DATE).isEmpty() && !jsonObject.getString(RENEWAL_DATE).equalsIgnoreCase("null")) {
                casteObj.setTr_ins_renewal_date(jsonObject.getString(RENEWAL_DATE));
            }
            if (jsonObject.has(SEATING_CAPACITY) && !jsonObject.getString(SEATING_CAPACITY).isEmpty() && !jsonObject.getString(SEATING_CAPACITY).equalsIgnoreCase("null")) {
                casteObj.setTr_seating_capacity(jsonObject.getString(SEATING_CAPACITY));
            }
            if (jsonObject.has(STATUS) && !jsonObject.getString(STATUS).isEmpty() && !jsonObject.getString(STATUS).equalsIgnoreCase("null")) {
                casteObj.setStatus(jsonObject.getString(STATUS));
            }
            if (jsonObject.has(IMG) && !jsonObject.getString(IMG).isEmpty() && !jsonObject.getString(IMG).equalsIgnoreCase("null")) {
                casteObj.setImage(jsonObject.getString(IMG));
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }
        return casteObj;
    }


}
