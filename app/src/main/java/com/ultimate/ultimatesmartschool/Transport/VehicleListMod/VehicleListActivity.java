package com.ultimate.ultimatesmartschool.Transport.VehicleListMod;

import android.content.DialogInterface;
import android.content.Intent;

import android.os.Bundle;

import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;

import com.google.gson.Gson;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Transport.TransBeanMod.VehicleBean;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class VehicleListActivity extends AppCompatActivity implements VehicleAdap.MethodCallBack {

    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.recycle)
    RecyclerView recycleVehicle;
    RecyclerView.LayoutManager layoutManager;
    private VehicleAdap adap;
    ArrayList<VehicleBean> arrayList=new ArrayList<>();
    @BindView(R.id.totalRecord)
    TextView totalRecord;
    @BindView(R.id.ggg)
    TextView ggg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_common_lyt);
        ButterKnife.bind(this);
        txtTitle.setText("Vehicle List");
        ggg.setText("Tap to edit Vehicle details!");
        layoutManager=new LinearLayoutManager(this);
        recycleVehicle.setLayoutManager(layoutManager);
        adap=new VehicleAdap(arrayList,this,this);
        recycleVehicle.setAdapter(adap);

    }

    @Override
    protected void onResume() {
        super.onResume();
        fetchVicledetails();
    }

    @OnClick(R.id.imgBack)
    public void imgBackssss() {
        finish();
    }

    private void fetchVicledetails() {
        ErpProgress.showProgressBar(this,"Please wait...");
        HashMap<String,String> params=new HashMap<>();
        ApiHandler.apiHit(Request.Method.GET, Constants.getBaseURL() + Constants.VEHICLE_URL, apiCallBack, this,null);
    }

    ApiHandler.ApiCallback apiCallBack=new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            if (error==null){
                try{
                    if (arrayList != null){
                        arrayList.clear();
                        arrayList=VehicleBean.parseVehicleArray(jsonObject.getJSONArray("vehical_data"));
                        adap.setArrayList(arrayList);
                        adap.notifyDataSetChanged();
                        totalRecord.setText("Total Entries:- "+String.valueOf(arrayList.size()));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            } else {
                totalRecord.setText("Total Entries:- 0");
                Toast.makeText(VehicleListActivity.this,error.getMessage(),Toast.LENGTH_SHORT).show();
            }
        }
    };

    @Override
    public void addVehicleCallBack(VehicleBean vehicledata, int pos) {

        //Toast.makeText(VehicleListActivity.this,"add",Toast.LENGTH_SHORT).show();
        Intent intent=new Intent(VehicleListActivity.this,AddVehicleActivity.class);
        startActivity(intent);
    }

    @Override
    public void deleteVehicleCallback(final VehicleBean vehicledata, int pos) {
        DialogInterface.OnClickListener onClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:

                        HashMap<String, String> params = new HashMap<String, String>();
                        String url = Constants.getBaseURL() + Constants.VEHICLE_URL + Constants.TRID + vehicledata.getEs_transportid();
                        ApiHandler.apiHit(Request.Method.DELETE, url, apiCallBack,VehicleListActivity.this, params);
                        Toast.makeText(VehicleListActivity.this,"Delete Successfully",Toast.LENGTH_SHORT).show();
                        break;
                    case DialogInterface.BUTTON_NEGATIVE:
                        //No button clicked
                        break;
                }
            }
        };
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Are you sure you want to delete?").setPositiveButton("Yes", onClickListener)
                .setNegativeButton("No", onClickListener).show();
    }

    @Override
    public void editVehicleCallback(VehicleBean vehicledata, int pos) {

        Gson gson = new Gson();
        String vehicle_data = gson.toJson(vehicledata, VehicleBean.class);
        Intent intent = new Intent(VehicleListActivity.this, UpdateVehicleActivity.class);
        intent.putExtra("vehicle_data", vehicle_data);
        startActivity(intent);
    }
}
