package com.ultimate.ultimatesmartschool.Transport;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetSequence;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.squareup.picasso.Picasso;
import com.ultimate.ultimatesmartschool.BeanModule.User;
import com.ultimate.ultimatesmartschool.Homework.GETHomeworkActivity;
import com.ultimate.ultimatesmartschool.Login.LoginActivity;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.CommonProgress;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class FuelExpenseList extends AppCompatActivity  implements FuelExpenseAdapter.StudentPro {
    ArrayList<Fuelbean> datalist = new ArrayList<>();
    private LinearLayoutManager layoutManager;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @BindView(R.id.parent)
    RelativeLayout parent;
    @BindView(R.id.imgBack)
    ImageView imgBack;
    @BindView(R.id.textNorecord)
    TextView txtNoData;
    @BindView(R.id.txtTitle)
    TextView txtTitle;

    SharedPreferences sharedPreferences;

    private RequestQueue queue;
    private String url;
    private FuelExpenseAdapter adapter1;
    int loaded = 0;
    Dialog mBottomSheetDialog;
    @BindView(R.id.totalRecord)
    TextView totalRecord;

    @BindView(R.id.today_date)
    TextView cal_text;

    String from_date = "";

    @BindView(R.id.export)
    FloatingActionButton export;
    CommonProgress commonProgress;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fuel_expense_list);
        ButterKnife.bind(this);
        txtTitle.setText("Fuel Expense List");
        //txtSub.setText(Utility.setNaviHeaderClassData());
        // adepter set here
        commonProgress = new CommonProgress(this);
        layoutManager = new LinearLayoutManager(FuelExpenseList.this);
        recyclerView.setLayoutManager(layoutManager);
        adapter1 = new FuelExpenseAdapter(datalist, FuelExpenseList.this, this,0);
        recyclerView.setAdapter(adapter1);
        fetchNoticeBoard();

    }

    @OnClick(R.id.cal_lyttttt)
    public void cal_lyttttt() {

        java.util.Calendar cal = java.util.Calendar.getInstance(TimeZone.getDefault()); // Get current date
        DatePickerDialog datePicker = new DatePickerDialog(this,
                R.style.MyDatePickerDialogTheme, ondate,
                cal.get(java.util.Calendar.YEAR),
                cal.get(java.util.Calendar.MONTH),
                cal.get(java.util.Calendar.DAY_OF_MONTH));
        datePicker.getDatePicker();
        datePicker.setCancelable(false);
        datePicker.show();

    }

    DatePickerDialog.OnDateSetListener ondate = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            java.util.Calendar calendar = java.util.Calendar.getInstance(TimeZone.getDefault());
            calendar.set(year, monthOfYear, dayOfMonth);
            calendar.set(Calendar.HOUR_OF_DAY, 00);
            calendar.set(Calendar.MINUTE, 00);
            calendar.set(Calendar.SECOND, 00);
            Date setdate = calendar.getTime();
            SimpleDateFormat fmtOut = new SimpleDateFormat("dd MMM, yyyy");
            String dateString = fmtOut.format(setdate);
            cal_text.setText(dateString);
            SimpleDateFormat dateFrmOut = new SimpleDateFormat("yyyy-MM-dd");
            from_date = dateFrmOut.format(setdate);
            // Toast.makeText(HomeWorkByDate.this,from_date,Toast.LENGTH_SHORT).show();
            fetchNoticeBoard();
        }
    };


    private void fetchNoticeBoard() {

        commonProgress.show();

        HashMap<String, String> params = new HashMap<>();
        params.put("date", from_date);
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.GETEXPENSE_URL, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                ErpProgress.cancelProgressBar();
                commonProgress.dismiss();
                if (error == null) {

                    try {
                        JSONArray noticeArray = jsonObject.getJSONArray("expense_data");
                        datalist = Fuelbean.parseNoticetArray(noticeArray);

                        if (datalist.size() > 0) {
                            adapter1.setHQList(datalist);
                            adapter1.notifyDataSetChanged();
                            txtNoData.setVisibility(View.GONE);
                            export.setVisibility(View.VISIBLE);
                            totalRecord.setVisibility(View.VISIBLE);
                            totalRecord.setText("Total Entries:- " + String.valueOf(datalist.size()));
                            if (!restorePrefDataP()){
                                setShowcaseViewP();
                            }
                        } else {
                            export.setVisibility(View.GONE);
                            totalRecord.setText("Total Entries:- 0");
                            txtNoData.setVisibility(View.VISIBLE);
                            adapter1.setHQList(datalist);
                            adapter1.notifyDataSetChanged();
                        }
                        //   main_progress.setVisibility(View.GONE);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    export.setVisibility(View.GONE);
                    totalRecord.setText("Total Entries:- 0");
                    txtNoData.setVisibility(View.VISIBLE);
                    datalist.clear();
                    adapter1.setHQList(datalist);
                    adapter1.notifyDataSetChanged();
                }


            }
        }, this, params);

    }

    private void setShowcaseViewP() {
        new TapTargetSequence(this)
                .targets(TapTarget.forView(export,"Export Excel!","Tap the Export button to export Fuel Expense List.")
                        .outerCircleColor(R.color.light)
                        .outerCircleAlpha(0.96f)
                        .targetCircleColor(R.color.white)
                        .titleTextSize(20)
                        .titleTextColor(R.color.white)
                        .descriptionTextSize(14)
                        .descriptionTextColor(R.color.black)
                        .textColor(R.color.black)
                        .textTypeface(Typeface.SANS_SERIF)
                        .dimColor(R.color.black)
                        .drawShadow(true)
                        .cancelable(false)
                        .tintTarget(true)
                        .transparentTarget(true)
                        .targetRadius(40)).listener(new TapTargetSequence.Listener() {
                    @RequiresApi(api = Build.VERSION_CODES.N)
                    @Override
                    public void onSequenceFinish() {
                        // Toast.makeText(getApplicationContext(),"Sequence Finished",Toast.LENGTH_SHORT).show();
                        savePrefDataP();

                    }

                    @Override
                    public void onSequenceStep(TapTarget lastTarget, boolean targetClicked) {
                        // Toast.makeText(getActivity(),"GREAT!",Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onSequenceCanceled(TapTarget lastTarget) {

                    }
                }).start();
    }

    private void savePrefDataP(){
        sharedPreferences=this.getSharedPreferences("boarding_pref_view_export",MODE_PRIVATE);
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putBoolean("IsFirstTimeVisit_view_export",true);
        editor.apply();
    }

    private boolean restorePrefDataP(){
        sharedPreferences = this.getSharedPreferences("boarding_pref_view_export",MODE_PRIVATE);
        return sharedPreferences.getBoolean("IsFirstTimeVisit_view_export",false);
    }

    @SuppressLint("RestrictedApi")
    @OnClick(R.id.export)
    public void submit() {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.S) {
            importData();
        } else {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {

                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    Toast.makeText(this, "Please provide storage permission from app settings", Toast.LENGTH_LONG).show();
                } else {

                    ActivityCompat.requestPermissions(this,
                            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            1);
                }
            } else {
                importData();

            }
        }
    }

    @SuppressLint("RestrictedApi")
    private void importData() {
        // File filePath = new File(Environment.getExternalStorageDirectory() + "/Demo.xls");
        Workbook wb = new HSSFWorkbook();
        Cell cell = null;
        String ndate = "", sub_staff = "", sub_date = "";
        Sheet sheet = null;
        sheet = wb.createSheet(sub_staff + "Fuel Expense List");
        //Now column and row
        Row row = sheet.createRow(0);

        cell = row.createCell(0);
        cell.setCellValue("S No");

        cell = row.createCell(1);
        cell.setCellValue("Receipt No");

        cell = row.createCell(2);
        cell.setCellValue("Route");

        cell = row.createCell(3);
        cell.setCellValue("Staff Id");

        cell = row.createCell(4);
        cell.setCellValue("Staff Name");

        cell = row.createCell(5);
        cell.setCellValue("Fuel Price");

        cell = row.createCell(6);
        cell.setCellValue("Amount");

        cell = row.createCell(7);
        cell.setCellValue("Morning Reading");

        cell = row.createCell(8);
        cell.setCellValue("Evening Reading");


        //column width
        sheet.setColumnWidth(0, (20 * 100));
        sheet.setColumnWidth(1, (20 * 150));
        sheet.setColumnWidth(2, (30 * 200));
        sheet.setColumnWidth(3, (20 * 150));
        sheet.setColumnWidth(4, (30 * 200));
        sheet.setColumnWidth(5, (20 * 200));
        sheet.setColumnWidth(6, (20 * 200));
        sheet.setColumnWidth(7, (30 * 200));
        sheet.setColumnWidth(8, (30 * 200));


        for (int i = 0; i < datalist.size(); i++) {

            Row row1 = sheet.createRow(i + 1);

            cell = row1.createCell(0);
            cell.setCellValue(i + 1);

            cell = row1.createCell(1);
            cell.setCellValue(datalist.get(i).getId());

            cell = row1.createCell(2);
            cell.setCellValue(datalist.get(i).getRoute_name()+"("+datalist.get(i).getRoute_no()+")");

            cell = row1.createCell(3);
            cell.setCellValue((datalist.get(i).getD_id()));
            //  cell.setCellStyle(cellStyle);

            cell = row1.createCell(4);
            cell.setCellValue(datalist.get(i).getD_name());

            cell = row1.createCell(5);
            cell.setCellValue(datalist.get(i).getFuel_price());

            cell = row1.createCell(6);
            cell.setCellValue(datalist.get(i).getAmount());

            cell = row1.createCell(7);
            cell.setCellValue(datalist.get(i).getMorn_reading());

            cell = row1.createCell(8);
            cell.setCellValue(datalist.get(i).getEvng_reading());

            sheet.setColumnWidth(0, (20 * 100));
            sheet.setColumnWidth(1, (20 * 150));
            sheet.setColumnWidth(2, (30 * 200));
            sheet.setColumnWidth(3, (20 * 150));
            sheet.setColumnWidth(4, (30 * 200));
            sheet.setColumnWidth(5, (20 * 200));
            sheet.setColumnWidth(6, (20 * 200));
            sheet.setColumnWidth(7, (30 * 200));
            sheet.setColumnWidth(8, (30 * 200));

        }
        String fileName;

        fileName = sub_staff + "Fuel_Expense_List" + System.currentTimeMillis() + ".xls";

        // File file = new File(getActivity().getExternalFilesDir(null), fileName);

        File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS), fileName);

        FileOutputStream fileOutputStream = null;

        try {
            fileOutputStream = new FileOutputStream(file);
            wb.write(fileOutputStream);
            Utils.openSuccessDialog("Success!", "Export file in " + file, "", FuelExpenseList.this);
        } catch (
                IOException e) {
            Utils.openErrorDialog("Error writing Exception: " + e, FuelExpenseList.this);
        } catch (Exception e) {
            Utils.openErrorDialog("Failed to save file due to Exception: " + e, FuelExpenseList.this);
        } finally {
            try {
                if (null != fileOutputStream) {
                    fileOutputStream.close();
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

    }

    @SuppressLint("RestrictedApi")
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == 1 && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            importData();
        } else {
            Toast.makeText(getApplicationContext(), "Permission Denied", Toast.LENGTH_SHORT).show();
        }
    }



    @OnClick(R.id.imgBack)
    public void imgBack() {
        // Animatoo.animateZoom(ViewNoticeInfo.this);
        finish();
    }

    @Override
    public void StudentProfile(Fuelbean std) {
        commonCode(std.getMorn_reading_img());
    }

    private void commonCode(String img) {
        Dialog mBottomSheetDialog = new Dialog(this);
        final View sheetView = getLayoutInflater().inflate(R.layout.image_lyt, null);
        mBottomSheetDialog.setContentView(sheetView);
        ImageView imgShow = (ImageView) mBottomSheetDialog.findViewById(R.id.imgShow);
        ImageView imgDownload = (ImageView) mBottomSheetDialog.findViewById(R.id.imgDownload);
        imgDownload.setVisibility(View.GONE);
        Picasso.get().load(img).placeholder(getResources().getDrawable(R.color.transparent)).into(imgShow);
        mBottomSheetDialog.show();
        Window window = mBottomSheetDialog.getWindow();
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    }

    @Override
    public void onUpdateCallback(Fuelbean std) {
        commonCode(std.getEvng_reading_img());
    }

    @Override
    public void onDelecallback(Fuelbean homeworkbean) {
        commonProgress.show();
        HashMap<String, String> params = new HashMap<>();
        params.put("check", "fuel");
        params.put("h_id", homeworkbean.getId());
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.DELETEHW, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                ErpProgress.cancelProgressBar();
                commonProgress.dismiss();
                if (error == null) {
                    int pos = datalist.indexOf(homeworkbean);
                    datalist.remove(pos);
                    adapter1.setHQList(datalist);
                    adapter1.notifyDataSetChanged();
                    Utils.showSnackBar("Deleted Successfully!", parent);
                    totalRecord.setText("Total Entries:- "+String.valueOf(datalist.size()));
                } else {
                    Log.e("error", error.getMessage() + "");
                    Utils.showSnackBar(error.getMessage(), parent);
                    if (error.getStatusCode() == 405) {
                        User.logout();
                        startActivity(new Intent(FuelExpenseList.this, LoginActivity.class));
                        finish();
                    }
                }
            }
        }, this, params);
    }
}