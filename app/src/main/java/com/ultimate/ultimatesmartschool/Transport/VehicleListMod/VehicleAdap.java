package com.ultimate.ultimatesmartschool.Transport.VehicleListMod;

import android.content.Context;

import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;


import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.github.siyamed.shapeimageview.CircularImageView;
import com.squareup.picasso.Picasso;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Transport.TransBeanMod.VehicleBean;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by admin on 6/18/2018.
 */

public class VehicleAdap extends RecyclerView.Adapter<VehicleAdap.ViewHolder> {

    private MethodCallBack callBackMethod;
    ArrayList<VehicleBean> arrayList;
    Context context;

    public VehicleAdap(ArrayList<VehicleBean> arrayList, Context context,MethodCallBack callBackMethod) {
        this.arrayList=arrayList;
        this.context=context;
        this.callBackMethod=callBackMethod;

    }

    public void setArrayList(ArrayList<VehicleBean> arrayList) {
        this.arrayList = arrayList;
    }

    public interface MethodCallBack {

        void addVehicleCallBack(VehicleBean vehicledata, int pos);
        void deleteVehicleCallback(VehicleBean vehicledata, int pos);
        void editVehicleCallback(VehicleBean vehicledata, int pos);

    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.lytAddData)
        RelativeLayout lytAddData;
        @BindView(R.id.lytAll)
        RelativeLayout lytAll;
        @BindView(R.id.fetchNo)
        TextView fetchNo;
        @BindView(R.id.fetchType)
        TextView fetchType;
        @BindView(R.id.fetchCapacity)
        TextView fetchCapacity;
        @BindView(R.id.fetchInsuranceDte)
        TextView fetchInsuranceDte;
        @BindView(R.id.fetchRenewal)
        TextView fetchRenewal;
        @BindView(R.id.fetchpursdate)
        TextView fetchpursdate;
        @BindView(R.id.trash)
        ImageView delete;
        @BindView(R.id.RouteTitle)
        TextView RouteTitle;
        @BindView(R.id.albm_id)
        TextView albm_id;
        @BindView(R.id.visitimage)
        CircularImageView visitimage;
        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);

        }
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.vehicle_adap_lyt, parent, false);
        ViewHolder viewHolder=new VehicleAdap.ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {

        if (arrayList.size() == position) {   //suppose data size is 4 then when position is 4 we have show add UI as it start count from o  then 4th position means  index 5
            holder.lytAll.setVisibility(View.GONE);
            holder.lytAddData.setVisibility(View.VISIBLE);
            holder.lytAddData.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (callBackMethod != null) {
                        callBackMethod.addVehicleCallBack(null,-1);
                    }
                }
            });
        } else {


            if (arrayList.get(position).getEs_transportid() != null) {
                String title = getColoredSpanned("ID: ", "#000000");
                String Name = getColoredSpanned(arrayList.get(position).getEs_transportid(), "#5A5C59");
                holder.albm_id.setText(Html.fromHtml(title + " " + Name));
            }

            if (arrayList.get(position).getTr_transport_name() != null) {
                String title = getColoredSpanned("Vehicle Name: ", "#F4212C");
                String Name = getColoredSpanned("<b>"+arrayList.get(position).getTr_transport_name()+"</b>", "#000000");
                holder.RouteTitle.setText(Html.fromHtml(title + " " + Name));
            }

            holder.lytAll.setVisibility(View.VISIBLE);
            holder.lytAddData.setVisibility(View.GONE);
            holder.fetchNo.setText(arrayList.get(position).getTr_vehicle_no());
            holder.fetchType.setText(arrayList.get(position).getTr_transport_type());
            holder.fetchCapacity.setText(arrayList.get(position).getTr_seating_capacity());
            holder.fetchInsuranceDte.setText(Utils.getDateFormated(arrayList.get(position).getTr_insurance_date()));
            holder.fetchRenewal.setText(Utils.getDateFormated(arrayList.get(position).getTr_ins_renewal_date()));
            holder.fetchpursdate.setText(Utils.getDateFormated(arrayList.get(position).getTr_purchase_date()));

            String image_url="https://ultimatesolutiongroup.com/office_admin/images/dirverdoc/"+arrayList.get(position).getImage();
            Log.e("image_url",image_url+arrayList.get(position).getImage());
            if (arrayList.get(position).getImage()!=null){
                //   Picasso.with(UpdateVehicleActivity.this).load(image_url).placeholder(R.drawable.boy).into(visitimage);
                Utils.progressImg(image_url,holder.visitimage,context);
            }else {

                Picasso.get().load(R.drawable.bus_school_s).into(holder.visitimage);
            }

            holder.lytAll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (callBackMethod != null) {
                        callBackMethod.editVehicleCallback(arrayList.get(position),position);
                    }
                }
            });
            holder.delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (callBackMethod != null) {

                        callBackMethod.deleteVehicleCallback(arrayList.get(position),position);
                    }
                }
            });
        }
    }
    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }
    @Override
    public int getItemCount() {
        return (arrayList.size() + 1);
    }


}
