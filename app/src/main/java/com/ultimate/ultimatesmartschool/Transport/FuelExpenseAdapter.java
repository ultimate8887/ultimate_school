package com.ultimate.ultimatesmartschool.Transport;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.daimajia.swipe.SimpleSwipeListener;
import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.squareup.picasso.Picasso;
import com.ultimate.ultimatesmartschool.R;

import java.util.ArrayList;

public class FuelExpenseAdapter extends RecyclerSwipeAdapter<FuelExpenseAdapter.Viewholder>{
    public ArrayList<Fuelbean> stulist;
    Context mContext;
    StudentPro mStudentPro;
    int value;

    public FuelExpenseAdapter(ArrayList<Fuelbean> stulist, Context mContext, StudentPro mStudentPro,int value) {
        this.mContext = mContext;
        this.mStudentPro=mStudentPro;
        this.stulist = stulist;
        this.value = value;
    }

    @Override
    public Viewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_news_fuel, parent, false);
        Viewholder viewholder = new Viewholder(view);
        return viewholder;
    }

    @Override
    public void onBindViewHolder(final Viewholder viewHolder, @SuppressLint("RecyclerView") final int position) {


//        viewHolder.circleimg.setAnimation(AnimationUtils.loadAnimation(mContext,R.anim.fade_transition_animation));
//        viewHolder.parent.setAnimation(AnimationUtils.loadAnimation(mContext,R.anim.fade_scale_animation));
        Fuelbean mData=stulist.get(position);

        if (stulist.get(position).getImage() != null) {
            Picasso.get().load(stulist.get(position).getImage()).placeholder(R.drawable.receptionistsss).into(viewHolder.circleimg);
        } else {
            Picasso.get().load(R.drawable.receptionistsss).into(viewHolder.circleimg);
        }
        if (mData.getRoute_no() != null) {
            String title = getColoredSpanned("Route: ", "#000000");
            String Name = getColoredSpanned(mData.getRoute_name()+"("+mData.getRoute_no()+")", "#5A5C59");
            viewHolder.txtid1.setText(Html.fromHtml(title + " " + Name));
        }

        if (mData.getId() != null) {
            String title = getColoredSpanned("ID: ", "#000000");
            String Name = getColoredSpanned(mData.getD_id(), "#5A5C59");
            viewHolder.txtid.setText(Html.fromHtml(title + " " + Name));
        }

        // holder.employee_name.setText("Name: " + " " + viewstafflist.get(position).getName());
        if (mData.getD_name() != null) {
            String title = getColoredSpanned("Name: ", "#000000");
            String Name = getColoredSpanned(mData.getD_name(), "#5A5C59");
            viewHolder.textViewData.setText(Html.fromHtml(title + " " + Name));
        }else {
            viewHolder.textViewData.setText("Name: Not Mentioned");
        }

        //  holder.post.setText("Post:" + " " + viewstafflist.get(position).getPost());
        if (mData.getFuel_price() != null) {
            String title = getColoredSpanned("Fuel Price: ", "#000000");
            String Name = getColoredSpanned(mData.getFuel_price(), "#5A5C59");
            viewHolder.txtFather.setText(Html.fromHtml(title + " " + Name));
        }else {
            viewHolder.txtFather.setText("Not Mentioned");
        }

        // holder.employee_doj.setText("Date of Joining: " + " " + viewstafflist.get(position).getDateofjoining());
        if (mData.getAmount() != null) {
            String title = getColoredSpanned("Amount: ", "#000000");
            String Name = getColoredSpanned(mData.getAmount(), "#5A5C59");
            viewHolder.txtClass.setText(Html.fromHtml(title + " " + Name));
        } else {
            viewHolder.txtClass.setText("Not Mentioned");
        }

        if (mData.getMorn_reading() != null) {
            String title = getColoredSpanned("Morning Reading: ", "#000000");
            String Name = getColoredSpanned(mData.getMorn_reading(), "#5A5C59");
            viewHolder.txtMobile.setText(Html.fromHtml(title + " " + Name));
        }else {
            viewHolder.txtMobile.setText("Not Mentioned");
        }

        if (mData.getEvng_reading() != null) {
            String title = getColoredSpanned("Evening Reading: ", "#000000");
            String Name = getColoredSpanned(mData.getEvng_reading(), "#5A5C59");
            viewHolder.txtMobile1.setText(Html.fromHtml(title + " " + Name));
        }else {
            viewHolder.txtMobile1.setText("Not Mentioned");
        }
        Animation animation = AnimationUtils.loadAnimation(mContext, R.anim.btn_blink_animation);

        viewHolder.mor_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                viewHolder.mor_img.startAnimation(animation);
                mStudentPro.StudentProfile(stulist.get(position));
            }
        });

        viewHolder.evn_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                viewHolder.evn_img.startAnimation(animation);
                mStudentPro.onUpdateCallback(stulist.get(position));
            }
        });


        viewHolder.swipeLayout.setShowMode(SwipeLayout.ShowMode.PullOut);
        viewHolder.swipeLayout.addSwipeListener(new SimpleSwipeListener() {
            @Override
            public void onOpen(SwipeLayout layout) {

            }

            @Override
            public void onStartClose(SwipeLayout layout) {
                viewHolder.swipeLayout.close();
            }
        });


        viewHolder.trash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder1 = new AlertDialog.Builder(mContext);
                builder1.setMessage("Do you want to delete? ");
                builder1.setCancelable(false);
                builder1.setPositiveButton(
                        "Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                                if (mStudentPro != null) {
                                    mStudentPro.onDelecallback(stulist.get(position));
//                                    Toast.makeText(listner, "deleted", Toast.LENGTH_SHORT).show();
                                }
                                // mAdapterCallback.deleteNotification(notificationList.get(postion));
                            }
                        });
                builder1.setNegativeButton(
                        "No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        });
                AlertDialog alert11 = builder1.create();
                alert11.show();
            }
        });

    }

    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }

    public interface StudentPro{
        public void StudentProfile(Fuelbean std);
        public void onUpdateCallback(Fuelbean std);

        public void onDelecallback(Fuelbean std);

    }
    @Override
    public int getItemCount() {
        if (stulist != null)
            return stulist.size();
        return 0;
    }



    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe;
    }

    public void setHQList(ArrayList<Fuelbean> stulist) {
        this.stulist = stulist;
    }

    public class Viewholder extends RecyclerView.ViewHolder {
        //        SwipeLayout swipeLayout;
        TextView textViewData, txtMobile,evn_img,mor_img,txtMobile1, txtFather, txtClass,txtid,txtid1,status;
        ImageView trash,imgOfDialog,update;
        // CardView update;
        ImageView arrow;
        RelativeLayout parent,root;
        CircularImageView circleimg;
        SwipeLayout swipeLayout;

        public Viewholder(View itemView) {
            super(itemView);
            root =(RelativeLayout)itemView.findViewById(R.id.root);
            parent =(RelativeLayout)itemView.findViewById(R.id.parent);
            circleimg=(CircularImageView)itemView.findViewById(R.id.circleimg);
            swipeLayout = (SwipeLayout) itemView.findViewById(R.id.swipe);
            textViewData = (TextView) itemView.findViewById(R.id.name);
            txtFather = (TextView) itemView.findViewById(R.id.fathername);
            txtMobile = (TextView) itemView.findViewById(R.id.mobileno);
            txtMobile1 = (TextView) itemView.findViewById(R.id.mobileno1);
            mor_img = (TextView) itemView.findViewById(R.id.mor_img);
            evn_img = (TextView) itemView.findViewById(R.id.evn_img);
            txtClass = (TextView) itemView.findViewById(R.id.classess);
            status = (TextView) itemView.findViewById(R.id.status);
            update = (ImageView) itemView.findViewById(R.id.update);
            trash = (ImageView) itemView.findViewById(R.id.trash);
            imgOfDialog = (ImageView) itemView.findViewById(R.id.imgOfDialog);
            txtid=(TextView)itemView.findViewById(R.id.textViewid);
            txtid1=(TextView)itemView.findViewById(R.id.textViewid1);
            arrow = (ImageView) itemView.findViewById(R.id.arrow);
            arrow.setVisibility(View.GONE);
        }
    }


}
