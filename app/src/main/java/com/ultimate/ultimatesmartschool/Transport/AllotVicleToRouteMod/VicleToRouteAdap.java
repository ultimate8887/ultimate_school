package com.ultimate.ultimatesmartschool.Transport.AllotVicleToRouteMod;

import android.content.Context;

import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;


import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Transport.TransBeanMod.AlotVicleToRouteBean;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by admin on 6/22/2018.
 */

public class VicleToRouteAdap extends RecyclerView.Adapter<VicleToRouteAdap.Viewholder> {

    ArrayList<AlotVicleToRouteBean> arrayList;
    Context context;
    private MethodCallback methodCallback;


    public VicleToRouteAdap( Context context, ArrayList<AlotVicleToRouteBean> arrayList,MethodCallback methodCallback) {
        this.arrayList=arrayList;
        this.context=context;
        this.methodCallback=methodCallback;

    }

    public void setArrayList(ArrayList<AlotVicleToRouteBean> arrayList) {
        this.arrayList = arrayList;
    }

    public interface MethodCallback {

        void alotVicleRouteCallBack(AlotVicleToRouteBean vicleToRouteBean,int pos);
        void addVicleRouteCallBack(AlotVicleToRouteBean vicleToRouteBean, int pos);
    }

    public class Viewholder extends RecyclerView.ViewHolder {

        @BindView(R.id.fetchFirst)
        TextView route;
        @BindView(R.id.fetchSecond)
        TextView vehicleNo;
        @BindView(R.id.fetchThid)
        TextView vicleType;
        @BindView(R.id.first)
        TextView first;
        @BindView(R.id.second)
        TextView second;
        @BindView(R.id.third)
        TextView third;
        @BindView(R.id.lytAddData)
        RelativeLayout lytAddData;
        @BindView(R.id.lytAll)
        RelativeLayout lytAll;
        @BindView(R.id.r1)
        RelativeLayout delete;
        @BindView(R.id.albm_id)
        TextView albm_id;
        public Viewholder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }

    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.common_adap_lyt, parent, false);
        Viewholder viewholder= new VicleToRouteAdap.Viewholder(v);
         return viewholder;
    }

    @Override
    public void onBindViewHolder(@NonNull Viewholder holder, final int position) {


        if (arrayList.size() == position) {   //suppose data size is 4 then when position is 4 we have show add UI as it start count from o  then 4th position means  index 5
            holder.lytAll.setVisibility(View.GONE);
            holder.lytAddData.setVisibility(View.VISIBLE);
            holder.lytAddData.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (methodCallback != null) {
                        methodCallback.addVicleRouteCallBack(null, -1);
                    }
                }
            });
        } else {


            if (arrayList.get(position).getId() != null) {
                String title = getColoredSpanned("ID: ", "#000000");
                String Name = getColoredSpanned(arrayList.get(position).getId(), "#5A5C59");
                holder.albm_id.setText(Html.fromHtml(title + " " + Name));
            }

            AlotVicleToRouteBean data = arrayList.get(position);
            holder.first.setText("Route: ");
            holder.second.setText("Vehicle No: ");
            holder.third.setText("Vehicle Type: ");
            holder.delete.setVisibility(View.GONE);
            holder.lytAll.setVisibility(View.VISIBLE);
            holder.lytAddData.setVisibility(View.GONE);
            if (data.getRoute_title() != null) {
                holder.route.setText(data.getRoute_title()+"("+data.getBoard_id()+")");
            } else {
                holder.route.setText("-");
            }

            if (data.getV_id() != null) {
                holder.vehicleNo.setText(data.getV_no());
            } else {
                holder.vehicleNo.setText("-");

            }
            if (data.getV_type() != null) {
                holder.vicleType.setText(data.getV_type());
            } else {
                holder.vicleType.setText("-");
            }

            holder.lytAll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    methodCallback.alotVicleRouteCallBack(arrayList.get(position), position);
                }
            });
        }
    }

    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }


    @Override
    public int getItemCount() {
        return arrayList.size()+1;
    }


}
