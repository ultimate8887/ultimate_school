package com.ultimate.ultimatesmartschool.DietChart;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.android.volley.Request;
import com.squareup.picasso.Picasso;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Wedfrag extends Fragment {
    @BindView(R.id.texttipss)
    TextView addtips;
    @BindView(R.id.updatemeal)
    ImageView updatemeal;
    @BindView(R.id.tipss)TextView tips;
    @BindView(R.id.parent)
    RelativeLayout parent;
    @BindView(R.id.recipiname)TextView recipetiitle;
    @BindView(R.id.imageView6)ImageView recipepic;
    @BindView(R.id.txtfullprocedure)TextView description;
    @BindView(R.id.designlay)RelativeLayout dishpiclayput;
    @BindView(R.id.tiplay)RelativeLayout tipslayout;
    @BindView(R.id.textNorecord)
    TextView txtNorecord;
    int loaded=0;

    String day_id="2";
    String abcd;
    EditText edretips;
    ArrayList<DailytipsBean> tipsList = new ArrayList<>();
    ArrayList<DietchartBean> dietchrtList = new ArrayList<>();
    @BindView(R.id.daytext)TextView daytext;
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_mon,container,false);
        ButterKnife.bind(this,view);
        daytext.setText("Wednesday");
        fetchdietchart();
        updatemeal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), UploadmealplanActivity.class);
                intent.putExtra("day_id", day_id);
                startActivity(intent);
                // startActivity(new Intent(getActivity(),UploadmealplanActivity.class));
            }
        });


        addtips.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog addtipsDialog = new Dialog(getContext());
                addtipsDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                addtipsDialog.setCancelable(true);
                addtipsDialog.setContentView(R.layout.addtips_dialog);
                edretips=(EditText)addtipsDialog.findViewById(R.id.edretips);
                // final String result = edretips.getText().toString();
                Button button=(Button)addtipsDialog.findViewById(R.id.savetips);
                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (checkValid()) {
                            HashMap<String, String> params = new HashMap<>();
                            String orihinal=edretips.getText().toString();
                            params.put("tips", String.valueOf(Html.fromHtml(orihinal)));
                            params.put("day_id", day_id);
                            ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.UPDATEDAILYPOST, new ApiHandler.ApiCallback() {
                                @Override
                                public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                                    if (error == null) {
                                        try {

                                            Toast.makeText(getActivity(), jsonObject.getString("msg"), Toast.LENGTH_SHORT).show();
                                            tips.setText(edretips.getText().toString());
//                    getActivity().finish();
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    } else {
                                        Utils.showSnackBar(error.getMessage(), parent);
                                    }

                                }
                            },getContext(),params);
                        }
                        addtipsDialog.dismiss();
                    }
                });
                addtipsDialog.show();
            }
        });

        return view;
    }


    private boolean checkValid() {
        boolean valid = true;
        String errorMsg = null;
        if (edretips.getText().toString().length() <= 0) {
            valid = false;
            errorMsg = "Please enter your tips";
        }
        if (!valid) {
            Utils.showSnackBar(errorMsg, parent);
        }
        return valid;
    }

    private void fetchdailytips() {
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("day_id", day_id);
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.DAILYTIPSURL, apiCallback,getContext(), params);
    }

    ApiHandler.ApiCallback apiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            //txtupload.setVisibility(View.VISIBLE);
            if (error == null) {
                try {
                    if (tipsList != null) {
                        tipsList.clear();
                    }
                    JSONArray jsonArray = jsonObject.getJSONArray("dailytips_data");
                    tipsList = DailytipsBean.parseDTArray(jsonArray);
                    abcd=tipsList.get(0).getTips();
                    tips.setText(abcd);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {

            }
        }
    };
    @Override
    public void onResume() {
        super.onResume();
        fetchdailytips();
        fetchdietchart();
    }

    private void fetchdietchart() {
//        if (loaded == 0) {
//            ErpProgress.showProgressBar(getContext(), "Please wait...");
//        }
//        loaded++;
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("day_id", day_id);
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.DIETCHARTURL, dietchartapiCallback,getContext(), params);

    }


    ApiHandler.ApiCallback dietchartapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            //txtupload.setVisibility(View.VISIBLE);
            if (error == null) {
                try {
                    if (dietchrtList != null) {
                        dietchrtList.clear();
                    }
                    JSONArray jsonArray = jsonObject.getJSONArray("dietchart_data");
                    dietchrtList = DietchartBean.parseDCArray(jsonArray);
                    if (dietchrtList.size() > 0) {
                        recipetiitle.setText(dietchrtList.get(0).getTittle());
                        description.setText(dietchrtList.get(0).getDescription());
                        if (dietchrtList.get(0).getImage() != null) {
                            Picasso.get().load(dietchrtList.get(0).getImage()).into(recipepic);
                        }else{
                            Picasso.get().load(String.valueOf(getActivity().getResources())).into(recipepic);

                        }
                        txtNorecord.setVisibility(View.GONE);


                    } else {
                        dishpiclayput.setVisibility(View.GONE);
                        tipslayout.setVisibility(View.GONE);
                        txtNorecord.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Toast.makeText(getActivity(),error.getMessage(),Toast.LENGTH_SHORT).show();
                dishpiclayput.setVisibility(View.GONE);
                tipslayout.setVisibility(View.GONE);
                txtNorecord.setVisibility(View.VISIBLE);
                dietchrtList.clear();
            }
        }
    };
}
