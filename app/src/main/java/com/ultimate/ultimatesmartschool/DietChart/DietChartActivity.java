package com.ultimate.ultimatesmartschool.DietChart;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.tabs.TabLayout;
import com.ultimate.ultimatesmartschool.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DietChartActivity extends AppCompatActivity {
    @BindView(R.id.pager)
    ViewPager viewpager;
    @BindView(R.id.tab_layout)
    TabLayout tablayout;
    @BindView(R.id.imgBack)
    ImageView back;

    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_diet_chart);
        ButterKnife.bind(this);
        txtTitle.setText("Diet Chart");
        tablayout.setupWithViewPager(viewpager);
        final PagerAdapter adapter = new Pager
                (getSupportFragmentManager());
        viewpager.setAdapter(adapter);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
