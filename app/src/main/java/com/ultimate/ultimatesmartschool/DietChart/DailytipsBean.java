package com.ultimate.ultimatesmartschool.DietChart;

import com.ultimate.ultimatesmartschool.Utility.DebugLog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class DailytipsBean {


    private static String ID="id";
    private static String TIPS="tips";

    /**
     * id : 4
     * tips : From the foods you offer, kids get to choose what they will eat or whether to eat at all
     * class_id :
     * day_id : 1
     */

    private String id;
    private String tips;
    private String class_id;
    private String day_id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTips() {
        return tips;
    }

    public void setTips(String tips) {
        this.tips = tips;
    }

    public String getClass_id() {
        return class_id;
    }

    public void setClass_id(String class_id) {
        this.class_id = class_id;
    }

    public String getDay_id() {
        return day_id;
    }

    public void setDay_id(String day_id) {
        this.day_id = day_id;
    }

    public static ArrayList<DailytipsBean> parseDTArray(JSONArray arrayObj) {
        ArrayList<DailytipsBean> list = new ArrayList<DailytipsBean>();
        try {

            for (int i = 0; i < arrayObj.length(); i++) {
                DailytipsBean p = parseDTObject(arrayObj.getJSONObject(i));
                if (p != null) {
                    list.add(p);
                }
            }
        } catch (Exception e) {
            DebugLog.printLog("exp",e.getMessage());
            e.printStackTrace();
        }
        return list;
    }

    private static DailytipsBean parseDTObject(JSONObject jsonObject) {
        DailytipsBean casteObj = new DailytipsBean();
        try {
            if (jsonObject.has(ID)) {
                casteObj.setId(jsonObject.getString(ID));
            }

            if (jsonObject.has(TIPS) && !jsonObject.getString(TIPS).isEmpty() && !jsonObject.getString(TIPS).equalsIgnoreCase("null")) {
                casteObj.setTips(jsonObject.getString(TIPS));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return casteObj;

    }
}
