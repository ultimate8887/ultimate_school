package com.ultimate.ultimatesmartschool.DietChart;

import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.DebugLog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class DietchartBean {

    private static String ID="id";
    private static String DESCRIPTION="description";
    private static String TITTLE="tittle";
    private static String IMAGE="image";
    private static String DAY_ID="day_id";
    /**
     * id : 1
     * tittle : noodle
     * description : hellothis is only testingpurpose
     * image : office_admin/images/
     * day_id : 0
     */

    private String id;
    private String tittle;
    private String description;
    private String image;
    private String day_id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTittle() {
        return tittle;
    }

    public void setTittle(String tittle) {
        this.tittle = tittle;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDay_id() {
        return day_id;
    }

    public void setDay_id(String day_id) {
        this.day_id = day_id;
    }

    public static ArrayList<DietchartBean> parseDCArray(JSONArray arrayObj) {
        ArrayList<DietchartBean> list = new ArrayList<DietchartBean>();
        try {

            for (int i = 0; i < arrayObj.length(); i++) {
                DietchartBean p = parseDCObject(arrayObj.getJSONObject(i));
                if (p != null) {
                    list.add(p);
                }
            }
        } catch (Exception e) {
            DebugLog.printLog("exp",e.getMessage());
            e.printStackTrace();
        }
        return list;
    }

    private static DietchartBean parseDCObject(JSONObject jsonObject) {

        DietchartBean casteObj = new DietchartBean();
        try {
            if (jsonObject.has(ID)) {
                casteObj.setId(jsonObject.getString(ID));
            }

            if (jsonObject.has(DESCRIPTION) && !jsonObject.getString(DESCRIPTION).isEmpty() && !jsonObject.getString(DESCRIPTION).equalsIgnoreCase("null")) {
                casteObj.setDescription(jsonObject.getString(DESCRIPTION));
            }
            if (jsonObject.has(TITTLE) && !jsonObject.getString(TITTLE).isEmpty() && !jsonObject.getString(TITTLE).equalsIgnoreCase("null")) {
                casteObj.setTittle(jsonObject.getString(TITTLE));
            }
            if (jsonObject.has(IMAGE) && !jsonObject.getString(IMAGE).isEmpty() && !jsonObject.getString(IMAGE).equalsIgnoreCase("null")) {
                casteObj.setImage(Constants.getImageBaseURL()+jsonObject.getString(IMAGE));
            }
            if (jsonObject.has(DAY_ID) && !jsonObject.getString(DAY_ID).isEmpty() && !jsonObject.getString(DAY_ID).equalsIgnoreCase("null")) {
                casteObj.setDay_id(jsonObject.getString(DAY_ID));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return casteObj;


    }

}
