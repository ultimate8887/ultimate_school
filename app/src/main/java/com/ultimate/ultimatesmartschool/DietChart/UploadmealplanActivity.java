package com.ultimate.ultimatesmartschool.DietChart;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.ultimate.ultimatesmartschool.BeanModule.User;
import com.ultimate.ultimatesmartschool.ForCamera.IPickResult;
import com.ultimate.ultimatesmartschool.ForCamera.PickImageDialog;
import com.ultimate.ultimatesmartschool.ForCamera.PickResult;
import com.ultimate.ultimatesmartschool.ForCamera.PickSetup;
import com.ultimate.ultimatesmartschool.Login.LoginActivity;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class UploadmealplanActivity extends AppCompatActivity implements IPickResult {
    @BindView(R.id.imgBack)
    ImageView back;
    @BindView(R.id.updatedietchrt)
    Button updatedietchrt;
    @BindView(R.id.edtAddmessagestaff)
    EditText edittittle;
    @BindView(R.id.edttitlerecpie)
    EditText editdescription;
    @BindView(R.id.parent)
    RelativeLayout parent;
    private int type;
    private Bitmap dcbitmap;
    private String day_id;
    @BindView(R.id.imageView6)
    ImageView imageView6;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_uploadmealplan);
        ButterKnife.bind(this);
        txtTitle.setText("Diet Chart");
        Bundle bunddle = getIntent().getExtras();
        day_id = bunddle.getString("day_id");
        Log.e("dayidkyahai", day_id);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        imageView6.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {

                type = 1;
                pick_img();

            }
        });
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    @SuppressLint("ResourceType")
    private void pick_img() {

        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.S) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_MEDIA_IMAGES)
                    != PackageManager.PERMISSION_GRANTED) {

                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_MEDIA_IMAGES)) {
                    Toast.makeText(this, "Please provide storage permission from app settings", Toast.LENGTH_LONG).show();
                } else {

                    ActivityCompat.requestPermissions(this,
                            new String[]{Manifest.permission.READ_MEDIA_IMAGES},
                            1);
                }
            } else {

                onImageViewClick();
            }

        } else {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {

                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    Toast.makeText(this, "Please provide storage permission from app settings", Toast.LENGTH_LONG).show();
                } else {

                    ActivityCompat.requestPermissions(this,
                            new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                            1);
                }
            } else {
                onImageViewClick();

            }
        }
    }

    private void onImageViewClick() {
        //  type = 1;
        PickSetup setup = new PickSetup();
        // super.customize(setup);

        PickImageDialog.build(setup)
                //.setOnClick(this)
                .show(this);
    }

    @Override
    public void onPickResult(PickResult r) {
        if (r.getError() == null) {
            //If you want the Bitmap.
            imageView6.setImageBitmap(r.getBitmap());
            dcbitmap = r.getBitmap();
            imageView6.setVisibility(View.VISIBLE);

        } else {
            //Handle possible errors
            //TODO: do what you have to do with r.getError();
            Toast.makeText(this, r.getError().getMessage(), Toast.LENGTH_LONG).show();
        }


    }


    public static boolean checkPermission(Context mContext, String Permission) {
        int result = ContextCompat.checkSelfPermission(mContext, Permission);
        if (result == PackageManager.PERMISSION_GRANTED) {

            return true;
        } else {
            return false;
        }
    }


    @OnClick(R.id.updatedietchrt)
    public void updatedietchrt() {

        if (checkValid()) {
            ErpProgress.showProgressBar(this, "Please wait...");
            HashMap<String, String> params = new HashMap<String, String>();
            if (dcbitmap != null) {
                String encoded = Utils.encodeToBase64(dcbitmap, Bitmap.CompressFormat.JPEG, 50);
                encoded = String.format("data:image/jpeg;base64,%s", encoded);
                params.put("image", encoded);
            }
            params.put("tittle", edittittle.getText().toString());
            params.put("description", editdescription.getText().toString());
            params.put("day_id", day_id);
            ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.UPDTDIETCHARTURL, apiCallback, this, params);

        }

    }

    ApiHandler.ApiCallback apiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            if (error == null) {
                try {
                    Utils.showSnackBar(jsonObject.getString("msg"), parent);
                    finish();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("error", error.getMessage() + "");
                Utils.showSnackBar(error.getMessage(), parent);

                if (error.getStatusCode() == 405) {
                    User.logout();
                    startActivity(new Intent(UploadmealplanActivity.this, LoginActivity.class));
                    finish();
                }
            }
        }
    };

    private boolean checkValid() {
        boolean valid = true;
        String errorMsg = null;
        if (dcbitmap == null) {
            valid = false;
            errorMsg = "Please Put your Diet image";
        } else if (edittittle.getText().toString().trim().length() <= 0) {
            valid = false;
            errorMsg = "Please enter tittle";
        } else if (editdescription.getText().toString().trim().length() <= 0) {
            valid = false;
            errorMsg = "Please enter description";
        }
        if (!valid) {
            Utils.showSnackBar(errorMsg, parent);
        }
        return valid;
    }

}
