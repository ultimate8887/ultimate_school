package com.ultimate.ultimatesmartschool.Message;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.text.ClipboardManager;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.ultimate.ultimatesmartschool.BeanModule.User;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.CommonProgress;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

public class InboxActivity extends AppCompatActivity implements Message_AdapterNew.Mycallback {
    @BindView(R.id.recyclerViewmsg)
    RecyclerView recyclerView;
    @BindView(R.id.imgBack)
    ImageView back;
    RecyclerView.LayoutManager layoutManager;
    ArrayList<Message_Bean> messagelist=new ArrayList<>();
    private Message_AdapterNew adapter;
    @BindView(R.id.parent)
    RelativeLayout parent;
    String id;
    int loaded=0;
    @BindView(R.id.txtTitle)
    TextView txtTitle;

    ImageView imageView6;
    EditText subject;
    EditText message;
    TextView sendername,txtdate;
    String type="1",name="",date="";
    @BindView(R.id.textNorecord)
    TextView txtNorecord;
    Animation animation;
    @BindView(R.id.spinnerGroups)
    Spinner spinnerMonth;
    ArrayList<String> monthList = new ArrayList<>();
    @BindView(R.id.totalRecord)
    TextView totalRecord;
    CommonProgress commonProgress;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inbox);
        ButterKnife.bind(this);
        txtTitle.setText("Inbox");
        commonProgress=new CommonProgress(this);
        if (getIntent().getExtras()!=null){
            name = getIntent().getExtras().getString("Today");
        }
        animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.btn_blink_animation);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                back.startAnimation(animation);
                finish();
            }
        });
        id= User.getCurrentUser().getId();
        layoutManager=new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        adapter=new Message_AdapterNew(messagelist,InboxActivity.this,this);
        recyclerView.setAdapter(adapter);

    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onResume() {
        super.onResume();
       // fetchmessagelist();
        fetnewmsg();
        fetchmonthList();
        getTodayDate();
    }

    public void getTodayDate() {
        java.util.Calendar c = java.util.Calendar.getInstance();
        c.set(java.util.Calendar.HOUR_OF_DAY, 00);
        c.set(java.util.Calendar.MINUTE, 00);
        c.set(java.util.Calendar.SECOND, 00);
        Date setdate = c.getTime();
        java.text.SimpleDateFormat fmtOut = new java.text.SimpleDateFormat("dd MMM, yyyy");
        String dateString = fmtOut.format(setdate);
        java.text.SimpleDateFormat dateFrmOut = new java.text.SimpleDateFormat("yyyy-MM-dd");
        date = dateFrmOut.format(setdate);
    }


    @RequiresApi(api = Build.VERSION_CODES.N)
    private void fetchmonthList() {

        monthList.add("All");
        monthList.add("Today");
        monthList.add("History");

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, monthList);
        //set the view for the Drop down list
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //set the ArrayAdapter to the spinner
        spinnerMonth.setAdapter(dataAdapter);

        // setting here spinner item selected with position..
//        for(int i=0;i<monthList.size();i++) {
//            if (monthList.get(i).equalsIgnoreCase(name)) {
//                name = monthList.get(i);
//                spinnerMonth.setSelection(i);
//            }
//        }

        //attach the listener to the spinner
        spinnerMonth.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long l) {

                if ( monthList.get(pos).equalsIgnoreCase("All")) {
                    name = "";
                }else {
                    name = monthList.get(pos);
                }
                fetchmessagelist(name);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }



    private void fetnewmsg() {
        HashMap<String, String> params = new HashMap<>();
        params.put("user_id",User.getCurrentUser().getId());
        params.put("check","");
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.NEW_MSG, apicallback5, this, params);
    }

    ApiHandler.ApiCallback apicallback5 = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();

            if (error == null) {

                try {
                    Log.e("USERDATA", jsonObject.getJSONObject("doc_data").toString());

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            } else {

                //  Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_LONG).show();
                //Utils.showSnackBar(error.getMessage(), parent);
            }
        }
    };


    public void fetchmessagelist(String name){
        if (loaded == 0) {
            commonProgress.show();
        }
        loaded++;
        HashMap<String,String> params=new HashMap<>();
      //  ApiHandler.apiHit(Request.Method.GET, Constants.getBaseURL() + Constants.MESSAGE+Constants.MSGID+id+Constants.MSGTYPE+type,apicallback,this,params);
        ApiHandler.apiHit(Request.Method.GET, Constants.getBaseURL() + Constants.MESSAGE+Constants.MSGID+id+Constants.MSGTYPE+type
                +Constants.NAME1+name+Constants.DATE+date,apicallback,this,params);
    }
    ApiHandler.ApiCallback apicallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            commonProgress.dismiss();
            if (error == null) {
                try {
                    if (messagelist != null) {
                        messagelist.clear();
                    }
                    JSONArray jsonArray = jsonObject.getJSONArray("msg_data");
                    messagelist = Message_Bean.parseMessageArray(jsonArray);
                    if (messagelist.size() > 0) {
                        adapter.setmessageList(messagelist);
                        adapter.notifyDataSetChanged();
                        txtNorecord.setVisibility(View.GONE);
                        totalRecord.setText("Total Entries:- "+String.valueOf(messagelist.size()));
                    } else {
                        totalRecord.setText("Total Entries:- 0");
                        adapter.setmessageList(messagelist);
                        adapter.notifyDataSetChanged();
                        txtNorecord.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                totalRecord.setText("Total Entries:- 0");
                messagelist.clear();
                txtNorecord.setVisibility(View.VISIBLE);
                adapter.setmessageList(messagelist);
                adapter.notifyDataSetChanged();
                Utils.showSnackBar(error.getMessage(), parent);
            }
        }
    };

    @Override
    public void onMethod_Image_callback(Message_Bean message_bean) {
        Dialog mBottomSheetDialog = new Dialog(this);
        final View sheetView = getLayoutInflater().inflate(R.layout.view_msgimage_lyt, null);
        mBottomSheetDialog.setContentView(sheetView);
        mBottomSheetDialog.setCancelable(true);

        Animation animation;

        animation = AnimationUtils.loadAnimation(this, R.anim.btn_blink_animation);
        imageView6 = (ImageView) sheetView.findViewById(R.id.imageView6);
        subject = (EditText) sheetView.findViewById(R.id.txtsubject);
        message = (EditText) sheetView.findViewById(R.id.message_body);
        sendername = (TextView) sheetView.findViewById(R.id.txtsendername);
        txtdate = (TextView) sheetView.findViewById(R.id.txtdate);
        setData(message_bean);

        ImageView copy = (ImageView) sheetView.findViewById(R.id.copy);
        copy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                copy.startAnimation(animation);
                ClipboardManager cm = (ClipboardManager)getApplicationContext().getSystemService(Context.CLIPBOARD_SERVICE);
                cm.setText(message.getText().toString());
                Toast.makeText(getApplicationContext(), "Text copied to clipboard", Toast.LENGTH_SHORT).show();
            }
        });

        imageView6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                downloadfile(message_bean);
                //  Toast.makeText(GDSTest.this, "Downloaded successfully", Toast.LENGTH_LONG).show();
            }
        });
        RelativeLayout btnNo= (RelativeLayout) sheetView.findViewById(R.id.btnNo);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnNo.startAnimation(animation);
                mBottomSheetDialog.dismiss();
            }
        });

        mBottomSheetDialog.show();
    }

    @Override
    public void onMethod_Image_callback_new(Message_Bean message_bean) {

    }

    @Override
    public void onMethod_recording_callback(Message_Bean message_bean) {

    }

    @Override
    public void onMethod_pdf_call_back(Message_Bean message_bean) {

    }

    private void setData(Message_Bean sentmsglist) {
        subject.setEnabled(false);
        message.setEnabled(false);
        subject.setText(sentmsglist.getSubject());
        txtdate.setText(Utils.getTimeHr(sentmsglist.getCreated_on()));
        if (User.getCurrentUser().getSchoolData().getFi_school_id().equalsIgnoreCase("MSS2024")
                || User.getCurrentUser().getSchoolData().getFi_school_id().equalsIgnoreCase("NMSNEW")){
            message.setText(Html.fromHtml(sentmsglist.getMessage()));
        }else{
            message.setText(sentmsglist.getMessage());
        }

        if (sentmsglist.getFrom_name() != null) {
            String title = getColoredSpanned("From: ", "#e31e25");
            String Name = getColoredSpanned(sentmsglist.getFrom_name(), "#7D7D7D");
            String l_Name = getColoredSpanned("["+ sentmsglist.getFrom_type()+"]", "#5A5C59");
            sendername.setText(Html.fromHtml(title + " " + Name + " " + l_Name));
        }
        if (sentmsglist.getImage() != null) {
            imageView6.setVisibility(View.VISIBLE);
            Utils.progressImg_two(sentmsglist.getImage(),imageView6,InboxActivity.this,"");
           // Picasso.with(this).load(sentmsglist.getImage()).placeholder(this.getResources().getDrawable(R.drawable.logo)).into(imageView6);
        }  else {
            imageView6.setVisibility(View.GONE);
        }
    }

    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }

    private void downloadfile(Message_Bean message_bean) {

        Dialog mBottomSheetDialog = new Dialog(this);
        final View sheetView = getLayoutInflater().inflate(R.layout.image_lyt, null);
        mBottomSheetDialog.setContentView(sheetView);
        mBottomSheetDialog.setCancelable(true);

        Animation animation;
        animation = AnimationUtils.loadAnimation(this, R.anim.btn_blink_animation);
        ImageView imgShow = (ImageView) sheetView.findViewById(R.id.imgShow);


        ImageView imgDownload = (ImageView) sheetView.findViewById(R.id.imgDownload);
        imgDownload.setVisibility(View.GONE);
        Utils.progressImg_two(message_bean.getImage(),imgShow,InboxActivity.this,"");
        //Picasso.with(this).load(message_bean.getImage()).into(imgShow);
        imgDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imgDownload.startAnimation(animation);
               // ErpProgress.showProgressBar(InboxActivity.this, "downloading...");
                //Picasso.with(InboxActivity.this).load(message_bean.getImage()).into(imgTraget);
                //  Toast.makeText(GDSTest.this, "Downloaded successfully", Toast.LENGTH_LONG).show();
            }
        });


        mBottomSheetDialog.show();
        Window window = mBottomSheetDialog.getWindow();
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

    }


}
