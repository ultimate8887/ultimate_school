package com.ultimate.ultimatesmartschool.Message;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.text.ClipboardManager;
import android.text.Html;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.google.android.material.tabs.TabLayout;
import com.squareup.picasso.Picasso;
import com.ultimate.ultimatesmartschool.BeanModule.ClassBean;
import com.ultimate.ultimatesmartschool.BeanModule.User;
import com.ultimate.ultimatesmartschool.MSG_NEW.ClassAdapter_New;
import com.ultimate.ultimatesmartschool.MSG_NEW.FilterTabPager;
import com.ultimate.ultimatesmartschool.MSG_NEW.SendMSG;
import com.ultimate.ultimatesmartschool.MSG_NEW.StaffFilterTabPager;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Staff.Deparment_bean;
import com.ultimate.ultimatesmartschool.StaffGatePass.Adapteraddstff_departmnts;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.CommonProgress;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;
import com.ultimate.ultimatesmartschool.Utility.PaginationListener;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class StafftostuchatActivity extends AppCompatActivity {
    @BindView(R.id.imgBack)
    ImageView imgBackks;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    private Animation animation;

    @BindView(R.id.viewpager)
    ViewPager viewpager;
    @BindView(R.id.tab)
    TabLayout tablayout;
    String id="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_m_s_g);
        ButterKnife.bind(this);
        txtTitle.setText("Staff to Students");
        animation = AnimationUtils.loadAnimation(StafftostuchatActivity.this, R.anim.btn_blink_animation);
        setupTabPager();
    }
    private void setupTabPager() {
        StaffFilterTabPager adapter;
        adapter = new StaffFilterTabPager(getSupportFragmentManager());
        viewpager.setAdapter(adapter);
        tablayout.setupWithViewPager(viewpager);
    }

    @OnClick(R.id.imgBack)
    public void imgBackks()  {
        imgBackks.startAnimation(animation);
        finish();
    }

}
