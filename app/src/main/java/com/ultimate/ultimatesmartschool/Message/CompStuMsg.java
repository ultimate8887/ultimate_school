package com.ultimate.ultimatesmartschool.Message;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.speech.RecognizerIntent;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;

import com.ultimate.ultimatesmartschool.BeanModule.User;
import com.ultimate.ultimatesmartschool.ClassTest.Messageclass_adpter;
import com.ultimate.ultimatesmartschool.ForCamera.IPickResult;
import com.ultimate.ultimatesmartschool.ForCamera.PickImageDialog;
import com.ultimate.ultimatesmartschool.ForCamera.PickResult;
import com.ultimate.ultimatesmartschool.ForCamera.PickSetup;
import com.ultimate.ultimatesmartschool.Login.LoginActivity;
import com.ultimate.ultimatesmartschool.NoticeMod.AddNoticeBoard;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.StuGatePass.ClassBeans;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.CommonProgress;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;
import com.ultimate.ultimatesmartschool.Utility.ErpProgressVert;
import com.ultimate.ultimatesmartschool.Utility.FileUtils;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import smartdevelop.ir.eram.showcaseviewlib.GuideView;
import smartdevelop.ir.eram.showcaseviewlib.config.DismissType;
import smartdevelop.ir.eram.showcaseviewlib.listener.GuideListener;

public class CompStuMsg extends AppCompatActivity implements IPickResult {
    @BindView(R.id.imgBack)
    ImageView back;
    @BindView(R.id.spinnercls)
    Spinner spinnerClass;
    @BindView(R.id.spinnerstudnt) Spinner spinnerStudent;
    @BindView(R.id.parent)
    RelativeLayout parent;
    @BindView(R.id.send_message)
    Button send;
    @BindView(R.id.edtsub)
    EditText subedittext;
    @BindView(R.id.edtAddmessage)EditText editmsg;
    @BindView(R.id.stu_layout)RelativeLayout stulayout;
    String classid = "";
    String stu_id;
    String to_type="student";
    String from_id;
    ArrayList<ClassBeans> classList = new ArrayList<>();
    ArrayList<Student_msg_bean> stuList = new ArrayList<>();
    Spinner_stu_adapter adapterstu;
    CheckBox toallclssstudent;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    private Bitmap hwbitmap;
    @BindView(R.id.imageView6)
    ImageView imageView6;
    private int type;
    CommonProgress commonProgress;
    SharedPreferences sharedPreferences;
    @BindView(R.id.speak_text)
    ImageView speak_text;
    private static final int REQUEST_CODE_SPEECH_INPUT = 1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comp_stu_msg);
        ButterKnife.bind(this);
        commonProgress=new CommonProgress(this);
        toallclssstudent=(CheckBox)findViewById(R.id.checkBoxallstud);
        from_id= User.getCurrentUser().getId();
        txtTitle.setText("Message");
        Log.e("studentss2",from_id);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        fetchclasslist();
        toallclssstudent.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (toallclssstudent.isChecked()) {
                    stulayout.setVisibility(View.GONE);

                } else {

                    stulayout.setVisibility(View.VISIBLE);
                }

            }
        });
        if (!restorePrefData()){
            setShowcaseView();
        }

        speak_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent
                        = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
                intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                        RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
                intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE,
                        Locale.getDefault());
                intent.putExtra(RecognizerIntent.EXTRA_PROMPT, "Speak to text");

                try {
                    startActivityForResult(intent, REQUEST_CODE_SPEECH_INPUT);
                }
                catch (Exception e) {
                    Toast
                            .makeText(CompStuMsg.this, " " + e.getMessage(),
                                    Toast.LENGTH_SHORT)
                            .show();
                }

            }
        });
    }

    private void savePrefData(){
        sharedPreferences=this.getSharedPreferences("main_view_new_text_speech",MODE_PRIVATE);
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putBoolean("main_view_new_text_speech",true);
        editor.apply();
        // dataPasser.onTargetViewPass("save");
    }

    private boolean restorePrefData(){
        sharedPreferences = this.getSharedPreferences("main_view_new_text_speech",MODE_PRIVATE);
        return sharedPreferences.getBoolean("main_view_new_text_speech",false);
    }


    private void setShowcaseView() {
        new GuideView.Builder(this)
                .setTitle("Speech to text")
                .setContentText("Tap the Speech to \ntext Icon")
                .setTargetView(speak_text)
                .setContentTextSize(12)//optional
                .setTitleTextSize(14)//optional
                .setDismissType(DismissType.outside) //optional - default dismissible by TargetView
                .setGuideListener(new GuideListener() {
                    @Override
                    public void onDismiss(View view) {
                        savePrefData();
                    }
                })
                .build()
                .show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_SPEECH_INPUT) {
            if (resultCode == RESULT_OK && data != null) {
                ArrayList<String> result = data.getStringArrayListExtra(
                        RecognizerIntent.EXTRA_RESULTS);
                editmsg.setText(
                        Objects.requireNonNull(result).get(0));
            }
        }
    }


    private void fetchclasslist() {
        commonProgress.show();
        HashMap<String, String> params = new HashMap<String, String>();
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.CLASSLIST_URL, classapiCallback, this, params);
    }


    ApiHandler.ApiCallback classapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            commonProgress.dismiss();
            if (error == null) {
                try {
                    classList = ClassBeans.parseClassArray(jsonObject.getJSONArray(Constants.CLASSDATA));
                    Messageclass_adpter adapter = new Messageclass_adpter(CompStuMsg.this, classList);
                    spinnerClass.setAdapter(adapter);
                    spinnerClass.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                            if (i == 0) {

                                if (adapterstu != null) {
                                    stuList.clear();
                                    adapterstu.notifyDataSetChanged();
                                }
                            }
                            if (i > 0) {
                                toallclssstudent.setVisibility(View.VISIBLE);
                                classid = classList.get(i-1).getId();
                                stu_id="";

                                Log.e("classsid",classid);

                                fetchStudent(classid);
                            }else{
                                classid="";
                            }

                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                toallclssstudent.setVisibility(View.INVISIBLE);
                Utils.showSnackBar(error.getMessage(), parent);
            }
        }
    };

    public void fetchStudent(String classid) {
        commonProgress.show();
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("class_id",classid);
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.STUDENTLIST, apiCallback, this, params);

    }

    ApiHandler.ApiCallback apiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            commonProgress.dismiss();
            if (error == null) {
                try {
                    stuList = Student_msg_bean.parsestumsgArray(jsonObject.getJSONArray("studentlist"));
                    adapterstu = new Spinner_stu_adapter(CompStuMsg.this, stuList);
                    spinnerStudent.setAdapter(adapterstu);
                    spinnerStudent.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {


                            if (i > 0) {
                                stu_id = stuList.get(i-1).getId();

                                Log.e("studentss1",stu_id);

                            }else{
                                stu_id="";
                            }

                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Utils.showSnackBar(error.getMessage(), parent);
            }
        }
    };
    @OnClick(R.id.send_message)
    public void send_message(){
        if(checkValid()){
            commonProgress.show();
            HashMap<String,String> params=new HashMap<>();


            if (toallclssstudent.isChecked()) {
                params.put("all", "1");
                params.put("class_id", classid);
            } else {
                params.put("to_id",stu_id);
            }
            params.put("from_id",from_id);
            //  params.put("to_id",stu_id);

            if (hwbitmap != null) {
                String encoded = Utils.encodeToBase64(hwbitmap, Bitmap.CompressFormat.JPEG, 50);
                encoded = String.format("data:image/jpeg;base64,%s", encoded);
                params.put("image", encoded);
            }
            params.put("to_type",to_type);
            params.put("sub",subedittext.getText().toString());
            params.put("msg",editmsg.getText().toString());

            ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.MESSAGE, apisendmsgCallback, this, params);

        }
    }
    ApiHandler.ApiCallback apisendmsgCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgressVert.cancelProgressBar();
            commonProgress.dismiss();
            if (error == null) {
                try {
                    Toast.makeText(getApplicationContext(),jsonObject.getString("msg"),Toast.LENGTH_LONG).show();
                    Utils.showSnackBar(jsonObject.getString("msg"), parent);
                    finish();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("error", error.getMessage() + "");
                Utils.showSnackBar(error.getMessage(), parent);
                // Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_LONG).show();
                if (error.getStatusCode() == 405) {
                    User.logout();
                    startActivity(new Intent(CompStuMsg.this, LoginActivity.class));
                    finish();
                }
            }
        }
    };

    private boolean checkValid() {

        boolean valid = true;
        String errorMsg = null;
        if (classid.isEmpty() || classid == "") {
            valid = false;
            errorMsg = "Please select class";
        }else
        if (!toallclssstudent.isChecked() && (stu_id.isEmpty() || stu_id == "")) {
            valid = false;
            errorMsg = "Please select student";
        }
        else if (subedittext.getText().toString().trim().length() <= 0) {
            valid = false;
            errorMsg = "Please enter subject";
        }else if(editmsg.getText().toString().trim().length()<=0){
            valid=false;
            errorMsg="Please enter the Messages";
        }

        if (!valid) {
            Utils.showSnackBar(errorMsg, parent);
        }
        return valid;
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    @OnClick(R.id.textView6)
    public void click() {
        type = 1;
        pick_img();
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @SuppressLint("ResourceType")
    private void pick_img() {
            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.S) {
                if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_MEDIA_IMAGES)
                        != PackageManager.PERMISSION_GRANTED) {

                    if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_MEDIA_IMAGES)) {
                        Toast.makeText(this, "Please provide storage permission from app settings", Toast.LENGTH_LONG).show();
                    } else {

                        ActivityCompat.requestPermissions(this,
                                new String[]{Manifest.permission.READ_MEDIA_IMAGES},
                                1);
                    }
                } else {

                    onImageViewClick();
                }

            } else {
                if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {

                    if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                        Toast.makeText(this, "Please provide storage permission from app settings", Toast.LENGTH_LONG).show();
                    } else {

                        ActivityCompat.requestPermissions(this,
                                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                                1);
                    }
                } else {
                    onImageViewClick();

                }
        }
    }

    private void onImageViewClick() {
        // type = 1;
        PickSetup setup = new PickSetup();
        // super.customize(setup);

        PickImageDialog.build(setup)
                //.setOnClick(this)
                .show(this);
    }

    @Override
    public void onPickResult(PickResult r) {
        if (r.getError() == null) {
            //If you want the Bitmap.
            imageView6.setImageBitmap(r.getBitmap());
            hwbitmap= r.getBitmap();

        } else {
            //Handle possible errors
            //TODO: do what you have to do with r.getError();
            Toast.makeText(this, r.getError().getMessage(), Toast.LENGTH_LONG).show();
        }


    }

    public static boolean checkPermission(Context mContext, String Permission) {
        int result = ContextCompat.checkSelfPermission(mContext, Permission);
        if (result == PackageManager.PERMISSION_GRANTED) {

            return true;
        } else {
            return false;
        }
    }


}
