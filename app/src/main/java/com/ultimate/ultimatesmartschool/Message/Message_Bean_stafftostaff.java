package com.ultimate.ultimatesmartschool.Message;

import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.DebugLog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Message_Bean_stafftostaff {

    private static String SUBJECT="subject";
    private static String MESSAGE="message";
    private static String DATE="created_on";
    private static String MSG_FROM="from_type";
    private static String MESSAGE_FROMNAME="from_name";
    private static String MSG_TO="to_type";
    private static String MESSAGE_TONAME="to_name";

    private static String IMAGE="image";
    private static String FIMAGE="from_image";
    private static String TIMAGE="to_image";
    private static String CLASS="class_name";

    private static String SDATE="seen_time";


    private String pdf_file;

    public String getPdf_file() {
        return pdf_file;
    }

    public void setPdf_file(String pdf_file) {
        this.pdf_file = pdf_file;
    }

    public String getRecordingsfile() {
        return recordingsfile;
    }

    public void setRecordingsfile(String recordingsfile) {
        this.recordingsfile = recordingsfile;
    }

    private String recordingsfile;


    public String getImage_tag() {
        return image_tag;
    }

    public void setImage_tag(String image_tag) {
        this.image_tag = image_tag;
    }

    private String image_tag;

    public ArrayList<String> getMulti_image() {
        return multi_image;
    }

    public void setMulti_image(ArrayList<String> multi_image) {
        this.multi_image = multi_image;
    }

    private ArrayList<String> multi_image;


    public String getRowcount() {
        return rowcount;
    }

    public void setRowcount(String rowcount) {
        this.rowcount = rowcount;
    }

    private String rowcount;
    /**
     * id : 143
     * from_id : 5
     * from_type : staff
     * to_id : 8
     * to_type : staff
     * date : 2019-01-02 05:01:05
     * subject : hello
     * message : hello put test
     * from_name :
     * to_name : govind kish
     */
    private String image;
    private String to_image;
    private String class_name;

    public String getPost_name() {
        return post_name;
    }

    public void setPost_name(String post_name) {
        this.post_name = post_name;
    }

    private String post_name;

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getTo_image() {
        return to_image;
    }

    public void setTo_image(String to_image) {
        this.to_image = to_image;
    }

    public String getClass_name() {
        return class_name;
    }

    public void setClass_name(String class_name) {
        this.class_name = class_name;
    }

    public String getFrom_image() {
        return from_image;
    }

    public void setFrom_image(String from_image) {
        this.from_image = from_image;
    }

    private String from_image;

    private String created_on;
    public String getCreated_on() {
        return created_on;
    }

    public void setCreated_on(String created_on) {
        this.created_on = created_on;
    }
    public String getSeen() {
        return seen;
    }

    public void setSeen(String seen) {
        this.seen = seen;
    }

    private String seen;
    private String id;
    private String from_id;
    private String from_type;
    private String to_id;
    private String to_type;
    private String date;
    private String subject;
    private String message;
    private String from_name;
    private String to_name;

    public String getSeen_time() {
        return seen_time;
    }

    public void setSeen_time(String seen_time) {
        this.seen_time = seen_time;
    }

    private String seen_time;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFrom_id() {
        return from_id;
    }

    public void setFrom_id(String from_id) {
        this.from_id = from_id;
    }

    public String getFrom_type() {
        return from_type;
    }

    public void setFrom_type(String from_type) {
        this.from_type = from_type;
    }

    public String getTo_id() {
        return to_id;
    }

    public void setTo_id(String to_id) {
        this.to_id = to_id;
    }

    public String getTo_type() {
        return to_type;
    }

    public void setTo_type(String to_type) {
        this.to_type = to_type;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getFrom_name() {
        return from_name;
    }

    public void setFrom_name(String from_name) {
        this.from_name = from_name;
    }

    public String getTo_name() {
        return to_name;
    }

    public void setTo_name(String to_name) {
        this.to_name = to_name;
    }

    public static ArrayList<Message_Bean_stafftostaff> parseMessagestosArray(JSONArray jsonArray) {


        ArrayList<Message_Bean_stafftostaff> list = new ArrayList<Message_Bean_stafftostaff>();
        try {

            for (int i = 0; i < jsonArray.length(); i++) {
                Message_Bean_stafftostaff p = parsemessagestosObject(jsonArray.getJSONObject(i));
                if (p != null) {
                    list.add(p);
                }
            }
        } catch (Exception e) {
            DebugLog.printLog("exp",e.getMessage());
            e.printStackTrace();
        }
        return list;
    }



    private static Message_Bean_stafftostaff parsemessagestosObject(JSONObject jsonObject) {

        Message_Bean_stafftostaff msgObj = new Message_Bean_stafftostaff();
        try {


            if (jsonObject.has(SUBJECT) && !jsonObject.getString(SUBJECT).isEmpty() && !jsonObject.getString(SUBJECT).equalsIgnoreCase("null")) {
                msgObj.setSubject(jsonObject.getString(SUBJECT));
            }
            if (jsonObject.has(DATE) && !jsonObject.getString(DATE).isEmpty() && !jsonObject.getString(DATE).equalsIgnoreCase("null")) {
                msgObj.setCreated_on(jsonObject.getString(DATE));
            }
            if (jsonObject.has(MESSAGE) && !jsonObject.getString(MESSAGE).isEmpty() && !jsonObject.getString(MESSAGE).equalsIgnoreCase("null")) {
                msgObj.setMessage(jsonObject.getString(MESSAGE));
            }
            if (jsonObject.has(MSG_FROM) && !jsonObject.getString(MSG_FROM).isEmpty() && !jsonObject.getString(MSG_FROM).equalsIgnoreCase("null")) {
                msgObj.setFrom_type(jsonObject.getString(MSG_FROM));
            }

            if (jsonObject.has("seen") && !jsonObject.getString("seen").isEmpty() && !jsonObject.getString("seen").equalsIgnoreCase("null")) {
                msgObj.setSeen(jsonObject.getString("seen"));
            }

            if (jsonObject.has("rowcount") && !jsonObject.getString("rowcount").isEmpty() && !jsonObject.getString("rowcount").equalsIgnoreCase("null")) {
                msgObj.setRowcount(jsonObject.getString("rowcount"));
            }

            if (jsonObject.has(SDATE) && !jsonObject.getString(SDATE).isEmpty() && !jsonObject.getString(SDATE).equalsIgnoreCase("null")) {
                msgObj.setSeen_time(jsonObject.getString(SDATE));
            }
            if (jsonObject.has(MESSAGE_FROMNAME) && !jsonObject.getString(MESSAGE_FROMNAME).isEmpty() && !jsonObject.getString(MESSAGE_FROMNAME).equalsIgnoreCase("null")) {
                msgObj.setFrom_name(jsonObject.getString(MESSAGE_FROMNAME));
            }
            if (jsonObject.has(MSG_TO) && !jsonObject.getString(MSG_TO).isEmpty() && !jsonObject.getString(MSG_TO).equalsIgnoreCase("null")) {
                msgObj.setTo_type(jsonObject.getString(MSG_TO));
            }

            if (jsonObject.has(MESSAGE_TONAME) && !jsonObject.getString(MESSAGE_TONAME).isEmpty() && !jsonObject.getString(MESSAGE_TONAME).equalsIgnoreCase("null")) {
                msgObj.setTo_name(jsonObject.getString(MESSAGE_TONAME));
            }

            if (jsonObject.has(CLASS) && !jsonObject.getString(CLASS).isEmpty() && !jsonObject.getString(CLASS).equalsIgnoreCase("null")) {
                msgObj.setClass_name(jsonObject.getString(CLASS));
            }
            if (jsonObject.has("post_name") && !jsonObject.getString("post_name").isEmpty() && !jsonObject.getString("post_name").equalsIgnoreCase("null")) {
                msgObj.setPost_name(jsonObject.getString("post_name"));
            }

            if (jsonObject.has(IMAGE) && !jsonObject.getString(IMAGE).isEmpty() && !jsonObject.getString(IMAGE).equalsIgnoreCase("null")) {
                msgObj.setImage(Constants.getImageBaseURL()+jsonObject.getString(IMAGE));
            }


            if (jsonObject.has(FIMAGE) && !jsonObject.getString(FIMAGE).isEmpty() && !jsonObject.getString(FIMAGE).equalsIgnoreCase("null")) {
                msgObj.setFrom_image(Constants.getImageBaseURL()+jsonObject.getString(FIMAGE));
            }

            if (jsonObject.has(TIMAGE) && !jsonObject.getString(TIMAGE).isEmpty() && !jsonObject.getString(TIMAGE).equalsIgnoreCase("null")) {
                msgObj.setTo_image(Constants.getImageBaseURL()+jsonObject.getString(TIMAGE));
            }


            if (jsonObject.has("pdf_file") && !jsonObject.getString("pdf_file").isEmpty() && !jsonObject.getString("pdf_file").equalsIgnoreCase("null")) {
                msgObj.setPdf_file(Constants.getImageBaseURL()+jsonObject.getString("pdf_file"));
            }
            if (jsonObject.has("recordingsfile") && !jsonObject.getString("recordingsfile").isEmpty() && !jsonObject.getString("recordingsfile").equalsIgnoreCase("null")) {
                msgObj.setRecordingsfile(Constants.getImageBaseURL()+jsonObject.getString("recordingsfile"));
            }

            if (jsonObject.has("image_tag") && !jsonObject.getString("image_tag").isEmpty() && !jsonObject.getString("image_tag").equalsIgnoreCase("null")) {
                msgObj.setImage_tag(jsonObject.getString("image_tag"));
            }
            if (jsonObject.has("multi_image") && !jsonObject.getString("multi_image").isEmpty() && !jsonObject.getString("multi_image").equalsIgnoreCase("null")) {
                JSONArray img_arr = jsonObject.getJSONArray("multi_image");
                ArrayList<String> img_arrs = new ArrayList<>();
                for(int i=0;i<img_arr.length();i++){
                    img_arrs.add(Constants.getImageBaseURL()+img_arr.getString(i));
                }
                msgObj.setMulti_image(img_arrs);
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }
        return msgObj;


    }

}
