package com.ultimate.ultimatesmartschool.Message;

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.github.siyamed.shapeimageview.CircularImageView;
import com.squareup.picasso.Picasso;
import com.ultimate.ultimatesmartschool.BeanModule.User;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;

public class StafftostuMsg_Adapter extends RecyclerView.Adapter<StafftostuMsg_Adapter.Viewholder> {
    ArrayList<Message_Bean_stafftostaff> messagelist;
    Context context;
    Animation animation;
    private StafftostuMsg_Adapter.Mycallback mAdaptercall;

    public StafftostuMsg_Adapter(ArrayList<Message_Bean_stafftostaff> messagelist, Context context, StafftostuMsg_Adapter.Mycallback mAdaptercall) {
        this.context=context;
        this.messagelist=messagelist;
        this.mAdaptercall=mAdaptercall;
    }

    @NonNull
    @Override
    public StafftostuMsg_Adapter.Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.new_msg_adpt_lay_new_one,parent,false);
        StafftostuMsg_Adapter.Viewholder viewholder=new StafftostuMsg_Adapter.Viewholder(view);
        return viewholder;
    }

    @Override
    public void onBindViewHolder(@NonNull StafftostuMsg_Adapter.Viewholder holder, @SuppressLint("RecyclerView") int position) {
        animation = AnimationUtils.loadAnimation(context, R.anim.btn_blink_animation);
        //        holder.subject.setEnabled(false);
//        holder.message.setEnabled(false);
        //  holder.txtName.setText(messagelist.get(position).getFrom_type());
        //  holder.message.setText(Html.fromHtml(messagelist.get(position).getMessage()));
     //   holder.parent.setAnimation(AnimationUtils.loadAnimation(context,R.anim.fade_scale_animation));
        java.util.Calendar calendar = java.util.Calendar.getInstance(TimeZone.getDefault());
        Date setdate = calendar.getTime();
        SimpleDateFormat fmtOut = new SimpleDateFormat("dd MMM, yyyy");
        String dateString = fmtOut.format(setdate);
        //for Yesterday
        DateFormat dateFormat = new SimpleDateFormat("dd MMM, yyyy");
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);
        String dateString1= dateFormat.format(cal.getTime());


        String check= Utils.getDateFormated(messagelist.get(position).getCreated_on());
        if (check.equalsIgnoreCase(dateString)){
            //  holder.lytLine.setVisibility(View.VISIBLE);
            // holder.txtDT.setText("Today");
            String title = getColoredSpanned("Today", "#e31e25");
            //String Name = getColoredSpanned(messagelist.get(position).getFrom_type(), "#7D7D7D");
            String l_Name = getColoredSpanned("", "#5A5C59");
            holder.txtDT.setText(Html.fromHtml(title + " " + l_Name));


            // holder.txtDT.setText("Today at "+time);
            //  holder.vieww.setBackground(mContext.getResources().getDrawable(R.drawable.a_card_lyt2));
        }else if (check.equalsIgnoreCase(dateString1)){

            String title = getColoredSpanned("Yesterday", "#1C8B3B");
            //String Name = getColoredSpanned(messagelist.get(position).getFrom_type(), "#7D7D7D");
            String l_Name = getColoredSpanned("", "#5A5C59");
            holder.txtDT.setText(Html.fromHtml(title + " " + l_Name));

            //  holder.lytLine.setVisibility(View.GONE);
            //  holder.vieww.setBackground(mContext.getResources().getDrawable(R.drawable.card_lyt2));
        } else {
            holder.txtDT.setText(Utils.getDateFormated(messagelist.get(position).getCreated_on()));

            //  holder.lytLine.setVisibility(View.GONE);
            //  holder.vieww.setBackground(mContext.getResources().getDrawable(R.drawable.card_lyt2));
        }
        holder.txtTime.setText(Utils.getTimeHr(messagelist.get(position).getCreated_on()));
        //  holder.txtDT.setText(Utility.getDateTimeFormatedWithAMPM(messagelist.get(position).getCreated_on()));

        if (messagelist.get(position).getSeen().equalsIgnoreCase("yes")
                && messagelist.get(position).getSeen_time().equalsIgnoreCase("0000-00-00 00:00:00")) {
            //holder.login_device.setText("Birthday Card Seen at " + Utils.getTimeHr(mData.getDate_time()));
            holder.txtSeenTime.setVisibility(View.GONE);
        }else{

            if (messagelist.get(position).getSeen().equalsIgnoreCase("yes")){
                String title = getColoredSpanned("Seen at ", "#000000");
                String Name = getColoredSpanned("<b>" +Utils.getDateTimeFormatedWithAMPM(messagelist.get(position).getSeen_time())+"</b>", "#1C8B3B");                holder.txtSeenTime.setText(Html.fromHtml(title + " " + Name));
            }else{
                String title = getColoredSpanned("Not Seen yet", "#F4212C");
                String Name = getColoredSpanned("", "#5A5C59");
                holder.txtSeenTime.setText(Html.fromHtml(title + " " + Name));
            }

            holder.txtSeenTime.setVisibility(View.VISIBLE);

        }

        holder.check_in.setText(messagelist.get(position).getSubject());

        holder.txtName1.setText(messagelist.get(position).getFrom_name());
      //  holder.txtRegNo2.setText("["+messagelist.get(position).getTo_type()+"]");
        holder.txtRegNo.setText(messagelist.get(position).getTo_name());

        if (messagelist.get(position).getFrom_image() != null) {
            Picasso.get().load(messagelist.get(position).getTo_image()).placeholder(R.drawable.stud).into(holder.circleimg);
        } else {
            Picasso.get().load(R.drawable.stud).into(holder.circleimg);
        }

        if (messagelist.get(position).getFrom_type() != null) {
            String title = getColoredSpanned("From: ", "#1C8B3B");
            //String Name = getColoredSpanned(messagelist.get(position).getFrom_type(), "#7D7D7D");
            String l_Name = getColoredSpanned("["+ messagelist.get(position).getPost_name()+"]", "#5A5C59");
            holder.txtRegNo1.setText(Html.fromHtml(title + " " + l_Name));
        }

        if (messagelist.get(position).getClass_name() != null) {
            String title = getColoredSpanned("To: ", "#e31e25");
            //String Name = getColoredSpanned(messagelist.get(position).getFrom_type(), "#7D7D7D");
            String l_Name = getColoredSpanned( "["+ messagelist.get(position).getClass_name()+""+"]", "#5A5C59");
            holder.txtName.setText(Html.fromHtml(title + " " + l_Name));
        }


//        if (messagelist.get(position).getImage() != null) {
//            holder.imageView6.setVisibility(View.VISIBLE);
//            Picasso.with(context).load(messagelist.get(position).getImage()).placeholder(context.getResources().getDrawable(R.drawable.logo)).into(holder.imageView6);
//        holder.parent.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                holder.done.startAnimation(animation);
//                if(mAdaptercall!= null){
//                    mAdaptercall.onMethod_Image_callback(messagelist.get(position));
//                }
//            }
//        });


        holder.sendername.setVisibility(View.GONE);

        holder.subject.setEnabled(false);
        holder.message.setEnabled(false);
        holder.subject.setText(messagelist.get(position).getSubject());
        // message.setText(Html.fromHtml(sentmsglist.getMessage()));
        if (User.getCurrentUser().getSchoolData().getFi_school_id().equalsIgnoreCase("MSS2024")
                || User.getCurrentUser().getSchoolData().getFi_school_id().equalsIgnoreCase("NMS2024")){
            holder.message.setText(Html.fromHtml(messagelist.get(position).getMessage()));
        }else{
            holder.message.setText(messagelist.get(position).getMessage());
        }
        holder.txtdate.setText(Utils.getTimeHr(messagelist.get(position).getCreated_on()));

        if (messagelist.get(position).getImage_tag().equalsIgnoreCase("images")){
            holder.nofile.setVisibility(View.VISIBLE);
            holder.image_file.setVisibility(View.VISIBLE);
            holder.audio_file.setVisibility(View.GONE);
            holder.pdf_file.setVisibility(View.GONE);
            holder.empty_file.setVisibility(View.GONE);
            Log.d("selectedImageUri",(messagelist.get(position).getMulti_image().get(0)));
            Picasso.get().load(messagelist.get(position).getMulti_image().get(0)).placeholder(context.getResources().getDrawable(R.color.transparent)).into(holder.image_file);
            holder.image_file.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    holder.image_file.startAnimation(animation);
                    if(mAdaptercall!= null){
                        mAdaptercall.onMethod_Image_callback_new(messagelist.get(position));
                    }
                }
            });
        } else if (messagelist.get(position).getRecordingsfile() != null){
            holder.nofile.setVisibility(View.VISIBLE);
            holder.image_file.setVisibility(View.GONE);
            holder.empty_file.setVisibility(View.GONE);
            holder.audio_file.setVisibility(View.VISIBLE);
            holder.pdf_file.setVisibility(View.GONE);

            holder.audio_file.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    holder.audio_file.startAnimation(animation);
                    if(mAdaptercall!= null){
                        mAdaptercall.onMethod_recording_callback(messagelist.get(position));
                    }
                }
            });
        } else if (messagelist.get(position).getPdf_file() != null){
            holder.nofile.setVisibility(View.VISIBLE);
            holder.image_file.setVisibility(View.GONE);
            holder.audio_file.setVisibility(View.GONE);
            holder.pdf_file.setVisibility(View.VISIBLE);
            holder.empty_file.setVisibility(View.GONE);
            holder.pdf_file.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    holder.pdf_file.startAnimation(animation);
                    if(mAdaptercall!= null){
                        mAdaptercall.onMethod_pdf_call_back(messagelist.get(position));
                    }
                }
            });
        }else {
            holder.nofile.setVisibility(View.GONE);
            holder.image_file.setVisibility(View.GONE);
            holder.audio_file.setVisibility(View.GONE);
            holder.pdf_file.setVisibility(View.GONE);
            holder.empty_file.setVisibility(View.GONE);
        }


//        } else {
//            holder.imageView6.setVisibility(View.GONE);
//        }
    }

    public interface Mycallback{
        public void onMethod_Image_callback(Message_Bean_stafftostaff message_bean);

        public void onMethod_Image_callback_new(Message_Bean_stafftostaff message_bean);
        public void onMethod_recording_callback(Message_Bean_stafftostaff message_bean);
        public void onMethod_pdf_call_back(Message_Bean_stafftostaff message_bean);
    }


    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }

    @Override
    public int getItemCount() {
        return messagelist.size();
    }
    public void setsntmessageList(ArrayList<Message_Bean_stafftostaff> sentmsglist) {
        this.messagelist = sentmsglist;
    }

    public class Viewholder extends RecyclerView.ViewHolder {
        @BindView(R.id.txtRegNo)
        TextView txtRegNo;
        @BindView(R.id.txtRegNo1)
        TextView txtRegNo1;
        @BindView(R.id.txtName)
        TextView txtName;
        @BindView(R.id.txtName1)
        TextView txtName1;
        @BindView(R.id.txtFname)
        TextView txtFname;
        @BindView(R.id.circleimg)
        CircularImageView circleimg;
        @BindView(R.id.txtReason)
        EditText check_in;
        @BindView(R.id.txtDT)
        TextView txtDT;
        @BindView(R.id.txtTime)
        TextView txtTime;
        @BindView(R.id.parent)
        CardView parent;
        @BindView(R.id.done)
        ImageView done;
        @BindView(R.id.txtSeenTime)
        TextView txtSeenTime;

        @BindView(R.id.lytLine)
        View lytLine;


        @BindView(R.id.nofile)
        RelativeLayout nofile;

        @BindView(R.id.empty_file)
        ImageView empty_file;

        public ImageView image_file, audio_file, pdf_file;
        ImageView imageView6;
        EditText subject;
        EditText message;
        TextView sendername,txtdate;

        public Viewholder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
            imageView6 = (ImageView) itemView.findViewById(R.id.imageView6);
            subject = (EditText) itemView.findViewById(R.id.txtsubject);
            message = (EditText) itemView.findViewById(R.id.message_body);
            sendername = (TextView) itemView.findViewById(R.id.txtsendername);
            txtdate = (TextView) itemView.findViewById(R.id.txtdate);

            image_file = (ImageView) itemView.findViewById(R.id.image_file);
            pdf_file = (ImageView) itemView.findViewById(R.id.pdf_file);
            audio_file = (ImageView) itemView.findViewById(R.id.audio_file);
        }
    }
}
