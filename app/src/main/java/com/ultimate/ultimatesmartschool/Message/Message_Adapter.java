package com.ultimate.ultimatesmartschool.Message;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;
import com.ultimate.ultimatesmartschool.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Message_Adapter extends RecyclerView.Adapter<Message_Adapter.Viewholder> {
    ArrayList<Message_Bean> messagelist;
    Context context;

    public Message_Adapter(ArrayList<Message_Bean> messagelist, Context context) {
        this.context=context;
        this.messagelist=messagelist;

    }

    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.message_adpt_layout,parent,false);
        Message_Adapter.Viewholder viewholder=new Message_Adapter.Viewholder(view);
        return viewholder;
    }

    @Override
    public void onBindViewHolder(@NonNull Viewholder holder, int position) {
        holder.parent.setAnimation(AnimationUtils.loadAnimation(context,R.anim.fade_scale_animation));
        holder.subject.setText(messagelist.get(position).getSubject());
        holder.date.setText(messagelist.get(position).getCreated_on());
        holder.message.setText(Html.fromHtml(messagelist.get(position).getMessage()));
        holder.sendername.setText(messagelist.get(position).getFrom_name()+"["+ messagelist.get(position).getFrom_type()+"]");
        if (messagelist.get(position).getImage() != null) {
            holder.imageView6.setVisibility(View.VISIBLE);
            Picasso.get().load(messagelist.get(position).getImage()).placeholder(context.getResources().getDrawable(R.drawable.home)).into(holder.imageView6);
        } else {
            holder.imageView6.setVisibility(View.GONE);
            //Picasso.with(listner).load(String.valueOf(listner.getResources().getDrawable(R.drawable.home))).into(holder.iv2);
        }
    }

    @Override
    public int getItemCount() {
        return messagelist.size();
    }

    public void setmessageList(ArrayList<Message_Bean> messagelist) {
        this.messagelist = messagelist;
    }

    public class Viewholder extends RecyclerView.ViewHolder {
        @BindView(R.id.txtsubject)
        TextView subject;
        @BindView(R.id.txtdate)TextView date;
        @BindView(R.id.message_body)TextView message;
        @BindView(R.id.txtsendername)TextView sendername;
        @BindView(R.id.imageView6)
        ImageView imageView6;
        @BindView(R.id.card1)
        CardView parent;
        public Viewholder(View itemView) {

            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}
