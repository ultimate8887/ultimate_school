package com.ultimate.ultimatesmartschool.Message;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.ultimate.ultimatesmartschool.BeanModule.User;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

public class StafftostaffchatActivity extends AppCompatActivity {
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.recyclerViewmsgstaff)RecyclerView recyclerView;
    ArrayList<Message_Bean_stafftostaff> messagelist=new ArrayList<>();
    private StafftostaffMsg_Adapter adapter;
    String id;
    @BindView(R.id.parent)
    RelativeLayout parent;
    int loaded=0;
    RecyclerView.LayoutManager layoutManager;
    @BindView(R.id.imgBack)
    ImageView back;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stafftostaffchat);
        ButterKnife.bind(this);
        txtTitle.setText("Staff to Staff Message ");
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        id= User.getCurrentUser().getId();
        layoutManager=new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        adapter=new StafftostaffMsg_Adapter(messagelist,StafftostaffchatActivity.this);
        recyclerView.setAdapter(adapter);
        fetchmessagelist();
    }
    private void fetchmessagelist() {
        if (loaded == 0) {
            ErpProgress.showProgressBar(this, "Please wait...");
        }
        loaded++;
        HashMap<String,String> params=new HashMap<>();
        ApiHandler.apiHit(Request.Method.GET, Constants.getBaseURL() + Constants.STAFFTOSTAFFMSGLIST_URL,apicallback,this,params);
    }
    ApiHandler.ApiCallback apicallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            if (error == null) {
                try {
                    if (messagelist != null) {
                        messagelist.clear();
                    }
                    JSONArray jsonArray = jsonObject.getJSONArray("stafftostaffmsglist");
                    messagelist = Message_Bean_stafftostaff.parseMessagestosArray(jsonArray);
                    if (messagelist.size() > 0) {
                        adapter.setmessageList(messagelist);
                        adapter.notifyDataSetChanged();
                    } else {
                        adapter.setmessageList(messagelist);
                        adapter.notifyDataSetChanged();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                messagelist.clear();
                adapter.setmessageList(messagelist);
                adapter.notifyDataSetChanged();
                Utils.showSnackBar(error.getMessage(), parent);
            }
        }
    };


}
