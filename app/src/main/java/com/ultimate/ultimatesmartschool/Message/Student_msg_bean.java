package com.ultimate.ultimatesmartschool.Message;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Student_msg_bean {
    private static String ID = "id";
    private static String NAME = "name";
    private static String F_NAME="father_name";
    private static String  CLASS_NAME="class_name";
    private static String PHONE = "mobile";
    private static String DOB = "dob";
    private static String GENDER="gender";
    private static String ADDRESS = "address";
    private static String CITY = "city";
    private static String STATE="state";
    private static String ROLLNO="roll_no";
    private static String CLASSID="class_id";
    /**
     * id : 22
     * name : sapna goyal
     * class_name : PREP-1
     * father_name : sapna
     * mobile : 8857686867
     * profile : office_admin/images/st_05192018_080531.jpg
     */
    private String class_id;
    private String id;

    public String getFmobile() {
        return fmobile;
    }

    public void setFmobile(String fmobile) {
        this.fmobile = fmobile;
    }

    public String getMmobile() {
        return mmobile;
    }

    public void setMmobile(String mmobile) {
        this.mmobile = mmobile;
    }

    private String name;
    private String class_name;
    private String father_name;
    private String mobile;
    private String profile;
    private String fmobile;
    private String mmobile;
    public String getClass_id() {
        return class_id;
    }

    public void setClass_id(String class_id) {
        this.class_id = class_id;
    }

    /**
     * dob : 2018-10-18
     * gender : male
     * address : patna
     * city : patna
     * state :
     */

    private String dob;
    private String gender;
    private String address;
    private String city;
    private String state;
    /**
     * roll_no : 67
     */

    private String roll_no;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getClass_name() {
        return class_name;
    }

    public void setClass_name(String class_name) {
        this.class_name = class_name;
    }

    public String getFather_name() {
        return father_name;
    }

    public void setFather_name(String father_name) {
        this.father_name = father_name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }

    public static ArrayList<Student_msg_bean> parsestumsgArray(JSONArray studentlist) {
        ArrayList<Student_msg_bean> list = new ArrayList<Student_msg_bean>();
        try {

            for (int i = 0; i < studentlist.length(); i++) {
                Student_msg_bean p = parsestumsgObject(studentlist.getJSONObject(i));
                if (p != null) {
                    list.add(p);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    private static Student_msg_bean parsestumsgObject(JSONObject jsonObject) {
        Student_msg_bean casteObj = new Student_msg_bean();
        try {
            if (jsonObject.has(ID)) {
                casteObj.setId(jsonObject.getString(ID));
            }
            if (jsonObject.has(NAME) && !jsonObject.getString(NAME).isEmpty() && !jsonObject.getString(NAME).equalsIgnoreCase("null")) {
                casteObj.setName(jsonObject.getString(NAME));
            }
            if (jsonObject.has(F_NAME) && !jsonObject.getString(F_NAME).isEmpty() && !jsonObject.getString(F_NAME).equalsIgnoreCase("null")) {
                casteObj.setFather_name(jsonObject.getString(F_NAME));
            }
            if (jsonObject.has(CLASS_NAME) && !jsonObject.getString(CLASS_NAME).isEmpty() && !jsonObject.getString(CLASS_NAME).equalsIgnoreCase("null")) {
                casteObj.setClass_name(jsonObject.getString(CLASS_NAME));
            }
            if (jsonObject.has(PHONE) && !jsonObject.getString(PHONE).isEmpty() && !jsonObject.getString(PHONE).equalsIgnoreCase("null")) {
                casteObj.setMobile(jsonObject.getString(PHONE));
            }
            if (jsonObject.has(DOB) && !jsonObject.getString(DOB).isEmpty() && !jsonObject.getString(DOB).equalsIgnoreCase("null")) {
                casteObj.setDob(jsonObject.getString(DOB));
            }

            if (jsonObject.has(GENDER) && !jsonObject.getString(GENDER).isEmpty() && !jsonObject.getString(GENDER).equalsIgnoreCase("null")) {
                casteObj.setGender(jsonObject.getString(GENDER));
            }

            if (jsonObject.has(ADDRESS) && !jsonObject.getString(ADDRESS).isEmpty() && !jsonObject.getString(ADDRESS).equalsIgnoreCase("null")) {
                casteObj.setAddress(jsonObject.getString(ADDRESS));
            }
            if (jsonObject.has(CITY) && !jsonObject.getString(CITY).isEmpty() && !jsonObject.getString(CITY).equalsIgnoreCase("null")) {
                casteObj.setCity(jsonObject.getString(CITY));
            }
            if (jsonObject.has(STATE) && !jsonObject.getString(STATE).isEmpty() && !jsonObject.getString(STATE).equalsIgnoreCase("null")) {
                casteObj.setState(jsonObject.getString(STATE));
            }

            if (jsonObject.has(ROLLNO) && !jsonObject.getString(ROLLNO).isEmpty() && !jsonObject.getString(ROLLNO).equalsIgnoreCase("null")) {
                casteObj.setRoll_no(jsonObject.getString(ROLLNO));
            }

            if (jsonObject.has(CLASSID) && !jsonObject.getString(CLASSID).isEmpty() && !jsonObject.getString(CLASSID).equalsIgnoreCase("null")) {
                casteObj.setClass_id(jsonObject.getString(CLASSID));
            }

            if (jsonObject.has("fmobile") && !jsonObject.getString("fmobile").isEmpty() && !jsonObject.getString("fmobile").equalsIgnoreCase("null")) {
                casteObj.setFmobile(jsonObject.getString("fmobile"));
            }
            if (jsonObject.has("mmobile") && !jsonObject.getString("mmobile").isEmpty() && !jsonObject.getString("mmobile").equalsIgnoreCase("null")) {
                casteObj.setMmobile(jsonObject.getString("mmobile"));
            }
//            if (jsonObject.has(PROFILE) && !jsonObject.getString(PROFILE).isEmpty() && !jsonObject.getString(PROFILE).equalsIgnoreCase("null")) {
//                casteObj.setProfile(Constants.getImageBaseURL()+jsonObject.getString(PROFILE));
//            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return casteObj;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getRoll_no() {
        return roll_no;
    }

    public void setRoll_no(String roll_no) {
        this.roll_no = roll_no;
    }

}
