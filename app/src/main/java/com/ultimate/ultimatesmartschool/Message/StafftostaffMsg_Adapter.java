package com.ultimate.ultimatesmartschool.Message;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.ultimate.ultimatesmartschool.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class StafftostaffMsg_Adapter extends RecyclerView.Adapter<StafftostaffMsg_Adapter.Viewholder>{
    ArrayList<Message_Bean_stafftostaff> messagelist;
    Context context;

    public StafftostaffMsg_Adapter(ArrayList<Message_Bean_stafftostaff> messagelist, Context context) {

        this.context=context;
        this.messagelist=messagelist;
    }

    @NonNull
    @Override
    public StafftostaffMsg_Adapter.Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.message_adpt_layout_stos,parent,false);
        StafftostaffMsg_Adapter.Viewholder viewholder=new StafftostaffMsg_Adapter.Viewholder(view);
        return viewholder;
    }

    @Override
    public void onBindViewHolder(@NonNull StafftostaffMsg_Adapter.Viewholder holder, int position) {

        holder.parent.setAnimation(AnimationUtils.loadAnimation(context,R.anim.fade_scale_animation));

        holder.subject.setText(messagelist.get(position).getSubject());
        holder.date.setText(messagelist.get(position).getDate());
        holder.message.setText(Html.fromHtml(messagelist.get(position).getMessage()));
        holder.sendername.setText("To:"+messagelist.get(position).getFrom_name()+"["+ messagelist.get(position).getFrom_type()+"]");
        holder.gettername.setText("From:"+messagelist.get(position).getTo_name()+"["+messagelist.get(position).getTo_type()+"]");
    }

    @Override
    public int getItemCount() {
        return messagelist.size();
    }

    public void setmessageList(ArrayList<Message_Bean_stafftostaff> messagelist) {
        this.messagelist = messagelist;
    }

    public class Viewholder extends RecyclerView.ViewHolder {
        @BindView(R.id.txtsubject)
        TextView subject;
        @BindView(R.id.txtdate)TextView date;
        @BindView(R.id.message_body)TextView message;
        @BindView(R.id.txtsendername)TextView sendername;
        @BindView(R.id.txtsendernamekiskobheja)TextView gettername;
        @BindView(R.id.card1)
        CardView parent;
        public Viewholder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);



        }
    }
}
