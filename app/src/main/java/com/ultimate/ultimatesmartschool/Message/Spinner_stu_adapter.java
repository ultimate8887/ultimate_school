package com.ultimate.ultimatesmartschool.Message;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.siyamed.shapeimageview.CircularImageView;
import com.squareup.picasso.Picasso;
import com.ultimate.ultimatesmartschool.R;

import java.util.ArrayList;

public class Spinner_stu_adapter extends BaseAdapter {
    Context context;
    ArrayList<Student_msg_bean> stuList;
    LayoutInflater inflter;
    public Spinner_stu_adapter(Context context, ArrayList<Student_msg_bean> stuList) {
        this.context=context;
        this.stuList=stuList;
        inflter = (LayoutInflater.from(context));
    }

    @Override
    public int getCount() {
        return stuList.size()+1;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup parent) {
        view = inflter.inflate(R.layout.spinner_lyt_new, null);
        TextView label = (TextView) view.findViewById(R.id.txt);
        RelativeLayout lyt = (RelativeLayout) view.findViewById(R.id.lyt);
        TextView label1 = (TextView) view.findViewById(R.id.txtText);
        CircularImageView dp = (CircularImageView) view.findViewById(R.id.dp);
        if (i == 0) {
            // Default selected Spinner item
            label.setText("Select Student");
            lyt.setVisibility(View.GONE);
            label.setVisibility(View.VISIBLE);
        } else {
            label.setVisibility(View.GONE);
            lyt.setVisibility(View.VISIBLE);
            Student_msg_bean classObj = stuList.get(i - 1);
            label1.setText(classObj.getName()+" \n("+classObj.getId()+")");
            if (classObj.getProfile() != null) {
                Picasso.get().load(classObj.getProfile()).placeholder(R.drawable.stud).into(dp);
            }else {
                Picasso.get().load(R.drawable.stud).into(dp);
            }

        }
        return view;
    }
}
