package com.ultimate.ultimatesmartschool.Message;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.ClipboardManager;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.squareup.picasso.Picasso;
import com.ultimate.ultimatesmartschool.AddmissionForm.FeeCatBean;
import com.ultimate.ultimatesmartschool.BeanModule.User;
import com.ultimate.ultimatesmartschool.Login.LoginActivity;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.CommonProgress;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class Sent_MessageActivity extends AppCompatActivity implements Sent_msg_Adapter.Mycallback {
    @BindView(R.id.recyclerViewmsg)
    RecyclerView recyclerView;
    @BindView(R.id.imgBack)
    ImageView back;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.parent)
    RelativeLayout parent;
    LinearLayoutManager layoutManager;
    ArrayList<Sent_message_bean> sentmsglist = new ArrayList<>();
    Sent_msg_Adapter adapter;
    String from_date;
    String to_date;
    int loaded = 0;
    String id;

    ImageView imageView6;
    EditText subject;
    EditText message;
    TextView sendername,txtdate;
    String type="2",name="",date="";
    @BindView(R.id.textNorecord)
    TextView txtNorecord;
    Animation animation;
    @BindView(R.id.spinnerGroups)
    Spinner spinnerMonth;
    ArrayList<String> monthList = new ArrayList<>();
    @BindView(R.id.totalRecord)
    TextView totalRecord;
    CommonProgress commonProgress;

    @BindView(R.id.main_progress)
    ProgressBar main_progress;
    @BindView(R.id.idNestedSV)
    NestedScrollView nestedSV;
    int  limit=50,page_limit=0,total_pages=0;
    private static final int PAGE_START = 1;
    private int currentPage = PAGE_START;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inbox);
        ButterKnife.bind(this);
        txtTitle.setText("Sent Message");
        commonProgress=new CommonProgress(this);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.btn_blink_animation);
        id = User.getCurrentUser().getId();
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new Sent_msg_Adapter(sentmsglist, Sent_MessageActivity.this,this);
        recyclerView.setAdapter(adapter);

        // adding on scroll change listener method for our nested scroll view.
        nestedSV.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                // on scroll change we are checking when users scroll as bottom.
                if (scrollY == v.getChildAt(0).getMeasuredHeight() - v.getMeasuredHeight()) {
                    // in this method we are incrementing page number,
                    // making progress bar visible and calling get data method.
                    if (total_pages>=page_limit) {
                        main_progress.setVisibility(View.VISIBLE);
                        currentPage += 1;
                        page_limit = currentPage * limit;
                        fetchsentmsglist("", page_limit);
                    }else{
                        Utils.showSnackBar("No more messages found in message list.", parent);
                    }

                }
            }
        });

    }


    private void fetchsentmsglist(String name,int limit) {
        if (loaded == 0) {
            commonProgress.show();
        }
        loaded++;
        HashMap<String, String> params = new HashMap<>();
        ApiHandler.apiHit(Request.Method.GET, Constants.getBaseURL() + Constants.MESSAGE+Constants.MSGID+id+Constants.MSGTYPE+type
                +Constants.NAME1+name+Constants.DATE+date+"&page_limit="+limit,apicallback,this,params);
    }

    ApiHandler.ApiCallback apicallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            //   commonProgress.dismiss();
            if (error == null) {
                try {
                    if (sentmsglist != null) {
                        sentmsglist.clear();
                    }
                    JSONArray jsonArray = jsonObject.getJSONArray("msg_data");
                    ArrayList<Sent_message_bean> list = Sent_message_bean.parsesntMessageArray(jsonArray);
                    sentmsglist.addAll(list);
                    total_pages= Integer.parseInt(sentmsglist.get(0).getRowcount());
                    //   adapter.setsntmessageList(sentmsglist);
                    adapter.notifyDataSetChanged();
                    txtNorecord.setVisibility(View.GONE);
                    totalRecord.setText("Total Entries:- "+String.valueOf(sentmsglist.size()));
                    commonProgress.dismiss();
                    main_progress.setVisibility(View.GONE);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                commonProgress.dismiss();
                main_progress.setVisibility(View.GONE);
                totalRecord.setText("Total Entries:- 0");
                txtNorecord.setVisibility(View.VISIBLE);
                sentmsglist.clear();
//                adapter.setsntmessageList(sentmsglist);
                adapter.notifyDataSetChanged();
                Utils.showSnackBar(error.getMessage(), parent);
            }
        }
    };




    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onResume() {
        super.onResume();
        //if (classid != null && !classid.isEmpty())
        fetchmonthList();
        getTodayDate();
    }

    public void getTodayDate() {
        java.util.Calendar c = java.util.Calendar.getInstance();
        c.set(java.util.Calendar.HOUR_OF_DAY, 00);
        c.set(java.util.Calendar.MINUTE, 00);
        c.set(java.util.Calendar.SECOND, 00);
        Date setdate = c.getTime();
        java.text.SimpleDateFormat fmtOut = new java.text.SimpleDateFormat("dd MMM, yyyy");
        String dateString = fmtOut.format(setdate);
        java.text.SimpleDateFormat dateFrmOut = new java.text.SimpleDateFormat("yyyy-MM-dd");
        date = dateFrmOut.format(setdate);
    }


    @RequiresApi(api = Build.VERSION_CODES.N)
    private void fetchmonthList() {

        monthList.add("All");
        monthList.add("Today");
        monthList.add("History");

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, monthList);
        //set the view for the Drop down list
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //set the ArrayAdapter to the spinner
        spinnerMonth.setAdapter(dataAdapter);

        android.icu.util.Calendar cal= android.icu.util.Calendar.getInstance();
        android.icu.text.SimpleDateFormat month_date = new android.icu.text.SimpleDateFormat("MMMM");
        name = month_date.format(cal.getTime());


        //attach the listener to the spinner
        spinnerMonth.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long l) {

                if ( monthList.get(pos).equalsIgnoreCase("All")) {
                    name = "";
                }else {
                    name = monthList.get(pos);
                }
                fetchsentmsglist(name,limit);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }


    @Override
    public void onMethod_Image_callback(Sent_message_bean message_bean) {
        Dialog mBottomSheetDialog = new Dialog(this);
        final View sheetView = getLayoutInflater().inflate(R.layout.view_msgimage_lyt, null);
        mBottomSheetDialog.setContentView(sheetView);
        mBottomSheetDialog.setCancelable(true);

        Animation animation;

        animation = AnimationUtils.loadAnimation(this, R.anim.btn_blink_animation);
        imageView6 = (ImageView) sheetView.findViewById(R.id.imageView6);
        subject = (EditText) sheetView.findViewById(R.id.txtsubject);
        message = (EditText) sheetView.findViewById(R.id.message_body);
        sendername = (TextView) sheetView.findViewById(R.id.txtsendername);
        txtdate = (TextView) sheetView.findViewById(R.id.txtdate);
        setData(message_bean);
        ImageView copy = (ImageView) sheetView.findViewById(R.id.copy);
        copy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                copy.startAnimation(animation);
                ClipboardManager cm = (ClipboardManager)getApplicationContext().getSystemService(Context.CLIPBOARD_SERVICE);
                cm.setText(message.getText().toString());
                Toast.makeText(getApplicationContext(), "Text copied to clipboard", Toast.LENGTH_SHORT).show();
            }
        });
        imageView6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                downloadfile(message_bean);
                //  Toast.makeText(GDSTest.this, "Downloaded successfully", Toast.LENGTH_LONG).show();
            }
        });
        RelativeLayout btnNo= (RelativeLayout) sheetView.findViewById(R.id.btnNo);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnNo.startAnimation(animation);
                mBottomSheetDialog.dismiss();
            }
        });

        mBottomSheetDialog.show();
    }
    private void setData(Sent_message_bean sentmsglist) {
        subject.setEnabled(false);
        message.setEnabled(false);
        subject.setText(sentmsglist.getSubject());
        if (User.getCurrentUser().getSchoolData().getFi_school_id().equalsIgnoreCase("MSS2024")
                || User.getCurrentUser().getSchoolData().getFi_school_id().equalsIgnoreCase("NMS2024")){
            message.setText(Html.fromHtml(sentmsglist.getMessage()));
        }else{
            message.setText(sentmsglist.getMessage());
        }
        txtdate.setText(Utils.getTimeHr(sentmsglist.getCreated_on()));
        if (sentmsglist.getTo_name() != null) {
            String title = getColoredSpanned("To: ", "#e31e25");
            String Name = getColoredSpanned(sentmsglist.getTo_name(), "#7D7D7D");
            String l_Name = getColoredSpanned("["+ sentmsglist.getTo_type()+"]", "#5A5C59");
            sendername.setText(Html.fromHtml(title + " " + Name + " " + l_Name));
        }
        if (sentmsglist.getImage() != null) {
            imageView6.setVisibility(View.VISIBLE);
            Utils.progressImg_two(sentmsglist.getImage(),imageView6,Sent_MessageActivity.this,"");
        }  else {
            imageView6.setVisibility(View.GONE);
        }
    }

    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }

    private void downloadfile(Sent_message_bean message_bean) {

        Dialog mBottomSheetDialog = new Dialog(this);
        final View sheetView = getLayoutInflater().inflate(R.layout.image_lyt, null);
        mBottomSheetDialog.setContentView(sheetView);
        mBottomSheetDialog.setCancelable(true);

        Animation animation;
        animation = AnimationUtils.loadAnimation(this, R.anim.btn_blink_animation);
        ImageView imgShow = (ImageView) sheetView.findViewById(R.id.imgShow);


        ImageView imgDownload = (ImageView) sheetView.findViewById(R.id.imgDownload);
        imgDownload.setVisibility(View.GONE);
        Utils.progressImg_two(message_bean.getImage(),imgShow,Sent_MessageActivity.this,"");
        //  Picasso.with(this).load(message_bean.getImage()).into(imgShow);
        imgDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imgDownload.startAnimation(animation);
                // ErpProgress.showProgressBar(InboxActivity.this, "downloading...");
                //Picasso.with(InboxActivity.this).load(message_bean.getImage()).into(imgTraget);
                //  Toast.makeText(GDSTest.this, "Downloaded successfully", Toast.LENGTH_LONG).show();
            }
        });


        mBottomSheetDialog.show();
        Window window = mBottomSheetDialog.getWindow();
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

    }


    @Override
    public void onDelecallback(Sent_message_bean homeworkbean) {
        commonProgress.show();
        HashMap<String, String> params = new HashMap<>();
        params.put("h_id", homeworkbean.getEs_messagesid());
        params.put("check", "msg");
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.DELETEHW, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                ErpProgress.cancelProgressBar();
                commonProgress.dismiss();
                if (error == null) {
                    int pos = sentmsglist.indexOf(homeworkbean);
                    sentmsglist.remove(pos);
                    adapter.setsntmessageList(sentmsglist);
                    adapter.notifyDataSetChanged();
                    Utils.showSnackBar("Deleted Successfully!", parent);
                    totalRecord.setText("Total Entries:- "+String.valueOf(sentmsglist.size()));
                } else {
                    Log.e("error", error.getMessage() + "");
                    Utils.showSnackBar(error.getMessage(), parent);
                    //  Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_LONG).show();
                    if (error.getStatusCode() == 405) {
                        User.logout();
                        startActivity(new Intent(Sent_MessageActivity.this, LoginActivity.class));
                        finish();
                    }
                }
            }
        }, this, params);
    }

    @Override
    public void onMethod_Image_callback_new_sent(Sent_message_bean message_bean) {

    }

    @Override
    public void onMethod_recording_callback_sent(Sent_message_bean message_bean) {

    }

    @Override
    public void onMethod_pdf_call_back_sent(Sent_message_bean message_bean) {

    }

}
