package com.ultimate.ultimatesmartschool.StaffGatePass;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class GatePassstaffBean {

    static String ID = "id";
    static String REG_ID = "gate_regid";
    static String NAME = "gate_sname";
    static String FNAME = "gate_fname";
    static String PHONE = "gate_mobile";
    static String GDATE = "gate_date";
    static String STATUS = "status";
    static String GTIME = "gate_time";
    static String REASON = "gate_reason";
    static String A_BY = "gate_approved_by";
    static String A_NO = "gate_approved_no";
    static String PROFILE = "gate_profile";

    public String getRowcount() {
        return rowcount;
    }

    public void setRowcount(String rowcount) {
        this.rowcount = rowcount;
    }

    private String rowcount;

    /**
     * id : 1
     * gate_class : 1
     * gate_regid : 1
     * gate_sname : asd
     * gate_fname : qwerty
     * gate_mobile : 1234567890
     * gate_date : 2018-06-09
     * status : Inactive
     * gate_time : 05:25:02
     * gate_reason : wdcfthnjil
     * gate_approved_by :
     * gate_approved_no :
     */

    private String id;

    private String gate_regid;
    private String gate_sname;
    private String gate_fname;
    private String gate_mobile;
    private String gate_date;

    private String gate_time;
    private String gate_reason;

    public String getVerification() {
        return verification;
    }

    public void setVerification(String verification) {
        this.verification = verification;
    }

    private String verification;
    private String post_name;

    public String getPost_name() {
        return post_name;
    }

    public void setPost_name(String post_name) {
        this.post_name = post_name;
    }

    public String getDepart_name() {
        return depart_name;
    }

    public void setDepart_name(String depart_name) {
        this.depart_name = depart_name;
    }

    private String depart_name;

    private String gate_datetime;


    public String getGate_timeout() {
        return gate_timeout;
    }

    public void setGate_timeout(String gate_timeout) {
        this.gate_timeout = gate_timeout;
    }

    public String getStaff_sign() {
        return staff_sign;
    }

    public void setStaff_sign(String staff_sign) {
        this.staff_sign = staff_sign;
    }

    public String getStd_sign() {
        return std_sign;
    }

    public void setStd_sign(String std_sign) {
        this.std_sign = std_sign;
    }

    public String getAdmin_sign() {
        return admin_sign;
    }

    public void setAdmin_sign(String admin_sign) {
        this.admin_sign = admin_sign;
    }

    private String gate_timeout;
    private String staff_sign;
    private String std_sign;
    private String admin_sign;




    public String getGate_datetime() {
        return gate_datetime;
    }

    public void setGate_datetime(String gate_datetime) {
        this.gate_datetime = gate_datetime;
    }
    /**
     * gate_profile :
     */

    private String gate_profile;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }



    public String getGate_regid() {
        return gate_regid;
    }

    public void setGate_regid(String gate_regid) {
        this.gate_regid = gate_regid;
    }

    public String getGate_sname() {
        return gate_sname;
    }

    public void setGate_sname(String gate_sname) {
        this.gate_sname = gate_sname;
    }

    public String getGate_fname() {
        return gate_fname;
    }

    public void setGate_fname(String gate_fname) {
        this.gate_fname = gate_fname;
    }

    public String getGate_mobile() {
        return gate_mobile;
    }

    public void setGate_mobile(String gate_mobile) {
        this.gate_mobile = gate_mobile;
    }

    public String getGate_date() {
        return gate_date;
    }

    public void setGate_date(String gate_date) {
        this.gate_date = gate_date;
    }

    public String getGate_time() {
        return gate_time;
    }

    public void setGate_time(String gate_time) {
        this.gate_time = gate_time;
    }

    public String getGate_reason() {
        return gate_reason;
    }

    public void setGate_reason(String gate_reason) {
        this.gate_reason = gate_reason;
    }



    public static ArrayList<GatePassstaffBean> parseClassArray(JSONArray arrayObj) {
        ArrayList list = new ArrayList<GatePassstaffBean>();
        try {
            for (int i = 0; i < arrayObj.length(); i++) {
                GatePassstaffBean p = parseClassObject(arrayObj.getJSONObject(i));
                if (p != null) {
                    list.add(p);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public static GatePassstaffBean parseClassObject(JSONObject jsonObject) {
        GatePassstaffBean casteObj = new GatePassstaffBean();
        try {
            if (jsonObject.has(ID)) {
                casteObj.setId(jsonObject.getString(ID));
            }
            if (jsonObject.has(NAME) && !jsonObject.getString(NAME).isEmpty() && !jsonObject.getString(NAME).equalsIgnoreCase("null")) {
                casteObj.setGate_sname(jsonObject.getString(NAME));
            }
            if (jsonObject.has(FNAME) && !jsonObject.getString(FNAME).isEmpty() && !jsonObject.getString(FNAME).equalsIgnoreCase("null")) {
                casteObj.setGate_fname(jsonObject.getString(FNAME));
            }

            if (jsonObject.has(REG_ID) && !jsonObject.getString(REG_ID).isEmpty() && !jsonObject.getString(REG_ID).equalsIgnoreCase("null")) {
                casteObj.setGate_regid(jsonObject.getString(REG_ID));
            }

            if (jsonObject.has(PHONE) && !jsonObject.getString(PHONE).isEmpty() && !jsonObject.getString(PHONE).equalsIgnoreCase("null")) {
                casteObj.setGate_mobile(jsonObject.getString(PHONE));
            }
            if (jsonObject.has(GDATE) && !jsonObject.getString(GDATE).isEmpty() && !jsonObject.getString(GDATE).equalsIgnoreCase("null")) {
                casteObj.setGate_date(jsonObject.getString(GDATE));
            }
            if (jsonObject.has(GTIME) && !jsonObject.getString(GTIME).isEmpty() && !jsonObject.getString(GTIME).equalsIgnoreCase("null")) {
                casteObj.setGate_time(jsonObject.getString(GTIME));
            }
            if (jsonObject.has(REASON) && !jsonObject.getString(REASON).isEmpty() && !jsonObject.getString(REASON).equalsIgnoreCase("null")) {
                casteObj.setGate_reason(jsonObject.getString(REASON));
            }

            if (jsonObject.has(PROFILE) && !jsonObject.getString(PROFILE).isEmpty() && !jsonObject.getString(PROFILE).equalsIgnoreCase("null")) {
                casteObj.setGate_profile(jsonObject.getString(PROFILE));
            }

            if (jsonObject.has("rowcount") && !jsonObject.getString("rowcount").isEmpty() && !jsonObject.getString("rowcount").equalsIgnoreCase("null")) {
                casteObj.setRowcount(jsonObject.getString("rowcount"));
            }


            if (jsonObject.has("verification") && !jsonObject.getString("verification").isEmpty() && !jsonObject.getString("verification").equalsIgnoreCase("null")) {
                casteObj.setVerification(jsonObject.getString("verification"));
            }

            if (jsonObject.has("admin_sign") && !jsonObject.getString("admin_sign").isEmpty() && !jsonObject.getString("admin_sign").equalsIgnoreCase("null")) {
                casteObj.setAdmin_sign(jsonObject.getString("admin_sign"));
            } if (jsonObject.has("std_sign") && !jsonObject.getString("std_sign").isEmpty() && !jsonObject.getString("std_sign").equalsIgnoreCase("null")) {
                casteObj.setStd_sign(jsonObject.getString("std_sign"));
            } if (jsonObject.has("staff_sign") && !jsonObject.getString("staff_sign").isEmpty() && !jsonObject.getString("staff_sign").equalsIgnoreCase("null")) {
                casteObj.setStaff_sign(jsonObject.getString("staff_sign"));
            } if (jsonObject.has("gate_timeout") && !jsonObject.getString("gate_timeout").isEmpty() && !jsonObject.getString("gate_timeout").equalsIgnoreCase("null")) {
                casteObj.setGate_timeout(jsonObject.getString("gate_timeout"));
            }

            if (jsonObject.has("depart_name") && !jsonObject.getString("depart_name").isEmpty() && !jsonObject.getString("depart_name").equalsIgnoreCase("null")) {
                casteObj.setDepart_name(jsonObject.getString("depart_name"));
            }
            if (jsonObject.has("post_name") && !jsonObject.getString("post_name").isEmpty() && !jsonObject.getString("post_name").equalsIgnoreCase("null")) {
                casteObj.setPost_name(jsonObject.getString("post_name"));
            }

            if (jsonObject.has("gate_datetime") && !jsonObject.getString("gate_datetime").isEmpty() && !jsonObject.getString("gate_datetime").equalsIgnoreCase("null")) {
                casteObj.setGate_datetime(jsonObject.getString("gate_datetime"));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return casteObj;
    }

    public String getGate_profile() {
        return gate_profile;
    }

    public void setGate_profile(String gate_profile) {
        this.gate_profile = gate_profile;
    }
}
