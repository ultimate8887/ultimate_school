package com.ultimate.ultimatesmartschool.StaffGatePass;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Staff.Postname_Bean;

import java.util.ArrayList;

public class Adapteraddstaf_posts extends BaseAdapter {
    Context context;
    ArrayList<Postname_Bean> post_list;
    LayoutInflater inflter;

    public Adapteraddstaf_posts(Context context, ArrayList<Postname_Bean> post_list) {
        this.context=context;
        this.post_list=post_list;
        inflter = (LayoutInflater.from(context));
    }

    @Override
    public int getCount() {
        return post_list.size()+1;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        convertView = inflter.inflate(R.layout.spinner_lyt_msg, null);
        TextView label = (TextView) convertView.findViewById(R.id.txtText);
        if (position == 0) {
            // Default selected Spinner item
            label.setText("Post");
        } else {
            Postname_Bean classObj = post_list.get(position - 1);
            label.setText(classObj.getName());
        }

        return convertView;

    }
}
