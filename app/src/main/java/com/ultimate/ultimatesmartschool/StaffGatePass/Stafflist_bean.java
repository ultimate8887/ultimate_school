package com.ultimate.ultimatesmartschool.StaffGatePass;

import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.DebugLog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Stafflist_bean {


    private static String ID="id";
    private static String NAME="name";
    private static String FATHERNAME="fathername";
    private static String PHONENO="phonno";
    private static String DEPT="dept_name";

    private static String POST="post";
    private static String IMAGE="image";
    /**
     * id : 3
     * name : Staff Gupta
     * dateofbirth : 1995-08-17
     * dateofjoining : 2018-03-10
     * salary : 15000
     */

    private String id;
    private String name;
    private String dateofbirth;
    private String dateofjoining;
    private String salary;

    public String getDept_name() {
        return dept_name;
    }

    public void setDept_name(String dept_name) {
        this.dept_name = dept_name;
    }

    private String dept_name;
    /**
     * fathername : rk sgasheem
     * phonno : 9856555255
     */

    private String fathername;
    private String phonno;

    public String getPhoneno() {
        return phoneno;
    }

    public void setPhoneno(String phoneno) {
        this.phoneno = phoneno;
    }

    public String getFather() {
        return father;
    }

    public void setFather(String father) {
        this.father = father;
    }

    private String phoneno;
    private String father;
    private String image;
    private String post;

    public String getPost() {
        return post;
    }

    public void setPost(String post) {
        this.post = post;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDateofbirth() {
        return dateofbirth;
    }

    public void setDateofbirth(String dateofbirth) {
        this.dateofbirth = dateofbirth;
    }

    public String getDateofjoining() {
        return dateofjoining;
    }

    public void setDateofjoining(String dateofjoining) {
        this.dateofjoining = dateofjoining;
    }

    public String getSalary() {
        return salary;
    }

    public void setSalary(String salary) {
        this.salary = salary;
    }



    public static ArrayList<Stafflist_bean> parsestaffArray(JSONArray jsonArray) {
        ArrayList<Stafflist_bean> list=new ArrayList<>();
        try {

            for (int i = 0; i < jsonArray.length(); i++) {
                Stafflist_bean p = parsestaffObject(jsonArray.getJSONObject(i));
                if (p != null) {
                    list.add(p);
                }
            }
        } catch (Exception e) {
            DebugLog.printLog("exp",e.getMessage());
            e.printStackTrace();
        }
        return list;
    }

    private static Stafflist_bean parsestaffObject(JSONObject jsonObject) {
        Stafflist_bean casteObj = new Stafflist_bean();
        try {
            if (jsonObject.has(ID)) {
                casteObj.setId(jsonObject.getString(ID));
            }

            if (jsonObject.has(NAME) && !jsonObject.getString(NAME).isEmpty() && !jsonObject.getString(NAME).equalsIgnoreCase("null")) {
                casteObj.setName(jsonObject.getString(NAME));
            }
            if (jsonObject.has(FATHERNAME) && !jsonObject.getString(FATHERNAME).isEmpty() && !jsonObject.getString(FATHERNAME).equalsIgnoreCase("null")) {
                casteObj.setFathername(jsonObject.getString(FATHERNAME));
            }
            if (jsonObject.has(PHONENO) && !jsonObject.getString(PHONENO).isEmpty() && !jsonObject.getString(PHONENO).equalsIgnoreCase("null")) {
                casteObj.setPhonno(jsonObject.getString(PHONENO));
            }
            if (jsonObject.has(DEPT) && !jsonObject.getString(DEPT).isEmpty() && !jsonObject.getString(DEPT).equalsIgnoreCase("null")) {
                casteObj.setDept_name(jsonObject.getString(DEPT));
            }

            if (jsonObject.has(POST) && !jsonObject.getString(POST).isEmpty() && !jsonObject.getString(POST).equalsIgnoreCase("null")) {
                casteObj.setPost(jsonObject.getString(POST));
            }
            if (jsonObject.has(IMAGE) && !jsonObject.getString(IMAGE).isEmpty() && !jsonObject.getString(IMAGE).equalsIgnoreCase("null")) {
                casteObj.setImage(Constants.getImageBaseURL()+jsonObject.getString(IMAGE));
            }



        } catch (JSONException e) {
            e.printStackTrace();
        }
        return casteObj;
    }

    public String getFathername() {
        return fathername;
    }

    public void setFathername(String fathername) {
        this.fathername = fathername;
    }

    public String getPhonno() {
        return phonno;
    }

    public void setPhonno(String phonno) {
        this.phonno = phonno;
    }
}
