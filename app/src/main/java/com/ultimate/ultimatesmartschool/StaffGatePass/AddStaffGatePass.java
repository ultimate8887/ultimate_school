package com.ultimate.ultimatesmartschool.StaffGatePass;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.Request;
import com.github.siyamed.shapeimageview.CircularImageView;

import com.squareup.picasso.Picasso;
import com.ultimate.ultimatesmartschool.Activity.CustomSignatureView;
import com.ultimate.ultimatesmartschool.BeanModule.User;
import com.ultimate.ultimatesmartschool.Homework.StaffListAdapter;
import com.ultimate.ultimatesmartschool.Login.LoginActivity;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Staff.Deparment_bean;
import com.ultimate.ultimatesmartschool.Staff.Postname_Bean;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.CommonProgress;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AddStaffGatePass extends AppCompatActivity {
    @BindView(R.id.spinnerselectdepart)
    Spinner spinnerdepart;
    @BindView(R.id.spinnerselectpost)
    Spinner spinnerpost;
    @BindView(R.id.spinnerselectstff)
    Spinner spinnerstaff;
    String sFather="",sName="",sRegistration="",sMobile="",gName="",gMobile="",gRelation="";
    @BindView(R.id.parent)
    RelativeLayout parent;
    @BindView(R.id.imgBack)
    ImageView back;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    String departmentid,d_name;
    String post_id,p_name;
    String staff_id;
    String from_id;
    String to_type="staff";

    private Bitmap cust_bitmap,staff_bitmap;

    @BindView(R.id.edtName)
    TextView edtName;


    @BindView(R.id.edtFather)
    TextView edtFather;
    @BindView(R.id.profile)
    CircularImageView image;
    @BindView(R.id.edtRegistration)
    TextView edtRegistration;

    @BindView(R.id.edtMobile)
    TextView edtMobile;

    @BindView(R.id.edtTime)
    TextView edtTime;

    @BindView(R.id.edtReason)
    EditText edtReason;

    @BindView(R.id.btnSignIn)
    Button btnSignIn;

    @BindView(R.id.std_img)
    ImageView std_img;
    @BindView(R.id.staff_img)
    ImageView staff_img;

    String am_pm="",date="";
    String img="",img1="",gid="";
    ArrayList<Deparment_bean> department_list=new ArrayList<>();
    ArrayList<Postname_Bean> postlist=new ArrayList<>();
    ArrayList<Stafflist_bean> stafflist=new ArrayList<>();

    @BindView(R.id.firstcard)
    CardView firstcard;
    Adapteraddstaf_posts spinerpost;
    StaffListAdapter_New adapter;
    CommonProgress commonProgress;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_staff_gate_pass);
        ButterKnife.bind(this);
        txtTitle.setText("Apply Gate-Pass");
        commonProgress=new CommonProgress(this);
        java.util.Calendar calendar = java.util.Calendar.getInstance(TimeZone.getDefault());
        Date setdate = calendar.getTime();
        SimpleDateFormat fmtOut = new SimpleDateFormat("dd MMM, yyyy");
        String dateString = fmtOut.format(setdate);
        //adddate.setText(dateString);
        SimpleDateFormat dateFrmOut = new SimpleDateFormat("yyyy-MM-dd");
        date = dateFrmOut.format(setdate);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        from_id= User.getCurrentUser().getId();
        Log.e("fromIid what",from_id);
        fetchdepartment();
        initalView();
    }

    private void initalView() {
        edtFather.setEnabled(false);
        edtMobile.setEnabled(false);
        edtRegistration.setEnabled(false);
        edtName.setEnabled(false);
    }

    private void fetchdepartment() {
        commonProgress.show();
        HashMap<String,String> params=new HashMap<>();
        ApiHandler.apiHit(Request.Method.GET, Constants.getBaseURL() + Constants.deparmrnt, apiCallback, this, params);
    }

    ApiHandler.ApiCallback apiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            commonProgress.dismiss();
            // txtupload.setVisibility(View.VISIBLE);
            if (error == null) {
                try {
                    if (department_list != null) {
                        department_list.clear();
                    }
                    JSONArray jsonArray = jsonObject.getJSONArray("department_data");
                    department_list = Deparment_bean.parseDepartmentArray(jsonArray);
                    final Adapteraddstff_departmnts spinneradapt= new Adapteraddstff_departmnts(AddStaffGatePass.this,department_list);
                    spinnerdepart.setAdapter(spinneradapt);
                    spinnerdepart.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if (i == 0) {
                                if (spinerpost != null) {
                                    postlist.clear();
                                    spinneradapt.notifyDataSetChanged();
                                }

                                if (adapter != null) {
                                    stafflist.clear();
                                    spinneradapt.notifyDataSetChanged();
                                }
                            }

                            if (i > 0) {
                                departmentid = department_list.get(i-1 ).getId();
                                d_name=department_list.get(i-1 ).getName();
                                //fetchSubject(classid);
                                post_id="";
                                staff_id="";
                                fetchPostname(departmentid);


                                edtFather.setText("");
                                edtName.setText("");
                                edtRegistration.setText("");
                                edtMobile.setText("");

                            }else{
                                departmentid="";
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Utils.showSnackBar(error.getMessage(), parent);
                if (error.getStatusCode() == 405) {
                    User.logout();
                    startActivity(new Intent(AddStaffGatePass.this, LoginActivity.class));
                    AddStaffGatePass.this.finish();
                }
            }
        }
    };
    private void fetchPostname(String departmentid) {
        commonProgress.show();
        HashMap<String,String> params=new HashMap<>();
        ApiHandler.apiHit(Request.Method.GET, Constants.getBaseURL() + Constants.Post+Constants.dpartment_id+departmentid, apiCallbackpost, this, params);

    }
    ApiHandler.ApiCallback apiCallbackpost = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            commonProgress.dismiss();
            if (error == null) {
                try {
                    if (postlist != null) {
                        postlist.clear();
                    }
                    JSONArray jsonArray = jsonObject.getJSONArray("post_data");
                    postlist = Postname_Bean.parsePostnsmeArray(jsonArray);
                    spinerpost = new Adapteraddstaf_posts(AddStaffGatePass.this,postlist);
                    spinnerpost.setAdapter(spinerpost);
                    spinnerpost.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                            if (adapter != null) {
                                stafflist.clear();
                                adapter.notifyDataSetChanged();
                            }

                            if (i > 0) {
                                post_id = postlist.get(i-1 ).getId();
                                p_name=postlist.get(i-1 ).getName();
                                staff_id="";
                                fetchstaffname(post_id);

                                edtFather.setText("");
                                edtName.setText("");
                                edtRegistration.setText("");
                                edtMobile.setText("");

                            }else{
                                post_id="";
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Utils.showSnackBar(error.getMessage(), parent);
            }
        }
    };

    public void fetchstaffname(String post_id){
        commonProgress.show();
        HashMap<String,String> params=new HashMap<>();
        params.put("d_id",departmentid);
        params.put("p_id",post_id);
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.STAFF_DETAIL_POST, apiCallbackstaffdetail, this, params);
    }

    ApiHandler.ApiCallback apiCallbackstaffdetail = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            commonProgress.dismiss();
            if (error == null) {
                try {
                    stafflist = Stafflist_bean.parsestaffArray(jsonObject.getJSONArray("staff_data"));
                    adapter = new StaffListAdapter_New(AddStaffGatePass.this, stafflist);
                    spinnerstaff.setAdapter(adapter);
                    spinnerstaff.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                            edtFather.setText("");
                            edtName.setText("");
                            edtRegistration.setText("");
                            edtMobile.setText("");

                            if (i > 0) {
                                firstcard.setVisibility(View.VISIBLE);
                                btnSignIn.setVisibility(View.VISIBLE);
                                getStdData(stafflist.get(i-1));

                            }else{
                                firstcard.setVisibility(View.GONE);
                                btnSignIn.setVisibility(View.INVISIBLE);
                                staff_id="";
                                edtFather.setText("");
                                edtName.setText("");
                                edtRegistration.setText("");
                                edtMobile.setText("");
                                Picasso.get().load(R.drawable.receptionistsss).into(image);
                            }

                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Utils.showSnackBar(error.getMessage(), parent);
                firstcard.setVisibility(View.GONE);
                btnSignIn.setVisibility(View.INVISIBLE);
            }
        }
    };

    private void getStdData(Stafflist_bean studentbean) {


        staff_id = studentbean.getId();

        sFather=studentbean.getFathername();
        sName=studentbean.getName();
        sRegistration=studentbean.getId();
        sMobile=studentbean.getPhonno();

        if (studentbean.getName() != null) {
            String title = getColoredSpanned("Name: ", "#5A5C59");
            String Name = getColoredSpanned(studentbean.getName(), "#000000");
            edtName.setText(Html.fromHtml(title + " " + Name));
        }

        if (studentbean.getFathername() != null) {
            String title = getColoredSpanned("Father's Name: ", "#5A5C59");
            String Name = getColoredSpanned(studentbean.getFathername(), "#000000");
            edtFather.setText(Html.fromHtml(title + " " + Name));
        }

        if (studentbean.getId() != null) {
            String title = getColoredSpanned("Staff Id: ", "#5A5C59");
            String Name = getColoredSpanned(studentbean.getId(), "#000000");
            edtRegistration.setText(Html.fromHtml(title + " " + Name));
        }

        if (studentbean.getPhonno() != null) {
            String title = getColoredSpanned("Contact No: ", "#5A5C59");
            String Name = getColoredSpanned(studentbean.getPhonno(), "#000000");
            edtMobile.setText(Html.fromHtml(title + " " + Name));
        }

        img1=studentbean.getImage();
        if (img1 != null) {
            //Picasso.with(mContext).load(path+stulist.get(position).getGate_profile()).placeholder(R.drawable.stud).into(viewHolder.circleimg);
            Utils.progressImg(img1,image,AddStaffGatePass.this);
        } else {
            Picasso.get().load(R.drawable.stud).into(image);
        }

    }

    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }

    @OnClick(R.id.staff_sign)
    public void staff_sign(){

        //    cust_sign_head.setTextColor(getColor(R.color.colorAccent));

        final Dialog warningDialog = new Dialog(this);
        warningDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        warningDialog.setCancelable(true);

        warningDialog.setContentView(R.layout.signature_dialog);
        warningDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        RelativeLayout btnClose = (RelativeLayout) warningDialog.findViewById(R.id.btnNo);

        final CustomSignatureView signatureView= warningDialog.findViewById(R.id.signature_view);
        //   Button btnClear = (Button) warningDialog.findViewById(R.id.btnClear);
        Button btnYes = (Button) warningDialog.findViewById(R.id.btnOk);
        Button btnClear = (Button) warningDialog.findViewById(R.id.btnClear);
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                warningDialog.dismiss();
            }
        });

        btnClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signatureView.clear();
                staff_img.setVisibility(View.GONE);
            }
        });

        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                warningDialog.dismiss();
                ErpProgress.showProgressBar(AddStaffGatePass.this, "Please wait...");
                staff_bitmap = signatureView.getSignatureBitmap();
                // path = saveImage(bitmap);
                staff_img.setVisibility(View.VISIBLE);
                staff_img.setImageBitmap(staff_bitmap);
                ErpProgress.cancelProgressBar();
            }

        });

        warningDialog.show();


    }


    @OnClick(R.id.student_sign)
    public void student_sign(){

        //    cust_sign_head.setTextColor(getColor(R.color.colorAccent));

        final Dialog warningDialog = new Dialog(this);
        warningDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        warningDialog.setCancelable(true);

        warningDialog.setContentView(R.layout.signature_dialog);
        warningDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        RelativeLayout btnClose = (RelativeLayout) warningDialog.findViewById(R.id.btnNo);

        final CustomSignatureView signatureView= warningDialog.findViewById(R.id.signature_view);
        //   Button btnClear = (Button) warningDialog.findViewById(R.id.btnClear);
        Button btnYes = (Button) warningDialog.findViewById(R.id.btnOk);
        Button btnClear = (Button) warningDialog.findViewById(R.id.btnClear);
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                warningDialog.dismiss();
            }
        });

        btnClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signatureView.clear();
                staff_img.setVisibility(View.GONE);
            }
        });

        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                warningDialog.dismiss();
                ErpProgress.showProgressBar(AddStaffGatePass.this, "Please wait...");
                cust_bitmap = signatureView.getSignatureBitmap();
                // path = saveImage(bitmap);
                std_img.setVisibility(View.VISIBLE);
                std_img.setImageBitmap(cust_bitmap);
                ErpProgress.cancelProgressBar();
            }

        });

        warningDialog.show();

    }


    @OnClick(R.id.edtTime)
    public void edtTimemm(){
// TODO Auto-generated method stub
        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(AddStaffGatePass.this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int hourOfDay, int selectedMinute) {
                // edtTime.setText( selectedHour + ":" + selectedMinute );
                String status = "AM";
                if(hourOfDay > 11)
                {
                    // If the hour is greater than or equal to 12
                    // Then the current AM PM status is PM
                    status = "PM";
                }
                if (status.equalsIgnoreCase("AM")){
                    am_pm="Morning";
                }else {
                    am_pm="Evening";
                }
                // Initialize a new variable to hold 12 hour format hour value
                int hour_of_12_hour_format;
                if(hourOfDay > 11){
                    // If the hour is greater than or equal to 12
                    // Then we subtract 12 from the hour to make it 12 hour format time
                    hour_of_12_hour_format = hourOfDay - 12;
                }
                else {
                    hour_of_12_hour_format = hourOfDay;
                }
                String minute="";
                if (selectedMinute==0){
                    minute="00";
                }else if (selectedMinute==1){
                    minute="01";
                }else if (selectedMinute==2){
                    minute="02";
                }else if (selectedMinute==3){
                    minute="03";
                }else if (selectedMinute==4){
                    minute="04";
                }else if (selectedMinute==5){
                    minute="05";
                }else if (selectedMinute==6){
                    minute="06";
                }else if (selectedMinute==7){
                    minute="07";
                }else if (selectedMinute==8){
                    minute="08";
                }else if (selectedMinute==9){
                    minute="09";
                }else {
                    minute= String.valueOf(selectedMinute);
                }

                // Display the 12 hour format time in app interface
                if (hour_of_12_hour_format==0){
                    hour_of_12_hour_format=12;
                    edtTime.setText( hour_of_12_hour_format + " : " + minute + " : " + status );
                }else {
                    edtTime.setText( hour_of_12_hour_format + " : " + minute + " : " + status );
                }


            }
        }, hour, minute, false);//Yes 24 hour time
        mTimePicker.setTitle("Select Time");
        mTimePicker.show();
    }


    @OnClick(R.id.btnSignIn)
    public void btnSignInnn() {
        if (edtName.getText().toString().isEmpty()) {
            Toast.makeText(getApplicationContext(), "Kindly Select Staff", Toast.LENGTH_LONG).show();
        } else if (edtTime.getText().toString().equalsIgnoreCase("Set Check Out Time")) {
            Toast.makeText(getApplicationContext(), "Set Check Out Time", Toast.LENGTH_LONG).show();
        }else {
            commonProgress.show();
            HashMap<String, String> params = new HashMap<>();
            //  params.put("gate_class", classid);

            params.put("d_id",departmentid);
            params.put("p_id",post_id);

            params.put("depart_name",d_name);
            params.put("post_name",p_name);

            params.put("gate_regid", sRegistration);
            params.put("gate_sname", sName);

            params.put("gate_fname", sFather);
            params.put("gate_mobile", sMobile);

            params.put("gate_timeout", edtTime.getText().toString());

            params.put("gate_datetime", am_pm);
            params.put("gate_date", date);
            params.put("gate_reason", edtReason.getText().toString());
            //   params.put("staff_rating", staff_rate);

//
//            Log.e("param",departmentid+"\n"+post_id+"\n"+d_name+"\n"+p_name+"\n"+sRegistration+"\n"+sName
//                    +"\n"+sFather+"\n"+sMobile+"\n"+am_pm+"\n"+date+"\n"+edtReason.getText().toString()+"\n"+
//                    edtTime.getText().toString());

            if (cust_bitmap != null) {
                String encoded = Utils.encodeToBase64(cust_bitmap, Bitmap.CompressFormat.JPEG, 90);
                encoded = String.format("data:image/jpeg;base64,%s", encoded);
                params.put("std_sign", encoded);
            }
            if (staff_bitmap != null) {
                String encoded = Utils.encodeToBase64(staff_bitmap, Bitmap.CompressFormat.JPEG, 90);
                encoded = String.format("data:image/jpeg;base64,%s", encoded);
                params.put("staff_sign", encoded);
            }

            ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.GATESTAFFPASS_URL, apisendmsgCallback, this, params);

        }
    }
    ApiHandler.ApiCallback apisendmsgCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            commonProgress.dismiss();
            if (error == null) {
                Toast.makeText(getApplicationContext(),"Generate Successfully",Toast.LENGTH_LONG).show();
                // Utility.showSnackBar(jsonObject.getString("msg"), parent);
                finish();
            } else {
                Log.e("error", error.getMessage() + "");
                Utils.showSnackBar(error.getMessage(), parent);
                // Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_LONG).show();

            }
        }
    };



}
