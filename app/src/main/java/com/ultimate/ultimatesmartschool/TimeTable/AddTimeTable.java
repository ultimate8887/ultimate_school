package com.ultimate.ultimatesmartschool.TimeTable;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ClipData;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.bumptech.glide.Glide;

import com.kroegerama.imgpicker.BottomSheetImagePicker;

import com.ultimate.ultimatesmartschool.BeanModule.ClassBean;
import com.ultimate.ultimatesmartschool.BeanModule.SectionBean;
import com.ultimate.ultimatesmartschool.BeanModule.SubjectBeancls;
import com.ultimate.ultimatesmartschool.BeanModule.User;
import com.ultimate.ultimatesmartschool.Examination.ClassAdapter;
import com.ultimate.ultimatesmartschool.Homework.AdapterOfSubjectList;
import com.ultimate.ultimatesmartschool.Homework.AddHomeworkActivity;
import com.ultimate.ultimatesmartschool.Homework.Homeworkadapter;
import com.ultimate.ultimatesmartschool.Homework.Homeworkbean;
import com.ultimate.ultimatesmartschool.Homework.SectionnewAdapter;
import com.ultimate.ultimatesmartschool.Login.LoginActivity;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.CommonProgress;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;
import com.ultimate.ultimatesmartschool.Utility.ErpProgressVert;
import com.ultimate.ultimatesmartschool.Utility.FileUtils;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.apptik.widget.multiselectspinner.BaseMultiSelectSpinner;
import io.apptik.widget.multiselectspinner.MultiSelectSpinner;

public class AddTimeTable extends AppCompatActivity implements BottomSheetImagePicker.OnImagesSelectedListener {
    ArrayList<ClassBean> classList = new ArrayList<>();
    @BindView(R.id.parent)
    RelativeLayout parent;
    @BindView(R.id.spinnerClass) Spinner spinnerClass;
//    @BindView(R.id.multiselectSpinnerclass)
//    MultiSelectSpinner multiselectSpinnerclass;
    String classid = "";
    private Homeworkadapter adapter;
    ArrayList<Homeworkbean> hwList = new ArrayList<>();
    private LinearLayoutManager layoutManager;
    CommonProgress commonProgress;
    private int loaded = 0;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    List<String> classidsel;
    @BindView(R.id.spinnersection)
    Spinner spinnersection;
    ArrayList<SectionBean> sectionList = new ArrayList<>();
    String sectionid="";
    String sectionname;
    private int type;
    private Bitmap hwbitmap;
    @BindView(R.id.imageView6)
    ImageView imageView6;
    private final int LOAD_FILE_RESULTS = 1000;
    String fileBase64 = null;

    @BindView(R.id.textnote)TextView textnote;
    @BindView(R.id.addpdf)TextView addpdf;
    @BindView(R.id.addimage)TextView addimage;

    int PICK_IMAGE_MULTIPLE = 11;
    String imageEncoded;
    List<String> imagesEncodedList;

    //@BindView(R.id.testbtn)Button testbtn;
    @BindView(R.id.commenthmwrk)
    EditText commenthmwrk;
    private ViewGroup imageContainer;
    @BindView(R.id.scrollView)
    HorizontalScrollView scrollView;
    ArrayList<String> image;
    List<? extends Uri> image2;
    String encoded;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_time_table);
        ButterKnife.bind(this);
        commonProgress=new CommonProgress(this);
        imageContainer = findViewById(R.id.imageContainer);
        classidsel = new ArrayList<>();
        layoutManager = new LinearLayoutManager(AddTimeTable.this);
        //  recyclerview.setLayoutManager(layoutManager);
        //  adapter = new Homeworkadapter(hwList, AddHomeworkActivity.this, this);
        //recyclerview.setAdapter(adapter);
        fetchClass();
        fetchSection();
        txtTitle.setText("Add Timetable");
    }
    @OnClick(R.id.imgBack)
    public void onback() {
        finish();
    }
    private void fetchClass() {
        if (loaded == 0) {
            commonProgress.show();
        }
        loaded++;

        HashMap<String, String> params = new HashMap<String, String>();
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.CLASSLIST_URL, classapiCallback, this, params);
    }

    ApiHandler.ApiCallback classapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            commonProgress.dismiss();
            if (error == null) {
                try {
                    classList = ClassBean.parseClassArray(jsonObject.getJSONArray(Constants.CLASSDATA));
                    ClassAdapter adapter = new ClassAdapter(getApplicationContext(), classList);
                    spinnerClass.setAdapter(adapter);
                    spinnerClass.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if (i > 0) {
                                classid = classList.get(i - 1).getId();
                                fetchSection();
                            }

                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_LONG).show();
                // Utils.showSnackBar(error.getMessage(), view.findViewById(R.id.parent));
            }
        }
    };

    private void fetchSection() {
        HashMap<String, String> params = new HashMap<String, String>();
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.SECTIONLIST_URL, sectionapiCallback, this, params);
    }

    ApiHandler.ApiCallback sectionapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            if (error == null) {
                try {
                    sectionList = SectionBean.parseCommonArray(jsonObject.getJSONArray("hostelroom_data"));
                    SectionnewAdapter adapter = new SectionnewAdapter(AddTimeTable.this, sectionList);
                    spinnersection.setAdapter(adapter);
                    spinnersection.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if (i > 0) {
                                sectionid = sectionList.get(i - 1).getSection_id();
                                sectionname = sectionList.get(i - 1).getSection_name();
                                // fetchHomework();
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Utils.showSnackBar(error.getMessage(), parent);
                if (error.getStatusCode() == 405) {
                    User.logout();
                    startActivity(new Intent(AddTimeTable.this, LoginActivity.class));
                    finish();
                }
            }
        }
    };
    @SuppressLint("ResourceType")
    @RequiresApi(api = Build.VERSION_CODES.M)
    @OnClick(R.id.addimage)
    public void click() {
        type = 1;
        CallCropAct();
    }

    @SuppressLint("ResourceType")
    @RequiresApi(api = Build.VERSION_CODES.M)
    public void CallCropAct() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.UPSIDE_DOWN_CAKE) { // Android 14+ (API 34)
            //Toast.makeText(this, "Multi Images", Toast.LENGTH_LONG).show();
            // initialising intent
            Intent intent = new Intent();
            // setting type to select to be image
            intent.setType("image/*");
            // allowing multiple image to be selected
            intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(Intent.createChooser(intent, "Select Multiple Images"), PICK_IMAGE_MULTIPLE);

        } else if (Build.VERSION.SDK_INT > Build.VERSION_CODES.S) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_MEDIA_IMAGES)
                    != PackageManager.PERMISSION_GRANTED) {

                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_MEDIA_IMAGES)) {
                    Toast.makeText(this, "Please provide storage permission from app settings", Toast.LENGTH_LONG).show();
                } else {

                    ActivityCompat.requestPermissions(this,
                            new String[]{Manifest.permission.READ_MEDIA_IMAGES},
                            1);
                }
            } else {
                //Toast.makeText(this, "Multi Images", Toast.LENGTH_LONG).show();
                // initialising intent
                Intent intent = new Intent();
                // setting type to select to be image
                intent.setType("image/*");
                // allowing multiple image to be selected
                intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Multiple Images"), PICK_IMAGE_MULTIPLE);
            }

        } else {

            if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {

                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    Toast.makeText(this, "Please provide storage permission from app settings", Toast.LENGTH_LONG).show();
                } else {

                    ActivityCompat.requestPermissions(this,
                            new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                            1);

                }
            } else {
                new BottomSheetImagePicker.Builder(getString(R.xml.provider_paths))
                        .multiSelect(1, 6)
                        .multiSelectTitles(
                                R.plurals.pick_multi,
                                R.plurals.pick_multi_more,
                                R.string.pick_multi_limit
                        )
                        .peekHeight(R.dimen.peekHeight)
                        .columnSize(R.dimen.columnSize)
                        .requestTag("multi")
                        .show(getSupportFragmentManager(), null);
                // showFileChooser();
            }

        }

    }


    @Override
    public void onImagesSelected(List<? extends Uri> list, String s) {
        commonCode(list);
    }

    private void commonCode(List<? extends Uri> list) {
        imageView6.setVisibility(View.GONE);
        scrollView.setVisibility(View.VISIBLE);
        imageContainer.removeAllViews();
        image = new ArrayList<>();
        image2 = list;
        for (Uri uri : list) {
            ImageView iv = (ImageView) LayoutInflater.from(this).inflate(R.layout.scrollitem_image, imageContainer, false);
            imageContainer.addView(iv);
            Glide.with(this).load(uri).into(iv);
            hwbitmap = Utils.getBitmapFromUri(AddTimeTable.this, uri, 2048);
            Log.e("hwbitmapup", String.valueOf(hwbitmap));
            encoded = Utils.encodeToBase64(hwbitmap, Bitmap.CompressFormat.JPEG, 50);
            // encoded = String.format("data:image/jpeg;base64,%s", encoded);
            image.add(encoded);
        }
        Log.e("hwbitmapupdown", String.valueOf(hwbitmap));
        Log.e("image2", String.valueOf(image2));
        textnote.setVisibility(View.VISIBLE);
        textnote.setText("Image: You have selected "+String.valueOf(image.size())+" images" );
    }



    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == PICK_IMAGE_MULTIPLE && null != data) {
                // Get the Image from data

                String[] filePathColumn = { MediaStore.Images.Media.DATA };
                imagesEncodedList = new ArrayList<String>();
                if(data.getData()!=null){

                    Uri mImageUri=data.getData();

                    // Get the cursor
                    Cursor cursor = getContentResolver().query(mImageUri,
                            filePathColumn, null, null, null);
                    // Move to first row
                    cursor.moveToFirst();

                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    imageEncoded  = cursor.getString(columnIndex);
                    cursor.close();

                } else {
                    if (data.getClipData() != null) {
                        ClipData mClipData = data.getClipData();
                        ArrayList<Uri> mArrayUri = new ArrayList<Uri>();
                        for (int i = 0; i < mClipData.getItemCount(); i++) {

                            ClipData.Item item = mClipData.getItemAt(i);
                            Uri uri = item.getUri();
                            mArrayUri.add(uri);
                            // Get the cursor
                            Cursor cursor = getContentResolver().query(uri, filePathColumn, null, null, null);
                            // Move to first row
                            cursor.moveToFirst();

                            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                            imageEncoded  = cursor.getString(columnIndex);
                            imagesEncodedList.add(imageEncoded);
                            cursor.close();

                        }
                        commonCode(mArrayUri);

                    }
                }
            } else {
                if (type == 1) {
                    Uri imageUri = data.getData();
                    try {
                        String path = FileUtils.getRealPath(this, imageUri);
                        hwbitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), imageUri);
                        fileBase64 = null;
                        textnote.setVisibility(View.VISIBLE);
                        textnote.setText("Path: " + path);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    imageView6.setImageBitmap(hwbitmap);
//                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//                        add.setImageDrawable(getResources().getDrawable(R.drawable.ic_done_gr, getApplicationContext().getTheme()));
//                    } else {
//                        add.setImageDrawable(getResources().getDrawable(R.drawable.ic_done_gr));
//                    }
                } else {
//                    uri = data.getData();
//                    // String path = Utils.getPath(this, uri);
//
//                    if (uri != null) {
//                        try {
//                            ContentResolver contentResolver = getContentResolver();
//                            InputStream inputStream = contentResolver.openInputStream(uri);
//                            File file = new File(getCacheDir(), "temp.pdf");
//                            OutputStream outputStream = new FileOutputStream(file);
//                            byte[] buffer = new byte[4 * 1024]; // or other buffer size
//                            int read;
//
//                            while ((read = inputStream.read(buffer)) != -1) {
//                                outputStream.write(buffer, 0, read);
//                            }
//
//                            outputStream.flush();
//                            outputStream.close();
//                            inputStream.close();
//
//                            String path = file.getAbsolutePath();
//                            long fileSize = file.length();
//                            size = formatSize(fileSize);
//                            if (fileSize > 18 * 1024 * 1024) { // 18 MB
//                                Toast.makeText(getApplicationContext(),
//                                        "Sorry, Your selected file size is too large (" + size + ")." +
//                                                "Please select a file with a size of less than 18 MB.", Toast.LENGTH_LONG).show();
//                            } else {
//                                fileBase64 = Utility.encodeFileToBase64Binary(path);
//                                hwbitmap = null;
//                                showPdfFromUri(uri, path);
//                                textnote.setText("Pdf Size: " + "" + "" + size);
//                                textnote.setVisibility(View.VISIBLE);
//                                imageView6.setImageResource(R.drawable.pdfbig);
//                                scrollView.setVisibility(View.GONE);
//                                imageView6.setVisibility(View.VISIBLE);
//                                Toast.makeText(this, "File Picked...", Toast.LENGTH_SHORT).show();
//                            }
//                        } catch (IOException e) {
//                            e.printStackTrace();
//                        }
//                    }
                }
            }

        }else {
            Toast.makeText(this, "You haven't picked file", Toast.LENGTH_SHORT).show();
        }
    }

    public static boolean checkPermission(Context mContext, String Permission) {
        int result = ContextCompat.checkSelfPermission(mContext, Permission);
        if (result == PackageManager.PERMISSION_GRANTED) {

            return true;
        } else {
            return false;
        }
    }
    @OnClick(R.id.imageView6)
    public void imagecheck() {

        if (hwbitmap != null) {
            final Dialog EventDialog = new Dialog(this);
            EventDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            EventDialog.setCancelable(true);
            EventDialog.setContentView(R.layout.image_lyt);
            EventDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            ImageView imgShow = (ImageView) EventDialog.findViewById(R.id.imgShow);
            ImageView imgDownload = (ImageView) EventDialog.findViewById(R.id.imgDownload);
            imgDownload.setVisibility(View.GONE);
            imgShow.setImageBitmap(hwbitmap);
            // Picasso.with(this).load(String.valueOf(hwbitmap)).placeholder(getResources().getDrawable(R.drawable.home)).into(imgShow);

            EventDialog.show();
            Window window = EventDialog.getWindow();
            window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        } else{
            Toast.makeText(getApplicationContext(),"Please Select HomeWork Image",Toast.LENGTH_LONG).show();
        }
    }



    @OnClick(R.id.addpdf)
    public void browseDocuments() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                Toast.makeText(this,"Please provide storage permission from app settings",Toast.LENGTH_LONG).show();
            } else {

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        1);

            }
        }else{
            callPicker();
            // showFileChooser();
        }


    }


    public void callPicker(){
        type = 2;

        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("application/pdf");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        //startActivityForResult(intent, SAVE_REQUEST_CODE);
        startActivityForResult(Intent.createChooser(intent, "ChooseFile"), LOAD_FILE_RESULTS);
    }
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 1: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    callPicker();

                } else {
                    // permission denied, boo! Disable the
                    Toast.makeText(this, "Please provide storage permission from app settings, as it is mandatory to access your files!", Toast.LENGTH_LONG).show();
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request.
        }
    }
    @OnClick(R.id.button3)
    public void save() {
        if (classid.equalsIgnoreCase("")) {
            Toast.makeText(this,"Kindly select class",Toast.LENGTH_SHORT).show();

        } else if (sectionid.equalsIgnoreCase("")) {
            Toast.makeText(this,"Kindly select class section",Toast.LENGTH_SHORT).show();
        }  else if (hwbitmap==null && fileBase64==null) {
            Toast.makeText(this,"Kindly add Image file",Toast.LENGTH_SHORT).show();
        } else {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
        builder1.setMessage("Are you sure you want to submit this Timetable? (Before Submit! Please check Timetable Class) ");
        builder1.setCancelable(false);
        builder1.setPositiveButton(
                "Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                            commonProgress.show();
                            HashMap<String, String> params = new HashMap<String, String>();
                            if (hwbitmap != null) {
//                                String encoded = Utils.encodeToBase64(hwbitmap, Bitmap.CompressFormat.JPEG, 50);
//                                encoded = String.format("data:image/jpeg;base64,%s", encoded);
//                                params.put("image", encoded);
                                params.put("image", String.valueOf(image));
                            }
                            if (fileBase64 != null) {
                                params.put("file", fileBase64);
                                Log.e("file", fileBase64);
                            }

                         //   params.put("comment", commenthmwrk.getText().toString());
                            params.put("class_id", classid);
                            params.put("section_id", sectionid);


                            params.put("user_id", User.getCurrentUser().getId());
                            ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.AddNewTimetableURL, apiCallback, AddTimeTable.this, params);

                        }


                });
        builder1.setNegativeButton(
                "No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
        AlertDialog alert11 = builder1.create();
        alert11.show();

        }
    }



    ApiHandler.ApiCallback apiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgressVert.cancelProgressBar();
            commonProgress.dismiss();
            if (error == null) {

                    Utils.showSnackBar("Added Successfully!", parent);
                    finish();

            } else {
                Log.e("error", error.getMessage() + "");
                Utils.showSnackBar(error.getMessage(), parent);

                if (error.getStatusCode() == 405) {
                    User.logout();
                    startActivity(new Intent(AddTimeTable.this, LoginActivity.class));
                    finish();
                }
            }
        }
    };

}
