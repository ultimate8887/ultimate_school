package com.ultimate.ultimatesmartschool.TimeTable;

import com.ultimate.ultimatesmartschool.DateSheet.DatesheetBean;
import com.ultimate.ultimatesmartschool.Homework.Homeworkbean;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.DebugLog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Timetablebean {

    private static String ID="id";
    private static String IMAGE="image";
    private static String COMMENT="comment";
    private static String H_DATE="date";


    public ArrayList<String> getImage() {
        return image;
    }

    public void setImage(ArrayList<String> image) {
        this.image = image;
    }

    private ArrayList<String> image;

    /**
     * id : 2
     * comment : asfgsdfgdgegdfgdfgfg
     * image : office_admin/images/homework/hw_03272018_080342.jpg
     * date : 2018-03-27
     * class : 10
     */

    private String id;
    private String comment;
    private String date;
    private String section;
    private String section_name;
    private String pdf_file;
    private String recordingsfile;
    private String subject_name;

    public String getStaff_name() {
        return staff_name;
    }

    public void setStaff_name(String staff_name) {
        this.staff_name = staff_name;
    }

    public String getStaff_image() {
        return staff_image;
    }

    public void setStaff_image(String staff_image) {
        this.staff_image = staff_image;
    }

    public String getPost_name() {
        return post_name;
    }

    public void setPost_name(String post_name) {
        this.post_name = post_name;
    }

    private String staff_name;
    private String staff_image;
    private String post_name;

    public String getSubject_name() {
        return subject_name;
    }

    public void setSubject_name(String subject_name) {
        this.subject_name = subject_name;
    }

    public String getSection_name() {
        return section_name;
    }

    public void setSection_name(String section_name) {
        this.section_name = section_name;
    }

    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }

    public String getPdf_file() {
        return pdf_file;
    }

    public void setPdf_file(String pdf_file) {
        this.pdf_file = pdf_file;
    }

    public String getRecordingsfile() {
        return recordingsfile;
    }

    public void setRecordingsfile(String recordingsfile) {
        this.recordingsfile = recordingsfile;
    }

    /**
     * class_id : 10
     * class_name : 6TH
     */

    private String class_id;
    private String class_name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }


    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getClass_id() {
        return class_id;
    }

    public void setClass_id(String class_id) {
        this.class_id = class_id;
    }

    public String getClass_name() {
        return class_name;
    }

    public void setClass_name(String class_name) {
        this.class_name = class_name;
    }

    public static ArrayList<Timetablebean> parseHWArray(JSONArray arrayObj) {
        ArrayList<Timetablebean> list = new ArrayList<Timetablebean>();
        try {

            for (int i = 0; i < arrayObj.length(); i++) {
                Timetablebean p = parseHWObject(arrayObj.getJSONObject(i));
                if (p != null) {
                    list.add(p);
                }
            }
        } catch (Exception e) {
            DebugLog.printLog("exp",e.getMessage());
            e.printStackTrace();
        }
        return list;
    }

    public static Timetablebean parseHWObject(JSONObject jsonObject) {
        Timetablebean casteObj = new Timetablebean();
        try {
            if (jsonObject.has(ID)) {
                casteObj.setId(jsonObject.getString(ID));
            }
            if (jsonObject.has(IMAGE) && !jsonObject.getString(IMAGE).isEmpty() && !jsonObject.getString(IMAGE).equalsIgnoreCase("null")) {
                JSONArray img_arr = jsonObject.getJSONArray(IMAGE);
                ArrayList<String> img_arrs = new ArrayList<>();
                for(int i=0;i<img_arr.length();i++){
                    img_arrs.add(Constants.getImageBaseURL()+img_arr.getString(i));
                }
                casteObj.setImage(img_arrs);
            }
            if (jsonObject.has(COMMENT) && !jsonObject.getString(COMMENT).isEmpty() && !jsonObject.getString(COMMENT).equalsIgnoreCase("null")) {
                casteObj.setComment(jsonObject.getString(COMMENT));
            }
            if (jsonObject.has(H_DATE) && !jsonObject.getString(H_DATE).isEmpty() && !jsonObject.getString(H_DATE).equalsIgnoreCase("null")) {
                casteObj.setDate(jsonObject.getString(H_DATE));
            }
            if (jsonObject.has("section") && !jsonObject.getString("section").isEmpty() && !jsonObject.getString("section").equalsIgnoreCase("null")) {
                casteObj.setSection(jsonObject.getString("section"));
            }
            if (jsonObject.has("section_name") && !jsonObject.getString("section_name").isEmpty() && !jsonObject.getString("section_name").equalsIgnoreCase("null")) {
                casteObj.setSection_name(jsonObject.getString("section_name"));
            }
            if (jsonObject.has("class_name") && !jsonObject.getString("class_name").isEmpty() && !jsonObject.getString("class_name").equalsIgnoreCase("null")) {
                casteObj.setClass_name(jsonObject.getString("class_name"));
            }

            if (jsonObject.has("pdf_file") && !jsonObject.getString("pdf_file").isEmpty() && !jsonObject.getString("pdf_file").equalsIgnoreCase("null")) {
                casteObj.setPdf_file(Constants.getImageBaseURL()+jsonObject.getString("pdf_file"));
            }
            if (jsonObject.has("recordingsfile") && !jsonObject.getString("recordingsfile").isEmpty() && !jsonObject.getString("recordingsfile").equalsIgnoreCase("null")) {
                casteObj.setRecordingsfile(Constants.getImageBaseURL()+jsonObject.getString("recordingsfile"));
            }
            if (jsonObject.has("subject_name") && !jsonObject.getString("subject_name").isEmpty() && !jsonObject.getString("subject_name").equalsIgnoreCase("null")) {
                casteObj.setSubject_name(jsonObject.getString("subject_name"));
            }

            if (jsonObject.has("staff_name") && !jsonObject.getString("staff_name").isEmpty() && !jsonObject.getString("staff_name").equalsIgnoreCase("null")) {
                casteObj.setStaff_name(jsonObject.getString("staff_name"));
            }
            if (jsonObject.has("staff_image") && !jsonObject.getString("staff_image").isEmpty() && !jsonObject.getString("staff_image").equalsIgnoreCase("null")) {
                casteObj.setStaff_image(Constants.getImageBaseURL()+jsonObject.getString("staff_image"));
            }
            if (jsonObject.has("post_name") && !jsonObject.getString("post_name").isEmpty() && !jsonObject.getString("post_name").equalsIgnoreCase("null")) {
                casteObj.setPost_name(jsonObject.getString("post_name"));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return casteObj;
    }
}
