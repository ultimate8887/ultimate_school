package com.ultimate.ultimatesmartschool.BeanModule;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.ultimate.ultimatesmartschool.UltimateSchoolApp;
import com.ultimate.ultimatesmartschool.Utility.Constants;

import org.json.JSONException;
import org.json.JSONObject;

public class User {

    private static User sInstance;
    public static String PREFRENCES_USER = "user";
    private static String ID = "id";
    private static String EMAIL = "email";
    private static String NAME = "username";
    private static String PHONENO = "phoneno";
    private static String TYPE = "type";
    private static String FIRSTNAME = "firstname";
    private static String LASTNAME = "lastname";
    private static String SCHOOLDATA = "school_data";
    private static String CLASS_ID = "class_id";
    /**
     * id : 1
     * type : super
     * firstname : Admin
     * lastname : Admin
     * username : admin
     * email : admin@gmail.com
     * phoneno : 12345556
     */
    private String class_id;
    private String id;
    private String type;
    private String firstname;
    private String lastname;
    private String username;
    private String email;
    private String phoneno;
    private SchoolBean schoolData;
    /**
     * school_timming : no
     */

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }

    private String address;
    private String profile;
    public SchoolBean getSchoolData() {
        return schoolData;
    }

    public void setSchoolData(SchoolBean schoolData) {
        this.schoolData = schoolData;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneno() {
        return phoneno;
    }

    public void setPhoneno(String phoneno) {
        this.phoneno = phoneno;
    }

    public static void setCurrentUser(JSONObject jsonObject) {
        User user = getCurrentUser();
        try {

            if (user == null) {
                user = new User();
            }
            if (jsonObject.has(ID)) {
                user.setId(jsonObject.getString(ID));
            }
            if (jsonObject.has(EMAIL) && !jsonObject.getString(EMAIL).isEmpty() && !jsonObject.getString(EMAIL).equalsIgnoreCase("null")) {
                user.setEmail(jsonObject.getString(User.EMAIL));
            }
            if (jsonObject.has(NAME) && !jsonObject.getString(NAME).isEmpty() && !jsonObject.getString(NAME).equalsIgnoreCase("null")) {
                user.setUsername(jsonObject.getString(User.NAME));
            }
            if (jsonObject.has(PHONENO) && !jsonObject.getString(PHONENO).isEmpty() && !jsonObject.getString(PHONENO).equalsIgnoreCase("null")) {
                user.setPhoneno(jsonObject.getString(PHONENO));
            }
            if (jsonObject.has(TYPE) && !jsonObject.getString(TYPE).isEmpty() && !jsonObject.getString(TYPE).equalsIgnoreCase("null")) {
                user.setType(jsonObject.getString(TYPE));
            }
            if (jsonObject.has(FIRSTNAME) && !jsonObject.getString(FIRSTNAME).isEmpty() && !jsonObject.getString(FIRSTNAME).equalsIgnoreCase("null")) {
                user.setFirstname(jsonObject.getString(FIRSTNAME));
            }
            if (jsonObject.has(LASTNAME) && !jsonObject.getString(LASTNAME).isEmpty() && !jsonObject.getString(LASTNAME).equalsIgnoreCase("null")) {
                user.setLastname(jsonObject.getString(LASTNAME));
            }

            if (jsonObject.has(CLASS_ID) && !jsonObject.getString(CLASS_ID).isEmpty() && !jsonObject.getString(CLASS_ID).equalsIgnoreCase("null")) {
                user.setClass_id(jsonObject.getString(CLASS_ID));
            }

            if (jsonObject.has(SCHOOLDATA)) {
                user.setSchoolData(SchoolBean.parseSchoolObject(jsonObject.getJSONObject(SCHOOLDATA)));
            }
            if (jsonObject.has("address") && !jsonObject.getString("address").isEmpty() && !jsonObject.getString("address").equalsIgnoreCase("null")) {
                user.setAddress(jsonObject.getString("address"));
            }

            if (jsonObject.has("profile") && !jsonObject.getString("profile").isEmpty() && !jsonObject.getString("profile").equalsIgnoreCase("null")) {
                user.setProfile(Constants.getImageBaseURL()+jsonObject.getString("profile"));
            }
            Gson gson = new Gson();
            String userString = gson.toJson(user, User.class);
            SharedPreferences preferences = UltimateSchoolApp.getInstance().getSharedPreferences(Constants.PREF_NAME, Context.MODE_PRIVATE);
            preferences.edit().putString(PREFRENCES_USER, userString).commit();

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static User getCurrentUser() {
        if (sInstance == null) {
            SharedPreferences preferences = UltimateSchoolApp.getInstance().getSharedPreferences(Constants.PREF_NAME, Context.MODE_PRIVATE);
            String userString = preferences.getString(PREFRENCES_USER, null);
            if (userString != null) {
                Gson gson = new Gson();
                sInstance = gson.fromJson(userString, User.class);
                return sInstance;
            } else {
                return null;
            }
        } else {
            return sInstance;
        }
    }
    public static void logout() {
        sInstance = null;
        SharedPreferences pref = UltimateSchoolApp.getInstance().getSharedPreferences(Constants.PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.remove(PREFRENCES_USER);
        editor.commit();
    }


    public String getClass_id() {
        return class_id;
    }

    public void setClass_id(String class_id) {
        this.class_id = class_id;
    }
}
