package com.ultimate.ultimatesmartschool.BeanModule;

import com.ultimate.ultimatesmartschool.Utility.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Studentbean {

    public String getDate_time() {
        return date_time;
    }

    public void setDate_time(String date_time) {
        this.date_time = date_time;
    }

    private String date_time;
    private String es_housesid;

    public String getEs_housesid() {
        return es_housesid;
    }

    public void setEs_housesid(String es_housesid) {
        this.es_housesid = es_housesid;
    }

    public String getEs_housename() {
        return es_housename;
    }

    public void setEs_housename(String es_housename) {
        this.es_housename = es_housename;
    }

    private String es_housename;

    /**
     * id : 1
     * name : aniket garg
     */
    private static String ID = "id";
    private static String NAME = "name";
    private static String F_NAME = "father_name";
    private static String CLASS_NAME = "class_name";
    private static String PHONE = "mobile";
    private static String PROFILE = "profile";
    private static String CLASS_ID = "class_id";
    private static String R_ID = "r_id";
    private static String SCHOOLDATA = "school_data";
    private static String LAT = "lat";
    private static String LNG = "lng";
    private static String CLASSNAME = "class_name";

    private static String EMAIL = "email";
    private static String PHONENO = "phoneno";

    private String jan_id;
    private String jan_month;
    private String jan_paid;

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    private String height;
    private String weight;

    public String getSection_id() {
        return section_id;
    }

    public void setSection_id(String section_id) {
        this.section_id = section_id;
    }

    private String section_id;

    public String getJan_id() {
        return jan_id;
    }

    public void setJan_id(String jan_id) {
        this.jan_id = jan_id;
    }

    public String getJan_month() {
        return jan_month;
    }

    public void setJan_month(String jan_month) {
        this.jan_month = jan_month;
    }

    public String getJan_paid() {
        return jan_paid;
    }

    public void setJan_paid(String jan_paid) {
        this.jan_paid = jan_paid;
    }

    public String getFeb_id() {
        return feb_id;
    }

    public void setFeb_id(String feb_id) {
        this.feb_id = feb_id;
    }

    public String getFeb_month() {
        return feb_month;
    }

    public void setFeb_month(String feb_month) {
        this.feb_month = feb_month;
    }

    public String getFeb_paid() {
        return feb_paid;
    }

    public void setFeb_paid(String feb_paid) {
        this.feb_paid = feb_paid;
    }

    public String getMar_id() {
        return mar_id;
    }

    public void setMar_id(String mar_id) {
        this.mar_id = mar_id;
    }

    public String getMar_month() {
        return mar_month;
    }

    public void setMar_month(String mar_month) {
        this.mar_month = mar_month;
    }

    public String getMar_paid() {
        return mar_paid;
    }

    public void setMar_paid(String mar_paid) {
        this.mar_paid = mar_paid;
    }

    public String getApr_id() {
        return apr_id;
    }

    public void setApr_id(String apr_id) {
        this.apr_id = apr_id;
    }

    public String getApr_month() {
        return apr_month;
    }

    public void setApr_month(String apr_month) {
        this.apr_month = apr_month;
    }

    public String getApr_paid() {
        return apr_paid;
    }

    public void setApr_paid(String apr_paid) {
        this.apr_paid = apr_paid;
    }

    public String getMay_id() {
        return may_id;
    }

    public void setMay_id(String may_id) {
        this.may_id = may_id;
    }

    public String getMay_month() {
        return may_month;
    }

    public void setMay_month(String may_month) {
        this.may_month = may_month;
    }

    public String getMay_paid() {
        return may_paid;
    }

    public void setMay_paid(String may_paid) {
        this.may_paid = may_paid;
    }

    public String getJune_id() {
        return june_id;
    }

    public void setJune_id(String june_id) {
        this.june_id = june_id;
    }

    public String getJune_month() {
        return june_month;
    }

    public void setJune_month(String june_month) {
        this.june_month = june_month;
    }

    public String getJune_paid() {
        return june_paid;
    }

    public void setJune_paid(String june_paid) {
        this.june_paid = june_paid;
    }

    public String getJuly_id() {
        return july_id;
    }

    public void setJuly_id(String july_id) {
        this.july_id = july_id;
    }

    public String getJuly_month() {
        return july_month;
    }

    public void setJuly_month(String july_month) {
        this.july_month = july_month;
    }

    public String getJuly_paid() {
        return july_paid;
    }

    public void setJuly_paid(String july_paid) {
        this.july_paid = july_paid;
    }

    public String getAug_id() {
        return aug_id;
    }

    public void setAug_id(String aug_id) {
        this.aug_id = aug_id;
    }

    public String getAug_month() {
        return aug_month;
    }

    public void setAug_month(String aug_month) {
        this.aug_month = aug_month;
    }

    public String getAug_paid() {
        return aug_paid;
    }

    public void setAug_paid(String aug_paid) {
        this.aug_paid = aug_paid;
    }

    public String getSep_id() {
        return sep_id;
    }

    public void setSep_id(String sep_id) {
        this.sep_id = sep_id;
    }

    public String getSep_month() {
        return sep_month;
    }

    public void setSep_month(String sep_month) {
        this.sep_month = sep_month;
    }

    public String getCastid() {
        return castid;
    }

    public void setCastid(String castid) {
        this.castid = castid;
    }

    private String castid;

    public String getSep_paid() {
        return sep_paid;
    }

    public void setSep_paid(String sep_paid) {
        this.sep_paid = sep_paid;
    }

    public String getOct_id() {
        return oct_id;
    }

    public void setOct_id(String oct_id) {
        this.oct_id = oct_id;
    }

    public String getOct_month() {
        return oct_month;
    }

    public void setOct_month(String oct_month) {
        this.oct_month = oct_month;
    }

    public String getOct_paid() {
        return oct_paid;
    }

    public void setOct_paid(String oct_paid) {
        this.oct_paid = oct_paid;
    }

    public String getNov_id() {
        return nov_id;
    }

    public void setNov_id(String nov_id) {
        this.nov_id = nov_id;
    }

    public String getNov_month() {
        return nov_month;
    }

    public void setNov_month(String nov_month) {
        this.nov_month = nov_month;
    }

    public String getNov_paid() {
        return nov_paid;
    }

    public void setNov_paid(String nov_paid) {
        this.nov_paid = nov_paid;
    }

    public String getDec_id() {
        return dec_id;
    }

    public void setDec_id(String dec_id) {
        this.dec_id = dec_id;
    }

    public String getDec_month() {
        return dec_month;
    }

    public void setDec_month(String dec_month) {
        this.dec_month = dec_month;
    }

    public String getDec_paid() {
        return dec_paid;
    }

    public void setDec_paid(String dec_paid) {
        this.dec_paid = dec_paid;
    }

    private String feb_id;
    private String feb_month;
    private String feb_paid;

    private String mar_id;
    private String mar_month;
    private String mar_paid;

    private String apr_id;
    private String apr_month;
    private String apr_paid;



    private String may_id;
    private String may_month;
    private String may_paid;

    private String june_id;
    private String june_month;
    private String june_paid;

    private String july_id;
    private String july_month;
    private String july_paid;

    private String aug_id;
    private String aug_month;
    private String aug_paid;



    private String sep_id;
    private String sep_month;
    private String sep_paid;

    private String oct_id;
    private String oct_month;
    private String oct_paid;

    private String nov_id;
    private String nov_month;
    private String nov_paid;

    private String dec_id;
    private String dec_month;
    private String dec_paid;



    private String id;

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    private String age;


    private String name;

    public String getDevice_token() {
        return device_token;
    }

    public void setDevice_token(String device_token) {
        this.device_token = device_token;
    }

    /**
     * email : aman21081993@gmail.com
     * phoneno : 7889267816
     * device_token :
     * lat : 30.342423
     * lng : 72.243232
     */
    private String device_token;

    private String class_name;
    private String father_name;
    private String mobile;
    private String profile;
    private String course_name;
    private String section_name;


    private String admin_fname;

    public String getAdmin_fname() {
        return admin_fname;
    }

    public void setAdmin_fname(String admin_fname) {
        this.admin_fname = admin_fname;
    }

    public String getA_signature() {
        return a_signature;
    }

    public void setA_signature(String a_signature) {
        this.a_signature = a_signature;
    }

    public String getSt_firstname() {
        return st_firstname;
    }

    public void setSt_firstname(String st_firstname) {
        this.st_firstname = st_firstname;
    }

    public String getS_signature() {
        return s_signature;
    }

    public void setS_signature(String s_signature) {
        this.s_signature = s_signature;
    }

    private String a_signature;
    private String st_firstname;
    private String s_signature;


    public String getSection_name() {
        return section_name;
    }

    public void setSection_name(String section_name) {
        this.section_name = section_name;
    }

    public String getCourse_name() {
        return course_name;
    }

    public void setCourse_name(String course_name) {
        this.course_name = course_name;
    }

    /**
     * bg : abc
     * gender : female
     * dob : 2013-02-08
     * aadhaar : 1234567899
     * address : 27 B Green View Colony Rajbaha Road , Patiala , Pincode: 147001
     * device_token :
     * class_id : 2
     * r_id : 0
     * lat : 30.342423
     * lng : 72.243232
     * roll_no : 1
     */

    private String bg;
    private String gender;
    private String dob;
    private String aadhaar;
    private String address;
    private String class_id;
    private String r_id;
    private double lat;
    private double lng;
    private String roll_no;

    public String getMobile_name() {
        return mobile_name;
    }

    public void setMobile_name(String mobile_name) {
        this.mobile_name = mobile_name;
    }

    private String mobile_name;

    /**
     * email : aman21081993@gmail.com
     * phoneno : 7889267816
     * device_token :
     * lat : 30.342423
     * lng : 72.243232
     */

    private String email;
    private String phoneno;

    public String getFee_cat_id() {
        return fee_cat_id;
    }

    public void setFee_cat_id(String fee_cat_id) {
        this.fee_cat_id = fee_cat_id;
    }

    private String fee_cat_id;

    /**
     * device_token : dDr3bszSK7k:APA91bF3gDKdOD7ID1WNPGlgxk-iyuD_cvuqdCT2DzKJnOTdUsWI0QGmm4t8seGxYg-yrfBuxr68nsvstLNZSVnI
     * lat : 30.342423
     * lng : 72.243232
     * croll_no : Miss
     */

    private String croll_no;

    public String getAt_attendance_date() {
        return at_attendance_date;
    }

    public void setAt_attendance_date(String at_attendance_date) {
        this.at_attendance_date = at_attendance_date;
    }

    private String at_attendance_date;

    /**
     * device_token : d58Ci0HjTWI:APA91bE_sEFTs6Q3O4rx7Y5jSsFDxcsS75cwgyRGvVhPcQCHKkgMDn8HZBSUwWw1QHYkgNUHoqlRfZaxYmreJyj5
     * lat : 0
     * lng : 0
     * collegeroll_no : Miss
     */
    private String username;
    private String password;
    private String hostelstatus;
    private String admissiondate;
    private String admissionquota;
    private String religion;
    private String nationality;
    private String universityregno;

    private String pr_address;
    private String pr_city;
    private String pr_state;
    private String pr_pincode;
    private String pr_email;
    private String pr_mobile;
    private String pmt_address;
    private String pmt_city;
    private String pmt_state;
    private String pmt_pincode;
    private String pmt_email;
    private String pmt_mobile;
    private String neet_roll;
    private String neet_mrkobt;
    private String neet_totmrks;
    private String neet_prcntile;
    private String neet_statrank;
    private String neet_air;

    private String mtric_session;
    private String mtric_school;
    private String mtric_board;
    private String mtric_statscol;
    private String mtric_stboard;
    private String elev_session;
    private String elev_school;
    private String elev_board;
    private String elev_statscol;
    private String elev_stboard;
    private String twlv_session;
    private String twlv_school;
    private String twlv_board;
    private String twlv_statscol;
    private String twlv_stboard;

    private String twlv_roll;
    private String maxphy;
    private String obtphy;
    private String maxchem;
    private String obtchem;
    private String maxbio;
    private String obtbio;
    private String maxeng;
    private String obteng;
    private String maxpcb;
    private String obtpcb;
    private String maxmarks;
    private String minmarks;

    private String bdssession;
    private String bdsschoolname;
    private String bdsboard_univ;
    private String bdsmarksobt;
    private String bdstotalmarks;
    private String bdsstatcolg;
    private String bdsstatuniv;


    private String bdsfstyrmrks;
    private String bdsfstyrtotmrks;
    private String bdssecyrmrks;
    private String bdssecyrtotmrks;
    private String bdsthrdyrmrks;
    private String bdsthrdyrtotmrks;
    private String bdsfinlyrmrks;
    private String bdsfinlyrtotmrks;

    private String mother_name;
    private String gndfathermobile;
    private String gndmothermobile;

    private String fatheroccu;
    private String motheroccu;
    private String fatherage;
    private String motherage;
    private String fathereduquali;
    private String mothereduquali;

    private String castetype;

    private String es_morning_vid;
    private String es_evening_vid;
    private String village;

    private String place_id;

    public String getPlace_id() {
        return place_id;
    }

    public void setPlace_id(String place_id) {
        this.place_id = place_id;
    }

    public String getRoute_id() {
        return route_id;
    }

    public void setRoute_id(String route_id) {
        this.route_id = route_id;
    }

    private String route_id;


    private String aadhar_image;
    private String tenmarksheet_image;
    private String twelvemarksheet_image;

    private String schol_certif_image;
    private String schol_trans_certif_image;
    private String graduation_image;

    public String getAadhar_image() {
        return aadhar_image;
    }

    public void setAadhar_image(String aadhar_image) {
        this.aadhar_image = aadhar_image;
    }

    public String getTenmarksheet_image() {
        return tenmarksheet_image;
    }

    public void setTenmarksheet_image(String tenmarksheet_image) {
        this.tenmarksheet_image = tenmarksheet_image;
    }


    public String getMobile_brand() {
        return mobile_brand;
    }

    public void setMobile_brand(String mobile_brand) {
        this.mobile_brand = mobile_brand;
    }

    private String mobile_brand;

    public String getTwelvemarksheet_image() {
        return twelvemarksheet_image;
    }

    public void setTwelvemarksheet_image(String twelvemarksheet_image) {
        this.twelvemarksheet_image = twelvemarksheet_image;
    }

    public String getSchol_certif_image() {
        return schol_certif_image;
    }

    public void setSchol_certif_image(String schol_certif_image) {
        this.schol_certif_image = schol_certif_image;
    }

    public String getSchol_trans_certif_image() {
        return schol_trans_certif_image;
    }

    public void setSchol_trans_certif_image(String schol_trans_certif_image) {
        this.schol_trans_certif_image = schol_trans_certif_image;
    }

    public String getGraduation_image() {
        return graduation_image;
    }

    public void setGraduation_image(String graduation_image) {
        this.graduation_image = graduation_image;
    }

    public String getAadhar_pdf() {
        return aadhar_pdf;
    }

    public void setAadhar_pdf(String aadhar_pdf) {
        this.aadhar_pdf = aadhar_pdf;
    }

    public String getTenmarksheet_pdf() {
        return tenmarksheet_pdf;
    }

    public void setTenmarksheet_pdf(String tenmarksheet_pdf) {
        this.tenmarksheet_pdf = tenmarksheet_pdf;
    }

    public String getTwelvemarksheet_pdf() {
        return twelvemarksheet_pdf;
    }

    public void setTwelvemarksheet_pdf(String twelvemarksheet_pdf) {
        this.twelvemarksheet_pdf = twelvemarksheet_pdf;
    }

    public String getSchol_certif_pdf() {
        return schol_certif_pdf;
    }

    public void setSchol_certif_pdf(String schol_certif_pdf) {
        this.schol_certif_pdf = schol_certif_pdf;
    }

    public String getSchol_trans_certif_pdf() {
        return schol_trans_certif_pdf;
    }

    public void setSchol_trans_certif_pdf(String schol_trans_certif_pdf) {
        this.schol_trans_certif_pdf = schol_trans_certif_pdf;
    }

    public String getGraduation_pdf() {
        return graduation_pdf;
    }

    public void setGraduation_pdf(String graduation_pdf) {
        this.graduation_pdf = graduation_pdf;
    }

    private String aadhar_pdf;
    private String tenmarksheet_pdf;
    private String twelvemarksheet_pdf;

    private String schol_certif_pdf;

    private String schol_trans_certif_pdf;
    private String graduation_pdf;

    public String getVillage() {
        return village;
    }

    public void setVillage(String village) {
        this.village = village;
    }

    public String getEs_morning_vid() {
        return es_morning_vid;
    }

    public void setEs_morning_vid(String es_morning_vid) {
        this.es_morning_vid = es_morning_vid;
    }

    public String getEs_evening_vid() {
        return es_evening_vid;
    }

    public void setEs_evening_vid(String es_evening_vid) {
        this.es_evening_vid = es_evening_vid;
    }

    public String getCastetype() {
        return castetype;
    }

    public void setCastetype(String castetype) {
        this.castetype = castetype;
    }

    public String getFatheroccu() {
        return fatheroccu;
    }

    public void setFatheroccu(String fatheroccu) {
        this.fatheroccu = fatheroccu;
    }

    public String getMotheroccu() {
        return motheroccu;
    }

    public void setMotheroccu(String motheroccu) {
        this.motheroccu = motheroccu;
    }

    public String getFatherage() {
        return fatherage;
    }

    public void setFatherage(String fatherage) {
        this.fatherage = fatherage;
    }

    public String getMotherage() {
        return motherage;
    }

    public void setMotherage(String motherage) {
        this.motherage = motherage;
    }

    public String getFathereduquali() {
        return fathereduquali;
    }

    public void setFathereduquali(String fathereduquali) {
        this.fathereduquali = fathereduquali;
    }

    public String getMothereduquali() {
        return mothereduquali;
    }

    public void setMothereduquali(String mothereduquali) {
        this.mothereduquali = mothereduquali;
    }

    public String getGndfathermobile() {
        return gndfathermobile;
    }

    public void setGndfathermobile(String gndfathermobile) {
        this.gndfathermobile = gndfathermobile;
    }

    public String getGndmothermobile() {
        return gndmothermobile;
    }

    public void setGndmothermobile(String gndmothermobile) {
        this.gndmothermobile = gndmothermobile;
    }

    public String getMother_name() {
        return mother_name;
    }

    public void setMother_name(String mother_name) {
        this.mother_name = mother_name;
    }

    public String getBdsfinlyrmrks() {
        return bdsfinlyrmrks;
    }

    public void setBdsfinlyrmrks(String bdsfinlyrmrks) {
        this.bdsfinlyrmrks = bdsfinlyrmrks;
    }

    public String getBdsfinlyrtotmrks() {
        return bdsfinlyrtotmrks;
    }

    public void setBdsfinlyrtotmrks(String bdsfinlyrtotmrks) {
        this.bdsfinlyrtotmrks = bdsfinlyrtotmrks;
    }

    public String getBdsfstyrmrks() {
        return bdsfstyrmrks;
    }

    public void setBdsfstyrmrks(String bdsfstyrmrks) {
        this.bdsfstyrmrks = bdsfstyrmrks;
    }

    public String getBdsfstyrtotmrks() {
        return bdsfstyrtotmrks;
    }

    public void setBdsfstyrtotmrks(String bdsfstyrtotmrks) {
        this.bdsfstyrtotmrks = bdsfstyrtotmrks;
    }

    public String getBdssecyrmrks() {
        return bdssecyrmrks;
    }

    public void setBdssecyrmrks(String bdssecyrmrks) {
        this.bdssecyrmrks = bdssecyrmrks;
    }

    public String getBdssecyrtotmrks() {
        return bdssecyrtotmrks;
    }

    public void setBdssecyrtotmrks(String bdssecyrtotmrks) {
        this.bdssecyrtotmrks = bdssecyrtotmrks;
    }

    public String getBdsthrdyrmrks() {
        return bdsthrdyrmrks;
    }

    public void setBdsthrdyrmrks(String bdsthrdyrmrks) {
        this.bdsthrdyrmrks = bdsthrdyrmrks;
    }

    public String getBdsthrdyrtotmrks() {
        return bdsthrdyrtotmrks;
    }

    public void setBdsthrdyrtotmrks(String bdsthrdyrtotmrks) {
        this.bdsthrdyrtotmrks = bdsthrdyrtotmrks;
    }

    public String getBdssession() {
        return bdssession;
    }

    public void setBdssession(String bdssession) {
        this.bdssession = bdssession;
    }

    public String getBdsschoolname() {
        return bdsschoolname;
    }

    public void setBdsschoolname(String bdsschoolname) {
        this.bdsschoolname = bdsschoolname;
    }

    public String getBdsboard_univ() {
        return bdsboard_univ;
    }

    public void setBdsboard_univ(String bdsboard_univ) {
        this.bdsboard_univ = bdsboard_univ;
    }

    public String getBdsmarksobt() {
        return bdsmarksobt;
    }

    public void setBdsmarksobt(String bdsmarksobt) {
        this.bdsmarksobt = bdsmarksobt;
    }

    public String getBdstotalmarks() {
        return bdstotalmarks;
    }

    public void setBdstotalmarks(String bdstotalmarks) {
        this.bdstotalmarks = bdstotalmarks;
    }

    public String getBdsstatcolg() {
        return bdsstatcolg;
    }

    public void setBdsstatcolg(String bdsstatcolg) {
        this.bdsstatcolg = bdsstatcolg;
    }

    public String getBdsstatuniv() {
        return bdsstatuniv;
    }

    public void setBdsstatuniv(String bdsstatuniv) {
        this.bdsstatuniv = bdsstatuniv;
    }

    public String getTwlv_roll() {
        return twlv_roll;
    }

    public void setTwlv_roll(String twlv_roll) {
        this.twlv_roll = twlv_roll;
    }

    public String getMaxphy() {
        return maxphy;
    }

    public void setMaxphy(String maxphy) {
        this.maxphy = maxphy;
    }

    public String getObtphy() {
        return obtphy;
    }

    public void setObtphy(String obtphy) {
        this.obtphy = obtphy;
    }

    public String getMaxchem() {
        return maxchem;
    }

    public void setMaxchem(String maxchem) {
        this.maxchem = maxchem;
    }

    public String getObtchem() {
        return obtchem;
    }

    public void setObtchem(String obtchem) {
        this.obtchem = obtchem;
    }

    public String getMaxbio() {
        return maxbio;
    }

    public void setMaxbio(String maxbio) {
        this.maxbio = maxbio;
    }

    public String getObtbio() {
        return obtbio;
    }

    public void setObtbio(String obtbio) {
        this.obtbio = obtbio;
    }

    public String getMaxeng() {
        return maxeng;
    }

    public void setMaxeng(String maxeng) {
        this.maxeng = maxeng;
    }

    public String getObteng() {
        return obteng;
    }

    public void setObteng(String obteng) {
        this.obteng = obteng;
    }

    public String getMaxpcb() {
        return maxpcb;
    }

    public void setMaxpcb(String maxpcb) {
        this.maxpcb = maxpcb;
    }

    public String getObtpcb() {
        return obtpcb;
    }

    public void setObtpcb(String obtpcb) {
        this.obtpcb = obtpcb;
    }

    public String getMaxmarks() {
        return maxmarks;
    }

    public void setMaxmarks(String maxmarks) {
        this.maxmarks = maxmarks;
    }

    public String getMinmarks() {
        return minmarks;
    }

    public void setMinmarks(String minmarks) {
        this.minmarks = minmarks;
    }

    public String getMtric_session() {
        return mtric_session;
    }

    public void setMtric_session(String mtric_session) {
        this.mtric_session = mtric_session;
    }

    public String getMtric_school() {
        return mtric_school;
    }

    public void setMtric_school(String mtric_school) {
        this.mtric_school = mtric_school;
    }

    public String getMtric_board() {
        return mtric_board;
    }

    public void setMtric_board(String mtric_board) {
        this.mtric_board = mtric_board;
    }

    public String getMtric_statscol() {
        return mtric_statscol;
    }

    public void setMtric_statscol(String mtric_statscol) {
        this.mtric_statscol = mtric_statscol;
    }

    public String getMtric_stboard() {
        return mtric_stboard;
    }

    public void setMtric_stboard(String mtric_stboard) {
        this.mtric_stboard = mtric_stboard;
    }

    public String getElev_session() {
        return elev_session;
    }

    public void setElev_session(String elev_session) {
        this.elev_session = elev_session;
    }

    public String getElev_school() {
        return elev_school;
    }

    public void setElev_school(String elev_school) {
        this.elev_school = elev_school;
    }

    public String getElev_board() {
        return elev_board;
    }

    public void setElev_board(String elev_board) {
        this.elev_board = elev_board;
    }

    public String getElev_statscol() {
        return elev_statscol;
    }

    public void setElev_statscol(String elev_statscol) {
        this.elev_statscol = elev_statscol;
    }

    public String getElev_stboard() {
        return elev_stboard;
    }

    public void setElev_stboard(String elev_stboard) {
        this.elev_stboard = elev_stboard;
    }

    public String getTwlv_session() {
        return twlv_session;
    }

    public void setTwlv_session(String twlv_session) {
        this.twlv_session = twlv_session;
    }

    public String getTwlv_school() {
        return twlv_school;
    }

    public void setTwlv_school(String twlv_school) {
        this.twlv_school = twlv_school;
    }

    public String getTwlv_board() {
        return twlv_board;
    }

    public void setTwlv_board(String twlv_board) {
        this.twlv_board = twlv_board;
    }

    public String getTwlv_statscol() {
        return twlv_statscol;
    }

    public void setTwlv_statscol(String twlv_statscol) {
        this.twlv_statscol = twlv_statscol;
    }

    public String getTwlv_stboard() {
        return twlv_stboard;
    }

    public void setTwlv_stboard(String twlv_stboard) {
        this.twlv_stboard = twlv_stboard;
    }

    public String getPr_address() {
        return pr_address;
    }

    public void setPr_address(String pr_address) {
        this.pr_address = pr_address;
    }

    public String getPr_city() {
        return pr_city;
    }

    public void setPr_city(String pr_city) {
        this.pr_city = pr_city;
    }

    public String getPr_state() {
        return pr_state;
    }

    public void setPr_state(String pr_state) {
        this.pr_state = pr_state;
    }

    public String getPr_pincode() {
        return pr_pincode;
    }

    public void setPr_pincode(String pr_pincode) {
        this.pr_pincode = pr_pincode;
    }

    public String getPr_email() {
        return pr_email;
    }

    public void setPr_email(String pr_email) {
        this.pr_email = pr_email;
    }

    public String getPr_mobile() {
        return pr_mobile;
    }

    public void setPr_mobile(String pr_mobile) {
        this.pr_mobile = pr_mobile;
    }

    public String getPmt_address() {
        return pmt_address;
    }

    public void setPmt_address(String pmt_address) {
        this.pmt_address = pmt_address;
    }

    public String getPmt_city() {
        return pmt_city;
    }

    public void setPmt_city(String pmt_city) {
        this.pmt_city = pmt_city;
    }

    public String getPmt_state() {
        return pmt_state;
    }

    public void setPmt_state(String pmt_state) {
        this.pmt_state = pmt_state;
    }

    public String getPmt_pincode() {
        return pmt_pincode;
    }

    public void setPmt_pincode(String pmt_pincode) {
        this.pmt_pincode = pmt_pincode;
    }

    public String getPmt_email() {
        return pmt_email;
    }

    public void setPmt_email(String pmt_email) {
        this.pmt_email = pmt_email;
    }

    public String getPmt_mobile() {
        return pmt_mobile;
    }

    public void setPmt_mobile(String pmt_mobile) {
        this.pmt_mobile = pmt_mobile;
    }

    public String getNeet_roll() {
        return neet_roll;
    }

    public void setNeet_roll(String neet_roll) {
        this.neet_roll = neet_roll;
    }

    public String getNeet_mrkobt() {
        return neet_mrkobt;
    }

    public void setNeet_mrkobt(String neet_mrkobt) {
        this.neet_mrkobt = neet_mrkobt;
    }

    public String getNeet_totmrks() {
        return neet_totmrks;
    }

    public void setNeet_totmrks(String neet_totmrks) {
        this.neet_totmrks = neet_totmrks;
    }

    public String getNeet_prcntile() {
        return neet_prcntile;
    }

    public void setNeet_prcntile(String neet_prcntile) {
        this.neet_prcntile = neet_prcntile;
    }

    public String getNeet_statrank() {
        return neet_statrank;
    }

    public void setNeet_statrank(String neet_statrank) {
        this.neet_statrank = neet_statrank;
    }

    public String getNeet_air() {
        return neet_air;
    }

    public void setNeet_air(String neet_air) {
        this.neet_air = neet_air;
    }

    public String getUniversityregno() {
        return universityregno;
    }

    public void setUniversityregno(String universityregno) {
        this.universityregno = universityregno;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getReligion() {
        return religion;
    }

    public void setReligion(String religion) {
        this.religion = religion;
    }

    public String getCounseling_date() {
        return counseling_date;
    }

    public void setCounseling_date(String counseling_date) {
        this.counseling_date = counseling_date;
    }

    private String counseling_date;
    private String room_no;

    public String getRoom_no() {
        return room_no;
    }

    public void setRoom_no(String room_no) {
        this.room_no = room_no;
    }

    public String getRoom_type() {
        return room_type;
    }

    public void setRoom_type(String room_type) {
        this.room_type = room_type;
    }

    public String getRoom_capacity() {
        return room_capacity;
    }

    public void setRoom_capacity(String room_capacity) {
        this.room_capacity = room_capacity;
    }

    private String room_type;
    private String room_capacity;
    private String hostel_name;

    public String getHostel_name() {
        return hostel_name;
    }

    public void setHostel_name(String hostel_name) {
        this.hostel_name = hostel_name;
    }

    public String getHostelstatus() {
        return hostelstatus;
    }

    public void setHostelstatus(String hostelstatus) {
        this.hostelstatus = hostelstatus;
    }

    public String getAdmissiondate() {
        return admissiondate;
    }

    public void setAdmissiondate(String admissiondate) {
        this.admissiondate = admissiondate;
    }

    public String getSessionname() {
        return sessionname;
    }

    public void setSessionname(String sessionname) {
        this.sessionname = sessionname;
    }

    public String getMothersmobile() {
        return mothersmobile;
    }

    public void setMothersmobile(String mothersmobile) {
        this.mothersmobile = mothersmobile;
    }

    public String getFathermobile() {
        return fathermobile;
    }

    public void setFathermobile(String fathermobile) {
        this.fathermobile = fathermobile;
    }

    public String getCategoryname() {
        return categoryname;
    }

    public void setCategoryname(String categoryname) {
        this.categoryname = categoryname;
    }

    private String sessionname;
    private String mothersmobile;
    private String fathermobile;
    private String categoryname;

    private String no_of_siblings;
    private String no_of_siblings_other_school;

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    private String reference;

    public String getNo_of_siblings() {
        return no_of_siblings;
    }

    public void setNo_of_siblings(String no_of_siblings) {
        this.no_of_siblings = no_of_siblings;
    }

    public String getNo_of_siblings_other_school() {
        return no_of_siblings_other_school;
    }

    public void setNo_of_siblings_other_school(String no_of_siblings_other_school) {
        this.no_of_siblings_other_school = no_of_siblings_other_school;
    }

    public String getNo_of_siblings_in_school() {
        return no_of_siblings_in_school;
    }

    public void setNo_of_siblings_in_school(String no_of_siblings_in_school) {
        this.no_of_siblings_in_school = no_of_siblings_in_school;
    }

    public String getDistance_from_school() {
        return distance_from_school;
    }

    public void setDistance_from_school(String distance_from_school) {
        this.distance_from_school = distance_from_school;
    }

    public String getQualification() {
        return qualification;
    }

    public void setQualification(String qualification) {
        this.qualification = qualification;
    }

    public String getPre_emailid() {
        return pre_emailid;
    }

    public void setPre_emailid(String pre_emailid) {
        this.pre_emailid = pre_emailid;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }


    public String getPre_prev_school() {
        return pre_prev_school;
    }

    public void setPre_prev_school(String pre_prev_school) {
        this.pre_prev_school = pre_prev_school;
    }

    public String getReason_for_leave_school() {
        return reason_for_leave_school;
    }

    public void setReason_for_leave_school(String reason_for_leave_school) {
        this.reason_for_leave_school = reason_for_leave_school;
    }

    private String no_of_siblings_in_school;
    private String distance_from_school;
    private String qualification;
    private String pre_emailid;
    private String designation;

    public String getPre_prev_class() {
        return pre_prev_class;
    }

    public void setPre_prev_class(String pre_prev_class) {
        this.pre_prev_class = pre_prev_class;
    }

    private String pre_prev_class;
    private String pre_prev_school;
    private String reason_for_leave_school;


    public String getAdmissionquota() {
        return admissionquota;
    }

    public void setAdmissionquota(String admissionquota) {
        this.admissionquota = admissionquota;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static ArrayList<Studentbean> parseHWArray(JSONArray arrayObj) {
        ArrayList<Studentbean> list = new ArrayList<Studentbean>();
        try {

            for (int i = 0; i < arrayObj.length(); i++) {
                Studentbean p = parseHWObject(arrayObj.getJSONObject(i));
                if (p != null) {
                    list.add(p);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public static Studentbean parseHWObject(JSONObject jsonObject) {
        Studentbean user = new Studentbean();
        try {
            if (jsonObject.has(ID)) {
                user.setId(jsonObject.getString(ID));
            }
            if (jsonObject.has(NAME) && !jsonObject.getString(NAME).isEmpty() && !jsonObject.getString(NAME).equalsIgnoreCase("null")) {
                user.setName(jsonObject.getString(NAME));
            }
            if (jsonObject.has(F_NAME) && !jsonObject.getString(F_NAME).isEmpty() && !jsonObject.getString(F_NAME).equalsIgnoreCase("null")) {
                user.setFather_name(jsonObject.getString(F_NAME));
            }
            if (jsonObject.has(CLASS_NAME) && !jsonObject.getString(CLASS_NAME).isEmpty() && !jsonObject.getString(CLASS_NAME).equalsIgnoreCase("null")) {
                user.setClass_name(jsonObject.getString(CLASS_NAME));
            }
            if (jsonObject.has(PHONE) && !jsonObject.getString(PHONE).isEmpty() && !jsonObject.getString(PHONE).equalsIgnoreCase("null")) {
                user.setMobile(jsonObject.getString(PHONE));
            }
            if (jsonObject.has(PROFILE) && !jsonObject.getString(PROFILE).isEmpty() && !jsonObject.getString(PROFILE).equalsIgnoreCase("null")) {
                user.setProfile(Constants.getImageBaseURL() + jsonObject.getString(PROFILE));
            }
            if (jsonObject.has(CLASS_ID) && !jsonObject.getString(CLASS_ID).isEmpty() && !jsonObject.getString(CLASS_ID).equalsIgnoreCase("null")) {
                user.setClass_id(jsonObject.getString(CLASS_ID));
            }
            if (jsonObject.has(R_ID) && !jsonObject.getString(R_ID).isEmpty() && !jsonObject.getString(R_ID).equalsIgnoreCase("null")) {
                user.setR_id(jsonObject.getString(R_ID));
            }
            if (jsonObject.has(LAT) && !jsonObject.getString(LAT).isEmpty() && !jsonObject.getString(LAT).equalsIgnoreCase("null")) {
                user.setLat(jsonObject.getDouble(LAT));
            }
            if (jsonObject.has(EMAIL) && !jsonObject.getString(EMAIL).isEmpty() && !jsonObject.getString(EMAIL).equalsIgnoreCase("null")) {
                user.setEmail(jsonObject.getString(EMAIL));
            }
            if (jsonObject.has(PHONENO) && !jsonObject.getString(PHONENO).isEmpty() && !jsonObject.getString(PHONENO).equalsIgnoreCase("null")) {
                user.setPhoneno(jsonObject.getString(PHONENO));
            }
            if (jsonObject.has(LNG) && !jsonObject.getString(LNG).isEmpty() && !jsonObject.getString(LNG).equalsIgnoreCase("null")) {
                user.setLng(jsonObject.getDouble(LNG));
            }
            if (jsonObject.has(CLASSNAME) && !jsonObject.getString(CLASSNAME).isEmpty() && !jsonObject.getString(CLASSNAME).equalsIgnoreCase("null")) {
                user.setClass_name(jsonObject.getString(CLASSNAME));
            }
            if (jsonObject.has("section_id") && !jsonObject.getString("section_id").isEmpty() && !jsonObject.getString("section_id").equalsIgnoreCase("null")) {
                user.setSection_id(jsonObject.getString("section_id"));
            }

            if (jsonObject.has("weight") && !jsonObject.getString("weight").isEmpty() && !jsonObject.getString("weight").equalsIgnoreCase("null")) {
                user.setWeight(jsonObject.getString("weight"));
            }

            if (jsonObject.has("height") && !jsonObject.getString("height").isEmpty() && !jsonObject.getString("height").equalsIgnoreCase("null")) {
                user.setHeight(jsonObject.getString("height"));
            }


            if (jsonObject.has("bg") && !jsonObject.getString("bg").isEmpty() && !jsonObject.getString("bg").equalsIgnoreCase("null")) {
                user.setBg(jsonObject.getString("bg"));
            }
            if (jsonObject.has("gender") && !jsonObject.getString("gender").isEmpty() && !jsonObject.getString("gender").equalsIgnoreCase("null")) {
                user.setGender(jsonObject.getString("gender"));
            }
            if (jsonObject.has("dob") && !jsonObject.getString("dob").isEmpty() && !jsonObject.getString("dob").equalsIgnoreCase("null")) {
                user.setDob(jsonObject.getString("dob"));
            }
            if (jsonObject.has("aadhaar") && !jsonObject.getString("aadhaar").isEmpty() && !jsonObject.getString("aadhaar").equalsIgnoreCase("null")) {
                user.setAadhaar(jsonObject.getString("aadhaar"));
            }
            if (jsonObject.has("address") && !jsonObject.getString("address").isEmpty() && !jsonObject.getString("address").equalsIgnoreCase("null")) {
                user.setAddress(jsonObject.getString("address"));
            }
            if (jsonObject.has("roll_no") && !jsonObject.getString("roll_no").isEmpty() && !jsonObject.getString("roll_no").equalsIgnoreCase("null")) {
                user.setRoll_no(jsonObject.getString("roll_no"));
            }
            if (jsonObject.has("croll_no") && !jsonObject.getString("croll_no").isEmpty() && !jsonObject.getString("croll_no").equalsIgnoreCase("null")) {
                user.setCroll_no(jsonObject.getString("croll_no"));
            }

            if (jsonObject.has("username") && !jsonObject.getString("username").isEmpty() && !jsonObject.getString("username").equalsIgnoreCase("null")) {
                user.setUsername(jsonObject.getString("username"));
            }
            if (jsonObject.has("password") && !jsonObject.getString("password").isEmpty() && !jsonObject.getString("password").equalsIgnoreCase("null")) {
                user.setPassword(jsonObject.getString("password"));
            }

            if (jsonObject.has("fee_cat_id") && !jsonObject.getString("fee_cat_id").isEmpty() && !jsonObject.getString("fee_cat_id").equalsIgnoreCase("null")) {
                user.setFee_cat_id(jsonObject.getString("fee_cat_id"));
            }

            if (jsonObject.has("hostelstatus") && !jsonObject.getString("hostelstatus").isEmpty() && !jsonObject.getString("hostelstatus").equalsIgnoreCase("null")) {
                user.setHostelstatus(jsonObject.getString("hostelstatus"));
            }
            if (jsonObject.has("admissiondate") && !jsonObject.getString("admissiondate").isEmpty() && !jsonObject.getString("admissiondate").equalsIgnoreCase("null")) {
                user.setAdmissiondate(jsonObject.getString("admissiondate"));
            }

            if (jsonObject.has("sessionname") && !jsonObject.getString("sessionname").isEmpty() && !jsonObject.getString("sessionname").equalsIgnoreCase("null")) {
                user.setSessionname(jsonObject.getString("sessionname"));
            }
            if (jsonObject.has("mothersmobile") && !jsonObject.getString("mothersmobile").isEmpty() && !jsonObject.getString("mothersmobile").equalsIgnoreCase("null")) {
                user.setMothersmobile(jsonObject.getString("mothersmobile"));
            }

            if (jsonObject.has("fathermobile") && !jsonObject.getString("fathermobile").isEmpty() && !jsonObject.getString("fathermobile").equalsIgnoreCase("null")) {
                user.setFathermobile(jsonObject.getString("fathermobile"));
            }

            if (jsonObject.has("categoryname") && !jsonObject.getString("categoryname").isEmpty() && !jsonObject.getString("categoryname").equalsIgnoreCase("null")) {
                user.setCategoryname(jsonObject.getString("categoryname"));
            }


            if (jsonObject.has("room_no") && !jsonObject.getString("room_no").isEmpty() && !jsonObject.getString("room_no").equalsIgnoreCase("null")) {
                user.setRoll_no(jsonObject.getString("room_no"));
            }

            if (jsonObject.has("room_type") && !jsonObject.getString("room_type").isEmpty() && !jsonObject.getString("room_type").equalsIgnoreCase("null")) {
                user.setRoom_type(jsonObject.getString("room_type"));
            }

            if (jsonObject.has("room_capacity") && !jsonObject.getString("room_capacity").isEmpty() && !jsonObject.getString("room_capacity").equalsIgnoreCase("null")) {
                user.setRoom_capacity(jsonObject.getString("room_capacity"));
            }

            if (jsonObject.has("hostel_name") && !jsonObject.getString("hostel_name").isEmpty() && !jsonObject.getString("hostel_name").equalsIgnoreCase("null")) {
                user.setHostel_name(jsonObject.getString("hostel_name"));
            }

            if (jsonObject.has("course_name") && !jsonObject.getString("course_name").isEmpty() && !jsonObject.getString("course_name").equalsIgnoreCase("null")) {
                user.setCourse_name(jsonObject.getString("course_name"));
            }
            if (jsonObject.has("counseling_date") && !jsonObject.getString("counseling_date").isEmpty() && !jsonObject.getString("counseling_date").equalsIgnoreCase("null")) {
                user.setCounseling_date(jsonObject.getString("counseling_date"));
            }

            if (jsonObject.has("admissionquota") && !jsonObject.getString("admissionquota").isEmpty() && !jsonObject.getString("admissionquota").equalsIgnoreCase("null")) {
                user.setAdmissionquota(jsonObject.getString("admissionquota"));
            }
            if (jsonObject.has("religion") && !jsonObject.getString("religion").isEmpty() && !jsonObject.getString("religion").equalsIgnoreCase("null")) {
                user.setReligion(jsonObject.getString("religion"));
            }
            if (jsonObject.has("nationality") && !jsonObject.getString("nationality").isEmpty() && !jsonObject.getString("nationality").equalsIgnoreCase("null")) {
                user.setNationality(jsonObject.getString("nationality"));
            }
            if (jsonObject.has("universityregno") && !jsonObject.getString("universityregno").isEmpty() && !jsonObject.getString("universityregno").equalsIgnoreCase("null")) {
                user.setUniversityregno(jsonObject.getString("universityregno"));
            }


            if (jsonObject.has("pr_address") && !jsonObject.getString("pr_address").isEmpty() && !jsonObject.getString("pr_address").equalsIgnoreCase("null")) {
                user.setPr_address(jsonObject.getString("pr_address"));
            }

            if (jsonObject.has("pr_city") && !jsonObject.getString("pr_city").isEmpty() && !jsonObject.getString("pr_city").equalsIgnoreCase("null")) {
                user.setPr_city(jsonObject.getString("pr_city"));
            }

            if (jsonObject.has("pr_state") && !jsonObject.getString("pr_state").isEmpty() && !jsonObject.getString("pr_state").equalsIgnoreCase("null")) {
                user.setPr_state(jsonObject.getString("pr_state"));
            }

            if (jsonObject.has("pr_pincode") && !jsonObject.getString("pr_pincode").isEmpty() && !jsonObject.getString("pr_pincode").equalsIgnoreCase("null")) {
                user.setPr_pincode(jsonObject.getString("pr_pincode"));
            }

            if (jsonObject.has("pr_email") && !jsonObject.getString("pr_email").isEmpty() && !jsonObject.getString("pr_email").equalsIgnoreCase("null")) {
                user.setPr_email(jsonObject.getString("pr_email"));
            }
            if (jsonObject.has("pr_mobile") && !jsonObject.getString("pr_mobile").isEmpty() && !jsonObject.getString("pr_mobile").equalsIgnoreCase("null")) {
                user.setPr_mobile(jsonObject.getString("pr_mobile"));
            }

            if (jsonObject.has("pmt_address") && !jsonObject.getString("pmt_address").isEmpty() && !jsonObject.getString("pmt_address").equalsIgnoreCase("null")) {
                user.setPmt_address(jsonObject.getString("pmt_address"));
            }
            if (jsonObject.has("pmt_city") && !jsonObject.getString("pmt_city").isEmpty() && !jsonObject.getString("pmt_city").equalsIgnoreCase("null")) {
                user.setPmt_city(jsonObject.getString("pmt_city"));
            }
            if (jsonObject.has("pmt_state") && !jsonObject.getString("pmt_state").isEmpty() && !jsonObject.getString("pmt_state").equalsIgnoreCase("null")) {
                user.setPmt_state(jsonObject.getString("pmt_state"));
            }
            if (jsonObject.has("pmt_pincode") && !jsonObject.getString("pmt_pincode").isEmpty() && !jsonObject.getString("pmt_pincode").equalsIgnoreCase("null")) {
                user.setPmt_pincode(jsonObject.getString("pmt_pincode"));
            }
            if (jsonObject.has("pmt_email") && !jsonObject.getString("pmt_email").isEmpty() && !jsonObject.getString("pmt_email").equalsIgnoreCase("null")) {
                user.setPmt_email(jsonObject.getString("pmt_email"));
            }
            if (jsonObject.has("pmt_mobile") && !jsonObject.getString("pmt_mobile").isEmpty() && !jsonObject.getString("pmt_mobile").equalsIgnoreCase("null")) {
                user.setPmt_mobile(jsonObject.getString("pmt_mobile"));
            }


            if (jsonObject.has("neet_roll") && !jsonObject.getString("neet_roll").isEmpty() && !jsonObject.getString("neet_roll").equalsIgnoreCase("null")) {
                user.setNeet_roll(jsonObject.getString("neet_roll"));
            }
            if (jsonObject.has("neet_mrkobt") && !jsonObject.getString("neet_mrkobt").isEmpty() && !jsonObject.getString("neet_mrkobt").equalsIgnoreCase("null")) {
                user.setNeet_mrkobt(jsonObject.getString("neet_mrkobt"));
            }
            if (jsonObject.has("neet_totmrks") && !jsonObject.getString("neet_totmrks").isEmpty() && !jsonObject.getString("neet_totmrks").equalsIgnoreCase("null")) {
                user.setNeet_totmrks(jsonObject.getString("neet_totmrks"));
            }
            if (jsonObject.has("neet_prcntile") && !jsonObject.getString("neet_prcntile").isEmpty() && !jsonObject.getString("neet_prcntile").equalsIgnoreCase("null")) {
                user.setNeet_prcntile(jsonObject.getString("neet_prcntile"));
            }
            if (jsonObject.has("neet_statrank") && !jsonObject.getString("neet_statrank").isEmpty() && !jsonObject.getString("neet_statrank").equalsIgnoreCase("null")) {
                user.setNeet_statrank(jsonObject.getString("neet_statrank"));
            }
            if (jsonObject.has("neet_air") && !jsonObject.getString("neet_air").isEmpty() && !jsonObject.getString("neet_air").equalsIgnoreCase("null")) {
                user.setNeet_air(jsonObject.getString("neet_air"));
            }


            if (jsonObject.has("mtric_session") && !jsonObject.getString("mtric_session").isEmpty() && !jsonObject.getString("mtric_session").equalsIgnoreCase("null")) {
                user.setMtric_session(jsonObject.getString("mtric_session"));
            }
            if (jsonObject.has("mtric_school") && !jsonObject.getString("mtric_school").isEmpty() && !jsonObject.getString("mtric_school").equalsIgnoreCase("null")) {
                user.setMtric_school(jsonObject.getString("mtric_school"));
            }
            if (jsonObject.has("mtric_board") && !jsonObject.getString("mtric_board").isEmpty() && !jsonObject.getString("mtric_board").equalsIgnoreCase("null")) {
                user.setMtric_board(jsonObject.getString("mtric_board"));
            }
            if (jsonObject.has("mtric_statscol") && !jsonObject.getString("mtric_statscol").isEmpty() && !jsonObject.getString("mtric_statscol").equalsIgnoreCase("null")) {
                user.setMtric_statscol(jsonObject.getString("mtric_statscol"));
            }
            if (jsonObject.has("mtric_stboard") && !jsonObject.getString("mtric_stboard").isEmpty() && !jsonObject.getString("mtric_stboard").equalsIgnoreCase("null")) {
                user.setMtric_stboard(jsonObject.getString("mtric_stboard"));
            }


            if (jsonObject.has("elev_session") && !jsonObject.getString("elev_session").isEmpty() && !jsonObject.getString("elev_session").equalsIgnoreCase("null")) {
                user.setElev_session(jsonObject.getString("elev_session"));
            }
            if (jsonObject.has("elev_school") && !jsonObject.getString("elev_school").isEmpty() && !jsonObject.getString("elev_school").equalsIgnoreCase("null")) {
                user.setElev_school(jsonObject.getString("elev_school"));
            }
            if (jsonObject.has("elev_board") && !jsonObject.getString("elev_board").isEmpty() && !jsonObject.getString("elev_board").equalsIgnoreCase("null")) {
                user.setElev_board(jsonObject.getString("elev_board"));
            }
            if (jsonObject.has("elev_statscol") && !jsonObject.getString("elev_statscol").isEmpty() && !jsonObject.getString("elev_statscol").equalsIgnoreCase("null")) {
                user.setElev_statscol(jsonObject.getString("elev_statscol"));
            }
            if (jsonObject.has("elev_stboard") && !jsonObject.getString("elev_stboard").isEmpty() && !jsonObject.getString("elev_stboard").equalsIgnoreCase("null")) {
                user.setElev_stboard(jsonObject.getString("elev_stboard"));
            }


            if (jsonObject.has("twlv_session") && !jsonObject.getString("twlv_session").isEmpty() && !jsonObject.getString("twlv_session").equalsIgnoreCase("null")) {
                user.setTwlv_session(jsonObject.getString("twlv_session"));
            }
            if (jsonObject.has("twlv_school") && !jsonObject.getString("twlv_school").isEmpty() && !jsonObject.getString("twlv_school").equalsIgnoreCase("null")) {
                user.setTwlv_school(jsonObject.getString("twlv_school"));
            }
            if (jsonObject.has("twlv_board") && !jsonObject.getString("twlv_board").isEmpty() && !jsonObject.getString("twlv_board").equalsIgnoreCase("null")) {
                user.setTwlv_board(jsonObject.getString("twlv_board"));
            }
            if (jsonObject.has("twlv_statscol") && !jsonObject.getString("twlv_statscol").isEmpty() && !jsonObject.getString("twlv_statscol").equalsIgnoreCase("null")) {
                user.setTwlv_statscol(jsonObject.getString("twlv_statscol"));
            }
            if (jsonObject.has("twlv_stboard") && !jsonObject.getString("twlv_stboard").isEmpty() && !jsonObject.getString("twlv_stboard").equalsIgnoreCase("null")) {
                user.setTwlv_stboard(jsonObject.getString("twlv_stboard"));
            }


            if (jsonObject.has("twlv_roll") && !jsonObject.getString("twlv_roll").isEmpty() && !jsonObject.getString("twlv_roll").equalsIgnoreCase("null")) {
                user.setTwlv_roll(jsonObject.getString("twlv_roll"));
            }
            if (jsonObject.has("maxphy") && !jsonObject.getString("maxphy").isEmpty() && !jsonObject.getString("maxphy").equalsIgnoreCase("null")) {
                user.setMaxphy(jsonObject.getString("maxphy"));
            }
            if (jsonObject.has("obtphy") && !jsonObject.getString("obtphy").isEmpty() && !jsonObject.getString("obtphy").equalsIgnoreCase("null")) {
                user.setObtphy(jsonObject.getString("obtphy"));
            }
            if (jsonObject.has("maxchem") && !jsonObject.getString("maxchem").isEmpty() && !jsonObject.getString("maxchem").equalsIgnoreCase("null")) {
                user.setMaxchem(jsonObject.getString("maxchem"));
            }
            if (jsonObject.has("obtchem") && !jsonObject.getString("obtchem").isEmpty() && !jsonObject.getString("obtchem").equalsIgnoreCase("null")) {
                user.setObtchem(jsonObject.getString("obtchem"));
            }

            if (jsonObject.has("f_name") && !jsonObject.getString("f_name").isEmpty() && !jsonObject.getString("f_name").equalsIgnoreCase("null")) {
                user.setF_name(jsonObject.getString("f_name"));
            }
            if (jsonObject.has("l_name") && !jsonObject.getString("l_name").isEmpty() && !jsonObject.getString("l_name").equalsIgnoreCase("null")) {
                user.setL_name(jsonObject.getString("l_name"));
            }

            if (jsonObject.has("maxbio") && !jsonObject.getString("maxbio").isEmpty() && !jsonObject.getString("maxbio").equalsIgnoreCase("null")) {
                user.setMaxbio(jsonObject.getString("maxbio"));
            }
            if (jsonObject.has("obtbio") && !jsonObject.getString("obtbio").isEmpty() && !jsonObject.getString("obtbio").equalsIgnoreCase("null")) {
                user.setObtbio(jsonObject.getString("obtbio"));
            }
            if (jsonObject.has("maxeng") && !jsonObject.getString("maxeng").isEmpty() && !jsonObject.getString("maxeng").equalsIgnoreCase("null")) {
                user.setMaxeng(jsonObject.getString("maxeng"));
            }
            if (jsonObject.has("obteng") && !jsonObject.getString("obteng").isEmpty() && !jsonObject.getString("obteng").equalsIgnoreCase("null")) {
                user.setObteng(jsonObject.getString("obteng"));
            }


            if (jsonObject.has("maxpcb") && !jsonObject.getString("maxpcb").isEmpty() && !jsonObject.getString("maxpcb").equalsIgnoreCase("null")) {
                user.setMaxpcb(jsonObject.getString("maxpcb"));
            }
            if (jsonObject.has("obtpcb") && !jsonObject.getString("obtpcb").isEmpty() && !jsonObject.getString("obtpcb").equalsIgnoreCase("null")) {
                user.setObtpcb(jsonObject.getString("obtpcb"));
            }
            if (jsonObject.has("maxmarks") && !jsonObject.getString("maxmarks").isEmpty() && !jsonObject.getString("maxmarks").equalsIgnoreCase("null")) {
                user.setMaxmarks(jsonObject.getString("maxmarks"));
            }
            if (jsonObject.has("minmarks") && !jsonObject.getString("minmarks").isEmpty() && !jsonObject.getString("minmarks").equalsIgnoreCase("null")) {
                user.setMinmarks(jsonObject.getString("minmarks"));
            }
            if (jsonObject.has("castid") && !jsonObject.getString("castid").isEmpty() && !jsonObject.getString("castid").equalsIgnoreCase("null")) {
                user.setCastid(jsonObject.getString("castid"));
            }


            if (jsonObject.has("bdssession") && !jsonObject.getString("bdssession").isEmpty() && !jsonObject.getString("bdssession").equalsIgnoreCase("null")) {
                user.setBdssession(jsonObject.getString("bdssession"));
            }
            if (jsonObject.has("bdsschoolname") && !jsonObject.getString("bdsschoolname").isEmpty() && !jsonObject.getString("bdsschoolname").equalsIgnoreCase("null")) {
                user.setBdsschoolname(jsonObject.getString("bdsschoolname"));
            }
            if (jsonObject.has("bdsboard_univ") && !jsonObject.getString("bdsboard_univ").isEmpty() && !jsonObject.getString("bdsboard_univ").equalsIgnoreCase("null")) {
                user.setBdsboard_univ(jsonObject.getString("bdsboard_univ"));
            }
            if (jsonObject.has("bdsmarksobt") && !jsonObject.getString("bdsmarksobt").isEmpty() && !jsonObject.getString("bdsmarksobt").equalsIgnoreCase("null")) {
                user.setBdsmarksobt(jsonObject.getString("bdsmarksobt"));
            }
            if (jsonObject.has("bdstotalmarks") && !jsonObject.getString("bdstotalmarks").isEmpty() && !jsonObject.getString("bdstotalmarks").equalsIgnoreCase("null")) {
                user.setBdstotalmarks(jsonObject.getString("bdstotalmarks"));
            }
            if (jsonObject.has("bdsstatcolg") && !jsonObject.getString("bdsstatcolg").isEmpty() && !jsonObject.getString("bdsstatcolg").equalsIgnoreCase("null")) {
                user.setBdsstatcolg(jsonObject.getString("bdsstatcolg"));
            }
            if (jsonObject.has("bdsstatuniv") && !jsonObject.getString("bdsstatuniv").isEmpty() && !jsonObject.getString("bdsstatuniv").equalsIgnoreCase("null")) {
                user.setBdsstatuniv(jsonObject.getString("bdsstatuniv"));
            }


            if (jsonObject.has("bdsfstyrmrks") && !jsonObject.getString("bdsfstyrmrks").isEmpty() && !jsonObject.getString("bdsfstyrmrks").equalsIgnoreCase("null")) {
                user.setBdsfstyrmrks(jsonObject.getString("bdsfstyrmrks"));
            }
            if (jsonObject.has("bdsfstyrtotmrks") && !jsonObject.getString("bdsfstyrtotmrks").isEmpty() && !jsonObject.getString("bdsfstyrtotmrks").equalsIgnoreCase("null")) {
                user.setBdsfstyrtotmrks(jsonObject.getString("bdsfstyrtotmrks"));
            }
            if (jsonObject.has("bdssecyrmrks") && !jsonObject.getString("bdssecyrmrks").isEmpty() && !jsonObject.getString("bdssecyrmrks").equalsIgnoreCase("null")) {
                user.setBdssecyrmrks(jsonObject.getString("bdssecyrmrks"));
            }
            if (jsonObject.has("bdssecyrtotmrks") && !jsonObject.getString("bdssecyrtotmrks").isEmpty() && !jsonObject.getString("bdssecyrtotmrks").equalsIgnoreCase("null")) {
                user.setBdssecyrtotmrks(jsonObject.getString("bdssecyrtotmrks"));
            }
            if (jsonObject.has("bdsthrdyrmrks") && !jsonObject.getString("bdsthrdyrmrks").isEmpty() && !jsonObject.getString("bdsthrdyrmrks").equalsIgnoreCase("null")) {
                user.setBdsthrdyrmrks(jsonObject.getString("bdsthrdyrmrks"));
            }
            if (jsonObject.has("bdsthrdyrtotmrks") && !jsonObject.getString("bdsthrdyrtotmrks").isEmpty() && !jsonObject.getString("bdsthrdyrtotmrks").equalsIgnoreCase("null")) {
                user.setBdsthrdyrtotmrks(jsonObject.getString("bdsthrdyrtotmrks"));
            }
            if (jsonObject.has("bdsfinlyrmrks") && !jsonObject.getString("bdsfinlyrmrks").isEmpty() && !jsonObject.getString("bdsfinlyrmrks").equalsIgnoreCase("null")) {
                user.setBdsfinlyrmrks(jsonObject.getString("bdsfinlyrmrks"));
            }
            if (jsonObject.has("bdsfinlyrtotmrks") && !jsonObject.getString("bdsfinlyrtotmrks").isEmpty() && !jsonObject.getString("bdsfinlyrtotmrks").equalsIgnoreCase("null")) {
                user.setBdsfinlyrtotmrks(jsonObject.getString("bdsfinlyrtotmrks"));
            }
            if (jsonObject.has("mother_name") && !jsonObject.getString("mother_name").isEmpty() && !jsonObject.getString("mother_name").equalsIgnoreCase("null")) {
                user.setMother_name(jsonObject.getString("mother_name"));
            }

            if (jsonObject.has("gndfathermobile") && !jsonObject.getString("gndfathermobile").isEmpty() && !jsonObject.getString("gndfathermobile").equalsIgnoreCase("null")) {
                user.setGndfathermobile(jsonObject.getString("gndfathermobile"));
            }
            if (jsonObject.has("gndmothermobile") && !jsonObject.getString("gndmothermobile").isEmpty() && !jsonObject.getString("gndmothermobile").equalsIgnoreCase("null")) {
                user.setGndmothermobile(jsonObject.getString("gndmothermobile"));
            }

            if (jsonObject.has("fatheroccu") && !jsonObject.getString("fatheroccu").isEmpty() && !jsonObject.getString("fatheroccu").equalsIgnoreCase("null")) {
                user.setFatheroccu(jsonObject.getString("fatheroccu"));
            }
            if (jsonObject.has("motheroccu") && !jsonObject.getString("motheroccu").isEmpty() && !jsonObject.getString("motheroccu").equalsIgnoreCase("null")) {
                user.setMotheroccu(jsonObject.getString("motheroccu"));
            }
            if (jsonObject.has("fatherage") && !jsonObject.getString("fatherage").isEmpty() && !jsonObject.getString("fatherage").equalsIgnoreCase("null")) {
                user.setFatherage(jsonObject.getString("fatherage"));
            }
            if (jsonObject.has("motherage") && !jsonObject.getString("motherage").isEmpty() && !jsonObject.getString("motherage").equalsIgnoreCase("null")) {
                user.setMotherage(jsonObject.getString("motherage"));
            }
            if (jsonObject.has("fathereduquali") && !jsonObject.getString("fathereduquali").isEmpty() && !jsonObject.getString("fathereduquali").equalsIgnoreCase("null")) {
                user.setFathereduquali(jsonObject.getString("fathereduquali"));
            }
            if (jsonObject.has("mothereduquali") && !jsonObject.getString("mothereduquali").isEmpty() && !jsonObject.getString("mothereduquali").equalsIgnoreCase("null")) {
                user.setMothereduquali(jsonObject.getString("mothereduquali"));
            }
            if (jsonObject.has("castetype") && !jsonObject.getString("castetype").isEmpty() && !jsonObject.getString("castetype").equalsIgnoreCase("null")) {
                user.setCastetype(jsonObject.getString("castetype"));
            }
            if (jsonObject.has("section_name") && !jsonObject.getString("section_name").isEmpty() && !jsonObject.getString("section_name").equalsIgnoreCase("null")) {
                user.setSection_name(jsonObject.getString("section_name"));
            }
            if (jsonObject.has("es_morning_vid") && !jsonObject.getString("es_morning_vid").isEmpty() && !jsonObject.getString("es_morning_vid").equalsIgnoreCase("null")) {
                user.setEs_morning_vid(jsonObject.getString("es_morning_vid"));
            }
            if (jsonObject.has("device_token") && !jsonObject.getString("device_token").isEmpty() && !jsonObject.getString("device_token").equalsIgnoreCase("null")) {
                user.setDevice_token(jsonObject.getString("device_token"));
            }

            if (jsonObject.has("place_id") && !jsonObject.getString("place_id").isEmpty() && !jsonObject.getString("place_id").equalsIgnoreCase("null")) {
                user.setPlace_id(jsonObject.getString("place_id"));
            }
            if (jsonObject.has("route_id") && !jsonObject.getString("route_id").isEmpty() && !jsonObject.getString("route_id").equalsIgnoreCase("null")) {
                user.setRoute_id(jsonObject.getString("route_id"));
            }


            if (jsonObject.has("mobile_name") && !jsonObject.getString("mobile_name").isEmpty() && !jsonObject.getString("mobile_name").equalsIgnoreCase("null")) {
                user.setMobile_name(jsonObject.getString("mobile_name"));
            }
            if (jsonObject.has("mobile_brand") && !jsonObject.getString("mobile_brand").isEmpty() && !jsonObject.getString("mobile_brand").equalsIgnoreCase("null")) {
                user.setMobile_brand(jsonObject.getString("mobile_brand"));
            }


            if (jsonObject.has("es_evening_vid") && !jsonObject.getString("es_evening_vid").isEmpty() && !jsonObject.getString("es_evening_vid").equalsIgnoreCase("null")) {
                user.setEs_evening_vid(jsonObject.getString("es_evening_vid"));
            }
            if (jsonObject.has("village") && !jsonObject.getString("village").isEmpty() && !jsonObject.getString("village").equalsIgnoreCase("null")) {
                user.setVillage(jsonObject.getString("village"));
            }

            if (jsonObject.has("aadhar_image") && !jsonObject.getString("aadhar_image").isEmpty() && !jsonObject.getString("aadhar_image").equalsIgnoreCase("null")) {
                user.setAadhar_image(Constants.getImageBaseURL() + jsonObject.getString("aadhar_image"));
            }

            if (jsonObject.has("tenmarksheet_image") && !jsonObject.getString("tenmarksheet_image").isEmpty() && !jsonObject.getString("tenmarksheet_image").equalsIgnoreCase("null")) {
                user.setTenmarksheet_image(Constants.getImageBaseURL() + jsonObject.getString("tenmarksheet_image"));
            }

            if (jsonObject.has("twelvemarksheet_image") && !jsonObject.getString("twelvemarksheet_image").isEmpty() && !jsonObject.getString("twelvemarksheet_image").equalsIgnoreCase("null")) {
                user.setTwelvemarksheet_image(Constants.getImageBaseURL() + jsonObject.getString("twelvemarksheet_image"));
            }

            if (jsonObject.has("schol_certif_image") && !jsonObject.getString("schol_certif_image").isEmpty() && !jsonObject.getString("schol_certif_image").equalsIgnoreCase("null")) {
                user.setSchol_certif_image(Constants.getImageBaseURL() + jsonObject.getString("schol_certif_image"));
            }

            if (jsonObject.has("schol_trans_certif_image") && !jsonObject.getString("schol_trans_certif_image").isEmpty() && !jsonObject.getString("schol_trans_certif_image").equalsIgnoreCase("null")) {
                user.setSchol_trans_certif_image(Constants.getImageBaseURL() + jsonObject.getString("schol_trans_certif_image"));
            }


            if (jsonObject.has("graduation_image") && !jsonObject.getString("graduation_image").isEmpty() && !jsonObject.getString("graduation_image").equalsIgnoreCase("null")) {
                user.setGraduation_image(Constants.getImageBaseURL() + jsonObject.getString("graduation_image"));
            }

            if (jsonObject.has("aadhar_pdf") && !jsonObject.getString("aadhar_pdf").isEmpty() && !jsonObject.getString("aadhar_pdf").equalsIgnoreCase("null")) {
                user.setAadhar_pdf(Constants.getImageBaseURL() + jsonObject.getString("aadhar_pdf"));
            }

            if (jsonObject.has("tenmarksheet_pdf") && !jsonObject.getString("tenmarksheet_pdf").isEmpty() && !jsonObject.getString("tenmarksheet_pdf").equalsIgnoreCase("null")) {
                user.setTenmarksheet_pdf(Constants.getImageBaseURL() + jsonObject.getString("tenmarksheet_pdf"));
            }

            if (jsonObject.has("twelvemarksheet_pdf") && !jsonObject.getString("twelvemarksheet_pdf").isEmpty() && !jsonObject.getString("twelvemarksheet_pdf").equalsIgnoreCase("null")) {
                user.setTwelvemarksheet_pdf(Constants.getImageBaseURL() + jsonObject.getString("twelvemarksheet_pdf"));
            }

            if (jsonObject.has("schol_certif_pdf") && !jsonObject.getString("schol_certif_pdf").isEmpty() && !jsonObject.getString("schol_certif_pdf").equalsIgnoreCase("null")) {
                user.setSchol_certif_pdf(Constants.getImageBaseURL() + jsonObject.getString("schol_certif_pdf"));
            }

            if (jsonObject.has("schol_trans_certif_pdf") && !jsonObject.getString("schol_trans_certif_pdf").isEmpty() && !jsonObject.getString("schol_trans_certif_pdf").equalsIgnoreCase("null")) {
                user.setSchol_trans_certif_pdf(Constants.getImageBaseURL() + jsonObject.getString("schol_trans_certif_pdf"));
            }

            if (jsonObject.has("at_attendance_date") && !jsonObject.getString("at_attendance_date").isEmpty() && !jsonObject.getString("at_attendance_date").equalsIgnoreCase("null")) {
                user.setAt_attendance_date(jsonObject.getString("at_attendance_date"));
            }

            if (jsonObject.has("age") && !jsonObject.getString("age").isEmpty() && !jsonObject.getString("age").equalsIgnoreCase("null")) {
                user.setAge(jsonObject.getString("age"));
            }

            if (jsonObject.has("graduation_pdf") && !jsonObject.getString("graduation_pdf").isEmpty() && !jsonObject.getString("graduation_pdf").equalsIgnoreCase("null")) {
                user.setGraduation_pdf(Constants.getImageBaseURL() + jsonObject.getString("graduation_pdf"));
            }

            if (jsonObject.has("date_time") && !jsonObject.getString("date_time").isEmpty() && !jsonObject.getString("date_time").equalsIgnoreCase("null")) {
                user.setDate_time(jsonObject.getString("date_time"));
            }


            if (jsonObject.has("admin_fname") && !jsonObject.getString("admin_fname").isEmpty() && !jsonObject.getString("admin_fname").equalsIgnoreCase("null")) {
                user.setAdmin_fname(jsonObject.getString("admin_fname"));
            }

            if (jsonObject.has("a_signature") && !jsonObject.getString("a_signature").isEmpty() && !jsonObject.getString("a_signature").equalsIgnoreCase("null")) {
                user.setA_signature(jsonObject.getString("a_signature"));
            }

            if (jsonObject.has("st_firstname") && !jsonObject.getString("st_firstname").isEmpty() && !jsonObject.getString("st_firstname").equalsIgnoreCase("null")) {
                user.setSt_firstname(jsonObject.getString("st_firstname"));
            }

            if (jsonObject.has("s_signature") && !jsonObject.getString("s_signature").isEmpty() && !jsonObject.getString("s_signature").equalsIgnoreCase("null")) {
                user.setS_signature(jsonObject.getString("s_signature"));
            }


            if (jsonObject.has("jan_id") && !jsonObject.getString("jan_id").isEmpty() && !jsonObject.getString("jan_id").equalsIgnoreCase("null")) {
                user.setJan_id(jsonObject.getString("jan_id"));
            }
            if (jsonObject.has("jan_month") && !jsonObject.getString("jan_month").isEmpty() && !jsonObject.getString("jan_month").equalsIgnoreCase("null")) {
                user.setJan_month(jsonObject.getString("jan_month"));
            }
            if (jsonObject.has("jan_paid") && !jsonObject.getString("jan_paid").isEmpty() && !jsonObject.getString("jan_paid").equalsIgnoreCase("null")) {
                user.setJan_paid(jsonObject.getString("jan_paid"));
            }
            if (jsonObject.has("feb_id") && !jsonObject.getString("feb_id").isEmpty() && !jsonObject.getString("feb_id").equalsIgnoreCase("null")) {
                user.setFeb_id(jsonObject.getString("feb_id"));
            }
            if (jsonObject.has("feb_month") && !jsonObject.getString("feb_month").isEmpty() && !jsonObject.getString("feb_month").equalsIgnoreCase("null")) {
                user.setFeb_month(jsonObject.getString("feb_month"));
            }
            if (jsonObject.has("feb_paid") && !jsonObject.getString("feb_paid").isEmpty() && !jsonObject.getString("feb_paid").equalsIgnoreCase("null")) {
                user.setFeb_paid(jsonObject.getString("feb_paid"));
            }
            if (jsonObject.has("mar_id") && !jsonObject.getString("mar_id").isEmpty() && !jsonObject.getString("mar_id").equalsIgnoreCase("null")) {
                user.setMar_id(jsonObject.getString("mar_id"));
            }
            if (jsonObject.has("mar_month") && !jsonObject.getString("mar_month").isEmpty() && !jsonObject.getString("mar_month").equalsIgnoreCase("null")) {
                user.setMar_month(jsonObject.getString("mar_month"));
            }
            if (jsonObject.has("mar_paid") && !jsonObject.getString("mar_paid").isEmpty() && !jsonObject.getString("mar_paid").equalsIgnoreCase("null")) {
                user.setMar_paid(jsonObject.getString("mar_paid"));
            }

            if (jsonObject.has("apr_id") && !jsonObject.getString("apr_id").isEmpty() && !jsonObject.getString("apr_id").equalsIgnoreCase("null")) {
                user.setApr_id(jsonObject.getString("apr_id"));
            }
            if (jsonObject.has("apr_paid") && !jsonObject.getString("apr_paid").isEmpty() && !jsonObject.getString("apr_paid").equalsIgnoreCase("null")) {
                user.setApr_paid(jsonObject.getString("apr_paid"));
            }
            if (jsonObject.has("apr_month") && !jsonObject.getString("apr_month").isEmpty() && !jsonObject.getString("apr_month").equalsIgnoreCase("null")) {
                user.setApr_month(jsonObject.getString("apr_month"));
            }
            if (jsonObject.has("may_id") && !jsonObject.getString("may_id").isEmpty() && !jsonObject.getString("may_id").equalsIgnoreCase("null")) {
                user.setMay_id(jsonObject.getString("may_id"));
            }
            if (jsonObject.has("may_month") && !jsonObject.getString("may_month").isEmpty() && !jsonObject.getString("may_month").equalsIgnoreCase("null")) {
                user.setMay_month(jsonObject.getString("may_month"));
            }
            if (jsonObject.has("may_paid") && !jsonObject.getString("may_paid").isEmpty() && !jsonObject.getString("may_paid").equalsIgnoreCase("null")) {
                user.setMay_paid(jsonObject.getString("may_paid"));
            }
            if (jsonObject.has("june_id") && !jsonObject.getString("june_id").isEmpty() && !jsonObject.getString("june_id").equalsIgnoreCase("null")) {
                user.setJune_id(jsonObject.getString("june_id"));
            }
            if (jsonObject.has("june_month") && !jsonObject.getString("june_month").isEmpty() && !jsonObject.getString("june_month").equalsIgnoreCase("null")) {
                user.setJune_month(jsonObject.getString("june_month"));
            }
            if (jsonObject.has("june_paid") && !jsonObject.getString("june_paid").isEmpty() && !jsonObject.getString("june_paid").equalsIgnoreCase("null")) {
                user.setJune_paid(jsonObject.getString("june_paid"));
            }

            if (jsonObject.has("july_id") && !jsonObject.getString("july_id").isEmpty() && !jsonObject.getString("july_id").equalsIgnoreCase("null")) {
                user.setJuly_id(jsonObject.getString("july_id"));
            }
            if (jsonObject.has("july_month") && !jsonObject.getString("july_month").isEmpty() && !jsonObject.getString("july_month").equalsIgnoreCase("null")) {
                user.setJuly_month(jsonObject.getString("july_month"));
            }
            if (jsonObject.has("july_paid") && !jsonObject.getString("july_paid").isEmpty() && !jsonObject.getString("july_paid").equalsIgnoreCase("null")) {
                user.setJuly_paid(jsonObject.getString("july_paid"));
            }
            if (jsonObject.has("aug_id") && !jsonObject.getString("aug_id").isEmpty() && !jsonObject.getString("aug_id").equalsIgnoreCase("null")) {
                user.setAug_id(jsonObject.getString("aug_id"));
            }
            if (jsonObject.has("aug_month") && !jsonObject.getString("aug_month").isEmpty() && !jsonObject.getString("aug_month").equalsIgnoreCase("null")) {
                user.setAug_month(jsonObject.getString("aug_month"));
            }
            if (jsonObject.has("aug_paid") && !jsonObject.getString("aug_paid").isEmpty() && !jsonObject.getString("aug_paid").equalsIgnoreCase("null")) {
                user.setAug_paid(jsonObject.getString("aug_paid"));
            }
            if (jsonObject.has("sep_id") && !jsonObject.getString("sep_id").isEmpty() && !jsonObject.getString("sep_id").equalsIgnoreCase("null")) {
                user.setSep_id(jsonObject.getString("sep_id"));
            }
            if (jsonObject.has("sep_month") && !jsonObject.getString("sep_month").isEmpty() && !jsonObject.getString("sep_month").equalsIgnoreCase("null")) {
                user.setSep_month(jsonObject.getString("sep_month"));
            }
            if (jsonObject.has("sep_paid") && !jsonObject.getString("sep_paid").isEmpty() && !jsonObject.getString("sep_paid").equalsIgnoreCase("null")) {
                user.setSep_paid(jsonObject.getString("sep_paid"));
            }

            if (jsonObject.has("oct_id") && !jsonObject.getString("oct_id").isEmpty() && !jsonObject.getString("oct_id").equalsIgnoreCase("null")) {
                user.setOct_id(jsonObject.getString("oct_id"));
            }
            if (jsonObject.has("oct_month") && !jsonObject.getString("oct_month").isEmpty() && !jsonObject.getString("oct_month").equalsIgnoreCase("null")) {
                user.setOct_month(jsonObject.getString("oct_month"));
            }
            if (jsonObject.has("oct_paid") && !jsonObject.getString("oct_paid").isEmpty() && !jsonObject.getString("oct_paid").equalsIgnoreCase("null")) {
                user.setOct_paid(jsonObject.getString("oct_paid"));
            }
            if (jsonObject.has("nov_id") && !jsonObject.getString("nov_id").isEmpty() && !jsonObject.getString("nov_id").equalsIgnoreCase("null")) {
                user.setNov_id(jsonObject.getString("nov_id"));
            }
            if (jsonObject.has("nov_month") && !jsonObject.getString("nov_month").isEmpty() && !jsonObject.getString("nov_month").equalsIgnoreCase("null")) {
                user.setNov_month(jsonObject.getString("nov_month"));
            }
            if (jsonObject.has("nov_paid") && !jsonObject.getString("nov_paid").isEmpty() && !jsonObject.getString("nov_paid").equalsIgnoreCase("null")) {
                user.setNov_paid(jsonObject.getString("nov_paid"));
            }
            if (jsonObject.has("dec_id") && !jsonObject.getString("dec_id").isEmpty() && !jsonObject.getString("dec_id").equalsIgnoreCase("null")) {
                user.setDec_id(jsonObject.getString("dec_id"));
            }
            if (jsonObject.has("dec_month") && !jsonObject.getString("dec_month").isEmpty() && !jsonObject.getString("dec_month").equalsIgnoreCase("null")) {
                user.setDec_month(jsonObject.getString("dec_month"));
            }
            if (jsonObject.has("dec_paid") && !jsonObject.getString("dec_paid").isEmpty() && !jsonObject.getString("dec_paid").equalsIgnoreCase("null")) {
                user.setDec_paid(jsonObject.getString("dec_paid"));
            }
            if (jsonObject.has("es_housename") && !jsonObject.getString("es_housename").isEmpty() && !jsonObject.getString("es_housename").equalsIgnoreCase("null")) {
                user.setEs_housename(jsonObject.getString("es_housename"));
            }
            if (jsonObject.has("es_housesid") && !jsonObject.getString("es_housesid").isEmpty() && !jsonObject.getString("es_housesid").equalsIgnoreCase("null")) {
                user.setEs_housesid(jsonObject.getString("es_housesid"));
            }

            if (jsonObject.has("pre_prev_class") && !jsonObject.getString("pre_prev_class").isEmpty() && !jsonObject.getString("pre_prev_class").equalsIgnoreCase("null")) {
                user.setPre_prev_class(jsonObject.getString("pre_prev_class"));
            }
            if (jsonObject.has("pre_prev_school") && !jsonObject.getString("pre_prev_school").isEmpty() && !jsonObject.getString("pre_prev_school").equalsIgnoreCase("null")) {
                user.setPre_prev_school(jsonObject.getString("pre_prev_school"));
            }
            if (jsonObject.has("reason_for_leave_school") && !jsonObject.getString("reason_for_leave_school").isEmpty() && !jsonObject.getString("reason_for_leave_school").equalsIgnoreCase("null")) {
                user.setReason_for_leave_school(jsonObject.getString("reason_for_leave_school"));
            }

            if (jsonObject.has("no_of_siblings") && !jsonObject.getString("no_of_siblings").isEmpty() && !jsonObject.getString("no_of_siblings").equalsIgnoreCase("null")) {
                user.setNo_of_siblings(jsonObject.getString("no_of_siblings"));
            }
            if (jsonObject.has("no_of_siblings_other_school") && !jsonObject.getString("no_of_siblings_other_school").isEmpty() && !jsonObject.getString("no_of_siblings_other_school").equalsIgnoreCase("null")) {
                user.setNo_of_siblings_other_school(jsonObject.getString("no_of_siblings_other_school"));
            }
            if (jsonObject.has("no_of_siblings_in_school") && !jsonObject.getString("no_of_siblings_in_school").isEmpty() && !jsonObject.getString("no_of_siblings_in_school").equalsIgnoreCase("null")) {
                user.setNo_of_siblings_in_school(jsonObject.getString("no_of_siblings_in_school"));
            }

            if (jsonObject.has("distance_from_school") && !jsonObject.getString("distance_from_school").isEmpty() && !jsonObject.getString("distance_from_school").equalsIgnoreCase("null")) {
                user.setDistance_from_school(jsonObject.getString("distance_from_school"));
            }
            if (jsonObject.has("qualification") && !jsonObject.getString("qualification").isEmpty() && !jsonObject.getString("qualification").equalsIgnoreCase("null")) {
                user.setQualification(jsonObject.getString("qualification"));
            }

            if (jsonObject.has("pre_emailid") && !jsonObject.getString("pre_emailid").isEmpty() && !jsonObject.getString("pre_emailid").equalsIgnoreCase("null")) {
                user.setPre_emailid(jsonObject.getString("pre_emailid"));
            }
            if (jsonObject.has("designation") && !jsonObject.getString("designation").isEmpty() && !jsonObject.getString("designation").equalsIgnoreCase("null")) {
                user.setDesignation(jsonObject.getString("designation"));
            }

            if (jsonObject.has("reference") && !jsonObject.getString("reference").isEmpty() && !jsonObject.getString("reference").equalsIgnoreCase("null")) {
                user.setReference(jsonObject.getString("reference"));
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }
        return user;
    }

    public String getClass_name() {
        return class_name;
    }

    public void setClass_name(String class_name) {
        this.class_name = class_name;
    }

    public String getFather_name() {
        return father_name;
    }

    public void setFather_name(String father_name) {
        this.father_name = father_name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }

    public String getBg() {
        return bg;
    }

    public void setBg(String bg) {
        this.bg = bg;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getAadhaar() {
        return aadhaar;
    }

    public void setAadhaar(String aadhaar) {
        this.aadhaar = aadhaar;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getF_name() {
        return f_name;
    }

    public void setF_name(String f_name) {
        this.f_name = f_name;
    }

    public String getL_name() {
        return l_name;
    }

    public void setL_name(String l_name) {
        this.l_name = l_name;
    }

    private String f_name;
    private String l_name;


    public String getClass_id() {
        return class_id;
    }

    public void setClass_id(String class_id) {
        this.class_id = class_id;
    }

    public String getR_id() {
        return r_id;
    }

    public void setR_id(String r_id) {
        this.r_id = r_id;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public String getRoll_no() {
        return roll_no;
    }

    public void setRoll_no(String roll_no) {
        this.roll_no = roll_no;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneno() {
        return phoneno;
    }

    public void setPhoneno(String phoneno) {
        this.phoneno = phoneno;
    }


    public String getCroll_no() {
        return croll_no;
    }

    public void setCroll_no(String croll_no) {
        this.croll_no = croll_no;
    }
}




