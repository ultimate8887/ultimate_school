package com.ultimate.ultimatesmartschool.BeanModule;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class CommonBean {

    private static String ID = "id";
    private static String NAME = "name";
    private static String CREATED_DATE = "created_on";
    private static String DATE = "date";
    private static String S_NO = "serial_no";
    /**
     * id : 2
     * name : NURSERY
     */

    private String id;
    private String name;
    private String createdOn;
    private String batch;

    public String getSerial_no() {
        return serial_no;
    }

    public void setSerial_no(String serial_no) {
        this.serial_no = serial_no;
    }

    private String serial_no;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    private String date;

    public String getBatch() {
        return batch;
    }

    public void setBatch(String batch) {
        this.batch = batch;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static ArrayList<CommonBean> parseCommonArray(JSONArray arrayObj) {
        ArrayList list = new ArrayList<CommonBean>();
        try {
            for (int i = 0; i < arrayObj.length(); i++) {
                CommonBean p = parseCommonObject(arrayObj.getJSONObject(i));
                if (p != null) {
                    list.add(p);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public static CommonBean parseCommonObject(JSONObject jsonObject) {
        CommonBean casteObj = new CommonBean();
        try {
            if (jsonObject.has(ID)) {
                casteObj.setId(jsonObject.getString(ID));
            }
            if (jsonObject.has(NAME) && !jsonObject.getString(NAME).isEmpty() && !jsonObject.getString(NAME).equalsIgnoreCase("null")) {
                casteObj.setName(jsonObject.getString(NAME));
            }
            if (jsonObject.has("batch") && !jsonObject.getString("batch").isEmpty() && !jsonObject.getString("batch").equalsIgnoreCase("null")) {
                casteObj.setBatch(jsonObject.getString("batch"));
            }
            if (jsonObject.has(CREATED_DATE) && !jsonObject.getString(CREATED_DATE).isEmpty() && !jsonObject.getString(CREATED_DATE).equalsIgnoreCase("null")) {

                casteObj.setCreatedOn(jsonObject.getString(CREATED_DATE));
            }
            if (jsonObject.has(DATE)) {
                casteObj.setDate(jsonObject.getString(DATE));
            }
            if (jsonObject.has(S_NO)) {
                casteObj.setSerial_no(jsonObject.getString(S_NO));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return casteObj;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getCreatedOn() {
        return createdOn;
    }
}
