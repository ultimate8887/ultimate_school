package com.ultimate.ultimatesmartschool.BeanModule;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class SectionBean {



    private static String ID = "section_id";
    private static String NAME = "section_name";

    private String section_id;
    private String section_name;

    public String getSection_id() {
        return section_id;
    }

    public void setSection_id(String section_id) {
        this.section_id = section_id;
    }

    public String getSection_name() {
        return section_name;
    }

    public void setSection_name(String section_name) {
        this.section_name = section_name;
    }

    public static ArrayList<SectionBean> parseCommonArray(JSONArray arrayObj) {
        ArrayList list = new ArrayList<SectionBean>();
        try {
            for (int i = 0; i < arrayObj.length(); i++) {
                SectionBean p = parseCommonObject(arrayObj.getJSONObject(i));
                if (p != null) {
                    list.add(p);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public static SectionBean parseCommonObject(JSONObject jsonObject) {
        SectionBean casteObj = new SectionBean();
        try {

            if (jsonObject.has(NAME) && !jsonObject.getString(NAME).isEmpty() && !jsonObject.getString(NAME).equalsIgnoreCase("null")) {
                casteObj.setSection_name(jsonObject.getString(NAME));
            }
            if (jsonObject.has(ID) && !jsonObject.getString(ID).isEmpty() && !jsonObject.getString(ID).equalsIgnoreCase("null")) {
                casteObj.setSection_id(jsonObject.getString(ID));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return casteObj;
    }
}
