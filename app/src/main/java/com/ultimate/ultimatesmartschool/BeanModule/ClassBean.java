package com.ultimate.ultimatesmartschool.BeanModule;

import com.ultimate.ultimatesmartschool.Utility.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ClassBean {

    private static String ID = "id";
    private static String NAME = "name";
    /**
     * id : 2
     * name : NURSERY
     * school_id : 0
     * group_id : 9
     * order_by : 1
     */

    private String id;
    private String name;
    private String school_id;
    private String order_by;
    private String section_id;

    private String male_student;

    public String getMale_student() {
        return male_student;
    }

    public void setMale_student(String male_student) {
        this.male_student = male_student;
    }

    public String getFemale_student() {
        return female_student;
    }

    public void setFemale_student(String female_student) {
        this.female_student = female_student;
    }

    private String female_student;

    public String getTotal_std() {
        return total_std;
    }

    public void setTotal_std(String total_std) {
        this.total_std = total_std;
    }

    private String total_std;

    public String getSection_name() {
        return section_name;
    }

    public void setSection_name(String section_name) {
        this.section_name = section_name;
    }

    private String section_name;

    private String p_student;

    public String getP_student() {
        return p_student;
    }

    public void setP_student(String p_student) {
        this.p_student = p_student;
    }

    public String getA_student() {
        return a_student;
    }

    public void setA_student(String a_student) {
        this.a_student = a_student;
    }

    public String getL_student() {
        return l_student;
    }

    public void setL_student(String l_student) {
        this.l_student = l_student;
    }

    private String a_student;
    private String l_student;

    public String getSection_id() {
        return section_id;
    }

    public void setSection_id(String section_id) {
        this.section_id = section_id;
    }

    private boolean checked = false;

    /**
     * group_data : {"id":"1","name":"Pre-Primary(Nry-Prep2)"}
     */

    public CommonBean getGroup_data() {
        return group_data;
    }

    public void setGroup_data(CommonBean group_data) {
        this.group_data = group_data;
    }

    private CommonBean group_data;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSchool_id() {
        return school_id;
    }

    public void setSchool_id(String school_id) {
        this.school_id = school_id;
    }


    public String getOrder_by() {
        return order_by;
    }

    public void setOrder_by(String order_by) {
        this.order_by = order_by;
    }

    public static ArrayList<ClassBean> parseClassArray(JSONArray arrayObj) {
        ArrayList list = new ArrayList<ClassBean>();
        try {
            for (int i = 0; i < arrayObj.length(); i++) {
                ClassBean p = parseClassObject(arrayObj.getJSONObject(i));
                if (p != null) {
                    list.add(p);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public static ClassBean parseClassObject(JSONObject jsonObject) {
        ClassBean casteObj = new ClassBean();
        try {
            if (jsonObject.has(ID)) {
                casteObj.setId(jsonObject.getString(ID));
            }
            if (jsonObject.has(NAME) && !jsonObject.getString(NAME).isEmpty() && !jsonObject.getString(NAME).equalsIgnoreCase("null")) {
                casteObj.setName(jsonObject.getString(NAME));
            }
            if (jsonObject.has(Constants.GROUP_DATA)) {
                casteObj.setGroup_data(CommonBean.parseCommonObject(jsonObject.getJSONObject(Constants.GROUP_DATA)));
            }
            if (jsonObject.has("section_id") && !jsonObject.getString("section_id").isEmpty() && !jsonObject.getString("section_id").equalsIgnoreCase("null")) {
                casteObj.setSection_id(jsonObject.getString("section_id"));
            }

            if (jsonObject.has("section_name") && !jsonObject.getString("section_name").isEmpty() && !jsonObject.getString("section_name").equalsIgnoreCase("null")) {
                casteObj.setSection_name(jsonObject.getString("section_name"));
            }

            if (jsonObject.has("total_std") && !jsonObject.getString("total_std").isEmpty() && !jsonObject.getString("total_std").equalsIgnoreCase("null")) {
                casteObj.setTotal_std(jsonObject.getString("total_std"));
            }

            if (jsonObject.has("p_student") && !jsonObject.getString("p_student").isEmpty() && !jsonObject.getString("p_student").equalsIgnoreCase("null")) {
                casteObj.setP_student(jsonObject.getString("p_student"));
            }

            if (jsonObject.has("a_student") && !jsonObject.getString("a_student").isEmpty() && !jsonObject.getString("a_student").equalsIgnoreCase("null")) {
                casteObj.setA_student(jsonObject.getString("a_student"));
            }

            if (jsonObject.has("l_student") && !jsonObject.getString("l_student").isEmpty() && !jsonObject.getString("l_student").equalsIgnoreCase("null")) {
                casteObj.setL_student(jsonObject.getString("l_student"));
            }

            if (jsonObject.has("female_student") && !jsonObject.getString("female_student").isEmpty() && !jsonObject.getString("female_student").equalsIgnoreCase("null")) {
                casteObj.setFemale_student(jsonObject.getString("female_student"));
            }

            if (jsonObject.has("male_student") && !jsonObject.getString("male_student").isEmpty() && !jsonObject.getString("male_student").equalsIgnoreCase("null")) {
                casteObj.setMale_student(jsonObject.getString("male_student"));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return casteObj;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public boolean isChecked() {
        return checked;
    }

//
//    public void setChecked(boolean checked) {
//        this.checked = checked;
//    }
//
//    public boolean isChecked() {
//        return checked;
//    }
}
