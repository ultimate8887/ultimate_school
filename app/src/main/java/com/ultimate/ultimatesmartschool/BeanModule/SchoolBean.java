package com.ultimate.ultimatesmartschool.BeanModule;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.ultimate.ultimatesmartschool.UltimateSchoolApp;
import com.ultimate.ultimatesmartschool.Utility.Constants;

import org.json.JSONException;
import org.json.JSONObject;

public class SchoolBean {
    private static String ID = "id";
    private static String NAME = "name";
    private static String ADDRESS = "address";
    private static String LOGO = "logo";
    private static String PHONENO = "phoneno";
    private static String EMAIL = "email";
    private static String WEBSITE = "website";
    private static String URL = "url";
    private static String ShowDsr = "show_dsr";
    private static String ShowLeave = "show_leave";
    private static String DB_HOST = "dbhost";
    private static String DB_USER = "dbuser";
    private static String DB_PASS = "dbpass";
    private static String DB_NAME = "dbname";
    private static String SCHOOL_TIMMING = "school_timming";
    private static String SCHOOL_ID_KEY="fi_school_id";
    private static String NO_OF_SUBJECT = "fi_no_subjects";

    /**
     * id : 1
     * address : Bathinda
     * logo : st_12272017_041245_logo (2).png
     * phoneno : 8264883507
     * email : ravinder@smartedgeindia.com
     * website :
     * name : Demo
     */

    private String id;
    private String address;
    private String logo;
    private String phoneno;
    private String email;
    private String website;
    private String name;
    private String url;
    private String show_leave;
    /**
     * dbhost : localhost
     * dbuser : smartm79_android
     * dbpass : test1
     * dbname : smartm79_androidtest
     */

    private String dbhost;
    private String dbuser;

    public String getFi_no_subjects() {
        return fi_no_subjects;
    }

    public void setFi_no_subjects(String fi_no_subjects) {
        this.fi_no_subjects = fi_no_subjects;
    }

    private String dbpass;
    private String dbname;
    private String school_timming="no";
    private String fi_no_subjects;
    private String fi_school_id;

    public String getFi_school_id() {
        return fi_school_id;
    }

    public void setFi_school_id(String fi_school_id) {
        this.fi_school_id = fi_school_id;
    }
//    public String getSchool_id() {
//        return school_id;
//    }
//
//    public void setSchool_id(String school_id) {
//        this.school_id = school_id;
//    }

    public String getShow_dsr() {
        return show_dsr;
    }

    public void setShow_dsr(String show_dsr) {
        this.show_dsr = show_dsr;
    }

    private String show_dsr;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getPhoneno() {
        return phoneno;
    }

    public void setPhoneno(String phoneno) {
        this.phoneno = phoneno;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static SchoolBean parseSchoolObject(JSONObject jsonObject) {
        SchoolBean schoolObj = new SchoolBean();
        try {
            if (jsonObject.has(ID)) {
                schoolObj.setId(jsonObject.getString(ID));
            }
            if (jsonObject.has(EMAIL) && !jsonObject.getString(EMAIL).isEmpty() && !jsonObject.getString(EMAIL).equalsIgnoreCase("null")) {
                schoolObj.setEmail(jsonObject.getString(EMAIL));
            }
            if (jsonObject.has(NAME) && !jsonObject.getString(NAME).isEmpty() && !jsonObject.getString(NAME).equalsIgnoreCase("null")) {
                schoolObj.setName(jsonObject.getString(NAME));
            }
            if (jsonObject.has(PHONENO) && !jsonObject.getString(PHONENO).isEmpty() && !jsonObject.getString(PHONENO).equalsIgnoreCase("null")) {
                schoolObj.setPhoneno(jsonObject.getString(PHONENO));
            }
            if (jsonObject.has(ADDRESS) && !jsonObject.getString(ADDRESS).isEmpty() && !jsonObject.getString(ADDRESS).equalsIgnoreCase("null")) {
                schoolObj.setAddress(jsonObject.getString(ADDRESS));
            }
            if (jsonObject.has(LOGO) && !jsonObject.getString(LOGO).isEmpty() && !jsonObject.getString(LOGO).equalsIgnoreCase("null")) {
                schoolObj.setLogo(Constants.getImageBaseURL()+jsonObject.getString(LOGO));
            }
            if (jsonObject.has(WEBSITE) && !jsonObject.getString(WEBSITE).isEmpty() && !jsonObject.getString(WEBSITE).equalsIgnoreCase("null")) {
                schoolObj.setWebsite(jsonObject.getString(WEBSITE));
            }
            if (jsonObject.has(URL) && !jsonObject.getString(URL).isEmpty() && !jsonObject.getString(URL).equalsIgnoreCase("null")) {
                schoolObj.setUrl(jsonObject.getString(URL));

            }
            if (jsonObject.has(ShowDsr) && !jsonObject.getString(ShowDsr).isEmpty() && !jsonObject.getString(ShowDsr).equalsIgnoreCase("null")) {
                schoolObj.setShow_dsr(jsonObject.getString(ShowDsr));
            }
            if (jsonObject.has(ShowLeave) && !jsonObject.getString(ShowLeave).isEmpty() && !jsonObject.getString(ShowLeave).equalsIgnoreCase("null")) {
                schoolObj.setShow_leave(jsonObject.getString(ShowLeave));
            }
            if (jsonObject.has(DB_NAME) && !jsonObject.getString(DB_NAME).isEmpty() && !jsonObject.getString(DB_NAME).equalsIgnoreCase("null")) {
                schoolObj.setDbname(jsonObject.getString(DB_NAME));
            }
            if (jsonObject.has(DB_HOST) && !jsonObject.getString(DB_HOST).isEmpty() && !jsonObject.getString(DB_HOST).equalsIgnoreCase("null")) {
                schoolObj.setDbhost(jsonObject.getString(DB_HOST));
            }
            if (jsonObject.has(DB_PASS) && !jsonObject.getString(DB_PASS).isEmpty() && !jsonObject.getString(DB_PASS).equalsIgnoreCase("null")) {
                schoolObj.setDbpass(jsonObject.getString(DB_PASS));
            }
            if (jsonObject.has(DB_USER) && !jsonObject.getString(DB_USER).isEmpty() && !jsonObject.getString(DB_USER).equalsIgnoreCase("null")) {
                schoolObj.setDbuser(jsonObject.getString(DB_USER));
            }

            if (jsonObject.has(SCHOOL_TIMMING) && !jsonObject.getString(SCHOOL_TIMMING).isEmpty() && !jsonObject.getString(SCHOOL_TIMMING).equalsIgnoreCase("null")) {
                schoolObj.setSchool_timming(jsonObject.getString(SCHOOL_TIMMING));
            }

            if (jsonObject.has(NO_OF_SUBJECT) && !jsonObject.getString(NO_OF_SUBJECT).isEmpty() && !jsonObject.getString(NO_OF_SUBJECT).equalsIgnoreCase("null")) {
                schoolObj.setFi_no_subjects(jsonObject.getString(NO_OF_SUBJECT));
            }

            if (jsonObject.has(SCHOOL_ID_KEY) && !jsonObject.getString(SCHOOL_ID_KEY).isEmpty() && !jsonObject.getString(SCHOOL_ID_KEY).equalsIgnoreCase("null")) {
                schoolObj.setFi_school_id(jsonObject.getString(SCHOOL_ID_KEY));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return schoolObj;
    }

    public void setShow_leave(String show_leave) {
        this.show_leave = show_leave;
    }

    public String getShow_leave() {
        return show_leave;
    }

    public String getDbhost() {
        return dbhost;
    }

    public void setDbhost(String dbhost) {
        this.dbhost = dbhost;
    }

    public String getDbuser() {
        return dbuser;
    }

    public void setDbuser(String dbuser) {
        this.dbuser = dbuser;
    }

    public String getDbpass() {
        return dbpass;
    }

    public void setDbpass(String dbpass) {
        this.dbpass = dbpass;
    }

    public String getDbname() {
        return dbname;
    }

    public void setDbname(String dbname) {
        this.dbname = dbname;
    }

    public String getSchool_timming() {
        return school_timming;
    }

    public void setSchool_timming(String school_timming) {
        this.school_timming = school_timming;
    }
    public void setSchoolTimming(String school_timming) {
        User.getCurrentUser().getSchoolData().setSchool_timming(school_timming);
        Gson gson = new Gson();
        String userString = gson.toJson(User.getCurrentUser(), User.class);
        SharedPreferences preferences = UltimateSchoolApp.getInstance().getSharedPreferences(Constants.PREF_NAME, Context.MODE_PRIVATE);
        preferences.edit().putString(User.PREFRENCES_USER, userString).commit();

    }
}
