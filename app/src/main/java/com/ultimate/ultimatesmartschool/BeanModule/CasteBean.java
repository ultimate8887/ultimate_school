package com.ultimate.ultimatesmartschool.BeanModule;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class CasteBean {


    private static String ID="id";
    private static String CASTE_NAME="caste";
    /**
     * caste_id : 1
     * caste : OBC
     */

    private String caste_id;
    private String caste;
    private String created_on;

    public String getCaste_id() {
        return caste_id;
    }

    public void setCaste_id(String caste_id) {
        this.caste_id = caste_id;
    }

    public String getCaste() {
        return caste;
    }

    public void setCaste(String caste) {
        this.caste = caste;
    }

    public static ArrayList<CasteBean> parseCasteArray(JSONArray arrayObj){
        ArrayList list = new ArrayList<CasteBean>();
        try {
            for (int i = 0; i < arrayObj.length(); i++) {
                CasteBean p =parseCasteObject(arrayObj.getJSONObject(i));
                if(p!= null){
                    list.add(p);
                }}
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public static CasteBean parseCasteObject(JSONObject jsonObject) {
        CasteBean casteObj = new CasteBean();
        try {
            if (jsonObject.has("caste_id")) {
                casteObj.setCaste_id(jsonObject.getString("caste_id"));
            }
            if (jsonObject.has(CASTE_NAME) && !jsonObject.getString(CASTE_NAME).isEmpty() && !jsonObject.getString(CASTE_NAME).equalsIgnoreCase("null")) {
                casteObj.setCaste(jsonObject.getString(CASTE_NAME));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return casteObj;
    }
}
