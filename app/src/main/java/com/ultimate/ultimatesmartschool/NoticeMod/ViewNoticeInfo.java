package com.ultimate.ultimatesmartschool.NoticeMod;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.cuneytayyildiz.gestureimageview.GestureImageView;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.ultimate.ultimatesmartschool.Activity.UserPermissions;
import com.ultimate.ultimatesmartschool.BeanModule.User;
import com.ultimate.ultimatesmartschool.ClassWork.CWViewPagerAdapter;
import com.ultimate.ultimatesmartschool.Ebook.WebViewActivity;
import com.ultimate.ultimatesmartschool.Homework.GETHomeworkActivity;
import com.ultimate.ultimatesmartschool.Login.LoginActivity;
import com.ultimate.ultimatesmartschool.Message.Message_Bean_stafftostaff;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.CommonProgress;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;
import com.ultimate.ultimatesmartschool.Utility.PaginationListener;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.relex.circleindicator.CircleIndicator;

public class ViewNoticeInfo extends AppCompatActivity implements NoticeAdapter.Mycallback{
    ArrayList<NoticeBean> datalist = new ArrayList<>();
    private LinearLayoutManager layoutManager;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.imgBack)
    ImageView imgBack;
    @BindView(R.id.txtNoData)
    TextView txtNoData;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    private RequestQueue queue;
    private String url;
    private NoticeAdapter adapter1;
    int loaded=0;
    @BindView(R.id.parent)
    RelativeLayout parent;
    Dialog mBottomSheetDialog;
    @BindView(R.id.totalRecord)
    TextView totalRecord;
    CommonProgress commonProgress;

    @BindView(R.id.main_progress)
    ProgressBar main_progress;
    int  limit=6,page_limit=0,total_pages=0;
    private static final int PAGE_START = 1;
    private int currentPage = PAGE_START;
    private boolean isLastPage = false;
    private boolean isLoading = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_notice_info);
        ButterKnife.bind(this);
        txtTitle.setText("Notice Board");
        // adepter set here
        commonProgress=new CommonProgress(this);
        layoutManager = new LinearLayoutManager(ViewNoticeInfo.this);
        recyclerView.setLayoutManager(layoutManager);
        adapter1 = new NoticeAdapter(datalist, ViewNoticeInfo.this,this);
        recyclerView.setAdapter(adapter1);
        fetchNoticeBoard(limit, "yes");
//        recyclerView.addOnScrollListener(new PaginationListener(layoutManager) {
//            @Override
//            protected void loadMoreItems() {
//                // isLoading = true;
//                //  Utils.showSnackBar("No more messages found in message list.", parent);
//                //  currentPage++;
//                // doApiCall();
//
//                    main_progress.setVisibility(View.VISIBLE);
//                    currentPage += 10;
//                    page_limit = currentPage + limit;
//
//                   fetchNoticeBoard(page_limit, "no");
//
//            }
//            @Override
//            public boolean isLastPage() {
//                return isLastPage;
//            }
//            @Override
//            public boolean isLoading() {
//                return isLoading;
//            }
//        });
    }

    private void fetchNoticeBoard(int limit,String progress) {

        if (progress.equalsIgnoreCase("yes")){
            commonProgress.show();
        }
        HashMap<String, String> params = new HashMap<>();
        params.put("page_limit", String.valueOf(limit));
        params.put("type", "0");
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.NOTICE_URL, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                ErpProgress.cancelProgressBar();
                commonProgress.dismiss();
                if (error == null) {

                    try {
                        JSONArray noticeArray = jsonObject.getJSONArray("notice_data");
                        datalist = NoticeBean.parseNoticetArray(noticeArray);

                        if (datalist.size() > 0) {
                            adapter1.setNoticeList(datalist);
                            adapter1.notifyDataSetChanged();
                            totalRecord.setVisibility(View.VISIBLE);
                            totalRecord.setText("Total Entries:- "+String.valueOf(datalist.size()));
                            txtNoData.setVisibility(View.GONE);
                        } else {
                            totalRecord.setText("Total Entries:- 0");
                            txtNoData.setVisibility(View.VISIBLE);
                            adapter1.setNoticeList(datalist);
                            adapter1.notifyDataSetChanged();
                        }
                        main_progress.setVisibility(View.GONE);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    totalRecord.setText("Total Entries:- 0");
                    txtNoData.setVisibility(View.VISIBLE);
                    datalist.clear();
                    adapter1.setNoticeList(datalist);
                    adapter1.notifyDataSetChanged();
                }


            }
        }, this, params);

    }


    @OnClick(R.id.imgBack)
    public void imgBack() {
        finish();
    }


    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onMethodCallback(final NoticeBean obj) {
        mBottomSheetDialog = new Dialog(this);
        final View sheetView = getLayoutInflater().inflate(R.layout.image_lyt, null);
        mBottomSheetDialog.setContentView(sheetView);
        ImageView imgShow = (ImageView) mBottomSheetDialog.findViewById(R.id.imgShow);

        ImageView imgDownload = (ImageView) mBottomSheetDialog.findViewById(R.id.imgDownload);
        imgDownload.setVisibility(View.GONE);
        Picasso.get().load(obj.getImage()).placeholder(getResources().getDrawable(R.drawable.home)).into(imgShow);
        imgDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ErpProgress.showProgressBar(ViewNoticeInfo.this, "downloading...");
               // Picasso.with(ViewNoticeInfo.this).load(obj.getImage()).into(imgTraget);
               // Toast.makeText(ViewNoticeInfo.this, "Downloaded successfully", Toast.LENGTH_LONG).show();
            }
        });
        mBottomSheetDialog.show();
        Window window = mBottomSheetDialog.getWindow();
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

    }

    @Override
    public void onDelecallback(NoticeBean obj) {
        if (!User.getCurrentUser().getType().equalsIgnoreCase("user")){
        HashMap<String, String> params = new HashMap<>();
        params.put("n_id", obj.getEs_noticeid());
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.DELETEnoticeboard, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                ErpProgress.cancelProgressBar();
                commonProgress.dismiss();
                if (error == null) {
                    int pos = datalist.indexOf(obj);
                    datalist.remove(pos);
                    if (datalist.size() > 0) {
                        adapter1.setNoticeList(datalist);
                        adapter1.notifyDataSetChanged();
                        totalRecord.setText("Total Entries:- "+String.valueOf(datalist.size()));
                    } else {
                        totalRecord.setText("Total Entries:- 0");
                        adapter1.setNoticeList(datalist);
                        adapter1.notifyDataSetChanged();
                    }
                    //      Utility.openSuccessDialog("Deleted!","Classwork entry deleted successfully.","",ViewClassWork.this);
                    Utils.showSnackBar("Deleted Successfully!", parent);
                    totalRecord.setText("Total Entries:- "+String.valueOf(datalist.size()));
                } else {
                    //  Utility.openErrorDialog(error.getMessage(),ViewClassWork.this);
                    Utils.showSnackBar(error.getMessage(), parent);

                }
            }
        }, this, params);
    }else {
            Toast.makeText(getApplicationContext(),"Unable to delete",Toast.LENGTH_LONG).show();
        }
    }


    @Override
    public void onMethod_Image_callback_new(NoticeBean message_bean) {
        openCommonMultiImage(message_bean.getTitle(),message_bean.getMulti_image());
    }

    public ViewPager intro_images;
    LinearLayout pager_indicator;
    public CircleIndicator indicator;
    public TextView textView;
    String viewwwww = "";

    private void openCommonMultiImage(String subject, ArrayList<String> multiImage) {
        final Dialog warningDialog = new Dialog(ViewNoticeInfo.this);
        warningDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        warningDialog.setCancelable(true);

        warningDialog.setContentView(R.layout.class_img_dialog);
        warningDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        RelativeLayout btnClose = (RelativeLayout) warningDialog.findViewById(R.id.btnNo);

        indicator = (CircleIndicator) warningDialog.findViewById(R.id.indicator);
        textView = (TextView) warningDialog.findViewById(R.id.clsswrktexttt);
        intro_images = (ViewPager) warningDialog.findViewById(R.id.pager_introduction);

        textView.setText(subject);
        CWViewPagerAdapter mAdapter = new CWViewPagerAdapter(ViewNoticeInfo.this, multiImage, "Notice");
        intro_images.setAdapter(mAdapter);
        intro_images.setCurrentItem(0);
        indicator.setViewPager(intro_images);

        TextView tap_count = (TextView) warningDialog.findViewById(R.id.tap_count);
        String title1 = getColoredSpanned("<b>" + "1" + "</b>" + "", "#F4212C");
        String l_Name1 = getColoredSpanned(" / " + String.valueOf(multiImage.size()), "#000000");
        tap_count.setText(Html.fromHtml(title1 + " " + l_Name1 + " "));

        intro_images.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int arg0) {
                // TODO Auto-generated method stub
                // textView.setText((arg0+1)+" of "+ data.getRep_image().size());
                int c = arg0 + 1;
                String title1 = getColoredSpanned("<b>" + String.valueOf(c) + "</b>" + "", "#F4212C");
                String l_Name1 = getColoredSpanned(" / " + String.valueOf(multiImage.size()), "#000000");
                tap_count.setText(Html.fromHtml(title1 + " " + l_Name1 + " "));
            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
                // TODO Auto-generated method stub

                System.out.println("onPageScrolled");
            }

            @Override
            public void onPageScrollStateChanged(int num) {
                // TODO Auto-generated method stub

            }
        });
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                warningDialog.dismiss();
            }
        });
        warningDialog.show();
        Window window = warningDialog.getWindow();
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    }

    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }
    @Override
    public void onMethod_recording_callback(NoticeBean message_bean) {
        openCommonVoice(message_bean.getRecordingsfile());
    }

    private MediaPlayer mediaPlayer;
    private Button playButton, pauseButton;
    ImageView play_img, pause_img;
    RelativeLayout play_lyt;
    private SeekBar seekBar;
    private Handler handler = new Handler();
    private TextView currentDurationTextView, totalDurationTextView;
    LottieAnimationView loti_record_play;
    private Button btnRetake, btnPick,btnSave;
    BottomSheetDialog mBottomSheetDialog_play;
    private void openCommonVoice(String recordingsfile) {

        mBottomSheetDialog_play = new BottomSheetDialog(ViewNoticeInfo.this);
        final View sheetView = getLayoutInflater().inflate(R.layout.activity_audio, null);
        mBottomSheetDialog_play.setContentView(sheetView);
        mBottomSheetDialog_play.setCancelable(false);

        btnRetake = sheetView.findViewById(R.id.btnRetake);
        btnPick = sheetView.findViewById(R.id.btnPick);
        btnSave = sheetView.findViewById(R.id.btnSave);
        play_lyt = sheetView.findViewById(R.id.play_lyt);
        play_img = sheetView.findViewById(R.id.play_img);
        pause_img = sheetView.findViewById(R.id.pause_img);
        playButton = sheetView.findViewById(R.id.playButton);
        pauseButton = sheetView.findViewById(R.id.pauseButton);
        seekBar = sheetView.findViewById(R.id.seekBar);
        loti_record_play = sheetView.findViewById(R.id.loti_play);
        currentDurationTextView = sheetView.findViewById(R.id.currentDurationTextView);
        totalDurationTextView = sheetView.findViewById(R.id.totalDurationTextView);


        btnSave.setVisibility(View.GONE);
        btnPick.setVisibility(View.GONE);

        mediaPlayer = new MediaPlayer();
        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                play_img.setVisibility(View.GONE);
                pause_img.setVisibility(View.VISIBLE);
                mediaPlayer.seekTo(0);
                seekBar.setProgress(0);
                loti_record_play.pauseAnimation();
            }
        });
        String url=recordingsfile;
      //  Log.d("selectedImageUri",url);
        Uri selectedImageUri = Uri.parse(url);

        try {
            mediaPlayer.setDataSource(ViewNoticeInfo.this, selectedImageUri);

            mediaPlayer.prepare();
            mediaPlayer.start();
            updateSeekBar();
        } catch (IOException e) {
            e.printStackTrace();
        }

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


            }
        });

        play_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                play_img.setVisibility(View.GONE);
                pause_img.setVisibility(View.VISIBLE);
                mediaPlayer.pause();
                loti_record_play.pauseAnimation();
            }
        });

        pause_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                play_img.setVisibility(View.VISIBLE);
                pause_img.setVisibility(View.GONE);
                mediaPlayer.start();
                updateSeekBar();
            }
        });

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (fromUser) {
                    mediaPlayer.seekTo(progress);
                    updateDurationTextView(progress);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });

        totalDurationTextView.setText(formatDuration(mediaPlayer.getDuration()));

        btnRetake.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                commonCodeForDssmiss();
            }
        });

        // Dismiss the dialog and reset the MediaPlayer when the dialog is closed
        mBottomSheetDialog_play.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                //  Toast.makeText(getApplicationContext(), "Dismiss", Toast.LENGTH_SHORT).show();
            }
        });

        mBottomSheetDialog_play.show();
    }

    int starts = 0;

    private void commonCodeForDssmiss() {
        if (mediaPlayer != null) {
            mediaPlayer.release();
            mediaPlayer = null;
        }
        mBottomSheetDialog_play.dismiss();
    }

    private void updateSeekBar() {
        loti_record_play.playAnimation();
        seekBar.setMax(mediaPlayer.getDuration());
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (mediaPlayer != null) {
                    int currentPosition = mediaPlayer.getCurrentPosition();
                    seekBar.setProgress(currentPosition);
                    updateDurationTextView(currentPosition);
                    handler.postDelayed(this, 100);
                }
            }
        }, 100);
    }

    private void updateDurationTextView(int duration) {
        currentDurationTextView.setText(formatDuration(duration));
    }

    private String formatDuration(int milliseconds) {
        int seconds = (milliseconds / 1000) % 60;
        int minutes = (milliseconds / (1000 * 60)) % 60;
        return String.format(Locale.getDefault(), "%02d:%02d", minutes, seconds);
    }


    @Override
    public void onMethod_pdf_call_back(NoticeBean message_bean) {
        openCommonPdf(message_bean.getPdf_file());
    }

    private void openCommonPdf(String pdfFile) {
        Intent intent = new Intent(ViewNoticeInfo.this, WebViewActivity.class);
        intent.putExtra("MY_kEY", pdfFile);
        startActivity(intent);
    }



}
