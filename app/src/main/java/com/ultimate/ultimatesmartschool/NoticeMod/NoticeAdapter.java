package com.ultimate.ultimatesmartschool.NoticeMod;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.ClipboardManager;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.daimajia.swipe.SimpleSwipeListener;
import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.squareup.picasso.Picasso;
import com.ultimate.ultimatesmartschool.BeanModule.User;
import com.ultimate.ultimatesmartschool.Homework.Homeworkbean;
import com.ultimate.ultimatesmartschool.Message.Message_Bean;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NoticeAdapter extends RecyclerSwipeAdapter<NoticeAdapter.ViewHolder> {

    private final Context mContext;
    ArrayList<NoticeBean> noticeList;
    private final Mycallback mMycallback;
    public void setNoticeList(ArrayList<NoticeBean> noticeList) {
        this.noticeList = noticeList;
    }
    Animation animation;
    public class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView  trash, update;
        SwipeLayout swipeLayout;
        public TextView txtTitle;
        public TextView txtDate;
        public TextView txtMsg;
        @BindView(R.id.albm_id)
        TextView albm_id;
        @BindView(R.id.copy)
        ImageView copy;
        ImageView imgNotice;
        CardView parent;


        @BindView(R.id.nofile)
        RelativeLayout nofile;

        public ImageView image_file, audio_file, pdf_file,empty_file;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
            parent =(CardView)itemView.findViewById(R.id.card1);
            txtTitle = (TextView) itemView.findViewById(R.id.textView);
            txtDate = (TextView) itemView.findViewById(R.id.txtDate);
            txtMsg = (TextView) itemView.findViewById(R.id.txtMsg);
            swipeLayout = (SwipeLayout) itemView.findViewById(R.id.swipe);
            trash = (ImageView) itemView.findViewById(R.id.trash);
            update = (ImageView) itemView.findViewById(R.id.update);
            imgNotice = (ImageView) itemView.findViewById(R.id.imgNotice);

            empty_file= (ImageView) itemView.findViewById(R.id.empty_file);
            image_file = (ImageView) itemView.findViewById(R.id.image_file);
            pdf_file = (ImageView) itemView.findViewById(R.id.pdf_file);
            audio_file = (ImageView) itemView.findViewById(R.id.audio_file);
        }
    }

    public NoticeAdapter(ArrayList<NoticeBean> noticeList, Context listner, Mycallback mMycallback) {
        this.noticeList = noticeList;
        this.mContext = listner;
        this.mMycallback = mMycallback;
    }


    @Override
    public NoticeAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int i) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.activity_notice_adapter, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;

    }

    @Override
    public void onBindViewHolder(@NonNull NoticeAdapter.ViewHolder viewHolder, @SuppressLint("RecyclerView") final int position) {
       // viewHolder.parent.setAnimation(AnimationUtils.loadAnimation(mContext,R.anim.fade_scale_animation));
        viewHolder.txtTitle.setText(noticeList.get(position).getTitle());


        //for Today
        java.util.Calendar calendar = java.util.Calendar.getInstance(TimeZone.getDefault());
        Date setdate = calendar.getTime();
        SimpleDateFormat fmtOut = new SimpleDateFormat("dd MMM, yyyy");
        String dateString = fmtOut.format(setdate);
        //for Yesterday
        DateFormat dateFormat = new SimpleDateFormat("dd MMM, yyyy");
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);
        String dateString1= dateFormat.format(cal.getTime());

        String check= noticeList.get(position).getDate();

        if (check.equalsIgnoreCase(dateString)){
            //  holder.lytLine.setVisibility(View.VISIBLE);
            // holder.txtDT.setText("Today");
            String title = getColoredSpanned("Today", "#e31e25");
            //String Name = getColoredSpanned(messagelist.get(position).getFrom_type(), "#7D7D7D");
            String l_Name = getColoredSpanned("", "#5A5C59");
            viewHolder.txtDate.setText(Html.fromHtml(title + " " + l_Name));


            // holder.txtDT.setText("Today at "+time);
            //  holder.vieww.setBackground(mContext.getResources().getDrawable(R.drawable.a_card_lyt2));
        }else if (check.equalsIgnoreCase(dateString1)){

            String title = getColoredSpanned("Yesterday", "#1C8B3B");
            //String Name = getColoredSpanned(messagelist.get(position).getFrom_type(), "#7D7D7D");
            String l_Name = getColoredSpanned("", "#5A5C59");
            viewHolder.txtDate.setText(Html.fromHtml(title + " " + l_Name));

            //  holder.lytLine.setVisibility(View.GONE);
            //  holder.vieww.setBackground(mContext.getResources().getDrawable(R.drawable.card_lyt2));
        }else {
            viewHolder.txtDate.setText(noticeList.get(position).getDate());
        }

        //viewHolder.txtDate.setText(noticeList.get(position).getDate());
        if (User.getCurrentUser().getSchoolData().getFi_school_id().equalsIgnoreCase("MSS2024")){
            viewHolder.txtMsg.setText(Html.fromHtml(noticeList.get(position).getMsg()));
        }else{
            viewHolder.txtMsg.setText(noticeList.get(position).getMsg());
        }


        viewHolder.copy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Animation animation;
                animation = AnimationUtils.loadAnimation(mContext, R.anim.btn_blink_animation);
                viewHolder.copy.startAnimation(animation);
                ClipboardManager cm = (ClipboardManager)mContext.getSystemService(Context.CLIPBOARD_SERVICE);
                cm.setText(viewHolder.txtMsg.getText().toString());
                Toast.makeText(mContext, "Text copied to clipboard", Toast.LENGTH_SHORT).show();
            }
        });

        if (noticeList.get(position).getEs_noticeid() != null) {
            String title = getColoredSpanned("ID:- ", "#000000");
            String Name = getColoredSpanned("nb-"+noticeList.get(position).getEs_noticeid(), "#5A5C59");
            viewHolder.albm_id.setText(Html.fromHtml(title + " " + Name));
        }
        mItemManger.bindView(viewHolder.itemView, position);

        viewHolder.swipeLayout.setShowMode(SwipeLayout.ShowMode.PullOut);
        viewHolder.swipeLayout.addSwipeListener(new SimpleSwipeListener() {
            @Override
            public void onOpen(SwipeLayout layout) {

            }

            @Override
            public void onStartClose(SwipeLayout layout) {
                viewHolder.swipeLayout.close();
            }
        });
        viewHolder.trash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder1 = new AlertDialog.Builder(mContext);
                builder1.setMessage("Do you want to delete? ");
                builder1.setCancelable(false);
                builder1.setPositiveButton(
                        "Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                                if (mMycallback != null) {
                                    mMycallback.onDelecallback(noticeList.get(position));
                                   // Toast.makeText(mContext, "deleted", Toast.LENGTH_SHORT).show();
                                }
                                // mAdapterCallback.deleteNotification(notificationList.get(postion));
                            }
                        });
                builder1.setNegativeButton(
                        "No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        });
                AlertDialog alert11 = builder1.create();
                alert11.show();
            }
        });
        //viewHolder.txtMsg.setText(noticeList.get(position).getMsg());
//        if (noticeList.get(position).getImage() != null) {
//            viewHolder.imgNotice.setVisibility(View.VISIBLE);
//            viewHolder.imgNotice.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    if (mMycallback != null) {
//                        mMycallback.onMethodCallback(noticeList.get(position));
//                    }
//                }
//            });
//            Picasso.with(mContext).load(noticeList.get(position).getImage()).into(viewHolder.imgNotice);
//        } else {
//            viewHolder.imgNotice.setVisibility(View.GONE);
//        }
        animation = AnimationUtils.loadAnimation(mContext, R.anim.btn_blink_animation);

        if (noticeList.get(position).getImage_tag().equalsIgnoreCase("images")){
            viewHolder.nofile.setVisibility(View.VISIBLE);
            viewHolder.image_file.setVisibility(View.VISIBLE);
            viewHolder.audio_file.setVisibility(View.GONE);
            viewHolder.pdf_file.setVisibility(View.GONE);
            viewHolder.empty_file.setVisibility(View.GONE);
            Log.d("selectedImageUri",(noticeList.get(position).getMulti_image().get(0)));
            Picasso.get().load(noticeList.get(position).getMulti_image().get(0)).
                    placeholder(mContext.getResources().getDrawable(R.color.transparent)).into(viewHolder.image_file);
            viewHolder.image_file.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    viewHolder.image_file.startAnimation(animation);
                    if(mMycallback!= null){
                        mMycallback.onMethod_Image_callback_new(noticeList.get(position));
                    }
                }
            });
        } else if (noticeList.get(position).getRecordingsfile() != null){
            viewHolder.nofile.setVisibility(View.VISIBLE);
            viewHolder.image_file.setVisibility(View.GONE);
            viewHolder.audio_file.setVisibility(View.VISIBLE);
            viewHolder.pdf_file.setVisibility(View.GONE);
            viewHolder.empty_file.setVisibility(View.GONE);
            viewHolder.audio_file.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    viewHolder.audio_file.startAnimation(animation);
                    if(mMycallback!= null){
                        mMycallback.onMethod_recording_callback(noticeList.get(position));
                    }
                }
            });
        } else if (noticeList.get(position).getPdf_file() != null){
            viewHolder.nofile.setVisibility(View.VISIBLE);
            viewHolder.image_file.setVisibility(View.GONE);
            viewHolder.audio_file.setVisibility(View.GONE);
            viewHolder.pdf_file.setVisibility(View.VISIBLE);
            viewHolder.empty_file.setVisibility(View.GONE);
            viewHolder.pdf_file.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    viewHolder.pdf_file.startAnimation(animation);
                    if(mMycallback!= null){
                        mMycallback.onMethod_pdf_call_back(noticeList.get(position));
                    }
                }
            });
        }else {
            viewHolder.nofile.setVisibility(View.GONE);
            viewHolder.image_file.setVisibility(View.GONE);
            viewHolder.audio_file.setVisibility(View.GONE);
            viewHolder.pdf_file.setVisibility(View.GONE);
            viewHolder.empty_file.setVisibility(View.VISIBLE);
        }

    }

    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }
    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe;
    }
    @Override
    public int getItemCount() {
        return noticeList.size();
    }

    public interface Mycallback {
        public void onMethodCallback(NoticeBean obj);
        public void onDelecallback(NoticeBean obj);
        public void onMethod_Image_callback_new(NoticeBean message_bean);
        public void onMethod_recording_callback(NoticeBean message_bean);
        public void onMethod_pdf_call_back(NoticeBean message_bean);
    }
}
