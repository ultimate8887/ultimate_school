package com.ultimate.ultimatesmartschool.NoticeMod;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ClipData;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.graphics.pdf.PdfRenderer;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.ParcelFileDescriptor;
import android.provider.MediaStore;
import android.speech.RecognizerIntent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;
import com.android.volley.Request;
import com.bumptech.glide.Glide;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.kroegerama.imgpicker.BottomSheetImagePicker;
import com.ultimate.ultimatesmartschool.Activity.SchoolInfoActivity;
import com.ultimate.ultimatesmartschool.BeanModule.User;
import com.ultimate.ultimatesmartschool.ForCamera.IPickResult;
import com.ultimate.ultimatesmartschool.ForCamera.PickImageDialog;
import com.ultimate.ultimatesmartschool.ForCamera.PickResult;
import com.ultimate.ultimatesmartschool.ForCamera.PickSetup;
import com.ultimate.ultimatesmartschool.Homework.AddHomeworkActivity;
import com.ultimate.ultimatesmartschool.Login.LoginActivity;
import com.ultimate.ultimatesmartschool.Message.ComposeStudentMsg;
import com.ultimate.ultimatesmartschool.PDF.PdfPageAdapter;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.CommonProgress;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;
import com.ultimate.ultimatesmartschool.Utility.ErpProgressVert;
import com.ultimate.ultimatesmartschool.Utility.FileUtils;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import smartdevelop.ir.eram.showcaseviewlib.GuideView;
import smartdevelop.ir.eram.showcaseviewlib.config.DismissType;
import smartdevelop.ir.eram.showcaseviewlib.listener.GuideListener;

public class AddNoticeBoard extends AppCompatActivity implements BottomSheetImagePicker.OnImagesSelectedListener, IPickResult {

    @BindView(R.id.all)
    RadioButton all;
    @BindView(R.id.staff)
    RadioButton staff;
    @BindView(R.id.student)
    RadioButton student;
    SharedPreferences sharedPreferences;
    @BindView(R.id.textnote)
    TextView textnote;
    @BindView(R.id.edittitle)
    EditText edittitle;
    @BindView(R.id.editnotice)
    EditText editnotice;
    private Bitmap hwbitmap;
    @BindView(R.id.imageView6)ImageView imageView6;
    private int type;
    @BindView(R.id.txtTitle)TextView txtTitle;
    @BindView(R.id.parent)
    RelativeLayout parent;
    @BindView(R.id.radioGroupProf)
    RadioGroup radioGroupProof;
    String am_pm="",v_type="",p_type="";
    CommonProgress commonProgress;
    @BindView(R.id.speak_text)
    ImageView speak_text;
    private static final int REQUEST_CODE_SPEECH_INPUT = 1;

    @BindView(R.id.scrollView)
    HorizontalScrollView scrollView;
    @BindView(R.id.addpdf)
    TextView addpdf;
    @BindView(R.id.addimage)
    TextView addimage;
    @BindView(R.id.viewvoice)
    TextView viewvoice;
    Animation animation;
    private final int LOAD_FILE_RESULTS = 1000;
    String fileBase64 = null;
    String fileBase65 = null;

    int PICK_IMAGE_MULTIPLE = 11;
    String imageEncoded;
    List<String> imagesEncodedList;
    private ViewGroup imageContainer;
    ArrayList<String> image;
    List<? extends Uri> image2;
    String encoded, ad;
    @BindView(R.id.img_lyt)
    RelativeLayout img_lyt;
    String size,a_size;
    String path = "";
    Uri uri;
    BottomSheetDialog mBottomSheetDialog1;
    @BindView(R.id.checkImg)
    CheckBox checkImg;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_notice_board);
        ButterKnife.bind(this);
        txtTitle.setText("Add Notice Board");
        commonProgress=new CommonProgress(this);
       // radioGroupProof.setSelected(true);
        // This overrides the radiogroup onCheckListener
        imageContainer = findViewById(R.id.imageContainer);
        animation = AnimationUtils.loadAnimation(AddNoticeBoard.this, R.anim.btn_blink_animation);
        checkImg.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (checkImg.isChecked()) {
                    img_lyt.setVisibility(View.VISIBLE);
                    //  add_img="add";
                } else {
                    img_lyt.setVisibility(View.GONE);
                    // add_img="empty";
                }

            }
        });
        radioGroupProof.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                // This will get the radiobutton that has changed in its check state
                RadioButton checkedRadioButton = (RadioButton)group.findViewById(checkedId);
                // This puts the value (true/false) into the variable
                boolean isChecked = checkedRadioButton.isChecked();
                // If the radiobutton that has changed in check state is now checked...
                if (isChecked)
                {
                  //  Toast.makeText(AddNoticeBoard.this,"radioGroupProof:" + checkedRadioButton.getText(),Toast.LENGTH_LONG).show();
                    if (checkedRadioButton.getText().toString().equalsIgnoreCase("Student")){
                        all.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.sender));
                        staff.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.sender));
                        student.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.light_red));

                    }else if (checkedRadioButton.getText().toString().equalsIgnoreCase("staff")){
                        all.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.sender));
                        staff.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.light_red));
                        student.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.sender));
                    }else{
                        all.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.light_red));
                        staff.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.sender));
                        student.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.sender));
                    }

                    p_type = checkedRadioButton.getText().toString();
                    // Changes the textview's text to "Checked: example radiobutton text"
                    //  tv.setText("Checked:" + checkedRadioButton.getText());
                }
            }
        });

        if (!restorePrefData()){
            setShowcaseView();
        }

        speak_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent
                        = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
                intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                        RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
                intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE,
                        Locale.getDefault());
                intent.putExtra(RecognizerIntent.EXTRA_PROMPT, "Speak to text");

                try {
                    startActivityForResult(intent, REQUEST_CODE_SPEECH_INPUT);
                }
                catch (Exception e) {
                    Toast
                            .makeText(AddNoticeBoard.this, " " + e.getMessage(),
                                    Toast.LENGTH_SHORT)
                            .show();
                }

            }
        });

    }


    private void savePrefData(){
        sharedPreferences=this.getSharedPreferences("main_view_new_text_speech",MODE_PRIVATE);
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putBoolean("main_view_new_text_speech",true);
        editor.apply();
        // dataPasser.onTargetViewPass("save");
    }

    private boolean restorePrefData(){
        sharedPreferences = this.getSharedPreferences("main_view_new_text_speech",MODE_PRIVATE);
        return sharedPreferences.getBoolean("main_view_new_text_speech",false);
    }


    private void setShowcaseView() {
        new GuideView.Builder(this)
                .setTitle("Speech to text")
                .setContentText("Tap the Speech to \ntext Icon")
                .setTargetView(speak_text)
                .setContentTextSize(12)//optional
                .setTitleTextSize(14)//optional
                .setDismissType(DismissType.outside) //optional - default dismissible by TargetView
                .setGuideListener(new GuideListener() {
                    @Override
                    public void onDismiss(View view) {
                        savePrefData();
                    }
                })
                .build()
                .show();
    }



    @OnClick(R.id.addpdf)
    public void browseDocuments() {
        addpdf.startAnimation(animation);
        pick_pdf();
    }

    private void pick_pdf() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.UPSIDE_DOWN_CAKE) { // Android 14+ (API 34)
            callPicker();

        } else if (Build.VERSION.SDK_INT > Build.VERSION_CODES.S) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_MEDIA_IMAGES)
                    != PackageManager.PERMISSION_GRANTED) {

                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_MEDIA_IMAGES)) {
                    Toast.makeText(this, "Please provide storage permission from app settings", Toast.LENGTH_LONG).show();
                } else {

                    ActivityCompat.requestPermissions(this,
                            new String[]{Manifest.permission.READ_MEDIA_IMAGES},
                            2);
                }
            } else {

                callPicker();
            }

        } else {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {

                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    Toast.makeText(this, "Please provide storage permission from app settings", Toast.LENGTH_LONG).show();
                } else {

                    ActivityCompat.requestPermissions(this,
                            new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                            2);
                }
            } else {
                callPicker();

            }
        }

    }


    public void callPicker() {
        type = 2;
        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        intent.setType("application/pdf");
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        startActivityForResult(intent, LOAD_FILE_RESULTS);

    }


//    @RequiresApi(api = Build.VERSION_CODES.M)
//    @OnClick(R.id.textView6)
//    public void click() {
//        // type = 1;
//        //    pick_img();
////        ImagePicker.Companion.with(ComposeStudentMsg.this)
////                .galleryOnly()
////                .crop()	    			//Crop image(Optional), Check Customization for more option
////                .compress(1024)			//Final image size will be less than 1 MB(Optional)
////                .maxResultSize(1080, 1080)	//Final image resolution will be less than 1080 x 1080(Optional)
////                .start();
//        // CallCropAct();
//        addimage.startAnimation(animation);
//        showPdfFromUri_new();
//    }

    @SuppressLint("ResourceType")
    @RequiresApi(api = Build.VERSION_CODES.M)
    @OnClick(R.id.addimage)
    public void clickeeddd() {
        addimage.startAnimation(animation);
        showPdfFromUri_new();
    }


    @SuppressLint("ResourceType")
    private void showPdfFromUri_new() {
        BottomSheetDialog mBottomSheetDialog1 = new BottomSheetDialog(AddNoticeBoard.this);
        final View sheetView = getLayoutInflater().inflate(R.layout.pdf_view_dialog1, null);
        mBottomSheetDialog1.setContentView(sheetView);

        mBottomSheetDialog1.setCancelable(true);

        TextView title = (TextView) sheetView.findViewById(R.id.title);
        RelativeLayout pdf = (RelativeLayout) sheetView.findViewById(R.id.pdf);
        RelativeLayout img = (RelativeLayout) sheetView.findViewById(R.id.img);
        TextView submitted = (TextView) sheetView.findViewById(R.id.submitted);
        TextView retake = (TextView) sheetView.findViewById(R.id.retake);


        title.setText("Choose File Option");
        //  path1.setText(path);
        //path1.setText("" + "" + ""+size);

        //  pdf.fromStream(inputStream).load();

        img.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View view) {
                retake.startAnimation(animation);
                pick_img();
                mBottomSheetDialog1.dismiss();
            }
        });

        pdf.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("ResourceType")
            @Override
            public void onClick(View view) {
                submitted.startAnimation(animation);
                mBottomSheetDialog1.dismiss();
                selectMultipleImage();
            }
        });
        RelativeLayout btnNo = (RelativeLayout) sheetView.findViewById(R.id.btnNo);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnNo.startAnimation(animation);
                //   fileBase64=null;
                mBottomSheetDialog1.dismiss();
            }
        });
        mBottomSheetDialog1.show();
    }

    @SuppressLint("ResourceType")
    private void selectMultipleImage() {

        //  showPdfFromUri();
//                mBottomSheetDialog1.dismiss();
        //  submitAssignment();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.UPSIDE_DOWN_CAKE) { // Android 14+ (API 34)
            //Toast.makeText(this, "Multi Images", Toast.LENGTH_LONG).show();
            // initialising intent
            Intent intent = new Intent();
            // setting type to select to be image
            intent.setType("image/*");
            // allowing multiple image to be selected
            intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(Intent.createChooser(intent, "Select Multiple Images"), PICK_IMAGE_MULTIPLE);

        } else if (Build.VERSION.SDK_INT > Build.VERSION_CODES.S) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_MEDIA_IMAGES)
                    != PackageManager.PERMISSION_GRANTED) {

                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_MEDIA_IMAGES)) {
                    Toast.makeText(this, "Please provide storage permission from app settings", Toast.LENGTH_LONG).show();
                } else {

                    ActivityCompat.requestPermissions(this,
                            new String[]{Manifest.permission.READ_MEDIA_IMAGES},
                            3);
                }
            } else {
                //Toast.makeText(this, "Multi Images", Toast.LENGTH_LONG).show();
                // initialising intent
                Intent intent = new Intent();
                // setting type to select to be image
                intent.setType("image/*");
                // allowing multiple image to be selected
                intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Multiple Images"), PICK_IMAGE_MULTIPLE);
            }

        } else {

            if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {

                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    Toast.makeText(this, "Please provide storage permission from app settings", Toast.LENGTH_LONG).show();
                } else {

                    ActivityCompat.requestPermissions(this,
                            new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                            3);

                }
            } else {

                new BottomSheetImagePicker.Builder(getString(R.xml.provider_paths))
                        .multiSelect(1, 8)
                        .multiSelectTitles(
                                R.plurals.pick_multi,
                                R.plurals.pick_multi_more,
                                R.string.pick_multi_limit
                        )
                        .peekHeight(R.dimen.peekHeight)
                        .columnSize(R.dimen.columnSize)
                        .requestTag("multi")
                        .show(getSupportFragmentManager(), null);
                // showFileChooser();
            }

        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @SuppressLint("ResourceType")
    private void pick_img() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.UPSIDE_DOWN_CAKE) { // Android 14+ (API 34)
            onImageViewClick();

        } else if (Build.VERSION.SDK_INT > Build.VERSION_CODES.S) {
                if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_MEDIA_IMAGES)
                        != PackageManager.PERMISSION_GRANTED) {

                    if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_MEDIA_IMAGES)) {
                        Toast.makeText(this, "Please provide storage permission from app settings", Toast.LENGTH_LONG).show();
                    } else {

                        ActivityCompat.requestPermissions(this,
                                new String[]{Manifest.permission.READ_MEDIA_IMAGES},
                                1);
                    }
                } else {

                    onImageViewClick();
                }

            } else {
                if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {

                    if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                        Toast.makeText(this, "Please provide storage permission from app settings", Toast.LENGTH_LONG).show();
                    } else {

                        ActivityCompat.requestPermissions(this,
                                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                                1);
                    }
                } else {
                    onImageViewClick();

                }
        }
    }

    private void onImageViewClick() {
        //  type = 1;
        PickSetup setup = new PickSetup();
        // super.customize(setup);

        PickImageDialog.build(setup)
                //.setOnClick(this)
                .show(this);
    }

    @Override
    public void onPickResult(PickResult r) {
        if (r.getError() == null) {
            //If you want the Bitmap.
            imageView6.setImageBitmap(r.getBitmap());
            hwbitmap = r.getBitmap();

            scrollView.setVisibility(View.GONE);
            imageView6.setVisibility(View.VISIBLE);
            image = new ArrayList<>();
            Log.e("hwbitmapup", String.valueOf(hwbitmap));
            encoded = Utils.encodeToBase64(hwbitmap, Bitmap.CompressFormat.JPEG, 50);
            image.add(encoded);
            //  add_img="add";
//            optLyt.setVisibility(View.GONE);
//            retakes.setVisibility(View.VISIBLE);
            //   Toast.makeText(this, images2, Toast.LENGTH_LONG).show();
            fileBase64 = null;

        } else {
            //Handle possible errors
            //TODO: do what you have to do with r.getError();
            Toast.makeText(this, r.getError().getMessage(), Toast.LENGTH_LONG).show();
        }


    }


    public static boolean checkPermission(Context mContext, String Permission) {
        int result = ContextCompat.checkSelfPermission(mContext, Permission);
        if (result == PackageManager.PERMISSION_GRANTED) {

            return true;
        } else {
            return false;
        }
    }


    private static final int REQUEST_RECORD_AUDIO_PERMISSION = 200;
    private boolean permissionToRecordAccepted = false;
    private String[] permissions = {Manifest.permission.RECORD_AUDIO};

    private MediaRecorder recorder;
    private MediaPlayer player;
    private String fileName;
    private boolean isRecording = false;

    private Button btnRecord, btnStop, btnPlay;
    private TextView tvTimer;
    private long startTime = 0;
    private Handler timerHandler = new Handler();
    LottieAnimationView loti_play;
    RelativeLayout btnNo;
    BottomSheetDialog mBottomSheetDialog_play, mBottomSheetDialog;
    public static final int RequestPermissionCode = 1;
    private static final int SELECT_AUDIO = 3;
    // First, define the permission request code

    @OnClick(R.id.viewvoice)
    public void recordaudiosss() {

        BottomSheetDialog mBottomSheetDialog1 = new BottomSheetDialog(AddNoticeBoard.this);
        final View sheetView = getLayoutInflater().inflate(R.layout.pdf_view_dialog1, null);
        mBottomSheetDialog1.setContentView(sheetView);

        mBottomSheetDialog1.setCancelable(true);

        TextView title = (TextView) sheetView.findViewById(R.id.title);
        RelativeLayout pdf = (RelativeLayout) sheetView.findViewById(R.id.pdf);
        RelativeLayout img = (RelativeLayout) sheetView.findViewById(R.id.img);
        TextView submitted = (TextView) sheetView.findViewById(R.id.submitted);
        TextView retake = (TextView) sheetView.findViewById(R.id.retake);
        retake.setText("Record");
        submitted.setText("Upload");

        title.setText("Choose File Option");
        //  path1.setText(path);
        //path1.setText("" + "" + ""+size);

        //  pdf.fromStream(inputStream).load();

        img.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View view) {
                retake.startAnimation(animation);
                if (checkPermissionForRecording()) {
                    openDialogRecording();
                } else {
                    // Toast.makeText(getApplicationContext(), "Recording denied", Toast.LENGTH_SHORT).show();
                    requestPermission();
                }
                mBottomSheetDialog1.dismiss();
            }
        });

        pdf.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("ResourceType")
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setType("audio/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivityForResult(Intent.createChooser(intent, "Select a Video "), SELECT_AUDIO);
                mBottomSheetDialog1.dismiss();
            }
        });
        RelativeLayout btnNo = (RelativeLayout) sheetView.findViewById(R.id.btnNo);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnNo.startAnimation(animation);
                //   fileBase64=null;
                mBottomSheetDialog1.dismiss();
            }
        });
        mBottomSheetDialog1.show();


    }

    private void openDialogRecording() {
        // Toast.makeText(getApplicationContext(), "Recording check", Toast.LENGTH_SHORT).show();
        starts++;
        mBottomSheetDialog = new BottomSheetDialog(AddNoticeBoard.this);
        final View sheetView = getLayoutInflater().inflate(R.layout.audiorecord_dialog_n, null);
        mBottomSheetDialog.setContentView(sheetView);
        mBottomSheetDialog.setCancelable(false);

        btnRecord = sheetView.findViewById(R.id.btnRecord);
        btnStop = sheetView.findViewById(R.id.btnStop);
        btnPlay = sheetView.findViewById(R.id.btnPlay);
        tvTimer = sheetView.findViewById(R.id.tvTimer);
        loti_play = sheetView.findViewById(R.id.loti_play);
        btnNo = (RelativeLayout) sheetView.findViewById(R.id.btnNo);

        // Request permission to record audio
        // ActivityCompat.requestPermissions(this, permissions, REQUEST_RECORD_AUDIO_PERMISSION);

        if (!isRecording) {
            startRecording();
        }
        //    ActivityCompat.requestPermissions(this, permissions, REQUEST_RECORD_AUDIO_PERMISSION);

        btnRecord.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startRecording();
            }
        });

        btnStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stopRecording();
            }
        });

        btnPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playRecording("play");
            }
        });

        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isRecording) {
                    recorder.stop();
                    recorder.release();
                    recorder = null;
                    isRecording = false;
                    stopTimer();
                    Toast.makeText(getApplicationContext(), "Recording stopped", Toast.LENGTH_SHORT).show();
                }
                mBottomSheetDialog.dismiss();
            }
        });

        // Dismiss the dialog and reset the MediaPlayer when the dialog is closed
        mBottomSheetDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                // Toast.makeText(getApplicationContext(), "Dismiss", Toast.LENGTH_SHORT).show();
            }
        });

        mBottomSheetDialog.show();
    }

    // Check permission result
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_RECORD_AUDIO_PERMISSION) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission granted, you can proceed with recording
                openDialogRecording();
            } else {
                // Permission denied, show a message or take appropriate action
                Toast.makeText(getApplicationContext(), "Permission denied for recording", Toast.LENGTH_SHORT).show();
            }
        }
    }

    // Check permission for recording
    private boolean checkPermissionForRecording() {
        return ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO)
                == PackageManager.PERMISSION_GRANTED;
    }

    // Request permission
    private void requestPermission() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.RECORD_AUDIO},
                REQUEST_RECORD_AUDIO_PERMISSION);
    }

    private void startRecording() {
        if (!isRecording) {
            loti_play.playAnimation();
            fileName = getExternalCacheDir().getAbsolutePath();
            fileName += "/audio_record.mp3"; // Change file extension to .mp3

            recorder = new MediaRecorder();
            recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
            // Set output format to MPEG-4
            recorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
            // Set audio encoder to MP3
            recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC); // For MP3, use AAC encoder

            recorder.setOutputFile(fileName);

            try {
                recorder.prepare();
            } catch (IOException e) {
                e.printStackTrace();
            }

            recorder.start();
            isRecording = true;
            startTime = System.currentTimeMillis();
            startTimer();
            Toast.makeText(getApplicationContext(), "Recording started", Toast.LENGTH_SHORT).show();
            btnRecord.setEnabled(false);
            btnRecord.setText("Recording....");
            btnStop.setEnabled(true);
            btnPlay.setEnabled(false);
        }
    }

    private void stopRecording() {
        if (isRecording) {
            loti_play.cancelAnimation();
            recorder.stop();
            recorder.release();
            recorder = null;
            isRecording = false;
            stopTimer();
            Toast.makeText(getApplicationContext(), "Recording stopped", Toast.LENGTH_SHORT).show();
            btnRecord.setEnabled(true);
            btnStop.setEnabled(false);
            btnPlay.setEnabled(true);
            voice_type="pick";
            playRecording("play");

        }
    }


    private void startTimer() {
        timerHandler.postDelayed(timerRunnable, 0);
    }

    private void stopTimer() {
        timerHandler.removeCallbacks(timerRunnable);
    }

    private Runnable timerRunnable = new Runnable() {
        @Override
        public void run() {
            long millis = System.currentTimeMillis() - startTime;
            int seconds = (int) (millis / 1000);
            int minutes = seconds / 60;
            seconds = seconds % 60;

            tvTimer.setText(String.format(Locale.getDefault(), "%02d:%02d", minutes, seconds));

            timerHandler.postDelayed(this, 500);
        }
    };


    //playing record audio

    private MediaPlayer mediaPlayer;
    private Button playButton, pauseButton;
    ImageView play_img, pause_img;
    RelativeLayout play_lyt;
    private SeekBar seekBar;
    private Handler handler = new Handler();
    private TextView currentDurationTextView, totalDurationTextView;
    LottieAnimationView loti_record_play;
    private Button btnRetake, btnPick;
    String audiofile = "";

    @SuppressLint("MissingInflatedId")
    private void playRecording(String val) {

        mBottomSheetDialog_play = new BottomSheetDialog(AddNoticeBoard.this);
        final View sheetView = getLayoutInflater().inflate(R.layout.activity_audio, null);
        mBottomSheetDialog_play.setContentView(sheetView);
        mBottomSheetDialog_play.setCancelable(false);

        btnRetake = sheetView.findViewById(R.id.btnRetake);
        btnPick = sheetView.findViewById(R.id.btnPick);
        play_lyt = sheetView.findViewById(R.id.play_lyt);
        play_img = sheetView.findViewById(R.id.play_img);
        pause_img = sheetView.findViewById(R.id.pause_img);
        playButton = sheetView.findViewById(R.id.playButton);
        pauseButton = sheetView.findViewById(R.id.pauseButton);
        seekBar = sheetView.findViewById(R.id.seekBar);
        loti_record_play = sheetView.findViewById(R.id.loti_play);
        currentDurationTextView = sheetView.findViewById(R.id.currentDurationTextView);
        totalDurationTextView = sheetView.findViewById(R.id.totalDurationTextView);

        if (val.equalsIgnoreCase("picked")) {
            btnPick.setVisibility(View.GONE);
            // audiofile = fileName;
        } else {
            btnPick.setVisibility(View.VISIBLE);
            // audiofile = fileName;
        }

        mediaPlayer = new MediaPlayer();
        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                play_img.setVisibility(View.GONE);
                pause_img.setVisibility(View.VISIBLE);
                mediaPlayer.seekTo(0);
                seekBar.setProgress(0);
                loti_record_play.pauseAnimation();
            }
        });

        Uri uri = Uri.parse("https://onlinetestcase.com/wp-content/uploads/2023/06/2-MB-MP3.mp3");

        try {
            if (voice_type.equalsIgnoreCase("upload")){
                mediaPlayer.setDataSource(getApplicationContext(),selectedImageUri);
            }else{
                mediaPlayer.setDataSource(fileName);
            }

            mediaPlayer.prepare();
            mediaPlayer.start();
            updateSeekBar();
        } catch (IOException e) {
            e.printStackTrace();
        }

        btnPick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (fileName != null) {
                    File file = new File(fileName);
                    if (file.exists()) {
                        // File exists, you can get its size
                        long fileSizeInBytes = file.length();
                        // Convert bytes to kilobytes
                        double fileSize = fileSizeInBytes / 1024.0;
                        String fileSizeString;
                        if (fileSize < 1024) {
                            // Size is less than 1 MB, display in KB
                            fileSizeString = String.format("%.2f KB", fileSize);
                        } else {
                            // Size is 1 MB or more, display in MB
                            fileSizeString = String.format("%.2f MB", fileSize / 1024.0);
                        }

                        // Now you can use fileSizeString to display the file size
                        //Toast.makeText(getApplicationContext(), "File Picked. Size: " + fileSizeString, Toast.LENGTH_SHORT).show();
                        try {
                            fileBase65 = Utils.encodeFileToBase64Binary(fileName);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        textnote.setText("File Size: " + "" + "" + fileSizeString);
                        textnote.setVisibility(View.VISIBLE);

                        fileBase64 = null;
                        hwbitmap = null;
                        imageView6.setImageResource(R.drawable.sound_file);
                        scrollView.setVisibility(View.GONE);
                        imageView6.setVisibility(View.VISIBLE);

                        mBottomSheetDialog.dismiss();
                        commonCodeForDssmiss();
                    } else {
                        // File doesn't exist
                        Toast.makeText(getApplicationContext(), "File not found.", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Can't Pick this file.", Toast.LENGTH_SHORT).show();
                }

            }
        });

        play_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                play_img.setVisibility(View.GONE);
                pause_img.setVisibility(View.VISIBLE);
                mediaPlayer.pause();
                loti_record_play.pauseAnimation();
            }
        });

        pause_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                play_img.setVisibility(View.VISIBLE);
                pause_img.setVisibility(View.GONE);
                mediaPlayer.start();
                updateSeekBar();
            }
        });

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (fromUser) {
                    mediaPlayer.seekTo(progress);
                    updateDurationTextView(progress);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });

        totalDurationTextView.setText(formatDuration(mediaPlayer.getDuration()));

        btnRetake.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                commonCodeForDssmiss();
            }
        });

        // Dismiss the dialog and reset the MediaPlayer when the dialog is closed
        mBottomSheetDialog_play.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                //  Toast.makeText(getApplicationContext(), "Dismiss", Toast.LENGTH_SHORT).show();
            }
        });

        mBottomSheetDialog_play.show();

//        player = new MediaPlayer();
//        try {
//            player.setDataSource(fileName);
//            player.prepare();
//            player.start();
//            Toast.makeText(getApplicationContext(), "Playing recording", Toast.LENGTH_SHORT).show();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
    }

    int starts=0;

    private void commonCodeForDssmiss() {
        if (mediaPlayer != null) {
            mediaPlayer.release();
            mediaPlayer = null;
            if (starts!=0){
                btnRecord.setEnabled(true);
                btnRecord.setText("Record");
                btnStop.setEnabled(false);
                tvTimer.setText("00:00");
                loti_play.cancelAnimation();
            }

        }
        mBottomSheetDialog_play.dismiss();
    }

    private void updateSeekBar() {
        loti_record_play.playAnimation();
        seekBar.setMax(mediaPlayer.getDuration());
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (mediaPlayer != null) {
                    int currentPosition = mediaPlayer.getCurrentPosition();
                    seekBar.setProgress(currentPosition);
                    updateDurationTextView(currentPosition);
                    handler.postDelayed(this, 100);
                }
            }
        }, 100);
    }

    private void updateDurationTextView(int duration) {
        currentDurationTextView.setText(formatDuration(duration));
    }

    private String formatDuration(int milliseconds) {
        int seconds = (milliseconds / 1000) % 60;
        int minutes = (milliseconds / (1000 * 60)) % 60;
        return String.format(Locale.getDefault(), "%02d:%02d", minutes, seconds);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

//    @Override
//    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//            if (requestCode == REQUEST_CODE_SPEECH_INPUT) {
//                if (resultCode == RESULT_OK && data != null) {
//                    ArrayList<String> result = data.getStringArrayListExtra(
//                            RecognizerIntent.EXTRA_RESULTS);
//                    editnotice.setText(
//                            Objects.requireNonNull(result).get(0));
//                }
//            }
//    }

    private String voice_type="";
    float  length;
    Uri selectedImageUri;
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == PICK_IMAGE_MULTIPLE && null != data) {
                // Get the Image from data

                String[] filePathColumn = {MediaStore.Images.Media.DATA};
                imagesEncodedList = new ArrayList<String>();
                if (data.getData() != null) {

                    Uri mImageUri = data.getData();

                    // Get the cursor
                    Cursor cursor = getContentResolver().query(mImageUri,
                            filePathColumn, null, null, null);
                    // Move to first row
                    cursor.moveToFirst();

                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    imageEncoded = cursor.getString(columnIndex);
                    cursor.close();

                } else {
                    if (data.getClipData() != null) {
                        ClipData mClipData = data.getClipData();
                        ArrayList<Uri> mArrayUri = new ArrayList<Uri>();
                        for (int i = 0; i < mClipData.getItemCount(); i++) {

                            ClipData.Item item = mClipData.getItemAt(i);
                            Uri uri = item.getUri();
                            mArrayUri.add(uri);
                            // Get the cursor
                            Cursor cursor = getContentResolver().query(uri, filePathColumn, null, null, null);
                            // Move to first row
                            cursor.moveToFirst();

                            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                            imageEncoded = cursor.getString(columnIndex);
                            imagesEncodedList.add(imageEncoded);
                            cursor.close();

                        }
                        commonCode(mArrayUri);
                        Log.e("LOG_TAG", "Selected Images" + mArrayUri.size());
//                        Toast.makeText(this, "Selected Images" + imagesEncodedList.size(),
//                                Toast.LENGTH_LONG).show();
                    }
                }
            } else if (requestCode == REQUEST_CODE_SPEECH_INPUT) {
                if (resultCode == RESULT_OK && data != null) {
                    ArrayList<String> result = data.getStringArrayListExtra(
                            RecognizerIntent.EXTRA_RESULTS);
                    editnotice.setText(
                            Objects.requireNonNull(result).get(0));
                }
            }else if (requestCode == SELECT_AUDIO) {
                selectedImageUri = data.getData();

                // selectedPath = getPath(selectedImageUri);
                fileName = FileUtils.getRealPath(this, selectedImageUri);
                File file = new File(fileName);
                length = file.length();
                length = length/1000000;
                long fileSize = file.length();
                a_size = formatSize(file.length());
                if(fileSize > 5 * 1024 * 1024){
                    Toast.makeText(getApplicationContext(),"Sorry, Your selected file size too large("+a_size+"), Kindly compress your file first to upload!" +
                            "(Max Size of file uploading should be less than 5MB)",Toast.LENGTH_LONG).show();
                }else {
                    try {
                        ContentResolver contentResolver = getContentResolver();
                        InputStream inputStream = contentResolver.openInputStream(selectedImageUri);
                        File file_made = new File(getCacheDir(), "tempVoice.mp3");
                        OutputStream outputStream = new FileOutputStream(file_made);
                        byte[] buffer = new byte[4 * 1024]; // or other buffer size
                        int read;

                        while ((read = inputStream.read(buffer)) != -1) {
                            outputStream.write(buffer, 0, read);
                        }

                        outputStream.flush();
                        outputStream.close();
                        inputStream.close();

                        String path = file_made.getAbsolutePath();


                        try {
                            fileBase65 = Utils.encodeFileToBase64Binary(path);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        textnote.setText("File Size: " + "" + "" + a_size);
                        textnote.setVisibility(View.VISIBLE);

                        fileBase64 = null;
                        hwbitmap = null;
                        voice_type="upload";
                        imageView6.setImageResource(R.drawable.sound_file);
                        scrollView.setVisibility(View.GONE);
                        imageView6.setVisibility(View.VISIBLE);
                        playRecording("picked");
                        Log.d("selectedImageUri", String.valueOf(selectedImageUri));

                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }

            }  else {

                if (type == 2) {

                    uri = data.getData();
                    // String path = Utils.getPath(this, uri);

                    if (uri != null) {
                        try {
                            ContentResolver contentResolver = getContentResolver();
                            InputStream inputStream = contentResolver.openInputStream(uri);
                            file = new File(getCacheDir(), "temp.pdf");
                            OutputStream outputStream = new FileOutputStream(file);
                            byte[] buffer = new byte[4 * 1024]; // or other buffer size
                            int read;

                            while ((read = inputStream.read(buffer)) != -1) {
                                outputStream.write(buffer, 0, read);
                            }

                            outputStream.flush();
                            outputStream.close();
                            inputStream.close();

                            String path = file.getAbsolutePath();
                            long fileSize = file.length();
                            size = formatSize(fileSize);
                            if (fileSize > 18 * 1024 * 1024) { // 18 MB
                                Toast.makeText(getApplicationContext(),
                                        "Sorry, Your selected file size is too large (" + size + ")." +
                                                "Please select a file with a size of less than 18 MB.", Toast.LENGTH_LONG).show();
                            } else {
                                fileBase64 = Utils.encodeFileToBase64Binary(path);
                                hwbitmap = null;
                                fileBase65 = null;
                                showPdfFromUri(uri, path, null);
                                textnote.setText("Pdf Size: " + "" + "" + size);
                                textnote.setVisibility(View.VISIBLE);
                                imageView6.setImageResource(R.drawable.pdfbig);
                                scrollView.setVisibility(View.GONE);
                                imageView6.setVisibility(View.VISIBLE);
                                Toast.makeText(this, "File Picked...", Toast.LENGTH_SHORT).show();
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

        } else {
            Toast.makeText(this, "You haven't picked file", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onImagesSelected(List<? extends Uri> list, String s) {
        commonCode(list);

    }

    private void commonCode(List<? extends Uri> list) {
        scrollView.setVisibility(View.VISIBLE);
        imageView6.setVisibility(View.GONE);
        imageContainer.removeAllViews();
        image = new ArrayList<>();
        image2 = list;
        for (Uri uri : list) {
            ImageView iv = (ImageView) LayoutInflater.from(this).inflate(R.layout.scrollitem_image, imageContainer, false);
            imageContainer.addView(iv);
            Glide.with(this).load(uri).into(iv);
            hwbitmap = Utils.getBitmapFromUri(AddNoticeBoard.this, uri, 2048);
            Log.e("hwbitmapup", String.valueOf(hwbitmap));
            encoded = Utils.encodeToBase64(hwbitmap, Bitmap.CompressFormat.JPEG, 50);
            image.add(encoded);
        }
        Log.e("hwbitmapupdown", String.valueOf(hwbitmap));
        Log.e("image2", String.valueOf(image2));
        //   add_img="add";
        textnote.setVisibility(View.VISIBLE);
        textnote.setText("Image: You have selected " + String.valueOf(image.size()) + " images");
        fileBase64 = null;
        fileBase65 = null;
    }

    File file;
    @SuppressLint("MissingInflatedId")
    private void showPdfFromUri(Uri uri, String path, InputStream inputStream) {
        mBottomSheetDialog1 = new BottomSheetDialog(AddNoticeBoard.this);
        final View sheetView = getLayoutInflater().inflate(R.layout.pdf_view_dialog, null);
        mBottomSheetDialog1.setContentView(sheetView);

        mBottomSheetDialog1.setCancelable(false);

        TextView title = (TextView) sheetView.findViewById(R.id.title);
        TextView path1 = (TextView) sheetView.findViewById(R.id.path);
        title.setText("Message PDF");
        //  path1.setText(path);
        path1.setText("" + "" + "" + size);
        //pdf code start here
        RecyclerView recyclerView;
        PdfPageAdapter adapter;
        recyclerView = (RecyclerView)  sheetView.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        if (file.exists()) {
            try {
                ParcelFileDescriptor parcelFileDescriptor = ParcelFileDescriptor.open(file, ParcelFileDescriptor.MODE_READ_ONLY);

                // Check if ParcelFileDescriptor is null
                if (parcelFileDescriptor == null) {
                    Log.e("PDF_ERROR", "ParcelFileDescriptor is null. Unable to open the file.");
                    //   Toast.makeText(this, "Unable to open the PDF file", Toast.LENGTH_SHORT).show();
                    return;
                }

                PdfRenderer pdfRenderer = new PdfRenderer(parcelFileDescriptor);
                int pageCount = pdfRenderer.getPageCount();

                // Log the number of pages found in the PDF
                Log.d("PDF_INFO", "Total number of pages: " + pageCount);

                List<Bitmap> bitmaps = new ArrayList<>();

                for (int i = 0; i < pageCount; i++) {
                    PdfRenderer.Page pdfPage = pdfRenderer.openPage(i);

                    // Check if pdfPage is null
                    if (pdfPage == null) {
                        Log.e("PDF_ERROR", "Failed to open page " + i);
                        continue;
                    }

                    Bitmap bitmap = Bitmap.createBitmap(pdfPage.getWidth(), pdfPage.getHeight(), Bitmap.Config.ARGB_8888);
                    pdfPage.render(bitmap, null, null, PdfRenderer.Page.RENDER_MODE_FOR_DISPLAY);
                    bitmaps.add(bitmap);

                    // Log successful page rendering
                    Log.d("PDF_INFO", "Page " + i + " rendered successfully.");

                    pdfPage.close();
                }

                adapter = new PdfPageAdapter(this, bitmaps);
                recyclerView.setAdapter(adapter);

                pdfRenderer.close();
                parcelFileDescriptor.close();

            } catch (FileNotFoundException e) {
                Log.e("PDF_ERROR", "File not found: " + e.getMessage());
                Toast.makeText(this, "PDF file not found", Toast.LENGTH_SHORT).show();
            } catch (SecurityException e) {
                Log.e("PDF_ERROR", "Permission denied: " + e.getMessage());
                Toast.makeText(this, "Permission denied", Toast.LENGTH_SHORT).show();
            } catch (IOException e) {
                Log.e("PDF_ERROR", "IO Exception: " + e.getMessage());
                Toast.makeText(this, "Failed to open the PDF file", Toast.LENGTH_SHORT).show();
            }
        } else {
            Log.e("PDF_ERROR", "File does not exist at the path: " + path);
            Toast.makeText(this, "PDF file not found at: " + path, Toast.LENGTH_SHORT).show();
        }
        //  pdf.fromStream(inputStream).load();
        Button retake = (Button) sheetView.findViewById(R.id.retake);
        retake.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                retake.startAnimation(animation);
                pick_pdf();
                mBottomSheetDialog1.dismiss();
            }
        });
        Button submitted = (Button) sheetView.findViewById(R.id.submitted);
        submitted.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                submitted.startAnimation(animation);
                mBottomSheetDialog1.dismiss();
                //  submitAssignment();

            }
        });
        RelativeLayout btnNo = (RelativeLayout) sheetView.findViewById(R.id.btnNo);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnNo.startAnimation(animation);
                //   fileBase64=null;
                mBottomSheetDialog1.dismiss();
            }
        });
        mBottomSheetDialog1.show();
    }

    public static String formatSize(long size) {
        String suffix = "B";
        double formattedSize = size;

        if (formattedSize >= 1024) {
            suffix = "KB";
            formattedSize /= 1024;
        }

        if (formattedSize >= 1024) {
            suffix = "MB";
            formattedSize /= 1024;
        }

        if (formattedSize >= 1024) {
            suffix = "GB";
            formattedSize /= 1024;
        }

        return String.format("%.2f %s", formattedSize, suffix);
    }

    @OnClick(R.id.imageView6)
    public void imagecheck() {
        imageView6.startAnimation(animation);
        if (hwbitmap != null) {
            final Dialog EventDialog = new Dialog(this);
            EventDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            EventDialog.setCancelable(true);
            EventDialog.setContentView(R.layout.image_lyt);
            EventDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            ImageView imgShow = (ImageView) EventDialog.findViewById(R.id.imgShow);
            ImageView imgDownload = (ImageView) EventDialog.findViewById(R.id.imgDownload);
            imgDownload.setVisibility(View.GONE);
            imgShow.setImageBitmap(hwbitmap);
            // Picasso.with(this).load(String.valueOf(hwbitmap)).placeholder(getResources().getDrawable(R.drawable.home)).into(imgShow);
            EventDialog.show();
            Window window = EventDialog.getWindow();
            window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        } else if (fileBase64 != null) {
            InputStream inputStream = null;
            showPdfFromUri(uri, path, inputStream);
        } else if (fileBase65 != null) {
            playRecording("picked");
        } else {
            Toast.makeText(getApplicationContext(), "Please Select Message IMAGE/PDF/VOICE", Toast.LENGTH_LONG).show();
        }
    }


    @OnClick(R.id.button3)
    public void save() {
         //   Toast.makeText(AddNoticeBoard.this,"es_type is :" + p_type,Toast.LENGTH_LONG).show();


        if(checkValid()) {
           commonProgress.show();
            HashMap<String, String> params = new HashMap<>();
            params.put("title", edittitle.getText().toString());
            params.put("msg", editnotice.getText().toString());

            if (p_type.equalsIgnoreCase("Student")) {
                params.put("check", "Student");
            }else if (p_type.equalsIgnoreCase("Staff")) {
                params.put("check", "Staff");
            }else {
                params.put("check", "All");
            }
//            if (hwbitmap != null) {
//                String encoded = Utils.encodeToBase64(hwbitmap, Bitmap.CompressFormat.JPEG, 50);
//                encoded = String.format("data:image/jpeg;base64,%s", encoded);
//                params.put("image", encoded);
//            }


            if (hwbitmap != null) {
                params.put("image", String.valueOf(image));
                params.put("h_type", "image");
                params.put("add_img", "add");
            }
            if (fileBase64 != null) {
                params.put("file", fileBase64);
                params.put("h_type", "pdf");
                Log.e("file", fileBase64);
            }
            if (fileBase65 != null) {
                params.put("recording", fileBase65);
                params.put("h_type", "voice");
                Log.e("recording", fileBase65);

            }

            params.put("new_msg","new");


            ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.ADDNOTICE_URL, new ApiHandler.ApiCallback() {
                @Override
                public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                    commonProgress.dismiss();
                    if (error == null) {
                        try {
                            Toast.makeText(AddNoticeBoard.this, jsonObject.getString("msg"), Toast.LENGTH_SHORT).show();
                            finish();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else {
                        Toast.makeText(AddNoticeBoard.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            }, this, params);

        }


    }

    private boolean checkValid() {
        boolean valid = true;
        String errorMsg = null;
//        if (hwbitmap == null) {
//            valid = false;
//            errorMsg = "Please Put your Homework image";
//        } else
        if (edittitle.getText().toString().trim().length() <= 0) {
            valid = false;
            errorMsg = "Please enter Title";
        }
      else  if (editnotice.getText().toString().trim().length() <= 0) {
            valid = false;
            errorMsg = "Please enter Notice";
        }
        if (!valid) {
            Utils.showSnackBar(errorMsg, parent);
        }
        return valid;
    }
    @OnClick(R.id.imgBack)
    public void onback() {
        finish();
    }
}
