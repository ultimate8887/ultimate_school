package com.ultimate.ultimatesmartschool.StuGatePass;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.Request;
import com.github.siyamed.shapeimageview.CircularImageView;

import com.squareup.picasso.Picasso;

import com.ultimate.ultimatesmartschool.Activity.CustomSignatureView;
import com.ultimate.ultimatesmartschool.BeanModule.ClassBean;
import com.ultimate.ultimatesmartschool.BeanModule.SectionBean;
import com.ultimate.ultimatesmartschool.BeanModule.Studentbean;
import com.ultimate.ultimatesmartschool.BeanModule.User;
import com.ultimate.ultimatesmartschool.ClassTest.Messageclass_adpter;
import com.ultimate.ultimatesmartschool.Homework.SectionnewAdapter;
import com.ultimate.ultimatesmartschool.Login.LoginActivity;
import com.ultimate.ultimatesmartschool.MSG_NEW.Fragments.Stu_adapterGds;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.CommonProgress;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AddStuGatePass extends AppCompatActivity {
    @BindView(R.id.imgBack)
    ImageView back;
    @BindView(R.id.txtTitle)
    TextView txtTitle;

    @BindView(R.id.g_details)
    RelativeLayout top_lyt;

    @BindView(R.id.firstcard)
    CardView firstcard;

    @BindView(R.id.staff_sign)
    TextView staff_sign;
    @BindView(R.id.student_sign)
    TextView student_sign;

    @BindView(R.id.std_img)
    ImageView std_img;
    @BindView(R.id.staff_img)
    ImageView staff_img;

    String sectionid="",sectionname="";
    @BindView(R.id.spinnersection1)
    Spinner spinnersection;
    ArrayList<SectionBean> sectionList = new ArrayList<>();

    ArrayList<ClassBeans> classList = new ArrayList<>();
    @BindView(R.id.spinnerClasssec)
    Spinner classspin;
    String classid = "";
    String stu_id= "";
    String sturoll= "";
    String stuname= "";
    String classname="";
    String am_pm="",date="";
    @BindView(R.id.parent)
    RelativeLayout parent;

    @BindView(R.id.spinnerStudent)
    Spinner spinnerStudent;
    private Stu_adapterGds adapterstu;
    private ArrayList<Studentbean> stuList;

    @BindView(R.id.profile)
    CircularImageView image;

    @BindView(R.id.gard_profile)
    CircularImageView gard_profile;


    @BindView(R.id.edtName)
    TextView edtName;

    @BindView(R.id.edtgName)
    TextView edtgName;

    @BindView(R.id.edtFather)
    TextView edtFather;

    @BindView(R.id.edtRegistration)
    TextView edtRegistration;

    @BindView(R.id.edtRelation)
    TextView edtRelation;

    CommonProgress commonProgress;

    @BindView(R.id.edtMobile)
    TextView edtMobile;

    @BindView(R.id.edtgMobile)
    TextView edtgMobile;

    @BindView(R.id.edtTime)
    TextView edtTime;

    @BindView(R.id.edtReason)
    TextView edtReason;

    @BindView(R.id.btnSignIn)
    Button btnSignIn;

    private int selection;

    @BindView(R.id.spinner)
    Spinner spinner;

    private ArrayList<GatePassBean> pList;
    private PersonAdapter personAdapter;
    String img="",img1="",gid="";
    String sFather="",sName="",sRegistration="",sMobile="",gName="",gMobile="",gRelation="";
    private Bitmap cust_bitmap,staff_bitmap,userBitmapProf;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_stu_gate_pass);
        ButterKnife.bind(this);
        txtTitle.setText("Apply Gate-Pass");
        commonProgress=new CommonProgress(this);

        Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
        Date setdate = calendar.getTime();
        SimpleDateFormat fmtOut = new SimpleDateFormat("dd MMM, yyyy");
        String dateString = fmtOut.format(setdate);
        //adddate.setText(dateString);
        SimpleDateFormat dateFrmOut = new SimpleDateFormat("yyyy-MM-dd");
        date = dateFrmOut.format(setdate);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        initalView();
        fetchclasslist();

    }

    private void fetchPerson(String stu_id) {
        commonProgress.show();
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("stu_id", stu_id);
        params.put("check", "person");
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.STDGATEPASS_URL, apiCallbackstudnt, getApplicationContext(), params);
    }

    ApiHandler.ApiCallback apiCallbackstudnt = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            commonProgress.dismiss();
            if (error == null) {
                try {
                    pList = GatePassBean.parseClassArray(jsonObject.getJSONArray(Constants.GATEPASS_DATA));
                    personAdapter = new PersonAdapter(getApplicationContext(), pList);
                    spinner.setAdapter(personAdapter);
                    spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if (i > 0) {
                                btnSignIn.setVisibility(View.VISIBLE);
                                top_lyt.setVisibility(View.VISIBLE);
                                setPersondata(pList.get(i-1));
                            } else {
                                top_lyt.setVisibility(View.GONE);
                                btnSignIn.setVisibility(View.INVISIBLE);
                                edtgName.setText("");
                                edtgMobile.setText("");
                                edtRelation.setText("");
                                Picasso.get().load(R.drawable.receptionistsss).into(gard_profile);
                                img="";
                                gid="";
                                stu_id="";
                            }
                        }
                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                top_lyt.setVisibility(View.GONE);
                btnSignIn.setVisibility(View.INVISIBLE);
                Toast.makeText(getApplicationContext(),"Guardian details not found, kindly add Guardian details first to add gate-pass.",Toast.LENGTH_LONG).show();
            }
        }
    };

    private void setPersondata(GatePassBean gatePassBean) {
        gid=gatePassBean.getId();

        gName=gatePassBean.getGate_person();
        gMobile=gatePassBean.getGate_personcont();
        gRelation=gatePassBean.getGate_relation();

        if (gatePassBean.getGate_person() != null) {
            String title = getColoredSpanned("Name: ", "#5A5C59");
            String Name = getColoredSpanned(gatePassBean.getGate_person(), "#000000");
            edtgName.setText(Html.fromHtml(title + " " + Name));
        }

        if (gatePassBean.getGate_personcont() != null) {
            String title = getColoredSpanned("Contact No: ", "#5A5C59");
            String Name = getColoredSpanned(gatePassBean.getGate_personcont(), "#000000");
            edtgMobile.setText(Html.fromHtml(title + " " + Name));
        }

        if (gatePassBean.getGate_relation() != null) {
            String title = getColoredSpanned("Relation: ", "#5A5C59");
            String Name = getColoredSpanned(gatePassBean.getGate_relation(), "#000000");
            edtRelation.setText(Html.fromHtml(title + " " + Name));
        }

        String path= Constants.getImageBaseURL()+ "office_admin/images/gatepass/";
        img=gatePassBean.getGate_profile();
        if (img != null) {
            //Picasso.with(mContext).load(path+stulist.get(position).getGate_profile()).placeholder(R.drawable.stud).into(viewHolder.circleimg);
            Utils.progressImg(path+img,gard_profile,AddStuGatePass.this);
        } else {
            Picasso.get().load(R.drawable.receptionistsss).into(gard_profile);
        }


    }

    private void getStdData(Studentbean studentbean) {

        if (pList != null) {
            pList.clear();
            personAdapter.notifyDataSetChanged();
        }
        stu_id = studentbean.getId();
        stuname=studentbean.getName();
        sturoll=studentbean.getRoll_no();
        sFather=studentbean.getFather_name();
        sName=studentbean.getName();
        sRegistration=studentbean.getId();
        sMobile=studentbean.getPhoneno();

        if (studentbean.getName() != null) {
            String title = getColoredSpanned("Name: ", "#5A5C59");
            String Name = getColoredSpanned(studentbean.getName(), "#000000");
            edtName.setText(Html.fromHtml(title + " " + Name));
        }

        if (studentbean.getFather_name() != null) {
            String title = getColoredSpanned("Father's Name: ", "#5A5C59");
            String Name = getColoredSpanned(studentbean.getFather_name(), "#000000");
            edtFather.setText(Html.fromHtml(title + " " + Name));
        }

        if (studentbean.getId() != null) {
            String title = getColoredSpanned("Registration No: ", "#5A5C59");
            String Name = getColoredSpanned(studentbean.getId(), "#000000");
            edtRegistration.setText(Html.fromHtml(title + " " + Name));
        }

        if (studentbean.getPhoneno() != null) {
            String title = getColoredSpanned("Contact No: ", "#5A5C59");
            String Name = getColoredSpanned(studentbean.getPhoneno(), "#000000");
            edtMobile.setText(Html.fromHtml(title + " " + Name));
        }

        img1=studentbean.getProfile();
        if (img1 != null) {
            //Picasso.with(mContext).load(path+stulist.get(position).getGate_profile()).placeholder(R.drawable.stud).into(viewHolder.circleimg);
            Utils.progressImg(img1,image,AddStuGatePass.this);
        } else {
            Picasso.get().load(R.drawable.stud).into(image);
        }

        fetchPerson(stu_id);
    }

    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }

    private void initalView() {
        edtFather.setEnabled(false);
        edtMobile.setEnabled(false);
        edtRegistration.setEnabled(false);
        edtName.setEnabled(false);
    }


//    @SuppressLint("MissingSuperCall")
//    @Override
//    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//
//        if (resultCode == Activity.RESULT_OK) {
//            switch (requestCode) {
//                case CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE:
//                    Uri imageUri = CropImage.getPickImageResultUri(AddStuGatePass.this, data);
//                    CropImage.activity(imageUri).setCropShape(CropImageView.CropShape.RECTANGLE).setAspectRatio(1, 1)
//                            .start(AddStuGatePass.this);
//                    break;
//
//                case CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE:
//                    CropImage.ActivityResult result = CropImage.getActivityResult(data);
//                    Uri resultUri = result.getUri();
//                    userBitmapProf = Utils.getBitmapFromUri(AddStuGatePass.this, resultUri, 2048);
//                    image.setImageBitmap(userBitmapProf);
//
//            }
//        }
//
//    }

    @OnClick(R.id.btnSignIn)
    public void btnSignInnn() {
        if (edtName.getText().toString().isEmpty()) {
            Toast.makeText(getApplicationContext(), "Kindly select class Student", Toast.LENGTH_LONG).show();
        } else if (edtTime.getText().toString().equalsIgnoreCase("Set Check Out Time")) {
            Toast.makeText(getApplicationContext(), "Set check-out Time", Toast.LENGTH_LONG).show();
        }else if (edtgName.getText().toString().isEmpty()) {
            Toast.makeText(getApplicationContext(), "Kindly select Guardian", Toast.LENGTH_LONG).show();
        }else {
            commonProgress.show();
            HashMap<String, String> params = new HashMap<>();
            params.put("gate_class", classid);
            params.put("gate_section", sectionid);
            params.put("gate_regid", sRegistration);
            params.put("gate_sname", sName);

            params.put("gate_fname", sFather);
            params.put("gate_mobile", sMobile);
            params.put("gate_personcont", gMobile);
//        if (userBitmapProf != null) {
//            String encoded = Utility.encodeToBase64(userBitmapProf, Bitmap.CompressFormat.JPEG, 50);
//            encoded = String.format("data:image/jpeg;base64,%s", encoded);
            params.put("gate_profile", img);
            //  }
            params.put("gate_person", gName);
            params.put("gate_relation", gRelation);

            params.put("gate_timeout", edtTime.getText().toString());

            params.put("gate_datetime", am_pm);
            params.put("gate_date", date);
            params.put("gate_reason", edtReason.getText().toString());
            //   params.put("staff_rating", staff_rate);

            if (cust_bitmap != null) {
                String encoded = Utils.encodeToBase64(cust_bitmap, Bitmap.CompressFormat.JPEG, 90);
                encoded = String.format("data:image/jpeg;base64,%s", encoded);
                params.put("std_sign", encoded);
            }
            if (staff_bitmap != null) {
                String encoded = Utils.encodeToBase64(staff_bitmap, Bitmap.CompressFormat.JPEG, 90);
                encoded = String.format("data:image/jpeg;base64,%s", encoded);
                params.put("staff_sign", encoded);
            }

            ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.ADDSTDGATEPASS, apisendmsgCallback, this, params);

        }
    }
    ApiHandler.ApiCallback apisendmsgCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            commonProgress.dismiss();
            if (error == null) {
                Toast.makeText(getApplicationContext(),"Generate Successfully",Toast.LENGTH_LONG).show();
                // Utility.showSnackBar(jsonObject.getString("msg"), parent);
                finish();
            } else {
                Log.e("error", error.getMessage() + "");
                Utils.showSnackBar(error.getMessage(), parent);
                // Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_LONG).show();

            }
        }
    };



    @OnClick(R.id.staff_sign)
    public void staff_sign(){

        //    cust_sign_head.setTextColor(getColor(R.color.colorAccent));

        final Dialog warningDialog = new Dialog(this);
        warningDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        warningDialog.setCancelable(true);

        warningDialog.setContentView(R.layout.signature_dialog);
        warningDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        RelativeLayout btnClose = (RelativeLayout) warningDialog.findViewById(R.id.btnNo);

        final CustomSignatureView signatureView= warningDialog.findViewById(R.id.signature_view);
        //   Button btnClear = (Button) warningDialog.findViewById(R.id.btnClear);
        Button btnYes = (Button) warningDialog.findViewById(R.id.btnOk);
        Button btnClear = (Button) warningDialog.findViewById(R.id.btnClear);
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                warningDialog.dismiss();
            }
        });

        btnClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signatureView.clear();
                staff_img.setVisibility(View.GONE);
            }
        });

        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                warningDialog.dismiss();
                ErpProgress.showProgressBar(AddStuGatePass.this, "Please wait...");
                staff_bitmap = signatureView.getSignatureBitmap();
                // path = saveImage(bitmap);
                staff_img.setVisibility(View.VISIBLE);
                staff_img.setImageBitmap(staff_bitmap);
                ErpProgress.cancelProgressBar();
            }

        });

        warningDialog.show();


    }


    @OnClick(R.id.student_sign)
    public void student_sign(){

        //    cust_sign_head.setTextColor(getColor(R.color.colorAccent));

        final Dialog warningDialog = new Dialog(this);
        warningDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        warningDialog.setCancelable(true);

        warningDialog.setContentView(R.layout.signature_dialog);
        warningDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        RelativeLayout btnClose = (RelativeLayout) warningDialog.findViewById(R.id.btnNo);

        final CustomSignatureView signatureView=warningDialog.findViewById(R.id.signature_view);
        //   Button btnClear = (Button) warningDialog.findViewById(R.id.btnClear);
        Button btnYes = (Button) warningDialog.findViewById(R.id.btnOk);
        Button btnClear = (Button) warningDialog.findViewById(R.id.btnClear);
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                warningDialog.dismiss();
            }
        });

        btnClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signatureView.clear();
                staff_img.setVisibility(View.GONE);
            }
        });

        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                warningDialog.dismiss();
                ErpProgress.showProgressBar(AddStuGatePass.this, "Please wait...");
                cust_bitmap = signatureView.getSignatureBitmap();
                // path = saveImage(bitmap);
                std_img.setVisibility(View.VISIBLE);
                std_img.setImageBitmap(cust_bitmap);
                ErpProgress.cancelProgressBar();
            }

        });

        warningDialog.show();

    }


    @OnClick(R.id.edtTime)
    public void edtTimemm(){
// TODO Auto-generated method stub
        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(AddStuGatePass.this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int hourOfDay, int selectedMinute) {
                // edtTime.setText( selectedHour + ":" + selectedMinute );
                String status = "AM";
                if(hourOfDay > 11)
                {
                    // If the hour is greater than or equal to 12
                    // Then the current AM PM status is PM
                    status = "PM";
                }
                if (status.equalsIgnoreCase("AM")){
                    am_pm="Morning";
                }else {
                    am_pm="Evening";
                }
                // Initialize a new variable to hold 12 hour format hour value
                int hour_of_12_hour_format;
                if(hourOfDay > 11){
                    // If the hour is greater than or equal to 12
                    // Then we subtract 12 from the hour to make it 12 hour format time
                    hour_of_12_hour_format = hourOfDay - 12;
                }
                else {
                    hour_of_12_hour_format = hourOfDay;
                }
                String minute="";
                if (selectedMinute==0){
                    minute="00";
                }else if (selectedMinute==1){
                    minute="01";
                }else if (selectedMinute==2){
                    minute="02";
                }else if (selectedMinute==3){
                    minute="03";
                }else if (selectedMinute==4){
                    minute="04";
                }else if (selectedMinute==5){
                    minute="05";
                }else if (selectedMinute==6){
                    minute="06";
                }else if (selectedMinute==7){
                    minute="07";
                }else if (selectedMinute==8){
                    minute="08";
                }else if (selectedMinute==9){
                    minute="09";
                }else {
                    minute= String.valueOf(selectedMinute);
                }

                // Display the 12 hour format time in app interface
                if (hour_of_12_hour_format==0){
                    hour_of_12_hour_format=12;
                    edtTime.setText( hour_of_12_hour_format + " : " + minute + " : " + status );
                }else {
                    edtTime.setText( hour_of_12_hour_format + " : " + minute + " : " + status );
                }


            }
        }, hour, minute, false);//Yes 24 hour time
        mTimePicker.setTitle("Select Time");
        mTimePicker.show();
    }

    private void fetchclasslist() {
        commonProgress.show();
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("check","gatepass");
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.CLASSLIST_URL, classapiCallback, this, params);
    }



    ApiHandler.ApiCallback classapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            commonProgress.dismiss();
            if (error == null) {
                try {
                    classList = ClassBeans.parseClassArray(jsonObject.getJSONArray(Constants.CLASSDATA));
                    Messageclass_adpter adapter = new Messageclass_adpter(AddStuGatePass.this, classList);
                    classspin.setAdapter(adapter);
                    classspin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                            if (i == 0) {

                                if (adapterstu != null) {
                                    stuList.clear();
                                    adapterstu.notifyDataSetChanged();
                                }
                            }
                            if (i > 0) {

                                classid = classList.get(i-1).getId();
                                classname=classList.get(i-1).getName();
                                stu_id="";

                                Log.e("classsid",classid);
                                fetchSection();

                            }else{
                                classid="";
                            }

                            edtFather.setText("");
                            edtName.setText("");
                            edtRegistration.setText("");
                            edtMobile.setText("");


                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {

                Utils.showSnackBar(error.getMessage(), parent);
            }
        }
    };

    private void fetchSection() {
        HashMap<String, String> params = new HashMap<String, String>();
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.SECTIONLIST_URL, sectionapiCallback, getApplicationContext(), params);
    }

    ApiHandler.ApiCallback sectionapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            if (error == null) {
                try {
                    sectionList = SectionBean.parseCommonArray(jsonObject.getJSONArray("hostelroom_data"));
                    SectionnewAdapter adapter = new SectionnewAdapter(getApplicationContext(), sectionList);
                    spinnersection.setAdapter(adapter);
                    spinnersection.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if (i > 0) {
                                sectionid = sectionList.get(i - 1).getSection_id();
                                sectionname = sectionList.get(i - 1).getSection_name();
                                // fetchHomework();
                                  fetchStudent(classid,sectionid);
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_LONG).show();
                // Utils.showSnackBar(error.getMessage(), parent);
                if (error.getStatusCode() == 405) {
                    User.logout();
                    startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                    finish();
                }
            }
        }
    };

    private void fetchStudent(String classid,String sectionid) {
        commonProgress.show();
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("class_id",classid);
        params.put("section_id",sectionid);
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.STUDENTLISTCLSSECWISE_URL, apiCallback, this, params);

    }

    ApiHandler.ApiCallback apiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            commonProgress.dismiss();
            if (error == null) {
                try {
                    stuList = Studentbean.parseHWArray(jsonObject.getJSONArray("student_list"));
                    adapterstu = new Stu_adapterGds(AddStuGatePass.this, stuList);
                    spinnerStudent.setAdapter(adapterstu);
                    spinnerStudent.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                            if (i == 0) {
                                if (pList != null) {
                                    pList.clear();
                                    personAdapter.notifyDataSetChanged();
                                }
                            }

                            edtFather.setText("");
                            edtName.setText("");
                            edtRegistration.setText("");
                            edtMobile.setText("");

                            if (i > 0) {
                                //  btnSignIn.setVisibility(View.VISIBLE);
                                firstcard.setVisibility(View.VISIBLE);
                                getStdData(stuList.get(i-1));
                            } else {
                                // btnSignIn.setVisibility(View.GONE);
                                firstcard.setVisibility(View.GONE);
                                edtFather.setText("");
                                edtName.setText("");
                                edtRegistration.setText("");
                                edtMobile.setText("");
                                stu_id = "";
                                if (pList != null) {
                                    pList.clear();
                                    personAdapter.notifyDataSetChanged();
                                }
                            }

                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                btnSignIn.setVisibility(View.GONE);
                firstcard.setVisibility(View.GONE);
            }
        }
    };

}
