package com.ultimate.ultimatesmartschool.StuGatePass;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.ultimate.ultimatesmartschool.BeanModule.Studentbean;
import com.ultimate.ultimatesmartschool.R;

import java.util.ArrayList;

public class StudentAdapters extends BaseAdapter {
    Context context;
    LayoutInflater inflter;
    private ArrayList<Studentbean> studentList;

    public StudentAdapters(Context applicationContext, ArrayList<Studentbean> studentList) {
        this.context = applicationContext;
        this.studentList = studentList;
        inflter = (LayoutInflater.from(applicationContext));
    }

    @Override
    public int getCount() {
        return studentList.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        view = inflter.inflate(R.layout.spinner_lyt_msg, null);
        TextView label = (TextView) view.findViewById(R.id.txtText);

        Studentbean classObj = studentList.get(i);
        if(classObj.getCroll_no()!=null) {
            label.setText("(" + classObj.getCroll_no() + ")" + classObj.getName());
        }else{
            label.setText( classObj.getName());
        }


        return view;
    }

    public void setStudentList(ArrayList<Studentbean> studentList) {
        this.studentList = studentList;
    }
}
