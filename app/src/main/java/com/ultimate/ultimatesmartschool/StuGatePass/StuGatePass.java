package com.ultimate.ultimatesmartschool.StuGatePass;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.ultimate.ultimatesmartschool.BeanModule.User;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class StuGatePass extends AppCompatActivity implements GatePassAdapter.Mycallback{
    @BindView(R.id.imgBack)
    ImageView back;

    @BindView(R.id.parent)
    RelativeLayout parent;
    @BindView(R.id.textNorecord)
    TextView txtNorecord;
    private LinearLayoutManager layoutManager;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerview;
    ArrayList<GatePassBean> list;
    private GatePassAdapter adapter;
    @BindView(R.id.txtTitle)
    TextView txtTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stu_gate_pass);
        ButterKnife.bind(this);
        txtTitle.setText("Gatepass");
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        list = new ArrayList<>();
        layoutManager = new LinearLayoutManager(StuGatePass.this);
        recyclerview.setLayoutManager(layoutManager);
        adapter = new GatePassAdapter(list, StuGatePass.this, this);
        recyclerview.setAdapter(adapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        fetchGatePass();
    }

    private void fetchGatePass() {
        ErpProgress.showProgressBar(this, "Please wait...");
        ApiHandler.apiHit(Request.Method.GET, Constants.getBaseURL() + Constants.GATEPASS_URL, apiCallback, this, null);

    }

    ApiHandler.ApiCallback apiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            if (error == null) {
                try {
                    if (list != null) {
                        list.clear();
                    }
                    JSONArray jsonArray = jsonObject.getJSONArray(Constants.GATEPASS_DATA);
                    list = GatePassBean.parseClassArray(jsonArray);
                    if (list.size() > 0) {
                        adapter.setGatePassList(list);
                        adapter.notifyDataSetChanged();
                        txtNorecord.setVisibility(View.GONE);
                    } else {
                        adapter.setGatePassList(list);
                        adapter.notifyDataSetChanged();
                        txtNorecord.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                txtNorecord.setVisibility(View.VISIBLE);
                list.clear();
                adapter.setGatePassList(list);
                adapter.notifyDataSetChanged();
            }
        }
    };



    @Override
    public void onApproveCallback(GatePassBean gatePassBean) {

    }

    @Override
    public void onUNApproveCallback(GatePassBean gatePassBean) {

    }
}
