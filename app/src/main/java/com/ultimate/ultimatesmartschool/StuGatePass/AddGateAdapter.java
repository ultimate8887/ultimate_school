package com.ultimate.ultimatesmartschool.StuGatePass;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.github.siyamed.shapeimageview.CircularImageView;
import com.squareup.picasso.Picasso;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.Utils;


import java.util.ArrayList;

public class AddGateAdapter extends RecyclerView.Adapter<AddGateAdapter.Viewholder>{
    ArrayList<GatePassBean> stulist;
    Context mContext;
    StudentPro mStudentPro;

    public AddGateAdapter(ArrayList<GatePassBean> stulist, Context mContext, StudentPro mStudentPro) {
        this.mContext = mContext;
        this.mStudentPro=mStudentPro;
        this.stulist = stulist;
    }

    @Override
    public Viewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_news_one, parent, false);
        Viewholder viewholder = new Viewholder(view);
        return viewholder;
    }

    @Override
    public void onBindViewHolder(final Viewholder viewHolder, @SuppressLint("RecyclerView") final int position) {

        GatePassBean mData=stulist.get(position);

             String path= Constants.getImageBaseURL()+ "office_admin/images/gatepass/";

            if (stulist.get(position).getGate_profile() != null) {
                //Picasso.with(mContext).load(path+stulist.get(position).getGate_profile()).placeholder(R.drawable.stud).into(viewHolder.circleimg);
                Utils.progressImg(path+stulist.get(position).getGate_profile(),viewHolder.circleimg,mContext);
            } else {
                Picasso.get().load(R.drawable.boy).into(viewHolder.circleimg);
            }

        if (mData.getId() != null) {
            String title = getColoredSpanned("ID: ", "#000000");
            String Name = getColoredSpanned("gp-"+mData.getId(), "#5A5C59");
            viewHolder.txtid.setText(Html.fromHtml(title + " " + Name));
        }

        // holder.employee_name.setText("Name: " + " " + viewstafflist.get(position).getName());
        if (mData.getGate_person() != null) {
            String title = getColoredSpanned("Name: ", "#000000");
            String Name = getColoredSpanned(mData.getGate_person(), "#5A5C59");
            viewHolder.textViewData.setText(Html.fromHtml(title + " " + Name));
        }else {
            viewHolder.textViewData.setText("Name: Not Mentioned");
        }

        //  holder.post.setText("Post:" + " " + viewstafflist.get(position).getPost());
        if (mData.getGate_personcont() != null) {
            String title = getColoredSpanned("Mobile: ", "#000000");
            String Name = getColoredSpanned(mData.getGate_personcont(), "#5A5C59");
            viewHolder.txtFather.setText(Html.fromHtml(title + " " + Name));
        }else {
            viewHolder.txtFather.setText("Mobile: Not Mentioned");
        }

        if (mData.getGate_relation() != null) {
            String title = getColoredSpanned("Relation: ", "#000000");
            String Name = getColoredSpanned(mData.getGate_relation(), "#5A5C59");
            viewHolder.txtClass.setText(Html.fromHtml(title + " " + Name));
        }
        else {
            viewHolder.txtClass.setText("Relation: Not Mentioned");
        }

        viewHolder.parent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mStudentPro!= null){
                    mStudentPro.onUpdateCallback(stulist.get(position));
                }
            }
        });

        viewHolder.trash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder1 = new AlertDialog.Builder(mContext);
                builder1.setMessage("Do you want to delete? ");
                builder1.setCancelable(false);
                builder1.setPositiveButton(
                        "Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                if (mStudentPro != null) {
                                    mStudentPro.StudentProfile(stulist.get(position));
                                }
                            }
                        });
                builder1.setNegativeButton(
                        "No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        });
                AlertDialog alert11 = builder1.create();
                alert11.show();
            }
        });

    }

    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }

    public interface StudentPro{
        public void StudentProfile(GatePassBean std);
        public void onUpdateCallback(GatePassBean std);
    }
    @Override
    public int getItemCount() {
        if (stulist != null)
            return stulist.size();
        return 0;
    }



    public void setHQList(ArrayList<GatePassBean> stulist) {
        this.stulist = stulist;
    }

    public class Viewholder extends RecyclerView.ViewHolder {

        TextView textViewData, txtMobile, txtFather, txtClass,txtid;
        ImageView trash,arrow;
        CardView update;
        RelativeLayout parent;
        CircularImageView circleimg;

        public Viewholder(View itemView) {
            super(itemView);
            parent =(RelativeLayout)itemView.findViewById(R.id.parent);
            circleimg=(CircularImageView)itemView.findViewById(R.id.circleimg);
            textViewData = (TextView) itemView.findViewById(R.id.name);
            txtFather = (TextView) itemView.findViewById(R.id.fathername);
            txtMobile = (TextView) itemView.findViewById(R.id.mobileno);
            txtClass = (TextView) itemView.findViewById(R.id.classess);
            trash = (ImageView) itemView.findViewById(R.id.trash);
            update = (CardView) itemView.findViewById(R.id.updates);
            arrow = (ImageView) itemView.findViewById(R.id.arrow);
            txtid=(TextView)itemView.findViewById(R.id.textViewid);
            arrow.setVisibility(View.VISIBLE);
            txtMobile.setVisibility(View.GONE);
            trash.setVisibility(View.VISIBLE);
            update.setVisibility(View.GONE);
        }
    }


}
