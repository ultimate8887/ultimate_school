package com.ultimate.ultimatesmartschool.StuGatePass;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.ultimate.ultimatesmartschool.R;

import java.util.ArrayList;

public class ClassAdapters extends BaseAdapter {
    private final ArrayList<ClassBeans> casteList;
    Context context;
    LayoutInflater inflter;

    public ClassAdapters(Context applicationContext, ArrayList<ClassBeans> casteList) {
        this.context = applicationContext;
        this.casteList = casteList;
        inflter = (LayoutInflater.from(applicationContext));
    }

    @Override
    public int getCount() {
        return casteList.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        view = inflter.inflate(R.layout.spinner_lyt_msg, null);
        TextView label = (TextView) view.findViewById(R.id.txtText);
        ClassBeans classObj = casteList.get(i);
        label.setText(classObj.getName());


        return view;
    }
}
