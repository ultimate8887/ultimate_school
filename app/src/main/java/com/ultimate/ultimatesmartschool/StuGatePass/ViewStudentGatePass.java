package com.ultimate.ultimatesmartschool.StuGatePass;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.DatePickerDialog;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.ultimate.ultimatesmartschool.BeanModule.User;
import com.ultimate.ultimatesmartschool.ClassWork.ViewClassWork;
import com.ultimate.ultimatesmartschool.Leave.OptionAdapter;
import com.ultimate.ultimatesmartschool.Leave.OptionBean;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.CommonProgress;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;
import com.ultimate.ultimatesmartschool.Utility.PaginationListener;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ViewStudentGatePass extends AppCompatActivity implements GatePassAdapter.Mycallback {


    @BindView(R.id.recyclerViewmsg)
    RecyclerView recyclerview;
    @BindView(R.id.parent)
    RelativeLayout parent;
    private LinearLayoutManager layoutManager;
    ArrayList<GatePassBean> list=new ArrayList<>();
    @BindView(R.id.textNorecord)
    TextView txtNorecord;
    int loaded = 0;
    String tag="all",save="All";
    SharedPreferences sharedPreferences;
    @BindView(R.id.totalRecord)
    TextView totalRecord;
    @BindView(R.id.today_date)
    TextView cal_text;
    ArrayList<OptionBean> vehicleList = new ArrayList<>();
    @BindView(R.id.spinerVehicletype)
    Spinner vehicleType;
    CommonProgress commonProgress;
    GatePassAdapter adapter;
    @BindView(R.id.txtTitle)TextView txtTitle;
    public String from_date="";
    private Bitmap admin_bitmap;

    private LinearLayoutManager layoutManager_new;
    @BindView(R.id.main_progress)
    ProgressBar main_progress;
    int  limit=10,page_limit=0,total_pages=0;
    private static final int PAGE_START = 1;
    private int currentPage = PAGE_START;
    private boolean isLastPage = false;
    private boolean isLoading = false;

    @BindView(R.id.root1)
    RelativeLayout root1;

    @BindView(R.id.scrollView)
    NestedScrollView scrollView;
    @BindView(R.id.pagination)
    LinearLayout pagination;

    Animation animation;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_staff_leave_list);
        ButterKnife.bind(this);
        txtTitle.setText("Student Gate-Pass");
        commonProgress=new CommonProgress(this);
//        Bundle extras = getIntent().getExtras();
//        if (extras != null) {
//             value = extras.getString("key");
//            //The key argument here must match that used in the other activity
//            Toast.makeText(getApplicationContext(), value, Toast.LENGTH_LONG).show();
//
//        }
        pagination.setVisibility(View.VISIBLE);
        scrollView.setVisibility(View.GONE);
        root1.setVisibility(View.VISIBLE);
        list = new ArrayList<>();
        layoutManager = new LinearLayoutManager(ViewStudentGatePass.this);
        animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.btn_blink_animation);

        recyclerview.setLayoutManager(layoutManager);
        adapter = new GatePassAdapter(list, ViewStudentGatePass.this,this);
        recyclerview.setAdapter(adapter);


        vehicleList.add(new OptionBean("Pending"));
        vehicleList.add(new OptionBean("Approved"));
        vehicleList.add(new OptionBean("Decline"));

        OptionAdapter dataAdapter = new OptionAdapter(ViewStudentGatePass.this, vehicleList);
        //set the ArrayAdapter to the spinner
        vehicleType.setAdapter(dataAdapter);
        //attach the listener to the spinner
        vehicleType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i != 0) {
                    tag = vehicleList.get(i - 1).getName();
                    if (tag.equalsIgnoreCase("Pending")){
                        tag="pending";
                        save="Check-In";
                    }else if (tag.equalsIgnoreCase("Decline")) {
                        tag = "decline";
                        save = "Decline";
                    }else{
                        tag="verify";
                        save="Check-Out";
                    }
                }else {
                    tag="all";
                    save="All";
                }

                limit=10;
                page_limit=0;
                total_pages=0;
                currentPage = PAGE_START;
                isLastPage = false;
                isLoading = false;

                cal_text.setText("Select date");
                fetchhwlist(tag,"",limit, "yes");

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        recyclerview.addOnScrollListener(new PaginationListener(layoutManager) {
            @Override
            protected void loadMoreItems() {
                // isLoading = true;
                //  Utils.showSnackBar("No more messages found in message list.", parent);
                //  currentPage++;
                // doApiCall();

                if (total_pages>=page_limit) {
                    main_progress.setVisibility(View.VISIBLE);
                    currentPage += 10;
                    page_limit = currentPage + limit;


                        fetchhwlist(tag,from_date, page_limit, "no");

                }else{
                    // main_progress.setVisibility(View.GONE);
                   //   Utils.showSnackBar("No more messages found in message list.", parent);
                }


            }
            @Override
            public boolean isLastPage() {
                return isLastPage;
            }
            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });


//        SimpleDateFormat timeStampFormat = new SimpleDateFormat("yyyy-MM-dd");
//        Date myDate = new Date();
//        from_date = timeStampFormat.format(myDate);
//        SimpleDateFormat fmtOut = new SimpleDateFormat("dd MMM, yyyy");
//        String dateString = fmtOut.format(myDate);
//        cal_text.setText(dateString);
//        fetchhwlist(from_date);
    }
    @Override
    protected void onResume() {
        super.onResume();
        //   fetchGatePass();

    }


    @OnClick(R.id.imgBack)
    public void backFinish() {
        finish();
    }

    @OnClick(R.id.today)
    public void cal_lyttttt(){

        java.util.Calendar cal = java.util.Calendar.getInstance(TimeZone.getDefault()); // Get current date
        DatePickerDialog datePicker = new DatePickerDialog(this,
                R.style.MyDatePickerDialogTheme, ondate,
                cal.get(java.util.Calendar.YEAR),
                cal.get(java.util.Calendar.MONTH),
                cal.get(java.util.Calendar.DAY_OF_MONTH));
        datePicker.getDatePicker();
        datePicker.setCancelable(false);
        datePicker.show();

    }

    DatePickerDialog.OnDateSetListener ondate = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            java.util.Calendar calendar = java.util.Calendar.getInstance(TimeZone.getDefault());
            calendar.set(year, monthOfYear, dayOfMonth);
            calendar.set(Calendar.HOUR_OF_DAY, 00);
            calendar.set(Calendar.MINUTE, 00);
            calendar.set(Calendar.SECOND, 00);
            Date setdate = calendar.getTime();
            SimpleDateFormat fmtOut = new SimpleDateFormat("dd MMM, yyyy");
            String dateString = fmtOut.format(setdate);
            cal_text.setText(dateString);
            SimpleDateFormat dateFrmOut = new SimpleDateFormat("yyyy-MM-dd");
            from_date = dateFrmOut.format(setdate);
            // Toast.makeText(HomeWorkByDate.this,from_date,Toast.LENGTH_SHORT).show();
            limit=10;
            page_limit=0;
            total_pages=0;
            currentPage = PAGE_START;
            isLastPage = false;
            isLoading = false;
            fetchhwlist(tag,from_date,limit,"yes");
        }
    };

    private void fetchhwlist(String tag, String from_date, int limit, String progress) {
        if (progress.equalsIgnoreCase("yes")){
            commonProgress.show();
        }
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("user_id", User.getCurrentUser().getId());
        params.put("date", from_date);
        params.put("check", tag);
        params.put("page_limit",String.valueOf(limit));
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.STDGATEPASS_URL, apiCallback, this, params);
    }


    ApiHandler.ApiCallback apiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            commonProgress.dismiss();
            if (error == null) {
                try {
                    if (list != null) {
                        list.clear();
                    }
                    JSONArray jsonArray = jsonObject.getJSONArray(Constants.GATEPASS_DATA);
                    list = GatePassBean.parseClassArray(jsonArray);
                    total_pages= Integer.parseInt(list.get(0).getRowcount());
                   // Toast.makeText(getApplicationContext(),String.valueOf(total_pages),Toast.LENGTH_SHORT).show();
                    if (list.size() > 0) {
                        adapter.setGatePassList(list);
                        //setanimation on adapter...
                        recyclerview.getAdapter().notifyDataSetChanged();
                        recyclerview.scheduleLayoutAnimation();
                        totalRecord.setText("Total Entries:- "+String.valueOf(total_pages));
                        //-----------end------------
                        txtNorecord.setVisibility(View.GONE);
                    } else {
                        totalRecord.setText("Total Entries:- 0");
                        adapter.setGatePassList(list);
                        adapter.notifyDataSetChanged();
                        txtNorecord.setVisibility(View.VISIBLE);
                    }
                    main_progress.setVisibility(View.GONE);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                totalRecord.setText("Total Entries:- 0");
                txtNorecord.setVisibility(View.VISIBLE);
                list.clear();
                adapter.setGatePassList(list);
                adapter.notifyDataSetChanged();
                Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_SHORT).show();
            }
        }
    };


    @Override
    public void onApproveCallback(GatePassBean gatePassBean) {

//        final Dialog warningDialog = new Dialog(this);
//        warningDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        warningDialog.setCancelable(true);
//
//        warningDialog.setContentView(R.layout.signature_dialog);
//        warningDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
//        RelativeLayout btnClose = (RelativeLayout) warningDialog.findViewById(R.id.btnNo);
//
//        final SignatureView signatureView = (SignatureView) warningDialog.findViewById(R.id.signature_view);
//        //   Button btnClear = (Button) warningDialog.findViewById(R.id.btnClear);
//        Button btnYes = (Button) warningDialog.findViewById(R.id.btnOk);
//        Button btnClear = (Button) warningDialog.findViewById(R.id.btnClear);
//        btnClose.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                warningDialog.dismiss();
//            }
//        });
//
//        btnClear.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                signatureView.clearCanvas();
//            }
//        });
//
//        btnYes.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                warningDialog.dismiss();
//                //  ErpProgress.showProgressBar(CheckIn.this, "Please wait...");
//                admin_bitmap = signatureView.getSignatureBitmap();
//                approve_paa(admin_bitmap,gatePassBean);
//            }
//
//        });
//
//        warningDialog.show();
        approve_paa(admin_bitmap, gatePassBean,"done");
    }

    @Override
    public void onUNApproveCallback(GatePassBean gatePassBean) {
        approve_paa(admin_bitmap, gatePassBean,"decline");
    }

    private void approve_paa(Bitmap admin_bitmap, GatePassBean gatePassBean,String tag) {
        commonProgress.show();
        HashMap<String, String> params = new HashMap<>();
        params.put("'user_id'", User.getCurrentUser().getId());
        params.put("pass_id", gatePassBean.getId());
        params.put("tag", tag);
        if (admin_bitmap != null) {
            String encoded = Utils.encodeToBase64(admin_bitmap, Bitmap.CompressFormat.JPEG, 90);
            encoded = String.format("data:image/jpeg;base64,%s", encoded);
            params.put("admin_sign", encoded);
        }

        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.APPROVE_STDPASS, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error ) {
                ErpProgress.cancelProgressBar();
                commonProgress.dismiss();
                if (error == null) {
                    if (tag.equalsIgnoreCase("done")){
                        Utils.showSnackBar("Gate-Pass Approved", parent);
                    }else {
                        Utils.showSnackBar("Gate-Pass Decline", parent);
                    }
                    fetchhwlist("all","", limit, "yes");
                } else {
                    Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }, this, params);
    }
}