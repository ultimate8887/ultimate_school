package com.ultimate.ultimatesmartschool.Notice;

import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.DebugLog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class NoticeBean {

    private static String MSG_ID="es_messagesid";
    private static String SUBJECT="subject";
    private static String MESSAGE="message";
    private static String DATE="created_on";
    //private static String TO_TYPE="from_type";
    private static String TO_TYPE="to_type";
    private static String TO_NAME="to_name";
    private static String RE_MSG="re_msg";



    private String es_messagesid;
    private String from_id;
    private String from_type;
    private String to_id;
    private String to_type;
    private String subject;
    private String message;
    private String created_on;
    private String to_name;

    private String  recordingsfile;

    public String getRecordingsfile() {
        return recordingsfile;
    }

    public void setRecordingsfile(String recordingsfile) {
        this.recordingsfile = recordingsfile;
    }
    private ReMsgBean re_msg;

    public String getEs_messagesid() {
        return es_messagesid;
    }

    public void setEs_messagesid(String es_messagesid) {
        this.es_messagesid = es_messagesid;
    }

    public String getFrom_id() {
        return from_id;
    }

    public void setFrom_id(String from_id) {
        this.from_id = from_id;
    }

    public String getFrom_type() {
        return from_type;
    }

    public void setFrom_type(String from_type) {
        this.from_type = from_type;
    }

    public String getTo_id() {
        return to_id;
    }

    public void setTo_id(String to_id) {
        this.to_id = to_id;
    }

    public String getTo_type() {
        return to_type;
    }

    public void setTo_type(String to_type) {
        this.to_type = to_type;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCreated_on() {
        return created_on;
    }

    public void setCreated_on(String created_on) {
        this.created_on = created_on;
    }

    public String getTo_name() {
        return to_name;
    }

    public void setTo_name(String to_name) {
        this.to_name = to_name;
    }

    public ReMsgBean getRe_msg() {
        return re_msg;
    }

    public void setRe_msg(ReMsgBean re_msg) {
        this.re_msg = re_msg;
    }

    public static ArrayList<NoticeBean> parseMessageArray(JSONArray jsonArray) {
        ArrayList<NoticeBean> list = new ArrayList<NoticeBean>();
        try {

            for (int i = 0; i < jsonArray.length(); i++) {
                NoticeBean p = parsemessageObject(jsonArray.getJSONObject(i));
                if (p != null) {
                    list.add(p);
                }
            }
        } catch (Exception e) {
            DebugLog.printLog("exp",e.getMessage());
            e.printStackTrace();
        }
        return list;

    }

    private static NoticeBean parsemessageObject(JSONObject jsonObject) {

        NoticeBean msgObj = new NoticeBean();
        try {


            if (jsonObject.has(SUBJECT) && !jsonObject.getString(SUBJECT).isEmpty() && !jsonObject.getString(SUBJECT).equalsIgnoreCase("null")) {
                msgObj.setSubject(jsonObject.getString(SUBJECT));
            }
            if (jsonObject.has(DATE) && !jsonObject.getString(DATE).isEmpty() && !jsonObject.getString(DATE).equalsIgnoreCase("null")) {
                msgObj.setCreated_on(jsonObject.getString(DATE));
            }
            if (jsonObject.has(MESSAGE) && !jsonObject.getString(MESSAGE).isEmpty() && !jsonObject.getString(MESSAGE).equalsIgnoreCase("null")) {
                msgObj.setMessage(jsonObject.getString(MESSAGE));
            }
            if (jsonObject.has(TO_TYPE) && !jsonObject.getString(TO_TYPE).isEmpty() && !jsonObject.getString(TO_TYPE).equalsIgnoreCase("null")) {
                msgObj.setTo_type(jsonObject.getString(TO_TYPE));
            }

            if (jsonObject.has(TO_NAME) && !jsonObject.getString(TO_NAME).isEmpty() && !jsonObject.getString(TO_NAME).equalsIgnoreCase("null")) {
                msgObj.setTo_name(jsonObject.getString(TO_NAME));
            }
            if (jsonObject.has("recordingsfile") && !jsonObject.getString("recordingsfile").isEmpty() && !jsonObject.getString("recordingsfile").equalsIgnoreCase("null")) {
                msgObj.setRecordingsfile(Constants.getImageBaseURL()+jsonObject.getString("recordingsfile"));
            }
            if (jsonObject.has(RE_MSG)) {
                ReMsgBean reMsgObj = ReMsgBean.parseReMsg(jsonObject.getJSONObject(RE_MSG));
                msgObj.setRe_msg(reMsgObj);
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }
        return msgObj;


    }
    public static class ReMsgBean {


        private String es_messagesid;
        private String from_id;
        private String from_type;
        private String to_id;
        private String to_type;
        private String subject;
        private String message;
        private String created_on;

        public String getEs_messagesid() {
            return es_messagesid;
        }

        public void setEs_messagesid(String es_messagesid) {
            this.es_messagesid = es_messagesid;
        }

        public String getFrom_id() {
            return from_id;
        }

        public void setFrom_id(String from_id) {
            this.from_id = from_id;
        }

        public String getFrom_type() {
            return from_type;
        }

        public void setFrom_type(String from_type) {
            this.from_type = from_type;
        }

        public String getTo_id() {
            return to_id;
        }

        public void setTo_id(String to_id) {
            this.to_id = to_id;
        }

        public String getTo_type() {
            return to_type;
        }

        public void setTo_type(String to_type) {
            this.to_type = to_type;
        }

        public String getSubject() {
            return subject;
        }

        public void setSubject(String subject) {
            this.subject = subject;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public String getCreated_on() {
            return created_on;
        }

        public void setCreated_on(String created_on) {
            this.created_on = created_on;
        }

        private static ReMsgBean parseReMsg(JSONObject jsonObject) {

            ReMsgBean msgObj = new ReMsgBean();
            try {


                if (jsonObject.has(SUBJECT) && !jsonObject.getString(SUBJECT).isEmpty() && !jsonObject.getString(SUBJECT).equalsIgnoreCase("null")) {
                    msgObj.setSubject(jsonObject.getString(SUBJECT));
                }
                if (jsonObject.has(DATE) && !jsonObject.getString(DATE).isEmpty() && !jsonObject.getString(DATE).equalsIgnoreCase("null")) {
                    msgObj.setCreated_on(jsonObject.getString(DATE));
                }
                if (jsonObject.has(MESSAGE) && !jsonObject.getString(MESSAGE).isEmpty() && !jsonObject.getString(MESSAGE).equalsIgnoreCase("null")) {
                    msgObj.setMessage(jsonObject.getString(MESSAGE));
                }

                if (jsonObject.has(MSG_ID) && !jsonObject.getString(MSG_ID).isEmpty() && !jsonObject.getString(MSG_ID).equalsIgnoreCase("null")) {
                    msgObj.setEs_messagesid(jsonObject.getString(MSG_ID));
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return msgObj;


        }
    }
}
