package com.ultimate.ultimatesmartschool.Assignment;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.android.volley.Request;

import com.ultimate.ultimatesmartschool.ClassWork.CWViewPagerAdapter;

import com.ultimate.ultimatesmartschool.Gallery.NewGallery.IMGGridView;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.CommonProgress;
import com.ultimate.ultimatesmartschool.Utility.Constants;


import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.relex.circleindicator.CircleIndicator;


public class CommonAssignFragment extends Fragment implements Reply_AssignmentAdapter.Mycallback {

    Context context;
    String value="";

    private String classid = "";
    @BindView(R.id.parent)
    RelativeLayout parent;
    @BindView(R.id.textNorecord)
    TextView imgNoData;
    private ArrayList<AssignmentBean> assignmentList;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerview;
    private Reply_AssignmentAdapter madapter;
    CommonProgress commonProgress;
    @BindView(R.id.totalRecord)
    TextView totalRecord;
    String  class_name, sub_id, sub_name;
    Animation animation;
    String folder_main = "Assignment";
    private final int LOAD_FILE_RESULTS = 1000;
    String fileBase64 = null;

    AssignmentBean assignmentBean;
    Dialog mBottomSheetDialog1;

    public ViewPager intro_images;
    LinearLayout pager_indicator;
    public CircleIndicator indicator;
    public TextView textView,tap_replied,tap_count;
    ImageView imageView;
    LinearLayout imgDownload;
    int assign_id=0;
    int size=0;
    ArrayList<String> hq_list= new ArrayList<>();
    public CommonAssignFragment(String value, int assign_id, Context context) {
        this.context=context;
        this.value=value;
        this.assign_id=assign_id;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_common_assign_new, container, false);
        ButterKnife.bind(this,view);
        commonProgress=new CommonProgress(getActivity());
        animation = AnimationUtils.loadAnimation(getActivity(), R.anim.btn_blink_animation);
        assignmentList = new ArrayList<>();
        // set animation on recyclerview
        int resId = R.anim.layout_animation_up_to_down;
        final LayoutAnimationController controller =
                AnimationUtils.loadLayoutAnimation(getActivity(), resId);
        recyclerview.setLayoutAnimation(controller);
        //----------------end--------------------
        recyclerview.setLayoutManager(new LinearLayoutManager(getActivity()));
        madapter = new Reply_AssignmentAdapter(getActivity(), assignmentList,this);
        recyclerview.setAdapter(madapter);
        fetchAssignment();
        return view;
    }

    private void fetchAssignment() {
        commonProgress.show();
        HashMap<String, String> params = new HashMap<String, String>();
            params.put("assign_id", String.valueOf(assign_id));

        if (value.equalsIgnoreCase("a")){
            params.put("key_type","active");
        }else if (value.equalsIgnoreCase("e")){
            params.put("key_type","expire");
        }else {
            params.put("key_type","submit");
        }

        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.ASSIGNMENT_URL, assignmentapiCallback, getActivity(), params);

    }

    ApiHandler.ApiCallback assignmentapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            commonProgress.dismiss();
            if (error == null) {
                try {
                    if (assignmentList != null)
                        assignmentList.clear();
                    assignmentList = AssignmentBean.parseAssignmentArray(jsonObject.getJSONArray(Constants.ASSIGNMENT_DATA));
                    madapter.setAssignmentList(assignmentList);
                    //setanimation on adapter...
                    recyclerview.getAdapter().notifyDataSetChanged();
                    recyclerview.scheduleLayoutAnimation();
                    //-----------end------------
                    imgNoData.setVisibility(View.GONE);
                    totalRecord.setText(getString(R.string.t_entries)+" "+String.valueOf(assignmentList.size()));
                    if (assignmentList.size() <= 0) {
                        totalRecord.setText(getString(R.string.t_entries)+" 0");
                        imgNoData.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }  else {
                totalRecord.setText(getString(R.string.t_entries)+" 0");
                assignmentList.clear();
                madapter.setAssignmentList(assignmentList);
                madapter.notifyDataSetChanged();
                imgNoData.setVisibility(View.VISIBLE);
            }
        }
    };

    @Override
    public void viewPdf(AssignmentBean data) {
     openDialog(data);
    }

    private void openDialog(AssignmentBean data) {


    }

    @Override
    public void replyPdf(AssignmentBean assignmentBean1) {
        assignmentBean=assignmentBean1;

    }


    @Override
    public void view_rep_Pdf(AssignmentBean data) {

        final Dialog warningDialog = new Dialog(getActivity());
        warningDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        warningDialog.setCancelable(true);

        warningDialog.setContentView(R.layout.class_img_dialog);
        warningDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        RelativeLayout btnClose = (RelativeLayout) warningDialog.findViewById(R.id.btnNo);

        indicator = (CircleIndicator) warningDialog.findViewById(R.id.indicator);
        textView=(TextView) warningDialog.findViewById(R.id.clsswrktexttt);
        intro_images=(ViewPager) warningDialog.findViewById(R.id.pager_introduction);
        tap_replied=(TextView) warningDialog.findViewById(R.id.tap_replied);
        textView.setText("Assignment_id:- "+data.getId());

        tap_replied.setVisibility(View.VISIBLE);

        String title = getColoredSpanned("Submitted by ", "#5A5C59");
        String l_Name = getColoredSpanned("<b>"+data.getName()+"</b>"+"("+data.getStu_id()+")", "#000000");
        tap_replied.setText(Html.fromHtml(title + " " + l_Name+ " "));

        tap_count=(TextView) warningDialog.findViewById(R.id.tap_count);
        String title1 = getColoredSpanned("<b>"+"1"+"</b>"+"", "#F4212C");
        String l_Name1 = getColoredSpanned(" / "+String.valueOf(data.getRep_image().size()), "#000000");
        tap_count.setText(Html.fromHtml(title1 + " " + l_Name1+ " "));

        CWViewPagerAdapter mAdapter = null;
            mAdapter = new CWViewPagerAdapter(getActivity(), data.getRep_image(), "assignment");
        intro_images.setAdapter(mAdapter);
        intro_images.setCurrentItem(0);
        indicator.setViewPager(intro_images);


        intro_images.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int arg0) {
                // TODO Auto-generated method stub
               // textView.setText((arg0+1)+" of "+ data.getRep_image().size());
                int c=arg0+1;
                String title1 = getColoredSpanned("<b>"+String.valueOf(c)+"</b>"+"", "#F4212C");
                String l_Name1 = getColoredSpanned(" / "+String.valueOf(data.getRep_image().size()), "#000000");
                tap_count.setText(Html.fromHtml(title1 + " " + l_Name1+ " "));
            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
                // TODO Auto-generated method stub

                System.out.println("onPageScrolled");
            }

            @Override
            public void onPageScrollStateChanged(int num) {
                // TODO Auto-generated method stub

            }
        });

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                warningDialog.dismiss();
            }
        });

        imageView=(ImageView) warningDialog.findViewById(R.id.img);

        LinearLayout imgDownload=(LinearLayout) warningDialog.findViewById(R.id.imgDownload);
        imgDownload.setVisibility(View.GONE);

        imgDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageView.startAnimation(animation);
                ArrayList<String> yourArray = new ArrayList<>();
                yourArray=data.getRep_image();
                Intent intent = new Intent(getActivity(), IMGGridView.class);
                intent.putExtra("data", yourArray);
                intent.putExtra("tag", "assignment");
                intent.putExtra("title", data.getName()+"("+data.getStu_id()+")");
                intent.putExtra("sub", "Assignment_id:- "+data.getId());
                startActivity(intent);
            }
        });

        warningDialog.show();
        Window window = warningDialog.getWindow();
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    }

    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }

}