package com.ultimate.ultimatesmartschool.Assignment;

import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;
import com.ultimate.ultimatesmartschool.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainAssignActivity extends AppCompatActivity {


    @BindView(R.id.imgBack)
    ImageView imgBackks;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    private Animation animation;
    @BindView(R.id.txtSub)
    TextView txtSub;
    @BindView(R.id.viewpager)
    ViewPager viewpager;
    @BindView(R.id.tab)
    TabLayout tablayout;
    int id=0;
    String className="",sectionname="";
    AssignmentBean data;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_s_t_d_list_new);
        ButterKnife.bind(this);
        Bundle intent_value = getIntent().getExtras();
        if (intent_value == null) {
            return;
        }
        if (intent_value.containsKey("data")) {
            Gson gson = new Gson();
            data = gson.fromJson(intent_value.getString("data"), AssignmentBean.class);
            className = getIntent().getExtras().getString("className");
            sectionname = getIntent().getExtras().getString("sectionname");
            setData(data);
        } else {
            return;
        }

        animation = AnimationUtils.loadAnimation(MainAssignActivity.this, R.anim.btn_blink_animation);

        setupTabPager();
    }

    private void setData(AssignmentBean data) {
        txtTitle.setText("Replied Student List");
        id= Integer.parseInt(data.getId());
        txtSub.setVisibility(View.VISIBLE);
        txtSub.setText("Assignment_id:- "+data.getId());
    }

    private void setupTabPager() {


        viewpager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tablayout));

        tablayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(viewpager) {

            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                super.onTabSelected(tab);
                String value=String.valueOf(tab.getText());

                if (value.equalsIgnoreCase("Pending")){
                    tablayout.setTabTextColors(

                            // Unselected Tab Text Color
                            ContextCompat.getColor(MainAssignActivity.this, R.color.d_light),
                            // Selected Tab Text Color
                            ContextCompat.getColor(MainAssignActivity.this, R.color.green)
                    );
                    // Selected Tab Indicator Color
                    //   tablayout.setSelectedTabIndicatorColor(getResources().getColor(R.color.leave));
                    //  tablayout.setSelectedTabIndicatorHeight(5);
                }else if (value.equalsIgnoreCase("Submitted")) {
                    tablayout.setTabTextColors(

                            // Unselected Tab Text Color
                            ContextCompat.getColor(MainAssignActivity.this, R.color.d_light),
                            // Selected Tab Text Color
                            ContextCompat.getColor(MainAssignActivity.this, R.color.leave)
                    );
                    // Selected Tab Indicator Color
                    //  tablayout.setSelectedTabIndicatorColor(getResources().getColor(R.color.present));
                    //tablayout.setSelectedTabIndicatorHeight(5);
                }else {
                    tablayout.setTabTextColors(

                            // Unselected Tab Text Color
                            ContextCompat.getColor(MainAssignActivity.this, R.color.d_light),
                            // Selected Tab Text Color
                            ContextCompat.getColor(MainAssignActivity.this, R.color.light_red)
                    );
                    // Selected Tab Indicator Color
                    //  tablayout.setSelectedTabIndicatorColor(getResources().getColor(R.color.absent));
                    //tablayout.setSelectedTabIndicatorHeight(5);
                }

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                super.onTabUnselected(tab);
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                super.onTabReselected(tab);
            }
        });


        FilterTabPagerAssign adapter;
//        if (id.equalsIgnoreCase("inbox")){
//            txtTitle.setText("Inbox");
//            adapter = new FilterTabPagerAssign(getSupportFragmentManager(),1,this);
//        }else {

        adapter = new FilterTabPagerAssign(getSupportFragmentManager(),id,this);
        //   }
        viewpager.setAdapter(adapter);
        tablayout.setupWithViewPager(viewpager);
    }

    @OnClick(R.id.imgBack)
    public void imgBackks()  {
        imgBackks.startAnimation(animation);
        finish();
    }
}