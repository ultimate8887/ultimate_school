package com.ultimate.ultimatesmartschool.Assignment;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.Request;
import com.ultimate.ultimatesmartschool.BeanModule.ClassBean;
import com.ultimate.ultimatesmartschool.BeanModule.SubjectBeancls;
import com.ultimate.ultimatesmartschool.BeanModule.User;
import com.ultimate.ultimatesmartschool.Ebook.WebViewActivity;
import com.ultimate.ultimatesmartschool.Homework.AdapterOfSubjectList;
import com.ultimate.ultimatesmartschool.Login.LoginActivity;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.apptik.widget.multiselectspinner.BaseMultiSelectSpinner;
import io.apptik.widget.multiselectspinner.MultiSelectSpinner;

public class ReplyAssignmentList extends AppCompatActivity implements ReplyAssignmentAdapter.Mycallback{
    ArrayList<ClassBean> classList = new ArrayList<>();
    @BindView(R.id.parent)
    RelativeLayout parent;
    @BindView(R.id.spinnersubject)
    Spinner spinnersubject;
    @BindView(R.id.multiselectSpinnerclass)
    MultiSelectSpinner multiselectSpinnerclass;
    String classid = "";

    private ArrayList<AssignmentBean> assignmentList = new ArrayList<>();
    private LinearLayoutManager layoutManager;

    private int loaded = 0;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    List<String> classidsel;
    @BindView(R.id.textView7)TextView textView7;
    ArrayList<SubjectBeancls> subjectList = new ArrayList<>();
    AdapterOfSubjectList adaptersub;
    String sub_id, sub_name;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerview;
    @BindView(R.id.textNorecord)
    TextView txtNorecord;
    private ArrayList<ReplyAssignmentBean> replyassignmentList=new ArrayList<>();
    private ReplyAssignmentAdapter madapter;
    AdapterOfAssignList  adapterasign;
    @BindView(R.id.spinner3)Spinner spinnerassinment;
    String assignment_id,assignment_name;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reply_assignment_list);
        ButterKnife.bind(this);
        classidsel = new ArrayList<>();
        layoutManager = new LinearLayoutManager(ReplyAssignmentList.this);
        recyclerview.setLayoutManager(layoutManager);
        madapter = new ReplyAssignmentAdapter(this, replyassignmentList, this);
        recyclerview.setAdapter(madapter);

        fetchClass();
        txtTitle.setText("Reply Assignment");
    }

    @OnClick(R.id.imgBack)
    public void onback() {
        finish();
    }
    private void fetchClass() {
        if (loaded == 0) {
            ErpProgress.showProgressBar(this, "Please wait...");
        }
        loaded++;

        HashMap<String, String> params = new HashMap<String, String>();
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.CLASSLIST_URL, classapiCallback, this, params);
    }

    ApiHandler.ApiCallback classapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            if (error == null) {
                try {
                    if (classList != null) {
                        classList.clear();
                    }
                    classList = ClassBean.parseClassArray(jsonObject.getJSONArray(Constants.CLASSDATA));
                    List<String> dstring = new ArrayList<>();
                    for (ClassBean cobj : classList) {
                        dstring.add(cobj.getName());
                        Log.e("getclassid",cobj.getId());
                    }

                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(ReplyAssignmentList.this, android.R.layout.simple_list_item_multiple_choice, dstring);

                    multiselectSpinnerclass.setListAdapter(adapter).setListener(new BaseMultiSelectSpinner.MultiSpinnerListener() {
                        @Override
                        public void onItemsSelected(boolean[] selected) {

                            Log.e("dcfcds", "fdsf");
                            String value = multiselectSpinnerclass.getSpinnerText();
                            List<String> clas = new ArrayList<>();
                            classidsel.clear();
                            if (!value.isEmpty()) {
                                clas = Arrays.asList(value.split("\\s*,\\s*"));
                            }
                            for (String dval : clas) {
                                for (ClassBean obj : classList) {
                                    if (obj.getName().equalsIgnoreCase(dval)) {
                                        classidsel.add(obj.getId());
                                        //  textView7.setVisibility(View.GONE);

                                        break;
                                    }
                                }
                            }

                            fetchSubject();
                            Log.e("dcfcds", value + "  ,  classid:  " + classidsel);
                            // }
                        }
                    }).setSelectAll(false)
                            .setSpinnerItemLayout(androidx.appcompat.R.layout.support_simple_spinner_dropdown_item)
                            .setTitle("Select Class")

                            .setMaxSelectedItems(1)
                    ;


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            } else {
                Utils.showSnackBar(error.getMessage(), parent);
                if (error.getStatusCode() == 405) {
                    User.logout();
                    startActivity(new Intent(ReplyAssignmentList.this, LoginActivity.class));
                    finish();
                }
            }
        }
    };

    private void fetchSubject() {
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("id", String.valueOf(classidsel));
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.SubjectList, subapiCallback, this, params);

    }

    ApiHandler.ApiCallback subapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            /* */

            if (error == null) {
                try {

                    subjectList =   SubjectBeancls.parseClassArray(jsonObject.getJSONArray("sub_data"));

                    adaptersub = new AdapterOfSubjectList(ReplyAssignmentList.this, subjectList);
                    spinnersubject.setAdapter(adaptersub);
                    spinnersubject.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if (i != 0) {
                                sub_name = subjectList.get(i - 1).getName();
                                sub_id = subjectList.get(i - 1).getId();
                                fetchAssignment(sub_id);
                            }

                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    };
    private void fetchAssignment(String sub_id) {
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("class_id", String.valueOf(classidsel));
        params.put("sub_id", sub_id);
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.ASSIGNMENT_URL, assignmentapiCallback, this, params);

    }
    ApiHandler.ApiCallback assignmentapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            /* */

            if (error == null) {
                try {

                    assignmentList =   AssignmentBean.parseAssignmentArray(jsonObject.getJSONArray(Constants.ASSIGNMENT_DATA));

                    adapterasign = new AdapterOfAssignList(ReplyAssignmentList.this, assignmentList);
                    spinnerassinment.setAdapter(adapterasign);
                    spinnerassinment.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if (i != 0) {
                                assignment_name = assignmentList.get(i - 1).getComment();
                                assignment_id = assignmentList.get(i - 1).getId();
                                fetchreplyAssignment( String.valueOf(classidsel), sub_id,assignment_id);

                            }

                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    };


    private void fetchreplyAssignment(String class_id,String sub_id,String assignment_id) {
        ErpProgress.showProgressBar(this, "Please wait...");
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("class_id", String.valueOf(classidsel));
        params.put("sub_id", sub_id);
        params.put("assign_id", assignment_id);
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.REPLYASSIGNMENT_URL, assignmentapiCallback2, this, params);

    }

    ApiHandler.ApiCallback assignmentapiCallback2 = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();

            if (error == null) {
                try {
                    if (replyassignmentList != null)
                        replyassignmentList.clear();
                    replyassignmentList = ReplyAssignmentBean.parseAssignmentArray(jsonObject.getJSONArray(Constants.ASSIGNMENT_DATA));
                    madapter.setAssignmentList(replyassignmentList);
                    madapter.notifyDataSetChanged();
                    txtNorecord.setVisibility(View.GONE);
                    if (assignmentList.size() <= 0) {
                        txtNorecord.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                replyassignmentList.clear();
                madapter.setAssignmentList(replyassignmentList);
                madapter.notifyDataSetChanged();
                txtNorecord.setVisibility(View.VISIBLE);
            }
        }
    };

    @Override
    public void viewPdf(ReplyAssignmentBean assignmentBean) {
        Intent intent = new Intent(ReplyAssignmentList.this, WebViewActivity.class);
        intent.putExtra("MY_kEY",assignmentBean.getRep_file());
        startActivity(intent);
    }
}
