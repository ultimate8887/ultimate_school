package com.ultimate.ultimatesmartschool.Assignment;

import com.ultimate.ultimatesmartschool.Utility.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class AssignmentBean {

    private static String SID = "stu_id";
    private static String NAME = "name";
    private static String F_NAME = "father_name";
    private static String PROFILE = "profile";

    private String stu_id;
    private String name;

    public String getStu_id() {
        return stu_id;
    }

    public void setStu_id(String stu_id) {
        this.stu_id = stu_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFather_name() {
        return father_name;
    }

    public void setFather_name(String father_name) {
        this.father_name = father_name;
    }

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    private String father_name;
    private String profile;
    private String gender;


    private static String ID = "id";
    private static String COMMENT = "comment";
    private static String SUB_ID = "subject_id";
    private static String LAST_DATE = "last_date";
    private static String IMAGE = "file";
    private static String CREATED_ON = "created_on";
    private static String MARK = "mark";
    private static String SUB_NAME = "subject_name";

    private static String AIMAGE="image";
    private static String RIMAGE="rep_image";



    private ArrayList<String> image;
    private ArrayList<String> rep_image;
    private String image_tag;

    public ArrayList<String> getImage() {
        return image;
    }

    public void setImage(ArrayList<String> image) {
        this.image = image;
    }

    public ArrayList<String> getRep_image() {
        return rep_image;
    }

    public void setRep_image(ArrayList<String> rep_image) {
        this.rep_image = rep_image;
    }

    public String getImage_tag() {
        return image_tag;
    }

    public void setImage_tag(String image_tag) {
        this.image_tag = image_tag;
    }

    public String getRep_image_tag() {
        return rep_image_tag;
    }

    public void setRep_image_tag(String rep_image_tag) {
        this.rep_image_tag = rep_image_tag;
    }

    private String rep_image_tag;


    /**
     * id : 1
     * comment : sfsdgdfsghsdggfgdf
     * subject_id : 1
     * last_date : 2018-03-30
     * image : appinfo.png
     * created_on : 2018-03-28
     * mark : 25
     */

    private String id;
    private String comment;
    private String subject_id;
    private String last_date;
    private String file;

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    private String created_on;

    public String getEs_lasttiming() {
        return es_lasttiming;
    }

    public void setEs_lasttiming(String es_lasttiming) {
        this.es_lasttiming = es_lasttiming;
    }

    private String es_lasttiming;


    private String mark;
    /**
     * subject_name : ENGLISH
     */

    private String subject_name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getSubject_id() {
        return subject_id;
    }

    public void setSubject_id(String subject_id) {
        this.subject_id = subject_id;
    }

    public String getLast_date() {
        return last_date;
    }

    public void setLast_date(String last_date) {
        this.last_date = last_date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    private String status;

    public String getAs_submit_date() {
        return as_submit_date;
    }

    public void setAs_submit_date(String as_submit_date) {
        this.as_submit_date = as_submit_date;
    }

    private String as_submit_date;

    public String getCreated_on() {
        return created_on;
    }

    public void setCreated_on(String created_on) {
        this.created_on = created_on;
    }

    public String getMark() {
        return mark;
    }

    public void setMark(String mark) {
        this.mark = mark;
    }

    public static ArrayList<AssignmentBean> parseAssignmentArray(JSONArray arrayObj) {
        ArrayList list = new ArrayList<AssignmentBean>();
        try {
            for (int i = 0; i < arrayObj.length(); i++) {
                AssignmentBean p = parseAssignmentObject(arrayObj.getJSONObject(i));
                if (p != null) {
                    list.add(p);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public static AssignmentBean parseAssignmentObject(JSONObject jsonObject) {
        AssignmentBean casteObj = new AssignmentBean();
        try {
            if (jsonObject.has(ID)) {
                casteObj.setId(jsonObject.getString(ID));
            }
            if (jsonObject.has(COMMENT) && !jsonObject.getString(COMMENT).isEmpty() && !jsonObject.getString(COMMENT).equalsIgnoreCase("null")) {
                casteObj.setComment(jsonObject.getString(COMMENT));
            }
            if (jsonObject.has(SUB_ID) && !jsonObject.getString(SUB_ID).isEmpty() && !jsonObject.getString(SUB_ID).equalsIgnoreCase("null")) {
                casteObj.setSubject_id(jsonObject.getString(SUB_ID));
            }
            if (jsonObject.has(LAST_DATE) && !jsonObject.getString(LAST_DATE).isEmpty() && !jsonObject.getString(LAST_DATE).equalsIgnoreCase("null")) {
                casteObj.setLast_date(jsonObject.getString(LAST_DATE));
            }
            if (jsonObject.has(IMAGE) && !jsonObject.getString(IMAGE).isEmpty() && !jsonObject.getString(IMAGE).equalsIgnoreCase("null")) {
                casteObj.setFile(Constants.getImageBaseURL() +jsonObject.getString(IMAGE));
            }
            if (jsonObject.has(CREATED_ON) && !jsonObject.getString(CREATED_ON).isEmpty() && !jsonObject.getString(CREATED_ON).equalsIgnoreCase("null")) {
                casteObj.setCreated_on(jsonObject.getString(CREATED_ON));
            }
            if (jsonObject.has(MARK) && !jsonObject.getString(MARK).isEmpty() && !jsonObject.getString(MARK).equalsIgnoreCase("null")) {
                casteObj.setMark(jsonObject.getString(MARK));
            }
            if (jsonObject.has(SUB_NAME) && !jsonObject.getString(SUB_NAME).isEmpty() && !jsonObject.getString(SUB_NAME).equalsIgnoreCase("null")) {
                casteObj.setSubject_name(jsonObject.getString(SUB_NAME));
            }

            if (jsonObject.has("es_lasttiming") && !jsonObject.getString("es_lasttiming").isEmpty() && !jsonObject.getString("es_lasttiming").equalsIgnoreCase("null")) {
                casteObj.setEs_lasttiming(jsonObject.getString("es_lasttiming"));
            }

            if (jsonObject.has(NAME) && !jsonObject.getString(NAME).isEmpty() && !jsonObject.getString(NAME).equalsIgnoreCase("null")) {
                casteObj.setName(jsonObject.getString(NAME));
            }
            if (jsonObject.has(F_NAME) && !jsonObject.getString(F_NAME).isEmpty() && !jsonObject.getString(F_NAME).equalsIgnoreCase("null")) {
                casteObj.setFather_name(jsonObject.getString(F_NAME));
            }

            if (jsonObject.has(SID) && !jsonObject.getString(SID).isEmpty() && !jsonObject.getString(SID).equalsIgnoreCase("null")) {
                casteObj.setStu_id(jsonObject.getString(SID));
            }

            if (jsonObject.has(PROFILE) && !jsonObject.getString(PROFILE).isEmpty() && !jsonObject.getString(PROFILE).equalsIgnoreCase("null")) {
                casteObj.setProfile(Constants.getImageBaseURL()+jsonObject.getString(PROFILE));
            }

            if (jsonObject.has("gender") && !jsonObject.getString("gender").isEmpty() && !jsonObject.getString("gender").equalsIgnoreCase("null")) {
                casteObj.setGender(jsonObject.getString("gender"));
            }

            if (jsonObject.has("as_submit_date") && !jsonObject.getString("as_submit_date").isEmpty() && !jsonObject.getString("as_submit_date").equalsIgnoreCase("null")) {
                casteObj.setAs_submit_date(jsonObject.getString("as_submit_date"));
            }

            if (jsonObject.has(AIMAGE) && !jsonObject.getString(AIMAGE).isEmpty() && !jsonObject.getString(AIMAGE).equalsIgnoreCase("null")) {
                JSONArray img_arr = jsonObject.getJSONArray(AIMAGE);
                ArrayList<String> img_arrs = new ArrayList<>();
                for(int i=0;i<img_arr.length();i++){
                    img_arrs.add(Constants.getImageBaseURL()+img_arr.getString(i));
                }
                casteObj.setImage(img_arrs);
            }

            if (jsonObject.has(RIMAGE) && !jsonObject.getString(RIMAGE).isEmpty() && !jsonObject.getString(RIMAGE).equalsIgnoreCase("null")) {
                JSONArray img_arr = jsonObject.getJSONArray(RIMAGE);
                ArrayList<String> img_arrs = new ArrayList<>();
                for(int i=0;i<img_arr.length();i++){
                    img_arrs.add(Constants.getImageBaseURL()+img_arr.getString(i));
                }
                casteObj.setRep_image(img_arrs);
            }
            if (jsonObject.has("status") && !jsonObject.getString("status").isEmpty() && !jsonObject.getString("status").equalsIgnoreCase("null")) {
                casteObj.setStatus(jsonObject.getString("status"));
            }

            if (jsonObject.has("image_tag") && !jsonObject.getString("image_tag").isEmpty() && !jsonObject.getString("image_tag").equalsIgnoreCase("null")) {
                casteObj.setImage_tag(jsonObject.getString("image_tag"));
            }
            if (jsonObject.has("rep_image_tag") && !jsonObject.getString("rep_image_tag").isEmpty() && !jsonObject.getString("rep_image_tag").equalsIgnoreCase("null")) {
                casteObj.setRep_image_tag(jsonObject.getString("rep_image_tag"));
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }
        return casteObj;
    }

    public String getSubject_name() {
        return subject_name;
    }

    public void setSubject_name(String subject_name) {
        this.subject_name = subject_name;
    }
}
