package com.ultimate.ultimatesmartschool.Assignment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Utility.Utils;


import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Reply_AssignmentAdapter extends RecyclerView.Adapter<Reply_AssignmentAdapter.MyViewHolder> {

    private Context mContext;
    private ArrayList<AssignmentBean> assignmentList;
    Mycallback mAdaptercall;
    Animation animation;
    Reply_AssignmentAdapter(Context mContext, ArrayList<AssignmentBean> assignmentList, Mycallback mAdaptercall) {
        this.mContext = mContext;
        this.assignmentList = assignmentList;
        this.mAdaptercall = mAdaptercall;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.assignment_lyt_newww, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        AssignmentBean data = assignmentList.get(position);


        animation = AnimationUtils.loadAnimation(mContext, R.anim.btn_blink_animation);

        if (data.getName() != null) {
            String title = getColoredSpanned("", "#5A5C59");
            String Name="";
            Name = getColoredSpanned(data.getName(), "#000000");
            holder.classess.setText(Html.fromHtml(title + " " + Name));
        }else {
            holder.classess.setVisibility(View.GONE);
        }

        if (data.getFather_name() != null) {
            String title="";
            if (data.getGender().equalsIgnoreCase("male")){
                title = getColoredSpanned("S/O ", "#5A5C59");
            }else{
                title = getColoredSpanned("D/O ", "#5A5C59");
            }
            String Name = getColoredSpanned(data.getFather_name(), "#000000");
            holder.sub.setText(Html.fromHtml(title + " " + Name));

        }else {
            holder.sub.setVisibility(View.GONE);
        }

        if (data.getStu_id() != null) {
            String title = getColoredSpanned("Std_id:- ", "#000000");
            String Name = getColoredSpanned(""+data.getStu_id(), "#5A5C59");
            holder.id.setText(Html.fromHtml(title + " " + Name));
        }

        holder.homeTopic.setText(data.getComment());

        //for Today
        Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
        Date setdate = calendar.getTime();
        SimpleDateFormat fmtOut = new SimpleDateFormat("dd MMM, yyyy");
        String dateString = fmtOut.format(setdate);
        //for Yesterday
        DateFormat dateFormat = new SimpleDateFormat("dd MMM, yyyy");
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);
        String dateString1= dateFormat.format(cal.getTime());
        //for Tomorrow
        DateFormat dateFormat1 = new SimpleDateFormat("dd MMM, yyyy");
        Calendar cal1 = Calendar.getInstance();
        cal1.add(Calendar.DATE, +1);
        String dateString2= dateFormat1.format(cal1.getTime());

        String check= Utils.getDateFormated(data.getAs_submit_date());
        if (check.equalsIgnoreCase(dateString)){
            String title = getColoredSpanned("Submitted on ", "#000000");
            String l_Name = getColoredSpanned("<b>"+"Today"+"</b>", "#e31e25");
            holder.u_date.setText(Html.fromHtml(title + " " + l_Name));
        }else if (check.equalsIgnoreCase(dateString1)){
            String title = getColoredSpanned("Submitted on ", "#000000");
            String l_Name = getColoredSpanned("<b>"+"Yesterday"+"</b>", "#1C8B3B");
            holder.u_date.setText(Html.fromHtml(title + " " + l_Name));
        }else {
            if(data.getStatus().equalsIgnoreCase("active")){
                String title = getColoredSpanned("Status:- ", "#000000");
                String l_Name = getColoredSpanned("<b>"+"Pending"+"</b>", "#1C8B3B");
                holder.u_date.setText(Html.fromHtml(title + " " + l_Name));
            } else if(data.getStatus().equalsIgnoreCase("expire")) {
                String title = getColoredSpanned("Status:- ", "#000000");
                String l_Name = getColoredSpanned("<b>"+"Expire"+"</b>", "#e31e25");
                holder.u_date.setText(Html.fromHtml(title + " " + l_Name));
            }else {
                String title = getColoredSpanned("Submitted on ", "#000000");
                String Name = getColoredSpanned("<b>"+Utils.getDateTimeFormatedWithAMPM(data.getAs_submit_date())+"</b>", "#5A5C59");
                holder.u_date.setText(Html.fromHtml(title + " " + Name));
            }

        }

        String check1= Utils.getDateFormated(data.getLast_date());

        if (check1.equalsIgnoreCase(dateString)){
            String title = getColoredSpanned("Last Date: ", "#000000");
            String l_Name = getColoredSpanned("Today", "#e31e25");
            holder.homeTopic.setText(Html.fromHtml(title + " " + l_Name));
        }else if (check1.equalsIgnoreCase(dateString1)){

            String title = getColoredSpanned("Last Date: ", "#000000");
            String l_Name = getColoredSpanned("Yesterday", "#1C8B3B");
            holder.homeTopic.setText(Html.fromHtml(title + " " + l_Name));
        }else if (check1.equalsIgnoreCase(dateString2)){

            String title = getColoredSpanned("Last Date: ", "#000000");
            String l_Name = getColoredSpanned("Tomorrow", "#DE5C9D");
            holder.homeTopic.setText(Html.fromHtml(title + " " + l_Name));

        }else {
            // holder.sub.setText(Utils.getDateFormated(data.getCreated_on()));
            String title = getColoredSpanned("Last Date: ", "#000000");
            String l_Name = getColoredSpanned(Utils.getDateFormated(data.getLast_date()), "#cc7722");
            holder.homeTopic.setText(Html.fromHtml(title + " " + l_Name));
        }

        holder.nofile.setVisibility(View.VISIBLE);
        holder.audio_file.setVisibility(View.GONE);
        holder.pdf_file.setVisibility(View.GONE);

        if (data.getProfile()!=null){
            holder.empty_file.setVisibility(View.GONE);
            holder.image_file.setVisibility(View.VISIBLE);
            Picasso.get().load(data.getProfile()).placeholder(mContext.getResources().getDrawable(R.color.transparent)).into(holder.image_file);
            holder.image_file.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    holder.image_file.startAnimation(animation);
                    if (mAdaptercall != null) {
                        mAdaptercall.viewPdf(assignmentList.get(position));
                    }
                }
            });
        }else{
            //   Picasso.with(listner).load(hq_list.get(position).getImage().get(0)).placeholder(listner.getResources().getDrawable(R.drawable.classwork)).into(holder.image_file);
            Picasso.get().load(R.drawable.stud).into(holder.image_file);

        }

        if(data.getStatus().equalsIgnoreCase("submit")){
            holder.tap_view_pdf.setVisibility(View.VISIBLE);
            holder.tap_view_pdf.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    holder.tap_view_pdf.startAnimation(animation);
                    mAdaptercall.view_rep_Pdf(assignmentList.get(position));
                }
            });
        }else {
            holder.tap_view_pdf.setVisibility(View.GONE);
        }

    }

    public interface Mycallback {

        public void viewPdf(AssignmentBean assignmentBean);
        public void replyPdf(AssignmentBean assignmentBean);
        public void view_rep_Pdf(AssignmentBean assignmentBean);

    }

    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }

    @Override
    public int getItemCount() {
        return (assignmentList.size());
    }

    public void setAssignmentList(ArrayList<AssignmentBean> assignmentList) {
        this.assignmentList = assignmentList;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tap_view_pdf)
        TextView tap_view_pdf;
        public TextView u_date,homeTopic,sub;
        public RelativeLayout withfile;
        @BindView(R.id.nofile)
        RelativeLayout nofile;
        @BindView(R.id.empty_file)
        ImageView empty_file;
        @BindView(R.id.classess)
        TextView classess;
        @BindView(R.id.id)
        TextView id;
        public ImageView image_file, audio_file, pdf_file;
        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            sub = (TextView) itemView.findViewById(R.id.subject);
            u_date = (TextView) itemView.findViewById(R.id.u_date);
            homeTopic = (TextView) itemView.findViewById(R.id.homeTopic);
            withfile = (RelativeLayout) itemView.findViewById(R.id.head);
            image_file = (ImageView) itemView.findViewById(R.id.image_file);
            pdf_file = (ImageView) itemView.findViewById(R.id.pdf_file);
            audio_file = (ImageView) itemView.findViewById(R.id.audio_file);
        }
    }


}
