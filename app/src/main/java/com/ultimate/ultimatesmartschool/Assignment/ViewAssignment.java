package com.ultimate.ultimatesmartschool.Assignment;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetSequence;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.gson.Gson;
import com.ultimate.ultimatesmartschool.BeanModule.ClassBean;
import com.ultimate.ultimatesmartschool.BeanModule.SectionBean;
import com.ultimate.ultimatesmartschool.BeanModule.SubjectBeancls;
import com.ultimate.ultimatesmartschool.BeanModule.User;
import com.ultimate.ultimatesmartschool.ClassTest.AllAdapterOfSubjectList;
import com.ultimate.ultimatesmartschool.ClassWork.CWViewPagerAdapter;
import com.ultimate.ultimatesmartschool.Ebook.WebViewActivity;
import com.ultimate.ultimatesmartschool.Homework.SectionnewAdapter;
import com.ultimate.ultimatesmartschool.Login.LoginActivity;

import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.StudentAttendance.ClassAdapterAttend;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.CommonProgress;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.relex.circleindicator.CircleIndicator;

public class ViewAssignment extends AppCompatActivity implements AssignmentAdapter.Mycallback{
    ArrayList<ClassBean> classList = new ArrayList<>();
    @BindView(R.id.parent)
    RelativeLayout parent;
    @BindView(R.id.spinner)
    Spinner spinnersubject;
    Spinner spinnersection;
    Spinner spinnerClass;
    String classid = "";
    private AssignmentAdapter madapter;
    private ArrayList<AssignmentBean> assignmentList = new ArrayList<>();
    private LinearLayoutManager layoutManager;
    Dialog mBottomSheetDialog;
    private int loaded = 0;
    @BindView(R.id.txtTitle)
    TextView txtTitle;

    @BindView(R.id.root)
    RelativeLayout root;

    List<String> classidsel;
    ArrayList<SubjectBeancls> subjectList = new ArrayList<>();
    AllAdapterOfSubjectList adaptersub;
    String sub_id, sub_name;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerview;
    @BindView(R.id.textNorecord)
    TextView txtNorecord;
    @BindView(R.id.totalRecord)
    TextView totalRecord;
    SharedPreferences sharedPreferences;
    CommonProgress commonProgress;
    @BindView(R.id.dialog)
    ImageView dialog;
    int check=0;
    int size=0;
    String sectionid="",className = "";
    String sectionname;
    ArrayList<SectionBean> sectionList = new ArrayList<>();
    public ViewPager intro_images;
    LinearLayout pager_indicator;
    public CircleIndicator indicator;
    public TextView textView,tap_count;
    @BindView(R.id.txtSub)
    TextView txtSub;
    @BindView(R.id.cal_img)
    ImageView cal_img;

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_assignment);
        ButterKnife.bind(this);
        commonProgress=new CommonProgress(this);
        classidsel = new ArrayList<>();
        layoutManager = new LinearLayoutManager(ViewAssignment.this);
        recyclerview.setLayoutManager(layoutManager);
        madapter = new AssignmentAdapter(this, assignmentList, this);
        recyclerview.setAdapter(madapter);

        if (!restorePrefData()){
            setShowcaseView();
        }else {
            openDialog();
        }
        txtTitle.setText("Assignment Report");
    }

    private void setShowcaseView() {
        new TapTargetSequence(this)
                .targets(TapTarget.forView(dialog,"Search Button!","Tap the Search button to Search Class-Wise Assignment.")
                        .outerCircleColor(R.color.light)
                        .outerCircleAlpha(0.96f)
                        .targetCircleColor(R.color.white)
                        .titleTextSize(20)
                        .titleTextColor(R.color.white)
                        .descriptionTextSize(14)
                        .descriptionTextColor(R.color.black)
                        .textColor(R.color.black)
                        .textTypeface(Typeface.SANS_SERIF)
                        .dimColor(R.color.black)
                        .drawShadow(true)
                        .cancelable(false)
                        .tintTarget(true)
                        .transparentTarget(true)
                        .targetRadius(60)).listener(new TapTargetSequence.Listener() {
                    @RequiresApi(api = Build.VERSION_CODES.N)
                    @Override
                    public void onSequenceFinish() {
                        // Toast.makeText(getApplicationContext(),"Sequence Finished",Toast.LENGTH_SHORT).show();
                        savePrefData();
                        openDialog();
                    }

                    @Override
                    public void onSequenceStep(TapTarget lastTarget, boolean targetClicked) {
                        // Toast.makeText(getActivity(),"GREAT!",Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onSequenceCanceled(TapTarget lastTarget) {

                    }
                }).start();
    }

    private void savePrefData(){
        sharedPreferences=this.getSharedPreferences("boarding_pref_add_attend_search",MODE_PRIVATE);
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putBoolean("IsFirstTimeVisit_add_attend_search",true);
        editor.apply();
    }

    private boolean restorePrefData(){
        sharedPreferences = this.getSharedPreferences("boarding_pref_add_attend_search",MODE_PRIVATE);
        return sharedPreferences.getBoolean("IsFirstTimeVisit_add_attend_search",false);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void openDialog() {

        mBottomSheetDialog = new BottomSheetDialog(this);
        final View sheetView = getLayoutInflater().inflate(R.layout.search_dialog, null);
        mBottomSheetDialog.setContentView(sheetView);
        mBottomSheetDialog.setCancelable(false);

        spinnerClass=(Spinner) sheetView.findViewById(R.id.spinnerClass);
        spinnersection=(Spinner) sheetView.findViewById(R.id.spinnersection);
        ImageView close=(ImageView) sheetView.findViewById(R.id.close);
        TextView txtSetup=(TextView) sheetView.findViewById(R.id.txtSetup);
        txtSetup.setText("View Assignment");
        //txtTitle.setText("Mark Attendance");
        fetchClass();
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //assignData();
                if (check==0) {
                    finish();
                    mBottomSheetDialog.dismiss();
                }else {
                    mBottomSheetDialog.dismiss();
                }
            }
        });


        Button btnYes= (Button) sheetView.findViewById(R.id.btnYes);


        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (classid.equalsIgnoreCase(""))  {
                    Toast.makeText(ViewAssignment.this,"Kindly Select Class!", Toast.LENGTH_SHORT).show();
                }  else if (sectionid.equalsIgnoreCase("")){
                    Toast.makeText(ViewAssignment.this,"Kindly Select Class Section!", Toast.LENGTH_SHORT).show();
                }else {
                    check++;
                    fetchSubject();
                    setCommonData();
                }

            }
        });
        mBottomSheetDialog.show();

    }
    private void setCommonData() {
        txtSub.setVisibility(View.VISIBLE);
        txtSub.setText(className+"("+sectionname+")");
        root.setVisibility(View.VISIBLE);
        txtNorecord.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.imgBack)
    public void onback() {
        finish();
    }


    private void fetchClass() {
        if (loaded == 0) {
            commonProgress.show();
        }
        loaded++;

        HashMap<String, String> params = new HashMap<String, String>();
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.CLASSLIST_URL, classapiCallback,getApplicationContext(), params);
    }


    ApiHandler.ApiCallback classapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            commonProgress.dismiss();
            if (error == null) {
                try {
                    classList = ClassBean.parseClassArray(jsonObject.getJSONArray(Constants.CLASSDATA));
                    ClassAdapterAttend adapter = new ClassAdapterAttend(getApplicationContext(), classList,0);
                    spinnerClass.setAdapter(adapter);
                    spinnerClass.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if (i > 0) {
                                sub_id= "";
                                sub_name= "" ;
                                sectionid="";
                                sectionname="";
                                classid = "";
                                className = "";
                                classid = classList.get(i - 1).getId();
                                className= classList.get(i - 1).getName();
                                fetchSection();
                            }

                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_LONG).show();
                // Utils.showSnackBar(error.getMessage(), view.findViewById(R.id.parent));
            }
        }
    };

    private void fetchSection() {
        HashMap<String, String> params = new HashMap<String, String>();
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.SECTIONLIST_URL, sectionapiCallback, this, params);
    }

    ApiHandler.ApiCallback sectionapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            if (error == null) {
                try {
                    sectionList = SectionBean.parseCommonArray(jsonObject.getJSONArray("hostelroom_data"));
                    SectionnewAdapter adapter = new SectionnewAdapter(ViewAssignment.this, sectionList);
                    spinnersection.setAdapter(adapter);
                    spinnersection.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if (i > 0) {
                                sectionid = sectionList.get(i - 1).getSection_id();
                                sectionname = sectionList.get(i - 1).getSection_name();

                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Utils.showSnackBar(error.getMessage(), parent);
                if (error.getStatusCode() == 405) {
                    User.logout();
                    startActivity(new Intent(ViewAssignment.this, LoginActivity.class));
                    finish();
                }
            }
        }
    };


    private void fetchSubject() {
        commonProgress.show();
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("id", classid);
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.SubjectList, subapiCallback, this, params);

    }

    ApiHandler.ApiCallback subapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            /* */
            commonProgress.dismiss();
            mBottomSheetDialog.dismiss();
            if (error == null) {
                try {

                    subjectList =   SubjectBeancls.parseClassArray(jsonObject.getJSONArray("sub_data"));

                    adaptersub = new AllAdapterOfSubjectList(ViewAssignment.this, subjectList);
                    spinnersubject.setAdapter(adaptersub);
                    spinnersubject.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                            if (i != 0) {
                                sub_name = subjectList.get(i - 1).getName();
                                sub_id = subjectList.get(i - 1).getId();
                            }else {
                                if (assignmentList != null)
                                    txtNorecord.setText("Kindly Select subject!");
                                totalRecord.setText("Total Entries:- 0");
                                assignmentList.clear();
                                madapter.setAssignmentList(assignmentList);
                                madapter.notifyDataSetChanged();
                                txtNorecord.setVisibility(View.VISIBLE);
                            }
                            fetchAssignment(i);
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    };

    @RequiresApi(api = Build.VERSION_CODES.N)
    @OnClick(R.id.dialog)
    public void dialogggg() {
        openDialog();
    }

    private void fetchAssignment(int i) {
        commonProgress.show();
        HashMap<String, String> params = new HashMap<String, String>();
        if (i == 0){
            params.put("class_id", classid);
            params.put("sub_id", "");
            params.put("sec_id", sectionid);
        } else {
            params.put("class_id", classid);
            params.put("sub_id", sub_id);
            params.put("sec_id", sectionid);
        }
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.ASSIGNMENT_URL, assignmentapiCallback, this, params);
    }

    ApiHandler.ApiCallback assignmentapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            commonProgress.dismiss();
            if (error == null) {
                try {
                    if (assignmentList != null)
                        assignmentList.clear();
                    assignmentList = AssignmentBean.parseAssignmentArray(jsonObject.getJSONArray(Constants.ASSIGNMENT_DATA));
                    madapter.setAssignmentList(assignmentList);
                    madapter.notifyDataSetChanged();
                    txtNorecord.setVisibility(View.GONE);
                    totalRecord.setText("Total Entries:- "+String.valueOf(assignmentList.size()));
                    if (assignmentList.size() <= 0) {
                        txtNorecord.setText("No Record Found!");
                        totalRecord.setText("Total Entries:- 0");
                        txtNorecord.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                txtNorecord.setText("No Record Found!");
                totalRecord.setText("Total Entries:- 0");
                assignmentList.clear();
                madapter.setAssignmentList(assignmentList);
                madapter.notifyDataSetChanged();
                txtNorecord.setVisibility(View.VISIBLE);
            }
        }
    };



    @Override
    public void viewPdf(AssignmentBean data) {
//        Intent intent = new Intent(ViewAssignment.this, WebViewActivity.class);
//        intent.putExtra("MY_kEY",assignmentBean.getFile());
//        startActivity(intent);

        final Dialog warningDialog = new Dialog(this);
        warningDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        warningDialog.setCancelable(true);

        warningDialog.setContentView(R.layout.class_img_dialog);
        warningDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        RelativeLayout btnClose = (RelativeLayout) warningDialog.findViewById(R.id.btnNo);

        indicator = (CircleIndicator) warningDialog.findViewById(R.id.indicator);
        textView=(TextView) warningDialog.findViewById(R.id.clsswrktexttt);
        intro_images=(ViewPager) warningDialog.findViewById(R.id.pager_introduction);

        tap_count=(TextView) warningDialog.findViewById(R.id.tap_count);
        String title1 = getColoredSpanned("<b>"+"1"+"</b>"+"", "#F4212C");
        String l_Name1 = getColoredSpanned(" / "+String.valueOf(data.getImage().size()), "#000000");
        tap_count.setText(Html.fromHtml(title1 + " " + l_Name1+ " "));

        textView.setText(data.getComment());
        CWViewPagerAdapter mAdapter = new  CWViewPagerAdapter(this,data.getImage(),"assignment");
        intro_images.setAdapter(mAdapter);
        intro_images.setCurrentItem(0);
        indicator.setViewPager(intro_images);

        intro_images.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int arg0) {
                // TODO Auto-generated method stub
                int c=arg0+1;
                String title1 = getColoredSpanned("<b>"+String.valueOf(c)+"</b>"+"", "#F4212C");
                String l_Name1 = getColoredSpanned(" / "+String.valueOf(data.getImage().size()), "#000000");
                tap_count.setText(Html.fromHtml(title1 + " " + l_Name1+ " "));
            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
                // TODO Auto-generated method stub

                System.out.println("onPageScrolled");
            }

            @Override
            public void onPageScrollStateChanged(int num) {
                // TODO Auto-generated method stub

            }
        });

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                warningDialog.dismiss();
            }
        });

        warningDialog.show();
        Window window = warningDialog.getWindow();
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    }

    @Override
    public void onMethod_pdf_call_back(AssignmentBean assignmentBean) {
        Intent intent = new Intent(ViewAssignment.this, WebViewActivity.class);
        intent.putExtra("MY_kEY",assignmentBean.getFile());
        startActivity(intent);
    }

    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }
    @Override
    public void replyPdf(AssignmentBean assignmentBean) {

    }

    @Override
    public void view_rep_Pdf(AssignmentBean assignmentBean) {
        Gson gson = new Gson();
        String classworkdata = gson.toJson(assignmentBean, AssignmentBean.class);
        Intent intent = new Intent(ViewAssignment.this, MainAssignActivity.class);
        intent.putExtra("data", classworkdata);
        intent.putExtra("sectionname", sectionname);
        intent.putExtra("className", className);
        startActivity(intent);
    }

    @Override
    public void onDelecallback(AssignmentBean homeworkbean) {

    }
}
