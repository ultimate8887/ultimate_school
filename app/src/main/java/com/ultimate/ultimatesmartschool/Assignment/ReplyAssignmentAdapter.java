package com.ultimate.ultimatesmartschool.Assignment;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.ultimate.ultimatesmartschool.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ReplyAssignmentAdapter extends RecyclerView.Adapter<ReplyAssignmentAdapter.MyViewHolder> {

    private Context mContext;
    private ArrayList<ReplyAssignmentBean> assignmentList;
    Mycallback mAdaptercall;

    ReplyAssignmentAdapter(Context mContext, ArrayList<ReplyAssignmentBean> assignmentList,Mycallback mAdaptercall) {
        this.mContext = mContext;
        this.assignmentList = assignmentList;
        this.mAdaptercall = mAdaptercall;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.assignment_lyt, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ReplyAssignmentAdapter.MyViewHolder holder, final int position) {
        ReplyAssignmentBean data = assignmentList.get(position);
        holder.txtTopic.setText("Topic: "+data.getComment());
        holder.txtDate.setText("Stu. Assign. Uploaded On: "+data.getCreated_on());

        holder.txtSM.setText("Subject: " + data.getSubject_name() );
        holder.txtcreatedon.setText("Date:"+data.getAssignment_date());
        holder.textcomment.setText("Student: " + data.getStu_name()+"("+data.getStu_id()+")");


if(assignmentList.get(position).getRep_file() != null) {
    holder.imgAssign.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (assignmentList.get(position).getRep_file() != null) {
                if (mAdaptercall != null) {
                    mAdaptercall.viewPdf(assignmentList.get(position));
                }
            }
        }
    });
}  else{

    holder.imgAssign.setVisibility(View.INVISIBLE);

}

//        if (data.getImage() != null) {
//            Picasso.with(mContext).load(data.getImage()).placeholder(mContext.getResources().getDrawable(R.drawable.picture)).into(holder.imgAssign);
//        }
    }
    public interface Mycallback {


        public void viewPdf(ReplyAssignmentBean assignmentBean);
    }
    @Override
    public int getItemCount() {
        return (assignmentList.size());
    }

    public void setAssignmentList(ArrayList<ReplyAssignmentBean> assignmentList) {
        this.assignmentList = assignmentList;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.imgAssign)
        TextView imgAssign;
        @BindView(R.id.txtTopic)
        TextView txtTopic;
        @BindView(R.id.txtSM)
        TextView txtSM;
        @BindView(R.id.txtDate)
        TextView txtDate;
        @BindView(R.id.txtcreatedon)TextView txtcreatedon;
        @BindView(R.id.textcomment)TextView textcomment;


        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}
