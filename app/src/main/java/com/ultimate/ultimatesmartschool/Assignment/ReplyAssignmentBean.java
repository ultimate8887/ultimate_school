package com.ultimate.ultimatesmartschool.Assignment;

import com.ultimate.ultimatesmartschool.Utility.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ReplyAssignmentBean {


    private static String ID = "id";
    private static String COMMENT = "comment";
    private static String SUB_ID = "subject_id";
    private static String LAST_DATE = "assignment_date";
    private static String IMAGE = "file";
    private static String CREATED_ON = "created_on";
    private static String STATUS = "status";
    private static String SUB_NAME = "subject_name";
    private String id;
    private String assign_id;
    private String comment;
    private String subject_id;
    private String assignment_date;
    private String file;
    private String  class_id;
    private String class_name;
    private String  stu_id;
    private String subject_name;
    private String created_on;
    private String status;
    private String stu_name;
    private String rep_file;

    public String getRep_file() {
        return rep_file;
    }

    public void setRep_file(String rep_file) {
        this.rep_file = rep_file;
    }

    public String getStu_name() {
        return stu_name;
    }

    public void setStu_name(String stu_name) {
        this.stu_name = stu_name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAssign_id() {
        return assign_id;
    }

    public void setAssign_id(String assign_id) {
        this.assign_id = assign_id;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getSubject_id() {
        return subject_id;
    }

    public void setSubject_id(String subject_id) {
        this.subject_id = subject_id;
    }

    public String getAssignment_date() {
        return assignment_date;
    }

    public void setAssignment_date(String assignment_date) {
        this.assignment_date = assignment_date;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public String getClass_id() {
        return class_id;
    }

    public void setClass_id(String class_id) {
        this.class_id = class_id;
    }

    public String getClass_name() {
        return class_name;
    }

    public void setClass_name(String class_name) {
        this.class_name = class_name;
    }

    public String getStu_id() {
        return stu_id;
    }

    public void setStu_id(String stu_id) {
        this.stu_id = stu_id;
    }

    public String getSubject_name() {
        return subject_name;
    }

    public void setSubject_name(String subject_name) {
        this.subject_name = subject_name;
    }

    public String getCreated_on() {
        return created_on;
    }

    public void setCreated_on(String created_on) {
        this.created_on = created_on;
    }

    public static ArrayList<ReplyAssignmentBean> parseAssignmentArray(JSONArray arrayObj) {
        ArrayList list = new ArrayList<ReplyAssignmentBean>();
        try {
            for (int i = 0; i < arrayObj.length(); i++) {
                ReplyAssignmentBean p = parseAssignmentObject(arrayObj.getJSONObject(i));
                if (p != null) {
                    list.add(p);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public static ReplyAssignmentBean parseAssignmentObject(JSONObject jsonObject) {
        ReplyAssignmentBean casteObj = new ReplyAssignmentBean();
        try {
            if (jsonObject.has(ID)) {
                casteObj.setId(jsonObject.getString(ID));
            }
            if (jsonObject.has(COMMENT) && !jsonObject.getString(COMMENT).isEmpty() && !jsonObject.getString(COMMENT).equalsIgnoreCase("null")) {
                casteObj.setComment(jsonObject.getString(COMMENT));
            }
            if (jsonObject.has(SUB_ID) && !jsonObject.getString(SUB_ID).isEmpty() && !jsonObject.getString(SUB_ID).equalsIgnoreCase("null")) {
                casteObj.setSubject_id(jsonObject.getString(SUB_ID));
            }
            if (jsonObject.has(LAST_DATE) && !jsonObject.getString(LAST_DATE).isEmpty() && !jsonObject.getString(LAST_DATE).equalsIgnoreCase("null")) {
                casteObj.setAssignment_date(jsonObject.getString(LAST_DATE));
            }
            if (jsonObject.has(IMAGE) && !jsonObject.getString(IMAGE).isEmpty() && !jsonObject.getString(IMAGE).equalsIgnoreCase("null")) {
                casteObj.setFile(Constants.getImageBaseURL() +jsonObject.getString(IMAGE));
            }
            if (jsonObject.has(CREATED_ON) && !jsonObject.getString(CREATED_ON).isEmpty() && !jsonObject.getString(CREATED_ON).equalsIgnoreCase("null")) {
                casteObj.setCreated_on(jsonObject.getString(CREATED_ON));
            }
            if (jsonObject.has(SUB_NAME) && !jsonObject.getString(SUB_NAME).isEmpty() && !jsonObject.getString(SUB_NAME).equalsIgnoreCase("null")) {
                casteObj.setSubject_name(jsonObject.getString(SUB_NAME));
            }
            if (jsonObject.has("class_id") && !jsonObject.getString("class_id").isEmpty() && !jsonObject.getString("class_id").equalsIgnoreCase("null")) {
                casteObj.setClass_id(jsonObject.getString("class_id"));
            }
            if (jsonObject.has("class_name") && !jsonObject.getString("class_name").isEmpty() && !jsonObject.getString("class_name").equalsIgnoreCase("null")) {
                casteObj.setClass_name(jsonObject.getString("class_name"));
            }
            if (jsonObject.has("assign_id") && !jsonObject.getString("assign_id").isEmpty() && !jsonObject.getString("assign_id").equalsIgnoreCase("null")) {
                casteObj.setAssign_id(jsonObject.getString("assign_id"));
            }
            if (jsonObject.has(STATUS) && !jsonObject.getString(STATUS).isEmpty() && !jsonObject.getString(STATUS).equalsIgnoreCase(STATUS)) {
                casteObj.setStatus(jsonObject.getString(STATUS));
            }
            if (jsonObject.has("stu_name") && !jsonObject.getString("stu_name").isEmpty() && !jsonObject.getString("stu_name").equalsIgnoreCase("null")) {
                casteObj.setStu_name(jsonObject.getString("stu_name"));
            }
            if (jsonObject.has("stu_id") && !jsonObject.getString("stu_id").isEmpty() && !jsonObject.getString("stu_id").equalsIgnoreCase("null")) {
                casteObj.setStu_id(jsonObject.getString("stu_id"));
            }
            if (jsonObject.has("rep_file") && !jsonObject.getString("rep_file").isEmpty() && !jsonObject.getString("rep_file").equalsIgnoreCase("null")) {
                casteObj.setRep_file(Constants.getImageBaseURL() +jsonObject.getString("rep_file"));
            }



        } catch (JSONException e) {
            e.printStackTrace();
        }
        return casteObj;
    }


}
