package com.ultimate.ultimatesmartschool.Holiday;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.ultimate.ultimatesmartschool.BeanModule.User;
import com.ultimate.ultimatesmartschool.Fee.New_Fees.FeePaymentHistory;
import com.ultimate.ultimatesmartschool.Gallery.Add_album;
import com.ultimate.ultimatesmartschool.Gallery.AlbmlistAdapter;
import com.ultimate.ultimatesmartschool.Gallery.Albumlistbean;
import com.ultimate.ultimatesmartschool.Home.HolidayBean;
import com.ultimate.ultimatesmartschool.Home.Holidayadapter;
import com.ultimate.ultimatesmartschool.Homework.StaffWiseHWActivity;
import com.ultimate.ultimatesmartschool.Login.LoginActivity;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.CommonProgress;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AddHolidayActivity extends AppCompatActivity implements Holidayadapter.Mycallback {
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.textNorecord)
    TextView textNorecord;
    @BindView(R.id.textView11)
    TextView textView;
    @BindView(R.id.text)
    TextView totalRecord;
    @BindView(R.id.edtalbumnm)
    EditText albumname;
    @BindView(R.id.cal_img1)
    ImageView cal_img1;
    @BindView(R.id.parent)
    RelativeLayout parent;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    private Holidayadapter adapter;
    ArrayList<HolidayBean> hwList = new ArrayList<>();
    private LinearLayoutManager layoutManager;
    CommonProgress commonProgress;
    String h_date="";
    Animation animation;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_holiday);
        ButterKnife.bind(this);
        animation = AnimationUtils.loadAnimation(this, R.anim.btn_blink_animation);
        commonProgress=new CommonProgress(this);
        txtTitle.setText("Add/View Holiday");
        layoutManager=new LinearLayoutManager(AddHolidayActivity.this);
        recyclerView.setLayoutManager(layoutManager);
        adapter=new Holidayadapter(hwList,AddHolidayActivity.this,1,this);
        recyclerView.setAdapter(adapter);
        fetchalbumlist();
    }

    @Override
    protected void onResume() {
        super.onResume();

    }


    @OnClick(R.id.one)
    public void today() {
        cal_img1.startAnimation(animation);
        java.util.Calendar cal = java.util.Calendar.getInstance(TimeZone.getDefault()); // Get current date
        DatePickerDialog datePicker = new DatePickerDialog(AddHolidayActivity.this,
                R.style.MyDatePickerDialogTheme, ondate,
                cal.get(java.util.Calendar.YEAR),
                cal.get(java.util.Calendar.MONTH),
                cal.get(java.util.Calendar.DAY_OF_MONTH));
        datePicker.getDatePicker().setMinDate(new java.util.Date().getTime());
        datePicker.setCancelable(false);
        datePicker.show();
    }

    DatePickerDialog.OnDateSetListener ondate = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            java.util.Calendar calendar = java.util.Calendar.getInstance(TimeZone.getDefault());
            calendar.set(year, monthOfYear, dayOfMonth);
            calendar.set(Calendar.HOUR_OF_DAY, 00);
            calendar.set(Calendar.MINUTE, 00);
            calendar.set(Calendar.SECOND, 00);
            java.util.Date setdate = calendar.getTime();
            SimpleDateFormat fmtOut = new SimpleDateFormat("dd MMM, yyyy");
            String dateString = fmtOut.format(setdate);
            textView.setText(dateString);
            SimpleDateFormat dateFrmOut = new SimpleDateFormat("yyyy-MM-dd");
            h_date = dateFrmOut.format(setdate);

        }
    };


    public void fetchalbumlist(){
        commonProgress.show();
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("check", "full");
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.HOLIDAY_URL, albmapiCallback, this, params);

    }

    ApiHandler.ApiCallback albmapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            commonProgress.dismiss();
            if (error == null) {
                try {
                    if (hwList != null) {
                        hwList.clear();
                    }
                    JSONArray hArray = jsonObject.getJSONArray("holiday_data");
                    hwList = HolidayBean.parseArray(hArray);
                    if (hwList.size() > 0) {
                        adapter.setHList(hwList);
                        adapter.notifyDataSetChanged();
                        textNorecord.setVisibility(View.GONE);
                        totalRecord.setText("Total Entries:- "+String.valueOf(hwList.size()));
                    } else {
                        adapter.setHList(hwList);
                        adapter.notifyDataSetChanged();
                        textNorecord.setVisibility(View.VISIBLE);
                        totalRecord.setText("Total Entries:- 0");

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                textNorecord.setVisibility(View.VISIBLE);
                hwList.clear();
                adapter.setHList(hwList);
                totalRecord.setText("Total Entries:- 0");
                adapter.notifyDataSetChanged();
            }
        }
    };



    @OnClick(R.id.add_album)public void addalbum(){
        if (checkValid()) {
            commonProgress.show();
            HashMap<String, String> params = new HashMap<String, String>();
            params.put("albm_title", albumname.getText().toString());
            params.put("hdate", h_date);
            ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.ADDHOLIDAY_URL, apiCallback, this, params);
        }

    }
    ApiHandler.ApiCallback apiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            commonProgress.dismiss();
            if (error == null) {
                try {
                    Utils.showSnackBar(jsonObject.getString("msg"), parent);
                    albumname.setText("");
                    h_date="";
                    textView.setText("Add Holiday Date");
                    fetchalbumlist();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("error", error.getMessage() + "");
                Utils.showSnackBar(error.getMessage(), parent);
                if (error.getStatusCode() == 405) {
                    User.logout();
                    startActivity(new Intent(AddHolidayActivity.this, LoginActivity.class));
                    finish();
                }
            }
        }
    };
    private boolean checkValid() {
        boolean valid = true;
        String errorMsg = null;
        if (albumname.getText().toString().trim().length() <= 0) {
            valid = false;
            errorMsg = "Please enter title";
        }
        if (h_date.equalsIgnoreCase("")) {
            valid = false;
            errorMsg = "Please add holiday date";
        }
        if (!valid) {
            Utils.showSnackBar(errorMsg, parent);
        }
        return valid;
    }

    @OnClick(R.id.imgBack)
    public void onback() {
        finish();
    }

    @Override
    public void onDelecallback(HolidayBean homeworkbean) {
        commonProgress.show();
        HashMap<String, String> params = new HashMap<>();
        params.put("h_id", homeworkbean.getId());
        params.put("check", "holiday");
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.DELETEHW, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                ErpProgress.cancelProgressBar();
                commonProgress.dismiss();
                if (error == null) {
                    int pos = hwList.indexOf(homeworkbean);
                    hwList.remove(pos);
                    adapter.setHList(hwList);
                    adapter.notifyDataSetChanged();
                    Utils.showSnackBar("Deleted Successfully!", parent);
                    totalRecord.setText("Total Entries:- "+String.valueOf(hwList.size()));
                } else {
                    Log.e("error", error.getMessage() + "");
                    Utils.showSnackBar(error.getMessage(), parent);

                    if (error.getStatusCode() == 405) {
                        User.logout();
                        startActivity(new Intent(AddHolidayActivity.this, LoginActivity.class));
                        finish();
                    }
                }
            }
        }, this, params);
    }
}