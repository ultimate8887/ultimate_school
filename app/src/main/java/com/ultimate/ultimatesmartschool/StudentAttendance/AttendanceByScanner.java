package com.ultimate.ultimatesmartschool.StudentAttendance;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.google.zxing.Result;
import com.journeyapps.barcodescanner.BarcodeCallback;
import com.journeyapps.barcodescanner.BarcodeResult;
import com.journeyapps.barcodescanner.DecoratedBarcodeView;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AttendanceByScanner extends AppCompatActivity {
    @BindView(R.id.lightButton)
    ImageView flashImageView;
    @BindView(R.id.parent)
    LinearLayout parent;

    public String date, classid, sectionid;

    private DecoratedBarcodeView barcodeScannerView;
    private boolean flashState = false;
    private ArrayList<AttendBean> dataList;
    TextView name, fname;
    Dialog dialog;
    private GDSStudAttendAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attendance_by_scanner);
        ButterKnife.bind(this);

        dataList = new ArrayList<>();
        adapter = new GDSStudAttendAdapter(this, dataList);
        ActivityCompat.requestPermissions(AttendanceByScanner.this,
                new String[]{Manifest.permission.CAMERA},
                1);

        barcodeScannerView = findViewById(R.id.barcode_scanner);

        SimpleDateFormat dateFrmOut = new SimpleDateFormat("yyyy-MM-dd");
        date = dateFrmOut.format(new Date());

//
//        ImageView imgback = (ImageView) findViewById(R.id.imgBack);
//        imgback.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                finish();
//            }
//        });

        flashImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleFlash();
            }
        });

        barcodeScannerView.decodeContinuous(new BarcodeCallback() {
            @Override
            public void barcodeResult(BarcodeResult result) {
                handleResult(result.getResult());
            }
        });
    }

    private void toggleFlash() {
//        flashState = !flashState;
//        barcodeScannerView.setTorch(flashState);
//        flashImageView.setBackgroundResource(flashState ? R.drawable.ic_flash_off : R.drawable.ic_flash_on);
//        Toast.makeText(getApplicationContext(), flashState ? "Flashlight turned on" : "Flashlight turned off", Toast.LENGTH_SHORT).show();
    }

    private void handleResult(Result rawResult) {
        searchStudent(rawResult.getText());
        dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.custom_dialogqrscan);
        View v = dialog.getWindow().getDecorView();
        v.setBackgroundResource(android.R.color.transparent);
        TextView text = dialog.findViewById(R.id.someText);
        text.setText(rawResult.getText());
        ImageView img = dialog.findViewById(R.id.imgOfDialog);
        img.setImageResource(R.drawable.ic_done_gr);
        name = dialog.findViewById(R.id.name);
        fname = dialog.findViewById(R.id.fname);

        Button webSearch = dialog.findViewById(R.id.searchButton);
        webSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addAttend();
                dialog.dismiss();
                barcodeScannerView.resume();
            }
        });

        ImageView close = (ImageView) dialog.findViewById(R.id.closss);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                //   dialog.dismiss();
            }
        });

        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                barcodeScannerView.resume();
            }
        });
        dialog.show();
    }

    private void addAttend() {
        ErpProgress.showProgressBar(this, "Please wait...");
        dataList = adapter.getAttendList();
        ArrayList<String> attendList = new ArrayList<>();
        ArrayList<String> id = new ArrayList<>();
        ArrayList<String> st_id = new ArrayList<>();
        ArrayList<String> st_name = new ArrayList<>();
        for (AttendBean bean : dataList) {
            attendList.add(bean.getAttendance());
            st_id.add(bean.getStudent_id());
            id.add(bean.getId());
            st_name.add(bean.getStudent_name());
        }
        HashMap<String, String> params = new HashMap<>();
        params.put("attend", attendList.toString());
        params.put("st_id", st_id.toString());
        params.put("id", id.toString());
        params.put("st_name", st_name.toString());
        params.put("class_id", classid);
        params.put("date", date);
        params.put("section_id", sectionid);
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.ADD_ST_ATTEND_URLGDS, apiCallback, this, params);
    }

    ApiHandler.ApiCallback apiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            if (error == null) {
                try {
                    Toast.makeText(AttendanceByScanner.this, jsonObject.getString("msg"), Toast.LENGTH_SHORT).show();
                    finish();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("error", error.getMessage() + "");
                Utils.showSnackBar("student have already marked attendance", parent);
            }
        }
    };

    public void searchStudent(String student_id) {
        ErpProgress.showProgressBar(this, "Please wait...");
        HashMap<String, String> params = new HashMap<>();
        params.put("stu_id", student_id);
        if (date != null) {
            params.put("mdate", date);
        }
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.GDSSTUDENT_ATTEND_URLNEW, studentapiCallback, this, params);
    }

    ApiHandler.ApiCallback studentapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            if (error == null) {
                try {
                    ArrayList<AttendBean> studattendList = new ArrayList<>();
                    ArrayList<AttendBean> attendList = AttendBean.parseAttendArray(jsonObject.getJSONArray(Constants.ATTEND_DATA));
                    classid = attendList.get(0).getClass_id();
                    sectionid = attendList.get(0).getSection_id();
                    name.setText(attendList.get(0).getStudent_name() + "(" + attendList.get(0).getClass_name() + ")" + attendList.get(0).getSection_name());
                    fname.setText(attendList.get(0).getFather_name());
                    if (jsonObject.has("stud_data")) {
                        studattendList = AttendBean.parseAttendArray(jsonObject.getJSONArray("stud_data"));

                        if (studattendList.size() > attendList.size()) {
                            for (AttendBean studBean : studattendList) {
                                boolean check = true;
                                for (AttendBean attendBean : attendList) {
                                    if (studBean.getStudent_id().equals(attendBean.getStudent_id())) {
                                        check = false;
                                        break;
                                    }
                                }
                                if (check) {
                                    attendList.add(studBean);
                                }
                            }
                        } else {
                            for (int i = 0; i < attendList.size(); i++) {
                                boolean check = true;
                                for (AttendBean studBean : studattendList) {
                                    if (studBean.getStudent_id().equals(attendList.get(i).getStudent_id())) {
                                        check = false;
                                        break;
                                    }
                                }
                                if (check) {
                                    attendList.remove(i);
                                    i--;
                                }
                            }
                        }
                    }

                    dataList.clear();
                    dataList.addAll(attendList);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Utils.showSnackBar(error.getMessage(), parent);
            }
        }
    };

    @Override
    public void onResume() {
        super.onResume();
        barcodeScannerView.resume();
    }

    @Override
    public void onPause() {
        super.onPause();
        barcodeScannerView.pause();
    }
}
