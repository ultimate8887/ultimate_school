package com.ultimate.ultimatesmartschool.StudentAttendance;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class AttendBean {


    private static String ID = "id";
    private static String STUDENT_ID = "student_id";
    private static String STUDENT_NAME = "student_name";
    private static String ATTENDANCE = "attendance";

    /**
     * id : 2
     * student_id : 8
     * student_name : tjytuyt
     * attendance : P
     */
    private String id;
    private String student_id;
    private String student_name;
    private String attendance;

    private String section_name;
    private String class_id;
    private String class_name;
    private String section_id;

    private String morning_vehicle_id;
    private String evening_vehicle_id;

    private String tr_transport_name;
    private String tr_vehicle_no;

    private String route_name;

    private String route_id;
    private String pickupid;
    private String checkintime;
    private String checkouttime;
//these are new parameter for transport evening transport scanning
    private String  tr_transport_name_eveni;
    private String  tr_vehicle_no_eveni;

    private String driver_shift;
    private String evendriver_shift;

    private String driver_name;
    private String evendriver_name;
    private String diver_mobile;
    private String evendiver_mobile;

    private String driver_id;
    private String evendriver_id;

    private String AttendMarkByUser;

    public String getAttendMarkByUser() {
        return AttendMarkByUser;
    }

    public void setAttendMarkByUser(String attendMarkByUser) {
        AttendMarkByUser = attendMarkByUser;
    }

    public String getL_remarks() {
        return l_remarks;
    }

    public void setL_remarks(String l_remarks) {
        this.l_remarks = l_remarks;
    }


    private String l_remarks;

    public String getDriver_id() {
        return driver_id;
    }

    public void setDriver_id(String driver_id) {
        this.driver_id = driver_id;
    }

    public String getEvendriver_id() {
        return evendriver_id;
    }

    public void setEvendriver_id(String evendriver_id) {
        this.evendriver_id = evendriver_id;
    }



    public String getDriver_shift() {
        return driver_shift;
    }

    public void setDriver_shift(String driver_shift) {
        this.driver_shift = driver_shift;
    }

    public String getEvendriver_shift() {
        return evendriver_shift;
    }

    public void setEvendriver_shift(String evendriver_shift) {
        this.evendriver_shift = evendriver_shift;
    }

    public String getDriver_name() {
        return driver_name;
    }

    public void setDriver_name(String driver_name) {
        this.driver_name = driver_name;
    }

    public String getEvendriver_name() {
        return evendriver_name;
    }

    public void setEvendriver_name(String evendriver_name) {
        this.evendriver_name = evendriver_name;
    }

    public String getDiver_mobile() {
        return diver_mobile;
    }

    public void setDiver_mobile(String diver_mobile) {
        this.diver_mobile = diver_mobile;
    }

    public String getEvendiver_mobile() {
        return evendiver_mobile;
    }

    public void setEvendiver_mobile(String evendiver_mobile) {
        this.evendiver_mobile = evendiver_mobile;
    }

    public String getTr_transport_name_eveni() {
        return tr_transport_name_eveni;
    }

    public void setTr_transport_name_eveni(String tr_transport_name_eveni) {
        this.tr_transport_name_eveni = tr_transport_name_eveni;
    }

    public String getTr_vehicle_no_eveni() {
        return tr_vehicle_no_eveni;
    }

    public void setTr_vehicle_no_eveni(String tr_vehicle_no_eveni) {
        this.tr_vehicle_no_eveni = tr_vehicle_no_eveni;
    }

    public String getCheckouttime() {
        return checkouttime;
    }

    public void setCheckouttime(String checkouttime) {
        this.checkouttime = checkouttime;
    }

    private String drivername;

    private String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCheckintime() {
        return checkintime;
    }

    public void setCheckintime(String checkintime) {
        this.checkintime = checkintime;
    }

    public String getDrivername() {
        return drivername;
    }

    public void setDrivername(String drivername) {
        this.drivername = drivername;
    }

    public String getRoute_id() {
        return route_id;
    }

    public void setRoute_id(String route_id) {
        this.route_id = route_id;
    }

    public String getPickupid() {
        return pickupid;
    }

    public void setPickupid(String pickupid) {
        this.pickupid = pickupid;
    }

    public String getRoute_name() {
        return route_name;
    }

    public void setRoute_name(String route_name) {
        this.route_name = route_name;
    }

    public String getPickupname() {
        return pickupname;
    }

    public void setPickupname(String pickupname) {
        this.pickupname = pickupname;
    }

    private String pickupname;
    public String getMorning_vehicle_id() {
        return morning_vehicle_id;
    }

    public void setMorning_vehicle_id(String morning_vehicle_id) {
        this.morning_vehicle_id = morning_vehicle_id;
    }

    public String getEvening_vehicle_id() {
        return evening_vehicle_id;
    }

    public void setEvening_vehicle_id(String evening_vehicle_id) {
        this.evening_vehicle_id = evening_vehicle_id;
    }

    public String getTr_transport_name() {
        return tr_transport_name;
    }

    public void setTr_transport_name(String tr_transport_name) {
        this.tr_transport_name = tr_transport_name;
    }

    public String getTr_vehicle_no() {
        return tr_vehicle_no;
    }

    public void setTr_vehicle_no(String tr_vehicle_no) {
        this.tr_vehicle_no = tr_vehicle_no;
    }

    public String getSection_name() {
        return section_name;
    }

    public void setSection_name(String section_name) {
        this.section_name = section_name;
    }

    public String getClass_id() {
        return class_id;
    }

    public void setClass_id(String class_id) {
        this.class_id = class_id;
    }

    public String getClass_name() {
        return class_name;
    }

    public void setClass_name(String class_name) {
        this.class_name = class_name;
    }

    public String getSection_id() {
        return section_id;
    }

    public void setSection_id(String section_id) {
        this.section_id = section_id;
    }

    /**
     * father_name : Sandeep Kumar
     */

    private String father_name="";
    private String sno;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public static ArrayList<AttendBean> parseAttendArray(JSONArray arrayObj) {
        ArrayList list = new ArrayList<AttendBean>();
        try {
            for (int i = 0; i < arrayObj.length(); i++) {
                AttendBean p = parseAttendObject(arrayObj.getJSONObject(i));
                if (p != null) {
                    list.add(p);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public static AttendBean parseAttendObject(JSONObject jsonObject) {
        AttendBean casteObj = new AttendBean();
        try {
            if (jsonObject.has(ID)) {
                casteObj.setId(jsonObject.getString(ID));
            }
            if (jsonObject.has(STUDENT_ID) && !jsonObject.getString(STUDENT_ID).isEmpty() && !jsonObject.getString(STUDENT_ID).equalsIgnoreCase("null")) {
                casteObj.setStudent_id(jsonObject.getString(STUDENT_ID));
            }
            if (jsonObject.has(STUDENT_NAME) && !jsonObject.getString(STUDENT_NAME).isEmpty() && !jsonObject.getString(STUDENT_NAME).equalsIgnoreCase("null")) {
                casteObj.setStudent_name(jsonObject.getString(STUDENT_NAME));
            }
            if (jsonObject.has("father_name") && !jsonObject.getString("father_name").isEmpty() && !jsonObject.getString("father_name").equalsIgnoreCase("null")) {
                casteObj.setFather_name(jsonObject.getString("father_name"));
            }
            if (jsonObject.has("sno") && !jsonObject.getString("sno").isEmpty() && !jsonObject.getString("sno").equalsIgnoreCase("null")) {
                casteObj.setSno(jsonObject.getString("sno"));
            }

            if (jsonObject.has(ATTENDANCE) && !jsonObject.getString(ATTENDANCE).isEmpty() && !jsonObject.getString(ATTENDANCE).equalsIgnoreCase("null")) {
                casteObj.setAttendance(jsonObject.getString(ATTENDANCE));
            } else {
                casteObj.setAttendance("P");
            }


            if (jsonObject.has("l_remarks") && !jsonObject.getString("l_remarks").isEmpty() && !jsonObject.getString("l_remarks").equalsIgnoreCase("null")) {
                casteObj.setL_remarks(jsonObject.getString("l_remarks"));
            }

            if (jsonObject.has("class_id") && !jsonObject.getString("class_id").isEmpty() && !jsonObject.getString("class_id").equalsIgnoreCase("null")) {
                casteObj.setClass_id(jsonObject.getString("class_id"));
            }
            if (jsonObject.has("class_name") && !jsonObject.getString("class_name").isEmpty() && !jsonObject.getString("class_name").equalsIgnoreCase("null")) {
                casteObj.setClass_name(jsonObject.getString("class_name"));
            }
            if (jsonObject.has("section_id") && !jsonObject.getString("section_id").isEmpty() && !jsonObject.getString("section_id").equalsIgnoreCase("null")) {
                casteObj.setSection_id(jsonObject.getString("section_id"));
            }
            if (jsonObject.has("section_name") && !jsonObject.getString("section_name").isEmpty() && !jsonObject.getString("section_name").equalsIgnoreCase("null")) {
                casteObj.setSection_name(jsonObject.getString("section_name"));
            }



            if (jsonObject.has("morning_vehicle_id") && !jsonObject.getString("morning_vehicle_id").isEmpty() && !jsonObject.getString("morning_vehicle_id").equalsIgnoreCase("null")) {
                casteObj.setMorning_vehicle_id(jsonObject.getString("morning_vehicle_id"));
            }
            if (jsonObject.has("evening_vehicle_id") && !jsonObject.getString("evening_vehicle_id").isEmpty() && !jsonObject.getString("evening_vehicle_id").equalsIgnoreCase("null")) {
                casteObj.setEvening_vehicle_id(jsonObject.getString("evening_vehicle_id"));
            }
            if (jsonObject.has("tr_transport_name") && !jsonObject.getString("tr_transport_name").isEmpty() && !jsonObject.getString("tr_transport_name").equalsIgnoreCase("null")) {
                casteObj.setTr_transport_name(jsonObject.getString("tr_transport_name"));
            }
            if (jsonObject.has("tr_vehicle_no") && !jsonObject.getString("tr_vehicle_no").isEmpty() && !jsonObject.getString("tr_vehicle_no").equalsIgnoreCase("null")) {
                casteObj.setTr_vehicle_no(jsonObject.getString("tr_vehicle_no"));
            }


            if (jsonObject.has("route_name") && !jsonObject.getString("route_name").isEmpty() && !jsonObject.getString("route_name").equalsIgnoreCase("null")) {
                casteObj.setRoute_name(jsonObject.getString("route_name"));
            }
            if (jsonObject.has("pickupname") && !jsonObject.getString("pickupname").isEmpty() && !jsonObject.getString("pickupname").equalsIgnoreCase("null")) {
                casteObj.setPickupname(jsonObject.getString("pickupname"));
            }

            if (jsonObject.has("route_id") && !jsonObject.getString("route_id").isEmpty() && !jsonObject.getString("route_id").equalsIgnoreCase("null")) {
                casteObj.setRoute_id(jsonObject.getString("route_id"));
            }
            if (jsonObject.has("pickupid") && !jsonObject.getString("pickupid").isEmpty() && !jsonObject.getString("pickupid").equalsIgnoreCase("null")) {
                casteObj.setPickupid(jsonObject.getString("pickupid"));
            }

            if (jsonObject.has("checkintime") && !jsonObject.getString("checkintime").isEmpty() && !jsonObject.getString("checkintime").equalsIgnoreCase("null")) {
                casteObj.setCheckintime(jsonObject.getString("checkintime"));
            }
            if (jsonObject.has("drivername") && !jsonObject.getString("drivername").isEmpty() && !jsonObject.getString("drivername").equalsIgnoreCase("null")) {
                casteObj.setDrivername(jsonObject.getString("drivername"));
            }

            if (jsonObject.has("status") && !jsonObject.getString("status").isEmpty() && !jsonObject.getString("status").equalsIgnoreCase("null")) {
                casteObj.setStatus(jsonObject.getString("status"));
            }
            if (jsonObject.has("checkouttime") && !jsonObject.getString("checkouttime").isEmpty() && !jsonObject.getString("checkouttime").equalsIgnoreCase("null")) {
                casteObj.setCheckouttime(jsonObject.getString("checkouttime"));
            }


            if (jsonObject.has("tr_transport_name_eveni") && !jsonObject.getString("tr_transport_name_eveni").isEmpty() && !jsonObject.getString("tr_transport_name_eveni").equalsIgnoreCase("null")) {
                casteObj.setTr_transport_name_eveni(jsonObject.getString("tr_transport_name_eveni"));
            }
            if (jsonObject.has("tr_vehicle_no_eveni") && !jsonObject.getString("tr_vehicle_no_eveni").isEmpty() && !jsonObject.getString("tr_vehicle_no_eveni").equalsIgnoreCase("null")) {
                casteObj.setTr_vehicle_no_eveni(jsonObject.getString("tr_vehicle_no_eveni"));
            }


            if (jsonObject.has("driver_shift") && !jsonObject.getString("driver_shift").isEmpty() && !jsonObject.getString("driver_shift").equalsIgnoreCase("null")) {
                casteObj.setDriver_shift(jsonObject.getString("driver_shift"));
            }
            if (jsonObject.has("evendriver_shift") && !jsonObject.getString("evendriver_shift").isEmpty() && !jsonObject.getString("evendriver_shift").equalsIgnoreCase("null")) {
                casteObj.setEvendriver_shift(jsonObject.getString("evendriver_shift"));
            }

            if (jsonObject.has("driver_name") && !jsonObject.getString("driver_name").isEmpty() && !jsonObject.getString("driver_name").equalsIgnoreCase("null")) {
                casteObj.setDrivername(jsonObject.getString("driver_name"));
            }
            if (jsonObject.has("evendriver_name") && !jsonObject.getString("evendriver_name").isEmpty() && !jsonObject.getString("evendriver_name").equalsIgnoreCase("null")) {
                casteObj.setEvendriver_name(jsonObject.getString("evendriver_name"));
            }

            if (jsonObject.has("diver_mobile") && !jsonObject.getString("diver_mobile").isEmpty() && !jsonObject.getString("diver_mobile").equalsIgnoreCase("null")) {
                casteObj.setDiver_mobile(jsonObject.getString("diver_mobile"));
            }
            if (jsonObject.has("evendiver_mobile") && !jsonObject.getString("evendiver_mobile").isEmpty() && !jsonObject.getString("evendiver_mobile").equalsIgnoreCase("null")) {
                casteObj.setEvendiver_mobile(jsonObject.getString("evendiver_mobile"));
            }



            if (jsonObject.has("driver_id") && !jsonObject.getString("driver_id").isEmpty() && !jsonObject.getString("driver_id").equalsIgnoreCase("null")) {
                casteObj.setDriver_id(jsonObject.getString("driver_id"));
            }
            if (jsonObject.has("evendriver_id") && !jsonObject.getString("evendriver_id").isEmpty() && !jsonObject.getString("evendriver_id").equalsIgnoreCase("null")) {
                casteObj.setEvendriver_id(jsonObject.getString("evendriver_id"));
            }
            if (jsonObject.has("AttendMarkByUser") && !jsonObject.getString("AttendMarkByUser").isEmpty() && !jsonObject.getString("AttendMarkByUser").equalsIgnoreCase("null")) {
                casteObj.setAttendMarkByUser(jsonObject.getString("AttendMarkByUser"));
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }
        return casteObj;
    }

    public String getStudent_id() {
        return student_id;
    }

    public void setStudent_id(String student_id) {
        this.student_id = student_id;
    }

    public String getStudent_name() {
        return student_name;
    }

    public void setStudent_name(String student_name) {
        this.student_name = student_name;
    }

    public String getAttendance() {
        return attendance;
    }

    public void setAttendance(String attendance) {
        this.attendance = attendance;
    }

    public String getFather_name() {
        return father_name;
    }

    public void setFather_name(String father_name) {
        this.father_name = father_name;
    }

    public String getSno() {
        return sno;
    }

    public void setSno(String sno) {
        this.sno = sno;
    }
}
