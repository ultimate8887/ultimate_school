package com.ultimate.ultimatesmartschool.StudentAttendance;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.ultimate.ultimatesmartschool.R;

import java.util.ArrayList;
import java.util.LinkedHashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

public class GDSStudAttendAdapter extends RecyclerView.Adapter<GDSStudAttendAdapter.MyViewHolder> {

    private Context mContext;
    ArrayList<AttendBean> dataList;
    private LinkedHashMap<String, String> mapAttendanceData;
    String[] list = {
            "P",
            "A",
            "L",
    };

    public GDSStudAttendAdapter(Context mContext, ArrayList<AttendBean> dataList) {
        this.mContext = mContext;
        this.dataList = dataList;
        this.mapAttendanceData = mapAttendanceData;
    }

    @Override
    public GDSStudAttendAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.stud_attend_lyt, parent, false);
        GDSStudAttendAdapter.MyViewHolder vh = new GDSStudAttendAdapter.MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final GDSStudAttendAdapter.MyViewHolder holder, final int position) {
        final AttendBean data = dataList.get(position);
        holder.txtName.setText(data.getStudent_name());
        holder.txtRegNo.setText(data.getStudent_id()+"("+data.getClass_name()+")"+" "+data.getSection_name());
      //  Log.e("myyyyy",data.getSection_name());
        holder.txtFname.setText("Father: "+data.getFather_name());

        Log.e("fathername",data.getFather_name());
        if (!data.getId().equalsIgnoreCase("0")) {
            for (int i = 0; i < list.length; i++) {
                if (data.getAttendance().equalsIgnoreCase(list[i])) {
                    holder.spinnerAttend.setSelection(i);
                    // holder.txtRegNo.setText(i);
                    Log.e("whats what","aaa");
                    break;
                }
            }
        }
        else {

            // holder.spinnerAttend.setSelection(Integer.parseInt(mapAttendanceData.get(data.getId())));
            holder.spinnerAttend.setSelection(0);
            for (int i = 0; i < list.length; i++) {
                if (data.getAttendance().equalsIgnoreCase(list[i])) {
                    holder.spinnerAttend.setSelection(i);
                    // holder.txtRegNo.setText(i);
                    Log.e("whats what","aaa");
                    break;
                }
            }

        }






        holder.spinnerAttend.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                dataList.get(position).setAttendance(list[i]);//isme selected attendance ka value set ho ra hai
                //holder.txtRegNo.setText(data.getAttendance());
                // Toast.makeText(mContext,list[i],Toast.LENGTH_LONG).show();



            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
    }

    @Override
    public int getItemCount() {
        return (dataList.size());
    }

    public ArrayList<AttendBean> getAttendList() {
        return dataList;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txtRegNo)
        TextView txtRegNo;
        @BindView(R.id.txtName)
        TextView txtName;
        @BindView(R.id.spinnerAttend)
        Spinner spinnerAttend;
        @BindView(R.id.txtFname)
        TextView txtFname;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(mContext, android.
                    R.layout.simple_spinner_dropdown_item, list);
            spinnerAttend.setAdapter(adapter);
        }
    }




}
