package com.ultimate.ultimatesmartschool.StudentAttendance;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.ultimate.ultimatesmartschool.BeanModule.Studentbean;
import com.ultimate.ultimatesmartschool.R;

import java.util.ArrayList;

public class Spinner_stu_adapterGds extends BaseAdapter {
    Context context;
    ArrayList<Studentbean> stuList;
    LayoutInflater inflter;
    public Spinner_stu_adapterGds(Context context, ArrayList<Studentbean> stuList) {
        this.context=context;
        this.stuList=stuList;
        inflter = (LayoutInflater.from(context));
    }

    @Override
    public int getCount() {
        return stuList.size()+1;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = inflter.inflate(R.layout.spinner_lyt, null);
        TextView label = (TextView) convertView.findViewById(R.id.txtText);
        if (position == 0) {
            // Default selected Spinner item
            label.setText("Select Student");
        } else {
            Studentbean classObj = stuList.get(position - 1);
            label.setText(classObj.getName());
            if(classObj.getFather_name()!= null && classObj.getId()!=null){
                label.setText("("+classObj.getId()+")"+classObj.getName()+" ("+classObj.getFather_name()+" )");
            }else{
                label.setText(classObj.getName()+" ("+classObj.getId()+" )");
            }

        }
        return convertView;
    }
}
