package com.ultimate.ultimatesmartschool.StudentAttendance.SubjectWiseAttendeance;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.ultimate.ultimatesmartschool.BeanModule.ClassBean;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.StudentAttendance.AttendMonthBean;
import com.ultimate.ultimatesmartschool.StudentAttendance.ClassReportMonAdapter;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AttendReportAdapter extends RecyclerView.Adapter<AttendReportAdapter.MyViewHolder> {

    private Context mContext;
    ArrayList<ClassBean> dataList;
    public AttendReportAdapter(Context mContext, ArrayList<ClassBean> dataList) {
        this.mContext = mContext;
        this.dataList = dataList;
    }


    @Override
    public AttendReportAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.mon_class_report_new, parent, false);
        AttendReportAdapter.MyViewHolder vh = new AttendReportAdapter.MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(AttendReportAdapter.MyViewHolder holder, final int position) {
        if (position == 0) {
            holder.lytHeader.setVisibility(View.GONE);
            holder.lytData.setVisibility(View.GONE);
        } else {
            holder.lytHeader.setVisibility(View.GONE);
            holder.lytData.setVisibility(View.VISIBLE);
            final ClassBean data = dataList.get(position - 1);
            holder.txtRoll.setText(data.getName());
//            holder.txtFName.setText(data.getFather_name());
//            if(data.get)
            holder.txtAbsent.setText(data.getA_student());
            holder.txtLeave.setText(data.getL_student());
            holder.txtPresent.setText(data.getP_student());

            int total=0,a=0,p=0,l=0;
            a= Integer.parseInt(data.getA_student());
            p= Integer.parseInt(data.getP_student());
            l= Integer.parseInt(data.getL_student());
            total=a+p+l;
            holder.txtName.setText(String.valueOf(total));

        }

    }

    @Override
    public int getItemCount() {
        return (dataList.size() + 1);
    }


    public void setList(ArrayList<ClassBean> dataList) {
        this.dataList = dataList;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txtAbsent)
        TextView txtAbsent;
        @BindView(R.id.txtLeave)
        TextView txtLeave;
        @BindView(R.id.txtPresent)
        TextView txtPresent;
        @BindView(R.id.txtName)
        TextView txtName;
        @BindView(R.id.txtRoll)
        TextView txtRoll;
        @BindView(R.id.lytData)
        LinearLayout lytData;
        @BindView(R.id.lytHeader)
        LinearLayout lytHeader;
        @BindView(R.id.txtFName)
        TextView txtFName;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
