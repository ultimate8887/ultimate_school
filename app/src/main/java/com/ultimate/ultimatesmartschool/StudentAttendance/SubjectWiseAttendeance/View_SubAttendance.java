package com.ultimate.ultimatesmartschool.StudentAttendance.SubjectWiseAttendeance;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetSequence;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.ultimate.ultimatesmartschool.BeanModule.ClassBean;
import com.ultimate.ultimatesmartschool.BeanModule.SectionBean;
import com.ultimate.ultimatesmartschool.BeanModule.Studentbean;
import com.ultimate.ultimatesmartschool.BeanModule.SubjectBeancls;
import com.ultimate.ultimatesmartschool.BeanModule.User;
import com.ultimate.ultimatesmartschool.Homework.AdapterOfSubjectList;
import com.ultimate.ultimatesmartschool.Homework.SectionnewAdapter;
import com.ultimate.ultimatesmartschool.Login.LoginActivity;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.StudentAttendance.AttendMod;
import com.ultimate.ultimatesmartschool.StudentAttendance.ClassAdapterAttend;
import com.ultimate.ultimatesmartschool.StudentAttendance.MonthAdapter;
import com.ultimate.ultimatesmartschool.StudentAttendance.Spinner_stu_adapterGds;
import com.ultimate.ultimatesmartschool.StudentAttendance.Util;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class View_SubAttendance extends AppCompatActivity {

    @BindView(R.id.gridview)
    RecyclerView recyclerView;
    @BindView(R.id.currentMonth)
    TextView currentMonth;
    @BindView(R.id.h_currentday)
    TextView h_currentday;
    @BindView(R.id.h_day_name)
    TextView h_day_name;
    @BindView(R.id.h_currentMonth)
    TextView h_currentMonth;
    ArrayList<AttendMod> attendList = new ArrayList<>();
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.txtSub)
    TextView txtSub;
    @BindView(R.id.attendlayout)
    NestedScrollView attendlayout;
    @BindView(R.id.noofpresent)TextView noofpresent;
    @BindView(R.id.noofabsent)TextView noofabsent;
    @BindView(R.id.noofleave)TextView noofleave;
    @BindView(R.id.noofholydy)TextView noofholydy;
    @BindView(R.id.totalwork)TextView totalwork;
    @BindView(R.id.totalpresent)TextView totalpresent;
    @BindView(R.id.annualpercent)TextView annualpercent;
    @BindView(R.id.attenddetailpercentlay)
    CardView attenddetailpercentlay;
    @BindView(R.id.lytCard)
    LinearLayout lytCard;
    @BindView(R.id.lytCard1)
    LinearLayout lytCard1;
    @BindView(R.id.h_calender)
    ImageView h_calender;
    @BindView(R.id.btnBefore)
    TextView btnBefore;
    @BindView(R.id.btnNext)
    TextView btnNext;
    @BindView(R.id.totalannualworkd)
    TextView totalannualworkd;
    @BindView(R.id.dialog)
    ImageView dialog;
    Animation animation,animation1,animation2,animation3,top_curve_anim;
    @BindView(R.id.imgBack)
    ImageView back;


    SharedPreferences sharedPreferences;
    ArrayList<ClassBean> classList = new ArrayList<>();
    @BindView(R.id.parent)
    RelativeLayout parent;
    //@BindView(R.id.spinner) Spinner spinnerClass;
    BottomSheetDialog mBottomSheetDialog;
    private int loaded = 0;
    int check=0;
    Spinner spinnersection;
    Spinner spinnerClass;
    Spinner spinnersubject;
    @BindView(R.id.spinnerstudnt)
    Spinner spinnerstudnt;
    @BindView(R.id.stdlayout)
    RelativeLayout stdlayout;
    String studentid= "",sub_id= "", sub_name= "" ,sectionid="",sectionname="",classid = "",className = "";
    ArrayList<SubjectBeancls> subjectList = new ArrayList<>();
    AdapterOfSubjectList adaptersub;
    ArrayList<SectionBean> sectionList = new ArrayList<>();


    int month;
    int year;
    int day;
    int day_name;
    private Calendar mCalendar;
    private MonthAdapter madapter;
    private String start_date;
    private String end_date;
    private List<String> mItems;
    private final String[] mDays = {"Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"};
    private final int[] mDaysInMonth = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
    private int mDaysShown = 0;
    private int mDaysLastMonth=0;
    private int mDaysNextMonth=0;
    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view__sub_attendance);
        ButterKnife.bind(this);

        // txtTitle.setText("Attendance");
        // set animation on recyclerview
        int resId = R.anim.layout_animation_up_to_down;
        final LayoutAnimationController controller =
                AnimationUtils.loadLayoutAnimation(this, resId);
        recyclerView.setLayoutAnimation(controller);
        //----------------end--------------------
        animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.btn_blink_animation);
        animation1 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.logo_animation);
        animation2 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.enter_from_left_animation);
        animation3 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.enter_from_right_animation);
        top_curve_anim = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.fade_animation);


        if (!restorePrefData()){
            setShowcaseView();
        }else {
            openDialog();
        }
        // setCalendar();
    }



    private void setShowcaseViewP() {
        new TapTargetSequence(this)
                .targets(TapTarget.forView(btnBefore,"Previous Button!","Tap the Previous Button to view selected student previous month Attendance")
                        .outerCircleColor(R.color.light)
                        .outerCircleAlpha(0.96f)
                        .targetCircleColor(R.color.white)
                        .titleTextSize(20)
                        .titleTextColor(R.color.white)
                        .descriptionTextSize(14)
                        .descriptionTextColor(R.color.black)
                        .textColor(R.color.black)
                        .textTypeface(Typeface.SANS_SERIF)
                        .dimColor(R.color.black)
                        .drawShadow(true)
                        .cancelable(false)
                        .tintTarget(true)
                        .transparentTarget(true)
                        .targetRadius(60),TapTarget.forView(btnNext,"Next Button!","Tap the Next Button to view selected student next month Attendance.")
                        .outerCircleColor(R.color.light)
                        .outerCircleAlpha(0.96f)
                        .targetCircleColor(R.color.white)
                        .titleTextSize(20)
                        .titleTextColor(R.color.white)
                        .descriptionTextSize(14)
                        .descriptionTextColor(R.color.black)
                        .textColor(R.color.black)
                        .textTypeface(Typeface.SANS_SERIF)
                        .dimColor(R.color.black)
                        .drawShadow(true)
                        .cancelable(false)
                        .tintTarget(true)
                        .transparentTarget(true)
                        .targetRadius(60)).listener(new TapTargetSequence.Listener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onSequenceFinish() {
                // Toast.makeText(getApplicationContext(),"Sequence Finished",Toast.LENGTH_SHORT).show();
                savePrefDataP();
            }

            @Override
            public void onSequenceStep(TapTarget lastTarget, boolean targetClicked) {
                // Toast.makeText(getActivity(),"GREAT!",Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSequenceCanceled(TapTarget lastTarget) {

            }
        }).start();
    }



    private void setShowcaseView1() {
        new TapTargetSequence(this)
                .targets(TapTarget.forView(stdlayout,"Spinner Button!","Tap the Spinner Button and select Student to view Subject-Wise Attendance.")
                        .outerCircleColor(R.color.light)
                        .outerCircleAlpha(0.96f)
                        .targetCircleColor(R.color.white)
                        .titleTextSize(20)
                        .titleTextColor(R.color.white)
                        .descriptionTextSize(14)
                        .descriptionTextColor(R.color.black)
                        .textColor(R.color.black)
                        .textTypeface(Typeface.SANS_SERIF)
                        .dimColor(R.color.black)
                        .drawShadow(true)
                        .cancelable(true)
                        .tintTarget(true)
                        .transparentTarget(true)
                        .targetRadius(60)).listener(new TapTargetSequence.Listener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onSequenceFinish() {
                // Toast.makeText(getApplicationContext(),"Sequence Finished",Toast.LENGTH_SHORT).show();
                savePrefData1();
            }

            @Override
            public void onSequenceStep(TapTarget lastTarget, boolean targetClicked) {
                // Toast.makeText(getActivity(),"GREAT!",Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSequenceCanceled(TapTarget lastTarget) {

            }
        }).start();
    }


    private void setShowcaseView() {
        new TapTargetSequence(this)
                .targets(TapTarget.forView(dialog,"Search Button!","Tap the Search Button to Search Subject-Wise Students Attendance.")
                        .outerCircleColor(R.color.light)
                        .outerCircleAlpha(0.96f)
                        .targetCircleColor(R.color.white)
                        .titleTextSize(20)
                        .titleTextColor(R.color.white)
                        .descriptionTextSize(14)
                        .descriptionTextColor(R.color.black)
                        .textColor(R.color.black)
                        .textTypeface(Typeface.SANS_SERIF)
                        .dimColor(R.color.black)
                        .drawShadow(true)
                        .cancelable(false)
                        .tintTarget(true)
                        .transparentTarget(true)
                        .targetRadius(60)).listener(new TapTargetSequence.Listener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onSequenceFinish() {
                // Toast.makeText(getApplicationContext(),"Sequence Finished",Toast.LENGTH_SHORT).show();
                savePrefData();
                openDialog();
            }

            @Override
            public void onSequenceStep(TapTarget lastTarget, boolean targetClicked) {
                // Toast.makeText(getActivity(),"GREAT!",Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSequenceCanceled(TapTarget lastTarget) {

            }
        }).start();
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @OnClick(R.id.dialog)
    public void dialogggg() {
        openDialog();
    }

    private void savePrefData1(){
        sharedPreferences=this.getSharedPreferences("boarding_pref_view_attend1",MODE_PRIVATE);
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putBoolean("IsFirstTimeVisit_view_attend1",true);
        editor.apply();
    }

    private boolean restorePrefData1(){
        sharedPreferences = this.getSharedPreferences("boarding_pref_view_attend1",MODE_PRIVATE);
        return sharedPreferences.getBoolean("IsFirstTimeVisit_view_attend1",false);
    }


    private void savePrefData(){
        sharedPreferences=this.getSharedPreferences("boarding_pref_view_attend",MODE_PRIVATE);
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putBoolean("IsFirstTimeVisit_view_attend",true);
        editor.apply();
    }

    private boolean restorePrefData(){
        sharedPreferences = this.getSharedPreferences("boarding_pref_view_attend",MODE_PRIVATE);
        return sharedPreferences.getBoolean("IsFirstTimeVisit_view_attend",false);
    }

    private void savePrefDataP(){
        sharedPreferences=this.getSharedPreferences("boarding_pref_view_attendP",MODE_PRIVATE);
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putBoolean("IsFirstTimeVisit_view_attendP",true);
        editor.apply();
    }

    private boolean restorePrefDataP(){
        sharedPreferences = this.getSharedPreferences("boarding_pref_view_attendP",MODE_PRIVATE);
        return sharedPreferences.getBoolean("IsFirstTimeVisit_view_attendP",false);
    }


    @RequiresApi(api = Build.VERSION_CODES.N)
    private void openDialog() {

        mBottomSheetDialog = new BottomSheetDialog(this);
        final View sheetView = getLayoutInflater().inflate(R.layout.search_dialog, null);
        mBottomSheetDialog.setContentView(sheetView);
        mBottomSheetDialog.setCancelable(false);

        spinnerClass=(Spinner) sheetView.findViewById(R.id.spinnerClass);
        spinnersection=(Spinner) sheetView.findViewById(R.id.spinnersection);
        spinnersubject=(Spinner) sheetView.findViewById(R.id.spinnersubject);
        ImageView close=(ImageView) sheetView.findViewById(R.id.close);
        TextView txtSetup=(TextView) sheetView.findViewById(R.id.txtSetup);
        Button btnYes= (Button) sheetView.findViewById(R.id.btnYes);
        // getTodayDate();
        txtSetup.setText("View Subject-Wise \nAttendance");
        //btnYes.setText("View Subject-Wise \nAttendance");
        fetchClass();

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //assignData();
                if (check==0) {
                    finish();
                    mBottomSheetDialog.dismiss();
                }else {
                    mBottomSheetDialog.dismiss();
                }
            }
        });
        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (classid.equalsIgnoreCase(""))  {
                    Toast.makeText(View_SubAttendance.this,"Kindly Select Class!", Toast.LENGTH_SHORT).show();
                }  else if (sectionid.equalsIgnoreCase("")){
                    Toast.makeText(View_SubAttendance.this,"Kindly Select Class Section!", Toast.LENGTH_SHORT).show();
                }else if (sub_id.equalsIgnoreCase("")){
                    Toast.makeText(View_SubAttendance.this,"Kindly Select Class Subject!", Toast.LENGTH_SHORT).show();
                }else {
                    check++;

                    if (!restorePrefData1()){
                        setShowcaseView1();
                    }
                    savePrefData1();
//                    searchStudent(date);
                    fetchStudent(classid,sectionid,sub_id);
                    setCommonData();
                    // mBottomSheetDialog.dismiss();
                }

            }
        });
        mBottomSheetDialog.show();

    }


    private void fetchClass() {
        if (loaded == 0) {
            ErpProgress.showProgressBar(this, "Please wait...");
        }
        loaded++;

        HashMap<String, String> params = new HashMap<String, String>();
      //  params.put("check","true");
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.CLASSLIST_URL, classapiCallback,getApplicationContext(), params);
    }


    ApiHandler.ApiCallback classapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            if (error == null) {
                try {
                    classList = ClassBean.parseClassArray(jsonObject.getJSONArray(Constants.CLASSDATA));
                    ClassAdapterAttend adapter = new ClassAdapterAttend(getApplicationContext(), classList,0);
                    spinnerClass.setAdapter(adapter);
                    spinnerClass.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if (i > 0) {
                                sub_id= "";
                                sub_name= "" ;
                                sectionid="";
                                sectionname="";
                                classid = "";
                                className = "";
                                classid = classList.get(i - 1).getId();
                                className= classList.get(i - 1).getName();
                                fetchSection();
                                fetchSubject();
                            }

                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_LONG).show();
                // Utils.showSnackBar(error.getMessage(), view.findViewById(R.id.parent));
            }
        }
    };


    private void fetchSection() {
        HashMap<String, String> params = new HashMap<String, String>();
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.SECTIONLIST_URL, sectionapiCallback, this, params);
    }

    ApiHandler.ApiCallback sectionapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            if (error == null) {
                try {
                    sectionList = SectionBean.parseCommonArray(jsonObject.getJSONArray("hostelroom_data"));
                    SectionnewAdapter adapter = new SectionnewAdapter(View_SubAttendance.this, sectionList);
                    spinnersection.setAdapter(adapter);
                    spinnersection.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if (i > 0) {
                                sectionid = sectionList.get(i - 1).getSection_id();
                                sectionname = sectionList.get(i - 1).getSection_name();
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Utils.showSnackBar(error.getMessage(), parent);
                if (error.getStatusCode() == 405) {
                    User.logout();
                    startActivity(new Intent(View_SubAttendance.this, LoginActivity.class));
                    finish();
                }
            }
        }
    };


    private void fetchSubject() {
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("id", classid);
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.SubjectList, subapiCallback, this, params);

    }

    ApiHandler.ApiCallback subapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            /* */

            if (error == null) {
                try {

                    subjectList =   SubjectBeancls.parseClassArray(jsonObject.getJSONArray("sub_data"));

                    adaptersub = new AdapterOfSubjectList(View_SubAttendance.this, subjectList);
                    spinnersubject.setAdapter(adaptersub);
                    spinnersubject.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if (i != 0) {
                                sub_name = subjectList.get(i - 1).getName();
                                sub_id = subjectList.get(i - 1).getId();
                            }

                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    };

    private void fetchStudent(String classid, String sectionid,String sub_id) {

        HashMap<String, String> params = new HashMap<String, String>();
        params.put("class_id",classid);
        params.put("section_id",sectionid);
//        Log.e("class_id",classid);
//        Log.e("section_id",sectionid);
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.STUDENTLISTCLSSECWISE_URL, apiCallbackstudnt, getApplicationContext(), params);
    }
    private ArrayList<Studentbean> stuList;
    private Spinner_stu_adapterGds adapterstu;
    ApiHandler.ApiCallback apiCallbackstudnt = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            mBottomSheetDialog.dismiss();
            if (error == null) {
                try {
                    stuList = Studentbean.parseHWArray(jsonObject.getJSONArray("student_list"));
                    adapterstu = new Spinner_stu_adapterGds(getApplicationContext(), stuList);
                    spinnerstudnt.setAdapter(adapterstu);
                    spinnerstudnt.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if (i > 0) {
                                studentid = stuList.get(i - 1).getId();
                                //  lytCalender.setVisibility(View.VISIBLE);
                                setCalendar();

                            } else {
                                //lytCalender.setVisibility(View.GONE);
                                studentid = "";
                            }

                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {

            }
        }
    };

    private void setCommonData() {
        attendlayout.setVisibility(View.GONE);
        txtTitle.setText(className+"("+sectionname+")");
        txtSub.setText(sub_name);
    }
    public void setCalendar() {
        // callayout.setVisibility(View.VISIBLE);
        mCalendar = Calendar.getInstance();

        day = mCalendar.get(Calendar.DATE);
        day_name = mCalendar.get(Calendar.DAY_OF_WEEK);
        String weekday = new DateFormatSymbols().getWeekdays()[day_name];
        h_currentday.setText(String.valueOf(day));
        h_day_name.setText(weekday);
        SimpleDateFormat month_date = new SimpleDateFormat("MMMM");
        String month_name = month_date.format(mCalendar.getTime());
        h_currentMonth.setText(month_name);
        month = mCalendar.get(Calendar.MONTH); // zero based

        year = mCalendar.get(Calendar.YEAR);

        recyclerView.setLayoutManager(new GridLayoutManager(getApplicationContext(), 7));
        recyclerView.setHasFixedSize(false);

        getDateList(month, year);

        currentMonth.setText(Util.getMonth(month + 1) + " " + String.valueOf(year));
        String setmonth = (month+1)+"";
        if (month + 1 < 10) {
            setmonth = "0" + (month + 1);
        }
        start_date = year + "-" + setmonth + "-01";
        // end_date = year + "-" + setmonth + "-" + Utils.getMaxDateByMonth(month + 1);
        GregorianCalendar gcal = new GregorianCalendar();
        if(gcal.isLeapYear(year)){
            end_date = year + "-" + setmonth + "-" + Utils.getMaxDateByMonthleap(month + 1);
        }else{
            end_date = year + "-" + setmonth + "-" + Utils.getMaxDateByMonth(month + 1);
        }
        fetchAttedance();
    }

    private void fetchAttedance() {
        setCommonDataAnimation();
        HashMap<String, String> params = new HashMap<String, String>();
        params.put(Constants.ID, studentid);
        params.put("start_date", start_date);
        params.put("end_date", end_date);
        params.put("sub_id", sub_id);
        params.put("tag", "college");
        Log.e("start_date",start_date);
        Log.e("end_date",end_date);
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.STUATTENDANCE_URL, apiCallback, this, params);

    }

    private void setCommonDataAnimation() {

        if (!restorePrefDataP()){
            setShowcaseViewP();
        }


        attendlayout.setVisibility(View.VISIBLE);
        h_day_name.startAnimation(animation3);
        h_currentMonth.startAnimation(animation3);
        lytCard.startAnimation(animation2);
        lytCard1.startAnimation(animation3);
        h_currentday.startAnimation(top_curve_anim);
        currentMonth.startAnimation(top_curve_anim);
        h_currentMonth.startAnimation(top_curve_anim);
        h_day_name.startAnimation(top_curve_anim);
        h_calender.startAnimation(animation1);
    }

    ApiHandler.ApiCallback apiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            if (error == null) {
                try {

                    if (attendList!=null) {
                        attendList.clear();
                    }

                    attendList = AttendMod.parseAttendArray(jsonObject.getJSONArray("attend_data"));
                    if (attendList.get(0).getPr() != null) {
                        noofpresent.setText(attendList.get(0).getPr()+"d");
                    } else {
                        noofpresent.setText("0d");
                    }
                    if (attendList.get(0).getAb() != null) {
                        noofabsent.setText(attendList.get(0).getAb()+"d");
                    } else {
                        noofabsent.setText("0d");
                    }
                    if (attendList.get(0).getLv() != null) {
                        noofleave.setText(attendList.get(0).getLv()+"d");
                    } else {
                        noofleave.setText("0d");
                    }
                    if (attendList.get(0).getHl() != null) {
                        noofholydy.setText(attendList.get(0).getHl()+"d");
                    } else {
                        noofholydy.setText("0d");
                    }
//                    if (User.getCurrentUser().getSchoolData().getName().equalsIgnoreCase("Global Discovery School")) {
//                        holidylayout.setVisibility(View.VISIBLE);
//                        if (attendList.get(0).getHl() != null) {
//                            noofholydy.setText(attendList.get(0).getHl());
//                        } else {
//                            noofholydy.setText("0");
//                        }
//                    }else{
//                        holidylayout.setVisibility(View.GONE);
//                    }

                    //               if (User.getCurrentUser().getSchoolData().getName().equalsIgnoreCase("Global Discovery School")){
                    attenddetailpercentlay.setVisibility(View.GONE);
                    if (attendList.get(0).getTwcount() != null) {
                        totalwork.setText("Total Working Day: " + attendList.get(0).getTwcount());
                    } else {
                        totalwork.setText("0");
                    }
                    if (attendList.get(0).getTprcount() != null) {
                        totalpresent.setText("Total Annual Present Day: " + attendList.get(0).getTprcount());

                    } else {
                        totalpresent.setText("0");

                    }
                    if(attendList.get(0).getTotalworkcountwoh()!=null){
                        totalannualworkd.setText("Total Annual Working Day: "+attendList.get(0).getTotalworkcountwoh());
                    }else{
                        totalannualworkd.setText("0");
                    }

                    if (attendList.get(0).getPrtcont() != null) {
                        annualpercent.setText("Annual(%) Till Current Date: " + attendList.get(0).getPrtcont());
                    } else {
                        annualpercent.setText("0");
                    }
                    //   }else{
//                        attenddetailpercentlay.setVisibility(View.GONE);
//                    }
                    madapter.setAttendList(attendList);
//                    madapter.notifyDataSetChanged();
                    //setanimation on adapter...
                    recyclerView.getAdapter().notifyDataSetChanged();
                    recyclerView.scheduleLayoutAnimation();
                    lytCard.startAnimation(animation2);
                    lytCard1.startAnimation(animation3);
                    //-----------end------------
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                attendList.clear();
                madapter.setAttendList(attendList);
                // madapter.notifyDataSetChanged();
                //setanimation on adapter...
                recyclerView.getAdapter().notifyDataSetChanged();
                recyclerView.scheduleLayoutAnimation();
                //-----------end------------
                lytCard.startAnimation(animation2);
                lytCard1.startAnimation(animation3);
                setZero(error);
                //   noofholydy.setText("0");
                if (error.getStatusCode() != 401) {
                    //  Utils.showSnackBar(error.getMessage(), parent);
                    setZero(error);
                }
                if (error.getStatusCode() == 405) {
                    User.logout();
                    startActivity(new Intent(View_SubAttendance.this, LoginActivity.class));
                    finish();
                }
            }
        }
    };


    @OnClick(R.id.imgBack)
    public void onBackclick() {
        back.startAnimation(animation);
        finish();
    }


    private void setZero(ApiHandlerError error) {
        noofholydy.setText("0");
        noofleave.setText("0");
        noofabsent.setText("0");
        noofpresent.setText("0");
        Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_SHORT).show();
    }

    public void getDateList(int mMonth, int mYear) {
        mDaysLastMonth=0;
        mDaysNextMonth=0;
        mDaysShown =0;
        GregorianCalendar gCal = new GregorianCalendar(mYear, mMonth, 1);
        ArrayList<String> mItems = new ArrayList<String>();
        for (String day : mDays) {
            mItems.add(day);
            mDaysShown++;
        }

        int firstDay = getDay(gCal.get(Calendar.DAY_OF_WEEK));
        int prevDay;
        if (mMonth == 0) {
            mMonth = 11;
        }
        prevDay = daysInMonth(mMonth - 1, gCal, mYear) - firstDay + 1;



//        if (mMonth == 0)
//            prevDay = daysInMonth(11, gCal, mYear) - firstDay + 1;
//        else
//            prevDay = daysInMonth(mMonth - 1, gCal, mYear) - firstDay + 1;

        for (int i = 0; i < firstDay; i++) {
            mItems.add(String.valueOf(prevDay + i));
            mDaysLastMonth++;
            mDaysShown++;
        }

        int daysInMonth = daysInMonth(mMonth, gCal, mYear);
        for (int i = 1; i <= daysInMonth; i++) {
            mItems.add(String.valueOf(i));
            mDaysShown++;
        }

        mDaysNextMonth = 1;
        while (mDaysShown % 7 != 0) {
            mItems.add(String.valueOf(mDaysNextMonth));
            mDaysShown++;
            mDaysNextMonth++;
        }
        if (madapter == null) {
            madapter = new MonthAdapter(this, attendList, mItems, month, year, mDaysLastMonth, mDaysNextMonth,mDaysShown,1);
            recyclerView.setAdapter(madapter);
        }else{
            madapter.setDateList( mItems, month, year, mDaysLastMonth, mDaysNextMonth,mDaysShown);
            madapter.notifyDataSetChanged();
        }
    }

    @OnClick(R.id.btnNext)
    public void onClickNext() {
        btnNext.startAnimation(animation);

        month = month + 1;

        if (month > 11) {
            month = 0;
            year = year + 1;
        }

        currentMonth.setText(Util.getMonth(month + 1) + " " + String.valueOf(year));
        mCalendar.set(Calendar.MONTH, month);
        getDateList(month, year);

        String setmonth = ""+(month + 1);
        if (month + 1 < 10) {
            setmonth = "0" + (month + 1);
        }
        start_date = year + "-" + setmonth + "-01";
        // end_date = year + "-" + setmonth + "-" + Utils.getMaxDateByMonth(month + 1);
        GregorianCalendar gcal = new GregorianCalendar();
        if(gcal.isLeapYear(year)){
            end_date = year + "-" + setmonth + "-" + Utils.getMaxDateByMonthleap(month + 1);
        }else{
            end_date = year + "-" + setmonth + "-" + Utils.getMaxDateByMonth(month + 1);
        }
        fetchAttedance();
    }

    @OnClick(R.id.btnBefore)
    public void onClickBefore() {
        btnBefore.startAnimation(animation);
        month = month - 1;

        if (month < 0) {
            month = 11;
            year = year - 1;
        }
        mCalendar.set(Calendar.MONTH, month);


        currentMonth.setText(Util.getMonth(month + 1) + " " + String.valueOf(year));
        getDateList(month, year);


        String setmonth = ""+(month + 1);
        if (month + 1 < 10) {
            setmonth = "0" + (month + 1);
        }
        start_date = year + "-" + setmonth + "-01";
        // end_date = year + "-" + setmonth + "-" + Utils.getMaxDateByMonth(month + 1);
        GregorianCalendar gcal = new GregorianCalendar();
        if(gcal.isLeapYear(year)){
            end_date = year + "-" + setmonth + "-" + Utils.getMaxDateByMonthleap(month + 1);
        }else{
            end_date = year + "-" + setmonth + "-" + Utils.getMaxDateByMonth(month + 1);
        }
        //end_date = year + "-" + setmonth + "-" + mCalendar.getActualMaximum(Calendar.DAY_OF_MONTH);
        fetchAttedance();
    }

    private int daysInMonth(int month, GregorianCalendar gCal, int mYear) {
        int daysInMonth = mDaysInMonth[month];
        if (month == 1 && gCal.isLeapYear(mYear))
            daysInMonth++;
        return daysInMonth;
    }


    private int getDay(int day) {
        switch (day) {
            case Calendar.MONDAY:
                return 0;
            case Calendar.TUESDAY:
                return 1;
            case Calendar.WEDNESDAY:
                return 2;
            case Calendar.THURSDAY:
                return 3;
            case Calendar.FRIDAY:
                return 4;
            case Calendar.SATURDAY:
                return 5;
            case Calendar.SUNDAY:
                return 6;
            default:
                return 0;
        }
    }


}