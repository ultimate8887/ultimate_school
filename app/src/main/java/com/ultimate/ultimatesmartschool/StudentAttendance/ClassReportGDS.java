package com.ultimate.ultimatesmartschool.StudentAttendance;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetSequence;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.ultimate.ultimatesmartschool.BeanModule.ClassBean;
import com.ultimate.ultimatesmartschool.BeanModule.SectionBean;
import com.ultimate.ultimatesmartschool.BeanModule.SubjectBeancls;
import com.ultimate.ultimatesmartschool.BeanModule.User;
import com.ultimate.ultimatesmartschool.Homework.AdapterOfSubjectList;
import com.ultimate.ultimatesmartschool.Homework.SectionnewAdapter;
import com.ultimate.ultimatesmartschool.Login.LoginActivity;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.CommonProgress;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ClassReportGDS  extends AppCompatActivity {


    @BindView(R.id.export)
    FloatingActionButton export;
    int selected = 1;
    SharedPreferences sharedPreferences;
    ArrayList<ClassBean> classList = new ArrayList<>();
    @BindView(R.id.parent)
    RelativeLayout parent;
    //@BindView(R.id.spinner) Spinner spinnerClass;
    BottomSheetDialog mBottomSheetDialog;
    private int loaded = 0;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.txtSub)
    TextView txtSub;
    List<String> classidsel;
    @BindView(R.id.totalRecord)
    TextView totalRecord;

    ArrayList<SectionBean> sectionList = new ArrayList<>();

    @BindView(R.id.adddate)TextView adddate;
    public String date;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.textNorecord)
    TextView txtNorecord;
    private ArrayList<AttendBean> dayList;
    private ClassReportAdapter adapter;
//    @BindView(R.id.btnSubmit)
//    Button btnSubmit;

    @BindView(R.id.dialog)
    ImageView dialog;
    @BindView(R.id.sc)
    CardView scrollView;
    @BindView(R.id.filelayout)
    LinearLayout filelayout;

    Spinner spinnersection;
    Spinner spinnerClass;
    Spinner spinnersubject;

    ArrayList<SubjectBeancls> subjectList = new ArrayList<>();
    AdapterOfSubjectList adaptersub;
    String sub_id= "", sub_name= "" ,sectionid="",sectionname="",classid = "",className = "";

    int check=0;

    CommonProgress commonProgress;
    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_class_reportgds);
        ButterKnife.bind(this);
//        fetchClass();
//        fetchCurrentDay();
//        fetchsection();
//        txtTitle.setText("DayWise Report");
        commonProgress=new CommonProgress(this);
        classList = new ArrayList<>();
        classidsel = new ArrayList<>();
        dayList = new ArrayList<>();
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new ClassReportAdapter(this, dayList);
        recyclerView.setAdapter(adapter);
        if (!restorePrefData()){
            setShowcaseView();
        }else {
            openDialog();
        }
    }

    private void setShowcaseViewP() {
        new TapTargetSequence(this)
                .targets(TapTarget.forView(export,"Export Excel!","Tap the Export button to export date wise attendance report.")
                        .outerCircleColor(R.color.light)
                        .outerCircleAlpha(0.96f)
                        .targetCircleColor(R.color.white)
                        .titleTextSize(20)
                        .titleTextColor(R.color.white)
                        .descriptionTextSize(14)
                        .descriptionTextColor(R.color.black)
                        .textColor(R.color.black)
                        .textTypeface(Typeface.SANS_SERIF)
                        .dimColor(R.color.black)
                        .drawShadow(true)
                        .cancelable(false)
                        .tintTarget(true)
                        .transparentTarget(true)
                        .targetRadius(40)).listener(new TapTargetSequence.Listener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onSequenceFinish() {
                // Toast.makeText(getApplicationContext(),"Sequence Finished",Toast.LENGTH_SHORT).show();
                savePrefDataP();

            }

            @Override
            public void onSequenceStep(TapTarget lastTarget, boolean targetClicked) {
                // Toast.makeText(getActivity(),"GREAT!",Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSequenceCanceled(TapTarget lastTarget) {

            }
        }).start();
    }

    private void savePrefDataP(){
        sharedPreferences=this.getSharedPreferences("boarding_pref_view_export",MODE_PRIVATE);
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putBoolean("IsFirstTimeVisit_view_export",true);
        editor.apply();
    }

    private boolean restorePrefDataP(){
        sharedPreferences = this.getSharedPreferences("boarding_pref_view_export",MODE_PRIVATE);
        return sharedPreferences.getBoolean("IsFirstTimeVisit_view_export",false);
    }

    private void setShowcaseView_aday() {
        new TapTargetSequence(this)
                .targets(TapTarget.forView(filelayout,"Date Button!","Tap the Date button to View Day-Wise Attendance.")
                        .outerCircleColor(R.color.light)
                        .outerCircleAlpha(0.96f)
                        .targetCircleColor(R.color.white)
                        .titleTextSize(20)
                        .titleTextColor(R.color.white)
                        .descriptionTextSize(14)
                        .descriptionTextColor(R.color.black)
                        .textColor(R.color.black)
                        .textTypeface(Typeface.SANS_SERIF)
                        .dimColor(R.color.black)
                        .drawShadow(true)
                        .cancelable(false)
                        .tintTarget(true)
                        .transparentTarget(true)
                        .targetRadius(60)).listener(new TapTargetSequence.Listener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onSequenceFinish() {
                // Toast.makeText(getApplicationContext(),"Sequence Finished",Toast.LENGTH_SHORT).show();
                savePrefData_aday();
                //openDialog();
            }

            @Override
            public void onSequenceStep(TapTarget lastTarget, boolean targetClicked) {
                // Toast.makeText(getActivity(),"GREAT!",Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSequenceCanceled(TapTarget lastTarget) {

            }
        }).start();
    }

    private void savePrefData_aday(){
        sharedPreferences=this.getSharedPreferences("boarding_pref_add_attend_vday",MODE_PRIVATE);
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putBoolean("IsFirstTimeVisit_add_attend_vday",true);
        editor.apply();
    }

    private boolean restorePrefData_aday(){
        sharedPreferences = this.getSharedPreferences("boarding_pref_add_attend_vday",MODE_PRIVATE);
        return sharedPreferences.getBoolean("IsFirstTimeVisit_add_attend_vday",false);
    }


    private void setShowcaseView() {
        new TapTargetSequence(this)
                .targets(TapTarget.forView(dialog,"Search Button!","Tap the Search button to to view Class-Wise Attendance Report.")
                        .outerCircleColor(R.color.light)
                        .outerCircleAlpha(0.96f)
                        .targetCircleColor(R.color.white)
                        .titleTextSize(20)
                        .titleTextColor(R.color.white)
                        .descriptionTextSize(14)
                        .descriptionTextColor(R.color.black)
                        .textColor(R.color.black)
                        .textTypeface(Typeface.SANS_SERIF)
                        .dimColor(R.color.black)
                        .drawShadow(true)
                        .cancelable(false)
                        .tintTarget(true)
                        .transparentTarget(true)
                        .targetRadius(60)).listener(new TapTargetSequence.Listener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onSequenceFinish() {
                // Toast.makeText(getApplicationContext(),"Sequence Finished",Toast.LENGTH_SHORT).show();
                savePrefData();
                openDialog();
            }

            @Override
            public void onSequenceStep(TapTarget lastTarget, boolean targetClicked) {
                // Toast.makeText(getActivity(),"GREAT!",Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSequenceCanceled(TapTarget lastTarget) {

            }
        }).start();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        commonBack();
    }

    private void commonBack() {
        androidx.appcompat.app.AlertDialog.Builder builder1 = new androidx.appcompat.app.AlertDialog.Builder(ClassReportGDS.this);
        builder1.setMessage("Are you sure, you want to exit? ");
        builder1.setCancelable(false);
        builder1.setPositiveButton(
                "Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finish();
                    }
                });
        builder1.setNegativeButton(
                "No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

    private void savePrefData(){
        sharedPreferences=this.getSharedPreferences("boarding_pref_add_attend_day",MODE_PRIVATE);
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putBoolean("IsFirstTimeVisit_add_attend_day",true);
        editor.apply();
    }

    private boolean restorePrefData(){
        sharedPreferences = this.getSharedPreferences("boarding_pref_add_attend_day",MODE_PRIVATE);
        return sharedPreferences.getBoolean("IsFirstTimeVisit_add_attend_day",false);
    }


    @RequiresApi(api = Build.VERSION_CODES.N)
    private void openDialog() {

        mBottomSheetDialog = new BottomSheetDialog(this);
        final View sheetView = getLayoutInflater().inflate(R.layout.search_dialog, null);
        mBottomSheetDialog.setContentView(sheetView);
        mBottomSheetDialog.setCancelable(false);

        spinnerClass=(Spinner) sheetView.findViewById(R.id.spinnerClass);
        spinnersection=(Spinner) sheetView.findViewById(R.id.spinnersection);
        spinnersubject=(Spinner) sheetView.findViewById(R.id.spinnersubject);
        ImageView close=(ImageView) sheetView.findViewById(R.id.close);
        TextView txtSetup=(TextView) sheetView.findViewById(R.id.txtSetup);

        txtSetup.setText("View Day-Wise \nAttendance");

        getTodayDate();
        //txtTitle.setText("Mark Attendance");
        fetchClass();

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //assignData();
                if (check==0) {
                    finish();
                    mBottomSheetDialog.dismiss();
                }else {
                    mBottomSheetDialog.dismiss();
                }
            }
        });


        Button btnYes= (Button) sheetView.findViewById(R.id.btnYes);


        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (classid.equalsIgnoreCase(""))  {
                    Toast.makeText(ClassReportGDS.this,"Kindly Select Class!", Toast.LENGTH_SHORT).show();
                }  else if (sectionid.equalsIgnoreCase("")){
                    Toast.makeText(ClassReportGDS.this,"Kindly Select Class Section!", Toast.LENGTH_SHORT).show();
                }else {
                    check++;
                    fetchStudentList(date);
                    setCommonData();
                }

            }
        });
        mBottomSheetDialog.show();

    }

    public void getTodayDate() {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.HOUR_OF_DAY, 00);
        c.set(Calendar.MINUTE, 00);
        c.set(Calendar.SECOND, 00);
        java.util.Date setdate = c.getTime();
        SimpleDateFormat fmtOut = new SimpleDateFormat("dd MMM, yyyy");
        String dateString = fmtOut.format(setdate);
        adddate.setText(dateString);
        SimpleDateFormat dateFrmOut = new SimpleDateFormat("yyyy-MM-dd");
        date = dateFrmOut.format(setdate);
    }

    private void setCommonData() {
        filelayout.setVisibility(View.VISIBLE);
        if (!restorePrefData_aday()){
            setShowcaseView_aday();
        }
        txtTitle.setText(className+"("+sectionname+")");
        txtSub.setText(sub_name);
    }

    @SuppressLint("RestrictedApi")
    @OnClick(R.id.export)
    public void submit() {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.S) {
            importData();
        } else {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {

                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    Toast.makeText(this, "Please provide storage permission from app settings", Toast.LENGTH_LONG).show();
                } else {

                    ActivityCompat.requestPermissions(this,
                            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            1);
                }
            } else {
                importData();

            }
        }
    }

    @SuppressLint("RestrictedApi")
    private void importData() {
        // File filePath = new File(Environment.getExternalStorageDirectory() + "/Demo.xls");
        Workbook wb = new HSSFWorkbook();

        Cell cell = null;

        String ndate="",sub_staff="",sub_date="";
        ndate= Utils.getDateOnlyNEW(date)+","+Utils.getMonthFormated(date);
        sub_date=ndate.substring(0,3);


        if (className.equalsIgnoreCase("")){
            sub_staff="All_Classes";
        }else {
            sub_staff=className+"_"+sectionname;
        }

        Sheet sheet = null;
        sheet = wb.createSheet(adddate.getText().toString()+" Attendance Report");
        //Now column and row
        Row row = sheet.createRow(0);

        cell = row.createCell(0);
        cell.setCellValue("S No");

        cell = row.createCell(1);
        cell.setCellValue("Roll No");

        cell = row.createCell(2);
        cell.setCellValue("Class/Section");

        cell = row.createCell(3);
        cell.setCellValue("Student Name");

        cell = row.createCell(4);
        cell.setCellValue("Father Name");

        cell = row.createCell(5);
        cell.setCellValue("Attendance");


        cell = row.createCell(6);
        cell.setCellValue("Date");


        //column width
        sheet.setColumnWidth(0, (20 * 100));
        sheet.setColumnWidth(1, (20 * 150));
        sheet.setColumnWidth(2, (30 * 150));
        sheet.setColumnWidth(3, (30 * 250));
        sheet.setColumnWidth(4, (30 * 200));
        sheet.setColumnWidth(5, (20 * 200));
        sheet.setColumnWidth(6, (30 * 200));

        for (int i = 0; i < dayList.size(); i++) {

            Row row1 = sheet.createRow(i + 1);

            cell = row1.createCell(0);
            cell.setCellValue(i+1);

            cell = row1.createCell(1);
            cell.setCellValue(dayList.get(i).getStudent_id());

            cell = row1.createCell(2);
            cell.setCellValue(className+" ("+sectionname+")");

            cell = row1.createCell(3);
            cell.setCellValue((dayList.get(i).getStudent_name()));
            //  cell.setCellStyle(cellStyle);

            cell = row1.createCell(4);
            cell.setCellValue(dayList.get(i).getFather_name());


            String attend="N/A";
            if(dayList.get(i).getAttendance().equalsIgnoreCase("a")){
                attend ="Absent";
            }else if(dayList.get(i).getAttendance().equalsIgnoreCase("p")){
                attend ="Present";
            }else if(dayList.get(i).getAttendance().equalsIgnoreCase("l")){
                if(dayList.get(i).getL_remarks()!=null) {
                    attend="Leave(HL)";
                }else {
                    attend="Leave";
                }
            }else {
                attend="N/A";
            }

            cell = row1.createCell(5);
            cell.setCellValue(attend);

            cell = row1.createCell(6);
            cell.setCellValue(ndate);


            sheet.setColumnWidth(0, (20 * 100));
            sheet.setColumnWidth(1, (20 * 150));
            sheet.setColumnWidth(2, (30 * 150));
            sheet.setColumnWidth(3, (30 * 250));
            sheet.setColumnWidth(4, (30 * 200));
            sheet.setColumnWidth(5, (20 * 200));
            sheet.setColumnWidth(6, (30 * 200));




        }
        String fileName;
        if (sub_staff.equalsIgnoreCase("")){
            fileName = sub_date+"_" +System.currentTimeMillis() + ".xls";
        }else {
            fileName = sub_staff+"_"+Utils.getDateOnlyNEW(date)+"_"+Utils.getMonthFormated(date)+"_" + System.currentTimeMillis() + ".xls";
        }


        // File file = new File(getActivity().getExternalFilesDir(null), fileName);

        File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS), fileName);

        FileOutputStream fileOutputStream = null;

        try {
            fileOutputStream = new FileOutputStream(file);
            wb.write(fileOutputStream);
            Utils.openSuccessDialog("Success!","Export file in "+file,"",this);
        } catch (IOException e) {
            Utils.openErrorDialog("Error writing Exception: "+ e,this);
        } catch (Exception e) {
            Utils.openErrorDialog("Failed to save file due to Exception: "+ e,this);
        } finally {
            try {
                if (null != fileOutputStream) {
                    fileOutputStream.close();
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

    }

    @SuppressLint("RestrictedApi")
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == 1 && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            importData();
        } else {
            Toast.makeText(getApplicationContext(), "Permission Denied", Toast.LENGTH_SHORT).show();
        }
    }

    @OnClick(R.id.imgBack)
    public void onBackclick() {
        commonBack();
    }

    private void fetchsection() {
        HashMap<String, String> params = new HashMap<String, String>();
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.SECTIONLIST_URL, sectionapiCallback, getApplicationContext(), params);
    }

    ApiHandler.ApiCallback sectionapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            if (error == null) {
                try {
                    sectionList = SectionBean.parseCommonArray(jsonObject.getJSONArray("hostelroom_data"));
                    SectionnewAdapter adapter = new SectionnewAdapter(getApplicationContext(), sectionList);
                    spinnersection.setAdapter(adapter);
                    spinnersection.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if (i > 0) {
                                sectionid = sectionList.get(i - 1).getSection_id();
                                sectionname=sectionList.get(i - 1).getSection_name();
                                //  fetchStudentList(date);
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_LONG).show();
                // Utils.showSnackBar(error.getMessage(), parent);
                if (error.getStatusCode() == 405) {
                    User.logout();
                    startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                    finish();
                }
            }
        }
    };


    private void fetchClass() {
        // ErpProgress.showProgressBar(getActivity(), "Please wait...");
        commonProgress.show();
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("check","inch");
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.CLASSLIST_URL, classapiCallback, getApplicationContext(), params);
    }



    ApiHandler.ApiCallback classapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            commonProgress.dismiss();
            if (error == null) {
                try {
                    classList = ClassBean.parseClassArray(jsonObject.getJSONArray(Constants.CLASSDATA));
                    ClassAdapterAttend adapter = new ClassAdapterAttend(getApplicationContext(), classList,0);
                    spinnerClass.setAdapter(adapter);
                    spinnerClass.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if (i > 0) {
                                sub_id= "";
                                sub_name= "" ;
                                sectionid="";
                                sectionname="";
                                classid = "";
                                className = "";
                                classid = classList.get(i - 1).getId();
                                className= classList.get(i - 1).getName();
                                fetchsection();
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {
                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_LONG).show();
                //  Utils.showSnackBar(error.getMessage(), view.findViewById(R.id.parent));
            }
        }
    };

    @RequiresApi(api = Build.VERSION_CODES.N)
    @OnClick(R.id.dialog)
    public void dialogggg() {
        openDialog();
    }

    @OnClick(R.id.filelayout)
    public void fetchDate() {
        java.util.Calendar cal = java.util.Calendar.getInstance(TimeZone.getDefault()); // Get current date
        DatePickerDialog datePicker = new DatePickerDialog(this,
                R.style.MyDatePickerDialogTheme, ondate,
                cal.get(java.util.Calendar.YEAR),
                cal.get(java.util.Calendar.MONTH),
                cal.get(java.util.Calendar.DAY_OF_MONTH));
        datePicker.getDatePicker().setMaxDate(new java.util.Date().getTime());
        datePicker.setCancelable(false);
        datePicker.show();
    }

    DatePickerDialog.OnDateSetListener ondate = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            java.util.Calendar calendar = java.util.Calendar.getInstance(TimeZone.getDefault());
            calendar.set(year, monthOfYear, dayOfMonth);
            calendar.set(Calendar.HOUR_OF_DAY, 00);
            calendar.set(Calendar.MINUTE, 00);
            calendar.set(Calendar.SECOND, 00);
            java.util.Date setdate = calendar.getTime();
            SimpleDateFormat fmtOut = new SimpleDateFormat("dd MMM, yyyy");
            String dateString = fmtOut.format(setdate);
            adddate.setText(dateString);
            SimpleDateFormat dateFrmOut = new SimpleDateFormat("yyyy-MM-dd");
            date = dateFrmOut.format(setdate);
            fetchStudentList(date);
        }
    };


    private void fetchStudentList(String date) {
        commonProgress.show();
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("class_id", classid);
        if (date != null) {
            params.put("mdate", String.valueOf(date));
        }
        params.put("section",sectionid);
        params.put("type", selected + "");
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.STUDENT_ATTEND_LISTGDS, studentapiCallback, getApplicationContext(), params);
    }

    ApiHandler.ApiCallback studentapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            commonProgress.dismiss();
            mBottomSheetDialog.dismiss();
            if (error == null) {
                try {
//                    if(selected==1){
                    ArrayList<AttendBean> attendList = AttendBean.parseAttendArray(jsonObject.getJSONArray(Constants.ATTEND_DATA));
                    dayList.clear();
                    dayList.addAll(attendList);
                    adapter.notifyDataSetChanged();
                    totalRecord.setText("Total Entries:- "+String.valueOf(attendList.size()));
                    scrollView.setVisibility(View.VISIBLE);
                    export.setVisibility(View.VISIBLE);
                    if (!restorePrefDataP()){
                        setShowcaseViewP();
                    }
//                    }else{
//                        ArrayList<AttendMonthBean> attendList = AttendMonthBean.parseAttendArray(jsonObject.getJSONArray(Constants.ATTEND_DATA));
//                        monList.clear();
//                        monList.addAll(attendList);
//                        madapter.notifyDataSetChanged();
//                        totalRecord.setText("Total Entries:- "+String.valueOf(attendList.size()));
//                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                export.setVisibility(View.GONE);
                scrollView.setVisibility(View.GONE);
                totalRecord.setText("Total Entries:- 0");
                //  Utils.showSnackBar(error.getMessage(), view.findViewById(R.id.parent));
                Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_LONG).show();
            }
        }
    };


}
