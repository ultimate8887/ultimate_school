package com.ultimate.ultimatesmartschool.StudentAttendance;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class AttendMonthBean {



    private static String ID = "id";
    private static String STUDENT_ID = "student_id";
    private static String STUDENT_NAME = "student_name";

    /**
     * id : 2
     * student_id : 8
     * student_name : tjytuyt
     * attendance : P
     */
    private String id;
    private String student_id;
    private String student_name;
    /**
     * id : 0
     * p : -
     * l : -
     * a : -
     */
    private String pre_serialno;

    private String hl;
    private String sal;
    private String cl;

    public String getHl() {
        return hl;
    }

    public void setHl(String hl) {
        this.hl = hl;
    }

    public String getSal() {
        return sal;
    }

    public void setSal(String sal) {
        this.sal = sal;
    }

    public String getCl() {
        return cl;
    }

    public void setCl(String cl) {
        this.cl = cl;
    }

    public String getOtl() {
        return otl;
    }

    public void setOtl(String otl) {
        this.otl = otl;
    }

    private String otl;


    public String getPre_serialno() {
        return pre_serialno;
    }

    public void setPre_serialno(String pre_serialno) {
        this.pre_serialno = pre_serialno;
    }
    private String p;
    private String l;
    private String a;

    public String getSch_wd() {
        return sch_wd;
    }

    public void setSch_wd(String sch_wd) {
        this.sch_wd = sch_wd;
    }

    public String getStd_wd() {
        return std_wd;
    }

    public void setStd_wd(String std_wd) {
        this.std_wd = std_wd;
    }

    /**
     * father_name : sd
     */

    private String sch_wd;
    private String std_wd;

    private String father_name="";


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public static ArrayList<AttendMonthBean> parseAttendArray(JSONArray arrayObj) {
        ArrayList list = new ArrayList<AttendMonthBean>();
        try {
            for (int i = 0; i < arrayObj.length(); i++) {
                AttendMonthBean p = parseAttendObject(arrayObj.getJSONObject(i));
                if (p != null) {
                    list.add(p);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public static AttendMonthBean parseAttendObject(JSONObject jsonObject) {
        AttendMonthBean casteObj = new AttendMonthBean();
        try {
            if (jsonObject.has(ID)) {
                casteObj.setId(jsonObject.getString(ID));
            }
            if (jsonObject.has(STUDENT_ID) && !jsonObject.getString(STUDENT_ID).isEmpty() && !jsonObject.getString(STUDENT_ID).equalsIgnoreCase("null")) {
                casteObj.setStudent_id(jsonObject.getString(STUDENT_ID));
            }
            if (jsonObject.has(STUDENT_NAME) && !jsonObject.getString(STUDENT_NAME).isEmpty() && !jsonObject.getString(STUDENT_NAME).equalsIgnoreCase("null")) {
                casteObj.setStudent_name(jsonObject.getString(STUDENT_NAME));
            }
            if (jsonObject.has("father_name") && !jsonObject.getString("father_name").isEmpty() && !jsonObject.getString("father_name").equalsIgnoreCase("null")) {
                casteObj.setFather_name(jsonObject.getString("father_name"));
            }
            if (jsonObject.has("pre_serialno") && !jsonObject.getString("pre_serialno").isEmpty() && !jsonObject.getString("pre_serialno").equalsIgnoreCase("null")) {
                casteObj.setPre_serialno(jsonObject.getString("pre_serialno"));
            }
            if (jsonObject.has("a")) {
                casteObj.setA(jsonObject.getString("a"));
            }
            if (jsonObject.has("l")) {
                casteObj.setL(jsonObject.getString("l"));
            }
            if (jsonObject.has("p")) {
                casteObj.setP(jsonObject.getString("p"));
            }

            if (jsonObject.has("std_wd")) {
                casteObj.setStd_wd(jsonObject.getString("std_wd"));
            }
            if (jsonObject.has("sch_wd")) {
                casteObj.setSch_wd(jsonObject.getString("sch_wd"));
            }

            if (jsonObject.has("hl")) {
                casteObj.setHl(jsonObject.getString("hl"));
            }
            if (jsonObject.has("otl")) {
                casteObj.setOtl(jsonObject.getString("otl"));
            }
            if (jsonObject.has("cl")) {
                casteObj.setCl(jsonObject.getString("cl"));
            }
            if (jsonObject.has("sal")) {
                casteObj.setSal(jsonObject.getString("sal"));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return casteObj;
    }

    public String getStudent_id() {
        return student_id;
    }

    public void setStudent_id(String student_id) {
        this.student_id = student_id;
    }

    public String getStudent_name() {
        return student_name;
    }

    public void setStudent_name(String student_name) {
        this.student_name = student_name;
    }

    public String getP() {
        return p;
    }

    public void setP(String p) {
        this.p = p;
    }

    public String getL() {
        return l;
    }

    public void setL(String l) {
        this.l = l;
    }

    public String getA() {
        return a;
    }

    public void setA(String a) {
        this.a = a;
    }

    public String getFather_name() {
        return father_name;
    }

    public void setFather_name(String father_name) {
        this.father_name = father_name;
    }
}
