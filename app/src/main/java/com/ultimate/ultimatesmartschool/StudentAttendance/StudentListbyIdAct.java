package com.ultimate.ultimatesmartschool.StudentAttendance;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.google.gson.Gson;
import com.ultimate.ultimatesmartschool.BeanModule.Studentbean;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class StudentListbyIdAct extends AppCompatActivity {
String student_id;
    public String date;
    @BindView(R.id.parent)
    RelativeLayout parent;
    private int loaded = 0;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.textNorecord)
    TextView txtNorecord;
    @BindView(R.id.adddate)TextView adddate;
    @BindView(R.id.sc)
    ScrollView scrollView;
    private ArrayList<AttendBean> dataList;
    private GDSStudAttendAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_listby_id);
        ButterKnife.bind(this);
        dataList = new ArrayList<>();
        if(getIntent()!= null && getIntent().hasExtra("student_id")){
            student_id = getIntent().getStringExtra("student_id");
        }else {
            return;
        }
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new GDSStudAttendAdapter(this, dataList);
        recyclerView.setAdapter(adapter);
        searchStudent(student_id);
        Toast.makeText(getApplicationContext(),student_id,Toast.LENGTH_LONG).show();
        java.util.Calendar calendar = java.util.Calendar.getInstance(TimeZone.getDefault());
        Date setdate = calendar.getTime();
        SimpleDateFormat fmtOut = new SimpleDateFormat("dd MMM, yyyy");
        String dateString = fmtOut.format(setdate);
        adddate.setText(dateString);
        SimpleDateFormat dateFrmOut = new SimpleDateFormat("yyyy-MM-dd");
        date = dateFrmOut.format(setdate);
    }
    public void searchStudent(String student_id) {

            ErpProgress.showProgressBar(this, "Please wait...");
            HashMap<String, String> params = new HashMap<String, String>();
            params.put("stu_id",student_id);
            if (date != null) {
                params.put("mdate", String.valueOf(date));
            }
            ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.GDSSTUDENT_ATTEND_URLNEW, studentapiCallback, this, params);

    }

    ApiHandler.ApiCallback studentapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            if (error == null) {
                try {
                    ArrayList<AttendBean> studattendList = new ArrayList<>();
                    ArrayList<AttendBean> attendList = AttendBean.parseAttendArray(jsonObject.getJSONArray(Constants.ATTEND_DATA));
                    if (jsonObject.has("stud_data")) {
                        studattendList = AttendBean.parseAttendArray(jsonObject.getJSONArray("stud_data"));
                        if (studattendList.size() > attendList.size()) {
                            for (int i = 0; i < studattendList.size(); i++) {
                                boolean check = true;
                                for (int j = 0; j < attendList.size(); j++) {
                                    if (studattendList.get(i).getStudent_id().equals(attendList.get(j).getStudent_id())) {
                                        check = false;
                                        break;
                                    }
                                }
                                if (check) {
                                    attendList.add(studattendList.get(i));
                                }
                            }
                        } else {
                            for (int i = 0; i < attendList.size(); i++) {
                                boolean check = true;
                                for (int j = 0; j < studattendList.size(); j++) {
                                    if (studattendList.get(j).getStudent_id().equals(attendList.get(i).getStudent_id())) {
                                        check = false;
                                        break;
                                    }
                                }
                                if (check) {
                                    attendList.remove(i);
                                    i--;
                                }
                            }
                        }
                    }

                    dataList.clear();
                    dataList.addAll(attendList);
                    scrollView.setVisibility(View.VISIBLE);
                    txtNorecord.setVisibility(View.GONE);
                    adapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                scrollView.setVisibility(View.GONE);
                txtNorecord.setVisibility(View.VISIBLE);
                Utils.showSnackBar(error.getMessage(), parent);
            }
        }
    };
    @OnClick(R.id.btnSubmit)
    public void submitAttendance() {
        addAttend();
    }
    private void addAttend() {
        ErpProgress.showProgressBar(this,"Please wait...");
        dataList =adapter.getAttendList();
        ArrayList<String> attendList = new ArrayList<>();
        ArrayList<String> id = new ArrayList<>();
        ArrayList<String> st_id = new ArrayList<>();
        ArrayList<String> st_name = new ArrayList<>();
        for (int i = 0; i < dataList.size(); i++) {
            attendList.add(dataList.get(i).getAttendance());
            st_id.add(dataList.get(i).getStudent_id());
            id.add(dataList.get(i).getId());
            st_name.add(dataList.get(i).getStudent_name());
        }
        Log.e("list: ",attendList.toString().trim()+","+id.toString().trim()+","+st_id.toString().trim()+","+st_name.toString().trim());
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("attend", String.valueOf(attendList));
        params.put("st_id", String.valueOf(st_id));
        params.put("id", String.valueOf(id));
        params.put("st_name", String.valueOf(st_name));
        params.put("class_id", dataList.get(0).getClass_id());
        params.put("date", date);
        params.put("section_id",dataList.get(0).getSection_id());
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.ADD_ST_ATTEND_URLGDS, apiCallback, this, params);

    }

    ApiHandler.ApiCallback apiCallback = new ApiHandler.ApiCallback() {

        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            if (error == null) {
                try {
                    Toast.makeText(StudentListbyIdAct.this,jsonObject.getString("msg"), Toast.LENGTH_SHORT).show();
                    finish();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("error", error.getMessage() + "");
                Utils.showSnackBar(error.getMessage(), parent);
            }
        }
    };


    @OnClick(R.id.imgBack)
    public void onBackclick() {
        finish();
    }
}