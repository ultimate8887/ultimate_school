package com.ultimate.ultimatesmartschool.StudentAttendance;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.ultimate.ultimatesmartschool.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ClassReportAdapter extends RecyclerView.Adapter<ClassReportAdapter.MyViewHolder> {

    private Context mContext;
    ArrayList<AttendBean> dataList;

    public ClassReportAdapter(Context mContext, ArrayList<AttendBean> dataList) {
        this.mContext = mContext;
        this.dataList = dataList;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.class_day_report, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        if(position==0){
            holder.lytHeader.setVisibility(View.GONE);
            holder.lytData.setVisibility(View.GONE);
        }else {
            holder.lytHeader.setVisibility(View.INVISIBLE);
            holder.lytData.setVisibility(View.VISIBLE);
            final AttendBean data = dataList.get(position-1);
            holder.txtName.setText(data.getStudent_name());
            // holder.txtFName.setText(data.getFather_name());
            holder.txtRoll.setText(data.getStudent_id());
            holder.txtsno.setText(data.getSno());
            String attend="N/A";
            if(data.getAttendance().equalsIgnoreCase("a")){
                attend ="Absent";
                holder.txtAttend.setBackground(mContext.getResources().getDrawable(R.drawable.absent_bg));
            }else if(data.getAttendance().equalsIgnoreCase("p")){
                attend ="Present";
                holder.txtAttend.setBackground(mContext.getResources().getDrawable(R.drawable.present_bg));
            }else if(data.getAttendance().equalsIgnoreCase("l")){

                if(data.getL_remarks()!=null) {
                    holder.txtAttend.setBackground(mContext.getResources().getDrawable(R.drawable.half_leave_bg));
                    attend="Leave(HL)";
                }else {
                    attend="Leave";
                    holder.txtAttend.setBackground(mContext.getResources().getDrawable(R.drawable.leave_bg));
                }

            }else {
                holder.txtAttend.setBackground(mContext.getResources().getDrawable(R.drawable.white_bg));
            }
            holder.txtAttend.setText(attend);
        }

    }

    @Override
    public int getItemCount() {
        return (dataList.size()+1);
    }

    public ArrayList<AttendBean> getAttendList() {
        return dataList;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txtAttend)
        TextView txtAttend;
        @BindView(R.id.txtName)
        TextView txtName;
        @BindView(R.id.txtRoll)
        TextView txtRoll;
        @BindView(R.id.txtFName)
        TextView txtFName;

        @BindView(R.id.txtsno)
        TextView txtsno;
        @BindView(R.id.lytData)
        LinearLayout lytData;
        @BindView(R.id.lytHeader)LinearLayout lytHeader;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
