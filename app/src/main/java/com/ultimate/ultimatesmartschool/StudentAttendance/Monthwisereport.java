package com.ultimate.ultimatesmartschool.StudentAttendance;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetSequence;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.ultimate.ultimatesmartschool.BeanModule.ClassBean;
import com.ultimate.ultimatesmartschool.BeanModule.SectionBean;
import com.ultimate.ultimatesmartschool.BeanModule.SubjectBeancls;
import com.ultimate.ultimatesmartschool.BeanModule.User;
import com.ultimate.ultimatesmartschool.Homework.AdapterOfSubjectList;
import com.ultimate.ultimatesmartschool.Homework.SectionnewAdapter;
import com.ultimate.ultimatesmartschool.Login.LoginActivity;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.StaffAttendance.DepartmentReportMonAdapter;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.CommonProgress;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class Monthwisereport extends AppCompatActivity {
    View view;
    int selected = 2;
    private Date fromdate;
    private ArrayList<AttendMonthBean> monList;
    //    private ClassReportMonAdapter madapter;
    private YearlyAttendanceAdapter madapter;
    private static final String TAG = "PdfCreatorActivity";
    final private int REQUEST_CODE_ASK_PERMISSIONS = 111;
    private File pdfFile, csvfilee;
    Calendar calendar;
    String remarks;
    String filePath;
    String month_name;

    @BindView(R.id.parent)
    RelativeLayout parent;
    @BindView(R.id.txtTitle)
    TextView txtTitle;

    private ArrayList<ClassBean> classList;
    SharedPreferences sharedPreferences;

    @BindView(R.id.export)
    FloatingActionButton export;

    //@BindView(R.id.spinner) Spinner spinnerClass;
    BottomSheetDialog mBottomSheetDialog;
    private int loaded = 0;

    @BindView(R.id.txtSub)
    TextView txtSub;
    List<String> classidsel;
    @BindView(R.id.totalRecord)
    TextView totalRecord;

    ArrayList<SectionBean> sectionList = new ArrayList<>();

    @BindView(R.id.adddate)
    TextView adddate;
    public String date;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.textNorecord)
    TextView txtNorecord;
    //    @BindView(R.id.btnSubmit)
//    Button btnSubmit;
    @BindView(R.id.view_type)
    TextView view_type;

    @BindView(R.id.dialog)
    ImageView dialog;
    @BindView(R.id.sc)
    CardView scrollView;
    @BindView(R.id.filelayout)
    LinearLayout filelayout;

    Spinner spinnersection;
    Spinner spinnerClass;
    Spinner spinnersubject;

    ArrayList<SubjectBeancls> subjectList = new ArrayList<>();
    AdapterOfSubjectList adaptersub;
    String sub_id = "", sub_name = "", sectionid = "", sectionname = "", classid = "", className = "";

    int check = 0;
    CommonProgress commonProgress;
    int sundayCount = 0, total_h = 0;
    String holiday = "";


    @RequiresApi(api = Build.VERSION_CODES.N)

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_student_attend_view_gds);
        setContentView(R.layout.activity_year_wise_report);
        ButterKnife.bind(this);
        monList = new ArrayList<>();
        commonProgress = new CommonProgress(this);
        classidsel = new ArrayList<>();
        view_type.setText("Month: ");
//        recyclerView.setLayoutManager(new LinearLayoutManager(this));
//        madapter = new ClassReportMonAdapter(this, monList);
//        recyclerView.setAdapter(madapter);
        if (!restorePrefData()) {
            setShowcaseView();
        } else {
            openDialog();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @OnClick(R.id.dialog)
    public void dialogggg() {
        openDialog();
    }

    @OnClick(R.id.imgBack)
    public void onBackclick() {
        commonBack();
    }

    private void fetchsection() {
        HashMap<String, String> params = new HashMap<String, String>();
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.SECTIONLIST_URL, sectionapiCallback, getApplicationContext(), params);
    }

    ApiHandler.ApiCallback sectionapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            if (error == null) {
                try {
                    sectionList = SectionBean.parseCommonArray(jsonObject.getJSONArray("hostelroom_data"));
                    SectionnewAdapter adapter = new SectionnewAdapter(getApplicationContext(), sectionList);
                    spinnersection.setAdapter(adapter);
                    spinnersection.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if (i > 0) {
                                sectionid = sectionList.get(i - 1).getSection_id();
                                sectionname = sectionList.get(i - 1).getSection_name();

                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                //Utils.showSnackBar(error.getMessage(), parent);
                Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
                if (error.getStatusCode() == 405) {
                    User.logout();
                    startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                    finish();
                }
            }
        }
    };

    private void setShowcaseViewP() {
        new TapTargetSequence(this)
                .targets(TapTarget.forView(export, "Export Excel!", "Tap the Export button to export month wise attendance report.")
                        .outerCircleColor(R.color.light)
                        .outerCircleAlpha(0.96f)
                        .targetCircleColor(R.color.white)
                        .titleTextSize(20)
                        .titleTextColor(R.color.white)
                        .descriptionTextSize(14)
                        .descriptionTextColor(R.color.black)
                        .textColor(R.color.black)
                        .textTypeface(Typeface.SANS_SERIF)
                        .dimColor(R.color.black)
                        .drawShadow(true)
                        .cancelable(false)
                        .tintTarget(true)
                        .transparentTarget(true)
                        .targetRadius(40)).listener(new TapTargetSequence.Listener() {
                    @RequiresApi(api = Build.VERSION_CODES.N)
                    @Override
                    public void onSequenceFinish() {
                        // Toast.makeText(getApplicationContext(),"Sequence Finished",Toast.LENGTH_SHORT).show();
                        savePrefDataP();

                    }

                    @Override
                    public void onSequenceStep(TapTarget lastTarget, boolean targetClicked) {
                        // Toast.makeText(getActivity(),"GREAT!",Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onSequenceCanceled(TapTarget lastTarget) {

                    }
                }).start();
    }

    private void savePrefDataP() {
        sharedPreferences = this.getSharedPreferences("boarding_pref_view_export", MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean("IsFirstTimeVisit_view_export", true);
        editor.apply();
    }

    private boolean restorePrefDataP() {
        sharedPreferences = this.getSharedPreferences("boarding_pref_view_export", MODE_PRIVATE);
        return sharedPreferences.getBoolean("IsFirstTimeVisit_view_export", false);
    }


    @OnClick(R.id.filelayout)
    public void fetchMonth() {
        dateDialog();
    }


    public void dateDialog() {
        java.util.Calendar cal = java.util.Calendar.getInstance(TimeZone.getDefault()); // Get current date
        DatePickerDialog datePicker = new DatePickerDialog(this,
                R.style.MyDatePickerDialogTheme, ondate,
                cal.get(java.util.Calendar.YEAR),
                cal.get(java.util.Calendar.MONTH),
                cal.get(java.util.Calendar.DAY_OF_MONTH));
        datePicker.getDatePicker().setMaxDate(new java.util.Date().getTime());
        datePicker.setCancelable(false);
        datePicker.show();
    }

    DatePickerDialog.OnDateSetListener ondate = new DatePickerDialog.OnDateSetListener() {
        @RequiresApi(api = Build.VERSION_CODES.O)
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            java.util.Calendar calendar = java.util.Calendar.getInstance(TimeZone.getDefault());
            calendar.set(year, monthOfYear, dayOfMonth);
            calendar.set(Calendar.HOUR_OF_DAY, 00);
            calendar.set(Calendar.MINUTE, 00);
            calendar.set(Calendar.SECOND, 00);
            java.util.Date setdate = calendar.getTime();
            if (selected == 2) {
                SimpleDateFormat fmtOut = new SimpleDateFormat("MMMM, yyyy");
                String dateString = fmtOut.format(setdate);
                adddate.setText(dateString);
            }

//            else {
//                SimpleDateFormat fmtOut = new SimpleDateFormat("MMMM, yyyy");
//                String dateString = fmtOut.format(setdate);
//                txtMonth.setText(dateString);
//            }

            SimpleDateFormat dateFrmOut = new SimpleDateFormat("yyyy-MM-dd");
            date = dateFrmOut.format(setdate);
            fromdate = new Date(calendar.getTimeInMillis());
            if (classid != null) {
//                fetchStudentList(date);
                fetchSunday(date);
            }
        }
    };


    @SuppressLint("RestrictedApi")
    @OnClick(R.id.export)
    public void submit() {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.S) {
            importData();
        } else {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {

                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    Toast.makeText(this, "Please provide storage permission from app settings", Toast.LENGTH_LONG).show();
                } else {

                    ActivityCompat.requestPermissions(this,
                            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            1);
                }
            } else {
                importData();

            }
        }
    }

    @SuppressLint("RestrictedApi")
    private void importData() {
        // File filePath = new File(Environment.getExternalStorageDirectory() + "/Demo.xls");
        Workbook wb = new HSSFWorkbook();

        Cell cell = null;

        String ndate = "", sub_staff = "", sub_date = "";
        ndate = adddate.getText().toString();
        sub_date = ndate.substring(0, 3);


        if (className.equalsIgnoreCase("")) {
            sub_staff = "All_Classes";
        } else {
            sub_staff = className + "_" + sectionname;
        }

        Sheet sheet = null;
        sheet = wb.createSheet(adddate.getText().toString() + " Month Attendance Report");
        //Now column and row
        Row row = sheet.createRow(0);

        cell = row.createCell(0);
        cell.setCellValue("S No");

        cell = row.createCell(1);
        cell.setCellValue("Roll No");

        cell = row.createCell(2);
        cell.setCellValue("Class/Section");

        cell = row.createCell(3);
        cell.setCellValue("Student Name");

        cell = row.createCell(4);
        cell.setCellValue("Father Name");

        cell = row.createCell(5);
        cell.setCellValue("Present");

        cell = row.createCell(6);
        cell.setCellValue("Absent");

        cell = row.createCell(7);
        cell.setCellValue("Casual Leave)");

        cell = row.createCell(8);
        cell.setCellValue("Half Leave");

        cell = row.createCell(9);
        cell.setCellValue("School Activity Leave");

        cell = row.createCell(10);
        cell.setCellValue("Holiday & Sunday");

        cell = row.createCell(11);
        cell.setCellValue("School Working Day");

        cell = row.createCell(12);
        cell.setCellValue("Attendance Report");

        cell = row.createCell(13);
        cell.setCellValue("Month");


        //column width
        sheet.setColumnWidth(0, (20 * 100));
        sheet.setColumnWidth(1, (20 * 150));
        sheet.setColumnWidth(2, (30 * 150));
        sheet.setColumnWidth(3, (30 * 250));
        sheet.setColumnWidth(4, (30 * 200));
        sheet.setColumnWidth(5, (20 * 100));
        sheet.setColumnWidth(6, (20 * 100));
        sheet.setColumnWidth(7, (20 * 100));
        sheet.setColumnWidth(8, (30 * 200));
        sheet.setColumnWidth(9, (30 * 250));
        sheet.setColumnWidth(10, (30 * 250));
        sheet.setColumnWidth(11, (30 * 250));
        sheet.setColumnWidth(12, (30 * 250));
        sheet.setColumnWidth(13, (30 * 250));

        for (int i = 0; i < monList.size(); i++) {

            Row row1 = sheet.createRow(i + 1);

            cell = row1.createCell(0);
            cell.setCellValue(i + 1);

            cell = row1.createCell(1);
            cell.setCellValue(monList.get(i).getStudent_id());

            cell = row1.createCell(2);
            cell.setCellValue(className + " (" + sectionname + ")");

            cell = row1.createCell(3);
            cell.setCellValue((monList.get(i).getStudent_name()));
            //  cell.setCellStyle(cellStyle);

            cell = row1.createCell(4);
            cell.setCellValue(monList.get(i).getFather_name());

            cell = row1.createCell(5);
            cell.setCellValue(monList.get(i).getP());

            cell = row1.createCell(6);
            cell.setCellValue(monList.get(i).getA());
//
//            int hl=0,otl=0,sal=0,cl=0,l=0,tl=0;
//            l= Integer.parseInt(monList.get(i).getL());
//            hl= Integer.parseInt(monList.get(i).getHl());
//            otl= Integer.parseInt(monList.get(i).getOtl());
//            sal= Integer.parseInt(monList.get(i).getSal());
//            tl=hl+otl+sal;
//            cl=l-tl;

            cell = row1.createCell(7);
            cell.setCellValue(monList.get(i).getCl());

            cell = row1.createCell(8);
            cell.setCellValue(monList.get(i).getHl());

            cell = row1.createCell(9);
            cell.setCellValue(monList.get(i).getSal());

            cell = row1.createCell(10);
            cell.setCellValue(String.valueOf(total_h));

            cell = row1.createCell(11);
            cell.setCellValue(monList.get(i).getSch_wd());

            cell = row1.createCell(12);
            cell.setCellValue(monList.get(i).getStd_wd());

            cell = row1.createCell(13);
            cell.setCellValue(ndate);


            sheet.setColumnWidth(0, (20 * 100));
            sheet.setColumnWidth(1, (20 * 150));
            sheet.setColumnWidth(2, (30 * 150));
            sheet.setColumnWidth(3, (30 * 250));
            sheet.setColumnWidth(4, (30 * 200));
            sheet.setColumnWidth(5, (20 * 100));
            sheet.setColumnWidth(6, (20 * 100));
            sheet.setColumnWidth(7, (20 * 100));
            sheet.setColumnWidth(8, (30 * 200));
            sheet.setColumnWidth(9, (30 * 250));
            sheet.setColumnWidth(10, (30 * 250));
            sheet.setColumnWidth(11, (30 * 250));
            sheet.setColumnWidth(12, (30 * 250));
            sheet.setColumnWidth(13, (30 * 250));


        }
        String fileName;
        if (sub_staff.equalsIgnoreCase("")) {
            fileName = sub_date + "_" + System.currentTimeMillis() + ".xls";
        } else {
            fileName = sub_staff + "_" + sub_date + "_" + System.currentTimeMillis() + ".xls";
        }


        // File file = new File(getActivity().getExternalFilesDir(null), fileName);

        File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS), fileName);

        FileOutputStream fileOutputStream = null;

        try {
            fileOutputStream = new FileOutputStream(file);
            wb.write(fileOutputStream);
            Utils.openSuccessDialog("Success!", "Export file in " + file, "", this);
        } catch (IOException e) {
            Utils.openErrorDialog("Error writing Exception: " + e, this);
        } catch (Exception e) {
            Utils.openErrorDialog("Failed to save file due to Exception: " + e, this);
        } finally {
            try {
                if (null != fileOutputStream) {
                    fileOutputStream.close();
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

    }

    @SuppressLint("RestrictedApi")
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 1 && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            importData();
        } else {
            Toast.makeText(getApplicationContext(), "Permission Denied", Toast.LENGTH_SHORT).show();
        }
    }

    private void fetchStudentList(String date) {
        commonProgress.show();
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("class_id", classid);
        if (date != null) {
            params.put("mdate", String.valueOf(date));
        }
        params.put("section", sectionid);
        params.put("type", selected + "");
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.STUDENT_ATTEND_LISTGDS, studentapiCallback, getApplicationContext(), params);
    }

    ApiHandler.ApiCallback studentapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            commonProgress.dismiss();
            ;
            mBottomSheetDialog.dismiss();
            if (error == null) {
                try {
                    if (selected == 2) {
                        ArrayList<AttendMonthBean> attendList = AttendMonthBean.parseAttendArray(jsonObject.getJSONArray(Constants.ATTEND_DATA));
                        monList.clear();
                        monList.addAll(attendList);
                        madapter.notifyDataSetChanged();
                        totalRecord.setText("Total Entries:- " + String.valueOf(attendList.size()));
                        scrollView.setVisibility(View.VISIBLE);
                        export.setVisibility(View.VISIBLE);
                        if (!restorePrefDataP()) {
                            setShowcaseViewP();
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                export.setVisibility(View.GONE);
                scrollView.setVisibility(View.GONE);
                // Utils.showSnackBar(error.getMessage(), view.findViewById(R.id.parent));
                totalRecord.setText("Total Entries:- 0");
                Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }
    };

    private void setShowcaseView_aday() {
        new TapTargetSequence(this)
                .targets(TapTarget.forView(filelayout, "Date Button!", "Tap the Date button to View Month-Wise Attendance.")
                        .outerCircleColor(R.color.light)
                        .outerCircleAlpha(0.96f)
                        .targetCircleColor(R.color.white)
                        .titleTextSize(20)
                        .titleTextColor(R.color.white)
                        .descriptionTextSize(14)
                        .descriptionTextColor(R.color.black)
                        .textColor(R.color.black)
                        .textTypeface(Typeface.SANS_SERIF)
                        .dimColor(R.color.black)
                        .drawShadow(true)
                        .cancelable(false)
                        .tintTarget(true)
                        .transparentTarget(true)
                        .targetRadius(60)).listener(new TapTargetSequence.Listener() {
                    @RequiresApi(api = Build.VERSION_CODES.N)
                    @Override
                    public void onSequenceFinish() {
                        // Toast.makeText(getApplicationContext(),"Sequence Finished",Toast.LENGTH_SHORT).show();
                        savePrefData_aday();
                        //openDialog();
                    }

                    @Override
                    public void onSequenceStep(TapTarget lastTarget, boolean targetClicked) {
                        // Toast.makeText(getActivity(),"GREAT!",Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onSequenceCanceled(TapTarget lastTarget) {

                    }
                }).start();
    }

    private void savePrefData_aday() {
        sharedPreferences = this.getSharedPreferences("boarding_pref_add_attend_mday", MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean("IsFirstTimeVisit_add_attend_mday", true);
        editor.apply();
    }

    private boolean restorePrefData_aday() {
        sharedPreferences = this.getSharedPreferences("boarding_pref_add_attend_mday", MODE_PRIVATE);
        return sharedPreferences.getBoolean("IsFirstTimeVisit_add_attend_mday", false);
    }


    private void setShowcaseView() {
        new TapTargetSequence(this)
                .targets(TapTarget.forView(dialog, "Search Button!", "Tap the Search button to to view Class-Wise Attendance Report.")
                        .outerCircleColor(R.color.light)
                        .outerCircleAlpha(0.96f)
                        .targetCircleColor(R.color.white)
                        .titleTextSize(20)
                        .titleTextColor(R.color.white)
                        .descriptionTextSize(14)
                        .descriptionTextColor(R.color.black)
                        .textColor(R.color.black)
                        .textTypeface(Typeface.SANS_SERIF)
                        .dimColor(R.color.black)
                        .drawShadow(true)
                        .cancelable(false)
                        .tintTarget(true)
                        .transparentTarget(true)
                        .targetRadius(60)).listener(new TapTargetSequence.Listener() {
                    @RequiresApi(api = Build.VERSION_CODES.N)
                    @Override
                    public void onSequenceFinish() {
                        // Toast.makeText(getApplicationContext(),"Sequence Finished",Toast.LENGTH_SHORT).show();
                        savePrefData();
                        openDialog();
                    }

                    @Override
                    public void onSequenceStep(TapTarget lastTarget, boolean targetClicked) {
                        // Toast.makeText(getActivity(),"GREAT!",Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onSequenceCanceled(TapTarget lastTarget) {

                    }
                }).start();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        commonBack();
    }

    private void commonBack() {
        androidx.appcompat.app.AlertDialog.Builder builder1 = new androidx.appcompat.app.AlertDialog.Builder(Monthwisereport.this);
        builder1.setMessage("Are you sure, you want to exit? ");
        builder1.setCancelable(false);
        builder1.setPositiveButton(
                "Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finish();
                    }
                });
        builder1.setNegativeButton(
                "No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

    private void savePrefData() {
        sharedPreferences = this.getSharedPreferences("boarding_pref_add_attend_msday", MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean("IsFirstTimeVisit_add_attend_msday", true);
        editor.apply();
    }

    private boolean restorePrefData() {
        sharedPreferences = this.getSharedPreferences("boarding_pref_add_attend_msday", MODE_PRIVATE);
        return sharedPreferences.getBoolean("IsFirstTimeVisit_add_attend_msday", false);
    }


    @RequiresApi(api = Build.VERSION_CODES.N)
    private void openDialog() {

        mBottomSheetDialog = new BottomSheetDialog(this);
        final View sheetView = getLayoutInflater().inflate(R.layout.search_dialog, null);
        mBottomSheetDialog.setContentView(sheetView);
        mBottomSheetDialog.setCancelable(false);

        spinnerClass = (Spinner) sheetView.findViewById(R.id.spinnerClass);
        spinnersection = (Spinner) sheetView.findViewById(R.id.spinnersection);
        spinnersubject = (Spinner) sheetView.findViewById(R.id.spinnersubject);
        ImageView close = (ImageView) sheetView.findViewById(R.id.close);
        TextView txtSetup = (TextView) sheetView.findViewById(R.id.txtSetup);

        txtSetup.setText("View Month-Wise \nAttendance");

        getTodayDate();
        //txtTitle.setText("Mark Attendance");
        fetchClass();

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //assignData();
                if (check == 0) {
                    finish();
                    mBottomSheetDialog.dismiss();
                } else {
                    mBottomSheetDialog.dismiss();
                }
            }
        });


        Button btnYes = (Button) sheetView.findViewById(R.id.btnYes);


        btnYes.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onClick(View view) {
                if (classid.equalsIgnoreCase("")) {
                    Toast.makeText(Monthwisereport.this, "Kindly Select Class!", Toast.LENGTH_SHORT).show();
                } else if (sectionid.equalsIgnoreCase("")) {
                    Toast.makeText(Monthwisereport.this, "Kindly Select Class Section!", Toast.LENGTH_SHORT).show();
                } else {
                    check++;
                    // fetchStudentList(date);
                    setCommonData();
                }

            }
        });
        mBottomSheetDialog.show();

    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void setCommonData() {
        filelayout.setVisibility(View.VISIBLE);
        if (!restorePrefData_aday()) {
            setShowcaseView_aday();
        }
        txtTitle.setText(className + "(" + sectionname + ")");
        txtSub.setText(sub_name);
        fetchSunday(date);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void fetchSunday(String dateString) {
        // String dateString = "2024-04-01"; // Example date string

        sundayCount = 0;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate date = LocalDate.parse(dateString, formatter);

        LocalDate firstDayOfMonth = date.with(TemporalAdjusters.firstDayOfMonth());
        LocalDate lastDayOfMonth = date.with(TemporalAdjusters.lastDayOfMonth());

        for (LocalDate d = firstDayOfMonth; !d.isAfter(lastDayOfMonth); d = d.plusDays(1)) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                if (d.getDayOfWeek() == DayOfWeek.SUNDAY) {
                    sundayCount++;
                }
            }
        }
        //Toast.makeText(getApplicationContext(),"Sunday count"+String.valueOf(sundayCount),Toast.LENGTH_SHORT).show();
        fetchHoliday(dateString);

    }

    private void fetchHoliday(String dateString) {

        HashMap<String, String> params = new HashMap<>();
        params.put("user_id", User.getCurrentUser().getId());
        params.put("check", "fetch");
        params.put("h_date", dateString);

        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.UPDATE_SCHOOL_INFO, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                commonProgress.dismiss();
                ErpProgress.cancelProgressBar();
                if (error == null) {
                    try {

                        holiday = jsonObject.getJSONObject(Constants.USERDATA).getString("holiday");
                        calculateSum(holiday, dateString);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    calculateSum("0", dateString);
                }
            }
        }, this, params);

    }

    private void calculateSum(String holiday, String date) {

        int hol = Integer.parseInt(holiday);
        total_h = sundayCount + hol;

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        // madapter = new ClassReportMonAdapter(this, monList,total_h);
        madapter = new YearlyAttendanceAdapter(this, monList, total_h);
        recyclerView.setAdapter(madapter);
        fetchStudentList(date);
    }

    //    public void fetchCurrentMonth() {
//        Timestamp ts = new Timestamp(System.currentTimeMillis());
//        fromdate = new Date(ts.getTime());
//        SimpleDateFormat postFormater = new SimpleDateFormat("MMMM, yyyy");
//        month_name = postFormater.format(fromdate);
//        //txtMonth.setText(postFormater.format(fromdate));
//        if(classid!= null){
//            fetchStudentList();
//        }
//    }
    public void getTodayDate() {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.HOUR_OF_DAY, 00);
        c.set(Calendar.MINUTE, 00);
        c.set(Calendar.SECOND, 00);
        java.util.Date setdate = c.getTime();
        //  SimpleDateFormat fmtOut = new SimpleDateFormat("dd MMM, yyyy");
        SimpleDateFormat fmtOut = new SimpleDateFormat("MMMM, yyyy");
        String dateString = fmtOut.format(setdate);
        adddate.setText(dateString);
        SimpleDateFormat dateFrmOut = new SimpleDateFormat("yyyy-MM-dd");
        date = dateFrmOut.format(setdate);
    }


//    @OnClick(R.id.lytMonth)
//    public void selectMonth() {
//        selected = 2;
//        fetchCurrentMonth();
//
//        txtMonth.setVisibility(View.VISIBLE);
//
//        monthRecycler.setVisibility(View.VISIBLE);
//
//    }

    private void fetchClass() {
        commonProgress.show();
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("check", "inch");
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.CLASSLIST_URL, classapiCallback, getApplicationContext(), params);
    }


    ApiHandler.ApiCallback classapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            commonProgress.dismiss();
            if (error == null) {
                try {
                    classList = ClassBean.parseClassArray(jsonObject.getJSONArray(Constants.CLASSDATA));
                    ClassAdapterAttend adapter = new ClassAdapterAttend(getApplicationContext(), classList, 0);
                    spinnerClass.setAdapter(adapter);
                    spinnerClass.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if (i > 0) {
                                sub_id = "";
                                sub_name = "";
                                sectionid = "";
                                sectionname = "";
                                classid = "";
                                className = "";
                                classid = classList.get(i - 1).getId();
                                className = classList.get(i - 1).getName();
                                fetchsection();
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {
                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
                // Utils.showSnackBar(error.getMessage(), view.findViewById(R.id.parent));
            }
        }
    };
}
