package com.ultimate.ultimatesmartschool.StudentAttendance;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.ultimate.ultimatesmartschool.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class YearlyAttendanceAdapter extends RecyclerView.Adapter<YearlyAttendanceAdapter.MyViewHolder> {

    private Context mContext;
    ArrayList<AttendMonthBean> dataList;
    int value=0;

    public YearlyAttendanceAdapter(Context mContext, ArrayList<AttendMonthBean> dataList, int value) {
        this.mContext = mContext;
        this.dataList = dataList;
        this.value = value;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.table_view_lyt, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        holder.txtLeave_Totl.setVisibility(View.GONE);
        holder.txtLeave_otl.setVisibility(View.GONE);


        if (position == 0) {
            holder.lytHeader.setVisibility(View.VISIBLE);
            holder.lytData.setVisibility(View.GONE);
        } else {


            holder.lytHeader.setVisibility(View.GONE);
            holder.lytData.setVisibility(View.VISIBLE);

            final AttendMonthBean data = dataList.get(position - 1);

            holder.txtName.setText(data.getStudent_name());
            holder.txtRoll.setText(data.getStudent_id());
            holder.txtFName.setText(data.getFather_name());
//            if(data.get)
            holder.txtAbsent.setText(data.getA());

            holder.txtPresent.setText(data.getP());

            holder.txtLeave_hl.setText(data.getHl());
            holder.txtLeave_sal.setText(data.getSal());

//            int hl=0,otl=0,sal=0,cl=0,l=0,tl=0;
//            l= Integer.parseInt(data.getL());
//            hl= Integer.parseInt(data.getHl());
//            sal= Integer.parseInt(data.getSal());
//            tl=hl+sal;
//            cl=l-tl;
            holder.txtLeave.setText(data.getCl());

            holder.txtHoliday.setText(String.valueOf(value));
            holder.txtWorking.setText(data.getSch_wd());
            holder.txtAttendace.setText(data.getStd_wd());
        }

    }

    @Override
    public int getItemCount() {
        return (dataList.size() + 1);
    }


    public void setList(ArrayList<AttendMonthBean> dataList) {
        this.dataList = dataList;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txtAbsent)
        TextView txtAbsent;
        @BindView(R.id.txtLeave)
        TextView txtLeave;

        @BindView(R.id.txtLeave_hl)
        TextView txtLeave_hl;
        @BindView(R.id.txtLeave_otl)
        TextView txtLeave_otl;
        @BindView(R.id.txtLeave_Totl)
        TextView txtLeave_Totl;

        @BindView(R.id.txtLeave_sal)
        TextView txtLeave_sal;

        @BindView(R.id.txtPresent)
        TextView txtPresent;
        @BindView(R.id.txtName)
        TextView txtName;
        @BindView(R.id.txtRoll)
        TextView txtRoll;
        @BindView(R.id.lytData)
        LinearLayout lytData;
        @BindView(R.id.lytHeader)
        LinearLayout lytHeader;
        @BindView(R.id.txtFName)
        TextView txtFName;
        @BindView(R.id.txtAttend)
        TextView txtAttendace;

        @BindView(R.id.txtHoliday)
        TextView txtHoliday;

        @BindView(R.id.txtWorking)
        TextView txtWorking;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
