package com.ultimate.ultimatesmartschool.StudentAttendance;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

public class StudentPager extends FragmentPagerAdapter {

    int tabCount;
    // tab titles
    private String[] tabTitles = new String[]{"Class", "Student"};

    public StudentPager(FragmentManager fm) {
        super(fm);
        this.tabCount =2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabTitles[position];
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
//                ClassReportGDS tab1 = new ClassReportGDS();
//                return tab1;
//            case 1:
//                ViewAttendance tab2 = new ViewAttendance();
//                return tab2;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return tabCount;
    }
}


