package com.ultimate.ultimatesmartschool.StudentAttendance;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.ultimate.ultimatesmartschool.BeanModule.ClassBean;
import com.ultimate.ultimatesmartschool.BeanModule.SectionBean;
import com.ultimate.ultimatesmartschool.BeanModule.User;
import com.ultimate.ultimatesmartschool.Homework.SectionnewAdapter;
import com.ultimate.ultimatesmartschool.Login.LoginActivity;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.TimeTable.AddTimeTable;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.apptik.widget.multiselectspinner.BaseMultiSelectSpinner;
import io.apptik.widget.multiselectspinner.MultiSelectSpinner;

public class AddAttendance extends AppCompatActivity {
    ArrayList<ClassBean> classList = new ArrayList<>();
    @BindView(R.id.parent)
    RelativeLayout parent;
    //@BindView(R.id.spinner) Spinner spinnerClass;
    @BindView(R.id.multiselectSpinnerclass)
    MultiSelectSpinner multiselectSpinnerclass;
    String classid = "";

    private int loaded = 0;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    List<String> classidsel;
    @BindView(R.id.textView7)TextView textView7;
    @BindView(R.id.spinnersection)
    Spinner spinnersection;
    ArrayList<SectionBean> sectionList = new ArrayList<>();
    String sectionid="";
    String sectionname;
    @BindView(R.id.adddate)TextView adddate;
    public String date;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.textNorecord)
    TextView txtNorecord;
    private ArrayList<AttendBean> dataList;
    private GDSStudAttendAdapter adapter;
    @BindView(R.id.btnSubmit)
    Button btnSubmit;
@BindView(R.id.sc)
    ScrollView scrollView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_attendance);
        ButterKnife.bind(this);
        classidsel = new ArrayList<>();
        dataList = new ArrayList<>();
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new GDSStudAttendAdapter(this, dataList);
        recyclerView.setAdapter(adapter);
        fetchClass();
        fetchSection();
        getTodayDate();
        txtTitle.setText("Mark Attendance");
    }

    private void fetchClass() {
        if (loaded == 0) {
            ErpProgress.showProgressBar(this, "Please wait...");
        }
        loaded++;

        HashMap<String, String> params = new HashMap<String, String>();
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.CLASSLIST_URL, classapiCallback, this, params);
    }

    ApiHandler.ApiCallback classapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            if (error == null) {
                try {
                    if (classList != null) {
                        classList.clear();
                    }
                    classList = ClassBean.parseClassArray(jsonObject.getJSONArray(Constants.CLASSDATA));
                    List<String> dstring = new ArrayList<>();
                    for (ClassBean cobj : classList) {
                        dstring.add(cobj.getName());
                        Log.e("getclassid",cobj.getId());
                    }

                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(AddAttendance.this, android.R.layout.simple_list_item_multiple_choice, dstring);

                    multiselectSpinnerclass.setListAdapter(adapter).setListener(new BaseMultiSelectSpinner.MultiSpinnerListener() {
                        @Override
                        public void onItemsSelected(boolean[] selected) {

                            Log.e("dcfcds", "fdsf");
                            String value = multiselectSpinnerclass.getSpinnerText();
                            List<String> clas = new ArrayList<>();
                            classidsel.clear();
                            if (!value.isEmpty()) {
                                clas = Arrays.asList(value.split("\\s*,\\s*"));
                            }
                            for (String dval : clas) {
                                for (ClassBean obj : classList) {
                                    if (obj.getName().equalsIgnoreCase(dval)) {
                                        classidsel.add(obj.getId());
                                        //  textView7.setVisibility(View.GONE);

                                        break;
                                    }
                                }
                            }

                            //fetchHomework();
                            Log.e("dcfcds", value + "  ,  classid:  " + classidsel);
                            // }
                        }
                    }).setSelectAll(false)
                            .setSpinnerItemLayout(androidx.appcompat.R.layout.support_simple_spinner_dropdown_item)
                            .setTitle("Select Class")

                            .setMaxSelectedItems(1)
                    ;


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            } else {
                Utils.showSnackBar(error.getMessage(), parent);
                if (error.getStatusCode() == 405) {
                    User.logout();
                    startActivity(new Intent(AddAttendance.this, LoginActivity.class));
                    finish();
                }
            }
        }
    };


    private void fetchSection() {
        HashMap<String, String> params = new HashMap<String, String>();
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.SECTIONLIST_URL, sectionapiCallback, this, params);
    }

    ApiHandler.ApiCallback sectionapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            if (error == null) {
                try {
                    sectionList = SectionBean.parseCommonArray(jsonObject.getJSONArray("hostelroom_data"));
                    SectionnewAdapter adapter = new SectionnewAdapter(AddAttendance.this, sectionList);
                    spinnersection.setAdapter(adapter);
                    spinnersection.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if (i > 0) {
                                sectionid = sectionList.get(i - 1).getSection_id();
                                sectionname = sectionList.get(i - 1).getSection_name();
                                // fetchHomework();
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Utils.showSnackBar(error.getMessage(), parent);
                if (error.getStatusCode() == 405) {
                    User.logout();
                    startActivity(new Intent(AddAttendance.this, LoginActivity.class));
                    finish();
                }
            }
        }
    };
    @OnClick(R.id.filelayout)
    public void fetchDate() {
        java.util.Calendar cal = java.util.Calendar.getInstance(TimeZone.getDefault()); // Get current date
        DatePickerDialog datePicker = new DatePickerDialog(this,
                R.style.MyDatePickerDialogTheme, ondate,
                cal.get(java.util.Calendar.YEAR),
                cal.get(java.util.Calendar.MONTH),
                cal.get(java.util.Calendar.DAY_OF_MONTH));
        datePicker.getDatePicker().setMaxDate(new Date().getTime());
        datePicker.setCancelable(false);
        datePicker.show();
    }

    DatePickerDialog.OnDateSetListener ondate = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            java.util.Calendar calendar = java.util.Calendar.getInstance(TimeZone.getDefault());
            calendar.set(year, monthOfYear, dayOfMonth);
            calendar.set(Calendar.HOUR_OF_DAY, 00);
            calendar.set(Calendar.MINUTE, 00);
            calendar.set(Calendar.SECOND, 00);
            Date setdate = calendar.getTime();
            SimpleDateFormat fmtOut = new SimpleDateFormat("dd MMM, yyyy");
            String dateString = fmtOut.format(setdate);
            adddate.setText(dateString);
            SimpleDateFormat dateFrmOut = new SimpleDateFormat("yyyy-MM-dd");
            date = dateFrmOut.format(setdate);
        }
    };

    public void getTodayDate() {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.HOUR_OF_DAY, 00);
        c.set(Calendar.MINUTE, 00);
        c.set(Calendar.SECOND, 00);
        Date setdate = c.getTime();
        SimpleDateFormat fmtOut = new SimpleDateFormat("dd MMM, yyyy");
        String dateString = fmtOut.format(setdate);
        adddate.setText(dateString);
        SimpleDateFormat dateFrmOut = new SimpleDateFormat("yyyy-MM-dd");
        date = dateFrmOut.format(setdate);
    }

    @OnClick(R.id.button3)
    public void searchStudent() {
        if (classid == null) {
            Utils.showSnackBar("Please select class first", parent);
        } else {
            dataList.clear();
            adapter.notifyDataSetChanged();
            ErpProgress.showProgressBar(this, "Please wait...");
            HashMap<String, String> params = new HashMap<String, String>();
            params.put("class_id", String.valueOf(classidsel));
            params.put("section",sectionid);
            if (date != null) {
                params.put("mdate", String.valueOf(date));
            }
            ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.GDSSTUDENT_ATTEND_URL, studentapiCallback, this, params);
        }
    }

    ApiHandler.ApiCallback studentapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            if (error == null) {
                try {
                    ArrayList<AttendBean> studattendList = new ArrayList<>();
                    ArrayList<AttendBean> attendList = AttendBean.parseAttendArray(jsonObject.getJSONArray(Constants.ATTEND_DATA));
                    if (jsonObject.has("stud_data")) {
                        studattendList = AttendBean.parseAttendArray(jsonObject.getJSONArray("stud_data"));
                        if (studattendList.size() > attendList.size()) {
                            for (int i = 0; i < studattendList.size(); i++) {
                                boolean check = true;
                                for (int j = 0; j < attendList.size(); j++) {
                                    if (studattendList.get(i).getStudent_id().equals(attendList.get(j).getStudent_id())) {
                                        check = false;
                                        break;
                                    }
                                }
                                if (check) {
                                    attendList.add(studattendList.get(i));
                                }
                            }
                        } else {
                            for (int i = 0; i < attendList.size(); i++) {
                                boolean check = true;
                                for (int j = 0; j < studattendList.size(); j++) {
                                    if (studattendList.get(j).getStudent_id().equals(attendList.get(i).getStudent_id())) {
                                        check = false;
                                        break;
                                    }
                                }
                                if (check) {
                                    attendList.remove(i);
                                    i--;
                                }
                            }
                        }
                    }

                    dataList.clear();
                    dataList.addAll(attendList);
                    scrollView.setVisibility(View.VISIBLE);
                    txtNorecord.setVisibility(View.GONE);
                    adapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                scrollView.setVisibility(View.GONE);
                txtNorecord.setVisibility(View.VISIBLE);
                Utils.showSnackBar(error.getMessage(), parent);
            }
        }
    };
    @OnClick(R.id.btnSubmit)
    public void submitAttendance() {
        addAttend();
    }
    private void addAttend() {
        ErpProgress.showProgressBar(this,"Please wait...");
        dataList =adapter.getAttendList();
        ArrayList<String> attendList = new ArrayList<>();
        ArrayList<String> id = new ArrayList<>();
        ArrayList<String> st_id = new ArrayList<>();
        ArrayList<String> st_name = new ArrayList<>();
        for (int i = 0; i < dataList.size(); i++) {
            attendList.add(dataList.get(i).getAttendance());
            st_id.add(dataList.get(i).getStudent_id());
            id.add(dataList.get(i).getId());
            st_name.add(dataList.get(i).getStudent_name());
        }
        Log.e("list: ",attendList.toString().trim()+","+id.toString().trim()+","+st_id.toString().trim()+","+st_name.toString().trim());
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("attend", String.valueOf(attendList));
        params.put("st_id", String.valueOf(st_id));
        params.put("id", String.valueOf(id));
        params.put("st_name", String.valueOf(st_name));
        params.put("class_id", String.valueOf(classidsel));

        Log.e("class_id", String.valueOf(classidsel));
        params.put("date", date);
        params.put("section_id",sectionid);
        Log.e("section_id",sectionid);
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.ADD_ST_ATTEND_URLGDS, apiCallback, this, params);

    }

    ApiHandler.ApiCallback apiCallback = new ApiHandler.ApiCallback() {

        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            if (error == null) {
                try {
                    Toast.makeText(AddAttendance.this,jsonObject.getString("msg"), Toast.LENGTH_SHORT).show();
                    finish();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("error", error.getMessage() + "");
                Utils.showSnackBar(error.getMessage(), parent);
            }
        }
    };


    @OnClick(R.id.imgBack)
    public void onBackclick() {
        finish();
    }

}
