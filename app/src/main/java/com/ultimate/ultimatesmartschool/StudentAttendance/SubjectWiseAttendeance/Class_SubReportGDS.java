package com.ultimate.ultimatesmartschool.StudentAttendance.SubjectWiseAttendeance;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetSequence;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.ultimate.ultimatesmartschool.BeanModule.ClassBean;
import com.ultimate.ultimatesmartschool.BeanModule.SectionBean;
import com.ultimate.ultimatesmartschool.BeanModule.SubjectBeancls;
import com.ultimate.ultimatesmartschool.BeanModule.User;
import com.ultimate.ultimatesmartschool.Homework.AdapterOfSubjectList;
import com.ultimate.ultimatesmartschool.Homework.SectionnewAdapter;
import com.ultimate.ultimatesmartschool.Login.LoginActivity;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.StudentAttendance.AttendBean;
import com.ultimate.ultimatesmartschool.StudentAttendance.ClassAdapterAttend;
import com.ultimate.ultimatesmartschool.StudentAttendance.ClassReportAdapter;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class Class_SubReportGDS extends AppCompatActivity {


    SharedPreferences sharedPreferences;
    ArrayList<ClassBean> classList = new ArrayList<>();
    @BindView(R.id.parent)
    RelativeLayout parent;
    //@BindView(R.id.spinner) Spinner spinnerClass;
    BottomSheetDialog mBottomSheetDialog;
    private int loaded = 0;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.txtSub)
    TextView txtSub;
    List<String> classidsel;
    @BindView(R.id.totalRecord)
    TextView totalRecord;

    ArrayList<SectionBean> sectionList = new ArrayList<>();

    @BindView(R.id.adddate)TextView adddate;
    public String date;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.textNorecord)
    TextView txtNorecord;
    private ArrayList<AttendBean> dataList;
    private ClassReportAdapter adapter;
//    @BindView(R.id.btnSubmit)
//    Button btnSubmit;

    @BindView(R.id.dialog)
    ImageView dialog;
    @BindView(R.id.sc)
    CardView scrollView;
    @BindView(R.id.filelayout)
    LinearLayout filelayout;

    Spinner spinnersection;
    Spinner spinnerClass;
    Spinner spinnersubject;

    ArrayList<SubjectBeancls> subjectList = new ArrayList<>();
    AdapterOfSubjectList adaptersub;
    String sub_id= "", sub_name= "" ,sectionid="",sectionname="",classid = "",className = "";

    int check=0;


    @RequiresApi(api = Build.VERSION_CODES.N)

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_class__sub_report_g_d_s);
        ButterKnife.bind(this);
        classidsel = new ArrayList<>();
        dataList = new ArrayList<>();
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new ClassReportAdapter(this, dataList);
        recyclerView.setAdapter(adapter);
        if (!restorePrefData()){
            setShowcaseView();
        }else {
            openDialog();
        }

    }

    private void setShowcaseView_aday() {
        new TapTargetSequence(this)
                .targets(TapTarget.forView(filelayout,"Date Button!","Tap the Date button to View Day-Wise Attendance.")
                        .outerCircleColor(R.color.light)
                        .outerCircleAlpha(0.96f)
                        .targetCircleColor(R.color.white)
                        .titleTextSize(20)
                        .titleTextColor(R.color.white)
                        .descriptionTextSize(14)
                        .descriptionTextColor(R.color.black)
                        .textColor(R.color.black)
                        .textTypeface(Typeface.SANS_SERIF)
                        .dimColor(R.color.black)
                        .drawShadow(true)
                        .cancelable(false)
                        .tintTarget(true)
                        .transparentTarget(true)
                        .targetRadius(60)).listener(new TapTargetSequence.Listener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onSequenceFinish() {
                // Toast.makeText(getApplicationContext(),"Sequence Finished",Toast.LENGTH_SHORT).show();
                savePrefData_aday();
                //openDialog();
            }

            @Override
            public void onSequenceStep(TapTarget lastTarget, boolean targetClicked) {
                // Toast.makeText(getActivity(),"GREAT!",Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSequenceCanceled(TapTarget lastTarget) {

            }
        }).start();
    }

    private void savePrefData_aday(){
        sharedPreferences=this.getSharedPreferences("boarding_pref_add_attend_vday",MODE_PRIVATE);
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putBoolean("IsFirstTimeVisit_add_attend_vday",true);
        editor.apply();
    }

    private boolean restorePrefData_aday(){
        sharedPreferences = this.getSharedPreferences("boarding_pref_add_attend_vday",MODE_PRIVATE);
        return sharedPreferences.getBoolean("IsFirstTimeVisit_add_attend_vday",false);
    }


    private void setShowcaseView() {
        new TapTargetSequence(this)
                .targets(TapTarget.forView(dialog,"Search Button!","Tap the Search button to to view Subject-Wise Attendance Report.")
                        .outerCircleColor(R.color.light)
                        .outerCircleAlpha(0.96f)
                        .targetCircleColor(R.color.white)
                        .titleTextSize(20)
                        .titleTextColor(R.color.white)
                        .descriptionTextSize(14)
                        .descriptionTextColor(R.color.black)
                        .textColor(R.color.black)
                        .textTypeface(Typeface.SANS_SERIF)
                        .dimColor(R.color.black)
                        .drawShadow(true)
                        .cancelable(false)
                        .tintTarget(true)
                        .transparentTarget(true)
                        .targetRadius(60)).listener(new TapTargetSequence.Listener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onSequenceFinish() {
                // Toast.makeText(getApplicationContext(),"Sequence Finished",Toast.LENGTH_SHORT).show();
                savePrefData();
                openDialog();
            }

            @Override
            public void onSequenceStep(TapTarget lastTarget, boolean targetClicked) {
                // Toast.makeText(getActivity(),"GREAT!",Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSequenceCanceled(TapTarget lastTarget) {

            }
        }).start();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        commonBack();
    }

    private void commonBack() {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(Class_SubReportGDS.this);
        builder1.setMessage("Are you sure, you want to exit? ");
        builder1.setCancelable(false);
        builder1.setPositiveButton(
                "Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finish();
                    }
                });
        builder1.setNegativeButton(
                "No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

    private void savePrefData(){
        sharedPreferences=this.getSharedPreferences("boarding_pref_add_attend_day",MODE_PRIVATE);
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putBoolean("IsFirstTimeVisit_add_attend_day",true);
        editor.apply();
    }

    private boolean restorePrefData(){
        sharedPreferences = this.getSharedPreferences("boarding_pref_add_attend_day",MODE_PRIVATE);
        return sharedPreferences.getBoolean("IsFirstTimeVisit_add_attend_day",false);
    }


    @RequiresApi(api = Build.VERSION_CODES.N)
    private void openDialog() {

        mBottomSheetDialog = new BottomSheetDialog(this);
        final View sheetView = getLayoutInflater().inflate(R.layout.search_dialog, null);
        mBottomSheetDialog.setContentView(sheetView);
        mBottomSheetDialog.setCancelable(false);

        spinnerClass=(Spinner) sheetView.findViewById(R.id.spinnerClass);
        spinnersection=(Spinner) sheetView.findViewById(R.id.spinnersection);
        spinnersubject=(Spinner) sheetView.findViewById(R.id.spinnersubject);
        ImageView close=(ImageView) sheetView.findViewById(R.id.close);
        TextView txtSetup=(TextView) sheetView.findViewById(R.id.txtSetup);

        txtSetup.setText("View Day-Wise \nAttendance");

        getTodayDate();
        //txtTitle.setText("Mark Attendance");
        fetchClass();

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //assignData();
                if (check==0) {
                    finish();
                    mBottomSheetDialog.dismiss();
                }else {
                    mBottomSheetDialog.dismiss();
                }
            }
        });


        Button btnYes= (Button) sheetView.findViewById(R.id.btnYes);


        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (classid.equalsIgnoreCase(""))  {
                    Toast.makeText(Class_SubReportGDS.this,"Kindly Select Class!", Toast.LENGTH_SHORT).show();
                }  else if (sectionid.equalsIgnoreCase("")){
                    Toast.makeText(Class_SubReportGDS.this,"Kindly Select Class Section!", Toast.LENGTH_SHORT).show();
                }else if (sub_id.equalsIgnoreCase("")){
                    Toast.makeText(Class_SubReportGDS.this,"Kindly Select Class Subject!", Toast.LENGTH_SHORT).show();
                }else {
                    check++;
                    searchStudent(date);
                    setCommonData();
                }

            }
        });
        mBottomSheetDialog.show();

    }

    private void setCommonData() {
        filelayout.setVisibility(View.VISIBLE);
        if (!restorePrefData_aday()){
            setShowcaseView_aday();
        }
        txtTitle.setText(className+"("+sectionname+")");
        txtSub.setText(sub_name);
    }


    private void fetchClass() {
        if (loaded == 0) {
            ErpProgress.showProgressBar(this, "Please wait...");
        }
        loaded++;

        HashMap<String, String> params = new HashMap<String, String>();
        params.put("check","true");
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.CLASSLIST_URL, classapiCallback,getApplicationContext(), params);
    }


    ApiHandler.ApiCallback classapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            if (error == null) {
                try {
                    classList = ClassBean.parseClassArray(jsonObject.getJSONArray(Constants.CLASSDATA));
                    ClassAdapterAttend adapter = new ClassAdapterAttend(getApplicationContext(), classList,0);
                    spinnerClass.setAdapter(adapter);
                    spinnerClass.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if (i > 0) {
                                sub_id= "";
                                sub_name= "" ;
                                sectionid="";
                                sectionname="";
                                classid = "";
                                className = "";
                                classid = classList.get(i - 1).getId();
                                className= classList.get(i - 1).getName();
                                fetchSection();
                                fetchSubject();
                            }

                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_LONG).show();
                // Utils.showSnackBar(error.getMessage(), view.findViewById(R.id.parent));
            }
        }
    };


    private void fetchSection() {
        HashMap<String, String> params = new HashMap<String, String>();
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.SECTIONLIST_URL, sectionapiCallback, this, params);
    }

    ApiHandler.ApiCallback sectionapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            if (error == null) {
                try {
                    sectionList = SectionBean.parseCommonArray(jsonObject.getJSONArray("hostelroom_data"));
                    SectionnewAdapter adapter = new SectionnewAdapter(Class_SubReportGDS.this, sectionList);
                    spinnersection.setAdapter(adapter);
                    spinnersection.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if (i > 0) {
                                sectionid = sectionList.get(i - 1).getSection_id();
                                sectionname = sectionList.get(i - 1).getSection_name();
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Utils.showSnackBar(error.getMessage(), parent);
                if (error.getStatusCode() == 405) {
                    User.logout();
                    startActivity(new Intent(Class_SubReportGDS.this, LoginActivity.class));
                    finish();
                }
            }
        }
    };


    private void fetchSubject() {
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("id", classid);
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.SubjectList, subapiCallback, this, params);

    }

    ApiHandler.ApiCallback subapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            /* */

            if (error == null) {
                try {

                    subjectList =   SubjectBeancls.parseClassArray(jsonObject.getJSONArray("sub_data"));

                    adaptersub = new AdapterOfSubjectList(Class_SubReportGDS.this, subjectList);
                    spinnersubject.setAdapter(adaptersub);
                    spinnersubject.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if (i != 0) {
                                sub_name = subjectList.get(i - 1).getName();
                                sub_id = subjectList.get(i - 1).getId();

                            }

                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    };

    @RequiresApi(api = Build.VERSION_CODES.N)
    @OnClick(R.id.dialog)
    public void dialogggg() {
        openDialog();
    }

    @OnClick(R.id.filelayout)
    public void fetchDate() {
        java.util.Calendar cal = java.util.Calendar.getInstance(TimeZone.getDefault()); // Get current date
        DatePickerDialog datePicker = new DatePickerDialog(this,
                R.style.MyDatePickerDialogTheme, ondate,
                cal.get(java.util.Calendar.YEAR),
                cal.get(java.util.Calendar.MONTH),
                cal.get(java.util.Calendar.DAY_OF_MONTH));
        datePicker.getDatePicker().setMaxDate(new Date().getTime());
        datePicker.setCancelable(false);
        datePicker.show();
    }

    DatePickerDialog.OnDateSetListener ondate = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            java.util.Calendar calendar = java.util.Calendar.getInstance(TimeZone.getDefault());
            calendar.set(year, monthOfYear, dayOfMonth);
            calendar.set(Calendar.HOUR_OF_DAY, 00);
            calendar.set(Calendar.MINUTE, 00);
            calendar.set(Calendar.SECOND, 00);
            Date setdate = calendar.getTime();
            SimpleDateFormat fmtOut = new SimpleDateFormat("dd MMM, yyyy");
            String dateString = fmtOut.format(setdate);
            adddate.setText(dateString);
            SimpleDateFormat dateFrmOut = new SimpleDateFormat("yyyy-MM-dd");
            date = dateFrmOut.format(setdate);
            searchStudent(date);
        }
    };

    public void getTodayDate() {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.HOUR_OF_DAY, 00);
        c.set(Calendar.MINUTE, 00);
        c.set(Calendar.SECOND, 00);
        Date setdate = c.getTime();
        SimpleDateFormat fmtOut = new SimpleDateFormat("dd MMM, yyyy");
        String dateString = fmtOut.format(setdate);
        adddate.setText(dateString);
        SimpleDateFormat dateFrmOut = new SimpleDateFormat("yyyy-MM-dd");
        date = dateFrmOut.format(setdate);
    }



    public void searchStudent(String date) {
        if (classid == null) {
            Utils.showSnackBar("Please select class first", parent);
        } else {
            dataList.clear();
            adapter.notifyDataSetChanged();
            ErpProgress.showProgressBar(this, "Please wait...");
            HashMap<String, String> params = new HashMap<String, String>();
            params.put("class_id", classid);
            params.put("section",sectionid);
            params.put("sub_id",sub_id);
            params.put("type","1");
            if (date != null) {
                params.put("mdate", String.valueOf(date));
            }
            ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.STUDENT_ATTEND_LISTGDS_SUB, studentapiCallback, this, params);
        }
    }

    ApiHandler.ApiCallback studentapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            mBottomSheetDialog.dismiss();
            if (error == null) {
                try {
                    if (dataList != null){
                        dataList.clear();
                    }
                    ArrayList<AttendBean> attendList = AttendBean.parseAttendArray(jsonObject.getJSONArray(Constants.ATTEND_DATA));
                    dataList.addAll(attendList);
                    scrollView.setVisibility(View.VISIBLE);
                    txtNorecord.setVisibility(View.GONE);
                    adapter.notifyDataSetChanged();
                    totalRecord.setText("Total Entries:- "+String.valueOf(attendList.size()));


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                totalRecord.setText("Total Entries:- 0");
                scrollView.setVisibility(View.GONE);
                txtNorecord.setVisibility(View.VISIBLE);
                Utils.showSnackBar(error.getMessage(), parent);
            }
        }
    };

    @OnClick(R.id.imgBack)
    public void onBackclick() {
//        commonBack();
        finish();
    }

}