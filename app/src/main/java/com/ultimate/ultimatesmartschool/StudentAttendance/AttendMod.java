package com.ultimate.ultimatesmartschool.StudentAttendance;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class AttendMod {

    private static String DATE = "date";
    private static String ATTEND = "attend";
    private static String REMARK="remarks";
    /**
     * date : 2018-03-02
     * attend : P
     */
    private String date;
    private String attend="p";
    private String day;
    private String holiday;



    private String remarks;
    private String ab;
    private String lv;
    private String pr;
    private String hl;
    private String otl;
    private String holiday_date;
    private String twcount;
    private String tprcount;
    private String prtcont;
    private String sunday;
    private String totalworkcountwoh;

    public String getTotalworkcountwoh() {
        return totalworkcountwoh;
    }

    public void setTotalworkcountwoh(String totalworkcountwoh) {
        this.totalworkcountwoh = totalworkcountwoh;
    }

    public String getL_remarks() {
        return l_remarks;
    }

    public void setL_remarks(String l_remarks) {
        this.l_remarks = l_remarks;
    }


    private String l_remarks;

    public String getSal() {
        return sal;
    }

    public void setSal(String sal) {
        this.sal = sal;
    }

    private String sal;

    private String hlv;

    public String getHlv() {
        return hlv;
    }

    public void setHlv(String hlv) {
        this.hlv = hlv;
    }

    public String getStholiday() {
        return stholiday;
    }

    public void setStholiday(String stholiday) {
        this.stholiday = stholiday;
    }

    private String stholiday;

    public String getSunday() {
        return sunday;
    }

    public void setSunday(String sunday) {
        this.sunday = sunday;
    }

    public String getPrtcont() {
        return prtcont;
    }

    public void setPrtcont(String prtcont) {
        this.prtcont = prtcont;
    }

    public String getTwcount() {
        return twcount;
    }

    public void setTwcount(String twcount) {
        this.twcount = twcount;
    }

    public String getTprcount() {
        return tprcount;
    }

    public void setTprcount(String tprcount) {
        this.tprcount = tprcount;
    }

    public String getHoliday_date() {
        return holiday_date;
    }

    public void setHoliday_date(String holiday_date) {
        this.holiday_date = holiday_date;
    }

    public String getLv() {
        return lv;
    }

    public void setLv(String lv) {
        this.lv = lv;
    }

    public String getPr() {
        return pr;
    }

    public void setPr(String pr) {
        this.pr = pr;
    }

    public String getHl() {
        return hl;
    }

    public void setHl(String hl) {
        this.hl = hl;
    }

    public String getOtl() {
        return otl;
    }

    public void setOtl(String otl) {
        this.otl = otl;
    }

    public String getAb() {
        return ab;
    }

    public void setAb(String ab) {
        this.ab = ab;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getDate() {
        return date;
    }
    public void setDate(String date) {
        this.date = date;
    }
    public String getAttend() {
        return attend;
    }
    public void setAttend(String attend) {
        this.attend = attend;
    }

    public static AttendMod parseAttendObject(JSONObject jsonObject) {
        AttendMod attendObj = new AttendMod();
        try {
            if (jsonObject.has(DATE) && !jsonObject.getString(DATE).isEmpty() && !jsonObject.getString(DATE).equalsIgnoreCase("null")) {
                attendObj.setDate(jsonObject.getString(DATE));
                try {
                    String input_date = jsonObject.getString(DATE);
                    SimpleDateFormat format1 = new SimpleDateFormat("yyyy-mm-dd");
                    Date dt1 = format1.parse(input_date);
                    DateFormat format2 = new SimpleDateFormat("dd");
                    String finalDay = format2.format(dt1);
                    attendObj.setDay(finalDay);


                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
            if (jsonObject.has(ATTEND) && !jsonObject.getString(ATTEND).isEmpty() && !jsonObject.getString(ATTEND).equalsIgnoreCase("null")) {
                attendObj.setAttend(jsonObject.getString(ATTEND));
            }

            if (jsonObject.has(REMARK) && !jsonObject.getString(REMARK).isEmpty() && !jsonObject.getString(REMARK).equalsIgnoreCase("null")) {
                attendObj.setRemarks(jsonObject.getString(REMARK));
            }
            if (jsonObject.has("ab") && !jsonObject.getString("ab").isEmpty() && !jsonObject.getString("ab").equalsIgnoreCase("null")) {
                attendObj.setAb(jsonObject.getString("ab"));
            }
            if (jsonObject.has("lv") && !jsonObject.getString("lv").isEmpty() && !jsonObject.getString("lv").equalsIgnoreCase("null")) {
                attendObj.setLv(jsonObject.getString("lv"));
            }
            if (jsonObject.has("pr") && !jsonObject.getString("pr").isEmpty() && !jsonObject.getString("pr").equalsIgnoreCase("null")) {
                attendObj.setPr(jsonObject.getString("pr"));
            }
            if (jsonObject.has("hl") && !jsonObject.getString("hl").isEmpty() && !jsonObject.getString("hl").equalsIgnoreCase("null")) {
                attendObj.setHl(jsonObject.getString("hl"));
            }
            if (jsonObject.has("otl") && !jsonObject.getString("otl").isEmpty() && !jsonObject.getString("otl").equalsIgnoreCase("null")) {
                attendObj.setOtl(jsonObject.getString("otl"));
            }
            if (jsonObject.has("hlv") && !jsonObject.getString("hlv").isEmpty() && !jsonObject.getString("hlv").equalsIgnoreCase("null")) {
                attendObj.setHlv(jsonObject.getString("hlv"));
            }
            if (jsonObject.has("sal") && !jsonObject.getString("sal").isEmpty() && !jsonObject.getString("sal").equalsIgnoreCase("null")) {
                attendObj.setSal(jsonObject.getString("sal"));
            }

            if (jsonObject.has("l_remarks") && !jsonObject.getString("l_remarks").isEmpty() && !jsonObject.getString("l_remarks").equalsIgnoreCase("null")) {
                attendObj.setL_remarks(jsonObject.getString("l_remarks"));
            }

            if (jsonObject.has("twcount") && !jsonObject.getString("twcount").isEmpty() && !jsonObject.getString("twcount").equalsIgnoreCase("null")) {
                attendObj.setTwcount(jsonObject.getString("twcount"));
            }
            if (jsonObject.has("tprcount") && !jsonObject.getString("tprcount").isEmpty() && !jsonObject.getString("tprcount").equalsIgnoreCase("null")) {
                attendObj.setTprcount(jsonObject.getString("tprcount"));
            }

            if (jsonObject.has("prtcont") && !jsonObject.getString("prtcont").isEmpty() && !jsonObject.getString("prtcont").equalsIgnoreCase("null")) {
                attendObj.setPrtcont(jsonObject.getString("prtcont"));
            }
            if (jsonObject.has("stholiday") && !jsonObject.getString("stholiday").isEmpty() && !jsonObject.getString("stholiday").equalsIgnoreCase("null")) {
                attendObj.setStholiday(jsonObject.getString("stholiday"));
            }
            if (jsonObject.has("totalworkcountwoh") && !jsonObject.getString("totalworkcountwoh").isEmpty() && !jsonObject.getString("totalworkcountwoh").equalsIgnoreCase("null")) {
                attendObj.setTotalworkcountwoh(jsonObject.getString("totalworkcountwoh"));
            }

            if (jsonObject.has("holiday_date") && !jsonObject.getString("holiday_date").isEmpty() && !jsonObject.getString("holiday_date").equalsIgnoreCase("null")) {
                attendObj.setHoliday_date(jsonObject.getString("holiday_date"));


                try {
                    String input_date = jsonObject.getString("holiday_date");
                    SimpleDateFormat format1 = new SimpleDateFormat("yyyy-mm-dd");
                    Date dt1 = format1.parse(input_date);
                    DateFormat format2 = new SimpleDateFormat("dd");
                    String finalDay = format2.format(dt1);
                    attendObj.setHoliday(finalDay);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        }catch (JSONException e) {
            e.printStackTrace();
        }
        return attendObj;
    }
    public static ArrayList<AttendMod> parseAttendArray(JSONArray jsonArray) {
        try {

            ArrayList<AttendMod> arrayList = new ArrayList<>();
            for (int i = 0; i < jsonArray.length(); i++) {
                AttendMod attendObj = parseAttendObject(jsonArray.getJSONObject(i));
                arrayList.add(attendObj);
            }
            return arrayList;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getHoliday() {
        return holiday;
    }

    public void setHoliday(String holiday) {
        this.holiday = holiday;
    }
}
