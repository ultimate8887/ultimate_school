package com.ultimate.ultimatesmartschool;

import androidx.multidex.MultiDexApplication;

import com.ultimate.ultimatesmartschool.Utility.Utils;

public class UltimateSchoolApp extends MultiDexApplication {

    private static UltimateSchoolApp mInstance;
    private static final String TAG = UltimateSchoolApp.class.getName();

    @Override
    public void onCreate() {
        super.onCreate();
//        Fabric.with(this, new Crashlytics());
//        FirebaseMessaging.getInstance().setAutoInitEnabled(true);
        mInstance = this;


    }

    public static synchronized UltimateSchoolApp getInstance() {
        return mInstance;
    }

    @Override
    public void onTerminate() {
        mInstance = null;
        super.onTerminate();
    }

    @Override
    public void onLowMemory() {
        Utils.deleteCache(this);
        super.onLowMemory();
    }

}
