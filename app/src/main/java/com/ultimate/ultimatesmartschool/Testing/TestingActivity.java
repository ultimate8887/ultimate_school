package com.ultimate.ultimatesmartschool.Testing;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.ultimate.ultimatesmartschool.R;

import java.util.ArrayList;
import java.util.Arrays;

public class TestingActivity extends AppCompatActivity {
    ArrayList personNames = new ArrayList<>(Arrays.asList("Person 1", "Person 2", "Person 3", "Person 4", "Person 5", "Person 6", "Person 7","Person 8", "Person 9", "Person 10", "Person 11", "Person 12", "Person 13", "Person 14"));
    ArrayList personImages = new ArrayList<>(Arrays.asList(R.drawable.query, R.drawable.new_message_icon, R.drawable.attendance, R.drawable.homework, R.drawable.classwork, R.drawable.leave, R.drawable.syllabus,R.drawable.query, R.drawable.gatepass, R.drawable.hostel, R.drawable.gatepass, R.drawable.gatepass, R.drawable.gatepass, R.drawable.gatepass));
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_testing);
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        // set a LinearLayoutManager with default vertical orientation
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        // call the constructor of CustomAdapter to send the reference and data to Adapter
        CustomAdapter customAdapter = new CustomAdapter(TestingActivity.this, personNames,personImages);
        recyclerView.setAdapter(customAdapter);
    }
}
