package com.ultimate.ultimatesmartschool.Banner;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.squareup.picasso.Picasso;
import com.ultimate.ultimatesmartschool.Activity.NewsData;
import com.ultimate.ultimatesmartschool.R;


import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Banner_adapter extends RecyclerView.Adapter<Banner_adapter.Viewholder> {

    Context context;
    ArrayList<NewsData> securitylist;

    BannerCallBack bannerCallBack;

    public Banner_adapter(ArrayList<NewsData> securitylist,Context context,BannerCallBack bannerCallBack) {
        this.context=context;
        this.securitylist=securitylist;
        this.bannerCallBack=bannerCallBack;
    }

    public interface BannerCallBack{
        void bannerClick(NewsData newsData);
    }

    @NonNull
    @Override
    public Banner_adapter.Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.ban_adapt_layout,parent,false);
        Banner_adapter.Viewholder viewholder=new Banner_adapter.Viewholder(view);
        return viewholder;
    }


    @Override
    public void onBindViewHolder(@NonNull Banner_adapter.Viewholder holder, @SuppressLint("RecyclerView") int position) {
//        holder.parent.setAnimation(AnimationUtils.loadAnimation(context,R.anim.fade_scale_animation));

        holder.txtSub.setText(securitylist.get(position).getDescription());

        holder.txttitle.setText(securitylist.get(position).getTitle());

        holder.txtwriter.setText(securitylist.get(position).getDate());

        if(securitylist.get(position).getImage()!=null) {
            Picasso.get().load(securitylist.get(position).getImage()).placeholder(R.color.silver).into(holder.video_img);
        }
        holder.txtPdf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bannerCallBack.bannerClick(securitylist.get(position));
            }
        });
    }

    @Override
    public int getItemCount() {
        return securitylist.size();
    }

    public void setsecuList(ArrayList<NewsData> securitylist) {
        this.securitylist = securitylist;
    }

    public class Viewholder extends RecyclerView.ViewHolder {
        @BindView(R.id.txtPdf)
        RelativeLayout txtPdf;
        @BindView(R.id.txttitle)
        TextView txttitle;
        @BindView(R.id.txtSub)
        TextView txtSub;
        @BindView(R.id.txtwriter)
        TextView txtwriter;
        @BindView(R.id.shadhow)
        ImageView video_img;

        public Viewholder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }

}
