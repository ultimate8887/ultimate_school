package com.ultimate.ultimatesmartschool.Banner;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.ultimate.ultimatesmartschool.Security.CheckInDetailFrag;
import com.ultimate.ultimatesmartschool.Security.CheckOutDetailFrag;

public class BannerPager extends FragmentPagerAdapter {

    int tabCount;
    // tab titles
    private String[] tabTitles = new String[]{"Active Banner", "Banner List"};

    public BannerPager(FragmentManager fm) {
        super(fm);
        this.tabCount =2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabTitles[position];
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                ActiveBannerFragment tab1 = new ActiveBannerFragment();
                return tab1;
            case 1:
                BannerListFragment tab2 = new BannerListFragment();
                return tab2;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return tabCount;
    }
}
