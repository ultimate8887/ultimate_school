package com.ultimate.ultimatesmartschool.Banner;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetSequence;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.squareup.picasso.Picasso;

import com.ultimate.ultimatesmartschool.Activity.NewsData;
import com.ultimate.ultimatesmartschool.Activity.SchoolInfoActivity;
import com.ultimate.ultimatesmartschool.ForCamera.IPickResult;
import com.ultimate.ultimatesmartschool.ForCamera.PickImageDialog;
import com.ultimate.ultimatesmartschool.ForCamera.PickResult;
import com.ultimate.ultimatesmartschool.ForCamera.PickSetup;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.CommonProgress;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;
import com.ultimate.ultimatesmartschool.Utility.ErpProgressVert;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class BannerActivityNew extends AppCompatActivity implements Banner_adapter.BannerCallBack, IPickResult {


    SharedPreferences sharedPreferences;
    @BindView(R.id.imgBack)
    ImageView back;

    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.parent)
    RelativeLayout parent;
    @BindView(R.id.p_recyclerview)
    RecyclerView r1;
    @BindView(R.id.v_recyclerview)
    RecyclerView v_recyclerview;
    Spinner spinnerClass,spinnersection;

    @BindView(R.id.oneDay)
    RelativeLayout p_day;

    @BindView(R.id.multiDay)
    RelativeLayout v_day;

    @BindView(R.id.img1)
    ImageView img1;
    @BindView(R.id.img2)
    ImageView img2;

    String apply="pending";
    Dialog mBottomSheetDialog;
    @BindView(R.id.p_lyt)
    LinearLayout p_lyt;

    @BindView(R.id.v_lyt)
    LinearLayout v_lyt;

    @BindView(R.id.cal_img)
    ImageView cal_img;
    @BindView(R.id.today_date)
    TextView today_date;
    @BindView(R.id.dialog)
    ImageView contact_support;
    int check=0;
    private LinearLayoutManager layoutManager,layoutManager1;
    //    @BindView(R.id.dialog)
//    ImageView contact_support;
    Animation animation;
    @BindView(R.id.textNorecord)
    TextView txtNorecord;
    @BindView(R.id.totalRecord)
    TextView totalRecord;

    CommonProgress commonProgress;
    String classid = "",className="";
    String from_date = "";
    private int loaded = 0;

    Banner_adapter adminlistadapter;
    ArrayList<NewsData> adminlist = new ArrayList<>();

    ImageView clickimage, visitimage;
    EditText title, visitPurpose;
    private Bitmap bitmap;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_banner_new);
        ButterKnife.bind(this);
        txtTitle.setText("Banners Detail");
        commonProgress=new CommonProgress(this);
        animation = AnimationUtils.loadAnimation(this, R.anim.btn_blink_animation);

        if (!restorePrefData()){
            setShowcaseView();
        }
        layoutManager = new LinearLayoutManager(this);
        r1.setLayoutManager(layoutManager);
        adminlistadapter = new Banner_adapter(adminlist, this, this);
        r1.setAdapter(adminlistadapter);

        fetchlist("");
    }

    private void fetchlist(String tag) {
//        if (loaded == 0) {
//            ErpProgress.showProgressBar(getContext(), "Please wait...");
//        }
        loaded++;
        HashMap<String, String> params = new HashMap<>();
//        params.put("user_id", User.getCurrentUser().getId());
//        params.put("date", "");
        params.put("check", tag);
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.NEWS_URL, apiCallback, this, params);
    }

    ApiHandler.ApiCallback apiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            // txtupload.setVisibility(View.VISIBLE);
            if (error == null) {
                try {
                    if (adminlist != null) {
                        adminlist.clear();
                    }
                    JSONArray jsonArray = jsonObject.getJSONArray("news_data");
                    Log.e("jsonArray", String.valueOf(jsonArray));
                    adminlist = NewsData.parseNewsDataArrayyy(jsonArray);
                    if (adminlist.size() > 0) {
                        adminlistadapter.setsecuList(adminlist);
                        totalRecord.setText("Total Entries:- " + String.valueOf(adminlist.size()));
                        adminlistadapter.notifyDataSetChanged();
                        txtNorecord.setVisibility(View.GONE);
                    } else {
                        totalRecord.setText("Total Entries:- " + String.valueOf(adminlist.size()));
                        adminlistadapter.setsecuList(adminlist);
                        adminlistadapter.notifyDataSetChanged();
                        txtNorecord.setVisibility(View.VISIBLE);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                txtNorecord.setVisibility(View.VISIBLE);
                totalRecord.setText("Total Entries:- " + String.valueOf(adminlist.size()));
                //   Utility.showSnackBar(error.getMessage(), parent);

            }
        }
    };

    @OnClick(R.id.oneDay)
    public void oneDay() {
        apply="pending";
        img1.setVisibility(View.VISIBLE);
        img2.setVisibility(View.GONE);
        v_day.setBackgroundColor(Color.parseColor("#00000000"));
        p_day.setBackgroundColor(Color.parseColor("#66000000"));
        fetchlist("");
    }

    private void commonCode() {
        apply="verified";
        img2.setVisibility(View.VISIBLE);
        img1.setVisibility(View.GONE);
        p_day.setBackgroundColor(Color.parseColor("#00000000"));
        v_day.setBackgroundColor(Color.parseColor("#66000000"));
        fetchlist("list");
    }

    @OnClick(R.id.multiDay)
    public void multiDay() {
        commonCode();
    }


    private void openDialog(String tag, NewsData newsData) {
        bitmap=null;
        mBottomSheetDialog = new BottomSheetDialog(this);
        final View sheetView = getLayoutInflater().inflate(R.layout.add_banner_dialog, null);
        mBottomSheetDialog.setContentView(sheetView);

        mBottomSheetDialog.setCancelable(true);
        TextView txtSetup=(TextView) sheetView.findViewById(R.id.txtSetup);
        clickimage=(ImageView) sheetView.findViewById(R.id.clickimage);
        visitimage=(ImageView) sheetView.findViewById(R.id.visitimage);
        title=(EditText) sheetView.findViewById(R.id.title);
        visitPurpose=(EditText) sheetView.findViewById(R.id.visitPurpose);

        if (tag.equalsIgnoreCase("update")) {
            txtSetup.setText("Update Banner");
            title.setText(newsData.getTitle());
            visitPurpose.setText(newsData.getDescription());
            if (newsData.getImage() != null) {
                Picasso.get().load(newsData.getImage()).placeholder(R.color.silver).into(visitimage);
            }
        }else{
            txtSetup.setText("Upload New Banner");
        }
//
//        txtSetup=(TextView) sheetView.findViewById(R.id.txtSetup);
//        txtSetup.setText("Assign Subject");
//        classSubject=(RelativeLayout) sheetView.findViewById(R.id.classSubject);
//        classSubject.setVisibility(View.VISIBLE);
//        fetchStafffffff();
//        fetchclasslist();

        clickimage.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View view) {
                pick_img();
                //  assignData();
//                ImagePicker.Companion.with(BannerActivityNew.this)
//                        .galleryOnly()
//                        .crop()	    			//Crop image(Optional), Check Customization for more option
//                        .compress(1024)			//Final image size will be less than 1 MB(Optional)
//                        .maxResultSize(1080, 1080)	//Final image resolution will be less than 1080 x 1080(Optional)
//                        .start();
            }
        });

        Button btnYes= (Button) sheetView.findViewById(R.id.btnYes);
        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (tag.equalsIgnoreCase("add")) {
                    if (bitmap == null) {
                        Toast.makeText(BannerActivityNew.this, "Kindly Add Banner Image", Toast.LENGTH_SHORT).show();
                        //errorMsg = "Please enter Title";
                    } else{
                        assignData(tag,null);
                    }

                }else{
                    assignData(tag,newsData);
                }
            }
        });
        mBottomSheetDialog.show();

    }

    private void assignData(String tag, NewsData newsData) {

        if (title.getText().toString().trim().length() <= 0) {
            Toast.makeText(BannerActivityNew.this, "Kindly Enter Banner Title", Toast.LENGTH_SHORT).show();
            //errorMsg = "Please enter Title";
        } else if (visitPurpose.getText().toString().trim().length() <= 0) {
            Toast.makeText(BannerActivityNew.this, "Kindly Enter Banner Description", Toast.LENGTH_SHORT).show();
        } else{
            ErpProgressVert.showProgressBar(this, "Please wait");
            HashMap<String, String> params = new HashMap<>();
            params.put("title", title.getText().toString());
            params.put("desc", visitPurpose.getText().toString());
            if (tag.equalsIgnoreCase("add")) {
                params.put("check", "add");
            }else{
                params.put("check", "update");
                params.put("b_id", newsData.getId());
            }

            if (bitmap != null) {
                String encoded = Utils.encodeToBase64(bitmap, Bitmap.CompressFormat.JPEG, 50);
                encoded = String.format("data:image/jpeg;base64,%s", encoded);
                params.put("image", encoded);
            }

            ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.ADDBANNER_URL, new ApiHandler.ApiCallback() {
                @Override
                public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                    ErpProgressVert.cancelProgressBar();
                    mBottomSheetDialog.dismiss();
                    if (error == null) {
                        //Toast.makeText(BannerActivityNew.this, jsonObject.getString("msg"), Toast.LENGTH_SHORT).show();
                        if (tag.equalsIgnoreCase("add")) {
                            Utils.showSnackBar("Created successfully", parent);
                        }else{
                            Utils.showSnackBar("Updated successfully", parent);
                        }
                        bitmap=null;
                        commonCode();
                    } else {
                        Toast.makeText(BannerActivityNew.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            }, this, params);

        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @SuppressLint("ResourceType")
    private void pick_img() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.UPSIDE_DOWN_CAKE) { // Android 14+ (API 34)
           onImageViewClick();

        } else  if (Build.VERSION.SDK_INT > Build.VERSION_CODES.S) {
                if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_MEDIA_IMAGES)
                        != PackageManager.PERMISSION_GRANTED) {

                    if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_MEDIA_IMAGES)) {
                        Toast.makeText(this, "Please provide storage permission from app settings", Toast.LENGTH_LONG).show();
                    } else {

                        ActivityCompat.requestPermissions(this,
                                new String[]{Manifest.permission.READ_MEDIA_IMAGES},
                                1);
                    }
                } else {

                    onImageViewClick();
                }

            } else {
                if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {

                    if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                        Toast.makeText(this, "Please provide storage permission from app settings", Toast.LENGTH_LONG).show();
                    } else {

                        ActivityCompat.requestPermissions(this,
                                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                                1);
                    }
                } else {
                    onImageViewClick();

                }
        }
    }

    private void onImageViewClick() {
        // type = 1;
        PickSetup setup = new PickSetup();
        // super.customize(setup);

        PickImageDialog.build(setup)
                //.setOnClick(this)
                .show(this);
    }

    @Override
    public void onPickResult(PickResult r) {
        if (r.getError() == null) {
            //If you want the Bitmap.
            visitimage.setImageBitmap(r.getBitmap());
            bitmap= r.getBitmap();

        } else {
            //Handle possible errors
            //TODO: do what you have to do with r.getError();
            Toast.makeText(this, r.getError().getMessage(), Toast.LENGTH_LONG).show();
        }


    }

//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        if (resultCode == Activity.RESULT_OK) {
//            // Uri object will not be null for RESULT_OK
//            Uri imageUri = data.getData();
//            try {
//                bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), imageUri);
//                visitimage.setImageBitmap(bitmap);
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//
//        } else if (resultCode == ImagePicker.RESULT_ERROR) {
//            Toast.makeText(this, ImagePicker.Companion.getError(data), Toast.LENGTH_SHORT).show();
//        } else {
//            Toast.makeText(this, "Selection Cancelled", Toast.LENGTH_SHORT).show();
//        }
//    }

    @OnClick(R.id.dialog)
    public void contact_support() {
        openDialog("add", null);
    }

    private void savePrefData(){
        sharedPreferences= BannerActivityNew.this.getSharedPreferences("boarding_pref3",MODE_PRIVATE);
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putBoolean("IsFirstTimeVisit3",true);
        editor.apply();
    }

    private boolean restorePrefData(){
        sharedPreferences = BannerActivityNew.this.getSharedPreferences("boarding_pref3",MODE_PRIVATE);
        return sharedPreferences.getBoolean("IsFirstTimeVisit3",false);
    }

    private void setShowcaseView() {
        new TapTargetSequence(this)
                .targets(TapTarget.forView(contact_support,"Add Banner Button!","Tap the add banner button to upload new banner.")
                        .outerCircleColor(R.color.light)
                        .outerCircleAlpha(0.96f)
                        .targetCircleColor(R.color.white)
                        .titleTextSize(20)
                        .titleTextColor(R.color.white)
                        .descriptionTextSize(14)
                        .descriptionTextColor(R.color.black)
                        .textColor(R.color.black)
                        .textTypeface(Typeface.SANS_SERIF)
                        .dimColor(R.color.black)
                        .drawShadow(true)
                        .cancelable(false)
                        .tintTarget(true)
                        .transparentTarget(true)
                        .targetRadius(60)).listener(new TapTargetSequence.Listener() {
                    @Override
                    public void onSequenceFinish() {
                        // Toast.makeText(getApplicationContext(),"Sequence Finished",Toast.LENGTH_SHORT).show();
                        savePrefData();
                        openDialog("add", null);
                    }

                    @Override
                    public void onSequenceStep(TapTarget lastTarget, boolean targetClicked) {
                        // Toast.makeText(getActivity(),"GREAT!",Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onSequenceCanceled(TapTarget lastTarget) {

                    }
                }).start();


    }

    @Override
    public void bannerClick(NewsData newsData) {
        openDialog("update",newsData);
    }
    @OnClick(R.id.imgBack)
    public void callBackF() {
        finish();
    }
}