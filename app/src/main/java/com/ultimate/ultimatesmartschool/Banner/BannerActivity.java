package com.ultimate.ultimatesmartschool.Banner;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetSequence;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.tabs.TabLayout;
import com.ultimate.ultimatesmartschool.ForCamera.IPickResult;
import com.ultimate.ultimatesmartschool.ForCamera.PickImageDialog;
import com.ultimate.ultimatesmartschool.ForCamera.PickResult;
import com.ultimate.ultimatesmartschool.ForCamera.PickSetup;
import com.ultimate.ultimatesmartschool.NoticeMod.AddNoticeBoard;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Security.SecurityreportPager;
import com.ultimate.ultimatesmartschool.Staff.AssignSubject;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class BannerActivity extends AppCompatActivity implements IPickResult {
    BottomSheetDialog mBottomSheetDialog;
    SharedPreferences sharedPreferences;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.tab_layout)
    TabLayout tabLayout;
    @BindView(R.id.contact_support)
    ImageView contact_support;
    private Bitmap bitmap;
    ImageView clickimage,visitimage;
    EditText title,visitPurpose;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_security_report);
        ButterKnife.bind(this);
        contact_support.setVisibility(View.VISIBLE);
        txtTitle.setText("Banners Detail");
        if (!restorePrefData()){
            setShowcaseView();
        }
        //Tab layout
        final ViewPager viewPager = (ViewPager) findViewById(R.id.pager);
        tabLayout.setupWithViewPager(viewPager);
        final PagerAdapter adapter = new BannerPager
                (getSupportFragmentManager());
        viewPager.setAdapter(adapter);
    }


    private void savePrefData(){
        sharedPreferences= BannerActivity.this.getSharedPreferences("boarding_pref3",MODE_PRIVATE);
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putBoolean("IsFirstTimeVisit3",true);
        editor.apply();
    }

    private boolean restorePrefData(){
        sharedPreferences = BannerActivity.this.getSharedPreferences("boarding_pref3",MODE_PRIVATE);
        return sharedPreferences.getBoolean("IsFirstTimeVisit3",false);
    }

    private void setShowcaseView() {
        new TapTargetSequence(this)
                .targets(TapTarget.forView(contact_support,"Add Banner Button!","Tap the add banner button to upload new banner.")
                        .outerCircleColor(R.color.light)
                        .outerCircleAlpha(0.96f)
                        .targetCircleColor(R.color.white)
                        .titleTextSize(20)
                        .titleTextColor(R.color.white)
                        .descriptionTextSize(14)
                        .descriptionTextColor(R.color.black)
                        .textColor(R.color.black)
                        .textTypeface(Typeface.SANS_SERIF)
                        .dimColor(R.color.black)
                        .drawShadow(true)
                        .cancelable(false)
                        .tintTarget(true)
                        .transparentTarget(true)
                        .targetRadius(60)).listener(new TapTargetSequence.Listener() {
            @Override
            public void onSequenceFinish() {
                 Toast.makeText(getApplicationContext(),"Sequence Finished",Toast.LENGTH_SHORT).show();
                savePrefData();
                openDialog();
            }

            @Override
            public void onSequenceStep(TapTarget lastTarget, boolean targetClicked) {
                // Toast.makeText(getActivity(),"GREAT!",Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSequenceCanceled(TapTarget lastTarget) {

            }
        }).start();


    }

    private void openDialog() {

        mBottomSheetDialog = new BottomSheetDialog(this);
        final View sheetView = getLayoutInflater().inflate(R.layout.add_banner_dialog, null);
        mBottomSheetDialog.setContentView(sheetView);

        mBottomSheetDialog.setCancelable(true);

        clickimage=(ImageView) sheetView.findViewById(R.id.clickimage);
        visitimage=(ImageView) sheetView.findViewById(R.id.visitimage);
        title=(EditText) sheetView.findViewById(R.id.title);
        visitPurpose=(EditText) sheetView.findViewById(R.id.visitPurpose);
//
//        txtSetup=(TextView) sheetView.findViewById(R.id.txtSetup);
//        txtSetup.setText("Assign Subject");
//        classSubject=(RelativeLayout) sheetView.findViewById(R.id.classSubject);
//        classSubject.setVisibility(View.VISIBLE);
//        fetchStafffffff();
//        fetchclasslist();

        clickimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //  assignData();
                pick_img();
            }
        });

        Button btnYes= (Button) sheetView.findViewById(R.id.btnYes);
        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                assignData();
            }
        });
        mBottomSheetDialog.show();

    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @SuppressLint("ResourceType")
    private void pick_img() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.UPSIDE_DOWN_CAKE) { // Android 14+ (API 34)
          onImageViewClick();

        } else if (Build.VERSION.SDK_INT > Build.VERSION_CODES.S) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_MEDIA_IMAGES)
                    != PackageManager.PERMISSION_GRANTED) {

                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_MEDIA_IMAGES)) {
                    Toast.makeText(this, "Please provide storage permission from app settings", Toast.LENGTH_LONG).show();
                } else {

                    ActivityCompat.requestPermissions(this,
                            new String[]{Manifest.permission.READ_MEDIA_IMAGES},
                            1);
                }
            } else {

                onImageViewClick();
            }

        } else {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {

                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    Toast.makeText(this, "Please provide storage permission from app settings", Toast.LENGTH_LONG).show();
                } else {

                    ActivityCompat.requestPermissions(this,
                            new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                            1);
                }
            } else {
                onImageViewClick();

            }

        }
    }

    private void onImageViewClick() {
        //  type = 1;
        PickSetup setup = new PickSetup();
        // super.customize(setup);

        PickImageDialog.build(setup)
                //.setOnClick(this)
                .show(this);
    }

    @Override
    public void onPickResult(PickResult r) {
        if (r.getError() == null) {
            //If you want the Bitmap.
            visitimage.setImageBitmap(r.getBitmap());
            bitmap = r.getBitmap();

        } else {
            //Handle possible errors
            //TODO: do what you have to do with r.getError();
            Toast.makeText(this, r.getError().getMessage(), Toast.LENGTH_LONG).show();
        }


    }



    private void assignData() {

        if (bitmap == null) {
            Toast.makeText(BannerActivity.this, "Kindly Add Banner Image", Toast.LENGTH_SHORT).show();
            //errorMsg = "Please enter Title";
        } else if (title.getText().toString().trim().length() <= 0) {
            Toast.makeText(BannerActivity.this, "Kindly Enter Banner Title", Toast.LENGTH_SHORT).show();
            //errorMsg = "Please enter Title";
        } else if (visitPurpose.getText().toString().trim().length() <= 0) {
            Toast.makeText(BannerActivity.this, "Kindly Enter Banner Description", Toast.LENGTH_SHORT).show();
        } else{
            ErpProgress.showProgressBar(this, "Please wait");
        HashMap<String, String> params = new HashMap<>();
        params.put("title", title.getText().toString());
        params.put("desc", visitPurpose.getText().toString());

        if (bitmap != null) {
            String encoded = Utils.encodeToBase64(bitmap, Bitmap.CompressFormat.JPEG, 50);
            encoded = String.format("data:image/jpeg;base64,%s", encoded);
            params.put("image", encoded);
        }


        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.ADDBANNER_URL, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                ErpProgress.cancelProgressBar();
                if (error == null) {
                    try {
                        Toast.makeText(BannerActivity.this, jsonObject.getString("msg"), Toast.LENGTH_SHORT).show();
                        finish();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(BannerActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }, this, params);

    }
    }


//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        if (resultCode == Activity.RESULT_OK) {
//            // Uri object will not be null for RESULT_OK
//            Uri imageUri = data.getData();
//            try {
//                bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), imageUri);
//                visitimage.setImageBitmap(bitmap);
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//
//        }  else {
//            Toast.makeText(this, "Selection Cancelled", Toast.LENGTH_SHORT).show();
//        }
//    }

    @OnClick(R.id.contact_support)
    public void contact_support() {
        openDialog();
    }

    @OnClick(R.id.imgBack)
    public void callBackF() {
        finish();
    }
}

