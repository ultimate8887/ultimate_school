package com.ultimate.ultimatesmartschool.Banner;

import android.os.Bundle;

import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.ultimate.ultimatesmartschool.Activity.NewsData;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;


public class ActiveBannerFragment extends Fragment implements Banner_adapter.BannerCallBack {

    @BindView(R.id.recyadmnlst)
    RecyclerView r1;
    @BindView(R.id.parenradmin)
    RelativeLayout parent;
    RecyclerView.LayoutManager layoutManager;
    Banner_adapter adminlistadapter;
    ArrayList<NewsData> adminlist=new ArrayList<>();
    private int loaded = 0;
    View view;
    @BindView(R.id.textNorecord)
    TextView txtNorecord;
    private SearchView search;
    @BindView(R.id.totalRecord)
    TextView totalRecord;

    public ActiveBannerFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.common_frag, container, false);
        ButterKnife.bind(this, view);
        layoutManager=new LinearLayoutManager(getActivity());
        r1.setLayoutManager(layoutManager);
        adminlistadapter=new Banner_adapter(adminlist,getActivity(), this);
        r1.setAdapter(adminlistadapter);

        return view;
    }

    private void fetchlist() {
//        if (loaded == 0) {
//            ErpProgress.showProgressBar(getContext(), "Please wait...");
//        }
        loaded++;
        HashMap<String,String> params=new HashMap<>();
//        params.put("user_id", User.getCurrentUser().getId());
//        params.put("date", "");
//        params.put("check", "pending");
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.NEWS_URL, apiCallback, getActivity(), null);
    }

    ApiHandler.ApiCallback apiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            // txtupload.setVisibility(View.VISIBLE);
            if (error == null) {
                try {
                    if (adminlist != null) {
                        adminlist.clear();
                    }
                    JSONArray jsonArray = jsonObject.getJSONArray("news_data");
                    Log.e("jsonArray", String.valueOf(jsonArray));
                    adminlist = NewsData.parseNewsDataArrayyy(jsonArray);
                    if (adminlist.size() > 0) {
                        adminlistadapter.setsecuList(adminlist);
                        totalRecord.setText("Total Entries:- "+String.valueOf(adminlist.size()));
                        adminlistadapter.notifyDataSetChanged();
                        txtNorecord.setVisibility(View.GONE);
                    } else {
                        totalRecord.setText("Total Entries:- "+String.valueOf(adminlist.size()));
                        adminlistadapter.setsecuList(adminlist);
                        adminlistadapter.notifyDataSetChanged();
                        txtNorecord.setVisibility(View.VISIBLE);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                txtNorecord.setVisibility(View.VISIBLE);
                totalRecord.setText("Total Entries:- "+String.valueOf(adminlist.size()));
                //   Utility.showSnackBar(error.getMessage(), parent);

            }
        }
    };

    @Override
    public void onResume() {
        super.onResume();
        fetchlist();
    }

    @Override
    public void bannerClick(NewsData newsData) {

    }
}