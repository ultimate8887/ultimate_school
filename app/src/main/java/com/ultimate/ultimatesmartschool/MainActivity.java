package com.ultimate.ultimatesmartschool;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.IntentSenderRequest;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import android.Manifest;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.provider.Settings;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.Toast;

import androidx.multidex.BuildConfig;

import com.android.volley.Request;

import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.play.core.appupdate.AppUpdateInfo;
import com.google.android.play.core.appupdate.AppUpdateManager;
import com.google.android.play.core.appupdate.AppUpdateManagerFactory;
import com.google.android.play.core.appupdate.AppUpdateOptions;
import com.google.android.play.core.install.model.AppUpdateType;
import com.google.android.play.core.install.model.UpdateAvailability;
import com.ultimate.ultimatesmartschool.Activity.UserPermissions;
import com.ultimate.ultimatesmartschool.Activity.User_Permission;
import com.ultimate.ultimatesmartschool.BeanModule.User;
import com.ultimate.ultimatesmartschool.Home.AdminFragment;
import com.ultimate.ultimatesmartschool.Home.BottomViewPagerAdapter;
import com.ultimate.ultimatesmartschool.Home.Mainfragment;
import com.ultimate.ultimatesmartschool.Home.StaffFragment;
import com.ultimate.ultimatesmartschool.Home.StudentFragment;
import com.ultimate.ultimatesmartschool.NotificationMod.NotificationBean;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;
import com.ultimate.ultimatesmartschool.Utility.FirebaseGetDeviceToken;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MainActivity extends AppCompatActivity {
        private final int MY_REQUEST_CODE = 101;
        private BottomNavigationView bottomNavigationView;
        ArrayList<Fragment> fragmentList;
        private ViewPager viewPager;
        private JSONObject user_obj;
        private static final int NOTIFICATION_PERMISSION_REQUEST_CODE = 1;
        private SharedPreferences pref;
        private String deviceid;
        private int deviceIdChecked = 0;

        @RequiresApi(api = Build.VERSION_CODES.M)
        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_main);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                int startColor = getWindow().getStatusBarColor();
                int endColor = ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary);
                ObjectAnimator.ofArgb(getWindow(), "statusBarColor", startColor, endColor).start();
            }

            checkAppUpdate();
            getDeviceid();
            notification_permission();

            fragmentList = new ArrayList<>();
            fragmentList.add(new Mainfragment());
            fragmentList.add(new AdminFragment());
            fragmentList.add(new StaffFragment());
            fragmentList.add(new StudentFragment());

            viewPager = findViewById(R.id.viewPager);
            bottomNavigationView = findViewById(R.id.bottomNavigationView);

            // Set up ViewPager with an adapter
            BottomViewPagerAdapter adapter1 = new BottomViewPagerAdapter(getSupportFragmentManager(), fragmentList);
            viewPager.setAdapter(adapter1);

            // Link the BottomNavigationView with the ViewPager
            bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                    switch (item.getItemId()) {
                        case R.id.nav_home:
                            viewPager.setCurrentItem(0);
                            return true;
                        case R.id.nav_tool:
                            viewPager.setCurrentItem(1);
                            return true;
                        case R.id.nav_staff:
                            viewPager.setCurrentItem(2);
                            return true;
                        case R.id.nav_std:
                            viewPager.setCurrentItem(3);
                            return true;
                    }
                    return false;
                }
            });

            // Add page change listener to keep the BottomNavigationView in sync with ViewPager
            viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}

                @Override
                public void onPageSelected(int position) {
                    switch (position) {
                        case 0:
                            bottomNavigationView.setSelectedItemId(R.id.nav_home);
                            break;
                        case 1:
                            bottomNavigationView.setSelectedItemId(R.id.nav_tool);
                            break;
                        case 2:
                            bottomNavigationView.setSelectedItemId(R.id.nav_staff);
                            break;
                        case 3:
                            bottomNavigationView.setSelectedItemId(R.id.nav_std);
                            break;
                    }
                }

                @Override
                public void onPageScrollStateChanged(int state) {}
            });
        }

    @SuppressLint("UnsafeOptInUsageError")
    @RequiresApi(api = Build.VERSION_CODES.M)
    private void notification_permission() {

        // Check for notification permission
        // Check if the device is running Android 12 or higher
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.S) {
            if (ContextCompat.checkSelfPermission(MainActivity.this,
                    Manifest.permission.POST_NOTIFICATIONS) == PackageManager.PERMISSION_GRANTED) {
                // Log.e(TAG, "User accepted the notifications!");
                // sendNotification(HomePageActivity.this);
            } else if (shouldShowRequestPermissionRationale(Manifest.permission.POST_NOTIFICATIONS)) {

                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setTitle("Notification's Permission!");
                builder.setMessage("Please grant permission to receive notifications.").setCancelable(false).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                }).setPositiveButton("Settings", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts("package", getPackageName(), null);
                        intent.setData(uri);
                        startActivity(intent);
                        dialog.dismiss();

                    }
                });
                AlertDialog alert = builder.create();
                alert.show();
                Button positiveButton = alert.getButton(DialogInterface.BUTTON_POSITIVE);
                Button negativeButton = alert.getButton(DialogInterface.BUTTON_NEGATIVE);
                positiveButton.setTextColor(getResources().getColor(R.color.green));
                negativeButton.setTextColor(getResources().getColor(R.color.colorPrimaryDark));


            } else {
                ActivityCompat.requestPermissions(MainActivity.this,
                        new String[]{Manifest.permission.POST_NOTIFICATIONS},
                        NOTIFICATION_PERMISSION_REQUEST_CODE);
            }
        }
//        else {
//            // Handle devices running Android versions lower than 12 (Android 11 and below)
//            // You can implement a fallback mechanism here
//            Toast.makeText(this, "Notification permission ask", Toast.LENGTH_SHORT).show();
//        }

    }

    private void getDeviceid() {
        pref = UltimateSchoolApp.getInstance().getSharedPreferences(Constants.PREF_NAME, Context.MODE_PRIVATE);
        deviceid = pref.getString(Constants.DEVICE_TOKEN, "");
        if (deviceid.isEmpty()) {
            FirebaseGetDeviceToken.getDeviceToken();
        }
        if (deviceIdChecked != 0) {
            if (deviceIdChecked < 4) {
                checkDeviceIDValid();
            }
        }
    }

    public void checkDeviceIDValid() {
        if (deviceid == null && deviceid.isEmpty()) {
            if (deviceIdChecked < 3) {
                deviceIdChecked++;
                getDeviceid();
            } else {
                FirebaseGetDeviceToken.getDeviceToken();

            }
        } else {
            // registration_sucess();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);
    }
    private void checkAppUpdate() {
        AppUpdateManager appUpdateManager = AppUpdateManagerFactory.create(MainActivity.this);

// Returns an intent object that you use to check for an update.
        Task<AppUpdateInfo> appUpdateInfoTask = appUpdateManager.getAppUpdateInfo();

// Checks that the platform will allow the specified type of update.
        appUpdateInfoTask.addOnSuccessListener(appUpdateInfo -> {
            if (appUpdateInfo.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE
                    // This example applies an immediate update. To apply a flexible update
                    // instead, pass in AppUpdateType.FLEXIBLE
                    && appUpdateInfo.isUpdateTypeAllowed(AppUpdateType.IMMEDIATE)) {
                ActivityResultLauncher<IntentSenderRequest> activityResultLauncher = null;
                appUpdateManager.startUpdateFlowForResult(
                        // Pass the intent that is returned by 'getAppUpdateInfo()'.
                        appUpdateInfo,
                        // an activity result launcher registered via registerForActivityResult
                        activityResultLauncher,
                        // Or pass 'AppUpdateType.FLEXIBLE' to newBuilder() for
                        // flexible updates.
                        AppUpdateOptions.newBuilder(AppUpdateType.IMMEDIATE).build()
                );

                // Request the update.
            }
        });
    }

//    @Override
//    protected void onStop() {
//        super.onStop();
//        UserPermissions.logoutUserPermissions();
//        Log.i("checked","exit app");
//    }
//
//    @Override
//    protected void onDestroy() {
//        super.onDestroy();
//        UserPermissions.logoutUserPermissions();
//        Log.i("checked","exit app");
//    }

    @Override
    protected void onResume() {
        super.onResume();
        checkAppUpdate();
        updateMobileInfo();
    }

    private void updateMobileInfo() {
        // UltimateProgress.showProgressBar(getActivity(),"Please Wait........!");
        User currentUser = User.getCurrentUser();
        if (currentUser != null && currentUser.getId() != null) {
            int code = 0;
            try {
                HashMap<String, String> params = new HashMap<>();
                params.put("user_id", User.getCurrentUser().getId());

//                Log.i("TAG", "SERIAL: " + Build.SERIAL);
//                Log.i("TAG", "MODEL: " + Build.MODEL);
//                Log.i("TAG", "ID: " + Build.ID);
//                Log.i("TAG", "Manufacture: " + Build.MANUFACTURER);
//                Log.i("TAG", "brand: " + Build.BRAND);
//                Log.i("TAG", "type: " + Build.TYPE);
//                Log.i("TAG", "user: " + Build.USER);
//                Log.i("TAG", "BASE: " + Build.VERSION_CODES.BASE);
//                Log.i("TAG", "INCREMENTAL " + Build.VERSION.INCREMENTAL);
//                Log.i("TAG", "SDK  " + Build.VERSION.SDK);
//                Log.i("TAG", "BOARD: " + Build.BOARD);
//                Log.i("TAG", "BRAND " + Build.BRAND);
//                Log.i("TAG", "HOST " + Build.HOST);
//                Log.i("TAG", "FINGERPRINT: " + Build.FINGERPRINT);
//                Log.i("TAG", "Version Code: " + Build.VERSION.RELEASE);

                code = getApplicationContext().getPackageManager().getPackageInfo(getApplicationContext().getPackageName(), 0).versionCode;

                params.put(Constants.DEVICE_TOKEN, deviceid);
                params.put("mobile_name", Build.MODEL);
                params.put("mobile_id", Build.ID);
                params.put("manufacture", Build.MANUFACTURER);
                params.put("brand", Build.BRAND);
                params.put("mobile_sdk", Build.VERSION.SDK);
                params.put("fingerprint", Build.FINGERPRINT);
                params.put("mob_version_code", Build.VERSION.RELEASE);
                params.put("install_app_vcode", String.valueOf(code));


            ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.UPDATE_MOB_INFO, new ApiHandler.ApiCallback() {
                @Override
                public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                    ErpProgress.cancelProgressBar();
                    if (error == null) {
                        try {
                            String mobile_name = jsonObject.getJSONObject(Constants.USERDATA).getString("mobile_name");
                            // Log.i("USERDATA-Home", mobile_name);


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    } else {
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            }, this, params);

            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
              //  Log.i("TAG_exception found", "Version Code: " + e.getMessage());
            }

        } else {
            // Handle the null case
            Log.e("Debug", "User is null or User ID is null");
        }

        }

    }
