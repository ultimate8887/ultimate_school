package com.ultimate.ultimatesmartschool.Ebook;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.ultimate.ultimatesmartschool.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OnlineQuizAdapter extends RecyclerView.Adapter<OnlineQuizAdapter.Viewholder> {
    ArrayList<OnlineQuizBean> sy_list;
    Mycallback mAdaptercall;
    Context mContext;

    public OnlineQuizAdapter(ArrayList<OnlineQuizBean> sy_list, Context mContext, Mycallback mAdaptercall) {
        this.mAdaptercall = mAdaptercall;
        this.sy_list = sy_list;
        this.mContext = mContext;
    }


    @Override
    public Viewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.quizon_adpt_lyt, parent, false);
        Viewholder viewholder = new Viewholder(view);
        return viewholder;

    }

    @Override
    public void onBindViewHolder(Viewholder holder, @SuppressLint("RecyclerView") final int position) {
        holder.starttest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mAdaptercall != null) {
                    mAdaptercall.onMethodCallback(sy_list.get(position));
                }
            }
        });


        holder.txtPdf.setText(sy_list.get(position).getQuiz_title());
        holder.txtSub.setText(sy_list.get(position).getSub_name()+"("+sy_list.get(position).getClassname()+")");
        holder.txttime.setText("Time: "+sy_list.get(position).getTime());
        holder.txttitle.setText("Topic: "+sy_list.get(position).getTopic());
        holder.txtwriter.setText("No. of Ques: "+sy_list.get(position).getTot_qus());
        holder.txtauthor.setText("Chapter: "+sy_list.get(position).getChapter());


    }

    public interface Mycallback {
        public void onMethodCallback(OnlineQuizBean onlineQuizBean);

        public void viewPdf(OnlineQuizBean onlineQuizBean);
    }

    @Override
    public int getItemCount() {
        return sy_list.size();
    }

    public void setSyllabusList(ArrayList<OnlineQuizBean> sy_list) {
        this.sy_list = sy_list;
    }

    public class Viewholder extends RecyclerView.ViewHolder {
        @BindView(R.id.txtPdf)
        TextView txtPdf;
        @BindView(R.id.imgData)
        ImageView imgData;
        @BindView(R.id.txttitle)
        TextView txttitle;
        @BindView(R.id.txtSub)
        TextView txtSub;
        @BindView(R.id.txtwriter)
        TextView txtwriter;
        @BindView(R.id.txtauthor)
        TextView txtauthor;
        @BindView(R.id.txttime)TextView txttime;
        @BindView(R.id.starttest)TextView starttest;
        public Viewholder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
