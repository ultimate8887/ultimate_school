package com.ultimate.ultimatesmartschool.Ebook;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.pdf.PdfRenderer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.ParcelFileDescriptor;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.google.android.material.bottomsheet.BottomSheetDialog;

import com.ultimate.ultimatesmartschool.BeanModule.ClassBean;
import com.ultimate.ultimatesmartschool.BeanModule.SectionBean;
import com.ultimate.ultimatesmartschool.BeanModule.SubjectBeancls;
import com.ultimate.ultimatesmartschool.BeanModule.User;
import com.ultimate.ultimatesmartschool.Examination.ClassAdapter;
import com.ultimate.ultimatesmartschool.Homework.AdapterOfSubjectList;
import com.ultimate.ultimatesmartschool.Homework.AddHomeworkActivity;
import com.ultimate.ultimatesmartschool.Homework.Homeworkadapter;
import com.ultimate.ultimatesmartschool.Homework.Homeworkbean;
import com.ultimate.ultimatesmartschool.Login.LoginActivity;
import com.ultimate.ultimatesmartschool.PDF.PdfPageAdapter;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.TimeTable.AddTimeTable;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.CommonProgress;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;
import com.ultimate.ultimatesmartschool.Utility.ErpProgressVert;
import com.ultimate.ultimatesmartschool.Utility.FileUtils;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.apptik.widget.multiselectspinner.BaseMultiSelectSpinner;
import io.apptik.widget.multiselectspinner.MultiSelectSpinner;

public class Ebook extends AppCompatActivity {
    ArrayList<ClassBean> classList = new ArrayList<>();
    @BindView(R.id.parent)
    RelativeLayout parent;
    @BindView(R.id.spinnerClass)
    Spinner spinnerClass;
    String classid = "";
    private int loaded = 0;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    List<String> classidsel;
    @BindView(R.id.textView7)TextView textView7;
    @BindView(R.id.spinnersection)
    Spinner spinnersection;
    ArrayList<SectionBean> sectionList = new ArrayList<>();
    String sectionid="";
    String sectionname;
    private int type;
    private Bitmap hwbitmap;
    @BindView(R.id.imageView6)
    ImageView imageView6;
    private final int LOAD_FILE_RESULTS = 1000;
    String fileBase64 = null;
    BottomSheetDialog mBottomSheetDialog1;
    @BindView(R.id.textnote)TextView textnote;
    @BindView(R.id.addpdf)TextView addpdf;

    CommonProgress commonProgress;


    Animation animation;
    String path="";
    Uri uri;
    float  length;
    String size;

    //@BindView(R.id.testbtn)Button testbtn;

    @BindView(R.id.writer)
    EditText writer;
    @BindView(R.id.title)
    EditText title;
    ArrayList<SubjectBeancls> subjectList = new ArrayList<>();
    AdapterOfSubjectList adaptersub;
    String sub_id="", sub_name="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ebook);
        ButterKnife.bind(this);
        animation = AnimationUtils.loadAnimation(Ebook.this, R.anim.btn_blink_animation);
        commonProgress=new CommonProgress(this);
        classidsel = new ArrayList<>();
        fetchClass();

        txtTitle.setText("Ebook");
    }
    @OnClick(R.id.imgBack)
    public void onback() {
        finish();
    }

    private void fetchClass() {
        if (loaded == 0) {
            commonProgress.show();
        }
        loaded++;

        HashMap<String, String> params = new HashMap<String, String>();
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.CLASSLIST_URL, classapiCallback,getApplicationContext(), params);
    }


    ApiHandler.ApiCallback classapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            commonProgress.dismiss();
            if (error == null) {
                try {
                    classList = ClassBean.parseClassArray(jsonObject.getJSONArray(Constants.CLASSDATA));
                    ClassAdapter adapter = new ClassAdapter(getApplicationContext(), classList);
                    spinnerClass.setAdapter(adapter);
                    spinnerClass.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if (i > 0) {
                                classid = classList.get(i - 1).getId();
                                fetchSubject();
                            }

                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_LONG).show();
                // Utils.showSnackBar(error.getMessage(), view.findViewById(R.id.parent));
            }
        }
    };

    private void fetchSubject() {
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("id", classid);
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.SubjectList, subapiCallback, this, params);

    }

    ApiHandler.ApiCallback subapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            /* */

            if (error == null) {
                try {

                    subjectList =   SubjectBeancls.parseClassArray(jsonObject.getJSONArray("sub_data"));

                    adaptersub = new AdapterOfSubjectList(Ebook.this, subjectList);
                    spinnersection.setAdapter(adaptersub);
                    spinnersection.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if (i != 0) {
                                sub_name = subjectList.get(i - 1).getName();
                                sub_id = subjectList.get(i - 1).getId();

                            }

                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    };

    @OnClick(R.id.addpdf)
    public void browseDocuments() {

        addpdf.startAnimation(animation);
        pick_pdf();

    }

    private void pick_pdf() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.UPSIDE_DOWN_CAKE) { // Android 14+ (API 34)
         callPicker();

        } else if (Build.VERSION.SDK_INT > Build.VERSION_CODES.S) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_MEDIA_IMAGES)
                    != PackageManager.PERMISSION_GRANTED) {

                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_MEDIA_IMAGES)) {
                    Toast.makeText(this, "Please provide storage permission from app settings", Toast.LENGTH_LONG).show();
                } else {

                    ActivityCompat.requestPermissions(this,
                            new String[]{Manifest.permission.READ_MEDIA_IMAGES},
                            2);
                }
            } else {

                callPicker();
            }

        } else {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {

                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    Toast.makeText(this, "Please provide storage permission from app settings", Toast.LENGTH_LONG).show();
                } else {

                    ActivityCompat.requestPermissions(this,
                            new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                            2);
                }
            } else {
                callPicker();

            }
        }

    }


    public void callPicker(){
        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        intent.setType("application/pdf");
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        startActivityForResult(intent, LOAD_FILE_RESULTS);
    }
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 1: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    callPicker();

                } else {
                    // permission denied, boo! Disable the
                    Toast.makeText(this, "Please provide storage permission from app settings, as it is mandatory to access your files!", Toast.LENGTH_LONG).show();
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request.
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {

                case LOAD_FILE_RESULTS:
                    uri = data.getData();
                    // String path = Utils.getPath(this, uri);

                    if(uri!=null) {
                        try {
                            ContentResolver contentResolver = getContentResolver();
                            InputStream inputStream = contentResolver.openInputStream(uri);
                            file = new File(getCacheDir(), "temp.pdf");
                            OutputStream outputStream = new FileOutputStream(file);
                            byte[] buffer = new byte[4 * 1024]; // or other buffer size
                            int read;

                            while ((read = inputStream.read(buffer)) != -1) {
                                outputStream.write(buffer, 0, read);
                            }

                            outputStream.flush();
                            outputStream.close();
                            inputStream.close();

                            String path = file.getAbsolutePath();
                            long fileSize = file.length();
                            size = formatSize(fileSize);
                            if (fileSize > 18 * 1024 * 1024) { // 18 MB
                                Toast.makeText(getApplicationContext(),
                                        "Sorry, Your selected file size is too large (" + size + ")." +
                                                "Please select a file with a size of less than 18 MB.", Toast.LENGTH_LONG).show();
                            } else {
                                fileBase64 = Utils.encodeFileToBase64Binary(path);
                                hwbitmap = null;
                                showPdfFromUri(uri, path);
                                textnote.setText("Pdf Size: " + "" + "" + size);
                                textnote.setVisibility(View.VISIBLE);
                                imageView6.setImageResource(R.drawable.pdfbig);
                                //  scrollView.setVisibility(View.GONE);
                                imageView6.setVisibility(View.VISIBLE);
                                Toast.makeText(this, "File Picked...", Toast.LENGTH_SHORT).show();
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                    }

            }
        }
    }
    File file;
    @SuppressLint("MissingInflatedId")
    private void showPdfFromUri(Uri uri, String path) {
        mBottomSheetDialog1 = new BottomSheetDialog(Ebook.this);
        final View sheetView = getLayoutInflater().inflate(R.layout.pdf_view_dialog, null);
        mBottomSheetDialog1.setContentView(sheetView);
        mBottomSheetDialog1.setCancelable(false);

        TextView title= (TextView) sheetView.findViewById(R.id.title);
        TextView path1= (TextView) sheetView.findViewById(R.id.path);
        title.setText("Timetable PDF");
        //  path1.setText(path);
        path1.setText(path + "\nSize: " + size);

        //pdf code start here
        RecyclerView recyclerView;
        PdfPageAdapter adapter;
        recyclerView = (RecyclerView)  sheetView.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        if (file.exists()) {
            try {
                ParcelFileDescriptor parcelFileDescriptor = ParcelFileDescriptor.open(file, ParcelFileDescriptor.MODE_READ_ONLY);

                // Check if ParcelFileDescriptor is null
                if (parcelFileDescriptor == null) {
                    Log.e("PDF_ERROR", "ParcelFileDescriptor is null. Unable to open the file.");
                 //   Toast.makeText(this, "Unable to open the PDF file", Toast.LENGTH_SHORT).show();
                    return;
                }

                PdfRenderer pdfRenderer = new PdfRenderer(parcelFileDescriptor);
                int pageCount = pdfRenderer.getPageCount();

                // Log the number of pages found in the PDF
                Log.d("PDF_INFO", "Total number of pages: " + pageCount);

                List<Bitmap> bitmaps = new ArrayList<>();

                for (int i = 0; i < pageCount; i++) {
                    PdfRenderer.Page pdfPage = pdfRenderer.openPage(i);

                    // Check if pdfPage is null
                    if (pdfPage == null) {
                        Log.e("PDF_ERROR", "Failed to open page " + i);
                        continue;
                    }

                    Bitmap bitmap = Bitmap.createBitmap(pdfPage.getWidth(), pdfPage.getHeight(), Bitmap.Config.ARGB_8888);
                    pdfPage.render(bitmap, null, null, PdfRenderer.Page.RENDER_MODE_FOR_DISPLAY);
                    bitmaps.add(bitmap);

                    // Log successful page rendering
                    Log.d("PDF_INFO", "Page " + i + " rendered successfully.");

                    pdfPage.close();
                }

                adapter = new PdfPageAdapter(this, bitmaps);
                recyclerView.setAdapter(adapter);

                pdfRenderer.close();
                parcelFileDescriptor.close();

            } catch (FileNotFoundException e) {
                Log.e("PDF_ERROR", "File not found: " + e.getMessage());
                Toast.makeText(this, "PDF file not found", Toast.LENGTH_SHORT).show();
            } catch (SecurityException e) {
                Log.e("PDF_ERROR", "Permission denied: " + e.getMessage());
                Toast.makeText(this, "Permission denied", Toast.LENGTH_SHORT).show();
            } catch (IOException e) {
                Log.e("PDF_ERROR", "IO Exception: " + e.getMessage());
                Toast.makeText(this, "Failed to open the PDF file", Toast.LENGTH_SHORT).show();
            }
        } else {
            Log.e("PDF_ERROR", "File does not exist at the path: " + path);
            Toast.makeText(this, "PDF file not found at: " + path, Toast.LENGTH_SHORT).show();
        }
        Button retake= (Button) sheetView.findViewById(R.id.retake);
        retake.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                retake.startAnimation(animation);
                pick_pdf();
                mBottomSheetDialog1.dismiss();
            }
        });
        Button submitted= (Button) sheetView.findViewById(R.id.submitted);
        submitted.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                submitted.startAnimation(animation);
                mBottomSheetDialog1.dismiss();
                //  submitAssignment();

            }
        });
        RelativeLayout btnNo= (RelativeLayout) sheetView.findViewById(R.id.btnNo);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnNo.startAnimation(animation);
                //   fileBase64=null;
                mBottomSheetDialog1.dismiss();
            }
        });
        mBottomSheetDialog1.show();
    }

    public static String formatSize(long size) {
        String suffix = "B";
        double formattedSize = size;

        if (formattedSize >= 1024) {
            suffix = "KB";
            formattedSize /= 1024;
        }

        if (formattedSize >= 1024) {
            suffix = "MB";
            formattedSize /= 1024;
        }

        if (formattedSize >= 1024) {
            suffix = "GB";
            formattedSize /= 1024;
        }

        return String.format("%.2f %s", formattedSize, suffix);
    }


    @OnClick(R.id.button3)
    public void save() {

        if (classid.equalsIgnoreCase("")) {
            Toast.makeText(this,"Kindly select class",Toast.LENGTH_SHORT).show();

        } else if (sub_id.equalsIgnoreCase("")) {
            Toast.makeText(this,"Kindly select class subject",Toast.LENGTH_SHORT).show();
        } else if (title.getText().toString().isEmpty()) {
            Toast.makeText(this,"Kindly add title",Toast.LENGTH_SHORT).show();
        } else if (writer.getText().toString().isEmpty()) {
            Toast.makeText(this,"Kindly add writer",Toast.LENGTH_SHORT).show();
        } else if (fileBase64==null) {
            Toast.makeText(this,"Kindly select E-book file",Toast.LENGTH_SHORT).show();
        } else{
            commonProgress.show();
            HashMap<String, String> params = new HashMap<String, String>();
            params.put("class_id",  classid);
            params.put("sub_id",sub_id);
            params.put("title", title.getText().toString());
            params.put("writer", writer.getText().toString());
            if (fileBase64 != null)
                params.put("file", fileBase64);
            ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.ADD_EBOOK, apiCallback, this, params);
        }

    }



    ApiHandler.ApiCallback apiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgressVert.cancelProgressBar();
            commonProgress.dismiss();
            if (error == null) {
                try {
                    Utils.showSnackBar(jsonObject.getString("msg"), parent);
                    finish();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("error", error.getMessage() + "");
                Utils.showSnackBar(error.getMessage(), parent);

                if (error.getStatusCode() == 405) {
                    User.logout();
                    startActivity(new Intent(Ebook.this, LoginActivity.class));
                    finish();
                }
            }
        }
    };

    private boolean checkValid() {

        boolean valid = true;
        String errorMsg = null;
        if (sub_id == "" || sub_id == null) {
            valid = false;
            errorMsg = "Please select subject";
        }else if (title.getText().toString().trim().length() <= 0) {
            valid = false;
            errorMsg = "Please select title";
        }
        else if(writer.getText().toString().trim().length()<=0){
            valid=false;
            errorMsg="Please enter the writer";
        }

        if (!valid) {
            Toast.makeText(getApplicationContext(),errorMsg,Toast.LENGTH_LONG).show();

        }
        return valid;
    }
}
