package com.ultimate.ultimatesmartschool.DateSheet;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.bumptech.glide.Glide;
import com.kroegerama.imgpicker.BottomSheetImagePicker;
import com.ultimate.ultimatesmartschool.BeanModule.ClassBean;
import com.ultimate.ultimatesmartschool.BeanModule.SectionBean;
import com.ultimate.ultimatesmartschool.BeanModule.User;
import com.ultimate.ultimatesmartschool.ClassWork.ViewClassWork;
import com.ultimate.ultimatesmartschool.Homework.SectionnewAdapter;
import com.ultimate.ultimatesmartschool.Login.LoginActivity;
import com.ultimate.ultimatesmartschool.Message.MultiClassComposeActivity;
import com.ultimate.ultimatesmartschool.Message.Spinner_stu_adapter;
import com.ultimate.ultimatesmartschool.Message.Student_msg_bean;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.StuGatePass.ClassBeans;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.CommonProgress;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;
import com.ultimate.ultimatesmartschool.Utility.ErpProgressVert;
import com.ultimate.ultimatesmartschool.Utility.FileUtils;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.apptik.widget.multiselectspinner.BaseMultiSelectSpinner;
import io.apptik.widget.multiselectspinner.MultiSelectSpinner;

public class AddDateSheetNew extends AppCompatActivity implements BottomSheetImagePicker.OnImagesSelectedListener {
    @BindView(R.id.imageView6)
    ImageView imageView6;
    private Bitmap hwbitmap;
    private int type;
    @BindView(R.id.multiselectSpinnerclass)
    MultiSelectSpinner multiselectSpinnerclass;
    @BindView(R.id.txtselection)
    TextView txtselection;
    @BindView(R.id.stu_layout)
    RelativeLayout stu_layout;

    //@BindView(R.id.spinnerselectstff)Spinner spinnerstaff;
    @BindView(R.id.imgBack)
    ImageView back;
    @BindView(R.id.parent)
    RelativeLayout parent;

    @BindView(R.id.txtTitle)
    TextView txtTitle;
    ArrayList<ClassBeans> classList = new ArrayList<>();

    String staff_id;
    List<String> classidsel;
    List<String> classname;
    @BindView(R.id.departtext)
    TextView departtext;

    @BindView(R.id.edtsubstaff)
    EditText comment;
    CommonProgress commonProgress;
    CheckBox toallstudent;
    private ViewGroup imageContainer;
    @BindView(R.id.scrollView)
    HorizontalScrollView scrollView;
    ArrayList<String> image;
    List<? extends Uri> image2;
    String encoded;

    int PICK_IMAGE_MULTIPLE = 1;
    String imageEncoded;
    List<String> imagesEncodedList;
    @BindView(R.id.spinnersection)
    Spinner spinnersection;
    ArrayList<SectionBean> sectionList = new ArrayList<>();
    String sectionid = "";
    String sectionname;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_date_sheet_new);
        ButterKnife.bind(this);
        commonProgress = new CommonProgress(this);
        imageContainer = findViewById(R.id.imageContainer);
        toallstudent = (CheckBox) findViewById(R.id.checkBoxallstud);
        classidsel = new ArrayList<>();
        classname = new ArrayList<>();
        txtTitle.setText("Add Date-sheet");
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        fetchclasslist();
        fetchSection();
        toallstudent.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (toallstudent.isChecked()) {
                    stu_layout.setVisibility(View.GONE);
                } else {
                    stu_layout.setVisibility(View.VISIBLE);

                }

            }
        });
    }

    private void fetchSection() {
        HashMap<String, String> params = new HashMap<String, String>();
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.SECTIONLIST_URL, sectionapiCallback, this, params);
    }

    ApiHandler.ApiCallback sectionapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            if (error == null) {
                try {
                    sectionList = SectionBean.parseCommonArray(jsonObject.getJSONArray("hostelroom_data"));
                    SectionnewAdapter adapter = new SectionnewAdapter(AddDateSheetNew.this, sectionList);
                    spinnersection.setAdapter(adapter);
                    spinnersection.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if (i > 0) {
                                sectionid = sectionList.get(i - 1).getSection_id();
                                sectionname = sectionList.get(i - 1).getSection_name();

                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Utils.showSnackBar(error.getMessage(), parent);
                if (error.getStatusCode() == 405) {
                    User.logout();
                    startActivity(new Intent(AddDateSheetNew.this, LoginActivity.class));
                    finish();
                }
            }
        }
    };

    private void fetchclasslist() {
        commonProgress.show();
        HashMap<String, String> params = new HashMap<String, String>();
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.CLASSLIST_URL, classapiCallback, this, params);
    }


    ApiHandler.ApiCallback classapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            commonProgress.dismiss();
            if (error == null) {
                try {
                    if (classList != null) {
                        classList.clear();
                    }
                    classList = ClassBeans.parseClassArray(jsonObject.getJSONArray(Constants.CLASSDATA));
//                    department_list = Deparment_bean.parseDepartmentArray(jsonArray);

                    List<String> dstring = new ArrayList<>();
                    for (ClassBeans cobj : classList) {
                        dstring.add(cobj.getName());
                    }

                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(AddDateSheetNew.this, android.R.layout.simple_list_item_multiple_choice, dstring);

                    multiselectSpinnerclass.setListAdapter(adapter).setListener(new BaseMultiSelectSpinner.MultiSpinnerListener() {
                                @Override
                                public void onItemsSelected(boolean[] selected) {
                                    //for (boolean res : selected) {
                                    //Log.e("taggg: ", "" + res);
                                    Log.e("dcfcds", "fdsf");
                                    String value = multiselectSpinnerclass.getSpinnerText();
                                    List<String> clas = new ArrayList<>();
                                    //List<String> departidsel = new ArrayList<>();
                                    classidsel.clear();

                                    if (!value.isEmpty()) {
                                        clas = Arrays.asList(value.split("\\s*,\\s*"));

                                        for (String dval : clas) {
                                            for (ClassBeans obj : classList) {
                                                if (obj.getName().equalsIgnoreCase(dval)) {
                                                    classidsel.add(obj.getId());
                                                    setData(clas.size(), "yes");
                                                    break;
                                                }
                                            }
                                        }
                                    } else {
                                        Utils.showSnackBar("Selection error!", parent);
                                        setData(0, "no");
                                    }
                                    Log.e("dcfcds", value + "  ,  classid:  " + classidsel);

                                }
                            }).setSelectAll(false)
                            .setSpinnerItemLayout(androidx.appcompat.R.layout.support_simple_spinner_dropdown_item)
                            .setTitle("Select Class")
                            .setMinSelectedItems(1)
                    ;


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Utils.showSnackBar(error.getMessage(), parent);
                if (error.getStatusCode() == 405) {
                    User.logout();
                    startActivity(new Intent(AddDateSheetNew.this, LoginActivity.class));
                    AddDateSheetNew.this.finish();
                }
            }
        }
    };

    private void setData(int size, String v) {
        txtselection.setVisibility(View.VISIBLE);
        departtext.setVisibility(View.GONE);

        String title = "", Name = "";
        if (v.equalsIgnoreCase("yes")) {
            title = getColoredSpanned("Total selected classes: ", "#9c9da1");
            Name = getColoredSpanned("<b>" + String.valueOf(size) + "</b>", "#656565");
        } else {
            title = getColoredSpanned("Total selected classes: ", "#656565");
            Name = getColoredSpanned("<b>" + "0" + "</b>", "#5A5C59");
        }
        txtselection.setText(Html.fromHtml(title + " " + Name));
    }

    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }

    @SuppressLint("ResourceType")
    @RequiresApi(api = Build.VERSION_CODES.M)
    @OnClick(R.id.textView6)
    public void click() {
        type = 1;
        //  CallCropAct();
        CallCropAct();
    }

    @SuppressLint("ResourceType")
    @RequiresApi(api = Build.VERSION_CODES.M)
    public void CallCropAct() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.UPSIDE_DOWN_CAKE) { // Android 14+ (API 34)
            //Toast.makeText(this, "Multi Images", Toast.LENGTH_LONG).show();
            // initialising intent
            Intent intent = new Intent();
            // setting type to select to be image
            intent.setType("image/*");
            // allowing multiple image to be selected
            intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(Intent.createChooser(intent, "Select Multiple Images"), PICK_IMAGE_MULTIPLE);

        } else if (Build.VERSION.SDK_INT > Build.VERSION_CODES.S) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_MEDIA_IMAGES)
                    != PackageManager.PERMISSION_GRANTED) {

                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_MEDIA_IMAGES)) {
                    Toast.makeText(this, "Please provide storage permission from app settings", Toast.LENGTH_LONG).show();
                } else {

                    ActivityCompat.requestPermissions(this,
                            new String[]{Manifest.permission.READ_MEDIA_IMAGES},
                            1);
                }
            } else {
                //Toast.makeText(this, "Multi Images", Toast.LENGTH_LONG).show();
                // initialising intent
                Intent intent = new Intent();
                // setting type to select to be image
                intent.setType("image/*");
                // allowing multiple image to be selected
                intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Multiple Images"), PICK_IMAGE_MULTIPLE);
            }

        } else {

            if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {

                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    Toast.makeText(this, "Please provide storage permission from app settings", Toast.LENGTH_LONG).show();
                } else {

                    ActivityCompat.requestPermissions(this,
                            new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                            1);

                }
            } else {
                new BottomSheetImagePicker.Builder(getString(R.xml.provider_paths))
                        .multiSelect(1, 6)
                        .multiSelectTitles(
                                R.plurals.pick_multi,
                                R.plurals.pick_multi_more,
                                R.string.pick_multi_limit
                        )
                        .peekHeight(R.dimen.peekHeight)
                        .columnSize(R.dimen.columnSize)
                        .requestTag("multi")
                        .show(getSupportFragmentManager(), null);
                // showFileChooser();
            }

        }

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            // When an Image is picked
            if (requestCode == PICK_IMAGE_MULTIPLE && resultCode == RESULT_OK
                    && null != data) {
                // Get the Image from data

                String[] filePathColumn = {MediaStore.Images.Media.DATA};
                imagesEncodedList = new ArrayList<String>();
                if (data.getData() != null) {

                    Uri mImageUri = data.getData();

                    // Get the cursor
                    Cursor cursor = getContentResolver().query(mImageUri,
                            filePathColumn, null, null, null);
                    // Move to first row
                    cursor.moveToFirst();

                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    imageEncoded = cursor.getString(columnIndex);
                    cursor.close();

                } else {
                    if (data.getClipData() != null) {
                        ClipData mClipData = data.getClipData();
                        ArrayList<Uri> mArrayUri = new ArrayList<Uri>();
                        for (int i = 0; i < mClipData.getItemCount(); i++) {

                            ClipData.Item item = mClipData.getItemAt(i);
                            Uri uri = item.getUri();
                            mArrayUri.add(uri);
                            // Get the cursor
                            Cursor cursor = getContentResolver().query(uri, filePathColumn, null, null, null);
                            // Move to first row
                            cursor.moveToFirst();

                            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                            imageEncoded = cursor.getString(columnIndex);
                            imagesEncodedList.add(imageEncoded);
                            cursor.close();

                        }
                        commonCode(mArrayUri);
                        Log.e("LOG_TAG", "Selected Images" + mArrayUri.size());
//                        Toast.makeText(this, "Selected Images" + imagesEncodedList.size(),
//                                Toast.LENGTH_LONG).show();
                    }
                }
            } else {
                Toast.makeText(this, "You haven't picked Image",
                        Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG)
                    .show();
            Log.e("LOG_TAG", "Exception is " + e);
        }


    }

    public static boolean checkPermission(Context mContext, String Permission) {
        int result = ContextCompat.checkSelfPermission(mContext, Permission);
        if (result == PackageManager.PERMISSION_GRANTED) {

            return true;
        } else {
            return false;
        }
    }

    @Override
    public void onImagesSelected(List<? extends Uri> list, String s) {
        commonCode(list);
    }

    private void commonCode(List<? extends Uri> list) {
        imageView6.setVisibility(View.GONE);
        scrollView.setVisibility(View.VISIBLE);
        imageContainer.removeAllViews();
        image = new ArrayList<>();
        image2 = list;
        for (Uri uri : list) {
            ImageView iv = (ImageView) LayoutInflater.from(this).inflate(R.layout.scrollitem_image, imageContainer, false);
            imageContainer.addView(iv);
            Glide.with(this).load(uri).into(iv);
            hwbitmap = Utils.getBitmapFromUri(AddDateSheetNew.this, uri, 2048);
            Log.e("hwbitmapup", String.valueOf(hwbitmap));
            encoded = Utils.encodeToBase64(hwbitmap, Bitmap.CompressFormat.JPEG, 50);
            // encoded = String.format("data:image/jpeg;base64,%s", encoded);
            image.add(encoded);
        }
        Log.e("hwbitmapupdown", String.valueOf(hwbitmap));
        Log.e("image2", String.valueOf(image2));
    }

    @OnClick(R.id.imageView6)
    public void imagecheck() {

        if (hwbitmap != null) {
            final Dialog EventDialog = new Dialog(this);
            EventDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            EventDialog.setCancelable(true);
            EventDialog.setContentView(R.layout.image_lyt);
            EventDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            ImageView imgShow = (ImageView) EventDialog.findViewById(R.id.imgShow);
            ImageView imgDownload = (ImageView) EventDialog.findViewById(R.id.imgDownload);
            imgDownload.setVisibility(View.GONE);
            imgShow.setImageBitmap(hwbitmap);
            // Picasso.with(this).load(String.valueOf(hwbitmap)).placeholder(getResources().getDrawable(R.drawable.home)).into(imgShow);

            EventDialog.show();
            Window window = EventDialog.getWindow();
            window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        } else {
            Toast.makeText(getApplicationContext(), "Please Select Image", Toast.LENGTH_LONG).show();
        }
    }

    @OnClick(R.id.send_messagestaff)
    public void clickSubmit() {
        if (hwbitmap == null) {
            Utils.hideKeyboard(this);
            Utils.showSnackBar("Please select Date sheet Image!", findViewById(R.id.parent));
        } else if (comment.getText().toString().length() <= 0) {
            Utils.hideKeyboard(this);
            Utils.showSnackBar("Please enter title!", findViewById(R.id.parent));
        } else {
            if (toallstudent.isChecked()) {
                if (sectionid.equalsIgnoreCase("")) {
                    Utils.hideKeyboard(this);
                    Utils.showSnackBar("Please select class section!", findViewById(R.id.parent));
                } else {
                    commonProgress.show();
                    HashMap<String, String> params = new HashMap<String, String>();
                    params.put("all", "1");
                    params.put("image", String.valueOf(image));
                    params.put("title", comment.getText().toString());
                    params.put("classid", String.valueOf(classidsel));
                    params.put("classname", String.valueOf(classname));
                    params.put("section_id", sectionid);
                    ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.DATE_SHEET_URLGDS, apiCallback, this, params);
                }
            } else {
                if (classidsel.size() <= 0) {
                    Utils.hideKeyboard(this);
                    Utils.showSnackBar("Please select class!", findViewById(R.id.parent));
                } else if (sectionid.equalsIgnoreCase("")) {
                    Utils.hideKeyboard(this);
                    Utils.showSnackBar("Please select class section!", findViewById(R.id.parent));
                } else {
                    commonProgress.show();
                    HashMap<String, String> params = new HashMap<String, String>();
                    params.put("all", "0");
//                  if (hwbitmap != null) {
//                      String encoded = Utils.encodeToBase64(hwbitmap, Bitmap.CompressFormat.JPEG, 50);
//                      encoded = String.format("data:image/jpeg;base64,%s", encoded);
//                      params.put("image", encoded);
//                  }
                    params.put("image", String.valueOf(image));
                    params.put("title", comment.getText().toString());
                    params.put("classid", String.valueOf(classidsel));
                    params.put("classname", String.valueOf(classname));
                    params.put("section_id", sectionid);
                    ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.DATE_SHEET_URLGDS, apiCallback, this, params);
                }

            }

        }
    }

    ApiHandler.ApiCallback apiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgressVert.cancelProgressBar();
            commonProgress.dismiss();
            if (error == null) {
                try {
                    Utils.showSnackBar(jsonObject.getString("msg"), findViewById(R.id.parent));
                    finish();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("error", error.getMessage() + "");
                Utils.showSnackBar(error.getMessage(), findViewById(R.id.parent));

                if (error.getStatusCode() == 405) {
                    User.logout();
                    startActivity(new Intent(AddDateSheetNew.this, LoginActivity.class));
                    finish();
                }
            }
        }
    };
}