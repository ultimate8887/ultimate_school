package com.ultimate.ultimatesmartschool.DateSheet;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.daimajia.swipe.SimpleSwipeListener;
import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;
import com.squareup.picasso.Picasso;
import com.ultimate.ultimatesmartschool.Homework.Homeworkbean;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DatesheetAdapter extends RecyclerSwipeAdapter<DatesheetAdapter.Viewholder> {
    ArrayList<DatesheetBean> hq_list;
    Mycallback mAdaptercall;
    Context listner;

    public DatesheetAdapter(ArrayList<DatesheetBean> hq_list, Context listener, Mycallback mAdaptercall) {
        this.mAdaptercall = mAdaptercall;
        this.hq_list = hq_list;
        this.listner = listener;
    }


    @Override
    public DatesheetAdapter.Viewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.datesheet_adpt_lyt, parent, false);
        DatesheetAdapter.Viewholder viewholder = new DatesheetAdapter.Viewholder(view);
        return viewholder;

    }
    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }

    @Override
    public void onBindViewHolder(final DatesheetAdapter.Viewholder holder, @SuppressLint("RecyclerView") final int position) {
     //   holder.card1.setAnimation(AnimationUtils.loadAnimation(listner,R.anim.fade_scale_animation));

        //animation = AnimationUtils.loadAnimation(listner, R.anim.btn_blink_animation);

        String Class_list = "";
        for (int i = 0; i < hq_list.get(position).getClass_ArrayList().size(); i++) {
            if (Class_list.length() > 0) {
                Class_list =Class_list+", "+ hq_list.get(position).getClass_ArrayList().get(i).getEs_name();
            }else{
                Class_list = hq_list.get(position).getClass_ArrayList().get(i).getEs_name();
            }
        }
            String title1 = getColoredSpanned("Classes:- ", "#000000");
            String Name1="";
            Name1 = getColoredSpanned(Class_list, "#395F89");
            holder.sub.setText(Html.fromHtml(title1 + " " + Name1));



        if (hq_list.get(position).getEs_id() != null) {
            String title = getColoredSpanned("ID:- ", "#000000");
            String Name = getColoredSpanned("ds-"+hq_list.get(position).getEs_id(), "#5A5C59");
            holder.id.setText(Html.fromHtml(title + " " + Name));
        }

        if (hq_list.get(position).getSection_name() != null) {
            String title = getColoredSpanned("Section:- ", "#000000");
            String Name = getColoredSpanned(""+hq_list.get(position).getSection_name(), "#5A5C59");
            holder.homeTopic.setText(Html.fromHtml(title + " " + Name));
        }else{
            String title = getColoredSpanned("Section:- ", "#000000");
            String Name = getColoredSpanned("All", "#5A5C59");
            holder.homeTopic.setText(Html.fromHtml(title + " " + Name));
        }

        holder.classess.setText(hq_list.get(position).getEs_title());


        //for Today
        java.util.Calendar calendar = java.util.Calendar.getInstance(TimeZone.getDefault());
        Date setdate = calendar.getTime();
        SimpleDateFormat fmtOut = new SimpleDateFormat("dd MMM, yyyy");
        String dateString = fmtOut.format(setdate);
        //for Yesterday
        DateFormat dateFormat = new SimpleDateFormat("dd MMM, yyyy");
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);
        String dateString1= dateFormat.format(cal.getTime());

        String check= Utils.getDateFormated(hq_list.get(position).getEs_date());
        if (check.equalsIgnoreCase(dateString)){
            //  holder.lytLine.setVisibility(View.VISIBLE);
            // holder.txtDT.setText("Today");
            String title = getColoredSpanned("Today", "#e31e25");
            //String Name = getColoredSpanned(messagelist.get(position).getFrom_type(), "#7D7D7D");
            String l_Name = getColoredSpanned("", "#5A5C59");
            holder.u_date.setText(Html.fromHtml(title + " " + l_Name));


            // holder.txtDT.setText("Today at "+time);
            //  holder.vieww.setBackground(mContext.getResources().getDrawable(R.drawable.a_card_lyt2));
        }else if (check.equalsIgnoreCase(dateString1)){

            String title = getColoredSpanned("Yesterday", "#1C8B3B");
            //String Name = getColoredSpanned(messagelist.get(position).getFrom_type(), "#7D7D7D");
            String l_Name = getColoredSpanned("", "#5A5C59");
            holder.u_date.setText(Html.fromHtml(title + " " + l_Name));

            //  holder.lytLine.setVisibility(View.GONE);
            //  holder.vieww.setBackground(mContext.getResources().getDrawable(R.drawable.card_lyt2));
        }else {
            holder.u_date.setText(Utils.getDateFormated(hq_list.get(position).getEs_date()));
        }

      //  holder.u_date.setText(Utils.getDateFormated(hq_list.get(position).getEs_date()));

        holder.nofile.setVisibility(View.VISIBLE);
        holder.audio_file.setVisibility(View.GONE);
        holder.pdf_file.setVisibility(View.GONE);

            holder.empty_file.setVisibility(View.GONE);
            holder.image_file.setVisibility(View.VISIBLE);
            Picasso.get().load(hq_list.get(position).getImage().get(0)).placeholder(listner.getResources().getDrawable(R.color.transparent)).into(holder.image_file);
            holder.image_file.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(mAdaptercall!= null){
                        mAdaptercall.onMethodCallback(hq_list.get(position));
                    }
                }
            });

        holder.swipeLayout.setShowMode(SwipeLayout.ShowMode.PullOut);
        holder.swipeLayout.addSwipeListener(new SimpleSwipeListener() {
            @Override
            public void onOpen(SwipeLayout layout) {

            }

            @Override
            public void onStartClose(SwipeLayout layout) {
                holder.swipeLayout.close();
            }
        });


        holder.trash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder1 = new AlertDialog.Builder(listner);
                builder1.setMessage("Do you want to delete? ");
                builder1.setCancelable(false);
                builder1.setPositiveButton(
                        "Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                                if (mAdaptercall != null) {
                                    mAdaptercall.onDelecallback(hq_list.get(position));
//                                    Toast.makeText(listner, "deleted", Toast.LENGTH_SHORT).show();
                                }
                                // mAdapterCallback.deleteNotification(notificationList.get(postion));
                            }
                        });
                builder1.setNegativeButton(
                        "No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        });
                AlertDialog alert11 = builder1.create();
                alert11.show();
            }
        });
    }

    public interface Mycallback {
        public void onMethodCallback(DatesheetBean homeworkbean);
        public void onDelecallback(DatesheetBean homeworkbean);
    }

    @Override
    public int getItemCount() {
        return hq_list.size();
    }

    public void setHQList(ArrayList<DatesheetBean> hq_list) {
        this.hq_list = hq_list;
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe;
    }


    public class Viewholder extends RecyclerView.ViewHolder {
        public TextView u_date,homeTopic,sub;
        public ImageView image_file, audio_file, pdf_file;
        public RelativeLayout withfile;
        @BindView(R.id.nofile)
        RelativeLayout nofile;
        @BindView(R.id.empty_file)
        ImageView empty_file;
        @BindView(R.id.classess)
        TextView classess;
        @BindView(R.id.id)
        TextView id;
        SwipeLayout swipeLayout;
        public ImageView trash, update;

        public Viewholder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            sub = (TextView) itemView.findViewById(R.id.subject);
            u_date = (TextView) itemView.findViewById(R.id.u_date);
            homeTopic = (TextView) itemView.findViewById(R.id.homeTopic);
            image_file = (ImageView) itemView.findViewById(R.id.image_file);
            pdf_file = (ImageView) itemView.findViewById(R.id.pdf_file);
            audio_file = (ImageView) itemView.findViewById(R.id.audio_file);
            withfile = (RelativeLayout) itemView.findViewById(R.id.head);
            swipeLayout = (SwipeLayout) itemView.findViewById(R.id.swipe);
            trash = (ImageView) itemView.findViewById(R.id.trash);
            update = (ImageView) itemView.findViewById(R.id.update);
            sub.setVisibility(View.VISIBLE);
        }
    }
}
