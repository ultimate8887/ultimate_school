package com.ultimate.ultimatesmartschool.DateSheet;

import com.ultimate.ultimatesmartschool.Utility.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class DatesheetBean {
    private String es_id;
    private String es_title;
    private String es_image;
    private String es_date;
    private String es_class;

    public String getSection_name() {
        return section_name;
    }

    public void setSection_name(String section_name) {
        this.section_name = section_name;
    }

    private String section_name;
     private static String IMAGE="image";
    private ArrayList<ClassArrayListBean> class_ArrayList;

    public ArrayList<String> getImage() {
        return image;
    }

    public void setImage(ArrayList<String> image) {
        this.image = image;
    }

    private ArrayList<String> image;

    public String getEs_id() {
        return es_id;
    }

    public void setEs_id(String es_id) {
        this.es_id = es_id;
    }

    public String getEs_title() {
        return es_title;
    }

    public void setEs_title(String es_title) {
        this.es_title = es_title;
    }

    public String getEs_image() {
        return es_image;
    }

    public void setEs_image(String es_image) {
        this.es_image = es_image;
    }

    public String getEs_date() {
        return es_date;
    }

    public void setEs_date(String es_date) {
        this.es_date = es_date;
    }

    public String getEs_class() {
        return es_class;
    }

    public void setEs_class(String es_class) {
        this.es_class = es_class;
    }

//    public String getImage() {
//        return image;
//    }
//
//    public void setImage(String image) {
//        this.image = image;
//    }

    public ArrayList<ClassArrayListBean> getClass_ArrayList() {
        return class_ArrayList;
    }

    public void setClass_ArrayList(ArrayList<ClassArrayListBean> class_ArrayList) {
        this.class_ArrayList = class_ArrayList;
    }

    public static class ClassArrayListBean {
        /**
         * es_id : 1
         * es_name : 6th
         * es_cid : 1
         * es_did : 1
         */

        private String es_id;
        private String es_name;
        private String es_cid;
        private String es_did;

        public String getEs_id() {
            return es_id;
        }

        public void setEs_id(String es_id) {
            this.es_id = es_id;
        }

        public String getEs_name() {
            return es_name;
        }

        public void setEs_name(String es_name) {
            this.es_name = es_name;
        }

        public String getEs_cid() {
            return es_cid;
        }

        public void setEs_cid(String es_cid) {
            this.es_cid = es_cid;
        }

        public String getEs_did() {
            return es_did;
        }

        public void setEs_did(String es_did) {
            this.es_did = es_did;
        }

        public static ArrayList<ClassArrayListBean> parseClassArray(JSONArray arrayObj) {
            ArrayList list = new ArrayList<ClassArrayListBean>();
            try {
                for (int i = 0; i < arrayObj.length(); i++) {
                    ClassArrayListBean p = parseClassObject(arrayObj.getJSONObject(i));
                    if (p != null) {
                        list.add(p);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return list;
        }

        public static ClassArrayListBean parseClassObject(JSONObject jsonObject) {
            ClassArrayListBean casteObj = new ClassArrayListBean();
            try {
                if (jsonObject.has("es_id")) {
                    casteObj.setEs_id(jsonObject.getString("es_id"));
                }
                if (jsonObject.has("es_name") && !jsonObject.getString("es_name").isEmpty() && !jsonObject.getString("es_name").equalsIgnoreCase("null")) {
                    casteObj.setEs_name(jsonObject.getString("es_name"));
                }
                if (jsonObject.has("es_cid") && !jsonObject.getString("es_cid").isEmpty() && !jsonObject.getString("es_cid").equalsIgnoreCase("null")) {
                    casteObj.setEs_cid(jsonObject.getString("es_cid"));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return casteObj;
        }
    }

    public static ArrayList<DatesheetBean> parseClassArray(JSONArray arrayObj) {
        ArrayList list = new ArrayList<DatesheetBean>();
        try {
            for (int i = 0; i < arrayObj.length(); i++) {
                DatesheetBean p = parseClassObject(arrayObj.getJSONObject(i));
                if (p != null) {
                    list.add(p);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public static DatesheetBean parseClassObject(JSONObject jsonObject) {
        DatesheetBean casteObj = new DatesheetBean();
        try {
            if (jsonObject.has("es_id")) {
                casteObj.setEs_id(jsonObject.getString("es_id"));
            }
            if (jsonObject.has("es_title") && !jsonObject.getString("es_title").isEmpty() && !jsonObject.getString("es_title").equalsIgnoreCase("null")) {
                casteObj.setEs_title(jsonObject.getString("es_title"));
            }
            if (jsonObject.has("section_name") && !jsonObject.getString("section_name").isEmpty() && !jsonObject.getString("section_name").equalsIgnoreCase("null")) {
                casteObj.setSection_name(jsonObject.getString("section_name"));
            }
            if (jsonObject.has("es_date") && !jsonObject.getString("es_date").isEmpty() && !jsonObject.getString("es_date").equalsIgnoreCase("null")) {
                casteObj.setEs_date(jsonObject.getString("es_date"));
            }
            if (jsonObject.has(IMAGE) && !jsonObject.getString(IMAGE).isEmpty() && !jsonObject.getString(IMAGE).equalsIgnoreCase("null")) {
                JSONArray img_arr = jsonObject.getJSONArray(IMAGE);
                ArrayList<String> img_arrs = new ArrayList<>();
                for(int i=0;i<img_arr.length();i++){
                    img_arrs.add(Constants.getImageBaseURL()+img_arr.getString(i));
                }
                casteObj.setImage(img_arrs);
            }
            if (jsonObject.has("class_list")) {
                casteObj.setClass_ArrayList(ClassArrayListBean.parseClassArray(jsonObject.getJSONArray("class_list")));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return casteObj;
    }

}
