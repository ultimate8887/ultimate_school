package com.ultimate.ultimatesmartschool.DateSheet;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.ultimate.ultimatesmartschool.BeanModule.User;
import com.ultimate.ultimatesmartschool.ClassWork.CWViewPagerAdapter;
import com.ultimate.ultimatesmartschool.Login.LoginActivity;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.CommonProgress;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.relex.circleindicator.CircleIndicator;

public class ViewDatesheet extends AppCompatActivity implements DatesheetAdapter.Mycallback{
    @BindView(R.id.recyclerView)
    RecyclerView recyclerview;
    ArrayList<DatesheetBean> datesheet_list = new ArrayList<>();
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    private DatesheetAdapter adapter;
    @BindView(R.id.textNorecord)
    TextView txtNorecord;
    @BindView(R.id.parent)
    RelativeLayout parent;
    Dialog mBottomSheetDialog;
    public ViewPager intro_images;
    LinearLayout pager_indicator;
    public CircleIndicator indicator;
    public TextView textView;
    CommonProgress commonProgress;
    @BindView(R.id.totalRecord)
    TextView totalRecord;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_gallery);
        ButterKnife.bind(this);
        commonProgress=new CommonProgress(this);
        recyclerview.setLayoutManager(new LinearLayoutManager(ViewDatesheet.this));
        adapter = new DatesheetAdapter(datesheet_list, ViewDatesheet.this, this);
        recyclerview.setAdapter(adapter);
        txtTitle.setText("Date-sheet");
    }

    @Override
    protected void onResume() {
        super.onResume();
        fetchDatesheet();
    }

    @Override
    public void onDelecallback(DatesheetBean homeworkbean) {
        commonProgress.show();
        HashMap<String, String> params = new HashMap<>();
        params.put("n_id", homeworkbean.getEs_id());
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.DELETEdatesheet, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                ErpProgress.cancelProgressBar();
                commonProgress.dismiss();
                if (error == null) {
                    int pos = datesheet_list.indexOf(homeworkbean);
                    datesheet_list.remove(pos);
                    if (datesheet_list.size() > 0) {
                        adapter.setHQList(datesheet_list);
                        adapter.notifyDataSetChanged();
                        txtNorecord.setVisibility(View.GONE);
                        totalRecord.setText("Total Entries:- "+String.valueOf(datesheet_list.size()));
                    } else {
                        totalRecord.setText("Total Entries:- 0");
                        adapter.setHQList(datesheet_list);
                        adapter.notifyDataSetChanged();
                        txtNorecord.setVisibility(View.VISIBLE);
                    }
                    //      Utility.openSuccessDialog("Deleted!","Classwork entry deleted successfully.","",ViewClassWork.this);
                    Utils.showSnackBar("Deleted Successfully!", parent);
                    totalRecord.setText("Total Entries:- "+String.valueOf(datesheet_list.size()));
                } else {
                    //  Utility.openErrorDialog(error.getMessage(),ViewClassWork.this);
                    Utils.showSnackBar(error.getMessage(), parent);

                }
            }
        }, this, params);
    }


    @OnClick(R.id.imgBack)
    public void callBack() {
        finish();
    }

    public void fetchDatesheet() {
        commonProgress.show();
        ApiHandler.apiHit(Request.Method.GET, Constants.getBaseURL() + Constants.DATE_SHEET_URL, apiCallback, this, null);
    }

    ApiHandler.ApiCallback apiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            commonProgress.dismiss();
            if (error == null) {
                try {
                    if (datesheet_list != null) {
                        datesheet_list.clear();
                    }
                    JSONArray jsonArray = jsonObject.getJSONArray("datesheet");
                    datesheet_list = DatesheetBean.parseClassArray(jsonArray);
                    if (datesheet_list.size() > 0) {
                        adapter.setHQList(datesheet_list);
                        adapter.notifyDataSetChanged();
                        txtNorecord.setVisibility(View.GONE);
                        totalRecord.setText("Total Entries:- "+String.valueOf(datesheet_list.size()));
                    } else {
                        totalRecord.setText("Total Entries:- 0");
                        adapter.setHQList(datesheet_list);
                        adapter.notifyDataSetChanged();
                        txtNorecord.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                totalRecord.setText("Total Entries:- 0");
                txtNorecord.setVisibility(View.VISIBLE);
                datesheet_list.clear();
                adapter.setHQList(datesheet_list);
                adapter.notifyDataSetChanged();
            }
        }
    };


    @Override
    public void onMethodCallback(DatesheetBean data) {
        final Dialog warningDialog = new Dialog(this);
        warningDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        warningDialog.setCancelable(true);

        warningDialog.setContentView(R.layout.class_img_dialog);
        warningDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        RelativeLayout btnClose = (RelativeLayout) warningDialog.findViewById(R.id.btnNo);

        indicator = (CircleIndicator) warningDialog.findViewById(R.id.indicator);
        textView=(TextView) warningDialog.findViewById(R.id.clsswrktexttt);
        intro_images=(ViewPager) warningDialog.findViewById(R.id.pager_introduction);

        TextView tap_count=(TextView) warningDialog.findViewById(R.id.tap_count);
        String title1 = getColoredSpanned("<b>"+"1"+"</b>"+"", "#F4212C");
        String l_Name1 = getColoredSpanned(" / "+String.valueOf(data.getImage().size()), "#000000");
        tap_count.setText(Html.fromHtml(title1 + " " + l_Name1+ " "));

        textView.setText(data.getEs_title());
        CWViewPagerAdapter mAdapter = new  CWViewPagerAdapter(this,data.getImage(),"msg");
        intro_images.setAdapter(mAdapter);
        intro_images.setCurrentItem(0);
        indicator.setViewPager(intro_images);

        intro_images.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int arg0) {
                // TODO Auto-generated method stub
                int c=arg0+1;
                String title1 = getColoredSpanned("<b>"+String.valueOf(c)+"</b>"+"", "#F4212C");
                String l_Name1 = getColoredSpanned(" / "+String.valueOf(data.getImage().size()), "#000000");
                tap_count.setText(Html.fromHtml(title1 + " " + l_Name1+ " "));
            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
                // TODO Auto-generated method stub

                System.out.println("onPageScrolled");
            }

            @Override
            public void onPageScrollStateChanged(int num) {
                // TODO Auto-generated method stub

            }
        });


        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                warningDialog.dismiss();
            }
        });
        warningDialog.show();
        Window window = warningDialog.getWindow();
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

    }
    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }
}
