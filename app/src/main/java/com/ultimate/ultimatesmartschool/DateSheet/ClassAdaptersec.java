package com.ultimate.ultimatesmartschool.DateSheet;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import androidx.recyclerview.widget.RecyclerView;

import com.ultimate.ultimatesmartschool.BeanModule.ClassBean;
import com.ultimate.ultimatesmartschool.R;

import java.util.ArrayList;

public class ClassAdaptersec extends RecyclerView.Adapter<ClassAdaptersec.Viewholder> {

    private final Context mContext;
    private ArrayList<ClassBean> classList;

    public ClassAdaptersec(ArrayList<ClassBean> classList,Context mContext) {
        this.classList = classList;
        this.mContext = mContext;

    }


    @Override
    public Viewholder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.permisionlist_lay, viewGroup, false);
        ClassAdaptersec.Viewholder viewholder = new ClassAdaptersec.Viewholder(view);
        return viewholder;

    }

    @Override
    public void onBindViewHolder(final Viewholder viewholder, final int i) {

        viewholder.checkBox.setText(classList.get(i).getName());
        //in some cases, it will prevent unwanted situations
        viewholder.checkBox.setOnCheckedChangeListener(null);

        viewholder.checkBox.setChecked(classList.get(i).isChecked());
        viewholder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                classList.get(i).setChecked(isChecked);
            }
        });


    }

    @Override
    public int getItemCount() {
        return classList.size();
    }

    public void setHQList(ArrayList<ClassBean> permsnList) {
        this.classList = permsnList;
    }

    public ArrayList<ClassBean> getClassList() {
        return classList;
    }


    public class Viewholder extends RecyclerView.ViewHolder {
        public CheckBox checkBox;

        public Viewholder(View itemView) {
            super(itemView);
            checkBox = (CheckBox) itemView.findViewById(R.id.setUpCheckBox);

        }
    }
}
