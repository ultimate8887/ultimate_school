package com.ultimate.ultimatesmartschool.DateSheet;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;

import com.ultimate.ultimatesmartschool.BeanModule.ClassBean;
import com.ultimate.ultimatesmartschool.BeanModule.User;
import com.ultimate.ultimatesmartschool.Login.LoginActivity;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AddDateSheet extends AppCompatActivity {
    ArrayList<ClassBean> classList = new ArrayList<>();
    @BindView(R.id.parent)
    RelativeLayout parent;
    //@BindView(R.id.spinner) Spinner spinnerClass;

    String classid = "";
    private int loaded = 0;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    List<String> classidsel;
    private int type;
    private Bitmap hwbitmap;
    @BindView(R.id.imageView6)
    ImageView imageView6;
    private final int LOAD_FILE_RESULTS = 1000;
    String fileBase64 = null;

    @BindView(R.id.textnote)TextView textnote;

    @BindView(R.id.comment)
    EditText comment;
    @BindView(R.id.recyclerview)
    RecyclerView recyclerview;
    private ClassAdaptersec adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_date_sheet);
        ButterKnife.bind(this);
        recyclerview.setLayoutManager(new GridLayoutManager(AddDateSheet.this, 3));
        adapter = new ClassAdaptersec(classList, AddDateSheet.this);
        recyclerview.setAdapter(adapter);
        fetchClass();
        txtTitle.setText("DateSheet");
    }

    private void fetchClass() {
        if (loaded == 0) {
            ErpProgress.showProgressBar(this, "Please wait...");
        }
        loaded++;

        HashMap<String, String> params = new HashMap<String, String>();
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.CLASSLIST_URL, classapiCallback, this, params);
    }

    ApiHandler.ApiCallback classapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            if (error == null) {
                try {
                    classList = ClassBean.parseClassArray(jsonObject.getJSONArray(Constants.CLASSDATA));
                    adapter.setHQList(classList);
                    adapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Utils.showSnackBar(error.getMessage(), findViewById(R.id.parent));
                finish();
                if (error.getStatusCode() == 405) {
                    User.logout();
                    startActivity(new Intent(AddDateSheet.this, LoginActivity.class));
                    finish();
                }
            }
        }
    };
    @OnClick(R.id.imgBack)
    public void callBack() {
        finish();
    }
    @RequiresApi(api = Build.VERSION_CODES.M)
    @OnClick(R.id.addimage)
    public void click() {
        type = 1;
        CallCropAct();
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public void CallCropAct() {

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                
            }
        }
    }

    public static boolean checkPermission(Context mContext, String Permission) {
        int result = ContextCompat.checkSelfPermission(mContext, Permission);
        if (result == PackageManager.PERMISSION_GRANTED) {

            return true;
        } else {
            return false;
        }
    }
    @OnClick(R.id.imageView6)
    public void imagecheck() {

        if (hwbitmap != null) {
            final Dialog EventDialog = new Dialog(this);
            EventDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            EventDialog.setCancelable(true);
            EventDialog.setContentView(R.layout.image_lyt);
            EventDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            ImageView imgShow = (ImageView) EventDialog.findViewById(R.id.imgShow);
            ImageView imgDownload = (ImageView) EventDialog.findViewById(R.id.imgDownload);
            imgDownload.setVisibility(View.GONE);
            imgShow.setImageBitmap(hwbitmap);
            // Picasso.with(this).load(String.valueOf(hwbitmap)).placeholder(getResources().getDrawable(R.drawable.home)).into(imgShow);

            EventDialog.show();
            Window window = EventDialog.getWindow();
            window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        } else{
            Toast.makeText(getApplicationContext(),"Please Select HomeWork Image",Toast.LENGTH_LONG).show();
        }
    }

    @OnClick(R.id.button3)
    public void clickSubmit() {
        if (hwbitmap == null) {
            Utils.hideKeyboard(this);
            Utils.showSnackBar("Please select Date sheet Image!", findViewById(R.id.parent));
        } else if (comment.getText().toString().length() <= 0) {
            Utils.hideKeyboard(this);
            Utils.showSnackBar("Please enter title!", findViewById(R.id.parent));
        } else {
            ArrayList<ClassBean> cList = adapter.getClassList();
            String classIdList = "";
            String classNameList = "";
            for (int i = 0; i < cList.size(); i++) {
                if (cList.get(i).isChecked()) {
                    if (classIdList.length() > 0) {
                        classIdList = classIdList+"," + cList.get(i).getId();
                        classNameList = classNameList+"," + cList.get(i).getName();
                    } else {
                        classNameList = cList.get(i).getName();
                        classIdList = cList.get(i).getId();
                    }
                }
            }
            if (classIdList.length() <= 0) {
                Utils.hideKeyboard(this);
                Utils.showSnackBar("Please select class!", findViewById(R.id.parent));
            } else {
                ErpProgress.showProgressBar(this, "Please wait...");
                HashMap<String, String> params = new HashMap<String, String>();
                if (hwbitmap != null) {
                    String encoded = Utils.encodeToBase64(hwbitmap, Bitmap.CompressFormat.JPEG, 50);
                    encoded = String.format("data:image/jpeg;base64,%s", encoded);
                    params.put("image", encoded);
                }
                params.put("title", comment.getText().toString());
                params.put("classid", classIdList);
                params.put("classname", classNameList);
                ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.DATE_SHEET_URL, apiCallback, this, params);

            }
        }
    }
    ApiHandler.ApiCallback apiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            if (error == null) {
                try {
                    Utils.showSnackBar(jsonObject.getString("msg"),  findViewById(R.id.parent));
                    finish();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("error", error.getMessage() + "");
                Utils.showSnackBar(error.getMessage(),  findViewById(R.id.parent));

                if (error.getStatusCode() == 405) {
                    User.logout();
                    startActivity(new Intent(AddDateSheet.this, LoginActivity.class));
                    finish();
                }
            }
        }
    };
}
