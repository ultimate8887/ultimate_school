package com.ultimate.ultimatesmartschool.Social_Post;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.ultimate.ultimatesmartschool.BeanModule.User;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgressVert;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AddSocialLinks extends AppCompatActivity {
    @BindView(R.id.leaveradio)
    RadioGroup radioGroup;
    @BindView(R.id.radioone)
    RadioButton radioone;
    @BindView(R.id.radiomulti)RadioButton radiomulti;
    @BindView(R.id.todate)
    TextView todate;
    @BindView(R.id.date)TextView fromdate;
    @BindView(R.id.applyleave)
    Button applyleave;
    @BindView(R.id.edtAddreason)
    EditText edittextreason;
    @BindView(R.id.parentsss)
    RelativeLayout parentss;@BindView(R.id.txtTitle)
    TextView txtTitle;
    public String to_date;
    public String from_date;
    private String s_id;

    @BindView(R.id.img)
    ImageView img;
    @BindView(R.id.img1)
    ImageView img1;
    @BindView(R.id.img2)
    ImageView img2;

    @BindView(R.id.text_s)
    TextView text_s;
    @BindView(R.id.text1_s)
    TextView text1_s;
    @BindView(R.id.text2_s)
    TextView text2_s;

    @BindView(R.id.edtTime)
    EditText edtTitle;

    @BindView(R.id.textView11)
    TextView textView11;

    @BindView(R.id.multi)
    RelativeLayout lyt2;

    @BindView(R.id.cal_img1)
    ImageView cal_img1;
    @BindView(R.id.cal_img2)
    ImageView cal_img2;

    @BindView(R.id.hDay)
    RelativeLayout hDay;
    @BindView(R.id.oneDay)
    RelativeLayout oneDay;
    @BindView(R.id.multiDay)
    RelativeLayout multiDay;
    String apply="instagram";
    SharedPreferences sharedPreferences;
    @BindView(R.id.speak_text)
    ImageView speak_text;
    private static final int REQUEST_CODE_SPEECH_INPUT = 1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_social_links);
        ButterKnife.bind(this);
        txtTitle.setText("Add Social Post");
        s_id= User.getCurrentUser().getId();
        // abcd=radioGroup.getCheckedRadioButtonId();

    }


    @OnClick(R.id.hDay)
    public void hDay() {
        apply="facebook";
        radioone.setChecked(true);
        todate.setVisibility(View.GONE);
        lyt2.setVisibility(View.GONE);
        img1.setVisibility(View.GONE);
        img.setVisibility(View.VISIBLE);
        text1_s.setVisibility(View.GONE);
        text2_s.setVisibility(View.GONE);
        text_s.setVisibility(View.VISIBLE);
        img2.setVisibility(View.GONE);
      //  edittextreason.setText(" ");
        oneDay.setBackgroundColor(Color.parseColor("#00000000"));
        multiDay.setBackgroundColor(Color.parseColor("#00000000"));
        hDay.setBackgroundColor(Color.parseColor("#66000000"));
    }


    @OnClick(R.id.oneDay)
    public void oneDay() {
        apply="instagram";
        radioone.setChecked(true);
        todate.setVisibility(View.GONE);
        lyt2.setVisibility(View.GONE);
        img.setVisibility(View.GONE);
        img1.setVisibility(View.VISIBLE);
        img2.setVisibility(View.GONE);
        text_s.setVisibility(View.GONE);
        text2_s.setVisibility(View.GONE);
        text1_s.setVisibility(View.VISIBLE);
       // edittextreason.setText(" ");
        hDay.setBackgroundColor(Color.parseColor("#00000000"));
        multiDay.setBackgroundColor(Color.parseColor("#00000000"));
        oneDay.setBackgroundColor(Color.parseColor("#66000000"));
    }

    @OnClick(R.id.multiDay)
    public void multiDay() {
        apply="youtube";
        radiomulti.setChecked(true);
        todate.setVisibility(View.VISIBLE);
        lyt2.setVisibility(View.GONE);
        img.setVisibility(View.GONE);
        img2.setVisibility(View.VISIBLE);
        img1.setVisibility(View.GONE);
        text_s.setVisibility(View.GONE);
        text1_s.setVisibility(View.GONE);
        text2_s.setVisibility(View.VISIBLE);
       // edittextreason.setText("");
        hDay.setBackgroundColor(Color.parseColor("#00000000"));
        oneDay.setBackgroundColor(Color.parseColor("#00000000"));
        multiDay.setBackgroundColor(Color.parseColor("#66000000"));
    }

    @OnClick(R.id.imgBack)
    public void onback() {
        finish();
    }

    @OnClick(R.id.applyleave)
    public void applyleave(){

            if (checkValidsec()) {
                //Toast.makeText(getApplicationContext(), "Apply sucess1", Toast.LENGTH_LONG).show();
                ErpProgressVert.showProgressBar(this,"Please wait...");
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("post",edittextreason.getText().toString());
                params.put("title",edtTitle.getText().toString());
                params.put("post_type",apply);
                ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.ADDPOST, new ApiHandler.ApiCallback() {
                    @Override
                    public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                        ErpProgressVert.cancelProgressBar();
                        if (error == null) {
                            try {
                                Toast.makeText(getApplicationContext(),jsonObject.getString("msg"),Toast.LENGTH_LONG).show();
                                Utils.showSnackBar(jsonObject.getString("msg"), parentss);
                                finish();

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else {
                            Log.e("error", error.getMessage() + "");
                            Utils.showSnackBar(error.getMessage(), parentss);


                        }
                    }
                }, this, params);
            }

    }

    private boolean checkValidsec() {
        boolean valid = true;
        String errorMsg = null;
        if (edtTitle.getText().toString().trim().length() <= 0) {
            valid = false;
            errorMsg = "Kindly add title";
        }
        else if (edittextreason.getText().toString().trim().length() <= 0) {
            valid = false;
            errorMsg = "Kindly add social post link";
        }

        if (!valid) {
            Utils.showSnackBar(errorMsg, parentss);
        }
        return valid;
    }


}