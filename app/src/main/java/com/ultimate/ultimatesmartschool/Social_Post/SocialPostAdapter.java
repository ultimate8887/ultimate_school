package com.ultimate.ultimatesmartschool.Social_Post;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.daimajia.swipe.SimpleSwipeListener;
import com.daimajia.swipe.SwipeLayout;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.ultimate.ultimatesmartschool.Home.HolidayBean;
import com.ultimate.ultimatesmartschool.Home.Holidayadapter;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Transport.FuelExpenseAdapter;
import com.ultimate.ultimatesmartschool.Transport.Fuelbean;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SocialPostAdapter extends RecyclerView.Adapter<SocialPostAdapter.Viewholder> {
    Context mContext;
    ArrayList<SocialPostBean> holidayList;
    StudentPro mStudentPro;
    int value = 0;

    public SocialPostAdapter(ArrayList<SocialPostBean> holidayList, Context mContext,StudentPro mStudentPro) {
        this.holidayList = holidayList;
        this.mContext = mContext;
        this.mStudentPro = mStudentPro;
        this.value = value;
    }

    public interface StudentPro{
        public void onDelecallback(SocialPostBean std);
    }


    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adpt_post_list, parent, false);
        Viewholder viewholder = new Viewholder(view);
        return viewholder;
    }

    @Override
    public void onBindViewHolder(@NonNull Viewholder holder, @SuppressLint("RecyclerView") int position) {


            holder.holiday.setVisibility(View.VISIBLE);
            holder.home.setVisibility(View.GONE);
            if (holidayList.get(position).getId() != null) {
                String title = getColoredSpanned("ID: ", "#000000");
                String Name = getColoredSpanned("post-" + holidayList.get(position).getId(), "#5A5C59");
                holder.albm_id.setText(Html.fromHtml(title + " " + Name));
            }
            Drawable drawable;
            if (holidayList.get(position).getPost_type().equalsIgnoreCase("youtube")){
                 drawable = mContext.getResources().getDrawable(R.drawable.youtube);
            } else if (holidayList.get(position).getPost_type().equalsIgnoreCase("facebook")) {
                drawable = mContext.getResources().getDrawable(R.drawable.facebook);
            }else {
                drawable = mContext.getResources().getDrawable(R.drawable.instagram);
            }

            holder.h_date.setCompoundDrawablesRelativeWithIntrinsicBounds(drawable, null, null, null);

            if (holidayList.get(position).getTitle() != null) {
                String title = getColoredSpanned("Title: ", "#000000");
                String Name = getColoredSpanned("" + holidayList.get(position).getTitle(), "#5A5C59");
                holder.title.setText(Html.fromHtml(title + " " + Name));
            }

            if (holidayList.get(position).getPost() != null) {
                String title = getColoredSpanned("", "#000000");
                String Name = getColoredSpanned("<u>" + holidayList.get(position).getPost()+"</u>", "#5A5C59");
                holder.h_date.setText(Html.fromHtml(title + " " + Name));
            }

            if (holidayList.get(position).getDate() != null) {
                String title = getColoredSpanned("Post on: ", "#000000");
                String Name = getColoredSpanned("" + Utils.getDateTimeFormatedWithAMPM(holidayList.get(position).getDate()), "#5A5C59");
                holder.createdon.setText(Html.fromHtml(title + " " + Name));
            }

        //for Today
        Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
        Date setdate = calendar.getTime();
        SimpleDateFormat fmtOut = new SimpleDateFormat("dd MMM, yyyy");
        String dateString = fmtOut.format(setdate);
        //for Yesterday
        DateFormat dateFormat = new SimpleDateFormat("dd MMM, yyyy");
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);
        String dateString1= dateFormat.format(cal.getTime());

        String check= Utils.getDateFormated(holidayList.get(position).getDate());
        if (check.equalsIgnoreCase(dateString)){
            String title = getColoredSpanned("Today", "#e31e25");
            String l_Name = getColoredSpanned("Post on:", "#5A5C59");
            holder.createdon.setText(Html.fromHtml(l_Name + " " + title));

        }else if (check.equalsIgnoreCase(dateString1)){
            String title = getColoredSpanned("Yesterday", "#1C8B3B");
            String l_Name = getColoredSpanned("Post on:", "#5A5C59");
            holder.createdon.setText(Html.fromHtml(l_Name + " " + title));
        }else {
                String title = getColoredSpanned("Post on: ", "#000000");
                String Name = getColoredSpanned("" + Utils.getDateTimeFormatedWithAMPM(holidayList.get(position).getDate()), "#5A5C59");
                holder.createdon.setText(Html.fromHtml(title + " " + Name));
        }

        holder.swipeLayout.setShowMode(SwipeLayout.ShowMode.PullOut);
        holder.swipeLayout.addSwipeListener(new SimpleSwipeListener() {
            @Override
            public void onOpen(SwipeLayout layout) {

            }

            @Override
            public void onStartClose(SwipeLayout layout) {
                holder.swipeLayout.close();
            }
        });


        holder.trash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder1 = new AlertDialog.Builder(mContext);
                builder1.setMessage("Do you want to delete? ");
                builder1.setCancelable(false);
                builder1.setPositiveButton(
                        "Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                                if (mStudentPro != null) {
                                    mStudentPro.onDelecallback(holidayList.get(position));
//                                    Toast.makeText(listner, "deleted", Toast.LENGTH_SHORT).show();
                                }
                                // mAdapterCallback.deleteNotification(notificationList.get(postion));
                            }
                        });
                builder1.setNegativeButton(
                        "No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        });
                AlertDialog alert11 = builder1.create();
                alert11.show();
            }
        });
        Animation animation = AnimationUtils.loadAnimation(mContext, R.anim.btn_blink_animation);

        holder.open_link.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.h_date.startAnimation(animation);
                mContext.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(holidayList.get(position).getPost())));

            }
        });

    }

    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }

    @Override
    public int getItemCount() {
        return holidayList.size();
    }

    public void setHList(ArrayList<SocialPostBean> holidayList) {
        this.holidayList = holidayList;
    }

    public class Viewholder extends RecyclerView.ViewHolder {

        @BindView(R.id.hol)
        RelativeLayout holiday;

        @BindView(R.id.home)
        RelativeLayout home;

        @BindView(R.id.open_link)
        LinearLayout open_link;

        @BindView(R.id.txtHoliday)
        TextView txtHoliday;

        @BindView(R.id.img)
        ImageView img;

        @BindView(R.id.albm_id)
        TextView albm_id;

        @BindView(R.id.title)
        TextView title;

        @BindView(R.id.albm_name)
        TextView h_date;

        @BindView(R.id.status)
        TextView createdon;

        ImageView trash,imgOfDialog,update;

        SwipeLayout swipeLayout;


        public Viewholder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            swipeLayout = (SwipeLayout) itemView.findViewById(R.id.swipe);
            trash = (ImageView) itemView.findViewById(R.id.trash);
        }
    }

}