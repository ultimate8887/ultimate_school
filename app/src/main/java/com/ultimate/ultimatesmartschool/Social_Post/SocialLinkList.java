package com.ultimate.ultimatesmartschool.Social_Post;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetSequence;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.squareup.picasso.Picasso;
import com.ultimate.ultimatesmartschool.BeanModule.User;
import com.ultimate.ultimatesmartschool.Login.LoginActivity;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Transport.FuelExpenseAdapter;
import com.ultimate.ultimatesmartschool.Transport.FuelExpenseList;
import com.ultimate.ultimatesmartschool.Transport.Fuelbean;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.CommonProgress;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SocialLinkList extends AppCompatActivity implements SocialPostAdapter.StudentPro {
    ArrayList<SocialPostBean> datalist = new ArrayList<>();
    private LinearLayoutManager layoutManager;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @BindView(R.id.parent)
    RelativeLayout parent;
    @BindView(R.id.imgBack)
    ImageView imgBack;
    @BindView(R.id.textNorecord)
    TextView txtNoData;
    @BindView(R.id.txtTitle)
    TextView txtTitle;

    SharedPreferences sharedPreferences;

    private RequestQueue queue;
    private String url;
    private SocialPostAdapter adapter1;
    int loaded = 0;
    Dialog mBottomSheetDialog;
    @BindView(R.id.totalRecord)
    TextView totalRecord;

    @BindView(R.id.today_date)
    TextView cal_text;

    String from_date = "";

    @BindView(R.id.export)
    FloatingActionButton export;
    CommonProgress commonProgress;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fuel_expense_list);
        ButterKnife.bind(this);
        txtTitle.setText("Social Post List");
        //txtSub.setText(Utility.setNaviHeaderClassData());
        // adepter set here
        commonProgress = new CommonProgress(this);
        layoutManager = new LinearLayoutManager(SocialLinkList.this);
        recyclerView.setLayoutManager(layoutManager);
        adapter1 = new SocialPostAdapter(datalist, SocialLinkList.this, this);
        recyclerView.setAdapter(adapter1);
        fetchNoticeBoard();

    }

    @OnClick(R.id.cal_lyttttt)
    public void cal_lyttttt() {

        Calendar cal = Calendar.getInstance(TimeZone.getDefault()); // Get current date
        DatePickerDialog datePicker = new DatePickerDialog(this,
                R.style.MyDatePickerDialogTheme, ondate,
                cal.get(Calendar.YEAR),
                cal.get(Calendar.MONTH),
                cal.get(Calendar.DAY_OF_MONTH));
        datePicker.getDatePicker();
        datePicker.setCancelable(false);
        datePicker.show();

    }

    DatePickerDialog.OnDateSetListener ondate = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
            calendar.set(year, monthOfYear, dayOfMonth);
            calendar.set(Calendar.HOUR_OF_DAY, 00);
            calendar.set(Calendar.MINUTE, 00);
            calendar.set(Calendar.SECOND, 00);
            Date setdate = calendar.getTime();
            SimpleDateFormat fmtOut = new SimpleDateFormat("dd MMM, yyyy");
            String dateString = fmtOut.format(setdate);
            cal_text.setText(dateString);
            SimpleDateFormat dateFrmOut = new SimpleDateFormat("yyyy-MM-dd");
            from_date = dateFrmOut.format(setdate);
            // Toast.makeText(HomeWorkByDate.this,from_date,Toast.LENGTH_SHORT).show();
            fetchNoticeBoard();
        }
    };


    private void fetchNoticeBoard() {

        commonProgress.show();

        HashMap<String, String> params = new HashMap<>();
        params.put("today", from_date);
        params.put("view_type", "post");
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.POST, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                ErpProgress.cancelProgressBar();
                commonProgress.dismiss();
                if (error == null) {

                    try {
                        JSONArray noticeArray = jsonObject.getJSONArray("hw_data");
                        datalist = SocialPostBean.parseNoticetArray(noticeArray);

                        if (datalist.size() > 0) {
                            adapter1.setHList(datalist);
                            adapter1.notifyDataSetChanged();
                            txtNoData.setVisibility(View.GONE);
                            export.setVisibility(View.GONE);
                            totalRecord.setVisibility(View.VISIBLE);
                            totalRecord.setText("Total Entries:- " + String.valueOf(datalist.size()));

                        } else {
                            export.setVisibility(View.GONE);
                            totalRecord.setText("Total Entries:- 0");
                            txtNoData.setVisibility(View.VISIBLE);
                            adapter1.setHList(datalist);
                            adapter1.notifyDataSetChanged();
                        }
                        //   main_progress.setVisibility(View.GONE);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    export.setVisibility(View.GONE);
                    totalRecord.setText("Total Entries:- 0");
                    txtNoData.setVisibility(View.VISIBLE);
                    datalist.clear();
                    adapter1.setHList(datalist);
                    adapter1.notifyDataSetChanged();
                }


            }
        }, this, params);

    }




    @OnClick(R.id.imgBack)
    public void imgBack() {
        // Animatoo.animateZoom(ViewNoticeInfo.this);
        finish();
    }


    private void commonCode(String img) {
        Dialog mBottomSheetDialog = new Dialog(this);
        final View sheetView = getLayoutInflater().inflate(R.layout.image_lyt, null);
        mBottomSheetDialog.setContentView(sheetView);
        ImageView imgShow = (ImageView) mBottomSheetDialog.findViewById(R.id.imgShow);
        ImageView imgDownload = (ImageView) mBottomSheetDialog.findViewById(R.id.imgDownload);
        imgDownload.setVisibility(View.GONE);
        Picasso.get().load(img).placeholder(getResources().getDrawable(R.color.transparent)).into(imgShow);
        mBottomSheetDialog.show();
        Window window = mBottomSheetDialog.getWindow();
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    }


    @Override
    public void onDelecallback(SocialPostBean homeworkbean) {
        commonProgress.show();
        HashMap<String, String> params = new HashMap<>();
        params.put("check", "post");
        params.put("h_id", homeworkbean.getId());
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.DELETEHW, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                ErpProgress.cancelProgressBar();
                commonProgress.dismiss();
                if (error == null) {
                    int pos = datalist.indexOf(homeworkbean);
                    datalist.remove(pos);
                    adapter1.setHList(datalist);
                    adapter1.notifyDataSetChanged();
                    Utils.showSnackBar("Deleted Successfully!", parent);
                    totalRecord.setText("Total Entries:- "+String.valueOf(datalist.size()));
                } else {
                    Log.e("error", error.getMessage() + "");
                    Utils.showSnackBar(error.getMessage(), parent);
                    if (error.getStatusCode() == 405) {
                        User.logout();
                        startActivity(new Intent(SocialLinkList.this, LoginActivity.class));
                        finish();
                    }
                }
            }
        }, this, params);
    }

}