package com.ultimate.ultimatesmartschool.Homework;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.daimajia.swipe.SimpleSwipeListener;
import com.daimajia.swipe.SwipeLayout;
import com.squareup.picasso.Picasso;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeworkadapterNew extends RecyclerView.Adapter<HomeworkadapterNew.Viewholder>{

    private final Mycallback mAdaptercall;
    ArrayList<Homeworkbean> hq_list;
    Animation animation;
    Context listner;

    public HomeworkadapterNew(ArrayList<Homeworkbean> hq_list, Context listner, Mycallback mAdaptercall){
        this.hq_list=hq_list;
        this.listner=listner;
        this.mAdaptercall=mAdaptercall;

    }

    @Override
    public Viewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.homeada_lay_new, parent, false);
        HomeworkadapterNew.Viewholder viewholder = new HomeworkadapterNew.Viewholder(view);
        return viewholder;
    }

    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }

    @Override
    public void onBindViewHolder(final HomeworkadapterNew.Viewholder holder, @SuppressLint("RecyclerView") final int position) {

        animation = AnimationUtils.loadAnimation(listner, R.anim.btn_blink_animation);
        if (hq_list.get(position).getSubject_name() != null){
            holder.sub.setText(hq_list.get(position).getSubject_name());
        }else{
            holder.sub.setText("Not Mentioned");
        }
        if (hq_list.get(position).getClass_name() != null) {
            String title = getColoredSpanned("Class-", "#000000");
            String Name="";
            if (hq_list.get(position).getSection_name() != null) {
                Name = getColoredSpanned(hq_list.get(position).getClass_name() + "(" + hq_list.get(position).getSection_name() + ")", "#5A5C59");
            }else {
                Name = getColoredSpanned(hq_list.get(position).getClass_name(), "#5A5C59");
            }
            holder.classess.setText(Html.fromHtml(title + " " + Name));
        }else {
            holder.classess.setVisibility(View.GONE);
        }

        if (hq_list.get(position).getId() != null) {
            String title = getColoredSpanned("ID:- ", "#000000");
            String Name = getColoredSpanned("hw-"+hq_list.get(position).getId(), "#5A5C59");
            holder.id.setText(Html.fromHtml(title + " " + Name));
        }

        if (hq_list.get(position).getStaff_name() != null) {
            String title = getColoredSpanned("Added by-", "#000000");
            String Name = getColoredSpanned(""+hq_list.get(position).getStaff_name()+"("+hq_list.get(position).getPost_name()+")", "#5A5C59");
            holder.addbyid.setText(Html.fromHtml(title + " " + Name));
        }

        holder.homeTopic.setText(hq_list.get(position).getComment());

       // holder.u_date.setText(Utils.getDateFormated(hq_list.get(position).getDate()));

        //for Today
        java.util.Calendar calendar = java.util.Calendar.getInstance(TimeZone.getDefault());
        Date setdate = calendar.getTime();
        SimpleDateFormat fmtOut = new SimpleDateFormat("dd MMM, yyyy");
        String dateString = fmtOut.format(setdate);
       //for Yesterday
        DateFormat dateFormat = new SimpleDateFormat("dd MMM, yyyy");
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);
        String dateString1= dateFormat.format(cal.getTime());

        String check= Utils.getDateFormated(hq_list.get(position).getDate());
        if (check.equalsIgnoreCase(dateString)){
            //  holder.lytLine.setVisibility(View.VISIBLE);
            // holder.txtDT.setText("Today");
            String title = getColoredSpanned("Today", "#e31e25");
            //String Name = getColoredSpanned(messagelist.get(position).getFrom_type(), "#7D7D7D");
            String l_Name = getColoredSpanned("", "#5A5C59");
            holder.u_date.setText(Html.fromHtml(title + " " + l_Name));


            // holder.txtDT.setText("Today at "+time);
            //  holder.vieww.setBackground(mContext.getResources().getDrawable(R.drawable.a_card_lyt2));
        }else if (check.equalsIgnoreCase(dateString1)){

            String title = getColoredSpanned("Yesterday", "#1C8B3B");
            //String Name = getColoredSpanned(messagelist.get(position).getFrom_type(), "#7D7D7D");
            String l_Name = getColoredSpanned("", "#5A5C59");
            holder.u_date.setText(Html.fromHtml(title + " " + l_Name));

            //  holder.lytLine.setVisibility(View.GONE);
            //  holder.vieww.setBackground(mContext.getResources().getDrawable(R.drawable.card_lyt2));
        }else {
            holder.u_date.setText(Utils.getDateFormated(hq_list.get(position).getDate()));
        }

        if (hq_list.get(position).getImage_tag().equalsIgnoreCase("images")){
            holder.nofile.setVisibility(View.VISIBLE);
            holder.image_file.setVisibility(View.VISIBLE);
            holder.audio_file.setVisibility(View.GONE);
            holder.pdf_file.setVisibility(View.GONE);
            holder.empty_file.setVisibility(View.GONE);
            Picasso.get().load(hq_list.get(position).getMulti_image().get(0)).placeholder(listner.getResources().getDrawable(R.color.transparent)).into(holder.image_file);
            holder.image_file.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    holder.image_file.startAnimation(animation);
                    if(mAdaptercall!= null){
                        mAdaptercall.onMethod_Image_callback(hq_list.get(position));
                    }
                }
            });
        }else if (hq_list.get(position).getRecordingsfile() != null){
            holder.nofile.setVisibility(View.VISIBLE);
            holder.image_file.setVisibility(View.GONE);
            holder.empty_file.setVisibility(View.GONE);
            holder.audio_file.setVisibility(View.VISIBLE);
            holder.pdf_file.setVisibility(View.GONE);

            holder.tap_view_audio.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    holder.tap_view_audio.startAnimation(animation);
                    if(mAdaptercall!= null){
                        mAdaptercall.onMethod_recording_callback(hq_list.get(position));
                    }
                }
            });
        } else if (hq_list.get(position).getPdf_file() != null){
            holder.nofile.setVisibility(View.VISIBLE);
            holder.image_file.setVisibility(View.GONE);
            holder.audio_file.setVisibility(View.GONE);
            holder.pdf_file.setVisibility(View.VISIBLE);
            holder.empty_file.setVisibility(View.GONE);
            holder.pdf_file.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    holder.tap_view_pdf.startAnimation(animation);
                    if(mAdaptercall!= null){
                        mAdaptercall.onMethod_pdf_call_back(hq_list.get(position));
                    }
                }
            });
        }else {
            holder.nofile.setVisibility(View.VISIBLE);
            holder.image_file.setVisibility(View.GONE);
            holder.audio_file.setVisibility(View.GONE);
            holder.pdf_file.setVisibility(View.GONE);
            holder.empty_file.setVisibility(View.VISIBLE);


        }

        holder.swipeLayout.setShowMode(SwipeLayout.ShowMode.PullOut);
        holder.swipeLayout.addSwipeListener(new SimpleSwipeListener() {
            @Override
            public void onOpen(SwipeLayout layout) {

            }

            @Override
            public void onStartClose(SwipeLayout layout) {
                holder.swipeLayout.close();
            }
        });

        // mAdaptercall.onMethodCallback();
        holder.update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAdaptercall.onUpdateCallback(hq_list.get(position));
            }
        });
        holder.trash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder1 = new AlertDialog.Builder(listner);
                builder1.setMessage("Do you want to delete? ");
                builder1.setCancelable(false);
                builder1.setPositiveButton(
                        "Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                                if (mAdaptercall != null) {
                                    mAdaptercall.onDelecallback(hq_list.get(position));
//                                    Toast.makeText(listner, "deleted", Toast.LENGTH_SHORT).show();
                                }
                                // mAdapterCallback.deleteNotification(notificationList.get(postion));
                            }
                        });
                builder1.setNegativeButton(
                        "No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        });
                AlertDialog alert11 = builder1.create();
                alert11.show();
            }
        });

    }

    @Override
    public int getItemCount() {
        return hq_list.size();
    }

    public void setHQList(ArrayList<Homeworkbean> hq_list) {

        this.hq_list = hq_list;
    }

    public class Viewholder extends RecyclerView.ViewHolder {
        public TextView u_date,homeTopic,sub;
        public ImageView image_file, audio_file, pdf_file;
        public RelativeLayout withfile;
        @BindView(R.id.nofile)
        RelativeLayout nofile;
        @BindView(R.id.d_lyt)
        RelativeLayout d_lyt;
        @BindView(R.id.empty_file)
        ImageView empty_file;
        @BindView(R.id.classess)
        TextView classess;
        @BindView(R.id.id)
        TextView id;
        SwipeLayout swipeLayout;
        public ImageView trash, update;
        @BindView(R.id.tap_view_pdf)
        TextView tap_view_pdf;
        @BindView(R.id.tap_view_pic)
        TextView tap_view_pic;
        @BindView(R.id.tap_view_audio)
        TextView tap_view_audio;
        @BindView(R.id.addbyid)
        TextView addbyid;


        public Viewholder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            sub = (TextView) itemView.findViewById(R.id.subject);
            u_date = (TextView) itemView.findViewById(R.id.u_date);
            homeTopic = (TextView) itemView.findViewById(R.id.homeTopic);
            image_file = (ImageView) itemView.findViewById(R.id.image_file);
            pdf_file = (ImageView) itemView.findViewById(R.id.pdf_file);
            audio_file = (ImageView) itemView.findViewById(R.id.audio_file);
            withfile = (RelativeLayout) itemView.findViewById(R.id.head);
            swipeLayout = (SwipeLayout) itemView.findViewById(R.id.swipe);
            trash = (ImageView) itemView.findViewById(R.id.trash);
            update = (ImageView) itemView.findViewById(R.id.update);
        }
    }
    public interface Mycallback{
        public void onMethod_Image_callback(Homeworkbean homeworkbean);
        public void onMethod_recording_callback(Homeworkbean homeworkbean);
        public void onMethod_pdf_call_back(Homeworkbean homeworkbean);
        public void onUpdateCallback(Homeworkbean homeworkbean);

        public void onDelecallback(Homeworkbean homeworkbean);
    }
}
