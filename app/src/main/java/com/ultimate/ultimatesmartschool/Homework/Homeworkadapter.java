package com.ultimate.ultimatesmartschool.Homework;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.daimajia.swipe.SimpleSwipeListener;
import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;
import com.squareup.picasso.Picasso;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import java.util.ArrayList;

public class Homeworkadapter extends RecyclerSwipeAdapter<Homeworkadapter.Viewholder> {
    ArrayList<Homeworkbean> hq_list;
    Mycallback mAdaptercall;
    Context listner;

    public Homeworkadapter(ArrayList<Homeworkbean> hq_list, Context listener, Mycallback mAdaptercall) {
        this.mAdaptercall = mAdaptercall;
        this.hq_list = hq_list;
        this.listner = listener;
    }


    @Override
    public Homeworkadapter.Viewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.homeada_lay, parent, false);
        Homeworkadapter.Viewholder viewholder = new Homeworkadapter.Viewholder(view);
        return viewholder;

    }

    @Override
    public void onBindViewHolder(final Homeworkadapter.Viewholder holder, final int position) {

        holder.parent.setAnimation(AnimationUtils.loadAnimation(listner,R.anim.fade_scale_animation));

        holder.t1.setText(hq_list.get(position).getComment());
        holder.t2.setText(Utils.getDateFormated(hq_list.get(position).getDate()));
        holder.subName.setText(hq_list.get(position).getSubject_name());
//        if (hq_list.get(position).getImage() != null) {
//            Picasso.with(listner).load(hq_list.get(position).getImage()).placeholder(listner.getResources().getDrawable(R.drawable.home)).into(holder.iv2);
//        } else {
//            Picasso.with(listner).load(String.valueOf(listner.getResources().getDrawable(R.drawable.home))).into(holder.iv2);
//        }

        mItemManger.bindView(holder.itemView, position);

        holder.swipeLayout.setShowMode(SwipeLayout.ShowMode.PullOut);
        holder.swipeLayout.addSwipeListener(new SimpleSwipeListener() {
            @Override
            public void onOpen(SwipeLayout layout) {

            }

            @Override
            public void onStartClose(SwipeLayout layout) {
                holder.swipeLayout.close();
            }
        });

        if (hq_list.get(position).getImage() != null) {
            holder.iv2.setVisibility(View.VISIBLE);
            holder.iv2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(mAdaptercall!= null){
                        mAdaptercall.onMethodCallback(hq_list.get(position));
                    }
                }
            });
        }else{

            holder.iv2.setVisibility(View.GONE);
        }


        if (hq_list.get(position).getPdf_file() != null) {
            holder.txtpdf.setVisibility(View.VISIBLE);

            holder.txtpdf.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(mAdaptercall!= null){
                        mAdaptercall.onpdfCallback(hq_list.get(position));
                    }
                }
            });
        }else{

            holder.txtpdf.setVisibility(View.GONE);
        }

        if (hq_list.get(position).getRecordingsfile() != null) {
            holder.txtrecord.setVisibility(View.VISIBLE);

            holder.txtrecord.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(mAdaptercall!= null){
                        mAdaptercall.onrecordfileCallback(hq_list.get(position));
                    }
                }
            });
        }else{

            holder.txtrecord.setVisibility(View.GONE);

        }
        // mAdaptercall.onMethodCallback();
        holder.update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAdaptercall.onUpdateCallback(hq_list.get(position));
            }
        });
        holder.trash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder1 = new AlertDialog.Builder(listner);
                builder1.setMessage("Do you want to delete? ");
                builder1.setCancelable(false);
                builder1.setPositiveButton(
                        "Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                                if (mAdaptercall != null) {
                                    mAdaptercall.onDelecallback(hq_list.get(position));
                                    Toast.makeText(listner, "deleted", Toast.LENGTH_SHORT).show();
                                }
                                // mAdapterCallback.deleteNotification(notificationList.get(postion));
                            }
                        });
                builder1.setNegativeButton(
                        "No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        });
                AlertDialog alert11 = builder1.create();
                alert11.show();
            }
        });
    }

    public interface Mycallback {
        public void onMethodCallback(Homeworkbean homeworkbean);
        public void onpdfCallback(Homeworkbean homeworkbean);
        public void   onrecordfileCallback(Homeworkbean homeworkbean);
        public void onUpdateCallback(Homeworkbean homeworkbean);

        public void onDelecallback(Homeworkbean homeworkbean);
    }

    @Override
    public int getItemCount() {
        return hq_list.size();
    }

    public void setHQList(ArrayList<Homeworkbean> hq_list) {
        this.hq_list = hq_list;
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe;
    }


    public class Viewholder extends RecyclerView.ViewHolder {
        public TextView t1;
        public TextView t2;
        public TextView iv2,txtpdf,txtrecord,subName;
        public ImageView  trash, update;
        SwipeLayout swipeLayout;
        CardView  parent;
         View view1;
        public Viewholder(View itemView) {
            super(itemView);
            parent =(CardView)itemView.findViewById(R.id.card1);
            swipeLayout = (SwipeLayout) itemView.findViewById(R.id.swipe);
            t1 = (TextView) itemView.findViewById(R.id.textcomment);
            t2 = (TextView) itemView.findViewById(R.id.textdate);
            subName= (TextView) itemView.findViewById(R.id.subName);
            iv2 = (TextView) itemView.findViewById(R.id.txtimage);
            txtpdf = (TextView) itemView.findViewById(R.id.txtpdf);
            txtrecord = (TextView) itemView.findViewById(R.id.txtrecord);
            trash = (ImageView) itemView.findViewById(R.id.trash);
            update = (ImageView) itemView.findViewById(R.id.update);
            view1=(View)itemView.findViewById(R.id.view1);
        }
    }
}
