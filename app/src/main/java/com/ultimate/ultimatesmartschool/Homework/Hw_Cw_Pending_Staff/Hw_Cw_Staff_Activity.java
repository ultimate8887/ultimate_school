package com.ultimate.ultimatesmartschool.Homework.Hw_Cw_Pending_Staff;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetSequence;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.ultimate.ultimatesmartschool.BeanModule.CommonBean;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Staff.View_staff_bean;
import com.ultimate.ultimatesmartschool.StaffAttendance.DepartmentAdapternew;
import com.ultimate.ultimatesmartschool.StaffAttendance.DepartmentReportAdapter;
import com.ultimate.ultimatesmartschool.StaffAttendance.StaffAttendBean;
import com.ultimate.ultimatesmartschool.StaffAttendance.StaffDayReort;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.CommonProgress;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class Hw_Cw_Staff_Activity extends AppCompatActivity {
    @BindView(R.id.export)
    FloatingActionButton export;
    int selected = 1;
    SharedPreferences sharedPreferences;
    // ArrayList<ClassBean> classList = new ArrayList<>();
    @BindView(R.id.parent)
    RelativeLayout parent;
    //@BindView(R.id.spinner) Spinner spinnerClass;
    BottomSheetDialog mBottomSheetDialog;
    private int loaded = 0;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.txtSub)
    TextView txtSub;
    List<String> classidsel;
    @BindView(R.id.totalRecord)
    TextView totalRecord;



    @BindView(R.id.adddate)TextView adddate;
    public String date;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.textNorecord)
    TextView txtNorecord;
    private ArrayList<View_staff_bean> dayList;
    private Hw_Cw_Staff_Adapter adapter;

    @BindView(R.id.statuss)
    TextView statuss;

    @BindView(R.id.dialog)
    ImageView dialog;
    @BindView(R.id.sc)
    CardView scrollView;
    @BindView(R.id.filelayout)
    LinearLayout filelayout;

    String check="";

    CommonProgress commonProgress;


    Spinner spinnerDepartment;

    int abc;

    private ArrayList<CommonBean> departmentList;
    private String departmentid="",departmentname="";


    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_staff_day_reort);
        ButterKnife.bind(this);
        commonProgress=new CommonProgress(this);
        dialog.setVisibility(View.GONE);
        if (getIntent().getExtras() != null) {
            check = getIntent().getExtras().getString("check");
            if(check.equalsIgnoreCase("hw")){
                txtTitle.setText("Pending Homework Staff List");
                statuss.setText("Homework");
            }else{
                txtTitle.setText("Pending Classwork Staff List");
                statuss.setText("Classwork");
            }
            departmentList = new ArrayList<>();
            dayList = new ArrayList<>();
            recyclerView.setLayoutManager(new LinearLayoutManager(this));
            adapter = new Hw_Cw_Staff_Adapter(this, dayList);
            recyclerView.setAdapter(adapter);
            filelayout.setVisibility(View.VISIBLE);
            getTodayDate();
        }


    }


    public void getTodayDate() {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.HOUR_OF_DAY, 00);
        c.set(Calendar.MINUTE, 00);
        c.set(Calendar.SECOND, 00);
        java.util.Date setdate = c.getTime();
        SimpleDateFormat fmtOut = new SimpleDateFormat("dd MMM, yyyy");
        String dateString = fmtOut.format(setdate);
        adddate.setText(dateString);
        SimpleDateFormat dateFrmOut = new SimpleDateFormat("yyyy-MM-dd");
        date = dateFrmOut.format(setdate);
        fetchStudentList(date);
    }

    private void setCommonData() {
        filelayout.setVisibility(View.VISIBLE);
        txtTitle.setText(departmentname);
    }


    @OnClick(R.id.imgBack)
    public void onBackclick() {
        finish();
    }

    private void fetchStudentList(String date) {
        commonProgress.show();
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("mdate", date);
        params.put("check", check);
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.HW_CW_STAFF, studentapiCallback, this, params);
    }

    ApiHandler.ApiCallback studentapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            commonProgress.dismiss();
            if (error == null) {
                try {
                    // if(selected==1){
                   // ArrayList<View_staff_bean> attendList  = View_staff_bean.parseviewstaffArray(jsonObject.getJSONArray(Constants.ATTEND_DATA));
                    JSONArray jsonArray = jsonObject.getJSONArray("staff_detail");
                    ArrayList<View_staff_bean> attendList = View_staff_bean.parseviewstaffArray(jsonArray);
                    dayList.clear();
                    dayList.addAll(attendList);
                    adapter.notifyDataSetChanged();
                    totalRecord.setText("Total Entries:- "+String.valueOf(attendList.size()));
                    scrollView.setVisibility(View.VISIBLE);
                    // }
                    export.setVisibility(View.VISIBLE);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                export.setVisibility(View.GONE);
                scrollView.setVisibility(View.GONE);
                totalRecord.setText("Total Entries:- 0");
                //Utils.showSnackBar(error.getMessage(), view.findViewById(R.id.parent));
                Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_LONG).show();
            }
        }
    };

    @SuppressLint("RestrictedApi")
    @OnClick(R.id.export)
    public void submit() {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.S) {
            importData();
        } else {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {

                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    Toast.makeText(this, "Please provide storage permission from app settings", Toast.LENGTH_LONG).show();
                } else {

                    ActivityCompat.requestPermissions(this,
                            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            1);
                }
            } else {
                importData();

            }
        }
    }

    @SuppressLint("RestrictedApi")
    private void importData() {
        // File filePath = new File(Environment.getExternalStorageDirectory() + "/Demo.xls");
        Workbook wb = new HSSFWorkbook();

        Cell cell = null;

        String ndate="",sub_staff="",sub_date="",sub_title="";
        ndate= Utils.getDateOnlyNEW(date)+","+Utils.getMonthFormated(date);
        sub_date=ndate.substring(0,3);

        if(check.equalsIgnoreCase("hw")){
            sub_staff="homework";
            sub_title="Pending Homework Staff List";
        }else{
            sub_staff="classwork";
            sub_title="Pending Classwork Staff List";
        }

        Sheet sheet = null;
        sheet = wb.createSheet(adddate.getText().toString()+" "+sub_title);
        //Now column and row
        Row row = sheet.createRow(0);

        cell = row.createCell(0);
        cell.setCellValue("S No");

        cell = row.createCell(1);
        cell.setCellValue("Staff Id");

        cell = row.createCell(2);
        cell.setCellValue("Department/Post");

        cell = row.createCell(3);
        cell.setCellValue("Staff Name");

        cell = row.createCell(4);
        cell.setCellValue("Father Name");

        cell = row.createCell(5);
        cell.setCellValue("Attendance");

        cell = row.createCell(6);
        cell.setCellValue("Date");


        //column width
        sheet.setColumnWidth(0, (20 * 100));
        sheet.setColumnWidth(1, (20 * 150));
        sheet.setColumnWidth(2, (30 * 250));
        sheet.setColumnWidth(3, (30 * 250));
        sheet.setColumnWidth(4, (30 * 200));
        sheet.setColumnWidth(5, (20 * 200));
        sheet.setColumnWidth(6, (30 * 200));

        for (int i = 0; i < dayList.size(); i++) {

            Row row1 = sheet.createRow(i + 1);

            cell = row1.createCell(0);
            cell.setCellValue(i+1);

            cell = row1.createCell(1);
            cell.setCellValue(dayList.get(i).getId());

            cell = row1.createCell(2);
            cell.setCellValue(dayList.get(i).getTeach_nonteach()+" ("+dayList.get(i).getDesignation()+")");

            cell = row1.createCell(3);
            cell.setCellValue((dayList.get(i).getName()));
            //  cell.setCellStyle(cellStyle);

            cell = row1.createCell(4);
            cell.setCellValue(dayList.get(i).getFathername());


            String attend="N/A";

            cell = row1.createCell(5);
            cell.setCellValue(attend);

            cell = row1.createCell(6);
            cell.setCellValue(ndate);

            sheet.setColumnWidth(0, (20 * 100));
            sheet.setColumnWidth(1, (20 * 150));
            sheet.setColumnWidth(2, (30 * 250));
            sheet.setColumnWidth(3, (30 * 250));
            sheet.setColumnWidth(4, (30 * 200));
            sheet.setColumnWidth(5, (20 * 200));
            sheet.setColumnWidth(6, (30 * 200));

        }
        String fileName;
        if (sub_staff.equalsIgnoreCase("")){
            fileName = sub_date+"_" +System.currentTimeMillis() + ".xls";
        }else {
            fileName = sub_staff+"_"+Utils.getDateOnlyNEW(date)+"_"+Utils.getMonthFormated(date)+"_" + System.currentTimeMillis() + ".xls";
        }

        // File file = new File(getActivity().getExternalFilesDir(null), fileName);

        File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS), fileName);

        FileOutputStream fileOutputStream = null;

        try {
            fileOutputStream = new FileOutputStream(file);
            wb.write(fileOutputStream);
            Utils.openSuccessDialog("Success!","Export file in "+file,"",this);
        } catch (IOException e) {
            Utils.openErrorDialog("Error writing Exception: "+ e,this);
        } catch (Exception e) {
            Utils.openErrorDialog("Failed to save file due to Exception: "+ e,this);
        } finally {
            try {
                if (null != fileOutputStream) {
                    fileOutputStream.close();
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

    }

    @SuppressLint("RestrictedApi")
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == 1 && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            importData();
        } else {
            Toast.makeText(getApplicationContext(), "Permission Denied", Toast.LENGTH_SHORT).show();
        }
    }



    @OnClick(R.id.filelayout)
    public void fetchDate() {
        java.util.Calendar cal = java.util.Calendar.getInstance(TimeZone.getDefault()); // Get current date
        DatePickerDialog datePicker = new DatePickerDialog(this,
                R.style.MyDatePickerDialogTheme, ondate,
                cal.get(java.util.Calendar.YEAR),
                cal.get(java.util.Calendar.MONTH),
                cal.get(java.util.Calendar.DAY_OF_MONTH));
        datePicker.getDatePicker().setMaxDate(new java.util.Date().getTime());
        datePicker.setCancelable(false);
        datePicker.show();
    }

    DatePickerDialog.OnDateSetListener ondate = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            java.util.Calendar calendar = java.util.Calendar.getInstance(TimeZone.getDefault());
            calendar.set(year, monthOfYear, dayOfMonth);
            calendar.set(Calendar.HOUR_OF_DAY, 00);
            calendar.set(Calendar.MINUTE, 00);
            calendar.set(Calendar.SECOND, 00);
            java.util.Date setdate = calendar.getTime();
            SimpleDateFormat fmtOut = new SimpleDateFormat("dd MMM, yyyy");
            String dateString = fmtOut.format(setdate);
            adddate.setText(dateString);
            SimpleDateFormat dateFrmOut = new SimpleDateFormat("yyyy-MM-dd");
            date = dateFrmOut.format(setdate);
            fetchStudentList(date);
        }
    };



}