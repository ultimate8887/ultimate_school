package com.ultimate.ultimatesmartschool.Homework.Hw_Cw_Pending_Staff;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Staff.View_staff_bean;
import com.ultimate.ultimatesmartschool.StaffAttendance.DepartmentReportAdapter;
import com.ultimate.ultimatesmartschool.StaffAttendance.StaffAttendBean;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Hw_Cw_Staff_Adapter extends RecyclerView.Adapter<Hw_Cw_Staff_Adapter.MyViewHolder> {

    private Context mContext;
    ArrayList<View_staff_bean> dataList;

    Hw_Cw_Staff_Adapter(Context mContext, ArrayList<View_staff_bean> dataList) {
        this.mContext = mContext;
        this.dataList = dataList;
    }


    @Override
    public Hw_Cw_Staff_Adapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.depart_day_report, parent, false);
        Hw_Cw_Staff_Adapter.MyViewHolder vh = new Hw_Cw_Staff_Adapter.MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(Hw_Cw_Staff_Adapter.MyViewHolder holder, final int position) {
        if (position == 0) {
            holder.lytHeader.setVisibility(View.GONE);
            holder.lytData.setVisibility(View.GONE);
        } else {
            holder.lytHeader.setVisibility(View.INVISIBLE);
            holder.lytData.setVisibility(View.VISIBLE);
            final View_staff_bean data = dataList.get(position - 1);
            holder.txtName.setText(data.getName());
            holder.txtRoll.setText(data.getId());
            holder.txtsno.setText(String.valueOf(position));
            String attend = "N/A";
            holder.txtAttend.setBackground(mContext.getResources().getDrawable(R.drawable.white_bg));
            holder.txtAttend.setText(attend);
        }

    }

    @Override
    public int getItemCount() {
        return (dataList.size() + 1);
    }

    public ArrayList<View_staff_bean> getAttendList() {
        return dataList;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txtAttend)
        TextView txtAttend;
        @BindView(R.id.txtName)
        TextView txtName;
        @BindView(R.id.txtRoll)
        TextView txtRoll;
        @BindView(R.id.lytData)
        LinearLayout lytData;
        @BindView(R.id.lytHeader)
        LinearLayout lytHeader;
        @BindView(R.id.txtsno)
        TextView txtsno;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
