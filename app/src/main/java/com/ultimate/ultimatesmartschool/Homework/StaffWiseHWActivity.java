package com.ultimate.ultimatesmartschool.Homework;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.cuneytayyildiz.gestureimageview.GestureImageView;
import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetSequence;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.squareup.picasso.Picasso;
import com.ultimate.ultimatesmartschool.BeanModule.ClassBean;
import com.ultimate.ultimatesmartschool.BeanModule.SectionBean;
import com.ultimate.ultimatesmartschool.BeanModule.SubjectBeancls;
import com.ultimate.ultimatesmartschool.BeanModule.User;
import com.ultimate.ultimatesmartschool.ClassWork.CWViewPagerAdapter;
import com.ultimate.ultimatesmartschool.ClassWork.ViewClassWork;
import com.ultimate.ultimatesmartschool.Ebook.WebViewActivity;
import com.ultimate.ultimatesmartschool.Examination.ClassAdapter;
import com.ultimate.ultimatesmartschool.Login.LoginActivity;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Staff.View_staff_bean;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.CommonProgress;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;
import com.ultimate.ultimatesmartschool.Utility.PaginationListener;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.relex.circleindicator.CircleIndicator;

public class StaffWiseHWActivity extends AppCompatActivity implements HomeworkadapterNew.Mycallback {


    public ViewPager intro_images;
    LinearLayout pager_indicator;
    public CircleIndicator indicator;
    public TextView textView;

    ArrayList<View_staff_bean> classList = new ArrayList<>();
    @BindView(R.id.parent)
    RelativeLayout parent;
    //@BindView(R.id.spinner) Spinner spinnerClass;
    @BindView(R.id.spinner)
    Spinner spinner;
    @BindView(R.id.spinner1)
    Spinner spinnerClass;
    String image_url="",school="",className = "";
    String classid = "";
    private HomeworkadapterNew adapter;
    ArrayList<Homeworkbean> hwList = new ArrayList<>();
    private LinearLayoutManager layoutManager;
    CommonProgress commonProgress;
    private int loaded = 0;
    int check=0;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.totalRecord)
    TextView totalRecord;
    List<String> classidsel;
    //    @BindView(R.id.textView7)
//    TextView textView7;
    Spinner spinnersection;
    ArrayList<SectionBean> sectionList = new ArrayList<>();
    String sectionid="";
    String sectionname;
    @BindView(R.id.recyclerViewmsg)
    RecyclerView recyclerview;
    @BindView(R.id.textNorecord)
    TextView txtNorecord;
    ArrayList<SubjectBeancls> subjectList = new ArrayList<>();
    AdapterOfSubjectListN adaptersub;
    String sub_id, sub_name;

    Dialog mBottomSheetDialog;
    @BindView(R.id.adddate)TextView adddate;
    String type="1",name="",date="",view_type="staff",filter_id="",tdate="",ydate="";
    StaffListAdapter staffListAdapter;

    @BindView(R.id.dialog)
    ImageView dialog;
    SharedPreferences sharedPreferences;


    @BindView(R.id.main_progress)
    ProgressBar main_progress;
    int  limit=100,page_limit=0,total_pages=0;
    private static final int PAGE_START = 1;
    private int currentPage = PAGE_START;
    private boolean isLastPage = false;
    private boolean isLoading = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_g_e_t_homework);
        ButterKnife.bind(this);
        commonProgress=new CommonProgress(this);
        classidsel = new ArrayList<>();
        dialog.setVisibility(View.GONE);
        spinner.setVisibility(View.GONE);
        spinnerClass.setVisibility(View.VISIBLE);
        recyclerview.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(StaffWiseHWActivity.this);
        recyclerview.setLayoutManager(layoutManager);
        adapter = new HomeworkadapterNew(hwList, StaffWiseHWActivity.this, this);
        recyclerview.setAdapter(adapter);
        fetchClass(limit);
        getTodayDate();

        txtTitle.setText("Home-Work");

        recyclerview.addOnScrollListener(new PaginationListener(layoutManager) {
            @Override
            protected void loadMoreItems() {
                // isLoading = true;
                //  Utils.showSnackBar("No more messages found in message list.", parent);
                //  currentPage++;
                // doApiCall();

                if (total_pages>=page_limit) {
                    main_progress.setVisibility(View.VISIBLE);
                    currentPage += 10;
                    page_limit = currentPage + limit;

                    if (sub_id.equalsIgnoreCase("")){
                        fetchHomework(date,0,sub_id, page_limit, "no");
                    }else{
                        fetchHomework(date,5,sub_id, page_limit, "no");
                    }
                }else{
                    // main_progress.setVisibility(View.GONE);
                    //  Utils.showSnackBar("No more messages found in message list.", parent);
                }


            }
            @Override
            public boolean isLastPage() {
                return isLastPage;
            }
            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });

        // fetchSection();
        //  txtTitle.setText("Homework");
        school= User.getCurrentUser().getSchoolData().getName();
    }

    public void getTodayDate() {
        java.util.Calendar c = java.util.Calendar.getInstance();
        c.set(java.util.Calendar.HOUR_OF_DAY, 00);
        c.set(java.util.Calendar.MINUTE, 00);
        c.set(java.util.Calendar.SECOND, 00);
        Date setdate = c.getTime();
        java.text.SimpleDateFormat fmtOut = new java.text.SimpleDateFormat("dd MMM, yyyy");
        String dateString = fmtOut.format(setdate);
        //  adddate.setText(dateString);
        java.text.SimpleDateFormat dateFrmOut = new java.text.SimpleDateFormat("yyyy-MM-dd");
        tdate = dateFrmOut.format(setdate);
        //for Yesterday
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);
        ydate= dateFormat.format(cal.getTime());
    }

    @OnClick(R.id.filelayout)
    public void fetchDate() {
        java.util.Calendar cal = java.util.Calendar.getInstance(TimeZone.getDefault()); // Get current date
        DatePickerDialog datePicker = new DatePickerDialog(StaffWiseHWActivity.this,
                R.style.MyDatePickerDialogTheme, ondate,
                cal.get(java.util.Calendar.YEAR),
                cal.get(java.util.Calendar.MONTH),
                cal.get(java.util.Calendar.DAY_OF_MONTH));
        datePicker.getDatePicker().setMaxDate(new java.util.Date().getTime());
        datePicker.setCancelable(false);
        datePicker.show();
    }

    DatePickerDialog.OnDateSetListener ondate = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            java.util.Calendar calendar = java.util.Calendar.getInstance(TimeZone.getDefault());
            calendar.set(year, monthOfYear, dayOfMonth);
            calendar.set(Calendar.HOUR_OF_DAY, 00);
            calendar.set(Calendar.MINUTE, 00);
            calendar.set(Calendar.SECOND, 00);
            java.util.Date setdate = calendar.getTime();
            SimpleDateFormat fmtOut = new SimpleDateFormat("dd MMM, yyyy");
            String dateString = fmtOut.format(setdate);
            //adddate.setText(dateString);
            SimpleDateFormat dateFrmOut = new SimpleDateFormat("yyyy-MM-dd");
            date = dateFrmOut.format(setdate);
            if (tdate.equalsIgnoreCase(date)){
                adddate.setText("Today");
                adddate.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.light_red));
            } else if (ydate.equalsIgnoreCase(date)){
                adddate.setText("Yesterday");
                adddate.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.green));
            }else {
                adddate.setText(dateString);
                adddate.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
            }
            page_limit=0;
            fetchClass(limit);
        }
    };

    private void fetchClass(int limit) {

        HashMap<String, String> params = new HashMap<String, String>();
        params.put("check", "list");
        params.put("d_id", "");
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.STAFF_DETAIL, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                if (error == null) {
                    try {
                        JSONArray jsonArray = jsonObject.getJSONArray("staff_detail");
                        classList = View_staff_bean.parseviewstaffArray(jsonArray);
                            staffListAdapter = new StaffListAdapter(StaffWiseHWActivity.this, classList);
                            spinnerClass.setAdapter(staffListAdapter);
                            spinnerClass.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                    sub_id="";
                                    if (i > 0) {
                                        sub_id = classList.get(i - 1).getId();
                                        fetchHomework(date,5,sub_id, limit, "yes");
                                    } else {
                                        fetchHomework(date,0,sub_id, limit, "yes");
                                    }
                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> adapterView) {

                                }
                            });

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
//                Utils.showSnackBar(error.getMessage(), parent);
                }
            }
        }, this, params);
    }


    public void fetchHomework(String date, int i,String sub_id,int limit,String progress) {
        if (progress.equalsIgnoreCase("yes")){
            commonProgress.show();
        }
        HashMap<String, String> params = new HashMap<String, String>();
        //params.put("class_id", classid);

        if (i == 0){
            params.put("class_id", classid);
            params.put("section_id", sectionid);
        } else {
            params.put("class_id", classid);
            params.put("section_id", sectionid);
            params.put("sub_id", sub_id);
        }
        params.put("today",date);
        params.put("view_type",view_type);
        params.put("page_limit",String.valueOf(limit));
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.gdshomeworkurl, apiCallback, this, params);
    }

    ApiHandler.ApiCallback apiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            commonProgress.dismiss();
            if (error == null) {
                try {
                    if (hwList != null) {
                        hwList.clear();
                    }
                    JSONArray jsonArray = jsonObject.getJSONArray("hw_data");
                    hwList = Homeworkbean.parseHWArray(jsonArray);
                    total_pages= Integer.parseInt(hwList.get(0).getRowcount());
                    if (hwList.size() > 0) {
                        adapter.setHQList(hwList);
                        adapter.notifyDataSetChanged();
                        txtNorecord.setVisibility(View.GONE);
                        totalRecord.setText("Total Entries:- "+String.valueOf(hwList.size()));
                    } else {
                        totalRecord.setText("Total Entries:- 0");
                        adapter.setHQList(hwList);
                        adapter.notifyDataSetChanged();
                        txtNorecord.setVisibility(View.VISIBLE);
                    }
                    main_progress.setVisibility(View.GONE);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                totalRecord.setText("Total Entries:- 0");
                txtNorecord.setVisibility(View.VISIBLE);
                hwList.clear();
                adapter.setHQList(hwList);
                adapter.notifyDataSetChanged();
            }
        }
    };

    @Override
    public void onMethod_Image_callback(Homeworkbean data) {
        final Dialog warningDialog = new Dialog(this);
        warningDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        warningDialog.setCancelable(true);

        warningDialog.setContentView(R.layout.class_img_dialog);
        warningDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        RelativeLayout btnClose = (RelativeLayout) warningDialog.findViewById(R.id.btnNo);

        indicator = (CircleIndicator) warningDialog.findViewById(R.id.indicator);
        textView=(TextView) warningDialog.findViewById(R.id.clsswrktexttt);
        intro_images=(ViewPager) warningDialog.findViewById(R.id.pager_introduction);

        TextView tap_count=(TextView) warningDialog.findViewById(R.id.tap_count);
        String title1 = getColoredSpanned("<b>"+"1"+"</b>"+"", "#F4212C");
        String l_Name1 = getColoredSpanned(" / "+String.valueOf(data.getMulti_image().size()), "#000000");
        tap_count.setText(Html.fromHtml(title1 + " " + l_Name1+ " "));

        textView.setText(data.getComment());
        CWViewPagerAdapter mAdapter = new  CWViewPagerAdapter(this,data.getMulti_image(),data.getComment());
        intro_images.setAdapter(mAdapter);
        intro_images.setCurrentItem(0);
        indicator.setViewPager(intro_images);

        intro_images.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int arg0) {
                // TODO Auto-generated method stub
                int c=arg0+1;
                String title1 = getColoredSpanned("<b>"+String.valueOf(c)+"</b>"+"", "#F4212C");
                String l_Name1 = getColoredSpanned(" / "+String.valueOf(data.getMulti_image().size()), "#000000");
                tap_count.setText(Html.fromHtml(title1 + " " + l_Name1+ " "));
            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
                // TODO Auto-generated method stub

                System.out.println("onPageScrolled");
            }

            @Override
            public void onPageScrollStateChanged(int num) {
                // TODO Auto-generated method stub

            }
        });


        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                warningDialog.dismiss();
            }
        });


        warningDialog.show();
        Window window = warningDialog.getWindow();
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    }

    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }

    @Override
    public void onMethod_recording_callback(Homeworkbean homeworkbean) {
        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(homeworkbean.getRecordingsfile())));
    }

    @Override
    public void onMethod_pdf_call_back(Homeworkbean homeworkbean) {
        Intent intent = new Intent(StaffWiseHWActivity.this, WebViewActivity.class);
        intent.putExtra("MY_kEY",homeworkbean.getPdf_file());
        startActivity(intent);
    }

    @Override
    public void onUpdateCallback(Homeworkbean homeworkbean) {

    }

    @Override
    public void onDelecallback(Homeworkbean homeworkbean) {
        commonProgress.show();
        HashMap<String, String> params = new HashMap<>();
        params.put("h_id", homeworkbean.getId());
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.DELETEHW, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                ErpProgress.cancelProgressBar();
                commonProgress.dismiss();
                if (error == null) {
                    int pos = hwList.indexOf(homeworkbean);
                    hwList.remove(pos);
                    adapter.setHQList(hwList);
                    adapter.notifyDataSetChanged();
                    Utils.showSnackBar("Deleted Successfully!", parent);
                    totalRecord.setText("Total Entries:- "+String.valueOf(hwList.size()));
                } else {
                    Log.e("error", error.getMessage() + "");
                    Utils.showSnackBar(error.getMessage(), parent);

                    if (error.getStatusCode() == 405) {
                        User.logout();
                        startActivity(new Intent(StaffWiseHWActivity.this, LoginActivity.class));
                        finish();
                    }
                }
            }
        }, this, params);
    }

    @Override
    protected void onResume() {
        super.onResume();
//        if (classidsel != null && !classidsel.isEmpty())
//            fetchHomework();
    }



    @OnClick(R.id.imgBack)
    public void onback() {
        finish();
    }
}