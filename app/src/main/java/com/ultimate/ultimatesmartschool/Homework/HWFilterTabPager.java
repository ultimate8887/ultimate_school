package com.ultimate.ultimatesmartschool.Homework;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.ultimate.ultimatesmartschool.Homework.Fragments.StaffHW_DateWise;
import com.ultimate.ultimatesmartschool.Homework.Fragments.StaffHW_MonthWise;
import com.ultimate.ultimatesmartschool.MSG_NEW.Fragments.Stafftostu_ClassWise;
import com.ultimate.ultimatesmartschool.MSG_NEW.Fragments.Stafftostu_StaffWise;

public class HWFilterTabPager extends FragmentStatePagerAdapter {

    public HWFilterTabPager(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new StaffHW_DateWise();
            case 1:
                return new StaffHW_MonthWise();
            default:
                break;
        }
        return null;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        // Generate title based on item position
        switch (position) {
            case 0:
                return "View Date Wise";
            case 1:
                return "View Month Wise";
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return 2;
    }
}
