package com.ultimate.ultimatesmartschool.Homework.Fragments;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetSequence;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.ultimate.ultimatesmartschool.BeanModule.SectionBean;
import com.ultimate.ultimatesmartschool.BeanModule.SubjectBeancls;
import com.ultimate.ultimatesmartschool.BeanModule.User;
import com.ultimate.ultimatesmartschool.Homework.AdapterOfSubjectListN;
import com.ultimate.ultimatesmartschool.Homework.HW_ReportAdapter;
import com.ultimate.ultimatesmartschool.Homework.HomeworkadapterNew;
import com.ultimate.ultimatesmartschool.Homework.Homeworkbean;
import com.ultimate.ultimatesmartschool.Homework.StaffListAdapter;
import com.ultimate.ultimatesmartschool.Message.Message_Bean_stafftostaff;
import com.ultimate.ultimatesmartschool.Message.StafftostuMsg_Adapter;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Staff.View_staff_bean;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.CommonProgress;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;
import com.ultimate.ultimatesmartschool.Utility.PaginationListener;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.content.Context.MODE_PRIVATE;


public class StaffHW_DateWise extends Fragment {

    ArrayList<View_staff_bean> classList = new ArrayList<>();
    @BindView(R.id.parent)
    RelativeLayout parent;
    //@BindView(R.id.spinner) Spinner spinnerClass;
    @BindView(R.id.spinner)
    Spinner spinner;
    @BindView(R.id.spinner1)
    Spinner spinnerClass;
    String image_url="",school="",className = "";
    String classid = "";
    private HW_ReportAdapter adapter;
    ArrayList<Homeworkbean> hwList = new ArrayList<>();
    private LinearLayoutManager layoutManager;
    CommonProgress commonProgress;
    private int loaded = 0;
    int check=0;

    @BindView(R.id.export)
    FloatingActionButton export;

    @BindView(R.id.lytHeader)
    LinearLayout lytHeader;
    @BindView(R.id.m_d)
    TextView m_d;

    @BindView(R.id.totalRecord)
    TextView totalRecord;
    List<String> classidsel;
    //    @BindView(R.id.textView7)
//    TextView textView7;
    Spinner spinnersection;
    ArrayList<SectionBean> sectionList = new ArrayList<>();
    String sectionid="";
    String sectionname;
    @BindView(R.id.recyclerViewmsg)
    RecyclerView recyclerview;
    @BindView(R.id.textNorecord)
    TextView txtNorecord;
    ArrayList<SubjectBeancls> subjectList = new ArrayList<>();
    AdapterOfSubjectListN adaptersub;
    String sub_id, staff_name="";

    Dialog mBottomSheetDialog;
    @BindView(R.id.adddate)TextView adddate;
    String type="1",name="",date="",view_type="date_report",filter_id="",tdate="",ydate="";
    StaffListAdapter staffListAdapter;

    SharedPreferences sharedPreferences;

    @BindView(R.id.main_progress)
    ProgressBar main_progress;
    int  limit=100,page_limit=0,total_pages=0;
    private static final int PAGE_START = 1;
    private int currentPage = PAGE_START;
    private boolean isLastPage = false;
    private boolean isLoading = false;

    public StaffHW_DateWise() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_send__common, container, false);
        ButterKnife.bind(this,view);
        commonProgress=new CommonProgress(getActivity());
        classidsel = new ArrayList<>();
        spinner.setVisibility(View.GONE);
        spinnerClass.setVisibility(View.VISIBLE);
        recyclerview.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerview.setLayoutManager(layoutManager);
        adapter = new HW_ReportAdapter(hwList, getActivity(), 2);
        recyclerview.setAdapter(adapter);
        m_d.setText("Date");
        getTodayDate();
        recyclerview.addOnScrollListener(new PaginationListener(layoutManager) {
            @Override
            protected void loadMoreItems() {
                // isLoading = true;
                //  Utils.showSnackBar("No more messages found in message list.", parent);
                //  currentPage++;
                // doApiCall();

                if (total_pages>=page_limit) {
                    main_progress.setVisibility(View.VISIBLE);
                    currentPage += 10;
                    page_limit = currentPage + limit;

                    if (sub_id.equalsIgnoreCase("")){
                        fetchHomework(date,0,sub_id, page_limit, "no");
                    }else{
                        fetchHomework(date,5,sub_id, page_limit, "no");
                    }
                }else{
                    // main_progress.setVisibility(View.GONE);
                    //  Utils.showSnackBar("No more messages found in message list.", parent);
                }


            }
            @Override
            public boolean isLastPage() {
                return isLastPage;
            }
            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });
        school=User.getCurrentUser().getSchoolData().getName();
        return view;
    }

    public void getTodayDate() {
        java.util.Calendar c = java.util.Calendar.getInstance();
        c.set(java.util.Calendar.HOUR_OF_DAY, 00);
        c.set(java.util.Calendar.MINUTE, 00);
        c.set(java.util.Calendar.SECOND, 00);
        Date setdate = c.getTime();
        java.text.SimpleDateFormat fmtOut = new java.text.SimpleDateFormat("d MMMM");
        String dateString = fmtOut.format(setdate);
        //  adddate.setText(dateString);
        java.text.SimpleDateFormat dateFrmOut = new java.text.SimpleDateFormat("yyyy-MM-dd");
        tdate = dateFrmOut.format(setdate);
        date = dateFrmOut.format(setdate);
        //for Yesterday
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);
        ydate= dateFormat.format(cal.getTime());

            adddate.setTextColor(ContextCompat.getColor(getActivity(), R.color.light_red));
            adddate.setText(dateString);

        page_limit=0;
        fetchClass(limit);

    }

    @OnClick(R.id.filelayout)
    public void fetchDate() {
        java.util.Calendar cal = java.util.Calendar.getInstance(TimeZone.getDefault()); // Get current date
        DatePickerDialog datePicker = new DatePickerDialog(getActivity(),
                R.style.MyDatePickerDialogTheme, ondate,
                cal.get(java.util.Calendar.YEAR),
                cal.get(java.util.Calendar.MONTH),
                cal.get(java.util.Calendar.DAY_OF_MONTH));
        datePicker.getDatePicker().setMaxDate(new java.util.Date().getTime());
        datePicker.setCancelable(false);
        datePicker.show();
    }

    DatePickerDialog.OnDateSetListener ondate = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            java.util.Calendar calendar = java.util.Calendar.getInstance(TimeZone.getDefault());
            calendar.set(year, monthOfYear, dayOfMonth);
            calendar.set(Calendar.HOUR_OF_DAY, 00);
            calendar.set(Calendar.MINUTE, 00);
            calendar.set(Calendar.SECOND, 00);
            java.util.Date setdate = calendar.getTime();
            java.text.SimpleDateFormat fmtOut = new java.text.SimpleDateFormat("d MMMM");
            String dateString = fmtOut.format(setdate);
            //adddate.setText(dateString);
            SimpleDateFormat dateFrmOut = new SimpleDateFormat("yyyy-MM-dd");
            date = dateFrmOut.format(setdate);

            adddate.setTextColor(ContextCompat.getColor(getActivity(), R.color.light_red));
            adddate.setText(dateString);

            page_limit=0;
            fetchClass(limit);
        }
    };

    @SuppressLint("RestrictedApi")
    @OnClick(R.id.export)
    public void submit() {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.S) {
            importData();
        } else {
            if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {

                if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    Toast.makeText(getActivity(), "Please provide storage permission from app settings", Toast.LENGTH_LONG).show();
                } else {

                    ActivityCompat.requestPermissions(getActivity(),
                            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            1);
                }
            } else {
                importData();

            }
        }
    }

    @SuppressLint("RestrictedApi")
    private void importData() {
        // File filePath = new File(Environment.getExternalStorageDirectory() + "/Demo.xls");
        Workbook wb = new HSSFWorkbook();

        Cell cell = null;

        String ndate="",sub_staff="",sub_date="";
        ndate= Utils.getDateOnlyNEW(date)+","+Utils.getMonthFormated(date);
        sub_date=ndate.substring(0,4);
        if (staff_name.equalsIgnoreCase("")){
            sub_staff="hw";
        }else {
            sub_staff=staff_name.substring(3,7)+"_hw";
        }

        Sheet sheet = null;
        sheet = wb.createSheet(adddate.getText().toString()+" date Homework Report");
        //Now column and row
        Row row = sheet.createRow(0);

        cell = row.createCell(0);
        cell.setCellValue("S_no");

        cell = row.createCell(1);
        cell.setCellValue("Hw_id");

        cell = row.createCell(2);
        cell.setCellValue("Class/Section");


        cell = row.createCell(3);
        cell.setCellValue("Teacher Name");


        cell = row.createCell(4);
        cell.setCellValue("Date");


        //column width
        sheet.setColumnWidth(0, (20 * 100));
        sheet.setColumnWidth(1, (20 * 200));
        sheet.setColumnWidth(2, (30 * 150));
        sheet.setColumnWidth(3, (30 * 250));
        sheet.setColumnWidth(4, (30 * 200));

        for (int i = 0; i < hwList.size(); i++) {

            Row row1 = sheet.createRow(i + 1);

            cell = row1.createCell(0);
            cell.setCellValue(i+1);

            cell = row1.createCell(1);
            cell.setCellValue("hw-"+hwList.get(i).getId());

            cell = row1.createCell(2);
            cell.setCellValue(hwList.get(i).getClass_name()+" ("+hwList.get(i).getSection_name()+")");

            cell = row1.createCell(3);
            cell.setCellValue((hwList.get(i).getStaff_name())+" ("+hwList.get(i).getPost_name()+")");
            //  cell.setCellStyle(cellStyle);

            cell = row1.createCell(4);
            cell.setCellValue(Utils.getDateTimeFormatedWithAMPM(hwList.get(i).getDate()));


            sheet.setColumnWidth(0, (20 * 100));
            sheet.setColumnWidth(1, (20 * 200));
            sheet.setColumnWidth(2, (30 * 150));
            sheet.setColumnWidth(3, (30 * 250));
            sheet.setColumnWidth(4, (30 * 200));

        }

        String fileName;
        if (sub_staff.equalsIgnoreCase("")){
            fileName = sub_date+"_" +System.currentTimeMillis() + ".xls";
        }else {
            fileName = sub_staff+"_"+Utils.getDateOnlyNEW(date)+"_"+Utils.getMonthFormated(date)+"_" + System.currentTimeMillis() + ".xls";
        }

       // File file = new File(getActivity().getExternalFilesDir(null), fileName);

        File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS), fileName);

        FileOutputStream fileOutputStream = null;

        try {
            fileOutputStream = new FileOutputStream(file);
            wb.write(fileOutputStream);
            Utils.openSuccessDialog("Success!","Export file in "+file,"",getActivity());
        } catch (IOException e) {
            Utils.openErrorDialog("Error writing Exception: "+ e,getActivity());
        } catch (Exception e) {
            Utils.openErrorDialog("Failed to save file due to Exception: "+ e,getActivity());
        } finally {
            try {
                if (null != fileOutputStream) {
                    fileOutputStream.close();
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

//
//        String folderName = "Import Excel";
//        String fileName = folderName + System.currentTimeMillis() + ".xls";
//        String path = Environment.getExternalStorageDirectory() + File.separator + folderName + File.separator + fileName;
//
//        File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS), folderName);
//
//
//       // File file = new File(Environment.getExternalStorageDirectory() + File.separator + folderName);
//        if (!file.exists()) {
//            file.mkdirs();
//        }
//
//
//        FileOutputStream outputStream = null;
//
//        try    {
//            outputStream = new FileOutputStream(path);
//            wb.write(outputStream);
//            // ShareViaEmail(file.getParentFile().getName(),file.getName());
//            Toast.makeText(getApplicationContext(), "Excel Created in " + path, Toast.LENGTH_SHORT).show();
//            Log.i("path",path);
//        } catch (IOException e) {
//            e.printStackTrace();
//
//            Toast.makeText(getApplicationContext(), "Not OK", Toast.LENGTH_LONG).show();
//            try {
//                outputStream.close();
//            } catch (Exception ex) {
//                ex.printStackTrace();
//
//            }
//        }


    }

    @SuppressLint("RestrictedApi")
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == 1 && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            importData();
        } else {
            Toast.makeText(getActivity(), "Permission Denied", Toast.LENGTH_SHORT).show();
        }
    }

    private void fetchClass(int limit) {

        HashMap<String, String> params = new HashMap<String, String>();
        params.put("check", "list");
        params.put("d_id", "");
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.STAFF_DETAIL, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                if (error == null) {
                    try {
                        JSONArray jsonArray = jsonObject.getJSONArray("staff_detail");
                        classList = View_staff_bean.parseviewstaffArray(jsonArray);
                        staffListAdapter = new StaffListAdapter(getActivity(), classList);
                        spinnerClass.setAdapter(staffListAdapter);
                        spinnerClass.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                sub_id="";
                                staff_name="";
                                if (i > 0) {
                                    sub_id = classList.get(i - 1).getId();
                                    staff_name=classList.get(i - 1).getName();
                                    fetchHomework(date,5,sub_id, limit, "yes");
                                } else {
                                    fetchHomework(date,0,sub_id, limit, "yes");
                                }
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> adapterView) {

                            }
                        });

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
//                Utils.showSnackBar(error.getMessage(), parent);
                }
            }
        }, getActivity(), params);
    }


    public void fetchHomework(String date, int i,String sub_id,int limit,String progress) {
        if (progress.equalsIgnoreCase("yes")){
            commonProgress.show();
        }
        HashMap<String, String> params = new HashMap<String, String>();
        //params.put("class_id", classid);

        if (i == 0){
            params.put("class_id", classid);
            params.put("section_id", sectionid);
        } else {
            params.put("class_id", classid);
            params.put("section_id", sectionid);
            params.put("sub_id", sub_id);
        }
        params.put("today",date);
        params.put("view_type",view_type);
        params.put("page_limit",String.valueOf(limit));
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.gdshomeworkurl, apiCallback, getActivity(), params);
    }

    ApiHandler.ApiCallback apiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            commonProgress.dismiss();
            if (error == null) {
                try {
                    if (hwList != null) {
                        hwList.clear();
                    }
                    JSONArray jsonArray = jsonObject.getJSONArray("hw_data");
                    hwList = Homeworkbean.parseHWArray(jsonArray);
                    total_pages= Integer.parseInt(hwList.get(0).getRowcount());
                    if (hwList.size() > 0) {
                        adapter.setHQList(hwList);
                        adapter.notifyDataSetChanged();
                        txtNorecord.setVisibility(View.GONE);
                        totalRecord.setText("Total Entries:- "+String.valueOf(hwList.size()));
                        export.setVisibility(View.VISIBLE);
                        if (!restorePrefData()){
                            setShowcaseView();
                        }
                    } else {
                        export.setVisibility(View.GONE);
                        totalRecord.setText("Total Entries:- 0");
                        adapter.setHQList(hwList);
                        adapter.notifyDataSetChanged();
                        txtNorecord.setVisibility(View.VISIBLE);
                    }
                    main_progress.setVisibility(View.GONE);
                    lytHeader.setVisibility(View.VISIBLE);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                export.setVisibility(View.GONE);
                lytHeader.setVisibility(View.GONE);
                totalRecord.setText("Total Entries:- 0");
                txtNorecord.setVisibility(View.VISIBLE);
                hwList.clear();
                adapter.setHQList(hwList);
                adapter.notifyDataSetChanged();
            }
        }
    };

    private void setShowcaseView() {
        new TapTargetSequence(getActivity())
                .targets(TapTarget.forView(export,"Export Excel!","Tap the Export button to export Staff-Wise HW report.")
                        .outerCircleColor(R.color.light)
                        .outerCircleAlpha(0.96f)
                        .targetCircleColor(R.color.white)
                        .titleTextSize(20)
                        .titleTextColor(R.color.white)
                        .descriptionTextSize(14)
                        .descriptionTextColor(R.color.black)
                        .textColor(R.color.black)
                        .textTypeface(Typeface.SANS_SERIF)
                        .dimColor(R.color.black)
                        .drawShadow(true)
                        .cancelable(false)
                        .tintTarget(true)
                        .transparentTarget(true)
                        .targetRadius(40)).listener(new TapTargetSequence.Listener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onSequenceFinish() {
                // Toast.makeText(getApplicationContext(),"Sequence Finished",Toast.LENGTH_SHORT).show();
                savePrefData();

            }

            @Override
            public void onSequenceStep(TapTarget lastTarget, boolean targetClicked) {
                // Toast.makeText(getActivity(),"GREAT!",Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSequenceCanceled(TapTarget lastTarget) {

            }
        }).start();
    }

    private void savePrefData(){
        sharedPreferences=getActivity().getSharedPreferences("boarding_pref_view_export",MODE_PRIVATE);
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putBoolean("IsFirstTimeVisit_view_export",true);
        editor.apply();
    }

    private boolean restorePrefData(){
        sharedPreferences = getActivity().getSharedPreferences("boarding_pref_view_export",MODE_PRIVATE);
        return sharedPreferences.getBoolean("IsFirstTimeVisit_view_export",false);
    }

}