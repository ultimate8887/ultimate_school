package com.ultimate.ultimatesmartschool.Homework;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.StudentAttendance.AttendBean;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HW_ReportAdapter extends RecyclerSwipeAdapter<HW_ReportAdapter.Viewholder> {
    ArrayList<Homeworkbean> hq_list;
    Context listner;
    int value;

    public HW_ReportAdapter(ArrayList<Homeworkbean> hq_list, Context listener, int value) {
        this.value = value;
        this.hq_list = hq_list;
        this.listner = listener;
    }


    @Override
    public HW_ReportAdapter.Viewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.report_hw_lay, parent, false);
        HW_ReportAdapter.Viewholder viewholder = new HW_ReportAdapter.Viewholder(view);
        return viewholder;

    }

    @Override
    public void onBindViewHolder(final HW_ReportAdapter.Viewholder holder, final int position) {
        final Homeworkbean data = hq_list.get(position);
        holder.txtRoll.setText(data.getClass_name()+"("+data.getSection_name()+")");
        holder.txtsno.setText(data.getId());
        holder.txtName.setText(data.getStaff_name());

        if (value==1){
            holder.txtAttend.setText(Utils.getDateTimeFormatedWithAMPM(data.getDate()));
        }else {
            holder.txtAttend.setText(Utils.getDateTimeFormatedWithAMPM(data.getDate()));
        }




    }

    @Override
    public int getItemCount() {
        return hq_list.size();
    }

    public void setHQList(ArrayList<Homeworkbean> hq_list) {
        this.hq_list = hq_list;
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe;
    }


    public class Viewholder extends RecyclerView.ViewHolder {
        @BindView(R.id.txtAttend)
        TextView txtAttend;
        @BindView(R.id.txtName)
        TextView txtName;
        @BindView(R.id.txtRoll)
        TextView txtRoll;
        @BindView(R.id.txtFName)
        TextView txtFName;
        @BindView(R.id.txtsno)
        TextView txtsno;
        public Viewholder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }


}
