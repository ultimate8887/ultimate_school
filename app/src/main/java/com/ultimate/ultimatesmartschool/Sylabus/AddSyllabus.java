package com.ultimate.ultimatesmartschool.Sylabus;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ClipData;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.pdf.PdfRenderer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.ParcelFileDescriptor;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.bumptech.glide.Glide;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.kroegerama.imgpicker.BottomSheetImagePicker;

import com.ultimate.ultimatesmartschool.BeanModule.ClassBean;
import com.ultimate.ultimatesmartschool.BeanModule.SectionBean;
import com.ultimate.ultimatesmartschool.BeanModule.User;
import com.ultimate.ultimatesmartschool.Ebook.Ebook;
import com.ultimate.ultimatesmartschool.Examination.ClassAdapter;
import com.ultimate.ultimatesmartschool.Homework.SectionnewAdapter;
import com.ultimate.ultimatesmartschool.Login.LoginActivity;
import com.ultimate.ultimatesmartschool.PDF.PdfPageAdapter;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.CommonProgress;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;
import com.ultimate.ultimatesmartschool.Utility.ErpProgressVert;
import com.ultimate.ultimatesmartschool.Utility.FileUtils;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.apptik.widget.multiselectspinner.BaseMultiSelectSpinner;
import io.apptik.widget.multiselectspinner.MultiSelectSpinner;

public class AddSyllabus extends AppCompatActivity implements BottomSheetImagePicker.OnImagesSelectedListener {
    ArrayList<ClassBean> classList = new ArrayList<>();
    @BindView(R.id.parent)
    RelativeLayout parent;
    @BindView(R.id.spinnerClass)
    Spinner spinnerClass;
    @BindView(R.id.addimage)
    TextView addimage;
    ArrayList<String> image;
    List<? extends Uri> image2;
    String classid = "",add_img="";
    private int loaded = 0;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    List<String> classidsel;
    @BindView(R.id.textView7)TextView textView7;
    private int type;
    private Bitmap hwbitmap;
    @BindView(R.id.imageView6)
    ImageView imageView6;
    @BindView(R.id.scrollView)
    HorizontalScrollView scrollView;
    private final int LOAD_FILE_RESULTS = 1000;
    String fileBase64 = null;
    BottomSheetDialog mBottomSheetDialog1;
    @BindView(R.id.textnote)TextView textnote;
    @BindView(R.id.addpdf)TextView addpdf;
    @BindView(R.id.comment)
    EditText comment;
    CommonProgress commonProgress;
    Animation animation;
    String path="";
    Uri uri;
    float  length;
    String size;
    private ViewGroup imageContainer;
    String encoded;

    private final int PICK_IMAGE_MULTIPLE = 11;
    String imageEncoded;
    List<String> imagesEncodedList;

    @BindView(R.id.spinnersection)
    Spinner spinnersection;

    ArrayList<SectionBean> sectionList = new ArrayList<>();
    String sectionid = "";
    String sectionname;
    String check = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_syllabus);
        ButterKnife.bind(this);

        if (getIntent().getExtras() != null) {
            check = getIntent().getExtras().getString("check");
            if(check.equalsIgnoreCase("month")){
                txtTitle.setText("Monthly Planner");
            }else{
                txtTitle.setText("Syllabus");
            }
        }

        imageContainer = findViewById(R.id.imageContainer);
        animation = AnimationUtils.loadAnimation(AddSyllabus.this, R.anim.btn_blink_animation);
        commonProgress=new CommonProgress(this);
        classidsel = new ArrayList<>();
        fetchClass();
        initializeViews();
       // txtTitle.setText("Syllabus");
    }

    private void initializeViews() {

    }

    private void fetchClass() {
        if (loaded == 0) {
            commonProgress.show();
        }
        loaded++;

        HashMap<String, String> params = new HashMap<String, String>();
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.CLASSLIST_URL, classapiCallback,getApplicationContext(), params);
    }


    ApiHandler.ApiCallback classapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            commonProgress.dismiss();
            if (error == null) {
                try {
                    classList = ClassBean.parseClassArray(jsonObject.getJSONArray(Constants.CLASSDATA));
                    ClassAdapter adapter = new ClassAdapter(getApplicationContext(), classList);
                    spinnerClass.setAdapter(adapter);
                    spinnerClass.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if (i > 0) {
                                classid = classList.get(i - 1).getId();
                                fetchSection();
                            }

                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_LONG).show();
                // Utils.showSnackBar(error.getMessage(), view.findViewById(R.id.parent));
            }
        }
    };

    private void fetchSection() {
        commonProgress.show();
        HashMap<String, String> params = new HashMap<String, String>();
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.SECTIONLIST_URL, sectionapiCallback, this, params);
    }

    ApiHandler.ApiCallback sectionapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            commonProgress.dismiss();
            if (error == null) {
                try {
                    sectionList = SectionBean.parseCommonArray(jsonObject.getJSONArray("hostelroom_data"));
                    SectionnewAdapter adapter = new SectionnewAdapter(AddSyllabus.this, sectionList);
                    spinnersection.setAdapter(adapter);
                    spinnersection.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if (i > 0) {
                                sectionid = sectionList.get(i - 1).getSection_id();
                                sectionname = sectionList.get(i - 1).getSection_name();
                                // fetchHomework();
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Utils.showSnackBar(error.getMessage(), parent);

            }
        }
    };

    @OnClick(R.id.addpdf)
    public void browseDocuments() {
        add_img="";
        addpdf.startAnimation(animation);
        pick_pdf();

    }
    private void pick_pdf() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.UPSIDE_DOWN_CAKE) { // Android 14+ (API 34)
           callPicker();

        } else  if (Build.VERSION.SDK_INT > Build.VERSION_CODES.S) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_MEDIA_IMAGES)
                    != PackageManager.PERMISSION_GRANTED) {

                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_MEDIA_IMAGES)) {
                    Toast.makeText(this, "Please provide storage permission from app settings", Toast.LENGTH_LONG).show();
                } else {

                    ActivityCompat.requestPermissions(this,
                            new String[]{Manifest.permission.READ_MEDIA_IMAGES},
                            2);
                }
            } else {

                callPicker();
            }

        } else {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {

                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    Toast.makeText(this, "Please provide storage permission from app settings", Toast.LENGTH_LONG).show();
                } else {

                    ActivityCompat.requestPermissions(this,
                            new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                            2);
                }
            } else {
                callPicker();

            }
        }
    }


    public void callPicker(){
        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        intent.setType("application/pdf");
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        startActivityForResult(intent, LOAD_FILE_RESULTS);
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    @SuppressLint("MissingSuperCall")
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (requestCode==2){
                        callPicker();
                    }else {
                        CallCropAct();
                    }

                } else {
                    // permission denied, boo! Disable the
                    Toast.makeText(this, "Please provide storage permission from app settings, as it is mandatory to access your files!", Toast.LENGTH_LONG).show();
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request.
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
            if (resultCode == Activity.RESULT_OK) {
                if (requestCode == PICK_IMAGE_MULTIPLE && null != data) {
                    // Get the Image from data
                    String[] filePathColumn = {MediaStore.Images.Media.DATA};
                    imagesEncodedList = new ArrayList<String>();
                    if (data.getData() != null) {

                        Uri mImageUri = data.getData();

                        // Get the cursor
                        Cursor cursor = getContentResolver().query(mImageUri,
                                filePathColumn, null, null, null);
                        // Move to first row
                        cursor.moveToFirst();

                        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                        imageEncoded = cursor.getString(columnIndex);
                        cursor.close();

                    } else {
                        if (data.getClipData() != null) {
                            ClipData mClipData = data.getClipData();
                            ArrayList<Uri> mArrayUri = new ArrayList<Uri>();
                            for (int i = 0; i < mClipData.getItemCount(); i++) {

                                ClipData.Item item = mClipData.getItemAt(i);
                                Uri uri = item.getUri();
                                mArrayUri.add(uri);
                                // Get the cursor
                                Cursor cursor = getContentResolver().query(uri, filePathColumn, null, null, null);
                                // Move to first row
                                cursor.moveToFirst();

                                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                                imageEncoded = cursor.getString(columnIndex);
                                imagesEncodedList.add(imageEncoded);
                                cursor.close();

                            }
                            commonCode(mArrayUri);

                        }
                    }
                } else {
                    uri = data.getData();
                    // String path = Utils.getPath(this, uri);

                    if (uri != null) {
                        try {
                            ContentResolver contentResolver = getContentResolver();
                            InputStream inputStream = contentResolver.openInputStream(uri);
                            file = new File(getCacheDir(), "temp.pdf");
                            OutputStream outputStream = new FileOutputStream(file);
                            byte[] buffer = new byte[4 * 1024]; // or other buffer size
                            int read;

                            while ((read = inputStream.read(buffer)) != -1) {
                                outputStream.write(buffer, 0, read);
                            }

                            outputStream.flush();
                            outputStream.close();
                            inputStream.close();

                            String path = file.getAbsolutePath();
                            long fileSize = file.length();
                            size = formatSize(fileSize);
                            if (fileSize > 18 * 1024 * 1024) { // 18 MB
                                Toast.makeText(getApplicationContext(),
                                        "Sorry, Your selected file size is too large (" + size + ")." +
                                                "Please select a file with a size of less than 18 MB.", Toast.LENGTH_LONG).show();
                            } else {
                                fileBase64 = Utils.encodeFileToBase64Binary(path);
                                hwbitmap = null;
                                showPdfFromUri(uri, path);
                                textnote.setText("Pdf Size: " + "" + "" + size);
                                textnote.setVisibility(View.VISIBLE);
                                imageView6.setImageResource(R.drawable.pdfbig);
                                scrollView.setVisibility(View.GONE);
                                imageView6.setVisibility(View.VISIBLE);
                                Toast.makeText(this, "File Picked...", Toast.LENGTH_SHORT).show();
                                add_img = "pdf";
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }

            }else {
                Toast.makeText(this, "Selection Cancelled", Toast.LENGTH_SHORT).show();
            }

    }
    File file;
    @SuppressLint("MissingInflatedId")
    private void showPdfFromUri(Uri uri, String path) {
        mBottomSheetDialog1 = new BottomSheetDialog(AddSyllabus.this);
        final View sheetView = getLayoutInflater().inflate(R.layout.pdf_view_dialog, null);
        mBottomSheetDialog1.setContentView(sheetView);
        mBottomSheetDialog1.setCancelable(false);

        TextView title= (TextView) sheetView.findViewById(R.id.title);
        TextView path1= (TextView) sheetView.findViewById(R.id.path);
        title.setText("Selected PDF");
        //  path1.setText(path);
        path1.setText(path + "\nSize: " + size);
        //pdf code start here
        RecyclerView recyclerView;
        PdfPageAdapter adapter;
        recyclerView = (RecyclerView)  sheetView.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        if (file.exists()) {
            try {
                ParcelFileDescriptor parcelFileDescriptor = ParcelFileDescriptor.open(file, ParcelFileDescriptor.MODE_READ_ONLY);

                // Check if ParcelFileDescriptor is null
                if (parcelFileDescriptor == null) {
                    Log.e("PDF_ERROR", "ParcelFileDescriptor is null. Unable to open the file.");
                    //   Toast.makeText(this, "Unable to open the PDF file", Toast.LENGTH_SHORT).show();
                    return;
                }

                PdfRenderer pdfRenderer = new PdfRenderer(parcelFileDescriptor);
                int pageCount = pdfRenderer.getPageCount();

                // Log the number of pages found in the PDF
                Log.d("PDF_INFO", "Total number of pages: " + pageCount);

                List<Bitmap> bitmaps = new ArrayList<>();

                for (int i = 0; i < pageCount; i++) {
                    PdfRenderer.Page pdfPage = pdfRenderer.openPage(i);

                    // Check if pdfPage is null
                    if (pdfPage == null) {
                        Log.e("PDF_ERROR", "Failed to open page " + i);
                        continue;
                    }

                    Bitmap bitmap = Bitmap.createBitmap(pdfPage.getWidth(), pdfPage.getHeight(), Bitmap.Config.ARGB_8888);
                    pdfPage.render(bitmap, null, null, PdfRenderer.Page.RENDER_MODE_FOR_DISPLAY);
                    bitmaps.add(bitmap);

                    // Log successful page rendering
                    Log.d("PDF_INFO", "Page " + i + " rendered successfully.");

                    pdfPage.close();
                }

                adapter = new PdfPageAdapter(this, bitmaps);
                recyclerView.setAdapter(adapter);

                pdfRenderer.close();
                parcelFileDescriptor.close();

            } catch (FileNotFoundException e) {
                Log.e("PDF_ERROR", "File not found: " + e.getMessage());
                Toast.makeText(this, "PDF file not found", Toast.LENGTH_SHORT).show();
            } catch (SecurityException e) {
                Log.e("PDF_ERROR", "Permission denied: " + e.getMessage());
                Toast.makeText(this, "Permission denied", Toast.LENGTH_SHORT).show();
            } catch (IOException e) {
                Log.e("PDF_ERROR", "IO Exception: " + e.getMessage());
                Toast.makeText(this, "Failed to open the PDF file", Toast.LENGTH_SHORT).show();
            }
        } else {
            Log.e("PDF_ERROR", "File does not exist at the path: " + path);
            Toast.makeText(this, "PDF file not found at: " + path, Toast.LENGTH_SHORT).show();
        }
        Button retake= (Button) sheetView.findViewById(R.id.retake);
        retake.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                retake.startAnimation(animation);
                pick_pdf();
                mBottomSheetDialog1.dismiss();
            }
        });
        Button submitted= (Button) sheetView.findViewById(R.id.submitted);
        submitted.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                submitted.startAnimation(animation);
                mBottomSheetDialog1.dismiss();
                //  submitAssignment();

            }
        });
        RelativeLayout btnNo= (RelativeLayout) sheetView.findViewById(R.id.btnNo);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnNo.startAnimation(animation);
                //   fileBase64=null;
                mBottomSheetDialog1.dismiss();
            }
        });
        mBottomSheetDialog1.show();
    }

    public static String formatSize(long size) {
        String suffix = "B";
        double formattedSize = size;

        if (formattedSize >= 1024) {
            suffix = "KB";
            formattedSize /= 1024;
        }

        if (formattedSize >= 1024) {
            suffix = "MB";
            formattedSize /= 1024;
        }

        if (formattedSize >= 1024) {
            suffix = "GB";
            formattedSize /= 1024;
        }

        return String.format("%.2f %s", formattedSize, suffix);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @SuppressLint("ResourceType")
    @OnClick(R.id.addimage)
    public void click() {
        addimage.startAnimation(animation);
        add_img="";
        CallCropAct();
    }

    @SuppressLint("ResourceType")
    @RequiresApi(api = Build.VERSION_CODES.M)
    public void CallCropAct() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.UPSIDE_DOWN_CAKE) { // Android 14+ (API 34)
            //Toast.makeText(this, "Multi Images", Toast.LENGTH_LONG).show();
            // initialising intent
            Intent intent = new Intent();
            // setting type to select to be image
            intent.setType("image/*");
            // allowing multiple image to be selected
            intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(Intent.createChooser(intent, "Select Multiple Images"), PICK_IMAGE_MULTIPLE);

        } else if (Build.VERSION.SDK_INT > Build.VERSION_CODES.S) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_MEDIA_IMAGES)
                    != PackageManager.PERMISSION_GRANTED) {

                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_MEDIA_IMAGES)) {
                    Toast.makeText(this, "Please provide storage permission from app settings", Toast.LENGTH_LONG).show();
                } else {

                    ActivityCompat.requestPermissions(this,
                            new String[]{Manifest.permission.READ_MEDIA_IMAGES},
                            1);
                }
            } else {
                //Toast.makeText(this, "Multi Images", Toast.LENGTH_LONG).show();
                // initialising intent
                Intent intent = new Intent();
                // setting type to select to be image
                intent.setType("image/*");
                // allowing multiple image to be selected
                intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Multiple Images"), PICK_IMAGE_MULTIPLE);
            }

        } else {

            if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {

                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    Toast.makeText(this, "Please provide storage permission from app settings", Toast.LENGTH_LONG).show();
                } else {

                    ActivityCompat.requestPermissions(this,
                            new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                            1);

                }
            } else {
                new BottomSheetImagePicker.Builder(getString(R.xml.provider_paths))
                        .multiSelect(1, 10)
                        .multiSelectTitles(
                                R.plurals.pick_multi,
                                R.plurals.pick_multi_more,
                                R.string.pick_multi_limit
                        )
                        .peekHeight(R.dimen.peekHeight)
                        .columnSize(R.dimen.columnSize)
                        .requestTag("multi")
                        .show(getSupportFragmentManager(), null);
            }

        }

    }


    @Override
    public void onImagesSelected(List<? extends Uri> list, String s) {
        commonCode(list);
    }
    private void commonCode(List<? extends Uri> list) {
        imageView6.setVisibility(View.GONE);
        scrollView.setVisibility(View.VISIBLE);
        imageContainer.removeAllViews();
        image = new ArrayList<>();
        image2 = list;
        for (Uri uri : list) {
            ImageView iv = (ImageView) LayoutInflater.from(this).inflate(R.layout.scrollitem_image, imageContainer, false);
            imageContainer.addView(iv);
            Glide.with(this).load(uri).into(iv);
            hwbitmap = Utils.getBitmapFromUri(AddSyllabus.this, uri, 2048);
            Log.e("hwbitmapup", String.valueOf(hwbitmap));
            encoded = Utils.encodeToBase64(hwbitmap, Bitmap.CompressFormat.JPEG, 50);
            // encoded = String.format("data:image/jpeg;base64,%s", encoded);
            image.add(encoded);

        }
        Log.e("hwbitmapupdown", String.valueOf(hwbitmap));
        Log.e("image2", String.valueOf(image2));
        textnote.setVisibility(View.VISIBLE);
        textnote.setText("Image: You have selected " + String.valueOf(image.size()) + " images");
        add_img = "add";
    }


    @OnClick(R.id.button3)
    public void save() {

        if (classid.equalsIgnoreCase("")) {
            Toast.makeText(this,"Kindly select class",Toast.LENGTH_SHORT).show();

        }else if (sectionid.equalsIgnoreCase("")) {
            Toast.makeText(this,"Kindly select class section",Toast.LENGTH_SHORT).show();
        } else if (comment.getText().toString().isEmpty()) {
            Toast.makeText(this,"Kindly add topic",Toast.LENGTH_SHORT).show();
        }  else if (add_img.equalsIgnoreCase("")) {
            Toast.makeText(this,"Kindly select file",Toast.LENGTH_SHORT).show();
        } else{
            commonProgress.show();
            HashMap<String, String> params = new HashMap<String, String>();
            params.put("class_id",  classid);
            params.put("image", String.valueOf(image));
            params.put("section_id", sectionid);
            params.put("add_img", add_img);
            params.put("check", check);
            params.put("content", comment.getText().toString());
            if (fileBase64 != null)
                params.put("file", fileBase64);
            ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.ADD_SYLLABUSGDS, apiCallback, this, params);
        }

    }



    ApiHandler.ApiCallback apiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgressVert.cancelProgressBar();
            commonProgress.dismiss();
            if (error == null) {
                try {
                    Utils.showSnackBar(jsonObject.getString("msg"), parent);
                    Toast.makeText(getApplicationContext(),"Upload Successfully!",Toast.LENGTH_SHORT).show();
                    finish();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("error", error.getMessage() + "");
                // Utils.showSnackBar(error.getMessage(), parent);
                Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_LONG).show();
                if (error.getStatusCode() == 405) {
                    User.logout();
                    startActivity(new Intent(AddSyllabus.this, LoginActivity.class));
                    finish();
                }
            }
        }
    };

    private boolean checkValid() {

        boolean valid = true;
        String errorMsg = null;

        if(comment.getText().toString().trim().length()<=0){
            valid=false;
            errorMsg="Please enter the comment";
        }

        if (!valid) {
            Toast.makeText(getApplicationContext(),errorMsg,Toast.LENGTH_LONG).show();

        }
        return valid;
    }
    @OnClick(R.id.imgBack)
    public void onback() {
        finish();
    }
}
