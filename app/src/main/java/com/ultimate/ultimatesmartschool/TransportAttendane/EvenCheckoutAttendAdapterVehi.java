package com.ultimate.ultimatesmartschool.TransportAttendane;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.StudentAttendance.AttendBean;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class EvenCheckoutAttendAdapterVehi extends RecyclerView.Adapter<EvenCheckoutAttendAdapterVehi.MyViewHolder> {
    private EvenCheckoutAttendAdapterVehi.CallBackMethodtrans callBackMethodtrans;
    private Context mContext;
    ArrayList<AttendBean> dataList;




    public EvenCheckoutAttendAdapterVehi(Context mContext, ArrayList<AttendBean> dataList, EvenCheckoutAttendAdapterVehi.CallBackMethodtrans callBackMethodtrans) {
        this.mContext = mContext;
        this.dataList = dataList;
        //   this.mapAttendanceData = mapAttendanceData;
        this.callBackMethodtrans=callBackMethodtrans;
    }

    public void setNoticeList(ArrayList<AttendBean> dataList) {
        this.dataList = dataList;
    }


    public interface CallBackMethodtrans {


        void editRouteCallback(AttendBean routedata, int pos);

    }



    @Override
    public EvenCheckoutAttendAdapterVehi.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.transstud_attend_lyt, parent, false);
        EvenCheckoutAttendAdapterVehi.MyViewHolder vh = new EvenCheckoutAttendAdapterVehi.MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final EvenCheckoutAttendAdapterVehi.MyViewHolder holder, final int position) {
        final AttendBean data = dataList.get(position);
        holder.txtName.setText(data.getStudent_name());
        holder.txtRegNo.setText(data.getStudent_id()+"("+data.getClass_name()+")"+" "+data.getSection_name());
        holder.txtFname.setText("Father: "+data.getFather_name());
        if(data.getRoute_name()!=null){
            holder.txtRoute.setText("Route: "+data.getRoute_name());
        }else{
            //  holder.txtRoute.setVisibility(View.GONE);
        }

        if(data.getPickupname()!=null){
            holder.txtpickup.setText("PickUp: "+data.getPickupname());
        }else{
            //  holder.txtpickup.setVisibility(View.GONE);
        }
        if(data.getTr_vehicle_no()!=null){
            holder.vehiclename.setText("Vehicle: "+data.getTr_vehicle_no_eveni()+"("+data.getTr_transport_name_eveni()+")");

        }else{
            // holder.vehiclename.setVisibility(View.GONE);
        }
        if(data.getDrivername()!=null) {
            holder.drivername.setText("Driver: " + data.getEvendriver_name());
        }else{
            //holder.drivername.setVisibility(View.GONE);
        }

        if(data.getCheckintime()!=null) {
            holder.checkintime.setText("CheckIn-Time: " + data.getCheckintime());
        }else{
            //  holder.checkintime.setVisibility(View.GONE);
        }
        if(data.getCheckouttime()!=null){
            holder. checkOUTtime.setText("CheckOut-Time: "+data.getCheckouttime());

        }else{
            //holder. checkOUTtime.setVisibility(View.GONE);
        }

        if(data.getDiver_mobile()!=null){
            holder. driverphone.setText("Driver Phone: "+data.getEvendiver_mobile());

        }else{
            //holder. checkOUTtime.setVisibility(View.GONE);
        }

        if(data.getAttendMarkByUser()!=null){
            holder. attndmarkby.setText("Attendance Marked By: "+data.getAttendMarkByUser());

        }else{
            //holder. checkOUTtime.setVisibility(View.GONE);
        }
        if(data.getStatus()!=null) {
            if (data.getStatus().equalsIgnoreCase("check-in")) {
                holder.checkout.setVisibility(View.VISIBLE);


            } else {
                holder.checkout.setVisibility(View.GONE);

            }
        }

        holder.checkout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (callBackMethodtrans != null) {
                    callBackMethodtrans.editRouteCallback(dataList.get(position),position);
                }
            }
        });
        Log.e("fathername",data.getFather_name());


//
    }

    @Override
    public int getItemCount() {
        return (dataList.size());
    }




    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txtRegNo)
        TextView txtRegNo;
        @BindView(R.id.txtName)
        TextView txtName;
        @BindView(R.id.spinnerAttend)
        Spinner spinnerAttend;
        @BindView(R.id.txtFname)
        TextView txtFname;
        @BindView(R.id.txtRoute)TextView txtRoute;
        @BindView(R.id.txtpickup)TextView txtpickup;
        @BindView(R.id.vehiclename)TextView vehiclename;
        @BindView(R.id.drivername)TextView drivername;
        @BindView(R.id.driverphone)TextView driverphone;
        @BindView(R.id.attndmarkby)TextView attndmarkby;
        @BindView(R.id.checkintime)TextView checkintime;
        @BindView(R.id.checkOUTtime)TextView checkOUTtime;

        @BindView(R.id.searchButton)
        Button checkout;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}
