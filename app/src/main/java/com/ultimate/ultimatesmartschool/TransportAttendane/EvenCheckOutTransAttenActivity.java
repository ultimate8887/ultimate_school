package com.ultimate.ultimatesmartschool.TransportAttendane;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.StudentAttendance.AttendBean;
import com.ultimate.ultimatesmartschool.Transport.AllotVicleToRouteMod.VehicleNoSpinnrAdap;
import com.ultimate.ultimatesmartschool.Transport.TransBeanMod.RouteBean;
import com.ultimate.ultimatesmartschool.Transport.TransBeanMod.VehicleBean;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class EvenCheckOutTransAttenActivity extends AppCompatActivity implements EvenCheckoutAttendAdapterVehi.CallBackMethodtrans{
    @BindView(R.id.parent)
    RelativeLayout parent;
    String classid = "";
    private int loaded = 0;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    List<String> classidsel;
    @BindView(R.id.textView7)TextView textView7;
    @BindView(R.id.spinnerroute)
    Spinner spinnerroute;
    @BindView(R.id.spinnerpickup)
    Spinner spinnerpickup;
    ArrayList<RouteBean> routeList = new ArrayList<>();
    ArrayList<VehicleBean> vehicleList = new ArrayList<>();
    String sectionid="";
    String sectionname;
    @BindView(R.id.adddate)TextView adddate;
    public String date;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.textNorecord)
    TextView txtNorecord;
    private ArrayList<AttendBean> dataList = new ArrayList<>();
    // private GDSStudAttendAdapter adapter;
    private EvenCheckoutAttendAdapterVehi adapter;
    @BindView(R.id.btnSubmit)
    Button btnSubmit;
    @BindView(R.id.sc)
    ScrollView scrollView;
    RouteBean routeBean;
    VehicleBean vehicleBean;
    String vehicle_id,route_id;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_even_check_out_trans_atten);

        ButterKnife.bind(this);


        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new EvenCheckoutAttendAdapterVehi(this, dataList,this);
        recyclerView.setAdapter(adapter);
        //  fetchRouteList();
        fetchvehicleNoListnewupdt();
        getTodayDate();
        txtTitle.setText("Evening Check-Out Report");
    }

    public void getTodayDate() {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.HOUR_OF_DAY, 00);
        c.set(Calendar.MINUTE, 00);
        c.set(Calendar.SECOND, 00);
        Date setdate = c.getTime();
        SimpleDateFormat fmtOut = new SimpleDateFormat("dd MMM, yyyy");
        String dateString = fmtOut.format(setdate);
        adddate.setText(dateString);
        SimpleDateFormat dateFrmOut = new SimpleDateFormat("yyyy-MM-dd");
        date = dateFrmOut.format(setdate);
    }
    @OnClick(R.id.imgBack)
    public void imgBack() {
        finish();
    }

    private void fetchvehicleNoListnewupdt() {

        HashMap<String, String> params = new HashMap<>();
        ApiHandler.apiHit(Request.Method.GET, Constants.getBaseURL() + Constants.VEHICLE_URL, apicallbacknewupdt, this, null);
    }

    ApiHandler.ApiCallback apicallbacknewupdt = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            if (error == null) {
                try {
                    vehicleList = VehicleBean.parseVehicleArray(jsonObject.getJSONArray("vehical_data"));
                    VehicleNoSpinnrAdap adap1 = new VehicleNoSpinnrAdap(EvenCheckOutTransAttenActivity.this, vehicleList);
                    spinnerpickup.setAdapter(adap1);
                    spinnerpickup.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                            if (i > 0) {
                                vehicleBean = vehicleList.get(i - 1);
                                vehicle_id=vehicleList.get(i-1).getEs_transportid();
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });
//                    int i = 0;
//                    for ( VehicleBean obj : vehicleList) {
//                        if (obj.getTr_vehicle_no().equals(alotVicleToRouteBean2.getV_no())) {
//                            spinnerpickup.setSelection(i + 1);
//                            break;
//                        }
//                        i++;
//                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    };
    @OnClick(R.id.filelayout)
    public void fetchDate() {
        java.util.Calendar cal = java.util.Calendar.getInstance(TimeZone.getDefault()); // Get current date
        DatePickerDialog datePicker = new DatePickerDialog(this,
                R.style.MyDatePickerDialogTheme, ondate,
                cal.get(java.util.Calendar.YEAR),
                cal.get(java.util.Calendar.MONTH),
                cal.get(java.util.Calendar.DAY_OF_MONTH));
        datePicker.getDatePicker().setMaxDate(new Date().getTime());
        datePicker.setCancelable(false);
        datePicker.show();
    }

    DatePickerDialog.OnDateSetListener ondate = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            java.util.Calendar calendar = java.util.Calendar.getInstance(TimeZone.getDefault());
            calendar.set(year, monthOfYear, dayOfMonth);
            calendar.set(Calendar.HOUR_OF_DAY, 00);
            calendar.set(Calendar.MINUTE, 00);
            calendar.set(Calendar.SECOND, 00);
            Date setdate = calendar.getTime();
            SimpleDateFormat fmtOut = new SimpleDateFormat("dd MMM, yyyy");
            String dateString = fmtOut.format(setdate);
            adddate.setText(dateString);
            SimpleDateFormat dateFrmOut = new SimpleDateFormat("yyyy-MM-dd");
            date = dateFrmOut.format(setdate);
        }
    };



    @OnClick(R.id.button3)
    public void searchStudent() {
        if (vehicle_id == null) {
            Utils.showSnackBar("Please select vehicle first", parent);
        } else {

            ErpProgress.showProgressBar(this, "Please wait...");
            HashMap<String, String> params = new HashMap<String, String>();
            params.put("vehicle_id", vehicle_id);
            if (date != null) {
                params.put("mdate", String.valueOf(date));
            }

            ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.EVENCHECKOUT_ATTENDBYVEHI_URL, studentapiCallback, this, params);
        }
    }

    ApiHandler.ApiCallback studentapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            if (error == null) {
                try {

                    JSONArray noticeArray = jsonObject.getJSONArray("attend_data");
                    dataList = AttendBean.parseAttendArray(noticeArray);
                    Toast.makeText(getApplicationContext(),dataList.get(0).getStudent_name(),Toast.LENGTH_LONG).show();

                    if (dataList.size() > 0) {
                        adapter.setNoticeList(dataList);
                        adapter.notifyDataSetChanged();
                        // txtNoData.setVisibility(View.GONE);
                    } else {
                        //txtNoData.setVisibility(View.VISIBLE);
                        adapter.setNoticeList(dataList);
                        adapter.notifyDataSetChanged();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                dataList.clear();
                adapter.setNoticeList(dataList);
                adapter.notifyDataSetChanged();
                Toast.makeText(getApplicationContext(),"no record found",Toast.LENGTH_LONG).show();
            }
        }
    };

    @Override
    public void editRouteCallback(AttendBean routedata, int pos) {

    }
}