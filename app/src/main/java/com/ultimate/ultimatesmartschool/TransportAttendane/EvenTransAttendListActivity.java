package com.ultimate.ultimatesmartschool.TransportAttendane;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.ultimate.ultimatesmartschool.BeanModule.User;
import com.ultimate.ultimatesmartschool.Login.LoginActivity;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.StudentAttendance.AttendBean;
import com.ultimate.ultimatesmartschool.Transport.AllotVicleToRouteMod.VehicleNoSpinnrAdap;
import com.ultimate.ultimatesmartschool.Transport.TransBeanMod.RouteBean;
import com.ultimate.ultimatesmartschool.Transport.TransBeanMod.VehicleBean;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class EvenTransAttendListActivity extends AppCompatActivity implements TransAttendAdapterVehiEven.CallBackMethodtrans{
    @BindView(R.id.parent)
    RelativeLayout parent;
    String classid = "";
    private int loaded = 0;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    List<String> classidsel;
    @BindView(R.id.textView7)TextView textView7;
    @BindView(R.id.spinnerroute)
    Spinner spinnerroute;
    @BindView(R.id.spinnerpickup)
    Spinner spinnerpickup;
    ArrayList<RouteBean> routeList = new ArrayList<>();
    ArrayList<VehicleBean> vehicleList = new ArrayList<>();
    String sectionid="";
    String sectionname;
    @BindView(R.id.adddate)TextView adddate;
    public String date;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.textNorecord)
    TextView txtNorecord;
    private ArrayList<AttendBean> dataList;
    // private GDSStudAttendAdapter adapter;
    private TransAttendAdapterVehiEven adapter;
    @BindView(R.id.btnSubmit)
    Button btnSubmit;
    @BindView(R.id.sc)
    ScrollView scrollView;
    RouteBean routeBean;
    VehicleBean vehicleBean;
    String vehicle_id,route_id;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_even_trans_attend_list);
        ButterKnife.bind(this);
        dataList = new ArrayList<>();

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new TransAttendAdapterVehiEven(this, dataList,this);
        recyclerView.setAdapter(adapter);
        //  fetchRouteList();
        fetchvehicleNoListnewupdt();
        getTodayDate();
        txtTitle.setText("Evening Check-In Report");
    }

    public void getTodayDate() {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.HOUR_OF_DAY, 00);
        c.set(Calendar.MINUTE, 00);
        c.set(Calendar.SECOND, 00);
        Date setdate = c.getTime();
        SimpleDateFormat fmtOut = new SimpleDateFormat("dd MMM, yyyy");
        String dateString = fmtOut.format(setdate);
        adddate.setText(dateString);
        SimpleDateFormat dateFrmOut = new SimpleDateFormat("yyyy-MM-dd");
        date = dateFrmOut.format(setdate);
    }
    @OnClick(R.id.imgBack)
    public void imgBack() {
        finish();
    }

    private void fetchvehicleNoListnewupdt() {

        HashMap<String, String> params = new HashMap<>();
        ApiHandler.apiHit(Request.Method.GET, Constants.getBaseURL() + Constants.VEHICLE_URL, apicallbacknewupdt, this, null);
    }

    ApiHandler.ApiCallback apicallbacknewupdt = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            if (error == null) {
                try {
                    vehicleList = VehicleBean.parseVehicleArray(jsonObject.getJSONArray("vehical_data"));
                    VehicleNoSpinnrAdap adap1 = new VehicleNoSpinnrAdap(EvenTransAttendListActivity.this, vehicleList);
                    spinnerpickup.setAdapter(adap1);
                    spinnerpickup.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                            if (i > 0) {
                                vehicleBean = vehicleList.get(i - 1);
                                vehicle_id=vehicleList.get(i-1).getEs_transportid();
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });
//                    int i = 0;
//                    for ( VehicleBean obj : vehicleList) {
//                        if (obj.getTr_vehicle_no().equals(alotVicleToRouteBean2.getV_no())) {
//                            spinnerpickup.setSelection(i + 1);
//                            break;
//                        }
//                        i++;
//                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    };


    @OnClick(R.id.filelayout)
    public void fetchDate() {
        java.util.Calendar cal = java.util.Calendar.getInstance(TimeZone.getDefault()); // Get current date
        DatePickerDialog datePicker = new DatePickerDialog(this,
                R.style.MyDatePickerDialogTheme, ondate,
                cal.get(java.util.Calendar.YEAR),
                cal.get(java.util.Calendar.MONTH),
                cal.get(java.util.Calendar.DAY_OF_MONTH));
        datePicker.getDatePicker().setMaxDate(new Date().getTime());
        datePicker.setCancelable(false);
        datePicker.show();
    }

    DatePickerDialog.OnDateSetListener ondate = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            java.util.Calendar calendar = java.util.Calendar.getInstance(TimeZone.getDefault());
            calendar.set(year, monthOfYear, dayOfMonth);
            calendar.set(Calendar.HOUR_OF_DAY, 00);
            calendar.set(Calendar.MINUTE, 00);
            calendar.set(Calendar.SECOND, 00);
            Date setdate = calendar.getTime();
            SimpleDateFormat fmtOut = new SimpleDateFormat("dd MMM, yyyy");
            String dateString = fmtOut.format(setdate);
            adddate.setText(dateString);
            SimpleDateFormat dateFrmOut = new SimpleDateFormat("yyyy-MM-dd");
            date = dateFrmOut.format(setdate);
        }
    };
    @OnClick(R.id.button3)
    public void searchStudent() {
        if (vehicle_id == null) {
            Utils.showSnackBar("Please select vehicle first", parent);
        } else {
//            dataList.clear();
//            adapter.notifyDataSetChanged();
            ErpProgress.showProgressBar(this, "Please wait...");
            HashMap<String, String> params = new HashMap<String, String>();
            params.put("vehicle_id", vehicle_id);
            Log.e("vehicvblid",vehicle_id);
            //  params.put("route_id",route_id);
            if (date != null) {
                params.put("mdate", String.valueOf(date));
            }
            Log.e("mdate",String.valueOf(date));
            Log.e("hobiii",Constants.getBaseURL() + Constants.GDSSTUDENT_ATTENDBYVEHI_URL);
            ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.EVENGDSSTUDENT_ATTENDBYVEHI_URL, studentapiCallback, this, params);
        }
    }

    ApiHandler.ApiCallback studentapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            if (error == null) {
                try {
                    ArrayList<AttendBean> studattendList = new ArrayList<>();
                    ArrayList<AttendBean> attendList = AttendBean.parseAttendArray(jsonObject.getJSONArray(Constants.ATTEND_DATA));
                    if (jsonObject.has("stud_data")) {
                        studattendList = AttendBean.parseAttendArray(jsonObject.getJSONArray("stud_data"));

                        if (studattendList.size() > attendList.size()) {
                            for (int i = 0; i < studattendList.size(); i++) {
                                boolean check = true;
                                for (int j = 0; j < attendList.size(); j++) {
                                    if (studattendList.get(i).getStudent_id().equals(attendList.get(j).getStudent_id())) {
                                        check = false;
                                        break;
                                    }
                                }
                                if (check) {
                                    attendList.add(studattendList.get(i));
                                }
                            }
                        } else {
                            for (int i = 0; i < attendList.size(); i++) {
                                boolean check = true;
                                for (int j = 0; j < studattendList.size(); j++) {
                                    if (studattendList.get(j).getStudent_id().equals(attendList.get(i).getStudent_id())) {
                                        check = false;
                                        break;
                                    }
                                }
                                if (check) {
                                    attendList.remove(i);
                                    i--;
                                }
                            }
                        }
                    }

                    dataList.clear();
                    dataList.addAll(attendList);
                    scrollView.setVisibility(View.VISIBLE);
                    txtNorecord.setVisibility(View.GONE);
                    adapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                scrollView.setVisibility(View.GONE);
                txtNorecord.setVisibility(View.VISIBLE);
                Utils.showSnackBar(error.getMessage(), parent);
            }
        }
    };


    @OnClick(R.id.btnSubmit)
    public void submitAttendance() {
        addAttend();
    }
    private void addAttend() {
        ErpProgress.showProgressBar(this,"Please wait...");
        dataList =adapter.getAttendList();
        ArrayList<String> attendList = new ArrayList<>();
        ArrayList<String> id = new ArrayList<>();
        ArrayList<String> st_id = new ArrayList<>();
        ArrayList<String> st_name = new ArrayList<>();
        for (int i = 0; i < dataList.size(); i++) {
            attendList.add(dataList.get(i).getAttendance());
            st_id.add(dataList.get(i).getStudent_id());
            id.add(dataList.get(i).getId());
            st_name.add(dataList.get(i).getStudent_name());
        }
        Log.e("list: ",attendList.toString().trim()+","+id.toString().trim()+","+st_id.toString().trim()+","+st_name.toString().trim());
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("attend", String.valueOf(attendList));
        params.put("st_id", String.valueOf(st_id));
        params.put("id", String.valueOf(id));
        params.put("st_name", String.valueOf(st_name));
        params.put("class_id", classid);
        params.put("date", date);
        params.put("section_id",sectionid);
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.ADD_ST_ATTEND_URLTRANSPORT, apiCallback, this, params);

    }

    ApiHandler.ApiCallback apiCallback = new ApiHandler.ApiCallback() {

        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            if (error == null) {
                try {
                    Toast.makeText(EvenTransAttendListActivity.this,jsonObject.getString("msg"), Toast.LENGTH_SHORT).show();
                    finish();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("error", error.getMessage() + "");
                Utils.showSnackBar(error.getMessage(), parent);
            }
        }
    };

    @Override
    public void editRouteCallback(AttendBean routedata, int pos) {

        HashMap<String, String> params = new HashMap<>();
        params.put("at_id", routedata.getId());
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.updatecheckouttransurleven, deletecallback, this, params);

    }

    ApiHandler.ApiCallback deletecallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            if (error == null) {
                try {
                    Utils.showSnackBar(jsonObject.getString("msg"), parent);
                    searchStudentsec(vehicle_id);
                    Log.e("hoooooo",vehicle_id);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("error", error.getMessage() + "");
                Utils.showSnackBar(error.getMessage(), parent);

                if (error.getStatusCode() == 405) {
                    User.logout();
                    startActivity(new Intent(EvenTransAttendListActivity.this, LoginActivity.class));
                    finish();
                }
            }
        }
    };

    public void searchStudentsec(String vehicle_id) {


        ErpProgress.showProgressBar(this, "Please wait...");
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("vehicle_id", vehicle_id);
        //  params.put("route_id",route_id);
        if (date != null) {
            params.put("mdate", String.valueOf(date));
        }
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.GDSSTUDENT_ATTENDBYVEHI_URL, studentapiCallbacksec, this, params);

    }

    ApiHandler.ApiCallback studentapiCallbacksec = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            if (error == null) {
                try {
                    ArrayList<AttendBean> studattendList = new ArrayList<>();
                    ArrayList<AttendBean> attendList = AttendBean.parseAttendArray(jsonObject.getJSONArray(Constants.ATTEND_DATA));
                    if (jsonObject.has("stud_data")) {
                        studattendList = AttendBean.parseAttendArray(jsonObject.getJSONArray("stud_data"));
                        if (studattendList.size() > attendList.size()) {
                            for (int i = 0; i < studattendList.size(); i++) {
                                boolean check = true;
                                for (int j = 0; j < attendList.size(); j++) {
                                    if (studattendList.get(i).getStudent_id().equals(attendList.get(j).getStudent_id())) {
                                        check = false;
                                        break;
                                    }
                                }
                                if (check) {
                                    attendList.add(studattendList.get(i));
                                }
                            }
                        } else {
                            for (int i = 0; i < attendList.size(); i++) {
                                boolean check = true;
                                for (int j = 0; j < studattendList.size(); j++) {
                                    if (studattendList.get(j).getStudent_id().equals(attendList.get(i).getStudent_id())) {
                                        check = false;
                                        break;
                                    }
                                }
                                if (check) {
                                    attendList.remove(i);
                                    i--;
                                }
                            }
                        }
                    }

                    dataList.clear();
                    dataList.addAll(attendList);
                    scrollView.setVisibility(View.VISIBLE);
                    txtNorecord.setVisibility(View.GONE);
                    adapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                scrollView.setVisibility(View.GONE);
                txtNorecord.setVisibility(View.VISIBLE);
                Utils.showSnackBar(error.getMessage(), parent);
            }
        }
    };
}