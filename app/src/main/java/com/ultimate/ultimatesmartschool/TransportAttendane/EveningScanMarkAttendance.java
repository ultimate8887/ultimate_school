package com.ultimate.ultimatesmartschool.TransportAttendane;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.google.zxing.Result;
import com.labters.lottiealertdialoglibrary.LottieAlertDialog;
import com.ultimate.ultimatesmartschool.BeanModule.User;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.StudentAttendance.AttendBean;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;


public class EveningScanMarkAttendance extends AppCompatActivity implements TransAttendAdapterVehi.CallBackMethodtrans{
    @BindView(R.id.lightButton)
    ImageView flashImageView;
    @BindView(R.id.parent)
    LinearLayout parent;
    //@BindView(R.id.imgBack)ImageView imgBack;
    public String date,classid,sectionid,routeid,routename,pickupid,pickupname,vehicleid,vehiclename,driverid,drivername;
    //Variables
    Intent i;
    // HistoryORM h = new HistoryORM();

    private boolean flashState = false;
    private ArrayList<AttendBean> dataList;
    TextView name,fname,vname,vnum,rname,pname,rid,pid,vid,dname,drivrmobile,did;;
    Dialog dialog;
    Button webSearch;
    public TransAttendAdapterVehi adapter;

    LottieAlertDialog alertDialog;

    String[] genderList = {
            "Right Student",
            "Wrong Student",
    };
    String gender = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_evening_scan_mark_attendance);

        ButterKnife.bind(this);
        dataList = new ArrayList<>();



        adapter = new TransAttendAdapterVehi(this, dataList,this);
        ActivityCompat.requestPermissions(EveningScanMarkAttendance.this,
                new String[]{Manifest.permission.CAMERA},
                1);

        ViewGroup contentFrame = (ViewGroup) findViewById(R.id.content_frame);
//        mScannerView = new ZXingScannerView(EveningScanMarkAttendance.this);
//        contentFrame.addView(mScannerView);

        java.util.Calendar calendar = java.util.Calendar.getInstance(TimeZone.getDefault());
        Date setdate = calendar.getTime();
        SimpleDateFormat fmtOut = new SimpleDateFormat("dd MMM, yyyy");
        String dateString = fmtOut.format(setdate);
        //adddate.setText(dateString);
        SimpleDateFormat dateFrmOut = new SimpleDateFormat("yyyy-MM-dd");
        date = dateFrmOut.format(setdate);

//        flashImageView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                mScannerView.setFlash(true);
//                if(flashState==false) {
//                    v.setBackgroundResource(R.drawable.ic_flash_off);
//                    Toast.makeText(getApplicationContext(), "Flashlight turned on", Toast.LENGTH_SHORT).show();
//                    mScannerView.setFlash(true);
//                    flashState = true;
//                }else if(flashState) {
//                    v.setBackgroundResource(R.drawable.ic_flash_on);
//                    Toast.makeText(getApplicationContext(), "Flashlight turned off", Toast.LENGTH_SHORT).show();
//                    mScannerView.setFlash(false);
//                    flashState = false;
//                }
//            }
//        });
    }

    @Override
    public void editRouteCallback(AttendBean routedata, int pos) {

    }
//
//    @Override
//    public void handleResult(Result rawResult) {
//        searchStudent(rawResult.getText());
//        dialog = new Dialog(this);
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialog.setCancelable(true);
//        dialog.setContentView(R.layout.custom_dialogqrscansec);
//        View v = dialog.getWindow().getDecorView();
//        v.setBackgroundResource(android.R.color.transparent);
//        Spinner spinnerGender = (Spinner) dialog.findViewById(R.id.spinnerGender);
//
//        ArrayAdapter<String> genderAdapter = new ArrayAdapter<String>(this, android.
//                R.layout.simple_spinner_dropdown_item, genderList);
//        spinnerGender.setAdapter(genderAdapter);
//        spinnerGender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
//                gender = genderList[i];
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> adapterView) {
//
//            }
//        });
//
//        TextView text = (TextView) dialog.findViewById(R.id.someText);
//        text.setText(rawResult.getText());
//        ImageView img = (ImageView) dialog.findViewById(R.id.imgOfDialog);
//        img.setImageResource(R.drawable.ic_done_gr);
//        name = (TextView) dialog.findViewById(R.id.name);
//        fname = (TextView) dialog.findViewById(R.id.fname);
//        vname=(TextView) dialog.findViewById(R.id.vname);
//        vnum=(TextView) dialog.findViewById(R.id.vnum);
//        rname=(TextView)dialog.findViewById(R.id.rname);
//        pname=(TextView)dialog.findViewById(R.id.pname);
//        rid=(TextView)dialog.findViewById(R.id.rid);
//        pid=(TextView)dialog.findViewById(R.id.pid);
//        vid=(TextView)dialog.findViewById(R.id.vid);
//        did=(TextView)dialog.findViewById(R.id.did);
//        dname=(TextView)dialog.findViewById(R.id.drivrname);
//        drivrmobile=(TextView)dialog.findViewById(R.id.drivrmobile);
//        webSearch = (Button) dialog.findViewById(R.id.searchButton);
//
//        webSearch.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//
//                addAttend();
//                dialog.dismiss();
//                mScannerView.resumeCameraPreview(EveningScanMarkAttendance.this);
//            }
//        });
//
//
//        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
//            @Override
//            public void onCancel(DialogInterface dialog) {
//                mScannerView.resumeCameraPreview(EveningScanMarkAttendance.this);
//            }
//        });
//        dialog.show();
//    }

//
//    @Override
//    public void onResume() {
//        super.onResume();
//        mScannerView.setResultHandler(this);
//        mScannerView.startCamera();
//    }
//
//    @Override
//    public void onPause() {
//        super.onPause();
//        mScannerView.stopCamera();
//    }
//

    public void searchStudent(String student_id) {

        ErpProgress.showProgressBar(this,"Please wait...");
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("stu_id",student_id);
        if (date != null) {
            params.put("mdate", String.valueOf(date));
        }
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.GDSSTUDENT_ATTEND_URLTRANSEVE, studentapiCallback, this, params);

    }

    ApiHandler.ApiCallback studentapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            // alertDialog.dismiss();
            ErpProgress.cancelProgressBar();
            if (error == null) {
                try {
                    ArrayList<AttendBean> studattendList = new ArrayList<>();
                    ArrayList<AttendBean> attendList = AttendBean.parseAttendArray(jsonObject.getJSONArray(Constants.ATTEND_DATA));
                    classid=attendList.get(0).getClass_id();
                    sectionid=attendList.get(0).getSection_id();
                    routename=attendList.get(0).getRoute_name();
                    routeid=attendList.get(0).getRoute_id();
                    pickupid=attendList.get(0).getPickupid();
                    pickupname=attendList.get(0).getPickupname();
                    vehicleid=attendList.get(0).getEvening_vehicle_id();
                    vehiclename=attendList.get(0).getTr_vehicle_no_eveni();
                    driverid= User.getCurrentUser().getId();
                    drivername=User.getCurrentUser().getFirstname()+User.getCurrentUser().getLastname();
                    name.setText("Name: "+attendList.get(0).getStudent_name()+"("+attendList.get(0).getClass_name()+")"+attendList.get(0).getSection_name());
                    fname.setText("Father's Name: "+attendList.get(0).getFather_name());
                    vname.setText("Vehicle Name: "+attendList.get(0).getTr_transport_name_eveni());
                    vnum.setText("Vehicle Num: "+attendList.get(0).getTr_vehicle_no_eveni());
                    rname.setText("Route Name: "+attendList.get(0).getRoute_name());
                    pname.setText("Pick-up Point: "+attendList.get(0).getPickupname());
                    dname.setText("Driver Name:"+attendList.get(0).getEvendriver_name()+"("+attendList.get(0).getEvendriver_shift()+")");
                    drivrmobile.setText("Driver Mobile"+attendList.get(0).getEvendiver_mobile());
                    vid.setText(attendList.get(0).getEvening_vehicle_id());
                    rid.setText(attendList.get(0).getRoute_id());
                    pid.setText(attendList.get(0).getPickupid());
                    did.setText(attendList.get(0).getDriver_id());
                    webSearch.setVisibility(View.VISIBLE);
                    if (jsonObject.has("stud_data")) {
                        studattendList = AttendBean.parseAttendArray(jsonObject.getJSONArray("stud_data"));

                        if (studattendList.size() > attendList.size()) {
                            for (int i = 0; i < studattendList.size(); i++) {
                                boolean check = true;
                                for (int j = 0; j < attendList.size(); j++) {
                                    if (studattendList.get(i).getStudent_id().equals(attendList.get(j).getStudent_id())) {
                                        check = false;
                                        break;
                                    }
                                }
                                if (check) {
                                    attendList.add(studattendList.get(i));
                                }
                            }
                        } else {
                            for (int i = 0; i < attendList.size(); i++) {
                                boolean check = true;
                                for (int j = 0; j < studattendList.size(); j++) {
                                    if (studattendList.get(j).getStudent_id().equals(attendList.get(i).getStudent_id())) {
                                        check = false;
                                        break;
                                    }
                                }
                                if (check) {
                                    attendList.remove(i);
                                    i--;
                                }
                            }
                        }
                    }

                    dataList.clear();
                    dataList.addAll(attendList);
//                    scrollView.setVisibility(View.VISIBLE);
//                    txtNorecord.setVisibility(View.GONE);
//                    adapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
//                scrollView.setVisibility(View.GONE);
//                txtNorecord.setVisibility(View.VISIBLE);
                Utils.showSnackBar(error.getMessage(), parent);
            }
        }
    };


    private void addAttend() {

        AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
        builder1.setMessage("Are you sure you want to submit? (Before Submit! Please check Student Bus Details) ");
        builder1.setCancelable(false);
        builder1.setPositiveButton(
                "Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int k) {

                        ErpProgress.showProgressBar(EveningScanMarkAttendance.this,"Please wait...");
                        dataList =adapter.getAttendList();
                        ArrayList<String> attendList = new ArrayList<>();
                        ArrayList<String> id = new ArrayList<>();
                        ArrayList<String> st_id = new ArrayList<>();
                        ArrayList<String> st_name = new ArrayList<>();
                        for (int i = 0; i < dataList.size(); i++) {
                            attendList.add(dataList.get(i).getAttendance());
                            st_id.add(dataList.get(i).getStudent_id());
                            id.add(dataList.get(i).getId());
                            st_name.add(dataList.get(i).getStudent_name());
                        }
                        Log.e("list: ",attendList.toString().trim()+","+id.toString().trim()+","+st_id.toString().trim()+","+st_name.toString().trim());
                        HashMap<String, String> params = new HashMap<String, String>();
                        params.put("attend", String.valueOf(attendList));
                        params.put("st_id", String.valueOf(st_id));
                        params.put("id", String.valueOf(id));
                        params.put("st_name", String.valueOf(st_name));
                        params.put("class_id", classid);
                        params.put("date", date);
                        params.put("section_id",sectionid);
                        // params.put("routeid",rid.getText().toString());
                        // params.put("pickupid",pid.getText().toString());
                        params.put("routename",rname.getText().toString());
                        params.put("pickupname",pname.getText().toString());
                        params.put("vehicleid",vid.getText().toString());
                        params.put("vehiclename",vname.getText().toString());
                        params.put("attendmarkby",User.getCurrentUser().getFirstname()+" "+User.getCurrentUser().getLastname());
                        params.put("drivername",dname.getText().toString());
                        params.put("driverid",did.getText().toString());
                        params.put("driverphone",drivrmobile.getText().toString());
                        params.put("user_id",User.getCurrentUser().getId());
                        params.put("stutype", gender);
                        Log.e("urlfoattentrans", Constants.getBaseURL() + Constants.ADD_ST_ATTEND_URLTRANSPORT);
                        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.ADD_ST_ATTEND_URLTRANSPORTEVE, apiCallback, EveningScanMarkAttendance.this, params);


                    }


                });
        builder1.setNegativeButton(
                "No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
        AlertDialog alert11 = builder1.create();
        alert11.show();


    }

    ApiHandler.ApiCallback apiCallback = new ApiHandler.ApiCallback() {

        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            if (error == null) {
                Log.e("error1", "error1");
                try {
                    Toast.makeText(EveningScanMarkAttendance.this,jsonObject.getString("msg"), Toast.LENGTH_SHORT).show();
                    finish();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("error11", "error11");
                Log.e("error", error.getMessage() + "");
                Utils.showSnackBar("student have already marked attendance ", parent);
            }
        }
    };
}