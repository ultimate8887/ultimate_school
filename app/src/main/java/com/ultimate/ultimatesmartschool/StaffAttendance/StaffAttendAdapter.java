package com.ultimate.ultimatesmartschool.StaffAttendance;

import android.content.Context;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.github.siyamed.shapeimageview.CircularImageView;
import com.squareup.picasso.Picasso;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class StaffAttendAdapter extends RecyclerView.Adapter<StaffAttendAdapter.MyViewHolder> {

    private Context mContext;
    ArrayList<StaffAttendBean> dataList;
    String[] list = {
            "P",
            "A",
            "L",
    };

    String[] remarklist = {
            "CL",
            "HL",
            "OTL",
            "SAL"
    };

    public StaffAttendAdapter(Context mContext, ArrayList<StaffAttendBean> dataList) {
        this.mContext = mContext;
        this.dataList = dataList;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.staff_attend_lyt, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final StaffAttendBean data = dataList.get(position);


        if (data.getStaff_name() != null) {
            String title = getColoredSpanned("Name:", "#000000");
            String Name = getColoredSpanned(data.getStaff_name(), "#383737");
            holder.txtName.setText(Html.fromHtml(title + " " + Name));
        }
        else {
            holder.txtName.setText("Name: Not Mentioned");
        }

        if (data.getStaff_id() != null) {
            String title = getColoredSpanned("ID:", "#000000");
            String Name = getColoredSpanned(data.getStaff_id(), "#383737");
            holder.txtRegNo.setText(Html.fromHtml(title + " " + Name));
        }
        else {
            holder.txtRegNo.setText("ID: Not Mentioned");
        }

        //  holder.employee_designation.setText("Designation: " + " " + viewstafflist.get(position).getDesignation());
        if (data.getStaff_desg_name() != null) {
            String title = getColoredSpanned("Designation:", "#000000");
            String Name = getColoredSpanned(data.getStaff_desg_name(), "#383737");
            holder.txtDesg.setText(Html.fromHtml(title + " " + Name));
        }
        else {
            holder.txtDesg.setText("ID: Not Mentioned");
        }

//        holder.txtName.setText(Utils.stringWithFirstcap(data.getStaff_name()));
//        holder.txtDesg.setText(Utils.stringWithFirstcap(data.getStaff_desg_name()));

        if (dataList.get(position).getGender().equalsIgnoreCase("Male")) {
            if (dataList.get(position).getProfile() != null) {
                Picasso.get().load(dataList.get(position).getProfile()).placeholder(R.drawable.boy).into(holder.circleimg);
            } else {
                Picasso.get().load(R.drawable.boy).into(holder.circleimg);
            }
        }else{
            if (dataList.get(position).getProfile() != null) {
                Picasso.get().load(dataList.get(position).getProfile()).placeholder(R.drawable.female_teacher).into(holder.circleimg);
            } else {
                Picasso.get().load(R.drawable.female_teacher).into(holder.circleimg);
            }
        }



        Log.e("getattendremark",data.getAttend_remark());

        if (!data.getId().equalsIgnoreCase("0")) {

            if(data.getAttendance().equalsIgnoreCase("a")){
                holder.lytAll.setBackground(mContext.getResources().getDrawable(R.drawable.ab_student_bg));
            }else if(data.getAttendance().equalsIgnoreCase("p")){

                holder.lytAll.setBackground(mContext.getResources().getDrawable(R.drawable.pr_student_bg));
            }else if(data.getAttendance().equalsIgnoreCase("l")){

                holder.lytAll.setBackground(mContext.getResources().getDrawable(R.drawable.le_student_bg));
            }else {
                holder.lytAll.setBackground(mContext.getResources().getDrawable(R.drawable.pr_student_bg));
            }

            for (int i = 0; i < list.length; i++) {
                if (data.getAttendance().equalsIgnoreCase(list[i])) {
                    holder.spinnerAttend.setSelection(i);
                    if (i == list.length - 1) {

                        for (int j = 0; j < remarklist.length; j++) {
                            if (data.getAttend_remark().equalsIgnoreCase(remarklist[j])) {
                                holder.spinnerRemark.setSelection(j);
                                break;
                            }
                        }
                        holder.spinnerRemark.setVisibility(View.VISIBLE);
                    } else {
                        holder.spinnerRemark.setVisibility(View.GONE);
                    }
                    break;
                }
            }

        } else {
            holder.lytAll.setBackground(mContext.getResources().getDrawable(R.drawable.pr_student_bg));
            holder.spinnerAttend.setSelection(0);
            for (int i = 0; i < list.length; i++) {
                if (data.getAttendance().equalsIgnoreCase(list[i])) {
                    holder.spinnerAttend.setSelection(i);
                    // holder.txtRegNo.setText(i);
                    Log.e("whats what","aaa");
                    break;
                }
            }
        }

        holder.spinnerAttend.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                data.setAttendance(list[i]);

                if(list[i].equalsIgnoreCase("a")){
                    holder.lytAll.setBackground(mContext.getResources().getDrawable(R.drawable.ab_student_bg));
                }else if(list[i].equalsIgnoreCase("p")){

                    holder.lytAll.setBackground(mContext.getResources().getDrawable(R.drawable.pr_student_bg));
                }else if(list[i].equalsIgnoreCase("l")){

                    holder.lytAll.setBackground(mContext.getResources().getDrawable(R.drawable.le_student_bg));
                }else {
                    holder.lytAll.setBackground(mContext.getResources().getDrawable(R.drawable.pr_student_bg));
                }


                if (i == list.length - 1) {
                    holder.spinnerRemark.setVisibility(View.VISIBLE);
                } else {
                    holder.spinnerRemark.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        holder.spinnerRemark.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                data.setAttend_remark(remarklist[i]);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }

    @Override
    public int getItemCount() {
        return (dataList.size());
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txtDesg)
        TextView txtDesg;
        @BindView(R.id.txtName)
        TextView txtName;
        @BindView(R.id.spinnerAttend)
        Spinner spinnerAttend;
        @BindView(R.id.spinnerRemark)
        Spinner spinnerRemark;
        @BindView(R.id.txtRegNo)
        TextView txtRegNo;
        @BindView(R.id.lytAll)
        RelativeLayout lytAll;
        @BindView(R.id.circleimg)
        CircularImageView circleimg;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(mContext,
                    R.layout.lyt_spinner_dropdown_item, list);
            spinnerAttend.setAdapter(adapter);
            ArrayAdapter<String> adapterl = new ArrayAdapter<String>(mContext,
                    R.layout.lyt_spinner_dropdown_item, remarklist);
            spinnerRemark.setAdapter(adapterl);
        }
    }


}
