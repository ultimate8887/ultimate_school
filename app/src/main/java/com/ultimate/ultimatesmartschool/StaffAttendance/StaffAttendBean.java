package com.ultimate.ultimatesmartschool.StaffAttendance;

import com.ultimate.ultimatesmartschool.Utility.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class StaffAttendBean {

    private static String ID = "id";
    private static String STAFF_ID = "staff_id";
    private static String STAFF_NAME = "staff_name";
    private static String STAFF_DESG = "staff_desg";
    private static String STAFF_DEPART = "staff_department";
    private static String ATTENDANCE = "attendance";
    private static String REMARK = "attend_remark";
    private static String STAFF_DESG_NAME = "staff_desg_name";
    private static String AT_STAFF_Remark = "at_staff_remarks";
    private static String PROFILE = "profile";
    /**
     * id : 5
     * staff_id : 1
     * staff_name : monika
     * staff_desg : 1
     * staff_department : 1
     * attendance : P
     * attend_remark :
     * staff_desg_name : ANDROID DEV
     */

    private String id;
    private String staff_id;
    private String staff_name;
    private String staff_desg;
    private String staff_department;
    private String attendance="P";
    private String attend_remark="";
    private String at_staff_remarks;
    private String checkin_time;

    private String profile;

    public String getAt_status() {
        return at_status;
    }

    public void setAt_status(String at_status) {
        this.at_status = at_status;
    }

    public String getCheckout_time() {
        return checkout_time;
    }

    public void setCheckout_time(String checkout_time) {
        this.checkout_time = checkout_time;
    }

    private String at_status;
    private String checkout_time;


    public String getAt_attendance_date() {
        return at_attendance_date;
    }

    public void setAt_attendance_date(String at_attendance_date) {
        this.at_attendance_date = at_attendance_date;
    }

    private String at_attendance_date;

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getFather_name() {
        return father_name;
    }

    public void setFather_name(String father_name) {
        this.father_name = father_name;
    }

    public String getTeach_nonteach() {
        return teach_nonteach;
    }

    public void setTeach_nonteach(String teach_nonteach) {
        this.teach_nonteach = teach_nonteach;
    }

    private String gender;
    private String father_name;
    private String teach_nonteach;

    public String getCheckin_time() {
        return checkin_time;
    }

    public void setCheckin_time(String checkin_time) {
        this.checkin_time = checkin_time;
    }

    public String getAt_staff_remarks() {
        return at_staff_remarks;
    }

    public void setAt_staff_remarks(String at_staff_remarks) {
        this.at_staff_remarks = at_staff_remarks;
    }

    private String staff_desg_name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStaff_id() {
        return staff_id;
    }

    public void setStaff_id(String staff_id) {
        this.staff_id = staff_id;
    }

    public String getStaff_name() {
        return staff_name;
    }

    public void setStaff_name(String staff_name) {
        this.staff_name = staff_name;
    }

    public String getStaff_desg() {
        return staff_desg;
    }

    public void setStaff_desg(String staff_desg) {
        this.staff_desg = staff_desg;
    }

    public String getStaff_department() {
        return staff_department;
    }

    public void setStaff_department(String staff_department) {
        this.staff_department = staff_department;
    }

    public String getAttendance() {
        return attendance;
    }

    public void setAttendance(String attendance) {
        this.attendance = attendance;
    }

    public String getAttend_remark() {
        return attend_remark;
    }

    public void setAttend_remark(String attend_remark) {
        this.attend_remark = attend_remark;
    }

    public static ArrayList<StaffAttendBean> parseAttendArray(JSONArray arrayObj) {
        ArrayList list = new ArrayList<StaffAttendBean>();
        try {
            for (int i = 0; i < arrayObj.length(); i++) {
                StaffAttendBean p = parseAttendObject(arrayObj.getJSONObject(i));
                if (p != null) {
                    list.add(p);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public static StaffAttendBean parseAttendObject(JSONObject jsonObject) {
        StaffAttendBean casteObj = new StaffAttendBean();
        try {
            if (jsonObject.has(ID)) {
                casteObj.setId(jsonObject.getString(ID));
            }
            if (jsonObject.has(STAFF_ID) && !jsonObject.getString(STAFF_ID).isEmpty() && !jsonObject.getString(STAFF_ID).equalsIgnoreCase("null")) {
                casteObj.setStaff_id(jsonObject.getString(STAFF_ID));
            }
            if (jsonObject.has(STAFF_NAME) && !jsonObject.getString(STAFF_NAME).isEmpty() && !jsonObject.getString(STAFF_NAME).equalsIgnoreCase("null")) {
                casteObj.setStaff_name(jsonObject.getString(STAFF_NAME));
            }
            if (jsonObject.has(STAFF_DEPART) && !jsonObject.getString(STAFF_DEPART).isEmpty() && !jsonObject.getString(STAFF_DEPART).equalsIgnoreCase("null")) {
                casteObj.setStaff_department(jsonObject.getString(STAFF_DEPART));
            }
            if (jsonObject.has(STAFF_DESG) && !jsonObject.getString(STAFF_DESG).isEmpty() && !jsonObject.getString(STAFF_DESG).equalsIgnoreCase("null")) {
                casteObj.setStaff_desg(jsonObject.getString(STAFF_DESG));
            }
            if (jsonObject.has(STAFF_DESG_NAME) && !jsonObject.getString(STAFF_DESG_NAME).isEmpty() && !jsonObject.getString(STAFF_DESG_NAME).equalsIgnoreCase("null")) {
                casteObj.setStaff_desg_name(jsonObject.getString(STAFF_DESG_NAME));
            }
            if (jsonObject.has(REMARK) && !jsonObject.getString(REMARK).isEmpty() && !jsonObject.getString(REMARK).equalsIgnoreCase("null")) {
                casteObj.setAttend_remark(jsonObject.getString(REMARK));
            }
            if (jsonObject.has(ATTENDANCE) && !jsonObject.getString(ATTENDANCE).isEmpty() && !jsonObject.getString(ATTENDANCE).equalsIgnoreCase("null")) {
                casteObj.setAttendance(jsonObject.getString(ATTENDANCE));
            }
            if (jsonObject.has(AT_STAFF_Remark) && !jsonObject.getString(AT_STAFF_Remark).isEmpty() && !jsonObject.getString(AT_STAFF_Remark).equalsIgnoreCase("null")) {
                casteObj.setAt_staff_remarks(jsonObject.getString(AT_STAFF_Remark));
            }

            if (jsonObject.has("father_name") && !jsonObject.getString("father_name").isEmpty() && !jsonObject.getString("father_name").equalsIgnoreCase("null")) {
                casteObj.setFather_name(jsonObject.getString("father_name"));
            }
            if (jsonObject.has("teach_nonteach") && !jsonObject.getString("teach_nonteach").isEmpty() && !jsonObject.getString("teach_nonteach").equalsIgnoreCase("null")) {
                casteObj.setTeach_nonteach(jsonObject.getString("teach_nonteach"));
            }

            if (jsonObject.has("gender") && !jsonObject.getString("gender").isEmpty() && !jsonObject.getString("gender").equalsIgnoreCase("null")) {
                casteObj.setGender(jsonObject.getString("gender"));
            }

            if (jsonObject.has(PROFILE) && !jsonObject.getString(PROFILE).isEmpty() && !jsonObject.getString(PROFILE).equalsIgnoreCase("null")) {
                casteObj.setProfile(Constants.getImageBaseURL()+jsonObject.getString(PROFILE));
            }

            if (jsonObject.has("at_attendance_date") && !jsonObject.getString("at_attendance_date").isEmpty() && !jsonObject.getString("at_attendance_date").equalsIgnoreCase("null")) {
                casteObj.setAt_attendance_date(jsonObject.getString("at_attendance_date"));
            }

            if (jsonObject.has("checkin_time") && !jsonObject.getString("checkin_time").isEmpty() && !jsonObject.getString("checkin_time").equalsIgnoreCase("null")) {
                casteObj.setCheckin_time(jsonObject.getString("checkin_time"));
            }
            if (jsonObject.has("checkout_time") && !jsonObject.getString("checkout_time").isEmpty() && !jsonObject.getString("checkout_time").equalsIgnoreCase("null")) {
                casteObj.setCheckout_time(jsonObject.getString("checkout_time"));
            }


            if (jsonObject.has("at_status") && !jsonObject.getString("at_status").isEmpty() && !jsonObject.getString("at_status").equalsIgnoreCase("null")) {
                casteObj.setAt_status(jsonObject.getString("at_status"));
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }
        return casteObj;
    }

    public String getStaff_desg_name() {
        return staff_desg_name;
    }

    public void setStaff_desg_name(String staff_desg_name) {
        this.staff_desg_name = staff_desg_name;
    }
}
