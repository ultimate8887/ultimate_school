package com.ultimate.ultimatesmartschool.StaffAttendance;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.ultimate.ultimatesmartschool.R;

import java.util.ArrayList;

public class StafflistspinAdapter extends BaseAdapter {
    ArrayList<Stafflistbean> stafflist;
    Context context;
    LayoutInflater inflter;


    public StafflistspinAdapter(Context context, ArrayList<Stafflistbean> stafflist) {
        this.context=context;
        this.stafflist=stafflist;
        inflter = (LayoutInflater.from(context));
    }


    @Override
    public int getCount() {
        return stafflist.size() + 1;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        view = inflter.inflate(R.layout.spinner_lyt, null);
        TextView label = (TextView) view.findViewById(R.id.txtText);
        if (i == 0) {
            // Default selected Spinner item
            label.setText("Staff Name");
        } else {
            Stafflistbean classObj = stafflist.get(i - 1);
            label.setText(classObj.getName());
        }
        return view;
    }
}
