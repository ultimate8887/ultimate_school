package com.ultimate.ultimatesmartschool.StaffAttendance;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Stafflistbean {
    private static String ID="id";
    private static String NAME="name";
    private static String DESIGNAMTION="designation";
    private static String DATEOFBIRTH="dateofbirth";
    private static String DATEOFJOINING="dateofjoining";
    private static String SALARY="salary";
    private static String POST="post";

    /**
     * id : 18
     * name : dk gk
     * designation : Developement
     * post : Android Dev
     * dateofbirth : 2018-06-26
     * dateofjoining : 2018-06-26
     * salary : 567
     */

    private String id;
    private String name;
    private String designation;
    private String post;
    private String dateofbirth;
    private String dateofjoining;
    private String salary;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getPost() {
        return post;
    }

    public void setPost(String post) {
        this.post = post;
    }

    public String getDateofbirth() {
        return dateofbirth;
    }

    public void setDateofbirth(String dateofbirth) {
        this.dateofbirth = dateofbirth;
    }

    public String getDateofjoining() {
        return dateofjoining;
    }

    public void setDateofjoining(String dateofjoining) {
        this.dateofjoining = dateofjoining;
    }

    public String getSalary() {
        return salary;
    }

    public void setSalary(String salary) {
        this.salary = salary;
    }

    public static ArrayList<Stafflistbean> parseCommonstaffArray(JSONArray staff_detaillist) {
        ArrayList<Stafflistbean> list = new ArrayList<Stafflistbean>();
        try {

            for (int i = 0; i < staff_detaillist.length(); i++) {
                Stafflistbean p = parsestaffObject(staff_detaillist.getJSONObject(i));
                if (p != null) {
                    list.add(p);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;

    }

    private static Stafflistbean parsestaffObject(JSONObject jsonObject) {
        Stafflistbean casteObj = new Stafflistbean();
        try {
            if (jsonObject.has(ID)) {
                casteObj.setId(jsonObject.getString(ID));
            }
            if (jsonObject.has(NAME) && !jsonObject.getString(NAME).isEmpty() && !jsonObject.getString(NAME).equalsIgnoreCase("null")) {
                casteObj.setName(jsonObject.getString(NAME));
            }

            if (jsonObject.has(DESIGNAMTION) && !jsonObject.getString(DESIGNAMTION).isEmpty() && !jsonObject.getString(DESIGNAMTION).equalsIgnoreCase("null")) {
                casteObj.setDesignation(jsonObject.getString(DESIGNAMTION));
            }
            if (jsonObject.has(DATEOFBIRTH) && !jsonObject.getString(DATEOFBIRTH).isEmpty() && !jsonObject.getString(DATEOFBIRTH).equalsIgnoreCase("null")) {
                casteObj.setDateofbirth(jsonObject.getString(DATEOFBIRTH));
            }

            if (jsonObject.has(DATEOFJOINING) && !jsonObject.getString(DATEOFJOINING).isEmpty() && !jsonObject.getString(DATEOFJOINING).equalsIgnoreCase("null")) {
                casteObj.setDateofjoining(jsonObject.getString(DATEOFJOINING));
            }
            if (jsonObject.has(SALARY) && !jsonObject.getString(SALARY).isEmpty() && !jsonObject.getString(SALARY).equalsIgnoreCase("null")) {
                casteObj.setSalary(jsonObject.getString(SALARY));
            } if (jsonObject.has(POST) && !jsonObject.getString(POST).isEmpty() && !jsonObject.getString(POST).equalsIgnoreCase("null")) {
                casteObj.setPost(jsonObject.getString(POST));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return casteObj;

    }


}
