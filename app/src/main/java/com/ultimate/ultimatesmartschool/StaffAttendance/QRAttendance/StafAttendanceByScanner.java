package com.ultimate.ultimatesmartschool.StaffAttendance.QRAttendance;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.google.zxing.ResultPoint;
import com.journeyapps.barcodescanner.BarcodeCallback;
import com.journeyapps.barcodescanner.BarcodeResult;
import com.journeyapps.barcodescanner.DecoratedBarcodeView;
import com.squareup.picasso.Picasso;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.StaffAttendance.StaffAttendAdapter;
import com.ultimate.ultimatesmartschool.StaffAttendance.StaffAttendBean;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.CommonProgress;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;


public class StafAttendanceByScanner extends AppCompatActivity {
    @BindView(R.id.lightButton)
    ImageView flashImageView;
    @BindView(R.id.parent)
    LinearLayout parent;
    //@BindView(R.id.imgBack)ImageView imgBack;
    public String date, departid, designationid;
    //Variables
    Intent i;
    CommonProgress commonProgress;
    // HistoryORM h = new HistoryORM();
    @BindView(R.id.barcode_scanner)
    DecoratedBarcodeView barcodeScannerView;
    private boolean flashState = false;
    private ArrayList<StaffAttendBean> dataList;
    TextView name, fname, text;
    Dialog dialog;
    CircularImageView img;
    private StaffAttendAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_staf_attendance_by_scanner);
        ButterKnife.bind(this);
        commonProgress = new CommonProgress(this);
        dataList = new ArrayList<>();
        adapter = new StaffAttendAdapter(this, dataList);

        ActivityCompat.requestPermissions(StafAttendanceByScanner.this,
                new String[]{Manifest.permission.CAMERA},
                1);
        ViewGroup contentFrame = (ViewGroup) findViewById(R.id.content_frame);

        java.util.Calendar calendar = java.util.Calendar.getInstance(TimeZone.getDefault());
        Date setdate = calendar.getTime();

        SimpleDateFormat fmtOut = new SimpleDateFormat("dd MMM, yyyy");
        String dateString = fmtOut.format(setdate);
        //adddate.setText(dateString);


        ImageView imgback = (ImageView) findViewById(R.id.imgBack);
        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        SimpleDateFormat dateFrmOut = new SimpleDateFormat("yyyy-MM-dd");
        date = dateFrmOut.format(setdate);
        setupBarcodeScanner();

    }


    private void setupBarcodeScanner() {
        barcodeScannerView.decodeContinuous(new BarcodeCallback() {
            @Override
            public void barcodeResult(BarcodeResult result) {
                if (result != null) {
                    handleResult(result.getText());
                    barcodeScannerView.pause();  // Stop scanning after getting the result
                }
            }

            @Override
            public void possibleResultPoints(List<ResultPoint> resultPoints) {
            }
        });
    }

    private void handleResult(String student_id) {
        searchStaff(student_id);
      //  Toast.makeText(getApplicationContext(), "QR Code Id: " + student_id, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onResume() {
        super.onResume();
        barcodeScannerView.resume();
    }

    @Override
    public void onPause() {
        super.onPause();
        barcodeScannerView.pause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        barcodeScannerView.pause();
    }


    public void searchStaff(String staf_id) {
        commonProgress.show();
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("staff_id", staf_id);
//            params.put("gate","gate");
        if (date != null) {
            params.put("mdate", String.valueOf(date));
        }
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.QRSTAFF_ATTEND_URLNEW, studentapiCallback, this, params);

    }

    ApiHandler.ApiCallback studentapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            //  ErpProgress.cancelProgressBar();
            commonProgress.dismiss();
            if (error == null) {
                try {
                    ArrayList<StaffAttendBean> studattendList = new ArrayList<>();
                    ArrayList<StaffAttendBean> attendList = StaffAttendBean.parseAttendArray(jsonObject.getJSONArray(Constants.ATTEND_DATA));
                    departid = attendList.get(0).getStaff_department();
                    designationid = attendList.get(0).getStaff_desg();
                    openDialoggg(attendList);
                    //   name.setText(attendList.get(0).getStaff_name()+"("+attendList.get(0).getStaff_department()+")"+attendList.get(0).getStaff_desg_name());
                    Log.e("getProfile", attendList.get(0).getProfile());


                    // fname.setText(attendList.get(0).getFather_name());
                    if (jsonObject.has("stud_data")) {
                        studattendList = StaffAttendBean.parseAttendArray(jsonObject.getJSONArray("stud_data"));
                        if (studattendList.size() > attendList.size()) {
                            for (int i = 0; i < studattendList.size(); i++) {
                                boolean check = true;
                                for (int j = 0; j < attendList.size(); j++) {
                                    if (studattendList.get(i).getStaff_id().equals(attendList.get(j).getStaff_id())) {
                                        check = false;
                                        break;
                                    }
                                }
                                if (check) {
                                    attendList.add(studattendList.get(i));
                                }
                            }
                        } else {
                            for (int i = 0; i < attendList.size(); i++) {
                                boolean check = true;
                                for (int j = 0; j < studattendList.size(); j++) {
                                    if (studattendList.get(j).getStaff_id().equals(attendList.get(i).getStaff_id())) {
                                        check = false;
                                        break;
                                    }
                                }
                                if (check) {
                                    attendList.remove(i);
                                    i--;
                                }
                            }
                        }
                    }

                    dataList.clear();
                    dataList.addAll(attendList);
//                    scrollView.setVisibility(View.VISIBLE);
//                    txtNorecord.setVisibility(View.GONE);
//                    adapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
//                scrollView.setVisibility(View.GONE);
//                txtNorecord.setVisibility(View.VISIBLE);
                Toast.makeText(getApplicationContext(), "Invalid QR Code! \nNo Staff Found", Toast.LENGTH_LONG).show();
                Utils.showSnackBar(error.getMessage(), parent);
                finish();
            }
        }
    };

    private void openDialoggg(ArrayList<StaffAttendBean> attendList) {

        dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.custom_dialogqrscan);
        View v = dialog.getWindow().getDecorView();
        v.setBackgroundResource(android.R.color.transparent);
        text = (TextView) dialog.findViewById(R.id.txtRegNo);
        // text.setText(rawResult.getText());
        img = (CircularImageView) dialog.findViewById(R.id.img);
        // img.setImageResource(R.drawable.ic_done_gr);
        name = (TextView) dialog.findViewById(R.id.nametxtone);
        fname = (TextView) dialog.findViewById(R.id.classtxtone);
        TextView h_txtRegNo = (TextView) dialog.findViewById(R.id.h_txtRegNo);
        h_txtRegNo.setText("Staff Id");
        TextView h_classtxtone = (TextView) dialog.findViewById(R.id.h_classtxtone);
        h_classtxtone.setText("Post");
        Button webSearch = (Button) dialog.findViewById(R.id.searchButton);

        webSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addAttend();
                barcodeScannerView.clearFocus();
                barcodeScannerView.resume();
            }
        });

        ImageView close = (ImageView) dialog.findViewById(R.id.closss);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
                //   dialog.dismiss();
            }
        });
        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                barcodeScannerView.resume();
            }
        });

        if (attendList.get(0).getGender().equalsIgnoreCase("male")) {
            name.setText(attendList.get(0).getStaff_name() + "\nS/O\n" + attendList.get(0).getFather_name());
            if (attendList.get(0).getProfile() != null) {
                Picasso.get().load(attendList.get(0).getProfile()).placeholder(R.drawable.stud).into(img);
            } else {
                Picasso.get().load(R.drawable.stud).into(img);
            }
        } else {
            name.setText(attendList.get(0).getStaff_name() + "\nS/O\n" + attendList.get(0).getFather_name());
            if (attendList.get(0).getProfile() != null) {
                Picasso.get().load(attendList.get(0).getProfile()).placeholder(R.drawable.f_student).into(img);
            } else {
                Picasso.get().load(R.drawable.f_student).into(img);
            }
        }

        fname.setText(attendList.get(0).getStaff_desg_name());
        text.setText(attendList.get(0).getStaff_id());

        dialog.show();
        Window window = dialog.getWindow();
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

    }


    private void addAttend() {
        commonProgress.show();

        ArrayList<String> attendList = new ArrayList<>();
        ArrayList<String> id = new ArrayList<>();
        ArrayList<String> st_id = new ArrayList<>();
        ArrayList<String> st_name = new ArrayList<>();
        ArrayList<String> st_des = new ArrayList<>();
        ArrayList<String> remark = new ArrayList<>();
        for (int i = 0; i < dataList.size(); i++) {
            attendList.add(dataList.get(i).getAttendance());
            st_id.add(dataList.get(i).getStaff_id());
            id.add(dataList.get(i).getId());
            st_name.add(dataList.get(i).getStaff_name());
            st_des.add(dataList.get(i).getStaff_desg());
            remark.add(dataList.get(i).getAttend_remark());
        }
        Log.e("list: ", attendList.toString().trim() + "," + id.toString().trim() + "," + st_id.toString().trim() + "," + st_name.toString().trim());
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("attend", String.valueOf(attendList));
        params.put("st_id", String.valueOf(st_id));
        params.put("id", String.valueOf(id));
        params.put("st_name", String.valueOf(st_name));
        params.put("depart_id", departid);
        params.put("date", date);
        params.put("desg_id", String.valueOf(st_des));
        params.put("remark", String.valueOf(remark));

        params.put("type", "gate");


        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.ADD_STAFF_ATTEND_URL, apiCallback, this, params);

    }

    ApiHandler.ApiCallback apiCallback = new ApiHandler.ApiCallback() {

        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            // ErpProgress.cancelProgressBar();
            commonProgress.dismiss();
            if (error == null) {
                try {
                    Toast.makeText(StafAttendanceByScanner.this, jsonObject.getString("msg"), Toast.LENGTH_SHORT).show();
                    dialog.dismiss();
                    finish();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                dialog.dismiss();
                Log.e("error", error.getMessage() + "");
                Utils.showSnackBar("Staff have already marked today attendance ", parent);
                finish();
            }
        }
    };
}