package com.ultimate.ultimatesmartschool.StaffAttendance;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.StudentAttendance.AttendMonthBean;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class StaffYearlyAttendanceAdapter extends RecyclerView.Adapter<StaffYearlyAttendanceAdapter.MyViewHolder> {

    private Context mContext;
    ArrayList<StaffMonthBean> dataList;
    int value=0;

    public StaffYearlyAttendanceAdapter(Context mContext, ArrayList<StaffMonthBean> dataList, int value) {
        this.mContext = mContext;
        this.dataList = dataList;
        this.value = value;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.table_view_lyt, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        if (position == 0) {
            holder.lytHeader.setVisibility(View.VISIBLE);
            holder.lytData.setVisibility(View.GONE);
        } else {
            holder.lytHeader.setVisibility(View.GONE);
            holder.lytData.setVisibility(View.VISIBLE);
            final StaffMonthBean data = dataList.get(position - 1);
            holder.txtName.setText(data.getStaff_name());
            holder.txtRoll.setText(data.getStaff_id());
            holder.txtFName.setText(data.getFather_name());
//            if(data.get)
            holder.txtAbsent.setText(data.getA());


            holder.txtLeave_hl.setText(data.getHl());
            holder.txtLeave_sal.setText(data.getSal());
            holder.txtLeave_otl.setText(data.getOtl());
            holder.txtLeave.setText(data.getCl());

//            Log.println(Log.ERROR,"Values","l"+String.valueOf(l)
//                    +"\nhl" +String.valueOf(hl)
//                    +"\notl" +String.valueOf(otl)
//                    +"\nsal" +String.valueOf(sal)
//                    +"\ntl" +String.valueOf(tl)
//                    +"\ncl" +String.valueOf(cl)
//                    +"\n");

            holder.txtPresent.setText(data.getP());
            holder.txtHoliday.setText(String.valueOf(value));
            holder.txtWorking.setText(data.getSch_wd());
            holder.txtAttendace.setText(data.getStd_wd());
        }

    }

    @Override
    public int getItemCount() {
        return (dataList.size() + 1);
    }


    public void setList(ArrayList<StaffMonthBean> dataList) {
        this.dataList = dataList;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txtAbsent)
        TextView txtAbsent;
        @BindView(R.id.txtLeave)
        TextView txtLeave;

        @BindView(R.id.txtLeave_hl)
        TextView txtLeave_hl;
        @BindView(R.id.txtLeave_otl)
        TextView txtLeave_otl;
        @BindView(R.id.txtLeave_sal)
        TextView txtLeave_sal;

        @BindView(R.id.txtPresent)
        TextView txtPresent;
        @BindView(R.id.txtName)
        TextView txtName;
        @BindView(R.id.txtRoll)
        TextView txtRoll;
        @BindView(R.id.lytData)
        LinearLayout lytData;
        @BindView(R.id.lytHeader)
        LinearLayout lytHeader;
        @BindView(R.id.txtFName)
        TextView txtFName;
        @BindView(R.id.txtAttend)
        TextView txtAttendace;

        @BindView(R.id.txtHoliday)
        TextView txtHoliday;

        @BindView(R.id.txtWorking)
        TextView txtWorking;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
