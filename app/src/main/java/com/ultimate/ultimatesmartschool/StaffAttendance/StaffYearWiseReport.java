package com.ultimate.ultimatesmartschool.StaffAttendance;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetSequence;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.ultimate.ultimatesmartschool.BeanModule.ClassBean;
import com.ultimate.ultimatesmartschool.BeanModule.CommonBean;
import com.ultimate.ultimatesmartschool.BeanModule.SectionBean;
import com.ultimate.ultimatesmartschool.BeanModule.SubjectBeancls;
import com.ultimate.ultimatesmartschool.BeanModule.User;
import com.ultimate.ultimatesmartschool.Homework.AdapterOfSubjectList;
import com.ultimate.ultimatesmartschool.Homework.SectionnewAdapter;
import com.ultimate.ultimatesmartschool.Login.LoginActivity;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.StudentAttendance.AttendMonthBean;
import com.ultimate.ultimatesmartschool.StudentAttendance.ClassAdapterAttend;
import com.ultimate.ultimatesmartschool.StudentAttendance.YearlyAttendanceAdapter;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.CommonProgress;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class StaffYearWiseReport extends AppCompatActivity {
    @BindView(R.id.export)
    FloatingActionButton export;
    View view;
    int selected = 2;
    private Date fromdate;
    private ArrayList<StaffMonthBean> monList;
    private StaffYearlyAttendanceAdapter madapter;
    private static final String TAG = "PdfCreatorActivity";
    final private int REQUEST_CODE_ASK_PERMISSIONS = 111;
    private File pdfFile,csvfilee;
    Calendar calendar;
    String remarks;
    String filePath;
    String month_name;

    @BindView(R.id.parent)
    RelativeLayout parent;
    @BindView(R.id.txtTitle)
    TextView txtTitle;

    SharedPreferences sharedPreferences;


    //@BindView(R.id.spinner) Spinner spinnerClass;
    BottomSheetDialog mBottomSheetDialog;
    private int loaded = 0;

    @BindView(R.id.txtSub)
    TextView txtSub;
    List<String> classidsel;
    @BindView(R.id.totalRecord)
    TextView totalRecord;

    @BindView(R.id.adddate)TextView adddate;
    public String date;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.textNorecord)
    TextView txtNorecord;
//    @BindView(R.id.btnSubmit)
//    Button btnSubmit;

    @BindView(R.id.dialog)
    ImageView dialog;
    @BindView(R.id.sc)
    CardView scrollView;
    @BindView(R.id.filelayout)
    LinearLayout filelayout;

    CommonProgress commonProgress;


    @RequiresApi(api = Build.VERSION_CODES.N)


    int check=0;

    Spinner spinnerDepartment;

    int abc;
    int sundayCount = 0,total_h=0;
    String holiday="";

    private ArrayList<CommonBean> departmentList;
    private String departmentid="",departmentname="";
    StaffAttendBean staffAttendBean;


    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_year_wise_report);
        ButterKnife.bind(this);
        monList = new ArrayList<>();
        commonProgress=new CommonProgress(this);
        classidsel = new ArrayList<>();

        if (!restorePrefData()){
            setShowcaseView();
        }else {
            openDialog();
        }
    }
    @RequiresApi(api = Build.VERSION_CODES.N)
    @OnClick(R.id.dialog)
    public void dialogggg() {
        openDialog();
    }

    @OnClick(R.id.imgBack)
    public void onBackclick() {
        commonBack();
    }

    @OnClick(R.id.filelayout)
    public void fetchMonth() {
       // dateDialog();
    }


    public void dateDialog() {
        java.util.Calendar cal = java.util.Calendar.getInstance(TimeZone.getDefault()); // Get current date
        DatePickerDialog datePicker = new DatePickerDialog(this,
                R.style.MyDatePickerDialogTheme, ondate,
                cal.get(java.util.Calendar.YEAR),
                cal.get(java.util.Calendar.MONTH),
                cal.get(java.util.Calendar.DAY_OF_MONTH));
        datePicker.getDatePicker().setMaxDate(new java.util.Date().getTime());
        datePicker.setCancelable(false);
        datePicker.show();
    }

    DatePickerDialog.OnDateSetListener ondate = new DatePickerDialog.OnDateSetListener() {
        @RequiresApi(api = Build.VERSION_CODES.O)
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            java.util.Calendar calendar = java.util.Calendar.getInstance(TimeZone.getDefault());
            calendar.set(year, monthOfYear, dayOfMonth);
            calendar.set(Calendar.HOUR_OF_DAY, 00);
            calendar.set(Calendar.MINUTE, 00);
            calendar.set(Calendar.SECOND, 00);
            java.util.Date setdate = calendar.getTime();
            if (selected == 2) {
                SimpleDateFormat fmtOut = new SimpleDateFormat("MMMM, yyyy");
                String dateString = fmtOut.format(setdate);
                adddate.setText(dateString);
            }

//            else {
//                SimpleDateFormat fmtOut = new SimpleDateFormat("MMMM, yyyy");
//                String dateString = fmtOut.format(setdate);
//                txtMonth.setText(dateString);
//            }

            SimpleDateFormat dateFrmOut = new SimpleDateFormat("yyyy-MM-dd");
            date = dateFrmOut.format(setdate);
            fromdate = new Date(calendar.getTimeInMillis());
            if (departmentid != null) {
                //fetchStudentList(date);
                fetchSunday(date);
            }
        }
    };

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void fetchSunday(String dateString) {
        // String dateString = "2024-04-01"; // Example date string

        sundayCount = 0;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate date = LocalDate.parse(dateString, formatter);

        LocalDate firstDayOfMonth = date.with(TemporalAdjusters.firstDayOfMonth());
        LocalDate lastDayOfMonth = date.with(TemporalAdjusters.lastDayOfMonth());

        for (LocalDate d = firstDayOfMonth; !d.isAfter(lastDayOfMonth); d = d.plusDays(1)) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                if (d.getDayOfWeek() == DayOfWeek.SUNDAY) {
                    sundayCount++;
                }
            }
        }
        // Toast.makeText(getApplicationContext(),"Sunday count"+String.valueOf(sundayCount),Toast.LENGTH_SHORT).show();
        fetchHoliday(dateString);

    }

    private void setShowcaseView_aday() {
        new TapTargetSequence(this)
                .targets(TapTarget.forView(filelayout,"Date Button!","Tap the Date button to View Month-Wise Attendance.")
                        .outerCircleColor(R.color.light)
                        .outerCircleAlpha(0.96f)
                        .targetCircleColor(R.color.white)
                        .titleTextSize(20)
                        .titleTextColor(R.color.white)
                        .descriptionTextSize(14)
                        .descriptionTextColor(R.color.black)
                        .textColor(R.color.black)
                        .textTypeface(Typeface.SANS_SERIF)
                        .dimColor(R.color.black)
                        .drawShadow(true)
                        .cancelable(false)
                        .tintTarget(true)
                        .transparentTarget(true)
                        .targetRadius(60)).listener(new TapTargetSequence.Listener() {
                    @RequiresApi(api = Build.VERSION_CODES.N)
                    @Override
                    public void onSequenceFinish() {
                        // Toast.makeText(getApplicationContext(),"Sequence Finished",Toast.LENGTH_SHORT).show();
                        savePrefData_aday();
                        //openDialog();
                    }

                    @Override
                    public void onSequenceStep(TapTarget lastTarget, boolean targetClicked) {
                        // Toast.makeText(getActivity(),"GREAT!",Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onSequenceCanceled(TapTarget lastTarget) {

                    }
                }).start();
    }

    private void savePrefData_aday(){
        sharedPreferences=this.getSharedPreferences("boarding_pref_add_attend_mday",MODE_PRIVATE);
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putBoolean("IsFirstTimeVisit_add_attend_mday",true);
        editor.apply();
    }

    private boolean restorePrefData_aday(){
        sharedPreferences = this.getSharedPreferences("boarding_pref_add_attend_mday",MODE_PRIVATE);
        return sharedPreferences.getBoolean("IsFirstTimeVisit_add_attend_mday",false);
    }


    private void setShowcaseView() {
        new TapTargetSequence(this)
                .targets(TapTarget.forView(dialog,"Search Button!","Tap the Search button to to view Department-Wise Attendance Report.")
                        .outerCircleColor(R.color.light)
                        .outerCircleAlpha(0.96f)
                        .targetCircleColor(R.color.white)
                        .titleTextSize(20)
                        .titleTextColor(R.color.white)
                        .descriptionTextSize(14)
                        .descriptionTextColor(R.color.black)
                        .textColor(R.color.black)
                        .textTypeface(Typeface.SANS_SERIF)
                        .dimColor(R.color.black)
                        .drawShadow(true)
                        .cancelable(false)
                        .tintTarget(true)
                        .transparentTarget(true)
                        .targetRadius(60)).listener(new TapTargetSequence.Listener() {
                    @RequiresApi(api = Build.VERSION_CODES.N)
                    @Override
                    public void onSequenceFinish() {
                        // Toast.makeText(getApplicationContext(),"Sequence Finished",Toast.LENGTH_SHORT).show();
                        savePrefData();
                        openDialog();
                    }

                    @Override
                    public void onSequenceStep(TapTarget lastTarget, boolean targetClicked) {
                        // Toast.makeText(getActivity(),"GREAT!",Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onSequenceCanceled(TapTarget lastTarget) {

                    }
                }).start();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        commonBack();
    }

    private void commonBack() {
        androidx.appcompat.app.AlertDialog.Builder builder1 = new androidx.appcompat.app.AlertDialog.Builder(StaffYearWiseReport.this);
        builder1.setMessage("Are you sure, you want to exit? ");
        builder1.setCancelable(false);
        builder1.setPositiveButton(
                "Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finish();
                    }
                });
        builder1.setNegativeButton(
                "No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

    private void savePrefData(){
        sharedPreferences=this.getSharedPreferences("boarding_pref_add_attend_msday",MODE_PRIVATE);
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putBoolean("IsFirstTimeVisit_add_attend_msday",true);
        editor.apply();
    }

    private boolean restorePrefData(){
        sharedPreferences = this.getSharedPreferences("boarding_pref_add_attend_msday",MODE_PRIVATE);
        return sharedPreferences.getBoolean("IsFirstTimeVisit_add_attend_msday",false);
    }


    @RequiresApi(api = Build.VERSION_CODES.N)
    private void openDialog() {


        mBottomSheetDialog = new BottomSheetDialog(this);
        final View sheetView = getLayoutInflater().inflate(R.layout.search_dialog, null);
        mBottomSheetDialog.setContentView(sheetView);
        mBottomSheetDialog.setCancelable(false);

        spinnerDepartment=(Spinner) sheetView.findViewById(R.id.spinnerClass);
        RelativeLayout spinnersection=(RelativeLayout) sheetView.findViewById(R.id.sectionlayout);
        spinnersection.setVisibility(View.GONE);
        TextView textView7=(TextView) sheetView.findViewById(R.id.textView7);
        textView7.setText("Select Department");
        TextView txtSetup=(TextView) sheetView.findViewById(R.id.txtSetup);
        txtSetup.setText("View Year-Wise \nAttendance");
        ImageView close=(ImageView) sheetView.findViewById(R.id.close);
        //txtTitle.setText("Mark Attendance");
        getTodayDate();
        fetchDepartment();

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //assignData();
                if (check==0) {
                    finish();
                    mBottomSheetDialog.dismiss();
                }else {
                    mBottomSheetDialog.dismiss();
                }
            }
        });


        Button btnYes= (Button) sheetView.findViewById(R.id.btnYes);


        btnYes.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onClick(View view) {
//                if (departmentid.equalsIgnoreCase(""))  {
//                    Toast.makeText(StaffYearWiseReport.this,"Kindly Select Department!", Toast.LENGTH_SHORT).show();
//                }  else {
                    check++;
//                    fetchStudentList(date);
                    setCommonData();
                // }

            }
        });
        mBottomSheetDialog.show();



    }

    public void getTodayDate() {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.HOUR_OF_DAY, 00);
        c.set(Calendar.MINUTE, 00);
        c.set(Calendar.SECOND, 00);
        java.util.Date setdate = c.getTime();
        //  SimpleDateFormat fmtOut = new SimpleDateFormat("dd MMM, yyyy");
        SimpleDateFormat fmtOut = new SimpleDateFormat("yyyy");
        String dateString = fmtOut.format(setdate);
        adddate.setText(dateString);
        SimpleDateFormat dateFrmOut = new SimpleDateFormat("yyyy-MM-dd");
        date = dateFrmOut.format(setdate);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void setCommonData() {
        filelayout.setVisibility(View.VISIBLE);
//        if (!restorePrefData_aday()){
//            setShowcaseView_aday();
//        }
        if (!departmentname.equalsIgnoreCase("")){
            txtTitle.setText(departmentname);
        }else{
            txtTitle.setText("All Department");
        }
        fetchSunday(date);
    }

    private void fetchHoliday(String dateString) {

        HashMap<String, String> params = new HashMap<>();
        params.put("user_id", User.getCurrentUser().getId());
        params.put("check", "fetch");
        params.put("h_date", dateString);

        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.UPDATE_SCHOOL_INFO, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error ) {
                commonProgress.dismiss();
                ErpProgress.cancelProgressBar();
                if (error == null) {
                    try {

                        holiday = jsonObject.getJSONObject(Constants.USERDATA).getString("holiday");
                        calculateSum(holiday,dateString);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    calculateSum("0",dateString);
                }
            }
        }, this, params);

    }

    private void calculateSum(String holiday,String date) {

        int hol= Integer.parseInt(holiday);
        total_h=sundayCount+hol;

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        madapter = new StaffYearlyAttendanceAdapter(this, monList,total_h);
        recyclerView.setAdapter(madapter);
        fetchStudentList(date);
    }

    private void fetchStudentList(String date) {
        commonProgress.show();
        HashMap<String, String> params = new HashMap<String, String>();
//        if(departmentid !=null){
//            params.put("d_id", departmentid);
//        }
//        else {
//            params.put("d_id", "0");
//
//        }

        if(!departmentid.equalsIgnoreCase("all")){
            params.put("d_id", departmentid);
            params.put("all", "0");
        }
        else {
            params.put("d_id", "0");
            params.put("all", "1");
        }

        if(abc==0){
            params.put("all", "1");
        }
        params.put("view_type", "year");
        params.put("mdate", String.valueOf(date));
        params.put("type", selected + "");
        Log.e("yyyy",Constants.getBaseURL() + Constants.STAFF_ATTEND_LIST);
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.STAFF_ATTEND_LIST, studentapiCallback, StaffYearWiseReport.this, params);

    }


    ApiHandler.ApiCallback studentapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            commonProgress.dismiss();
            mBottomSheetDialog.dismiss();
            if (error == null) {
                try {
//                    if(selected==2){
                    ArrayList<StaffMonthBean> attendList = StaffMonthBean.parseAttendArray(jsonObject.getJSONArray(Constants.ATTEND_DATA));
                    monList.clear();
                    monList.addAll(attendList);
                    madapter.notifyDataSetChanged();
                    totalRecord.setText("Total Entries:- "+String.valueOf(attendList.size()));
                    scrollView.setVisibility(View.VISIBLE);
                    //  }
                    export.setVisibility(View.VISIBLE);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                export.setVisibility(View.GONE);
                scrollView.setVisibility(View.GONE);
                totalRecord.setText("Total Entries:- 0");
                Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_LONG).show();
                // Utils.showSnackBar(error.getMessage(), view.findViewById(R.id.parent));
            }
        }
    };


    @SuppressLint("RestrictedApi")
    @OnClick(R.id.export)
    public void submit() {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.S) {
            importData();
        } else {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {

                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    Toast.makeText(this, "Please provide storage permission from app settings", Toast.LENGTH_LONG).show();
                } else {

                    ActivityCompat.requestPermissions(this,
                            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            1);
                }
            } else {
                importData();

            }
        }
    }

    @SuppressLint("RestrictedApi")
    private void importData() {
        // File filePath = new File(Environment.getExternalStorageDirectory() + "/Demo.xls");
        Workbook wb = new HSSFWorkbook();

        Cell cell = null;

        String ndate="",sub_staff="",sub_date="";
        ndate=adddate.getText().toString();
        sub_date=ndate.substring(0,3);


        if (departmentname.equalsIgnoreCase("")){
            sub_staff="All_departments";
        }else {
            sub_staff=departmentname+"_yearly";;
        }

        Sheet sheet = null;
        sheet = wb.createSheet(adddate.getText().toString()+" Yearly Staff Attendance Report");
        //Now column and row
        Row row = sheet.createRow(0);

        cell = row.createCell(0);
        cell.setCellValue("S No");

        cell = row.createCell(1);
        cell.setCellValue("Staff Id");

        cell = row.createCell(2);
        cell.setCellValue("Department/Post");

        cell = row.createCell(3);
        cell.setCellValue("Staff Name");

        cell = row.createCell(4);
        cell.setCellValue("Father Name");

        cell = row.createCell(5);
        cell.setCellValue("Present");

        cell = row.createCell(6);
        cell.setCellValue("Absent");

        cell = row.createCell(7);
        cell.setCellValue("Casual Leave)");

        cell = row.createCell(8);
        cell.setCellValue("Half Leave");

        cell = row.createCell(9);
        cell.setCellValue("One Third Leave");

        cell = row.createCell(10);
        cell.setCellValue("School Activity Leave");

        cell = row.createCell(11);
        cell.setCellValue("Holiday & Sunday");

        cell = row.createCell(12);
        cell.setCellValue("School Working Day");

        cell = row.createCell(13);
        cell.setCellValue("Attendance Report");


        //column width
        sheet.setColumnWidth(0, (20 * 100));
        sheet.setColumnWidth(1, (20 * 150));
        sheet.setColumnWidth(2, (30 * 250));
        sheet.setColumnWidth(3, (30 * 250));
        sheet.setColumnWidth(4, (30 * 200));
        sheet.setColumnWidth(5, (20 * 100));
        sheet.setColumnWidth(6, (20 * 100));
        sheet.setColumnWidth(7, (20 * 100));
        sheet.setColumnWidth(8, (30 * 200));
        sheet.setColumnWidth(9, (30 * 250));
        sheet.setColumnWidth(10, (30 * 250));
        sheet.setColumnWidth(11, (30 * 250));
        sheet.setColumnWidth(12, (30 * 250));
        sheet.setColumnWidth(13, (30 * 250));

        for (int i = 0; i < monList.size(); i++) {

            Row row1 = sheet.createRow(i + 1);

            cell = row1.createCell(0);
            cell.setCellValue(i+1);

            cell = row1.createCell(1);
            cell.setCellValue(monList.get(i).getStaff_id());

            cell = row1.createCell(2);
            cell.setCellValue(monList.get(i).getTeach_nonteach()+" ("+monList.get(i).getStaff_desg_name()+")");

            cell = row1.createCell(3);
            cell.setCellValue((monList.get(i).getStaff_name()));
            //  cell.setCellStyle(cellStyle);

            cell = row1.createCell(4);
            cell.setCellValue(monList.get(i).getFather_name());

            cell = row1.createCell(5);
            cell.setCellValue(monList.get(i).getP());

            cell = row1.createCell(6);
            cell.setCellValue(monList.get(i).getA());


//            int hl=0,otl=0,sal=0,cl=0,l=0,tl=0;
//            l= Integer.parseInt(monList.get(i).getL());
//            hl= Integer.parseInt(monList.get(i).getHl());
//            otl= Integer.parseInt(monList.get(i).getOtl());
//            sal= Integer.parseInt(monList.get(i).getSal());
//            tl=hl+otl+sal;
//            cl=l-tl;

            cell = row1.createCell(7);
            cell.setCellValue(monList.get(i).getCl());

            cell = row1.createCell(8);
            cell.setCellValue(monList.get(i).getHl());

            cell = row1.createCell(9);
            cell.setCellValue(monList.get(i).getOtl());

            cell = row1.createCell(10);
            cell.setCellValue(monList.get(i).getSal());

            cell = row1.createCell(11);
            cell.setCellValue(String.valueOf(total_h));

            cell = row1.createCell(12);
            cell.setCellValue(monList.get(i).getSch_wd());

            cell = row1.createCell(13);
            cell.setCellValue(monList.get(i).getStd_wd());


            sheet.setColumnWidth(0, (20 * 100));
            sheet.setColumnWidth(1, (20 * 150));
            sheet.setColumnWidth(2, (30 * 250));
            sheet.setColumnWidth(3, (30 * 250));
            sheet.setColumnWidth(4, (30 * 200));
            sheet.setColumnWidth(5, (20 * 100));
            sheet.setColumnWidth(6, (20 * 100));
            sheet.setColumnWidth(7, (20 * 100));
            sheet.setColumnWidth(8, (30 * 200));
            sheet.setColumnWidth(9, (30 * 250));
            sheet.setColumnWidth(10, (30 * 250));
            sheet.setColumnWidth(11, (30 * 250));
            sheet.setColumnWidth(12, (30 * 250));
            sheet.setColumnWidth(13, (30 * 250));



        }
        String fileName;
        if (sub_staff.equalsIgnoreCase("")){
            fileName = sub_date+"_" +System.currentTimeMillis() + ".xls";
        }else {
            fileName = sub_staff+"_attend_"+sub_date+"_" + System.currentTimeMillis() + ".xls";
        }


        // File file = new File(getActivity().getExternalFilesDir(null), fileName);

        File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS), fileName);

        FileOutputStream fileOutputStream = null;

        try {
            fileOutputStream = new FileOutputStream(file);
            wb.write(fileOutputStream);
            Utils.openSuccessDialog("Success!","Export file in "+file,"",this);
        } catch (IOException e) {
            Utils.openErrorDialog("Error writing Exception: "+ e,this);
        } catch (Exception e) {
            Utils.openErrorDialog("Failed to save file due to Exception: "+ e,this);
        } finally {
            try {
                if (null != fileOutputStream) {
                    fileOutputStream.close();
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

    }

    @SuppressLint("RestrictedApi")
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 1 && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            importData();
        } else {
            Toast.makeText(getApplicationContext(), "Permission Denied", Toast.LENGTH_SHORT).show();
        }
    }

    private void fetchDepartment() {
//        ErpProgress.showProgressBar(getActivity(), "Please wait...");
        commonProgress.show();
        HashMap<String, String> params = new HashMap<String, String>();
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.DEPARTMENT_LIST_URL, classapiCallback,StaffYearWiseReport.this, params);
    }

    ApiHandler.ApiCallback classapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            commonProgress.dismiss();
            if (error == null) {
                try {
                    departmentList = CommonBean.parseCommonArray(jsonObject.getJSONArray("department_data"));
                    DepartmentAdapternew adapter = new DepartmentAdapternew(StaffYearWiseReport.this, departmentList);
                    spinnerDepartment.setAdapter(adapter);
                    spinnerDepartment.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            abc = i;
                            if (abc > 0) {

                                departmentid = departmentList.get(i - 1).getId();
                                departmentname = departmentList.get(i - 1).getName();
                            }
                            else {
                                departmentid = "all";
                            }
//                            fetchStudentList(abc);
                            Log.e("depart", String.valueOf(abc));
                            Log.e("depart_id", String.valueOf(departmentid));

                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
                //Utils.showSnackBar(error.getMessage(), view.findViewById(R.id.parent));
            }
        }

    };
}