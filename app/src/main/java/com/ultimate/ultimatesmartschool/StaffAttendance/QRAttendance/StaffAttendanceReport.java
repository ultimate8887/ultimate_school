package com.ultimate.ultimatesmartschool.StaffAttendance.QRAttendance;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.ultimate.ultimatesmartschool.BeanModule.ClassBean;
import com.ultimate.ultimatesmartschool.BeanModule.CommonBean;
import com.ultimate.ultimatesmartschool.Examination.ClassAdapter;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.StaffAttendance.DepartmentAdapter;
import com.ultimate.ultimatesmartschool.StaffAttendance.StaffAttendance;
import com.ultimate.ultimatesmartschool.StudentAttendance.SubjectWiseAttendeance.AttendReportAdapter;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.CommonProgress;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class StaffAttendanceReport extends AppCompatActivity {
    @BindView(R.id.recyclerviewlist)
    RecyclerView recyclerView;
    Dialog dialog;
    @BindView(R.id.parent)
    RelativeLayout parent;
    @BindView(R.id.lytHeader)
    LinearLayout lytHeader;
    ArrayList<ClassBean> dataList = new ArrayList<>();
    private AttendReportAdapter adapter;
    CommonProgress commonProgress;
    @BindView(R.id.textNorecord)
    TextView txtNorecord;
    @BindView(R.id.totalRecord)
    TextView totalRecord;
    @BindView(R.id.today_date)
    TextView cal_text;
    @BindView(R.id.text)
    TextView text;
    @BindView(R.id.root1)
    RelativeLayout root1;
    String from_date = "";
    String image_url = "", school = "", className = "";
    String classid = "";
    ArrayList<ClassBean> classList = new ArrayList<>();
    @BindView(R.id.spinerVehicletype)
    Spinner spinnerClass;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    ArrayList<CommonBean> departmentList = new ArrayList<>();
    private String departmentid = "", departmentname = "";

    @BindView(R.id.txtAbsent)
    TextView txtAbsent;
    @BindView(R.id.txtLeave)
    TextView txtLeave;
    @BindView(R.id.txtPresent)
    TextView txtPresent;
    @BindView(R.id.txtName)
    TextView txtName;
    @BindView(R.id.txtRoll)
    TextView txtRoll;
    @BindView(R.id.lytData)
    LinearLayout lytData;

    int total=0,a=0,p=0,l=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_staff_leave_list);
        ButterKnife.bind(this);
        commonProgress = new CommonProgress(this);
        root1.setVisibility(View.VISIBLE);
        txtTitle.setText("Staff Attendance Report");
        text.setText("Department");
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new AttendReportAdapter(this, dataList);
        recyclerView.setAdapter(adapter);
        lytHeader.setVisibility(View.VISIBLE);
        fetchDepartment();
        //  fetchClassData();

    }

    @OnClick(R.id.today)
    public void cal_lyttttt() {

        java.util.Calendar cal = java.util.Calendar.getInstance(TimeZone.getDefault()); // Get current date
        DatePickerDialog datePicker = new DatePickerDialog(this,
                R.style.MyDatePickerDialogTheme, ondate,
                cal.get(java.util.Calendar.YEAR),
                cal.get(java.util.Calendar.MONTH),
                cal.get(java.util.Calendar.DAY_OF_MONTH));
        datePicker.getDatePicker();
        datePicker.setCancelable(false);
        datePicker.show();

    }

    DatePickerDialog.OnDateSetListener ondate = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            java.util.Calendar calendar = java.util.Calendar.getInstance(TimeZone.getDefault());
            calendar.set(year, monthOfYear, dayOfMonth);
            calendar.set(Calendar.HOUR_OF_DAY, 00);
            calendar.set(Calendar.MINUTE, 00);
            calendar.set(Calendar.SECOND, 00);
            Date setdate = calendar.getTime();
            SimpleDateFormat fmtOut = new SimpleDateFormat("dd MMM, yyyy");
            String dateString = fmtOut.format(setdate);
            cal_text.setText(dateString);
            SimpleDateFormat dateFrmOut = new SimpleDateFormat("yyyy-MM-dd");
            from_date = dateFrmOut.format(setdate);
            // Toast.makeText(HomeWorkByDate.this,from_date,Toast.LENGTH_SHORT).show();
            fetchClassData();
        }
    };


    @OnClick(R.id.imgBack)
    public void backFinish() {
        finish();
    }

    private void fetchDepartment() {
        commonProgress.show();
        HashMap<String, String> params = new HashMap<String, String>();
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.DEPARTMENT_LIST_URL, classapiCallback, this, params);
    }


    ApiHandler.ApiCallback classapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            commonProgress.dismiss();
            if (error == null) {
                try {
                    departmentList = CommonBean.parseCommonArray(jsonObject.getJSONArray("department_data"));
                    DepartmentAdapter adapter = new DepartmentAdapter(StaffAttendanceReport.this, departmentList);
                    spinnerClass.setAdapter(adapter);
                    spinnerClass.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @RequiresApi(api = Build.VERSION_CODES.N)
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if (i > 0) {
                                departmentid = "";
                                departmentname = "";
                                departmentid = departmentList.get(i - 1).getId();
                                departmentname = departmentList.get(i - 1).getName();
                                fetchClassData();
                            } else {
                                departmentid = "";
                                departmentname = "";
                                fetchClassData();
                            }

                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Utils.showSnackBar(error.getMessage(), parent);
            }
        }
    };


    private void fetchClassData() {
        total=0;
        a=0;
        p=0;
        l=0;
        commonProgress.show();
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("depart_id", departmentid);
        params.put("date", from_date);
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.STAFF_ATTEND_COUNT_URL, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                commonProgress.dismiss();
                if (error == null) {
                    try {
                        ArrayList<ClassBean> attendList = ClassBean.parseClassArray(jsonObject.getJSONArray(Constants.CLASSDATA));
                        dataList.clear();
                        dataList.addAll(attendList);
                        adapter.notifyDataSetChanged();

                        totalRecord.setText("Total Entries:- " + String.valueOf(dataList.size()));
                        //-----------end------------
                        txtNorecord.setVisibility(View.GONE);

                        for (int i = 0; i<attendList.size(); i++)
                        {
                            a += Integer.parseInt(attendList.get(i).getA_student());
                            p += Integer.parseInt(attendList.get(i).getP_student());
                            l += Integer.parseInt(attendList.get(i).getL_student());
                        }

                        total=a+p+l;
                        txtAbsent.setText(String.valueOf(a));
                        txtLeave.setText(String.valueOf(l));
                        txtPresent.setText(String.valueOf(p));
                        txtName.setText(String.valueOf(total));
                        lytData.setVisibility(View.VISIBLE);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    totalRecord.setText("Total Entries:- 0");
                    txtNorecord.setVisibility(View.VISIBLE);
                    Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();

                    total=a+p+l;
                    txtAbsent.setText(String.valueOf(a));
                    txtLeave.setText(String.valueOf(l));
                    txtPresent.setText(String.valueOf(p));
                    txtName.setText(String.valueOf(total));
                    lytData.setVisibility(View.VISIBLE);
                }
            }
        }, this, params);

    }

}