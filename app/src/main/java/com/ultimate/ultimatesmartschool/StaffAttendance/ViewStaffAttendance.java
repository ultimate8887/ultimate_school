package com.ultimate.ultimatesmartschool.StaffAttendance;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetSequence;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.ultimate.ultimatesmartschool.BeanModule.CommonBean;
import com.ultimate.ultimatesmartschool.BeanModule.User;
import com.ultimate.ultimatesmartschool.Homework.StaffListAdapter;
import com.ultimate.ultimatesmartschool.Login.LoginActivity;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Staff.View_staff_bean;
import com.ultimate.ultimatesmartschool.StudentAttendance.AttendMod;
import com.ultimate.ultimatesmartschool.StudentAttendance.MonthAdapter;
import com.ultimate.ultimatesmartschool.StudentAttendance.Util;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.CommonProgress;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ViewStaffAttendance extends AppCompatActivity {


    View view;
    @BindView(R.id.parent)
    RelativeLayout parent;
    @BindView(R.id.root)
    RelativeLayout root;

    CommonProgress commonProgress;
    private ArrayList<CommonBean> departmentList;
    private String departmentid="",departmentname="";
    @BindView(R.id.gridview)
    RecyclerView recyclerView;
    @BindView(R.id.currentMonth)
    TextView currentMonth;
    @BindView(R.id.attendlayout)
    NestedScrollView lytCalender;
    ArrayList<AttendMod> attendList = new ArrayList<>();
    int month;
    int year;
    private Calendar mCalendar;
    private MonthAdapters madapter;
    private String start_date;
    private String end_date;
    private List<String> mItems;
    private final String[] mDays = {"Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"};
    private final int[] mDaysInMonth = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
    private int mDaysShown = 0;
    private int mDaysLastMonth;
    private int mDaysNextMonth;
    int loaded = 0;
    int day;
    int day_name;
    @BindView(R.id.h_currentday)
    TextView h_currentday;
    @BindView(R.id.h_day_name)
    TextView h_day_name;
    @BindView(R.id.h_currentMonth)TextView h_currentMonth;

    @BindView(R.id.noofpresent)TextView noofpresent;
    @BindView(R.id.noofabsent)TextView noofabsent;
    @BindView(R.id.noofleave)TextView noofleave;
    @BindView(R.id.sal_noofleave)TextView sal_noofleave;
    @BindView(R.id.noofholydy)TextView noofholydy;
    @BindView(R.id.noofholydyotl)TextView noofholydyotl;
    @BindView(R.id.totalwork)TextView totalwork;
    @BindView(R.id.totalpresent)TextView totalpresent;
    @BindView(R.id.annualpercent)TextView annualpercent;
    @BindView(R.id.totalannualworkd)TextView totalannualworkd;
    //    @BindView(R.id.callayout)
//    RelativeLayout callayout;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.attenddetailpercentlay)
    CardView attenddetailpercentlay;


    @BindView(R.id.txtSub)
    TextView txtSub;
//    @BindView(R.id.attendlayout)
//    NestedScrollView attendlayout;

    @BindView(R.id.lytCard)
    LinearLayout lytCard;
    @BindView(R.id.lytCard1)
    LinearLayout lytCard1;
    @BindView(R.id.h_calender)
    ImageView h_calender;
    @BindView(R.id.btnBefore)
    TextView btnBefore;
    @BindView(R.id.btnNext)
    TextView btnNext;
    @BindView(R.id.stdlayout)
    LinearLayout stdlayout;
    String studentid= "",sub_id= "", sub_name= "" ,sectionid="",sectionname="",classid = "",className = "";
    @BindView(R.id.dialog)
    ImageView dialog;
    SharedPreferences sharedPreferences;
    BottomSheetDialog mBottomSheetDialog;
    int check=0;
    Spinner spinnersection;
    Spinner spinnerClass;
    Spinner spinnersubject;

    String staffid;
    Spinner spinnerDepartment;
    @BindView(R.id.spinnerstudnt)
    Spinner spinnerstaff;


//
//    @BindView(R.id.gridview)
//    RecyclerView recyclerView;
//    @BindView(R.id.currentMonth)
//    TextView currentMonth;
//    @BindView(R.id.parent)
//    LinearLayout parent;
//    ArrayList<AttendMod> attendList = new ArrayList<>();
//    int month;
//    int year;
//    private Calendar mCalendar;
//    private MonthAdapters madapter;
//    private String start_date;
//    private String end_date;
//    private List<String> mItems;
//    private final String[] mDays = {"Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"};
//    private final int[] mDaysInMonth = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
//    private int mDaysShown = 0;
//    private int mDaysLastMonth;
//    private int mDaysNextMonth;
//    String staffid;
//    int loaded = 0;
//    private View view;
//    //    @BindView(R.id.countOtl)TextView countOtl;
////    @BindView(R.id.countHl) TextView countHl;
//    @BindView(R.id.countleave)TextView countleave;
//    @BindView(R.id.countabsent)TextView countabsent;
//    @BindView(R.id.countpresent)TextView countpresent;
//
//    @BindView(R.id.noofholydy)TextView noofholydy;
//    //    @BindView(R.id.holidylayout)
////    RelativeLayout holidylayout;
//    @BindView(R.id.txtTitle)
//    TextView txtTitle;
//    @BindView(R.id.callayout)RelativeLayout callayout;
//    int day;
//    int day_name;
//    @BindView(R.id.h_currentday)
//    TextView h_currentday;
//    @BindView(R.id.h_day_name)
//    TextView h_day_name;
//    @BindView(R.id.h_currentMonth)TextView h_currentMonth;
    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_staff_attendance);
        ButterKnife.bind(this);
//        fetchClass();
//        fetchsection();
        // txtTitle.setText("View Attendance");
        commonProgress=new CommonProgress(this);

        if (!restorePrefData()){
            setShowcaseView();
        }else {
            openDialog();
        }
    }



    private void setShowcaseViewP() {
        new TapTargetSequence(this)
                .targets(TapTarget.forView(btnBefore,"Previous Button!","Tap the Previous Button to view selected staff previous month Attendance")
                        .outerCircleColor(R.color.light)
                        .outerCircleAlpha(0.96f)
                        .targetCircleColor(R.color.white)
                        .titleTextSize(20)
                        .titleTextColor(R.color.white)
                        .descriptionTextSize(14)
                        .descriptionTextColor(R.color.black)
                        .textColor(R.color.black)
                        .textTypeface(Typeface.SANS_SERIF)
                        .dimColor(R.color.black)
                        .drawShadow(true)
                        .cancelable(false)
                        .tintTarget(true)
                        .transparentTarget(true)
                        .targetRadius(60),TapTarget.forView(btnNext,"Next Button!","Tap the Next Button to view selected staff next month Attendance.")
                        .outerCircleColor(R.color.light)
                        .outerCircleAlpha(0.96f)
                        .targetCircleColor(R.color.white)
                        .titleTextSize(20)
                        .titleTextColor(R.color.white)
                        .descriptionTextSize(14)
                        .descriptionTextColor(R.color.black)
                        .textColor(R.color.black)
                        .textTypeface(Typeface.SANS_SERIF)
                        .dimColor(R.color.black)
                        .drawShadow(true)
                        .cancelable(false)
                        .tintTarget(true)
                        .transparentTarget(true)
                        .targetRadius(60)).listener(new TapTargetSequence.Listener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onSequenceFinish() {
                // Toast.makeText(getApplicationContext(),"Sequence Finished",Toast.LENGTH_SHORT).show();
                savePrefDataP();
            }

            @Override
            public void onSequenceStep(TapTarget lastTarget, boolean targetClicked) {
                // Toast.makeText(getActivity(),"GREAT!",Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSequenceCanceled(TapTarget lastTarget) {

            }
        }).start();
    }



    private void setShowcaseView1() {
        new TapTargetSequence(this)
                .targets(TapTarget.forView(spinnerstaff,"Spinner Button!","Tap the Spinner Button and select staff to view Staff-Wise Attendance.")
                        .outerCircleColor(R.color.light)
                        .outerCircleAlpha(0.96f)
                        .targetCircleColor(R.color.white)
                        .titleTextSize(20)
                        .titleTextColor(R.color.white)
                        .descriptionTextSize(14)
                        .descriptionTextColor(R.color.black)
                        .textColor(R.color.black)
                        .textTypeface(Typeface.SANS_SERIF)
                        .dimColor(R.color.black)
                        .drawShadow(true)
                        .cancelable(true)
                        .tintTarget(true)
                        .transparentTarget(true)
                        .targetRadius(70)).listener(new TapTargetSequence.Listener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onSequenceFinish() {
                // Toast.makeText(getApplicationContext(),"Sequence Finished",Toast.LENGTH_SHORT).show();
                savePrefData1();
            }

            @Override
            public void onSequenceStep(TapTarget lastTarget, boolean targetClicked) {
                // Toast.makeText(getActivity(),"GREAT!",Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSequenceCanceled(TapTarget lastTarget) {

            }
        }).start();
    }


    private void setShowcaseView() {
        new TapTargetSequence(this)
                .targets(TapTarget.forView(dialog,"Search Button!","Tap the Search Button to View Staff-Wise Attendance.")
                        .outerCircleColor(R.color.light)
                        .outerCircleAlpha(0.96f)
                        .targetCircleColor(R.color.white)
                        .titleTextSize(20)
                        .titleTextColor(R.color.white)
                        .descriptionTextSize(14)
                        .descriptionTextColor(R.color.black)
                        .textColor(R.color.black)
                        .textTypeface(Typeface.SANS_SERIF)
                        .dimColor(R.color.black)
                        .drawShadow(true)
                        .cancelable(false)
                        .tintTarget(true)
                        .transparentTarget(true)
                        .targetRadius(60)).listener(new TapTargetSequence.Listener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onSequenceFinish() {
                // Toast.makeText(getApplicationContext(),"Sequence Finished",Toast.LENGTH_SHORT).show();
                savePrefData();
                openDialog();
            }

            @Override
            public void onSequenceStep(TapTarget lastTarget, boolean targetClicked) {
                // Toast.makeText(getActivity(),"GREAT!",Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSequenceCanceled(TapTarget lastTarget) {

            }
        }).start();
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @OnClick(R.id.dialog)
    public void dialogggg() {
        openDialog();
    }

    private void savePrefData1(){
        sharedPreferences=this.getSharedPreferences("boarding_pref_view_attend1",MODE_PRIVATE);
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putBoolean("IsFirstTimeVisit_view_attend1",true);
        editor.apply();
    }

    private boolean restorePrefData1(){
        sharedPreferences = this.getSharedPreferences("boarding_pref_view_attend1",MODE_PRIVATE);
        return sharedPreferences.getBoolean("IsFirstTimeVisit_view_attend1",false);
    }


    private void savePrefData(){
        sharedPreferences=this.getSharedPreferences("boarding_pref_view_attend",MODE_PRIVATE);
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putBoolean("IsFirstTimeVisit_view_attend",true);
        editor.apply();
    }

    private boolean restorePrefData(){
        sharedPreferences = this.getSharedPreferences("boarding_pref_view_attend",MODE_PRIVATE);
        return sharedPreferences.getBoolean("IsFirstTimeVisit_view_attend",false);
    }

    private void savePrefDataP(){
        sharedPreferences=this.getSharedPreferences("boarding_pref_view_attendP",MODE_PRIVATE);
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putBoolean("IsFirstTimeVisit_view_attendP",true);
        editor.apply();
    }

    private boolean restorePrefDataP(){
        sharedPreferences = this.getSharedPreferences("boarding_pref_view_attendP",MODE_PRIVATE);
        return sharedPreferences.getBoolean("IsFirstTimeVisit_view_attendP",false);
    }


    @RequiresApi(api = Build.VERSION_CODES.N)
    private void openDialog() {


        mBottomSheetDialog = new BottomSheetDialog(this);
        final View sheetView = getLayoutInflater().inflate(R.layout.search_dialog, null);
        mBottomSheetDialog.setContentView(sheetView);
        mBottomSheetDialog.setCancelable(false);

        spinnerDepartment=(Spinner) sheetView.findViewById(R.id.spinnerClass);
        RelativeLayout spinnersection=(RelativeLayout) sheetView.findViewById(R.id.sectionlayout);
        spinnersection.setVisibility(View.GONE);
        TextView textView7=(TextView) sheetView.findViewById(R.id.textView7);
        textView7.setText("Select Department");

        TextView txtSetup=(TextView) sheetView.findViewById(R.id.txtSetup);
        txtSetup.setText("View Attendance");

        ImageView close=(ImageView) sheetView.findViewById(R.id.close);
        //txtTitle.setText("Mark Attendance");
        fetchDepartment();

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //assignData();
                if (check==0) {
                    finish();
                    mBottomSheetDialog.dismiss();
                }else {
                    mBottomSheetDialog.dismiss();
                }
            }
        });


        Button btnYes= (Button) sheetView.findViewById(R.id.btnYes);


        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (departmentid.equalsIgnoreCase(""))  {
                    Toast.makeText(ViewStaffAttendance.this,"Kindly Select Department!", Toast.LENGTH_SHORT).show();
                }  else {
                    check++;

                    if (!restorePrefData1()){
                        setShowcaseView1();
                    }
                    savePrefData1();
//                    searchStudent(date);
                    fetchstaff();
                    setCommonData();
                }

            }
        });
        mBottomSheetDialog.show();

    }
    private void setCommonData() {
        root.setVisibility(View.VISIBLE);
        txtTitle.setText(departmentname);
        // txtSub.setText(sub_name);
    }



    @OnClick(R.id.imgBack)
    public void onback() {
        finish();
    }
    private void fetchDepartment() {
       commonProgress.show();
        HashMap<String, String> params = new HashMap<String, String>();
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.DEPARTMENT_LIST_URL, classapiCallback, this, params);
    }



    ApiHandler.ApiCallback classapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            commonProgress.dismiss();
            if (error == null) {
                try {
                    departmentList = CommonBean.parseCommonArray(jsonObject.getJSONArray("department_data"));
                    DepartmentAdapter adapter = new DepartmentAdapter(ViewStaffAttendance.this, departmentList);
                    spinnerDepartment.setAdapter(adapter);
                    spinnerDepartment.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if (i > 0) {
                                departmentid = departmentList.get(i - 1).getId();
                                departmentname = departmentList.get(i - 1).getName();
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Utils.showSnackBar(error.getMessage(), view.findViewById(R.id.parent));
            }
        }
    };

    public void fetchstaff() {
        commonProgress.show();
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("d_id", departmentid);
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.STAFF_DETAIL, stafflistCallback, ViewStaffAttendance.this, params);
    }

    private ArrayList<View_staff_bean> stafflist;
    private StaffListAdapter adapterstafflist;
    ApiHandler.ApiCallback stafflistCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            mBottomSheetDialog.dismiss();
            commonProgress.dismiss();
            if (error == null) {
                try {
                    JSONArray jsonArray = jsonObject.getJSONArray("staff_detail");
                    stafflist = View_staff_bean.parseviewstaffArray(jsonArray);
                    adapterstafflist = new StaffListAdapter(ViewStaffAttendance.this, stafflist);
                    spinnerstaff.setAdapter(adapterstafflist);
                    spinnerstaff.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if (i > 0) {
                                staffid = stafflist.get(i - 1).getId();
                                lytCalender.setVisibility(View.VISIBLE);
                                setCalendar();
                            } else {
                                lytCalender.setVisibility(View.GONE);
                                staffid = "";
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Utils.showSnackBar(error.getMessage(), parent);
            }
        }
    };



    private void fetchAttedance() {
        if (loaded == 0) {
            commonProgress.show();
        }
        loaded++;
        HashMap<String, String> params = new HashMap<String, String>();
        params.put(Constants.ID, staffid);
        params.put("start_date", start_date);
        params.put("end_date", end_date);
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.STAFFATTENDANCE_URL, apiCallback, this, params);
    }


    ApiHandler.ApiCallback apiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            commonProgress.dismiss();
            if (error == null) {
                try {
                    attenddetailpercentlay.setVisibility(View.VISIBLE);
                    attendList.clear();
                    attendList = AttendMod.parseAttendArray(jsonObject.getJSONArray("attend_data"));
                    if(attendList.get(0).getPr()!=null) {
                        noofpresent.setText(attendList.get(0).getPr()+"d");
                    }else{
                        noofpresent.setText("0"+"d");
                    }
                    if(attendList.get(0).getAb()!=null) {
                        noofabsent.setText(attendList.get(0).getAb()+"d");
                        Log.e("no of absent",attendList.get(0).getAb());
                    }else{
                        noofabsent.setText("0"+"d");
                    }
                    if(attendList.get(0).getLv()!=null) {
                        noofleave.setText(attendList.get(0).getLv()+"d");
                    }else{
                        noofleave.setText("0"+"d");
                    }

                    if(attendList.get(0).getHlv()!=null ) {
                        noofholydy.setText(attendList.get(0).getHlv()+"d");
                      //  Log.e("no of holyday",attendList.get(0).getHl());
                    }else{
                        noofholydy.setText("0"+"d");
                    }

                    if(attendList.get(0).getSal()!=null ) {
                        sal_noofleave.setText(attendList.get(0).getSal()+"d");
                        //  Log.e("no of holyday",attendList.get(0).getHl());
                    }else{
                        sal_noofleave.setText("0"+"d");
                    }

                    if(attendList.get(0).getOtl()!=null ) {
                        noofholydyotl.setText(attendList.get(0).getOtl()+"d");
                        //  Log.e("no of holyday",attendList.get(0).getHl());
                    }else{
                        noofholydyotl.setText("0"+"d");
                    }

                    if(attendList.get(0).getTwcount()!=null) {
                        totalwork.setText("Total Working Day: "+attendList.get(0).getTwcount()+"d");
                    }else{
                        totalwork.setText("0"+"d");
                    }
                    if(attendList.get(0).getTprcount()!=null) {
                        totalpresent.setText("Total Annual Present Day: "+attendList.get(0).getTprcount()+"d");

                    }else{
                        totalpresent.setText("0"+"d");

                    }

                    if(attendList.get(0).getTotalworkcountwoh()!=null){
                        totalannualworkd.setText("Total Annual Working Day: "+attendList.get(0).getTotalworkcountwoh()+"d");
                    }else{
                        totalannualworkd.setText("0"+"d");
                    }
                    if(attendList.get(0).getPrtcont()!=null) {
                        annualpercent.setText("Annual(%) Till Current Date: "+attendList.get(0).getPrtcont()+"d");
                    }else{
                        annualpercent.setText("0"+"d");
                    }

                    madapter.setAttendList(attendList);
                    madapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                attendList.clear();
                madapter.setAttendList(attendList);
                madapter.notifyDataSetChanged();
                noofpresent.setText("0"+"d");
                noofabsent.setText("0"+"d");
                noofleave.setText("0"+"d");
                noofholydy.setText("0"+"d");
                sal_noofleave.setText("0"+"d");
                noofholydyotl.setText("0"+"d");

                if (error.getStatusCode() == 401) {
                    attenddetailpercentlay.setVisibility(View.GONE);
                    Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_LONG).show();
                    if(error.getMessage().equalsIgnoreCase("No record Found"))
                        madapter.setAttendList(attendList);
                    madapter.notifyDataSetChanged();
                    noofpresent.setText("0"+"d");
                    noofabsent.setText("0"+"d");
                    noofleave.setText("0"+"d");
                    noofholydy.setText("0"+"d");
                    sal_noofleave.setText("0"+"d");
                    noofholydyotl.setText("0"+"d");

                }
                if (error.getStatusCode() == 405) {
                    User.logout();
                    startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                    finish();
                }

            }
        }
    };

    public void setCalendar() {
        lytCalender.setVisibility(View.VISIBLE);
        if (!restorePrefDataP()){
            setShowcaseViewP();
        }
        mCalendar = Calendar.getInstance();

        day = mCalendar.get(Calendar.DATE);
        day_name = mCalendar.get(Calendar.DAY_OF_WEEK);
        String weekday = new DateFormatSymbols().getWeekdays()[day_name];
        h_currentday.setText(String.valueOf(day));
        h_day_name.setText(weekday);
        SimpleDateFormat month_date = new SimpleDateFormat("MMMM");
        String month_name = month_date.format(mCalendar.getTime());
        h_currentMonth.setText(month_name);
        month = mCalendar.get(Calendar.MONTH); // zero based

        year = mCalendar.get(Calendar.YEAR);

        recyclerView.setLayoutManager(new GridLayoutManager(getApplicationContext(), 7));
        recyclerView.setHasFixedSize(false);


        getDateList(month, year);


        currentMonth.setText(Util.getMonth(month + 1) + " " + String.valueOf(year));

        String setmonth = (month+1)+"";
        if (month + 1 < 10) {
            setmonth = "0" + (month + 1);
        }
        start_date = year + "-" + setmonth + "-01";
        // end_date = year + "-" + setmonth + "-" + Utils.getMaxDateByMonth(month + 1);
        GregorianCalendar gcal = new GregorianCalendar();
        if(gcal.isLeapYear(year)){
            end_date = year + "-" + setmonth + "-" + Utils.getMaxDateByMonthleap(month + 1);
        }else{
            end_date = year + "-" + setmonth + "-" + Utils.getMaxDateByMonth(month + 1);
        }
        fetchAttedance();
    }

    public void getDateList(int mMonth, int mYear) {
        mDaysLastMonth = 0;
        mDaysNextMonth = 0;
        mDaysShown =0;

        GregorianCalendar gCal = new GregorianCalendar(mYear, mMonth, 1);
        ArrayList<String> mItems = new ArrayList<String>();
        for (String day : mDays) {
            mItems.add(day);
            mDaysShown++;
        }

        int firstDay = getDay(gCal.get(Calendar.DAY_OF_WEEK));
        int prevDay;
        if (mMonth == 0) {
            mMonth = 11;
        }
        prevDay = daysInMonth(mMonth - 1, gCal, mYear) - firstDay + 1;
//        if (mMonth == 0)
//            prevDay = daysInMonth(11, gCal, mYear) - firstDay + 1;
//        else
//            prevDay = daysInMonth(mMonth - 1, gCal, mYear) - firstDay + 1;

        for (int i = 0; i < firstDay; i++) {
            mItems.add(String.valueOf(prevDay + i));
            mDaysLastMonth++;
            mDaysShown++;
        }

        int daysInMonth = daysInMonth(mMonth, gCal, mYear);
        for (int i = 1; i <= daysInMonth; i++) {
            mItems.add(String.valueOf(i));
            mDaysShown++;
        }

        mDaysNextMonth = 1;
        while (mDaysShown % 7 != 0) {
            mItems.add(String.valueOf(mDaysNextMonth));
            mDaysShown++;
            mDaysNextMonth++;
        }
        if (madapter == null) {
            madapter = new MonthAdapters(ViewStaffAttendance.this, attendList, mItems, month, year, mDaysLastMonth, mDaysNextMonth, mDaysShown);
            recyclerView.setAdapter(madapter);
        } else {
            madapter.setDateList(mItems, month, year, mDaysLastMonth, mDaysNextMonth, mDaysShown);
            madapter.notifyDataSetChanged();
        }
    }

    private int daysInMonth(int month, GregorianCalendar gCal, int mYear) {
        int daysInMonth = mDaysInMonth[month];
        if (month == 1 && gCal.isLeapYear(mYear))
            daysInMonth++;
        return daysInMonth;
    }


    private int getDay(int day) {
        switch (day) {
            case Calendar.MONDAY:
                return 0;
            case Calendar.TUESDAY:
                return 1;
            case Calendar.WEDNESDAY:
                return 2;
            case Calendar.THURSDAY:
                return 3;
            case Calendar.FRIDAY:
                return 4;
            case Calendar.SATURDAY:
                return 5;
            case Calendar.SUNDAY:
                return 6;
            default:
                return 0;
        }
    }

    @OnClick(R.id.btnNext)
    public void onClickNext() {
        month = month + 1;

        if (month > 11) {
            month = 0;
            year = year + 1;
        }

        currentMonth.setText(Util.getMonth(month + 1) + " " + String.valueOf(year));
        mCalendar.set(Calendar.MONTH, month);
        getDateList(month, year);

        String setmonth = (month + 1) + "";
        if (month + 1 < 10) {
            setmonth = "0" + (month + 1);
        }
        start_date = year + "-" + setmonth + "-01";
        // end_date = year + "-" + setmonth + "-" + Utils.getMaxDateByMonth(month + 1);
        GregorianCalendar gcal = new GregorianCalendar();
        if(gcal.isLeapYear(year)){
            end_date = year + "-" + setmonth + "-" + Utils.getMaxDateByMonthleap(month + 1);
        }else{
            end_date = year + "-" + setmonth + "-" + Utils.getMaxDateByMonth(month + 1);
        }
        fetchAttedance();
    }

    @OnClick(R.id.btnBefore)
    public void onClickBefore() {
        month = month - 1;

        if (month < 0) {
            month = 11;
            year = year - 1;
        }
        currentMonth.setText(Util.getMonth(month + 1) + " " + String.valueOf(year));
        getDateList(month, year);

        String setmonth = (month + 1) + "";
        if (month + 1 < 10) {
            setmonth = "0" + (month + 1);
        }
        start_date = year + "-" + setmonth + "-01";
        //end_date = year + "-" + setmonth + "-" + Utils.getMaxDateByMonth(month + 1);
        GregorianCalendar gcal = new GregorianCalendar();
        if(gcal.isLeapYear(year)){
            end_date = year + "-" + setmonth + "-" + Utils.getMaxDateByMonthleap(month + 1);
        }else{
            end_date = year + "-" + setmonth + "-" + Utils.getMaxDateByMonth(month + 1);
        }
        fetchAttedance();
    }

}
