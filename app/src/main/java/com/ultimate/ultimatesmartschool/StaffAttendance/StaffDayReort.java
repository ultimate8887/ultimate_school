package com.ultimate.ultimatesmartschool.StaffAttendance;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetSequence;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.ultimate.ultimatesmartschool.BeanModule.CommonBean;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.CommonProgress;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class StaffDayReort extends AppCompatActivity {

    @BindView(R.id.export)
    FloatingActionButton export;
    int selected = 1;
    SharedPreferences sharedPreferences;
   // ArrayList<ClassBean> classList = new ArrayList<>();
    @BindView(R.id.parent)
    RelativeLayout parent;
    //@BindView(R.id.spinner) Spinner spinnerClass;
    BottomSheetDialog mBottomSheetDialog;
    private int loaded = 0;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.txtSub)
    TextView txtSub;
    List<String> classidsel;
    @BindView(R.id.totalRecord)
    TextView totalRecord;



    @BindView(R.id.adddate)TextView adddate;
    public String date;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.textNorecord)
    TextView txtNorecord;
    private ArrayList<StaffAttendBean> dayList;
    private DepartmentReportAdapter adapter;
//    @BindView(R.id.btnSubmit)
//    Button btnSubmit;

    @BindView(R.id.dialog)
    ImageView dialog;
    @BindView(R.id.sc)
    CardView scrollView;
    @BindView(R.id.filelayout)
    LinearLayout filelayout;

    int check=0;

    CommonProgress commonProgress;


    Spinner spinnerDepartment;

    int abc;

    private ArrayList<CommonBean> departmentList;
    private String departmentid="",departmentname="";


    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_staff_day_reort);
        ButterKnife.bind(this);
//        fetchClass();
//        fetchCurrentDay();
//        fetchsection();
//        txtTitle.setText("DayWise Report");
        commonProgress=new CommonProgress(this);
        departmentList = new ArrayList<>();
        dayList = new ArrayList<>();
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new DepartmentReportAdapter(this, dayList);
        recyclerView.setAdapter(adapter);
        if (!restorePrefData()){
            setShowcaseView();
        }else {
            openDialog();
        }
    }

    private void setShowcaseView_aday() {
        new TapTargetSequence(this)
                .targets(TapTarget.forView(filelayout,"Date Button!","Tap the Date button to View Day-Wise Attendance.")
                        .outerCircleColor(R.color.light)
                        .outerCircleAlpha(0.96f)
                        .targetCircleColor(R.color.white)
                        .titleTextSize(20)
                        .titleTextColor(R.color.white)
                        .descriptionTextSize(14)
                        .descriptionTextColor(R.color.black)
                        .textColor(R.color.black)
                        .textTypeface(Typeface.SANS_SERIF)
                        .dimColor(R.color.black)
                        .drawShadow(true)
                        .cancelable(false)
                        .tintTarget(true)
                        .transparentTarget(true)
                        .targetRadius(60)).listener(new TapTargetSequence.Listener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onSequenceFinish() {
                // Toast.makeText(getApplicationContext(),"Sequence Finished",Toast.LENGTH_SHORT).show();
                savePrefData_aday();
                //openDialog();
            }

            @Override
            public void onSequenceStep(TapTarget lastTarget, boolean targetClicked) {
                // Toast.makeText(getActivity(),"GREAT!",Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSequenceCanceled(TapTarget lastTarget) {

            }
        }).start();
    }

    private void savePrefData_aday(){
        sharedPreferences=this.getSharedPreferences("boarding_pref_add_attend_vday",MODE_PRIVATE);
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putBoolean("IsFirstTimeVisit_add_attend_vday",true);
        editor.apply();
    }

    private boolean restorePrefData_aday(){
        sharedPreferences = this.getSharedPreferences("boarding_pref_add_attend_vday",MODE_PRIVATE);
        return sharedPreferences.getBoolean("IsFirstTimeVisit_add_attend_vday",false);
    }


    private void setShowcaseView() {
        new TapTargetSequence(this)
                .targets(TapTarget.forView(dialog,"Search Button!","Tap the Search button to to view Department-Wise Attendance Report.")
                        .outerCircleColor(R.color.light)
                        .outerCircleAlpha(0.96f)
                        .targetCircleColor(R.color.white)
                        .titleTextSize(20)
                        .titleTextColor(R.color.white)
                        .descriptionTextSize(14)
                        .descriptionTextColor(R.color.black)
                        .textColor(R.color.black)
                        .textTypeface(Typeface.SANS_SERIF)
                        .dimColor(R.color.black)
                        .drawShadow(true)
                        .cancelable(false)
                        .tintTarget(true)
                        .transparentTarget(true)
                        .targetRadius(60)).listener(new TapTargetSequence.Listener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onSequenceFinish() {
                // Toast.makeText(getApplicationContext(),"Sequence Finished",Toast.LENGTH_SHORT).show();
                savePrefData();
                openDialog();
            }

            @Override
            public void onSequenceStep(TapTarget lastTarget, boolean targetClicked) {
                // Toast.makeText(getActivity(),"GREAT!",Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSequenceCanceled(TapTarget lastTarget) {

            }
        }).start();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        commonBack();
    }

    private void commonBack() {
        androidx.appcompat.app.AlertDialog.Builder builder1 = new androidx.appcompat.app.AlertDialog.Builder(StaffDayReort.this);
        builder1.setMessage("Are you sure, you want to exit? ");
        builder1.setCancelable(false);
        builder1.setPositiveButton(
                "Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finish();
                    }
                });
        builder1.setNegativeButton(
                "No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

    private void savePrefData(){
        sharedPreferences=this.getSharedPreferences("boarding_pref_add_attend_day",MODE_PRIVATE);
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putBoolean("IsFirstTimeVisit_add_attend_day",true);
        editor.apply();
    }

    private boolean restorePrefData(){
        sharedPreferences = this.getSharedPreferences("boarding_pref_add_attend_day",MODE_PRIVATE);
        return sharedPreferences.getBoolean("IsFirstTimeVisit_add_attend_day",false);
    }


    @RequiresApi(api = Build.VERSION_CODES.N)
    private void openDialog() {


        mBottomSheetDialog = new BottomSheetDialog(this);
        final View sheetView = getLayoutInflater().inflate(R.layout.search_dialog, null);
        mBottomSheetDialog.setContentView(sheetView);
        mBottomSheetDialog.setCancelable(false);

        spinnerDepartment=(Spinner) sheetView.findViewById(R.id.spinnerClass);
        RelativeLayout spinnersection=(RelativeLayout) sheetView.findViewById(R.id.sectionlayout);
        spinnersection.setVisibility(View.GONE);
        TextView textView7=(TextView) sheetView.findViewById(R.id.textView7);
        textView7.setText("Select Department");
        TextView txtSetup=(TextView) sheetView.findViewById(R.id.txtSetup);
        txtSetup.setText("View Day-Wise \nAttendance");
        ImageView close=(ImageView) sheetView.findViewById(R.id.close);
        //txtTitle.setText("Mark Attendance");
        getTodayDate();
        fetchDepartment();

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //assignData();
                if (check==0) {
                    finish();
                    mBottomSheetDialog.dismiss();
                }else {
                    mBottomSheetDialog.dismiss();
                }
            }
        });


        Button btnYes= (Button) sheetView.findViewById(R.id.btnYes);


        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                if (departmentid.equalsIgnoreCase(""))  {
//                    Toast.makeText(StaffDayReort.this,"Kindly Select Department!", Toast.LENGTH_SHORT).show();
//                }  else {
                    check++;
                    fetchStudentList(date);
                    setCommonData();
              //  }

            }
        });
        mBottomSheetDialog.show();



    }

    public void getTodayDate() {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.HOUR_OF_DAY, 00);
        c.set(Calendar.MINUTE, 00);
        c.set(Calendar.SECOND, 00);
        java.util.Date setdate = c.getTime();
        SimpleDateFormat fmtOut = new SimpleDateFormat("dd MMM, yyyy");
        String dateString = fmtOut.format(setdate);
        adddate.setText(dateString);
        SimpleDateFormat dateFrmOut = new SimpleDateFormat("yyyy-MM-dd");
        date = dateFrmOut.format(setdate);
    }

    private void setCommonData() {
        filelayout.setVisibility(View.VISIBLE);
        if (!restorePrefData_aday()){
            setShowcaseView_aday();
        }
        if (!departmentname.equalsIgnoreCase("")){
            txtTitle.setText(departmentname);
        }else{
            txtTitle.setText("All Department");
        }

    }


    @OnClick(R.id.imgBack)
    public void onBackclick() {
        commonBack();
    }






    private void fetchDepartment() {
//        ErpProgress.showProgressBar(getActivity(), "Please wait...");
        commonProgress.show();
        HashMap<String, String> params = new HashMap<String, String>();
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.DEPARTMENT_LIST_URL, classapiCallback, this, params);
    }




    ApiHandler.ApiCallback classapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            commonProgress.dismiss();
            if (error == null) {
                try {
                    departmentList = CommonBean.parseCommonArray(jsonObject.getJSONArray("department_data"));
                    DepartmentAdapternew adapter = new DepartmentAdapternew(StaffDayReort.this, departmentList);
                    spinnerDepartment.setAdapter(adapter);
                    spinnerDepartment.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            abc=i;
                            if (abc > 0) {
                                departmentid = departmentList.get(i - 1).getId();
                                departmentname = departmentList.get(i - 1).getName();
                            }
                            else{
                                departmentid= "all";
                            }
                            //fetchStudentList(abc);
                            Log.e("depart", String.valueOf(abc));
                            Log.e("depart_id", String.valueOf(departmentid));

                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_LONG).show();
               // Utils.showSnackBar(error.getMessage(), view.findViewById(R.id.parent));
            }
        }
    };

    private void fetchStudentList(String date) {
       commonProgress.show();

        HashMap<String, String> params = new HashMap<String, String>();

        if(!departmentid.equalsIgnoreCase("all")){
            params.put("d_id", departmentid);
            params.put("all", "0");
        }
        else {
            params.put("d_id", "0");
            params.put("all", "1");
        }
//        if(abc==0){
//            params.put("all", "1");
//        }

        params.put("mdate", date);
        params.put("type", selected + "");
        Log.e("yyyy",Constants.getBaseURL() + Constants.STAFF_ATTEND_LIST);
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.STAFF_ATTEND_LIST, studentapiCallback, this, params);
    }

    ApiHandler.ApiCallback studentapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            commonProgress.dismiss();
            mBottomSheetDialog.dismiss();
            if (error == null) {
                try {
                   // if(selected==1){
                    ArrayList<StaffAttendBean> attendList  = StaffAttendBean.parseAttendArray(jsonObject.getJSONArray(Constants.ATTEND_DATA));
                        dayList.clear();
                        dayList.addAll(attendList);
                        adapter.notifyDataSetChanged();
                        totalRecord.setText("Total Entries:- "+String.valueOf(attendList.size()));
                        scrollView.setVisibility(View.VISIBLE);
                   // }
                    export.setVisibility(View.VISIBLE);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                export.setVisibility(View.GONE);
                scrollView.setVisibility(View.GONE);
                totalRecord.setText("Total Entries:- 0");
                //Utils.showSnackBar(error.getMessage(), view.findViewById(R.id.parent));
                Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_LONG).show();
            }
        }
    };

    @SuppressLint("RestrictedApi")
    @OnClick(R.id.export)
    public void submit() {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.S) {
            importData();
        } else {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {

                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    Toast.makeText(this, "Please provide storage permission from app settings", Toast.LENGTH_LONG).show();
                } else {

                    ActivityCompat.requestPermissions(this,
                            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            1);
                }
            } else {
                importData();

            }
        }
    }

    @SuppressLint("RestrictedApi")
    private void importData() {
        // File filePath = new File(Environment.getExternalStorageDirectory() + "/Demo.xls");
        Workbook wb = new HSSFWorkbook();

        Cell cell = null;

        String ndate="",sub_staff="",sub_date="";
        ndate= Utils.getDateOnlyNEW(date)+","+Utils.getMonthFormated(date);
        sub_date=ndate.substring(0,3);


        if (departmentname.equalsIgnoreCase("")){
            sub_staff="All_departments";
        }else {
            sub_staff=departmentname;
        }

        Sheet sheet = null;
        sheet = wb.createSheet(adddate.getText().toString()+", Staff Attendance Report");
        //Now column and row
        Row row = sheet.createRow(0);

        cell = row.createCell(0);
        cell.setCellValue("S No");

        cell = row.createCell(1);
        cell.setCellValue("Staff Id");

        cell = row.createCell(2);
        cell.setCellValue("Department/Post");

        cell = row.createCell(3);
        cell.setCellValue("Staff Name");

        cell = row.createCell(4);
        cell.setCellValue("Father Name");

        cell = row.createCell(5);
        cell.setCellValue("Attendance");

        cell = row.createCell(6);
        cell.setCellValue("Date");


        //column width
        sheet.setColumnWidth(0, (20 * 100));
        sheet.setColumnWidth(1, (20 * 150));
        sheet.setColumnWidth(2, (30 * 250));
        sheet.setColumnWidth(3, (30 * 250));
        sheet.setColumnWidth(4, (30 * 200));
        sheet.setColumnWidth(5, (20 * 200));
        sheet.setColumnWidth(6, (30 * 200));

        for (int i = 0; i < dayList.size(); i++) {

            Row row1 = sheet.createRow(i + 1);

            cell = row1.createCell(0);
            cell.setCellValue(i+1);

            cell = row1.createCell(1);
            cell.setCellValue(dayList.get(i).getStaff_id());

            cell = row1.createCell(2);
            cell.setCellValue(dayList.get(i).getTeach_nonteach()+" ("+dayList.get(i).getStaff_desg_name()+")");

            cell = row1.createCell(3);
            cell.setCellValue((dayList.get(i).getStaff_name()));
            //  cell.setCellStyle(cellStyle);

            cell = row1.createCell(4);
            cell.setCellValue(dayList.get(i).getFather_name());


            String attend="N/A";

            if(dayList.get(i).getAttendance().equalsIgnoreCase("a")){
                attend ="Absent";
            }else if(dayList.get(i).getAttendance().equalsIgnoreCase("p")){
                attend ="Present";
            }else if(dayList.get(i).getAttendance().equalsIgnoreCase("l")){

             if(dayList.get(i).getAttendance().equalsIgnoreCase("l") && dayList.get(i).getAt_staff_remarks().equalsIgnoreCase("CL")){
                 attend = "Leave(CL)";
             } else if(dayList.get(i).getAttendance().equalsIgnoreCase("l") && dayList.get(i).getAt_staff_remarks().equalsIgnoreCase("HL")){
                attend="Leave(HL)";
            }else if(dayList.get(i).getAttendance().equalsIgnoreCase("l") && dayList.get(i).getAt_staff_remarks().equalsIgnoreCase("OTL")){
                attend="Leave(OTL)";
            }else if(dayList.get(i).getAttendance().equalsIgnoreCase("l") && dayList.get(i).getAt_staff_remarks().equalsIgnoreCase("clu")){
                attend="Leave(CLU)";
            }else if(dayList.get(i).getAttendance().equalsIgnoreCase("l") && dayList.get(i).getAt_staff_remarks().equalsIgnoreCase("clp")){
                attend="Leave(CLP)";
            }else if(dayList.get(i).getAttendance().equalsIgnoreCase("l") && dayList.get(i).getAt_staff_remarks().equalsIgnoreCase("hlp")){
                attend="Leave(HLP)";
            }else if(dayList.get(i).getAttendance().equalsIgnoreCase("l") && dayList.get(i).getAt_staff_remarks().equalsIgnoreCase("hlu")){
                attend="Leave(HLU)";
            }else{
                attend="Leave";
            }
            }else {
                attend="N/A";
            }

            cell = row1.createCell(5);
            cell.setCellValue(attend);

            cell = row1.createCell(6);
            cell.setCellValue(ndate);


            sheet.setColumnWidth(0, (20 * 100));
            sheet.setColumnWidth(1, (20 * 150));
            sheet.setColumnWidth(2, (30 * 250));
            sheet.setColumnWidth(3, (30 * 250));
            sheet.setColumnWidth(4, (30 * 200));
            sheet.setColumnWidth(5, (20 * 200));
            sheet.setColumnWidth(6, (30 * 200));




        }
        String fileName;
        if (sub_staff.equalsIgnoreCase("")){
            fileName = sub_date+"_" +System.currentTimeMillis() + ".xls";
        }else {
            fileName = sub_staff+"_attend_"+Utils.getDateOnlyNEW(date)+"_"+Utils.getMonthFormated(date)+"_" + System.currentTimeMillis() + ".xls";
        }


        // File file = new File(getActivity().getExternalFilesDir(null), fileName);

        File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS), fileName);

        FileOutputStream fileOutputStream = null;

        try {
            fileOutputStream = new FileOutputStream(file);
            wb.write(fileOutputStream);
            Utils.openSuccessDialog("Success!","Export file in "+file,"",this);
        } catch (IOException e) {
            Utils.openErrorDialog("Error writing Exception: "+ e,this);
        } catch (Exception e) {
            Utils.openErrorDialog("Failed to save file due to Exception: "+ e,this);
        } finally {
            try {
                if (null != fileOutputStream) {
                    fileOutputStream.close();
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

    }

    @SuppressLint("RestrictedApi")
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 1 && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            importData();
        } else {
            Toast.makeText(getApplicationContext(), "Permission Denied", Toast.LENGTH_SHORT).show();
        }
    }


    @RequiresApi(api = Build.VERSION_CODES.N)
    @OnClick(R.id.dialog)
    public void dialogggg() {
        openDialog();
    }

    @OnClick(R.id.filelayout)
    public void fetchDate() {
        java.util.Calendar cal = java.util.Calendar.getInstance(TimeZone.getDefault()); // Get current date
        DatePickerDialog datePicker = new DatePickerDialog(this,
                R.style.MyDatePickerDialogTheme, ondate,
                cal.get(java.util.Calendar.YEAR),
                cal.get(java.util.Calendar.MONTH),
                cal.get(java.util.Calendar.DAY_OF_MONTH));
        datePicker.getDatePicker().setMaxDate(new java.util.Date().getTime());
        datePicker.setCancelable(false);
        datePicker.show();
    }

    DatePickerDialog.OnDateSetListener ondate = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            java.util.Calendar calendar = java.util.Calendar.getInstance(TimeZone.getDefault());
            calendar.set(year, monthOfYear, dayOfMonth);
            calendar.set(Calendar.HOUR_OF_DAY, 00);
            calendar.set(Calendar.MINUTE, 00);
            calendar.set(Calendar.SECOND, 00);
            java.util.Date setdate = calendar.getTime();
            SimpleDateFormat fmtOut = new SimpleDateFormat("dd MMM, yyyy");
            String dateString = fmtOut.format(setdate);
            adddate.setText(dateString);
            SimpleDateFormat dateFrmOut = new SimpleDateFormat("yyyy-MM-dd");
            date = dateFrmOut.format(setdate);
            fetchStudentList(date);
        }
    };






}
