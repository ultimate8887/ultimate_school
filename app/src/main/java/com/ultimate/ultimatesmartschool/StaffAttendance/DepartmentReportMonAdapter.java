package com.ultimate.ultimatesmartschool.StaffAttendance;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.ultimate.ultimatesmartschool.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DepartmentReportMonAdapter extends RecyclerView.Adapter<DepartmentReportMonAdapter.MyViewHolder> {

    private Context mContext;
    ArrayList<StaffMonthBean> dataList;
    int value=0;
    DepartmentReportMonAdapter(Context mContext, ArrayList<StaffMonthBean> dataList,int value) {
        this.mContext = mContext;
        this.dataList = dataList;
        this.value = value;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.mon_depart_report, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        if (position == 0) {
            holder.lytHeader.setVisibility(View.GONE);
            holder.lytData.setVisibility(View.GONE);
        } else {
            holder.lytHeader.setVisibility(View.INVISIBLE);
            holder.lytData.setVisibility(View.VISIBLE);
            final StaffMonthBean data = dataList.get(position - 1);
            holder.txtName.setText(data.getStaff_name());
            holder.txtRoll.setText(data.getStaff_id());
            holder.txtAbsent.setText(data.getA());
            holder.txtLeave.setText(data.getL());
            holder.txtPresent.setText(data.getP());
            holder.txtHoliday.setText(String.valueOf(value));
        }

    }

    @Override
    public int getItemCount() {
        return (dataList.size() + 1);
    }


    public void setList(ArrayList<StaffMonthBean> dataList) {
        this.dataList = dataList;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txtAbsent)
        TextView txtAbsent;
        @BindView(R.id.txtLeave)
        TextView txtLeave;
        @BindView(R.id.txtHoliday)
        TextView txtHoliday;
        @BindView(R.id.txtPresent)
        TextView txtPresent;
        @BindView(R.id.txtName)
        TextView txtName;
        @BindView(R.id.txtRoll)
        TextView txtRoll;
        @BindView(R.id.lytData)
        LinearLayout lytData;
        @BindView(R.id.lytHeader)
        LinearLayout lytHeader;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
