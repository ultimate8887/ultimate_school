package com.ultimate.ultimatesmartschool.StaffAttendance;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.ultimate.ultimatesmartschool.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DepartmentReportAdapter extends RecyclerView.Adapter<DepartmentReportAdapter.MyViewHolder> {

    private Context mContext;
    ArrayList<StaffAttendBean> dataList;

    DepartmentReportAdapter(Context mContext, ArrayList<StaffAttendBean> dataList) {
        this.mContext = mContext;
        this.dataList = dataList;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.depart_day_report, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        if (position == 0) {
            holder.lytHeader.setVisibility(View.GONE);
            holder.lytData.setVisibility(View.GONE);
        } else {
            holder.lytHeader.setVisibility(View.INVISIBLE);
            holder.lytData.setVisibility(View.VISIBLE);
            final StaffAttendBean data = dataList.get(position - 1);
            holder.txtName.setText(data.getStaff_name());
            holder.txtRoll.setText(data.getStaff_id());

            holder.txtsno.setText(String.valueOf(position));

            if (data.getAttendance().equalsIgnoreCase("a")) {
                holder.txtAttend.setBackground(mContext.getResources().getDrawable(R.drawable.absent_bg));
            } else if (data.getAttendance().equalsIgnoreCase("p")) {
                holder.txtAttend.setBackground(mContext.getResources().getDrawable(R.drawable.present_bg));
            } else if (data.getAttendance().equalsIgnoreCase("l")) {
                if (data.getAt_staff_remarks() != null) {
                    if (data.getAt_staff_remarks().equalsIgnoreCase("hlp") || data.getAt_staff_remarks().equalsIgnoreCase("hlu")
                            || data.getAt_staff_remarks().equalsIgnoreCase("HL")) {
                        holder.txtAttend.setBackground(mContext.getResources().getDrawable(R.drawable.half_leave_bg));
                    } else {
                        holder.txtAttend.setBackground(mContext.getResources().getDrawable(R.drawable.leave_bg));
                    }
                } else {
                    holder.txtAttend.setBackground(mContext.getResources().getDrawable(R.drawable.leave_bg));
                }
            } else {
                holder.txtAttend.setBackground(mContext.getResources().getDrawable(R.drawable.white_bg));
            }

            String attend = "N/A";

            if (data.getAttendance().equalsIgnoreCase("a")) {
                attend = "Absent";
            } else if (data.getAttendance().equalsIgnoreCase("p")) {
                attend = "Present";
            } else if (data.getAttendance().equalsIgnoreCase("l")) {
                if (data.getAt_staff_remarks() != null) {
                    if (data.getAttendance().equalsIgnoreCase("l") && data.getAt_staff_remarks().equalsIgnoreCase("CL")) {
                        attend = "Leave(CL)";
                    } else if (data.getAttendance().equalsIgnoreCase("l") && data.getAt_staff_remarks().equalsIgnoreCase("HL")) {
                        attend = "Leave(HL)";
                    } else if (data.getAttendance().equalsIgnoreCase("l") && data.getAt_staff_remarks().equalsIgnoreCase("OTL")) {
                        attend = "Leave(OTL)";
                    } else if (data.getAttendance().equalsIgnoreCase("l") && data.getAt_staff_remarks().equalsIgnoreCase("clu")) {
                        attend = "Leave(CLU)";
                    } else if (data.getAttendance().equalsIgnoreCase("l") && data.getAt_staff_remarks().equalsIgnoreCase("clp")) {
                        attend = "Leave(CLP)";
                    } else if (data.getAttendance().equalsIgnoreCase("l") && data.getAt_staff_remarks().equalsIgnoreCase("hlp")) {
                        attend = "Leave(HLP)";
                    } else if (data.getAttendance().equalsIgnoreCase("l") && data.getAt_staff_remarks().equalsIgnoreCase("hlu")) {
                        attend = "Leave(HLU)";
                    }
                } else {
                    attend = "Leave(CL)";
                }
            } else {
                attend = "N/A";
            }
            holder.txtAttend.setText(attend);
        }

    }

    @Override
    public int getItemCount() {
        return (dataList.size() + 1);
    }

    public ArrayList<StaffAttendBean> getAttendList() {
        return dataList;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txtAttend)
        TextView txtAttend;
        @BindView(R.id.txtName)
        TextView txtName;
        @BindView(R.id.txtRoll)
        TextView txtRoll;
        @BindView(R.id.lytData)
        LinearLayout lytData;
        @BindView(R.id.lytHeader)
        LinearLayout lytHeader;
        @BindView(R.id.txtsno)
        TextView txtsno;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }


}
