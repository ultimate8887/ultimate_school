package com.ultimate.ultimatesmartschool.StaffAttendance;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class StaffMonthBean {

    private String staff_id;
    private String staff_name;
    private String p;
    private String l;
    private String a;
    private static String STAFF_DESG_NAME = "staff_desg_name";

    private String hl;
    private String sal;
    private String cl;

    public String getHl() {
        return hl;
    }

    public void setHl(String hl) {
        this.hl = hl;
    }

    public String getSal() {
        return sal;
    }

    public void setSal(String sal) {
        this.sal = sal;
    }

    public String getCl() {
        return cl;
    }

    public void setCl(String cl) {
        this.cl = cl;
    }

    public String getOtl() {
        return otl;
    }

    public void setOtl(String otl) {
        this.otl = otl;
    }

    private String otl;

    private String father_name;

    public String getFather_name() {
        return father_name;
    }

    public void setFather_name(String father_name) {
        this.father_name = father_name;
    }

    public String getTeach_nonteach() {
        return teach_nonteach;
    }

    public void setTeach_nonteach(String teach_nonteach) {
        this.teach_nonteach = teach_nonteach;
    }

    public String getStaff_desg_name() {
        return staff_desg_name;
    }

    public void setStaff_desg_name(String staff_desg_name) {
        this.staff_desg_name = staff_desg_name;
    }

    public String getSch_wd() {
        return sch_wd;
    }

    public void setSch_wd(String sch_wd) {
        this.sch_wd = sch_wd;
    }

    public String getStd_wd() {
        return std_wd;
    }

    public void setStd_wd(String std_wd) {
        this.std_wd = std_wd;
    }

    /**
     * father_name : sd
     */

    private String sch_wd;
    private String std_wd;

    private String teach_nonteach;
    private String staff_desg_name;


    public String getStaff_id() {
        return staff_id;
    }

    public void setStaff_id(String staff_id) {
        this.staff_id = staff_id;
    }

    public String getStaff_name() {
        return staff_name;
    }

    public void setStaff_name(String staff_name) {
        this.staff_name = staff_name;
    }

    public String getP() {
        return p;
    }

    public void setP(String p) {
        this.p = p;
    }

    public String getL() {
        return l;
    }

    public void setL(String l) {
        this.l = l;
    }

    public String getA() {
        return a;
    }

    public void setA(String a) {
        this.a = a;
    }

    public static ArrayList<StaffMonthBean> parseAttendArray(JSONArray arrayObj) {
        ArrayList list = new ArrayList<StaffMonthBean>();
        try {
            for (int i = 0; i < arrayObj.length(); i++) {
                StaffMonthBean p = parseAttendObject(arrayObj.getJSONObject(i));
                if (p != null) {
                    list.add(p);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public static StaffMonthBean parseAttendObject(JSONObject jsonObject) {
        StaffMonthBean casteObj = new StaffMonthBean();
        try {
            if (jsonObject.has("staff_id")) {
                casteObj.setStaff_id(jsonObject.getString("staff_id"));
            }
            if (jsonObject.has("staff_name") && !jsonObject.getString("staff_name").isEmpty() && !jsonObject.getString("staff_name").equalsIgnoreCase("null")) {
                casteObj.setStaff_name(jsonObject.getString("staff_name"));
            }
            if (jsonObject.has("a")) {
                casteObj.setA(jsonObject.getString("a"));
            }
            if (jsonObject.has("l")) {
                casteObj.setL(jsonObject.getString("l"));
            }
            if (jsonObject.has("p")) {
                casteObj.setP(jsonObject.getString("p"));
            }

            if (jsonObject.has(STAFF_DESG_NAME) && !jsonObject.getString(STAFF_DESG_NAME).isEmpty() && !jsonObject.getString(STAFF_DESG_NAME).equalsIgnoreCase("null")) {
                casteObj.setStaff_desg_name(jsonObject.getString(STAFF_DESG_NAME));
            }

            if (jsonObject.has("father_name") && !jsonObject.getString("father_name").isEmpty() && !jsonObject.getString("father_name").equalsIgnoreCase("null")) {
                casteObj.setFather_name(jsonObject.getString("father_name"));
            }
            if (jsonObject.has("teach_nonteach") && !jsonObject.getString("teach_nonteach").isEmpty() && !jsonObject.getString("teach_nonteach").equalsIgnoreCase("null")) {
                casteObj.setTeach_nonteach(jsonObject.getString("teach_nonteach"));
            }

            if (jsonObject.has("std_wd")) {
                casteObj.setStd_wd(jsonObject.getString("std_wd"));
            }
            if (jsonObject.has("sch_wd")) {
                casteObj.setSch_wd(jsonObject.getString("sch_wd"));
            }

            if (jsonObject.has("hl")) {
                casteObj.setHl(jsonObject.getString("hl"));
            }
            if (jsonObject.has("otl")) {
                casteObj.setOtl(jsonObject.getString("otl"));
            }
            if (jsonObject.has("cl")) {
                casteObj.setCl(jsonObject.getString("cl"));
            }
            if (jsonObject.has("sal")) {
                casteObj.setSal(jsonObject.getString("sal"));
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }
        return casteObj;
    }


}
