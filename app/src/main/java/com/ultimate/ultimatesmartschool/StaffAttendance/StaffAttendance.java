package com.ultimate.ultimatesmartschool.StaffAttendance;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetSequence;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.ultimate.ultimatesmartschool.BeanModule.CommonBean;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.CommonProgress;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;
import com.ultimate.ultimatesmartschool.Utility.ErpProgressVert;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class StaffAttendance extends AppCompatActivity {


    ArrayList<CommonBean> departmentList = new ArrayList<>();
    @BindView(R.id.parent)
    RelativeLayout parent;
    //@BindView(R.id.spinner) Spinner spinnerClass;
    String classid = "";
    int check=0;
    private int loaded = 0;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.txtSub)
    TextView txtSub;
    List<String> classidsel;
    //    @BindView(R.id.textView7)
//    TextView textView7;
//    @BindView(R.id.spinnersection)

    String sectionid="";
    String sectionname;
    @BindView(R.id.adddate)TextView adddate;
    public String date;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.textNorecord)
    TextView txtNorecord;
    private ArrayList<StaffAttendBean> dataList;
    private StaffAttendAdapter adapter;
    @BindView(R.id.btnSubmit)
    Button btnSubmit;
    @BindView(R.id.sc)
    CardView scrollView;
    @BindView(R.id.totalRecord)
    TextView totalRecord;
    //    @BindView(R.id.spinnerClass)
    Spinner spinnerDepartment;
    CommonProgress commonProgress;

    @BindView(R.id.dialog)
    ImageView dialog;
    @BindView(R.id.filelayout)
    LinearLayout filelayout;
    SharedPreferences sharedPreferences;
    BottomSheetDialog mBottomSheetDialog;
    String sub_id= "", sub_name= "" ,className = "";
    @RequiresApi(api = Build.VERSION_CODES.N)

//    @BindView(R.id.spinnerClass)
//    Spinner spinnerDepartment;
//    private ArrayList<CommonBean> departmentList;
    private String departmentid="",departmentname="";
//    @BindView(R.id.parent)
//    RelativeLayout parent;
//    @BindView(R.id.adddate)
//    TextView edtDate;
//    public String date;
//    @BindView(R.id.recyclerView)
//    RecyclerView recyclerView;
//    private ArrayList<StaffAttendBean> dataList;
//    @BindView(R.id.btnSubmit)
//    Button btnSubmit;
//    private StaffAttendAdapter adapter;
//    @BindView(R.id.txtTitle)
//    TextView txtTitle;
//    StafflistspinAdapter adapterstafflist;
//    Spinner deprtment;
//    Spinner stafflists;
//    String atensspindeprtid;
//    String staffid;
//    ArrayList<Stafflistbean> stafflist = new ArrayList<>();
//    @BindView(R.id.sc)
//    ScrollView scrollView;
//    @BindView(R.id.textNorecord)
//    TextView txtNorecord;




    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_staff_attendance);
        ButterKnife.bind(this);

        commonProgress=new CommonProgress(this);
        classidsel = new ArrayList<>();
        dataList = new ArrayList<>();
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new StaffAttendAdapter(this, dataList);
        recyclerView.setAdapter(adapter);
        // fetchClass();
//        recyclerView.setAdapter(adapter);
        if (!restorePrefData()){
            setShowcaseView();
        }else {
            openDialog();
        }
        getTodayDate();

//        dataList = new ArrayList<>();
//        txtTitle.setText("Staff Attendance");
//        recyclerView.setLayoutManager(new LinearLayoutManager(this));
//        adapter = new StaffAttendAdapter(this, dataList);
//        recyclerView.setAdapter(adapter);
//        fetchDepartment();
//        getTodayDate();
    }

    private void setShowcaseView_aday() {
        new TapTargetSequence(this)
                .targets(TapTarget.forView(filelayout,"Date Button!","Tap the Date button to Mark Day-Wise Attendance.")
                        .outerCircleColor(R.color.light)
                        .outerCircleAlpha(0.96f)
                        .targetCircleColor(R.color.white)
                        .titleTextSize(20)
                        .titleTextColor(R.color.white)
                        .descriptionTextSize(14)
                        .descriptionTextColor(R.color.black)
                        .textColor(R.color.black)
                        .textTypeface(Typeface.SANS_SERIF)
                        .dimColor(R.color.black)
                        .drawShadow(true)
                        .cancelable(false)
                        .tintTarget(true)
                        .transparentTarget(true)
                        .targetRadius(60)).listener(new TapTargetSequence.Listener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onSequenceFinish() {
                // Toast.makeText(getApplicationContext(),"Sequence Finished",Toast.LENGTH_SHORT).show();
                savePrefData_aday();
                //openDialog();
            }

            @Override
            public void onSequenceStep(TapTarget lastTarget, boolean targetClicked) {
                // Toast.makeText(getActivity(),"GREAT!",Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSequenceCanceled(TapTarget lastTarget) {

            }
        }).start();
    }


    private void setShowcaseView() {
        new TapTargetSequence(this)
                .targets(TapTarget.forView(dialog,"Search Button!","Tap the Search button to Search Department-Wise Staff.")
                        .outerCircleColor(R.color.light)
                        .outerCircleAlpha(0.96f)
                        .targetCircleColor(R.color.white)
                        .titleTextSize(20)
                        .titleTextColor(R.color.white)
                        .descriptionTextSize(14)
                        .descriptionTextColor(R.color.black)
                        .textColor(R.color.black)
                        .textTypeface(Typeface.SANS_SERIF)
                        .dimColor(R.color.black)
                        .drawShadow(true)
                        .cancelable(false)
                        .tintTarget(true)
                        .transparentTarget(true)
                        .targetRadius(60)).listener(new TapTargetSequence.Listener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onSequenceFinish() {
                // Toast.makeText(getApplicationContext(),"Sequence Finished",Toast.LENGTH_SHORT).show();
                savePrefData();
                openDialog();
            }

            @Override
            public void onSequenceStep(TapTarget lastTarget, boolean targetClicked) {
                // Toast.makeText(getActivity(),"GREAT!",Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSequenceCanceled(TapTarget lastTarget) {

            }
        }).start();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        commonBack();
    }

    private void commonBack() {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(StaffAttendance.this);
        builder1.setMessage("Are you sure, you want to exit? ");
        builder1.setCancelable(false);
        builder1.setPositiveButton(
                "Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finish();
                    }
                });
        builder1.setNegativeButton(
                "No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

    private void savePrefData_aday(){
        sharedPreferences=this.getSharedPreferences("boarding_pref_add_attend_aday",MODE_PRIVATE);
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putBoolean("IsFirstTimeVisit_add_attend_aday",true);
        editor.apply();
    }

    private boolean restorePrefData_aday(){
        sharedPreferences = this.getSharedPreferences("boarding_pref_add_attend_aday",MODE_PRIVATE);
        return sharedPreferences.getBoolean("IsFirstTimeVisit_add_attend_aday",false);
    }

    private void savePrefData(){
        sharedPreferences=this.getSharedPreferences("boarding_pref_add_attend",MODE_PRIVATE);
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putBoolean("IsFirstTimeVisit_add_attend",true);
        editor.apply();
    }

    private boolean restorePrefData(){
        sharedPreferences = this.getSharedPreferences("boarding_pref_add_attend",MODE_PRIVATE);
        return sharedPreferences.getBoolean("IsFirstTimeVisit_add_attend",false);
    }


    @RequiresApi(api = Build.VERSION_CODES.N)
    private void openDialog() {

        mBottomSheetDialog = new BottomSheetDialog(this);
        final View sheetView = getLayoutInflater().inflate(R.layout.search_dialog, null);
        mBottomSheetDialog.setContentView(sheetView);
        mBottomSheetDialog.setCancelable(false);

        spinnerDepartment=(Spinner) sheetView.findViewById(R.id.spinnerClass);
        RelativeLayout spinnersection=(RelativeLayout) sheetView.findViewById(R.id.sectionlayout);
        TextView textView7=(TextView) sheetView.findViewById(R.id.textView7);
        textView7.setText("Select Department");
        spinnersection.setVisibility(View.GONE);
        ImageView close=(ImageView) sheetView.findViewById(R.id.close);

        getTodayDate();
        //txtTitle.setText("Mark Attendance");
        fetchDepartment();

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //assignData();
                if (check==0) {
                    finish();
                    mBottomSheetDialog.dismiss();
                }else {
                    mBottomSheetDialog.dismiss();
                }
            }
        });


        Button btnYes= (Button) sheetView.findViewById(R.id.btnYes);


        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (departmentid.equalsIgnoreCase(""))  {
                    Toast.makeText(StaffAttendance.this,"Kindly Select Department!", Toast.LENGTH_SHORT).show();
                }  else {
                    check++;
                    searchStudent(date);
                    setCommonData();
                }

            }
        });
        mBottomSheetDialog.show();

    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void setCommonData() {
        filelayout.setVisibility(View.VISIBLE);

        if (!restorePrefData_aday()){
            setShowcaseView_aday();
        }

        txtTitle.setText(departmentname);
        //  txtSub.setText(sub_name);
    }



    @RequiresApi(api = Build.VERSION_CODES.N)
    @OnClick(R.id.dialog)
    public void dialogggg() {
        openDialog();
    }

    @OnClick(R.id.imgBack)
    public void onback() {
        commonBack();
    }



//    private boolean checkValid() {
//
//        boolean valid = true;
//        String errorMsg = null;
//        if (atensspindeprtid.isEmpty() || atensspindeprtid == "") {
//            valid = false;
//            errorMsg = "Please select Department";
//        } else if (staffid.isEmpty() || staffid == "") {
//            valid = false;
//            errorMsg = "Please select Staff";
//        }
//
//        if (!valid) {
//            Utils.showSnackBar(errorMsg, parent);
//        }
//        return valid;
//    }

//    private void fetchDepartmentsec() {
//        ErpProgress.showProgressBar(this, "Please wait...");
//        HashMap<String, String> params = new HashMap<String, String>();
//        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.DEPARTMENT_LIST_URL, departapicallback, this, params);
//    }

//    ApiHandler.ApiCallback departapicallback = new ApiHandler.ApiCallback() {
//        @Override
//        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
//            ErpProgress.cancelProgressBar();
//            if (error == null) {
//                try {
//                    departmentList = CommonBean.parseCommonArray(jsonObject.getJSONArray("department_data"));
//                    DepartmentAdapter adapter = new DepartmentAdapter(StaffAttendance.this, departmentList);
//                    deprtment.setAdapter(adapter);
//                    deprtment.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//                        @Override
//                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
//                            if (adapterstafflist != null) {
//                                stafflist.clear();
//                                adapterstafflist.notifyDataSetChanged();
//                            }
//
//                            if (i > 0) {
//                                atensspindeprtid = departmentList.get(i - 1).getId();
//                                staffid = "";
//                                fetchstaff(atensspindeprtid);
//                            } else {
//                                atensspindeprtid = "";
//                            }
//                        }
//
//                        @Override
//                        public void onNothingSelected(AdapterView<?> adapterView) {
//
//                        }
//                    });
//
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            } else {
//                Utils.showSnackBar(error.getMessage(), parent);
//            }
//        }
//    };


//    public void fetchstaff(String atensspindeprtid) {
//        ErpProgress.showProgressBar(this, "Please wait...");
//        HashMap<String, String> params = new HashMap<String, String>();
//        params.put("d_id", atensspindeprtid);
//        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.Staff_LIST_URL, stafflistCallback, this, params);
//    }
//
//    ApiHandler.ApiCallback stafflistCallback = new ApiHandler.ApiCallback() {
//        @Override
//        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
//            ErpProgress.cancelProgressBar();
//            if (error == null) {
//                try {
//                    stafflist = Stafflistbean.parseCommonstaffArray(jsonObject.getJSONArray("staff_detaillist"));
//                    adapterstafflist = new StafflistspinAdapter(StaffAttendance.this, stafflist);
//                    stafflists.setAdapter(adapterstafflist);
//                    stafflists.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//                        @Override
//                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
//                            if (i > 0) {
//                                staffid = stafflist.get(i - 1).getId();
//                            } else {
//                                staffid = "";
//                            }
//                        }
//
//                        @Override
//                        public void onNothingSelected(AdapterView<?> adapterView) {
//
//                        }
//                    });
//
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            } else {
//                Utils.showSnackBar(error.getMessage(), parent);
//            }
//        }
//    };


    private void fetchDepartment() {
        commonProgress.show();
        HashMap<String, String> params = new HashMap<String, String>();
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.DEPARTMENT_LIST_URL, classapiCallback, this, params);
    }


    ApiHandler.ApiCallback classapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            commonProgress.dismiss();
            if (error == null) {
                try {
                    departmentList = CommonBean.parseCommonArray(jsonObject.getJSONArray("department_data"));
                    DepartmentAdapter adapter = new DepartmentAdapter(StaffAttendance.this, departmentList);
                    spinnerDepartment.setAdapter(adapter);
                    spinnerDepartment.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @RequiresApi(api = Build.VERSION_CODES.N)
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if (i > 0) {
                                departmentid = departmentList.get(i - 1).getId();
                                departmentname = departmentList.get(i - 1).getName();
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Utils.showSnackBar(error.getMessage(), parent);
            }
        }
    };

    @OnClick(R.id.filelayout)
    public void fetchDate() {
        java.util.Calendar cal = java.util.Calendar.getInstance(TimeZone.getDefault()); // Get current date
        DatePickerDialog datePicker = new DatePickerDialog(this,
                R.style.MyDatePickerDialogTheme, ondate,
                cal.get(java.util.Calendar.YEAR),
                cal.get(java.util.Calendar.MONTH),
                cal.get(java.util.Calendar.DAY_OF_MONTH));
        datePicker.getDatePicker().setMaxDate(new Date().getTime());
        datePicker.setCancelable(false);
        datePicker.show();
    }

    DatePickerDialog.OnDateSetListener ondate = new DatePickerDialog.OnDateSetListener() {
        @RequiresApi(api = Build.VERSION_CODES.N)
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            java.util.Calendar calendar = java.util.Calendar.getInstance(TimeZone.getDefault());
            calendar.set(year, monthOfYear, dayOfMonth);
            calendar.set(Calendar.HOUR_OF_DAY, 00);
            calendar.set(Calendar.MINUTE, 00);
            calendar.set(Calendar.SECOND, 00);
            Date setdate = calendar.getTime();
            SimpleDateFormat fmtOut = new SimpleDateFormat("dd MMM, yyyy");
            String dateString = fmtOut.format(setdate);
            adddate.setText(dateString);
            SimpleDateFormat dateFrmOut = new SimpleDateFormat("yyyy-MM-dd");
            date = dateFrmOut.format(setdate);
            searchStudent(date);
        }
    };

    public void getTodayDate() {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.HOUR_OF_DAY, 00);
        c.set(Calendar.MINUTE, 00);
        c.set(Calendar.SECOND, 00);
        Date setdate = c.getTime();
        SimpleDateFormat fmtOut = new SimpleDateFormat("dd MMM, yyyy");
        String dateString = fmtOut.format(setdate);
        adddate.setText(dateString);
        SimpleDateFormat dateFrmOut = new SimpleDateFormat("yyyy-MM-dd");
        date = dateFrmOut.format(setdate);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)

    public void searchStudent(String date) {
        if (departmentid.equalsIgnoreCase("")) {
            Utils.showSnackBar("Please select department", parent);
        } else {
            dataList.clear();
            adapter.notifyDataSetChanged();
           commonProgress.show();
            HashMap<String, String> params = new HashMap<String, String>();
            params.put("depart_id", departmentid);
            if (date != null) {
                params.put("mdate", date);
            }
            ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.STAFF_ATTEND_URL, studentapiCallback, this, params);
        }
    }

    ApiHandler.ApiCallback studentapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            mBottomSheetDialog.dismiss();
            commonProgress.dismiss();
            if (error == null) {
                try {
                    ArrayList<StaffAttendBean> staffdattendList = new ArrayList<>();
                    ArrayList<StaffAttendBean> attendList = StaffAttendBean.parseAttendArray(jsonObject.getJSONArray(Constants.ATTEND_DATA));
                    if (jsonObject.has("staff_data")) {
                        staffdattendList = StaffAttendBean.parseAttendArray(jsonObject.getJSONArray("staff_data"));
                        if (staffdattendList.size() > attendList.size()) {
                            for (int i = 0; i < staffdattendList.size(); i++) {
                                boolean check = true;
                                for (int j = 0; j < attendList.size(); j++) {
                                    if (staffdattendList.get(i).getStaff_id().equals(attendList.get(j).getStaff_id())) {
                                        check = false;
                                        break;
                                    }
                                }
                                if (check) {
                                    attendList.add(staffdattendList.get(i));
                                }
                            }
                        } else {
                            for (int i = 0; i < attendList.size(); i++) {
                                boolean check = true;
                                for (int j = 0; j < staffdattendList.size(); j++) {
                                    if (staffdattendList.get(j).getStaff_id().equals(attendList.get(i).getStaff_id())) {
                                        check = false;
                                        break;
                                    }
                                }
                                if (check) {
                                    attendList.remove(i);
                                    i--;
                                }
                            }
                        }
                    }

                    dataList.clear();
                    dataList.addAll(attendList);
                    scrollView.setVisibility(View.VISIBLE);
                    txtNorecord.setVisibility(View.GONE);
                    adapter.notifyDataSetChanged();
                    totalRecord.setText("Total Entries:- "+String.valueOf(attendList.size()));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                totalRecord.setText("Total Entries:- 0");
                scrollView.setVisibility(View.GONE);
                txtNorecord.setVisibility(View.VISIBLE);
                Utils.showSnackBar(error.getMessage(), parent);
            }
        }
    };

    @RequiresApi(api = Build.VERSION_CODES.N)
    @OnClick(R.id.btnSubmit)
    public void submitAttendance() {
        if (departmentid.equalsIgnoreCase("")) {
            Utils.showSnackBar("Please select department", parent);
        } else {
            addAttend();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void addAttend() {
        commonProgress.show();
        ArrayList<String> attendList = new ArrayList<>();
        ArrayList<String> id = new ArrayList<>();
        ArrayList<String> st_id = new ArrayList<>();
        ArrayList<String> st_name = new ArrayList<>();
        ArrayList<String> st_des = new ArrayList<>();
        ArrayList<String> remark = new ArrayList<>();
        for (int i = 0; i < dataList.size(); i++) {
            attendList.add(dataList.get(i).getAttendance());
            st_id.add(dataList.get(i).getStaff_id());
            id.add(dataList.get(i).getId());
            st_name.add(dataList.get(i).getStaff_name());
            st_des.add(dataList.get(i).getStaff_desg());
            remark.add(dataList.get(i).getAttend_remark());
        }
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("attend", String.valueOf(attendList));
        params.put("st_id", String.valueOf(st_id));
        params.put("id", String.valueOf(id));
        params.put("st_name", String.valueOf(st_name));
        params.put("depart_id", departmentid);
        params.put("date", date);
        params.put("desg_id", String.valueOf(st_des));
        params.put("remark", String.valueOf(remark));
        Log.e("value", attendList + "");
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.ADD_STAFF_ATTEND_URL, apiCallback, this, params);

    }

    ApiHandler.ApiCallback apiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgressVert.cancelProgressBar();
            commonProgress.dismiss();
            if (error == null) {
                try {
                    Toast.makeText(StaffAttendance.this, jsonObject.getString("msg"), Toast.LENGTH_SHORT).show();


                    finish();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("error", error.getMessage() + "");
                Utils.showSnackBar(error.getMessage(), parent);
            }
        }
    };


    @OnClick(R.id.imgBack)
    public void onBackclick() {
        finish();
    }

}
