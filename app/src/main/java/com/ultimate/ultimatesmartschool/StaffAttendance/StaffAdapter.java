package com.ultimate.ultimatesmartschool.StaffAttendance;

import android.content.Context;
import android.content.DialogInterface;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.squareup.picasso.Picasso;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Utility.Utils;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;

public class StaffAdapter extends RecyclerSwipeAdapter<StaffAdapter.Viewholder> implements Filterable {
    ArrayList<StaffAttendBean> dataList;
    private ArrayList<StaffAttendBean> filterModelClass;
    Context mContext;


    public StaffAdapter(ArrayList<StaffAttendBean> dataList, Context mContext) {
        this.mContext = mContext;
        this.dataList = dataList;
        this.filterModelClass = dataList;
    }

    @Override
    public StaffAdapter.Viewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.staff_item_news, parent, false);
        StaffAdapter.Viewholder viewholder = new StaffAdapter.Viewholder(view);
        return viewholder;
    }

    @Override
    public void onBindViewHolder(final StaffAdapter.Viewholder holder, final int position) {

        final StaffAttendBean data = dataList.get(position);
        holder.spinnerAttend.setVisibility(View.GONE);
        if (dataList.get(position).getGender().equalsIgnoreCase("Male")) {
            if (dataList.get(position).getProfile() != null) {
                Picasso.get().load(dataList.get(position).getProfile()).placeholder(R.drawable.stud).into(holder.circleimg);
            } else {
                Picasso.get().load(R.drawable.stud).into(holder.circleimg);
            }
        }else{
            if (dataList.get(position).getProfile() != null) {
                Picasso.get().load(dataList.get(position).getProfile()).placeholder(R.drawable.f_student).into(holder.circleimg);
            } else {
                Picasso.get().load(R.drawable.f_student).into(holder.circleimg);
            }
        }




//        holder.txtName.setText(data.getStudent_name());
//        holder.txtRegNo.setText("Father: "+data.getStudent_id());
//        holder.txtFname.setText("Father: "+data.getFather_name());

        //  holder.employee_designation.setText("Designation: " + " " + viewstafflist.get(position).getDesignation());
        if (data.getStaff_name() != null) {
            String title = getColoredSpanned("Name", "#000000");
            String Name = getColoredSpanned(data.getStaff_name(), "#383737");
            holder.txtName.setText(Html.fromHtml(title + " " + Name));
        }
        else {
            holder.txtName.setText("Name: Not Mentioned");
        }

        //  holder.employee_designation.setText("Designation: " + " " + viewstafflist.get(position).getDesignation());
        if (data.getStaff_id() != null) {
            String title = getColoredSpanned("Staff ID:", "#000000");
            String Name = getColoredSpanned(data.getStaff_id(), "#383737");
            holder.txtRegNo.setText(Html.fromHtml(title + " " + Name));
        }
        else {
            holder.txtRegNo.setText("ID: Not Mentioned");
        }

        //  holder.employee_designation.setText("Designation: " + " " + viewstafflist.get(position).getDesignation());
        if (data.getFather_name() != null) {
            String title = getColoredSpanned("Father: ", "#000000");
            String Name = getColoredSpanned(data.getFather_name(), "#383737");
            holder.txtFname.setText(Html.fromHtml(title + " " + Name));
        }
        else {
            holder.txtFname.setText("Father: Not Mentioned");
        }

        if (data.getStaff_desg_name()!=null){
            holder.txtDT.setText(data.getStaff_desg_name());
        }else {
            holder.txtDT.setText("Not Mentioned");
        }


        String title2 = getColoredSpanned("IN- ", "#000000");
        String Name2 = getColoredSpanned(Utils.getDateTimeFormatedWithAMPM(dataList.get(position).getAt_attendance_date()), "#F4212C");
        holder.check_in.setText(Html.fromHtml(title2 + " " + Name2));

        if (data.getAt_status().equalsIgnoreCase("check_out")){
            holder.done.setVisibility(View.VISIBLE);
            holder.txtcheckOut.setVisibility(View.VISIBLE);
            holder.processing.setVisibility(View.GONE);

            String title1 = getColoredSpanned("OUT- ", "#000000");
            String Name1 = getColoredSpanned(Utils.getDateTimeFormatedWithAMPM(dataList.get(position).getCheckout_time()), "#1C8B3B");
            holder.txtcheckOut.setText(Html.fromHtml(title1 + " " + Name1));
        }else {
            holder.done.setVisibility(View.GONE);
            holder.txtcheckOut.setVisibility(View.GONE);
            holder.processing.setVisibility(View.VISIBLE);

        }


    }

    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }

    @Override
    public int getItemCount() {
        if (dataList != null)
            return dataList.size();
        return 0;
    }



    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe;
    }

    public void setHQList(ArrayList<StaffAttendBean> stulist) {
        this.dataList = stulist;
    }

    public class Viewholder extends RecyclerView.ViewHolder {
        @BindView(R.id.txtRegNo)
        TextView txtRegNo;
        @BindView(R.id.txtName)
        TextView txtName;
        @BindView(R.id.spinnerAttend)
        Spinner spinnerAttend;
        @BindView(R.id.txtFname)
        TextView txtFname;
        @BindView(R.id.circleimg)
        CircularImageView circleimg;
        @BindView(R.id.txtReason)
        EditText check_in;
        @BindView(R.id.txtDT)
        TextView txtDT;

        @BindView(R.id.txtcheckOut)
        EditText txtcheckOut;
        @BindView(R.id.done)
        ImageView done;
        @BindView(R.id.processing)
        ImageView processing;

        public Viewholder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @Override
    public Filter getFilter() {

        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {

                String Key = constraint.toString();
                if (Key.isEmpty()) {

                    filterModelClass = dataList ;

                }
                else {
                    ArrayList<StaffAttendBean> lstFiltered = new ArrayList<>();
                    for (StaffAttendBean row : dataList) {

                        if (row.getStaff_name().toLowerCase().contains(Key.toLowerCase())){
                            lstFiltered.add(row);
                        }

                    }

                    filterModelClass = lstFiltered;

                }

                FilterResults filterResults = new FilterResults();
                filterResults.values= filterModelClass;
                return filterResults;

            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {

                dataList = (ArrayList<StaffAttendBean>) results.values;
                notifyDataSetChanged();

            }
        };

    }
}
