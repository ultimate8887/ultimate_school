package com.ultimate.ultimatesmartschool.BlockStudent;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.daimajia.swipe.SimpleSwipeListener;
import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.squareup.picasso.Picasso;
import com.ultimate.ultimatesmartschool.BeanModule.Studentbean;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Student.NewsAdapter;

import java.util.ArrayList;

public class BlockListAdapter extends RecyclerSwipeAdapter<BlockListAdapter.NewsViewHolder> implements Filterable {
    Context mContext;
    public ArrayList<Studentbean> mData;
    private ArrayList<Studentbean> mDataFiltered;
    boolean isDark = false;
    BlockListAdapter.StudentPro mStudentPro;



    public BlockListAdapter(Context mContext, ArrayList<Studentbean> mData, BlockListAdapter.StudentPro mStudentPro) {
        this.mContext = mContext;
        this.mData = mData;
        this.mDataFiltered = mData;
        this.mStudentPro=mStudentPro;
    }

    @NonNull
    @Override
    public BlockListAdapter.NewsViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View layout;
        layout = LayoutInflater.from(mContext).inflate(R.layout.blockitem_news,viewGroup,false);
        return new BlockListAdapter.NewsViewHolder(layout);
    }

    @Override
    public void onBindViewHolder(@NonNull BlockListAdapter.NewsViewHolder newsViewHolder, int position) {


        newsViewHolder.circleimg.setAnimation(AnimationUtils.loadAnimation(mContext,R.anim.fade_transition_animation));
        newsViewHolder.container.setAnimation(AnimationUtils.loadAnimation(mContext,R.anim.fade_scale_animation));

        if(mData.get(position).getProfile()!=null){
            Picasso.get().load(mData.get(position).getProfile()).placeholder(R.drawable.stud).into(newsViewHolder.circleimg);
        }else {
            Picasso.get().load(R.drawable.stud).into(newsViewHolder.circleimg);
        }

        if(mData.get(position).getCroll_no()!=null) {
            newsViewHolder.textViewData.setText("Name:" + mData.get(position).getName() + "(" + mData.get(position).getCroll_no() + ")");
        }else{
            newsViewHolder.textViewData.setText("Name:" + mData.get(position).getName());
        }
        newsViewHolder.txtMobile.setText("Mobile no.:"+mData.get(position).getPhoneno());
        newsViewHolder.txtFather.setText("Father's Name:"+ mData.get(position).getFather_name());
        if(mData.get(position).getSection_name()!=null){
            newsViewHolder.txtClass.setText("Class:"+mData.get(position).getClass_name()+"("+mData.get(position).getSection_name()+")");
        }else{
            newsViewHolder.txtClass.setText("Class:"+mData.get(position).getClass_name());
        }


        mItemManger.bindView(newsViewHolder.itemView, position);
        newsViewHolder.trash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                AlertDialog.Builder builder1 = new AlertDialog.Builder(mContext);
                builder1.setMessage("Do you want to Unblock? ");
                builder1.setCancelable(false);
                builder1.setPositiveButton(
                        "Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                                if (mStudentPro != null) {
                                    mStudentPro.onUnblockCallback(mData.get(position));

                                }
                            }
                        });
                builder1.setNegativeButton(
                        "No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        });
                AlertDialog alert11 = builder1.create();
                alert11.show();

            }
        });
        newsViewHolder.update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                Toast.makeText(view.getContext(), "clicked " + newsViewHolder.textViewData.getText().toString() + "!", Toast.LENGTH_SHORT).show();

                AlertDialog.Builder builder2 = new AlertDialog.Builder(mContext);
                builder2.setMessage("Do you want to Block? ");
                builder2.setCancelable(false);
                builder2.setPositiveButton(
                        "Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                if (mStudentPro != null) {
                                    mStudentPro.onUpdateCallback(mData.get(position));

                                }
                                mData.remove(position);
                                notifyItemRemoved(position);
                                notifyItemRangeChanged(position, mData.size());
                                mItemManger.closeAllItems();
                                Toast.makeText(view.getContext(), "Blocked " + newsViewHolder.textViewData.getText().toString() + "!", Toast.LENGTH_SHORT).show();
                            }
                        });
                builder2.setNegativeButton(
                        "No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        });
                AlertDialog alert11 = builder2.create();
                alert11.show();

            }
        });
        newsViewHolder.swipeLayout.setShowMode(SwipeLayout.ShowMode.PullOut);
        newsViewHolder.swipeLayout.addSwipeListener(new SimpleSwipeListener() {
            @Override
            public void onOpen(SwipeLayout layout) {

            }

            @Override
            public void onStartClose(SwipeLayout layout) {
                newsViewHolder.swipeLayout.close();
            }
        });
        newsViewHolder.swipeLayout.setOnDoubleClickListener(new SwipeLayout.DoubleClickListener() {
            @Override
            public void onDoubleClick(SwipeLayout layout, boolean surface) {
                Toast.makeText(mContext, "DoubleClick", Toast.LENGTH_SHORT).show();
            }
        });
        newsViewHolder.txtid.setText("ID:"+mData.get(position).getId());

        newsViewHolder.parent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mStudentPro!= null){
                    mStudentPro.StudentProfile(mData.get(position));
                }
            }
        });
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe;
    }
    public interface StudentPro{
        public void StudentProfile(Studentbean std);
        public void onUpdateCallback(Studentbean std);
        public void onUnblockCallback(Studentbean std);
    }
    @Override
    public int getItemCount() {
        return mData.size();
    }

    @Override
    public Filter getFilter() {

        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {

                String Key = constraint.toString();
                if (Key.isEmpty()) {

                    mDataFiltered = mData ;

                }
                else {
                    ArrayList<Studentbean> lstFiltered = new ArrayList<>();
                    for (Studentbean row : mData) {

                        if (row.getName().toLowerCase().contains(Key.toLowerCase())){
                            lstFiltered.add(row);
                        }

                    }

                    mDataFiltered = lstFiltered;

                }


                FilterResults filterResults = new FilterResults();
                filterResults.values= mDataFiltered;
                return filterResults;

            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {


                mData = (ArrayList<Studentbean>) results.values;
                notifyDataSetChanged();

            }
        };


    }

    public void setHQList(ArrayList<Studentbean> mData) {
        this.mData = mData;
    }

    public class NewsViewHolder extends RecyclerView.ViewHolder {

        SwipeLayout swipeLayout;
        TextView textViewData, txtMobile, txtFather, txtClass,txtid;
        ImageView trash,update;
        RelativeLayout parent;
        CircularImageView circleimg;
        CardView container;

        public NewsViewHolder(@NonNull View itemView) {
            super(itemView);

            if (isDark) {
                setDarkTheme();
            }

            container =(CardView)itemView.findViewById(R.id.container);
            parent =(RelativeLayout)itemView.findViewById(R.id.parent);
            circleimg=(CircularImageView)itemView.findViewById(R.id.circleimg);
            swipeLayout = (SwipeLayout) itemView.findViewById(R.id.swipe);
            textViewData = (TextView) itemView.findViewById(R.id.name);
            txtFather = (TextView) itemView.findViewById(R.id.fathername);
            txtMobile = (TextView) itemView.findViewById(R.id.mobileno);
            txtClass = (TextView) itemView.findViewById(R.id.classess);
            // textViewPos = (TextView) itemView.findViewById(R.id.position);
            trash = (ImageView) itemView.findViewById(R.id.trash);
            update = (ImageView) itemView.findViewById(R.id.update);
            txtid=(TextView)itemView.findViewById(R.id.textViewid);


        }


        private void setDarkTheme() {

            container.setBackgroundResource(R.drawable.card_bg_dark);

        }



    }
}
