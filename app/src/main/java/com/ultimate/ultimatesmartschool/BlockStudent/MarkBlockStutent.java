package com.ultimate.ultimatesmartschool.BlockStudent;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.google.gson.Gson;
import com.ultimate.ultimatesmartschool.BeanModule.ClassBean;
import com.ultimate.ultimatesmartschool.BeanModule.SectionBean;
import com.ultimate.ultimatesmartschool.BeanModule.Studentbean;
import com.ultimate.ultimatesmartschool.BeanModule.User;
import com.ultimate.ultimatesmartschool.Login.LoginActivity;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Student.NewsAdapter;
import com.ultimate.ultimatesmartschool.Student.StudentList;
import com.ultimate.ultimatesmartschool.Student.StudentListBySection;
import com.ultimate.ultimatesmartschool.Student.StudentProfile;
import com.ultimate.ultimatesmartschool.Student.Studentadapter;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.apptik.widget.multiselectspinner.MultiSelectSpinner;

public class MarkBlockStutent extends AppCompatActivity implements BlockListAdapter.StudentPro{
    RecyclerView.LayoutManager layoutManager;
    @BindView(R.id.recyclerViewstu)
    RecyclerView recyclerView;
    @BindView(R.id.multiselectSpinnerclass)
    MultiSelectSpinner multiselectSpinnerclass;
    //private Studentadapter adapter;
    private BlockListAdapter adapter;
    @BindView(R.id.textNorecord)
    TextView txtNorecord;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.parent)
    RelativeLayout parent;
    ArrayList<Studentbean> hwList = new ArrayList<>();
    private int loaded = 0;
    ArrayList<ClassBean> classList = new ArrayList<>();
    List<String> classidsel;
    @BindView(R.id.textView7)TextView textView7;
    @BindView(R.id.totalRecord)
    TextView totalRecord;
    @BindView(R.id.spinnersection)
    Spinner spinnersection;
    ArrayList<SectionBean> sectionList = new ArrayList<>();
    String sectionid="";
    EditText searchInput;
    String sectionname;
    boolean isDark = false;
    @BindView(R.id.imgBack)
    ImageView back;
    CharSequence search="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mark_block_stutent);
        ButterKnife.bind(this);
        classidsel = new ArrayList<>();
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        fetchStudent();
        layoutManager = new LinearLayoutManager(MarkBlockStutent.this);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new BlockListAdapter( MarkBlockStutent.this,hwList,this);
        recyclerView.setAdapter(adapter);
        txtTitle.setText("BlockList");
        parent.setBackgroundColor(getResources().getColor(R.color.white));
        searchInput = findViewById(R.id.search_input);


        isDark = !isDark ;
        if (isDark) {

            parent.setBackgroundColor(getResources().getColor(R.color.white));


        }
        else {
            parent.setBackgroundColor(getResources().getColor(R.color.white));

        }

        if (!search.toString().isEmpty()){

            adapter.getFilter().filter(search);

        }

        searchInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if(s.length()==0)
                {
                    fetchStudent();
                }else {
                    adapter.getFilter().filter(s);
                    search = s;
                    totalRecord.setText("Search Record:- " +String.valueOf(adapter.mData.size()));
                }
//                newsAdapter.getFilter().filter(s);
//                search = s;


            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


    }

    public void fetchStudent() {

        if(loaded==0){
            ErpProgress.showProgressBar(this,"please wait...");
        }loaded++;
        HashMap<String, String> params = new HashMap<String, String>();
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.BLOCKLISTSTUDENTURL, apiCallback, this, params);

    }

    ApiHandler.ApiCallback apiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            if (error == null) {

                try {
                    if (hwList != null) {
                        hwList.clear();
                    }
                    JSONArray jsonArray = jsonObject.getJSONArray("student_list");
                    hwList = Studentbean.parseHWArray(jsonArray);
                    if (hwList.size() > 0) {
                        adapter.setHQList(hwList);
                        adapter.notifyDataSetChanged();
                        totalRecord.setText("Total Entries:- "+String.valueOf(hwList.size()));
                        txtNorecord.setVisibility(View.GONE);
                    } else {
                        adapter.setHQList(hwList);
                        adapter.notifyDataSetChanged();
                        totalRecord.setText("Total Entries:- 0");
                        txtNorecord.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                totalRecord.setText("Total Entries:- 0");
                txtNorecord.setVisibility(View.VISIBLE);
                hwList.clear();
                adapter.setHQList(hwList);
                adapter.notifyDataSetChanged();
            }
        }
    };

    @Override
    public void StudentProfile(Studentbean std) {
        Gson gson  = new Gson();
        String student = gson.toJson(std,Studentbean.class);
        Intent i = new Intent(MarkBlockStutent.this, StudentProfile.class);
        i.putExtra("student",student);
        startActivity(i);
    }

    @Override
    public void onUpdateCallback(Studentbean std) {

    }

    @Override
    public void onUnblockCallback(Studentbean std) {
        HashMap<String, String> params = new HashMap<>();
        params.put("stu_id", std.getId());
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.UNBLOCKSTUDENTURL, apisendmsgCallback, this, params);
    }

    ApiHandler.ApiCallback apisendmsgCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            if (error == null) {
                try {
                    Toast.makeText(getApplicationContext(),jsonObject.getString("msg"),Toast.LENGTH_LONG).show();
                    Utils.showSnackBar(jsonObject.getString("msg"), parent);
                    finish();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("error", error.getMessage() + "");
                Utils.showSnackBar(error.getMessage(), parent);
                // Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_LONG).show();
                if (error.getStatusCode() == 405) {
                    User.logout();
                    startActivity(new Intent(MarkBlockStutent.this, LoginActivity.class));
                    finish();
                }
            }
        }
    };
}