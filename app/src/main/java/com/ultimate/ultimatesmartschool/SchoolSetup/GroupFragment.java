package com.ultimate.ultimatesmartschool.SchoolSetup;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.ultimate.ultimatesmartschool.BeanModule.CommonBean;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.CommonProgress;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

public class GroupFragment extends Fragment implements GroupAdapter.AddNewGroup {
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
//    @BindView(R.id.lytInst)
//    LinearLayout lytInst;
    @BindView(R.id.parent)
    RelativeLayout parent;
    ArrayList<CommonBean> dataList = new ArrayList<>();
    private GroupAdapter adapter;
    Dialog dialog;
    CommonProgress commonProgress;
    public GroupFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_group, container, false);
        ButterKnife.bind(this, view);
        commonProgress=new CommonProgress(getActivity());
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        adapter = new GroupAdapter(getActivity(), dataList, this,2);
        recyclerView.setAdapter(adapter);
        updateInst();
        fetchGroupList();
        return view;
    }

    private void fetchGroupList() {
        commonProgress.show();
        HashMap<String, String> params = new HashMap<String, String>();
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.GROUP_LIST_URL, groupapiCallback, getActivity(), params);

    }

    ApiHandler.ApiCallback groupapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            commonProgress.dismiss();
            if (error == null) {
                try {
                    ArrayList<CommonBean> groupList = CommonBean.parseCommonArray(jsonObject.getJSONArray(Constants.GROUP_DATA));
                    dataList.clear();
                    dataList.addAll(groupList);
                    adapter.notifyDataSetChanged();
                    updateInst();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Utils.showSnackBar(error.getMessage(), parent);
            }
        }
    };

    private void updateInst() {
//        if (dataList.size() > 0) {
//            lytInst.setVisibility(View.VISIBLE);
//        } else {
//            lytInst.setVisibility(View.GONE);
//        }
    }

    @SuppressLint("NewApi")
    @Override
    public void addEditNewGroup(final CommonBean data, final int pos) {
        dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.add_group_dialog);
        final EditText edtTitle = (EditText) dialog.findViewById(R.id.edtTitle);
        if (pos != -1) {
            edtTitle.setText(dataList.get(pos).getName());
        }
        Button btnSubmit = (Button) dialog.findViewById(R.id.btnSubmit);
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (pos == -1) {
                    addNewGroup(edtTitle.getText().toString());
                } else {
                    editGroup(data, pos, edtTitle.getText().toString());
                }
                adapter.notifyDataSetChanged();
                dialog.dismiss();
                updateInst();
            }
        });
        dialog.show();
        Window window = dialog.getWindow();
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    }

    private void editGroup(CommonBean data, final int pos, String name) {
        HashMap<String, String> params = new HashMap<String, String>();
        params.put(Constants.NAME, name);
        params.put(Constants.ID, data.getId());
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.EDIT_GROUP_URL, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                if (error == null) {
                    try {
                        Toast.makeText(getActivity(), jsonObject.getString("msg"), Toast.LENGTH_SHORT).show();
                        CommonBean groupdata = CommonBean.parseCommonObject(jsonObject.getJSONObject(Constants.GROUP_DATA));
                        dataList.set(pos, groupdata);
                        adapter.notifyItemChanged(pos);
                        dialog.dismiss();
                        updateInst();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    Log.e("error", error.getMessage() + "");
                    Utils.showSnackBar(error.getMessage(), parent);
                }
            }
        }, getActivity(), params);

    }

    private void addNewGroup(String name) {
        HashMap<String, String> params = new HashMap<String, String>();
        params.put(Constants.NAME, name);
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.ADD_GROUP_URL, apiCallback, getActivity(), params);
    }

    ApiHandler.ApiCallback apiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            if (error == null) {
                try {
                    Toast.makeText(getActivity(), jsonObject.getString("msg"), Toast.LENGTH_SHORT).show();
                    CommonBean groupdata = CommonBean.parseCommonObject(jsonObject.getJSONObject(Constants.GROUP_DATA));
                    dataList.add(groupdata);
                    adapter.notifyDataSetChanged();
                    dialog.dismiss();
                    updateInst();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("error", error.getMessage() + "");
                Utils.showSnackBar(error.getMessage(), parent);
            }
        }
    };

    @Override
    public void deleteGroup(final CommonBean data, final int position) {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:
                        HashMap<String, String> params = new HashMap<String, String>();
                        params.put(Constants.ID, data.getId());
                        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.DEL_GROUP_URL, new ApiHandler.ApiCallback() {
                            @Override
                            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                                if (error == null) {
                                    Toast.makeText(getActivity(), "Deleted Successfully", Toast.LENGTH_SHORT).show();
                                    dataList.remove(position);
                                    adapter.notifyDataSetChanged();
                                    updateInst();
                                } else {
                                    Utils.showSnackBar(error.getMessage(), parent);
                                }
                            }
                        }, getActivity(), params);
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        //No button clicked
                        break;
                }
            }
        };
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("Are you sure you want to delete?").setPositiveButton("Yes", dialogClickListener)
                .setNegativeButton("No", dialogClickListener).show();
    }
}
