package com.ultimate.ultimatesmartschool.SchoolSetup;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.ultimate.ultimatesmartschool.BeanModule.ClassBean;
import com.ultimate.ultimatesmartschool.BeanModule.CommonBean;
import com.ultimate.ultimatesmartschool.Examination.GroupSpinnerAdapter;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.StudentAttendance.ClassAdapterAttend;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.CommonProgress;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SubjectFragment extends Fragment implements SubjectAdapter.AddNewGroup {

    @BindView(R.id.spinnerGroup)
    Spinner spinnerGroup;
    @BindView(R.id.spinnerClass)
    Spinner spinnerClass;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.parent)
    RelativeLayout parent;

    private ArrayList<CommonBean> dataList = new ArrayList<>();
    private SubjectAdapter madapter;
    private ArrayList<CommonBean> groupList = new ArrayList<>();
    private String groupid = "";
    private String classid = "";
    public ArrayList<ClassBean> classList = new ArrayList<>();
    private ClassAdapterAttend classadapter;
    Dialog dialog;
    CommonProgress commonProgress;

    public SubjectFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_subject, container, false);
        ButterKnife.bind(this, view);
        commonProgress=new CommonProgress(getActivity());
        fetchGroup();
        recyclerView.setVisibility(View.GONE);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        madapter = new SubjectAdapter(getActivity(), dataList, this);
        recyclerView.setAdapter(madapter);
        if (madapter != null) {
            dataList.clear();
            madapter.notifyDataSetChanged();
        }
        return view;
    }

    private void fetchGroup() {
        commonProgress.show();
        HashMap<String, String> params = new HashMap<String, String>();
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.GROUP_LIST_URL, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                ErpProgress.cancelProgressBar();
                commonProgress.dismiss();
                if (error == null) {
                    try {
                        groupList.clear();
                        groupList = CommonBean.parseCommonArray(jsonObject.getJSONArray(Constants.GROUP_DATA));
                        GroupSpinnerAdapter adapter = new GroupSpinnerAdapter(getActivity(), groupList,0);
                        spinnerGroup.setAdapter(adapter);
                        spinnerGroup.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                if (i == 0) {
                                    if (classadapter != null) {
                                        classList.clear();
                                        classadapter.notifyDataSetChanged();
                                    }
                                }
                                if (i > 0) {
                                    dataList.clear();
                                    madapter.notifyDataSetChanged();
                                    groupid = groupList.get(i - 1).getId();
                                    classid = "";
                                    fetchClass(groupid);
                                }
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> adapterView) {

                            }
                        });
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    Utils.showSnackBar(error.getMessage(), parent);
                }
            }
        }, getActivity(), params);
    }

    private void fetchClass(String id) {
        spinnerClass.setVisibility(View.VISIBLE);
        HashMap<String, String> params = new HashMap<String, String>();
        params.put(Constants.ID, id);
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.GROUPCLASSLIST_URL, classapiCallback, getActivity(), params);
    }

    ApiHandler.ApiCallback classapiCallback = new ApiHandler.ApiCallback() {

        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            if (error == null) {
                try {
                    classList = ClassBean.parseClassArray(jsonObject.getJSONArray(Constants.CLASSDATA));
                    classadapter = new ClassAdapterAttend(getActivity(), classList,0);
                    spinnerClass.setAdapter(classadapter);
                    spinnerClass.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if (i > 0) {
                                dataList.clear();
                                madapter.notifyDataSetChanged();
                                classid = classList.get(i - 1).getId();
                                fetchSubject(classid);
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Utils.showSnackBar(error.getMessage(), parent);
            }
        }
    };

    private void fetchSubject(String classid) {
        HashMap<String, String> params = new HashMap<String, String>();
        params.put(Constants.ID, classid);
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.SUB_LIST_URL, subapiCallback, getActivity(), params);

    }

    ApiHandler.ApiCallback subapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            if (error == null) {
                try {
                    ArrayList<CommonBean> subList = CommonBean.parseCommonArray(jsonObject.getJSONArray(Constants.SUB_DATA));
                    dataList.clear();
                    dataList.addAll(subList);
                    madapter.notifyDataSetChanged();
                    recyclerView.setVisibility(View.VISIBLE);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                recyclerView.setVisibility(View.VISIBLE);
                Utils.showSnackBar(error.getMessage(), parent);
            }
        }
    };

    @SuppressLint("NewApi")
    @Override
    public void addEditNewGroup(final CommonBean data, final int pos) {
        dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.add_sub_dialog);
        TextView txtSetup = (TextView) dialog.findViewById(R.id.txtSetup);
        final EditText edtTitle = (EditText) dialog.findViewById(R.id.edtTitle);
        if (pos != -1) {
            edtTitle.setText(dataList.get(pos).getName());
        }
        Button btnSubmit = (Button) dialog.findViewById(R.id.btnSubmit);
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (edtTitle.getText().toString().length() > 0) {
                    if (pos == -1) {
                        if (classid != null && !classid.isEmpty())
                            addSub(edtTitle.getText().toString());
                    } else {
                        editSub(data, pos, edtTitle.getText().toString());
                    }
                }
            }
        });
        dialog.show();
        Window window = dialog.getWindow();
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    }

    private void editSub(CommonBean data, final int pos, String name) {
        HashMap<String, String> params = new HashMap<String, String>();
        params.put(Constants.NAME, name);
        params.put(Constants.ID, data.getId());
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.EDIT_SUB_URL, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                if (error == null) {
                    try {
                        Toast.makeText(getActivity(), jsonObject.getString("msg"), Toast.LENGTH_SHORT).show();
                        CommonBean groupdata = CommonBean.parseCommonObject(jsonObject.getJSONObject(Constants.SUB_DATA));
                        dataList.set(pos, groupdata);
                        madapter.notifyItemChanged(pos);
                        dialog.dismiss();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    Log.e("error", error.getMessage() + "");
                    Utils.showSnackBar(error.getMessage(), parent);
                }
            }
        }, getActivity(), params);
    }

    private void addSub(String name) {
        HashMap<String, String> params = new HashMap<String, String>();
        params.put(Constants.NAME, name);
        params.put("class_id", classid);
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.ADD_SUB_URL, apiCallback, getActivity(), params);

    }

    ApiHandler.ApiCallback apiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            if (error == null) {
                try {
                    Toast.makeText(getActivity(), jsonObject.getString("msg"), Toast.LENGTH_SHORT).show();
                    CommonBean groupdata = CommonBean.parseCommonObject(jsonObject.getJSONObject(Constants.SUB_DATA));
                    dataList.add(groupdata);
                    madapter.notifyDataSetChanged();
                    dialog.dismiss();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("error", error.getMessage() + "");
                Utils.showSnackBar(error.getMessage(), parent);
            }
        }
    };

    @Override
    public void deleteGroup(final CommonBean data, final int position) {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:
                        HashMap<String, String> params = new HashMap<String, String>();
                        params.put(Constants.ID, data.getId());
                        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.DEL_SUB_URL, new ApiHandler.ApiCallback() {
                            @Override
                            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                                if (error == null) {
                                    Toast.makeText(getActivity(), "Deleted Successfully", Toast.LENGTH_SHORT).show();
                                    dataList.remove(position);
                                    madapter.notifyDataSetChanged();
                                } else {
                                    Utils.showSnackBar(error.getMessage(), parent);
                                }
                            }
                        }, getActivity(), params);
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        //No button clicked
                        break;
                }
            }
        };
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("Are you sure you want to delete?").setPositiveButton("Yes", dialogClickListener)
                .setNegativeButton("No", dialogClickListener).show();
    }
}
