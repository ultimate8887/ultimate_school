package com.ultimate.ultimatesmartschool.SchoolSetup;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.ultimate.ultimatesmartschool.BeanModule.CommonBean;
import com.ultimate.ultimatesmartschool.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class GroupAdapter extends RecyclerView.Adapter<GroupAdapter.MyViewHolder> {

    private final AddNewGroup mAddNewGroup;
    private Context mContext;
    ArrayList<CommonBean> dataList;
    int value=0;
    public GroupAdapter(Context mContext, ArrayList<CommonBean> dataList, AddNewGroup mAddNewGroup,int value) {
        this.mContext = mContext;
        this.mAddNewGroup = mAddNewGroup;
        this.dataList = dataList;
        this.value = value;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        LayoutInflater inflater = LayoutInflater.from(
                parent.getContext());
        View v =
                inflater.inflate(R.layout.gcs_common_lyt, parent, false);
        // set the view's size, margins, paddings and layout parameters
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        if (value==1){
            holder.txtAddText.setText("Add House");
        }else{
            holder.txtAddText.setText("Add Group");
        }

        if (dataList.size() == position) {   //suppose data size is 4 then when position is 4 we have show add UI as it start count from o  then 4th position means  index 5
            holder.lytAll.setVisibility(View.GONE);
            holder.lytAddData.setVisibility(View.VISIBLE);
            holder.lytAddData.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mAddNewGroup != null) {
                        mAddNewGroup.addEditNewGroup(null, -1);
                    }
                }
            });
        } else {
            holder.lytAll.setVisibility(View.VISIBLE);
            holder.lytAddData.setVisibility(View.GONE);
            holder.txtTitle.setText(dataList.get(position).getName());
            holder.lytAll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mAddNewGroup != null) {
                        mAddNewGroup.addEditNewGroup(dataList.get(position), position);
                    }
                }
            });
            holder.imgTrash.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mAddNewGroup != null) {
                        mAddNewGroup.deleteGroup(dataList.get(position), position);
                    }
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return (dataList.size() + 1);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txtTitle)
        TextView txtTitle;
        @BindView(R.id.txtSub)
        TextView txtSub;
        @BindView(R.id.trash)
        ImageView imgTrash;
        @BindView(R.id.lytAddData)
        RelativeLayout lytAddData;
        @BindView(R.id.lytAll)
        RelativeLayout lytAll;
        @BindView(R.id.txtAddText)
        TextView txtAddText;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            txtSub.setVisibility(View.GONE);
        }
    }

    public interface AddNewGroup {
        public void addEditNewGroup(CommonBean data, int i);

        public void deleteGroup(CommonBean data, int position);
    }
}
