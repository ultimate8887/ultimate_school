package com.ultimate.ultimatesmartschool.SchoolSetup;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

public class TabPagerAdapter extends FragmentPagerAdapter {

    int tabCount;
    // tab titles
    private String[] tabTitles = new String[]{"Groups", "Classes", "Subjects", "Houses"};

    public TabPagerAdapter(FragmentManager fm) {
        super(fm);
        this.tabCount = 4;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabTitles[position];
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                GroupFragment tab1 = new GroupFragment();
                return tab1;
            case 1:
                ClassesFragment tab2 = new ClassesFragment();
                return tab2;
            case 2:
                SubjectFragment tab3 = new SubjectFragment();
                return tab3;

            case 3:
                HouseFragment tab4 = new HouseFragment();
                return tab4;

            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return tabCount;
    }


}
