package com.ultimate.ultimatesmartschool.SchoolSetup;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.ultimate.ultimatesmartschool.BeanModule.ClassBean;
import com.ultimate.ultimatesmartschool.BeanModule.CommonBean;
import com.ultimate.ultimatesmartschool.Examination.GroupSpinnerAdapter;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.CommonProgress;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ClassesFragment extends Fragment implements ClassesAdapter.AddNewGroup {
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    Dialog dialog;
    @BindView(R.id.parent)
    RelativeLayout parent;
    ArrayList<ClassBean> dataList = new ArrayList<>();
    private ClassesAdapter adapter;
//    @BindView(R.id.lytInst)
//    LinearLayout lytInst;
    private Spinner spinnerGroup;
    private ArrayList<CommonBean> groupList = new ArrayList<>();
    private String groupid = "";
    CommonProgress commonProgress;
    public ClassesFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_classes, container, false);
        ButterKnife.bind(this, view);
        commonProgress=new CommonProgress(getActivity());
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        adapter = new ClassesAdapter(getActivity(), dataList, this);
        recyclerView.setAdapter(adapter);
        updateInst();
        fetchClass();
        return view;
    }

    private void fetchClass() {
        HashMap<String, String> params = new HashMap<String, String>();
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.CLASSLIST_URL, classapiCallback, getActivity(), params);

    }

    ApiHandler.ApiCallback classapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            if (error == null) {
                try {
                    ArrayList<ClassBean> classList = ClassBean.parseClassArray(jsonObject.getJSONArray(Constants.CLASSDATA));
                    dataList.clear();
                    dataList.addAll(classList);
                    adapter.notifyDataSetChanged();
                    updateInst();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Utils.showSnackBar(error.getMessage(), parent);
            }
        }
    };

    private void updateInst() {
//        if (dataList.size() > 0) {
//            lytInst.setVisibility(View.VISIBLE);
//        } else {
//            lytInst.setVisibility(View.GONE);
//        }
    }

    @SuppressLint("NewApi")
    @Override
    public void addEditNewGroup(final ClassBean data, final int pos) {
        dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.add_classes_dialog);
        spinnerGroup = (Spinner) dialog.findViewById(R.id.spinnerGroup);
        final EditText edtTitle = (EditText) dialog.findViewById(R.id.edtTitle);
        if (pos != -1) {
            edtTitle.setText(data.getName());
            groupid = data.getGroup_data().getId();
        }
        fetchGroup(pos);
        Button btnSubmit = (Button) dialog.findViewById(R.id.btnSubmit);
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (groupid != null && !groupid.isEmpty() && !edtTitle.getText().toString().isEmpty()) {
                    if (pos == -1) {
                        addClassName(edtTitle.getText().toString(), groupid);
                    } else {
                        editClass(data, pos, edtTitle.getText().toString());
                    }
                } else {
                    Utils.showSnackBar("Please enter data", parent);
                }
            }
        });
        dialog.show();
        Window window = dialog.getWindow();
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    }

    private void editClass(ClassBean data, final int pos, String name) {
        HashMap<String, String> params = new HashMap<String, String>();
        params.put(Constants.NAME, name);
        params.put(Constants.ID, data.getId());
        params.put("group_id", groupid);
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.EDIT_Class_URL, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                if (error == null) {
                    try {
                        Toast.makeText(getActivity(), jsonObject.getString("msg"), Toast.LENGTH_SHORT).show();
                        ClassBean groupdata = ClassBean.parseClassObject(jsonObject.getJSONObject(Constants.CLASSDATA));
                        dataList.set(pos, groupdata);
                        adapter.notifyItemChanged(pos);
                        dialog.dismiss();
                        recyclerView.scrollToPosition(pos);
                        updateInst();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    Log.e("error", error.getMessage() + "");
                    Utils.showSnackBar(error.getMessage(), parent);
                }
            }
        }, getActivity(), params);
    }

    private void addClassName(String name, String group_id) {
        HashMap<String, String> params = new HashMap<String, String>();
        params.put(Constants.NAME, name);
        params.put("group_id", group_id);
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.ADD_Class_URL, apiCallback, getActivity(), params);
    }

    ApiHandler.ApiCallback apiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            if (error == null) {
                try {
                    Toast.makeText(getActivity(), jsonObject.getString("msg"), Toast.LENGTH_SHORT).show();
                    ClassBean classdata = ClassBean.parseClassObject(jsonObject.getJSONObject(Constants.CLASSDATA));
                    dataList.add(classdata);
                    adapter.notifyDataSetChanged();
                    dialog.dismiss();

                    updateInst();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("error", error.getMessage() + "");
                Utils.showSnackBar(error.getMessage(), parent);
            }
        }
    };

    private void fetchGroup(final int pos) {
     commonProgress.show();
        HashMap<String, String> params = new HashMap<String, String>();
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.GROUP_LIST_URL, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                ErpProgress.cancelProgressBar();
                commonProgress.dismiss();
                if (error == null) {
                    try {
                        groupList.clear();
                        groupList = CommonBean.parseCommonArray(jsonObject.getJSONArray(Constants.GROUP_DATA));
                        GroupSpinnerAdapter adapter = new GroupSpinnerAdapter(getActivity(), groupList,0);
                        spinnerGroup.setAdapter(adapter);
                        spinnerGroup.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                if (i > 0) {
                                    groupid = groupList.get(i - 1).getId();
                                }
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> adapterView) {

                            }
                        });
                        if (pos != -1) {
                            int i = 0;
                            for (CommonBean obj : groupList) {
                                if (obj.getId().equals(groupid)) {
                                    spinnerGroup.setSelection(i + 1);
                                    break;
                                }
                                i++;
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    Utils.showSnackBar(error.getMessage(), parent);
                }
            }
        }, getActivity(), params);
    }

    @Override
    public void deleteGroup(final ClassBean data, final int position) {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        HashMap<String, String> params = new HashMap<String, String>();
                        params.put(Constants.ID, data.getId());
                        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.DEL_CLASS_URL, new ApiHandler.ApiCallback() {
                            @Override
                            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                                if (error == null) {
                                    Toast.makeText(getActivity(), "Deleted Successfully", Toast.LENGTH_SHORT).show();
                                    dataList.remove(position);
                                    adapter.notifyDataSetChanged();
                                    updateInst();
                                } else {
                                    Utils.showSnackBar(error.getMessage(), parent);
                                }
                            }
                        }, getActivity(), params);
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        //No button clicked
                        break;
                }
            }
        };
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("Are you sure you want to delete?").setPositiveButton("Yes", dialogClickListener)
                .setNegativeButton("No", dialogClickListener).show();
    }
}
