package com.ultimate.ultimatesmartschool.PTMFeedBack;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.daimajia.swipe.SwipeLayout;
import com.ultimate.ultimatesmartschool.BeanModule.CommonBean;
import com.ultimate.ultimatesmartschool.Home.HolidayBean;
import com.ultimate.ultimatesmartschool.Home.Holidayadapter;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PTMListAdapter extends RecyclerView.Adapter<PTMListAdapter.Viewholder> {
    Context mContext;
    ArrayList<CommonBean> holidayList;
    private Mycallback mAdaptercall;

    int value=0;
    public PTMListAdapter(ArrayList<CommonBean> holidayList, Context mContext, int value,Mycallback mAdaptercall) {
        this.holidayList = holidayList;
        this.mContext = mContext;
        this.value = value;
        this.mAdaptercall=mAdaptercall;
    }
    public interface Mycallback{
        public void onDelecallback(CommonBean commonBean);
    }

    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adpt_holiday_list, parent, false);
        Viewholder viewholder = new Viewholder(view);
        return viewholder;
    }

    @Override
    public void onBindViewHolder(@NonNull Viewholder holder, @SuppressLint("RecyclerView") int position) {

            holder.holiday.setVisibility(View.VISIBLE);
            holder.home.setVisibility(View.GONE);
            holder.createdon.setVisibility(View.INVISIBLE);
            holder.swipeLayout.setVisibility(View.VISIBLE);

            if (holidayList.get(position).getId() != null) {
                String title = getColoredSpanned("ID: ", "#000000");
                String Name = getColoredSpanned("ptm-"+holidayList.get(position).getId(), "#5A5C59");
                holder.albm_id.setText(Html.fromHtml(title + " " + Name));
            }

            if (holidayList.get(position).getName() != null) {
                String title = getColoredSpanned("PTM Name: ", "#F4212C");
                String Name = getColoredSpanned(""+holidayList.get(position).getName(), "#5A5C59");
                holder.title.setText(Html.fromHtml(title + " " + Name));
            }

            if (holidayList.get(position).getDate() != null) {
                String title = getColoredSpanned("PTM Date: ", "#000000");
                String Name = getColoredSpanned(""+ Utils.getDateFormated(holidayList.get(position).getDate()), "#5A5C59");
                holder.h_date.setText(Html.fromHtml(title + " " + Name));
            }

            if (holidayList.get(position).getDate() != null) {
                String title = getColoredSpanned("Created-On: ", "#000000");
                String Name = getColoredSpanned(""+ Utils.getDateFormated(holidayList.get(position).getDate()), "#5A5C59");
                holder.createdon.setText(Html.fromHtml(title + " " + Name));
            }

        holder.trash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder1 = new AlertDialog.Builder(mContext);
                builder1.setMessage("Do you want to delete? ");
                builder1.setCancelable(false);
                builder1.setPositiveButton(
                        "Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                                if (mAdaptercall != null) {
                                    mAdaptercall.onDelecallback(holidayList.get(position));
//                                    Toast.makeText(listner, "deleted", Toast.LENGTH_SHORT).show();
                                }
                                // mAdapterCallback.deleteNotification(notificationList.get(postion));
                            }
                        });
                builder1.setNegativeButton(
                        "No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        });
                AlertDialog alert11 = builder1.create();
                alert11.show();
            }
        });


    }

    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }

    @Override
    public int getItemCount() {
        return holidayList.size();
    }

    public void setHList(ArrayList<CommonBean> holidayList) {
        this.holidayList = holidayList;
    }

    public class Viewholder extends RecyclerView.ViewHolder {

        @BindView(R.id.hol)
        RelativeLayout holiday;

        @BindView(R.id.home)
        RelativeLayout home;

        @BindView(R.id.txtHoliday)
        TextView txtHoliday;

        @BindView(R.id.albm_id)
        TextView albm_id;

        @BindView(R.id.title)
        TextView title;

        @BindView(R.id.albm_name)
        TextView h_date;

        @BindView(R.id.status)
        TextView createdon;

        SwipeLayout swipeLayout;
        public ImageView trash, update;

        public Viewholder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            swipeLayout = (SwipeLayout) itemView.findViewById(R.id.swipe);
            trash = (ImageView) itemView.findViewById(R.id.trash);
            update = (ImageView) itemView.findViewById(R.id.update);
        }
    }
}
