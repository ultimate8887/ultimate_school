package com.ultimate.ultimatesmartschool.PTMFeedBack;

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.SchoolActivity.ActivityStudentBean;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FeedbackListAdapter extends RecyclerView.Adapter<FeedbackListAdapter.Viewholder> {
    ArrayList<FeedbackBean> list;
    Context listner;
    Mycallback mAdaptercall;
    int value;

    public FeedbackListAdapter(ArrayList<FeedbackBean> list, Context listener, Mycallback mAdaptercall, int value) {
        this.list = list;
        this.value = value;
        this.listner = listener;
        this.mAdaptercall = mAdaptercall;
    }

    public interface Mycallback {
        public void onApproveCallback(ActivityStudentBean gatePassBean);

    }

    @Override
    public Viewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.feed_std_lyt_new, parent, false);
        Viewholder viewholder = new Viewholder(view);
        return viewholder;

    }

    @Override
    public void onBindViewHolder(Viewholder holder, @SuppressLint("RecyclerView") final int position) {
        final FeedbackBean mData = list.get(position);
        holder.txtstuName.setEnabled(false);
        holder.txtReason.setEnabled(false);
        holder.txtcontprsn.setEnabled(false);
        String mclass = "";

        if (mData.getType_name()!=null){
            if (mData.getType_name().toUpperCase().equalsIgnoreCase("PTM")){
                holder.ggg.setVisibility(View.VISIBLE);
                holder.txtdandtofcall.setVisibility(View.VISIBLE);
                holder.txtFatherName.setVisibility(View.VISIBLE);
            }else{
                holder.ggg.setVisibility(View.GONE);
                holder.txtdandtofcall.setVisibility(View.INVISIBLE);
                holder.txtFatherName.setVisibility(View.GONE);
            }
        }else{
            holder.ggg.setVisibility(View.GONE);
            holder.txtdandtofcall.setVisibility(View.INVISIBLE);
            holder.txtFatherName.setVisibility(View.GONE);
        }


        if (mData.getId() != null) {
            String title = getColoredSpanned("ID:- ", "#000000");
            String Name = getColoredSpanned("fb-" + mData.getId(), "#5A5C59");
            holder.id.setText(Html.fromHtml(title + " " + Name));
        }


        if (mData.getType_name() != null) {
            String title = getColoredSpanned("Feedback Type: ", "#5A5C59");
            String Name = getColoredSpanned(mData.getType_name(), "#000000");
            holder.txtstuName.setText(Html.fromHtml(title + " " + Name));
        }

        if (mData.getName() != null) {
            holder.txtdandtofcall.setVisibility(View.VISIBLE);
            String title = getColoredSpanned("PTM Name: ", "#5A5C59");
            String Name = getColoredSpanned(mData.getName(), "#1C8B3B");
            holder.txtdandtofcall.setText(Html.fromHtml(title + " " + Name));
        }

        if (mData.getDate() != null) {
            holder.txtFatherName.setVisibility(View.VISIBLE);
            String title = getColoredSpanned("PTM Date: ", "#5A5C59");
            String Name = getColoredSpanned(Utils.getDateFormated(mData.getDate()), "#000000");
            holder.txtFatherName.setText(Html.fromHtml(title + " " + Name));
        }


        if (mData.getEs_date() != null) {
            String title = getColoredSpanned("Feedback Date: ", "#5A5C59");
            String Name = getColoredSpanned(Utils.getDateFormated(mData.getEs_date()), "#1C8B3B");
            holder.txtDT.setText(Html.fromHtml(title + " " + Name));
        }

        if (mData.getEs_overall_feedback()!=null){
            holder.txtReason.setText(mData.getEs_overall_feedback());
        }else{
            holder.txtReason.setText("Not Found");
        }


        if (mData.getEs_area_of_improvement()!=null){
            holder.txtReason1.setText(mData.getEs_area_of_improvement());
        }else{
            holder.txtReason1.setText("N/A");
        }


        if (mData.getEs_positive_changes() != null) {
            holder.txtcontprsn.setVisibility(View.VISIBLE);
            holder.guardian_add.setVisibility(View.VISIBLE);
            String title = getColoredSpanned("Noticed any positive changes in your child: ", "#5A5C59");
            String Name = getColoredSpanned(mData.getEs_positive_changes(), "#000000");
            holder.txtcontprsn.setText(Html.fromHtml(title + " " + Name));
        }

        if (mData.getEs_attitude_of_teacher() != null) {
            holder.txtcontprsnnum.setVisibility(View.VISIBLE);
            String title = getColoredSpanned("Attitude of the teachers during PTM: ", "#383737");
            String Name = getColoredSpanned("<b>"+mData.getEs_attitude_of_teacher()+"</b>", "#000000");
            holder.txtcontprsnnum.setText(Html.fromHtml(title + " " + Name));
        }

        if (mData.getEs_ptm_experience() != null) {
            holder.txtrelation.setVisibility(View.VISIBLE);
            String title = getColoredSpanned("How was your PTM experience: ", "#383737");
            String Name = getColoredSpanned("<b>"+mData.getEs_ptm_experience()+"</b>", "#000000");
            holder.txtrelation.setText(Html.fromHtml(title + " " + Name));
        }

        if (mData.getEs_satisfaction() != null) {
            holder.txtconfirmationnum.setVisibility(View.VISIBLE);
            String title = getColoredSpanned("How far you are satisfied with School: ", "#383737");
            String Name = getColoredSpanned("<b>"+mData.getEs_satisfaction()+"</b>", "#000000");
            holder.txtconfirmationnum.setText(Html.fromHtml(title + " " + Name));
        }

        holder.guardian_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.guardian_add.setVisibility(View.GONE);
                holder.guardian_mini.setVisibility(View.VISIBLE);
                holder.more.setVisibility(View.VISIBLE);
            }
        });

        holder.more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.guardian_add.setVisibility(View.VISIBLE);
                holder.guardian_mini.setVisibility(View.GONE);
                holder.more.setVisibility(View.GONE);
            }
        });


    }

    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void setGatePassList(ArrayList<FeedbackBean> list) {
        this.list = list;
    }

    public class Viewholder extends RecyclerView.ViewHolder {
        @BindView(R.id.txtFatherName)
        TextView txtFatherName;
        @BindView(R.id.txtPhone)
        TextView txtPhone;
        @BindView(R.id.txtPhone1)
        TextView txtPhone1;
        @BindView(R.id.edtCheckOuts)
        TextView edtCheckOuts;

        @BindView(R.id.edtCheckOut)
        RelativeLayout edtCheckOut;
        @BindView(R.id.txtDT)
        TextView txtDT;
        @BindView(R.id.id)
        TextView id;
        @BindView(R.id.p_title)
        TextView p_title;
        @BindView(R.id.txtABy)
        TextView txtABy;
        @BindView(R.id.txtReason)
        EditText txtReason;
        @BindView(R.id.txtstuName)
        EditText txtstuName;
        @BindView(R.id.txtcontprsn)
        EditText txtcontprsn;
        @BindView(R.id.txtcontprsnnum)
        TextView txtcontprsnnum;
        @BindView(R.id.txtrelation)
        TextView txtrelation;
        @BindView(R.id.txtconfirmationnum)
        TextView txtconfirmationnum;
        @BindView(R.id.txtdandtofcall)
        TextView txtdandtofcall;

        @BindView(R.id.txtconfirmationnum1)
        TextView txtconfirmationnum1;
        @BindView(R.id.txtconfirmationnum2)
        TextView txtconfirmationnum2;
        @BindView(R.id.txtReason1)
        EditText txtReason1;

        @BindView(R.id.std_sign)
        ImageView std_sign;
        @BindView(R.id.staff_sign)
        ImageView staff_sign;
        @BindView(R.id.admin_sign)
        ImageView admin_sign;

        @BindView(R.id.std_profile)
        ImageView std_profile;

        @BindView(R.id.sign_lyt)
        CardView sign_lyt;

        @BindView(R.id.more)
        RelativeLayout more;

        @BindView(R.id.ggg)
        RelativeLayout ggg;

        @BindView(R.id.guardian_add)
        TextView guardian_add;
        @BindView(R.id.guardian_mini)
        TextView guardian_mini;



        public Viewholder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
