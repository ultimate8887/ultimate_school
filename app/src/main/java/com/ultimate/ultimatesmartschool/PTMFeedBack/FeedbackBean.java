package com.ultimate.ultimatesmartschool.PTMFeedBack;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class FeedbackBean {

    private static String ID = "id";
    private static String NAME = "name";
    private static String DATE = "date";

    private static String SAT = "es_satisfaction";
    private static String POS = "es_positive_changes";
    private static String IMP = "es_area_of_improvement";

    private static String ATT = "es_attitude_of_teacher";
    private static String EXP = "es_ptm_experience";
    private static String FEE = "es_overall_feedback";
    private static String ES_DATE = "es_date";
    private static String PTM_ID = "es_ptm_id";
    private static String PTM_TYPE = "type_name";

    public String getEs_satisfaction() {
        return es_satisfaction;
    }

    public void setEs_satisfaction(String es_satisfaction) {
        this.es_satisfaction = es_satisfaction;
    }

    public String getEs_positive_changes() {
        return es_positive_changes;
    }

    public void setEs_positive_changes(String es_positive_changes) {
        this.es_positive_changes = es_positive_changes;
    }

    public String getEs_area_of_improvement() {
        return es_area_of_improvement;
    }

    public void setEs_area_of_improvement(String es_area_of_improvement) {
        this.es_area_of_improvement = es_area_of_improvement;
    }

    public String getEs_attitude_of_teacher() {
        return es_attitude_of_teacher;
    }

    public void setEs_attitude_of_teacher(String es_attitude_of_teacher) {
        this.es_attitude_of_teacher = es_attitude_of_teacher;
    }

    public String getEs_ptm_experience() {
        return es_ptm_experience;
    }

    public void setEs_ptm_experience(String es_ptm_experience) {
        this.es_ptm_experience = es_ptm_experience;
    }

    public String getEs_overall_feedback() {
        return es_overall_feedback;
    }

    public void setEs_overall_feedback(String es_overall_feedback) {
        this.es_overall_feedback = es_overall_feedback;
    }

    public String getEs_date() {
        return es_date;
    }

    public void setEs_date(String es_date) {
        this.es_date = es_date;
    }

    public String getEs_ptm_id() {
        return es_ptm_id;
    }

    public void setEs_ptm_id(String es_ptm_id) {
        this.es_ptm_id = es_ptm_id;
    }

    public String getType_name() {
        return type_name;
    }

    public void setType_name(String type_name) {
        this.type_name = type_name;
    }

    private String es_satisfaction;
    private String es_positive_changes;
    private String es_area_of_improvement;
    private String es_attitude_of_teacher;
    private String es_ptm_experience;
    private String es_overall_feedback;
    private String es_date;
    private String es_ptm_id;
    private String type_name;
    /**
     * id : 2
     * name : NURSERY
     */

    private String id;
    private String name;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    private String date;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static ArrayList<FeedbackBean> parseCommonArray(JSONArray arrayObj) {
        ArrayList list = new ArrayList<FeedbackBean>();
        try {
            for (int i = 0; i < arrayObj.length(); i++) {
                FeedbackBean p = parseCommonObject(arrayObj.getJSONObject(i));
                if (p != null) {
                    list.add(p);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public static FeedbackBean parseCommonObject(JSONObject jsonObject) {
        FeedbackBean casteObj = new FeedbackBean();
        try {
            if (jsonObject.has(ID)) {
                casteObj.setId(jsonObject.getString(ID));
            }
            if (jsonObject.has(FEE) && !jsonObject.getString(FEE).isEmpty() && !jsonObject.getString(FEE).equalsIgnoreCase("null")) {
                casteObj.setEs_overall_feedback(jsonObject.getString(FEE));
            }
            if (jsonObject.has(ES_DATE) && !jsonObject.getString(ES_DATE).isEmpty() && !jsonObject.getString(ES_DATE).equalsIgnoreCase("null")) {
                casteObj.setEs_date(jsonObject.getString(ES_DATE));
            }
            if (jsonObject.has(PTM_ID) && !jsonObject.getString(PTM_ID).isEmpty() && !jsonObject.getString(PTM_ID).equalsIgnoreCase("null")) {
                casteObj.setEs_ptm_id(jsonObject.getString(PTM_ID));
            }
            if (jsonObject.has(PTM_TYPE) && !jsonObject.getString(PTM_TYPE).isEmpty() && !jsonObject.getString(PTM_TYPE).equalsIgnoreCase("null")) {
                casteObj.setType_name(jsonObject.getString(PTM_TYPE));
            }
            if (jsonObject.has(NAME) && !jsonObject.getString(NAME).isEmpty() && !jsonObject.getString(NAME).equalsIgnoreCase("null")) {
                casteObj.setName(jsonObject.getString(NAME));
            }
            if (jsonObject.has(DATE)) {
                casteObj.setDate(jsonObject.getString(DATE));
            }
            if (jsonObject.has(SAT) && !jsonObject.getString(SAT).isEmpty() && !jsonObject.getString(SAT).equalsIgnoreCase("null")) {
                casteObj.setEs_satisfaction(jsonObject.getString(SAT));
            }
            if (jsonObject.has(POS) && !jsonObject.getString(POS).isEmpty() && !jsonObject.getString(POS).equalsIgnoreCase("null")) {
                casteObj.setEs_positive_changes(jsonObject.getString(POS));
            }
            if (jsonObject.has(IMP) && !jsonObject.getString(IMP).isEmpty() && !jsonObject.getString(IMP).equalsIgnoreCase("null")) {
                casteObj.setEs_area_of_improvement(jsonObject.getString(IMP));
            }
            if (jsonObject.has(ATT) && !jsonObject.getString(ATT).isEmpty() && !jsonObject.getString(ATT).equalsIgnoreCase("null")) {
                casteObj.setEs_attitude_of_teacher(jsonObject.getString(ATT));
            }
            if (jsonObject.has(EXP) && !jsonObject.getString(EXP).isEmpty() && !jsonObject.getString(EXP).equalsIgnoreCase("null")) {
                casteObj.setEs_ptm_experience(jsonObject.getString(EXP));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return casteObj;
    }
}
