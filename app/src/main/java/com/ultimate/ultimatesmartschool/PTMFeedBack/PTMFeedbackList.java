package com.ultimate.ultimatesmartschool.PTMFeedBack;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetSequence;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.ultimate.ultimatesmartschool.BeanModule.ClassBean;
import com.ultimate.ultimatesmartschool.BeanModule.CommonBean;
import com.ultimate.ultimatesmartschool.BeanModule.SectionBean;
import com.ultimate.ultimatesmartschool.BeanModule.User;
import com.ultimate.ultimatesmartschool.Examination.ClassAdapter;
import com.ultimate.ultimatesmartschool.Homework.GETHomeworkActivity;
import com.ultimate.ultimatesmartschool.Homework.SectionnewAdapter;
import com.ultimate.ultimatesmartschool.Login.LoginActivity;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.SchoolActivity.ActivityStudentBean;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.CommonProgress;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PTMFeedbackList extends AppCompatActivity implements FeedbackListAdapter.Mycallback {

    SharedPreferences sharedPreferences;
    @BindView(R.id.imgBack)
    ImageView back;

    @BindView(R.id.txtTitle)
    TextView txtTitle;

    @BindView(R.id.txtSub)
    TextView txtSub;


    @BindView(R.id.p_recyclerview)
    RecyclerView p_recyclerview;

    Spinner spinnerGroup,spinnerActivity;
    Spinner spinnersubject;
    private String activity_id = "";
    private String activity_name = "";

    @BindView(R.id.oneDay)
    RelativeLayout p_day;

    @BindView(R.id.parent)
    RelativeLayout parent;

    @BindView(R.id.multiDay)
    RelativeLayout v_day;


    @BindView(R.id.layt)
    RelativeLayout layt;


    @BindView(R.id.img1)
    ImageView img1;
    @BindView(R.id.img2)
    ImageView img2;

    String apply="pending";
    Dialog mBottomSheetDialog;


    @BindView(R.id.cal_img)
    ImageView cal_img;
    @BindView(R.id.today_date)
    TextView today_date;
    @BindView(R.id.dialog)
    ImageView dialog;
    int check=0;
    private LinearLayoutManager layoutManager,layoutManager1;

    Animation animation;
    @BindView(R.id.textNorecord)
    TextView txtNorecord;
    @BindView(R.id.totalRecord)
    TextView totalRecord;
    CommonProgress commonProgress;
    String sub_id= "", sub_name= "" ,sectionid="",sectionname="",classid = "",className = "",ptm_id = "";

    String from_date = "";
    private int loaded = 0;
    FeedbackListAdapter p_mAdapter;
    ArrayList<FeedbackBean> p_list=new ArrayList<>();

    private ArrayList<CommonBean> groupList = new ArrayList<>();
    private String groupid = "";
    private String group_name = "";

    private ArrayList<CommonBean> groupList_ptm = new ArrayList<>();
    @BindView(R.id.spinner12)Spinner spinnercls;

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_students);
        ButterKnife.bind(this);
        commonProgress=new CommonProgress(this);
        animation = AnimationUtils.loadAnimation(this, R.anim.btn_blink_animation);
        layt.setVisibility(View.VISIBLE);
        txtSub.setVisibility(View.GONE);
        layoutManager = new LinearLayoutManager(this);
        p_recyclerview.setLayoutManager(layoutManager);
        p_mAdapter = new FeedbackListAdapter(p_list, this,this,2);
        p_recyclerview.setAdapter(p_mAdapter);
        txtTitle.setText("Feedback List");
        fetchgroupPTMlist();
        if (!restorePrefData()){
            setShowcaseView();
        }
        //setCommonData();
    }

    private void fetchgroupPTMlist() {
        commonProgress.show();
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("check","ptm");
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.GROUP_LIST_URL, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                commonProgress.dismiss();
                if (error == null) {
                    try {
                        groupList_ptm.clear();
                        groupList_ptm = CommonBean.parseCommonArray(jsonObject.getJSONArray(Constants.GROUP_DATA));
                        final GroupSpinnerAdapter adapter = new GroupSpinnerAdapter(PTMFeedbackList.this, groupList_ptm,1);
                        spinnercls.setAdapter(adapter);
                        spinnercls.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                if (position > 0) {
                                    ptm_id = groupList_ptm.get(position - 1).getId();
                                    p_fetchhwlist();
                                } else {
                                    ptm_id = "";
                                    p_fetchhwlist();
                                }
                            }
                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }else{
                    Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_LONG).show();
                }
            }
        },this,params);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @OnClick(R.id.dialog)
    public void dialogggg() {
        openDialog();
    }

    Spinner spinnerClass;
    Spinner spinnersection;
    ArrayList<SectionBean> sectionList = new ArrayList<>();
    ArrayList<ClassBean> classList = new ArrayList<>();

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void openDialog() {
        mBottomSheetDialog = new BottomSheetDialog(this);
        final View sheetView = getLayoutInflater().inflate(R.layout.search_dialog, null);
        mBottomSheetDialog.setContentView(sheetView);
        mBottomSheetDialog.setCancelable(false);

        spinnerClass=(Spinner) sheetView.findViewById(R.id.spinnerClass);
        spinnersection=(Spinner) sheetView.findViewById(R.id.spinnersection);
        ImageView close=(ImageView) sheetView.findViewById(R.id.close);
        TextView txtSetup=(TextView) sheetView.findViewById(R.id.txtSetup);
        txtSetup.setText("View Feedback List");
        fetchClass();
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //assignData();
                if (check==0) {
                    finish();
                    mBottomSheetDialog.dismiss();
                }else {
                    mBottomSheetDialog.dismiss();
                }
            }
        });

        Button btnYes= (Button) sheetView.findViewById(R.id.btnYes);

        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (classid.equalsIgnoreCase(""))  {
                    Toast.makeText(PTMFeedbackList.this,"Kindly Select Class!", Toast.LENGTH_SHORT).show();
                }  else if (sectionid.equalsIgnoreCase("")){
                    Toast.makeText(PTMFeedbackList.this,"Kindly Select Class Section!", Toast.LENGTH_SHORT).show();
                }else {
                    p_fetchhwlist();
                    setCommonData();
                }

            }
        });
        mBottomSheetDialog.show();

    }

    private void setShowcaseView() {
        new TapTargetSequence(this)
                .targets(TapTarget.forView(dialog,"Search Button!","Tap the Search button to Search Class-Wise Feedback List.")
                        .outerCircleColor(R.color.light)
                        .outerCircleAlpha(0.96f)
                        .targetCircleColor(R.color.white)
                        .titleTextSize(20)
                        .titleTextColor(R.color.white)
                        .descriptionTextSize(14)
                        .descriptionTextColor(R.color.black)
                        .textColor(R.color.black)
                        .textTypeface(Typeface.SANS_SERIF)
                        .dimColor(R.color.black)
                        .drawShadow(true)
                        .cancelable(false)
                        .tintTarget(true)
                        .transparentTarget(true)
                        .targetRadius(60)).listener(new TapTargetSequence.Listener() {
                    @RequiresApi(api = Build.VERSION_CODES.N)
                    @Override
                    public void onSequenceFinish() {
                        // Toast.makeText(getApplicationContext(),"Sequence Finished",Toast.LENGTH_SHORT).show();
                        savePrefData();
                    }

                    @Override
                    public void onSequenceStep(TapTarget lastTarget, boolean targetClicked) {
                        // Toast.makeText(getActivity(),"GREAT!",Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onSequenceCanceled(TapTarget lastTarget) {

                    }
                }).start();
    }

    private void savePrefData(){
        sharedPreferences=this.getSharedPreferences("boarding_pref_add_attend_search",MODE_PRIVATE);
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putBoolean("IsFirstTimeVisit_add_attend_search",true);
        editor.apply();
    }

    private boolean restorePrefData(){
        sharedPreferences = this.getSharedPreferences("boarding_pref_add_attend_search",MODE_PRIVATE);
        return sharedPreferences.getBoolean("IsFirstTimeVisit_add_attend_search",false);
    }

    private void fetchClass() {
        if (loaded == 0) {
            commonProgress.show();
        }
        loaded++;

        HashMap<String, String> params = new HashMap<String, String>();
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.CLASSLIST_URL, classapiCallback,getApplicationContext(), params);
    }


    ApiHandler.ApiCallback classapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            commonProgress.dismiss();
            if (error == null) {
                try {
                    classList = ClassBean.parseClassArray(jsonObject.getJSONArray(Constants.CLASSDATA));
                    ClassAdapter adapter = new ClassAdapter(getApplicationContext(), classList);
                    spinnerClass.setAdapter(adapter);
                    spinnerClass.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if (i > 0) {
                                sub_id= "";
                                sub_name= "" ;
                                sectionid="";
                                sectionname="";
                                classid = "";
                                className = "";
                                classid = classList.get(i - 1).getId();
                                className= classList.get(i - 1).getName();
                                fetchSection();
                            }

                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_LONG).show();
                // Utils.showSnackBar(error.getMessage(), view.findViewById(R.id.parent));
            }
        }
    };

    private void fetchSection() {
        HashMap<String, String> params = new HashMap<String, String>();
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.SECTIONLIST_URL, sectionapiCallback, this, params);
    }

    ApiHandler.ApiCallback sectionapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            if (error == null) {
                try {
                    sectionList = SectionBean.parseCommonArray(jsonObject.getJSONArray("hostelroom_data"));
                    SectionnewAdapter adapter = new SectionnewAdapter(PTMFeedbackList.this, sectionList);
                    spinnersection.setAdapter(adapter);
                    spinnersection.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if (i > 0) {
                                sectionid = sectionList.get(i - 1).getSection_id();
                                sectionname = sectionList.get(i - 1).getSection_name();

                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Utils.showSnackBar(error.getMessage(), parent);

            }
        }
    };



    private void setCommonData() {
        txtTitle.setText(className+"("+sectionname+")" +" class");
        //  txtSub.setText(sub_name);
    }

    @OnClick(R.id.imgBack)
    public void backFinish() {
        back.startAnimation(animation);
        finish();
    }


    @OnClick(R.id.oneDay)
    public void oneDay() {
        apply="pending";
        img1.setVisibility(View.VISIBLE);
        img2.setVisibility(View.GONE);
        v_day.setBackgroundColor(Color.parseColor("#00000000"));
        p_day.setBackgroundColor(Color.parseColor("#66000000"));
        //  p_fetchhwlist(from_date,"pending");
    }

    @OnClick(R.id.multiDay)
    public void multiDay() {
        apply="verified";
        img2.setVisibility(View.VISIBLE);
        img1.setVisibility(View.GONE);
        p_day.setBackgroundColor(Color.parseColor("#00000000"));
        v_day.setBackgroundColor(Color.parseColor("#66000000"));
        //   p_fetchhwlist(from_date,"");
    }


    private void p_fetchhwlist() {
        check++;
        commonProgress.show();
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("check","list");
        params.put("es_sectionid",sectionid);
        params.put("es_classid",classid);
        params.put("ptm_id",ptm_id);
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.GROUP_LIST_URL, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                commonProgress.dismiss();
                if(mBottomSheetDialog !=null){
                    mBottomSheetDialog.dismiss();
                }
                if (error == null) {
                    try {
                        if (p_list != null) {
                            p_list.clear();
                        }
                        p_list = FeedbackBean.parseCommonArray(jsonObject.getJSONArray(Constants.GROUP_DATA));
                        if (p_list.size() > 0) {
                            p_mAdapter.setGatePassList(p_list);
                            //setanimation on adapter...
                            p_recyclerview.getAdapter().notifyDataSetChanged();
                            p_recyclerview.scheduleLayoutAnimation();
                            totalRecord.setText("Total Entries:- "+String.valueOf(p_list.size()));
                            //-----------end------------
                            txtNorecord.setVisibility(View.GONE);
                        } else {
                            totalRecord.setText("Total Entries:- 0");
                            p_mAdapter.setGatePassList(p_list);
                            p_mAdapter.notifyDataSetChanged();
                            txtNorecord.setVisibility(View.VISIBLE);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    totalRecord.setText("Total Entries:- 0");
                    txtNorecord.setVisibility(View.VISIBLE);
                    p_list.clear();
                    p_mAdapter.setGatePassList(p_list);
                    p_mAdapter.notifyDataSetChanged();
                    Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_SHORT).show();
                }
            }
        }, this, params);
    }

    @Override
    public void onApproveCallback(ActivityStudentBean homeworkbean) {

    }
}
