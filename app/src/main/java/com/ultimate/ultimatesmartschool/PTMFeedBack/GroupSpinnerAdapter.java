package com.ultimate.ultimatesmartschool.PTMFeedBack;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.ultimate.ultimatesmartschool.BeanModule.CommonBean;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import java.util.ArrayList;

public class GroupSpinnerAdapter extends BaseAdapter {
    private final ArrayList<CommonBean> casteList;
    Context context;
    LayoutInflater inflter;
    int value=0;

    public GroupSpinnerAdapter(Context applicationContext, ArrayList<CommonBean> casteList, int value) {
        this.context = applicationContext;
        this.casteList = casteList;
        this.value = value;
        inflter = (LayoutInflater.from(applicationContext));
    }

    @Override
    public int getCount() {
        return casteList.size() + 1;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        view = inflter.inflate(R.layout.spinner_lyt, null);
        TextView label = (TextView) view.findViewById(R.id.txtText);
        if (i == 0) {
            // Default selected Spinner item
            if (value==2){
                label.setText("Select type");
            }else {
                label.setText("Select PTM");
            }
        } else {
            CommonBean classObj = casteList.get(i - 1);
            if (value==2){
                label.setText(classObj.getName());
            }else {
                label.setText(classObj.getName()+" ("+ Utils.getDateFormated(classObj.getDate())+")");
            }

        }

        return view;
    }
}

