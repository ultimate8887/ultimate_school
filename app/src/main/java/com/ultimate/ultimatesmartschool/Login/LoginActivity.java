package com.ultimate.ultimatesmartschool.Login;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.google.android.material.snackbar.Snackbar;
import com.ultimate.ultimatesmartschool.BeanModule.ClassBean;
import com.ultimate.ultimatesmartschool.BeanModule.User;
import com.ultimate.ultimatesmartschool.MainActivity;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.UltimateSchoolApp;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.CommonProgress;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;
import com.ultimate.ultimatesmartschool.Utility.FirebaseGetDeviceToken;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends AppCompatActivity {
    ImageView top_curve;
    EditText edtUsename,edtPassword,edtSchoolid;
    TextView email_text, password_text, login_title,schoolid_text;
    TextView logo;
    LinearLayout new_user_layout;
    ConstraintLayout parent;
    CardView login_card;
    private SharedPreferences pref;
    private String deviceid;
    private int deviceIdChecked = 0;
    CommonProgress commonProgress;
    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        commonProgress=new CommonProgress(this);

        edtUsename = findViewById(R.id.edtUsename);
        edtSchoolid = findViewById(R.id.edtSchoolid);
        edtPassword = findViewById(R.id.edtPassword);
        parent = (ConstraintLayout) findViewById(R.id.parent);


        Utils.hideKeyboard(this);
        getDeviceid();
        fetchschoolid();
    }


    private void getDeviceid() {
        pref = UltimateSchoolApp.getInstance().getSharedPreferences(Constants.PREF_NAME, Context.MODE_PRIVATE);
        deviceid = pref.getString(Constants.DEVICE_TOKEN, "");
        Log.d("MainActivity", "Device ID: " + deviceid);
        if (deviceid == null || deviceid.isEmpty()) {
            FirebaseGetDeviceToken.getDeviceToken();
        }

        if (deviceIdChecked != 0) {
            if (deviceIdChecked < 4) {
                checkDeviceIDValid();
            }
        }
    }

    public void checkDeviceIDValid() {
        Log.d("MainActivity", "Checking Device ID Validity: " + deviceid);
        if (deviceid == null || deviceid.isEmpty()) {
            if (deviceIdChecked < 3) {
                deviceIdChecked++;
                getDeviceid();
            } else {
                FirebaseGetDeviceToken.getDeviceToken();
            }
        } else {
             registration_success();
        }
    }


    @OnClick(R.id.btnSignIn)
    public void submit() {
        Utils.hideKeyboard(this);
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (Utils.isConnectionAvailable(LoginActivity.this)) {
            Utils.hideKeyboard(LoginActivity.this);
            if (checkValidation()) {
                commonProgress.show();
                checkDeviceIDValid();
            }
        } else {
            showSnackBar("Please check internet connectivity.");
        }


    }
    public void fetchschoolid(){
        HashMap<String, String> params = new HashMap<String, String>();

    }
    private void registration_success() {
        HashMap<String, String> params = new HashMap<String, String>();
        params.put(Constants.SCHOOL_ID,edtSchoolid.getText().toString());
        params.put(Constants.USER_NAME, edtUsename.getText().toString());
        params.put(Constants.PASSWORD, edtPassword.getText().toString());
        params.put(Constants.USER_TYPE, Constants.ADMIN);
        if (deviceid == null || deviceid.isEmpty()) {
            params.put(Constants.DEVICE_TOKEN, "device id not found");
        }else{
            params.put(Constants.DEVICE_TOKEN, deviceid);
        }

        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.Login_URL, apiCallback, this, params);

    }

    ApiHandler.ApiCallback apiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            commonProgress.dismiss();
            if (error == null) {
                try {
                    User.setCurrentUser(jsonObject.getJSONObject(Constants.USERDATA));
                    showSnackBar("Successfully Logged In!");
                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                    startActivity(intent);
                    finish();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {

                Log.e("error", error.getMessage() + "");
                showSnackBar(error.getMessage());
            }
        }
    };

    private boolean checkValidation() {
        boolean valid = true;
        String errorMsg = null;
        if(edtSchoolid.getText().toString().trim().length() <=0){
            valid=false;
            errorMsg="Please enter school id";
        }else if (edtUsename.getText().toString().trim().length() <= 0) {
            valid = false;
            errorMsg = "Please enter username";
        } else if (edtPassword.getText().toString().trim().length() <= 0) {
            valid = false;
            errorMsg = "Please enter password";
        }
        if (!valid) {
            showSnackBar(errorMsg);
        }
        return valid;
    }

    public void showSnackBar(String errorMsg) {
        Toast snack = Toast.makeText(getApplicationContext(), errorMsg, Toast.LENGTH_LONG);
        View view = snack.getView();
//        TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
//        tv.setTextColor(Color.WHITE);
        snack.show();
    }





}
