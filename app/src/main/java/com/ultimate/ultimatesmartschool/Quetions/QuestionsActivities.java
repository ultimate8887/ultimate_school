package com.ultimate.ultimatesmartschool.Quetions;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.ultimate.ultimatesmartschool.R;

import butterknife.BindView;
import butterknife.ButterKnife;
@SuppressLint("MissingInflatedId")
public class QuestionsActivities extends AppCompatActivity {
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.tab_layout)
    TabLayout tabLayout;
    @BindView(R.id.imgBack)
    ImageView back;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_que_activities);
        ButterKnife.bind(this);
        txtTitle.setText("Question Paper SetUp");
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        //Tab layout
        final ViewPager viewPager = (ViewPager) findViewById(R.id.pager);
        tabLayout.setupWithViewPager(viewPager);

        final PagerAdapter adapter = new QueTabAdapter
                (getSupportFragmentManager());
        viewPager.setAdapter(adapter);
    }
}
