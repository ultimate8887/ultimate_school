package com.ultimate.ultimatesmartschool.Quetions;

import com.ultimate.ultimatesmartschool.BeanModule.CommonBean;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class QueCateNameBean {

    private static String ID = "id";
    private static String NAME = "name";
    private static String CLASS_NAME = "class_name";
    private static String TO_DATE = "date";
    private static String SEC = "section";
    private static String SUB = "subject";
    private static String TOT_MARKS = "total_marks";
    private static String PASS_MARKS = "pass_marks";

    /**
     * id : 2
     * name : NURSERY
     * school_id : 0
     * group_id : 9
     * order_by : 1
     */

    private String id;
    private String name;

    private String class_id;
    private String sec_id;

    public String getSubject_id() {
        return subject_id;
    }

    public void setSubject_id(String subject_id) {
        this.subject_id = subject_id;
    }

    public String getSec_id() {
        return sec_id;
    }

    public void setSec_id(String sec_id) {
        this.sec_id = sec_id;
    }

    public String getClass_id() {
        return class_id;
    }

    public void setClass_id(String class_id) {
        this.class_id = class_id;
    }

    private String subject_id;

    public String getClass_name() {
        return class_name;
    }

    public void setClass_name(String class_name) {
        this.class_name = class_name;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getTotal_marks() {
        return total_marks;
    }

    public void setTotal_marks(String total_marks) {
        this.total_marks = total_marks;
    }

    public String getPass_marks() {
        return pass_marks;
    }

    public void setPass_marks(String pass_marks) {
        this.pass_marks = pass_marks;
    }

    private String class_name;
    private String date;
    private String section;
    private String subject;
    private String total_marks;
    private String pass_marks;

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    private String time;

    /**
     * group_data : {"id":"1","name":"Pre-Primary(Nry-Prep2)"}
     */

    public CommonBean getGroup_data() {
        return group_data;
    }

    public void setGroup_data(CommonBean group_data) {
        this.group_data = group_data;
    }

    private CommonBean group_data;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }



    public static ArrayList<QueCateNameBean> parseClassArray(JSONArray arrayObj) {
        ArrayList list = new ArrayList<QueCateNameBean>();
        try {
            for (int i = 0; i < arrayObj.length(); i++) {
                QueCateNameBean p = parseClassObject(arrayObj.getJSONObject(i));
                if (p != null) {
                    list.add(p);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public static QueCateNameBean parseClassObject(JSONObject jsonObject) {
        QueCateNameBean casteObj = new QueCateNameBean();
        try {
            if (jsonObject.has(ID)) {
                casteObj.setId(jsonObject.getString(ID));
            }
            if (jsonObject.has(NAME) && !jsonObject.getString(NAME).isEmpty() && !jsonObject.getString(NAME).equalsIgnoreCase("null")) {
                casteObj.setName(jsonObject.getString(NAME));
            }
            if (jsonObject.has(CLASS_NAME) && !jsonObject.getString(CLASS_NAME).isEmpty() && !jsonObject.getString(CLASS_NAME).equalsIgnoreCase("null")) {
                casteObj.setClass_name(jsonObject.getString(CLASS_NAME));
            }
            if (jsonObject.has(TO_DATE) && !jsonObject.getString(TO_DATE).isEmpty() && !jsonObject.getString(TO_DATE).equalsIgnoreCase("null")) {
                casteObj.setDate(jsonObject.getString(TO_DATE));
            }

            if (jsonObject.has(SUB) && !jsonObject.getString(SUB).isEmpty() && !jsonObject.getString(SUB).equalsIgnoreCase("null")) {
                casteObj.setSubject(jsonObject.getString(SUB));
            }

            if (jsonObject.has(TOT_MARKS) && !jsonObject.getString(TOT_MARKS).isEmpty() && !jsonObject.getString(TOT_MARKS).equalsIgnoreCase("null")) {
                casteObj.setTotal_marks(jsonObject.getString(TOT_MARKS));
            }
            if (jsonObject.has(PASS_MARKS) && !jsonObject.getString(PASS_MARKS).isEmpty() && !jsonObject.getString(PASS_MARKS).equalsIgnoreCase("null")) {
                casteObj.setPass_marks(jsonObject.getString(PASS_MARKS));
            }
            if (jsonObject.has("class_id") && !jsonObject.getString("class_id").isEmpty() && !jsonObject.getString("class_id").equalsIgnoreCase("null")) {
                casteObj.setClass_id(jsonObject.getString("class_id"));
            }
            if (jsonObject.has("subject_id") && !jsonObject.getString("subject_id").isEmpty() && !jsonObject.getString("subject_id").equalsIgnoreCase("null")) {
                casteObj.setSubject_id(jsonObject.getString("subject_id"));
            }
            if (jsonObject.has("time") && !jsonObject.getString("time").isEmpty() && !jsonObject.getString("time").equalsIgnoreCase("null")) {
                casteObj.setTime(jsonObject.getString("time"));
            }
            if (jsonObject.has("sec_id") && !jsonObject.getString("sec_id").isEmpty() && !jsonObject.getString("sec_id").equalsIgnoreCase("null")) {
                casteObj.setSec_id(jsonObject.getString("sec_id"));
            }
            if (jsonObject.has(SEC) && !jsonObject.getString(SEC).isEmpty() && !jsonObject.getString(SEC).equalsIgnoreCase("null")) {
                casteObj.setSection(jsonObject.getString(SEC));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return casteObj;
    }

}
