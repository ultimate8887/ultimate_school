package com.ultimate.ultimatesmartschool.Quetions;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.ultimate.ultimatesmartschool.BeanModule.ClassBean;
import com.ultimate.ultimatesmartschool.BeanModule.SectionBean;
import com.ultimate.ultimatesmartschool.BeanModule.SubjectBeancls;
import com.ultimate.ultimatesmartschool.Examination.ClassAdapter;
import com.ultimate.ultimatesmartschool.Homework.AdapterOfSubjectList;
import com.ultimate.ultimatesmartschool.Homework.SectionnewAdapter;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.CommonProgress;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;


public class Activity_QueGroupFragment extends Fragment implements QueGroupAdapter.AddNewGroup {

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.textNorecord)
    TextView textNorecord;
    @BindView(R.id.parent)
    RelativeLayout parent;
    ArrayList<QueCateNameBean> dataList = new ArrayList<>();
    private QueGroupAdapter adapter;
    BottomSheetDialog dialog;
    CommonProgress commonProgress;
    @BindView(R.id.lytAddData)
    RelativeLayout lytAddData;
    Animation animation;
    public Activity_QueGroupFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
      //  return inflater.inflate(R.layout.fragment_activity__group, container, false);
        View view = inflater.inflate(R.layout.fragment_activity__group, container, false);
        ButterKnife.bind(this, view);
        animation = AnimationUtils.loadAnimation(getActivity(), R.anim.btn_blink_animation);
        commonProgress=new CommonProgress(getActivity());
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        adapter = new QueGroupAdapter(getActivity(), dataList, this,2);
        recyclerView.setAdapter(adapter);
        updateInst();
        fetchGroupList();
        return view;
    }

    private void fetchGroupList() {
        commonProgress.show();
        HashMap<String, String> params = new HashMap<String, String>();
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.QUE_LIST_URL, groupapiCallback, getActivity(), params);

    }

    ApiHandler.ApiCallback groupapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            commonProgress.dismiss();
            if (error == null) {
                try {
                    ArrayList<QueCateNameBean> groupList = QueCateNameBean.parseClassArray(jsonObject.getJSONArray(Constants.GROUP_DATA));
                    dataList.clear();
                    dataList.addAll(groupList);
                    adapter.notifyDataSetChanged();
                    textNorecord.setVisibility(View.GONE);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                textNorecord.setVisibility(View.VISIBLE);
                Utils.showSnackBar(error.getMessage(), parent);
            }
        }
    };

    private void updateInst() {
        lytAddData.setVisibility(View.VISIBLE);
        lytAddData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lytAddData.startAnimation(animation);
                openDialog(null,-1);
            }
        });
    }
    Spinner spinnerClass;
    ArrayList<ClassBean> classList = new ArrayList<>();
    String classid = "", classid_u="";
    Spinner spinnersection;
    ArrayList<SectionBean> sectionList = new ArrayList<>();
    String sectionid="",sectionid_u="";
    Spinner spinnersubject;
    ArrayList<SubjectBeancls> subjectList = new ArrayList<>();
    AdapterOfSubjectList adaptersub;
    String sub_id="",sub_id_u="", sub_name="",groupid="",from_date="";
    EditText edtTitle,edtT_marks,edtP_marks,edtTime;
    TextView fromdate;
    RelativeLayout one;
    private void openDialog(final QueCateNameBean data, final int pos) {
        dialog = new BottomSheetDialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.add_que_group_dialog);
        classid = "";
        sub_id = "";
        sectionid = "";
        classid_u = "";
        sub_id_u = "";
        sectionid_u = "";
        fromdate = (TextView) dialog.findViewById(R.id.date);
        one = (RelativeLayout) dialog.findViewById(R.id.one);
        edtTitle = (EditText) dialog.findViewById(R.id.edtTitle);
        edtTime = (EditText) dialog.findViewById(R.id.edtTime);
        edtT_marks = (EditText) dialog.findViewById(R.id.edtVenue);
        edtP_marks = (EditText) dialog.findViewById(R.id.edtVenue1);
        spinnerClass = (Spinner) dialog.findViewById(R.id.spinnerClass);
        spinnersection = (Spinner) dialog.findViewById(R.id.spinnersection);
        spinnersubject = (Spinner) dialog.findViewById(R.id.spinnersubject);
        Button btnSubmit = (Button) dialog.findViewById(R.id.btnSubmit);
        TextView txtSetup = (TextView) dialog.findViewById(R.id.txtSetup);
        if (pos != -1) {
            edtTitle.setText(dataList.get(pos).getName());
            edtTime.setText(dataList.get(pos).getTime());
            edtT_marks.setText(dataList.get(pos).getTotal_marks());
            edtP_marks.setText(dataList.get(pos).getPass_marks());
            fromdate.setText(dataList.get(pos).getDate());
            from_date= dataList.get(pos).getDate();
            groupid = dataList.get(pos).getId();
            classid_u = dataList.get(pos).getClass_id();
            sub_id_u = dataList.get(pos).getSubject_id();
            sectionid_u = dataList.get(pos).getSec_id();
            btnSubmit.setText("Update");
            txtSetup.setText("Update Question Paper");
            fetchClass();
            fetchSection();
            fetchSubject();
        }

        one.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dateset();
            }
        });

        fetchClass();


        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (classid.equalsIgnoreCase("")){
                    commonToast("Class is required");
                }else if (sub_id.equalsIgnoreCase("")){
                    commonToast("Subject is required");
                }else if (edtTitle.getText().toString().isEmpty()){
                    commonToast("Paper name field is required");
                }else if (edtT_marks.getText().toString().isEmpty()){
                    commonToast("Total Marks field is required");
                }else if (edtP_marks.getText().toString().isEmpty()){
                    commonToast("Pass Marks field is required");
                }else if (edtTime.getText().toString().isEmpty()){
                    commonToast("Time field is required");
                }else if (from_date.equalsIgnoreCase("")){
                    commonToast("Paper date is required");
                }else {
                    if (pos == -1) {
                        addNewGroup(edtTitle.getText().toString());
                    } else {
                        editGroup(data, pos, edtTitle.getText().toString());
                    }
//                    adapter.notifyDataSetChanged();
//                    dialog.dismiss();
//                    updateInst();
                }
            }
        });
        dialog.show();
        Window window = dialog.getWindow();
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    }

    @SuppressLint("NewApi")
    @Override
    public void addEditNewGroup(final QueCateNameBean data, final int pos) {
       openDialog(data,pos);
    }

    private void commonToast(String s) {
        Toast.makeText(getActivity(),s,Toast.LENGTH_SHORT).show();
    }

    public void dateset(){

        Calendar cal = Calendar.getInstance(TimeZone.getDefault()); // Get current date
        DatePickerDialog datePicker = new DatePickerDialog(getActivity(),
                R.style.MyDatePickerDialogTheme, ondate,
                cal.get(Calendar.YEAR),
                cal.get(Calendar.MONTH),
                cal.get(Calendar.DAY_OF_MONTH));
        datePicker.getDatePicker().setMinDate(new Date().getTime());
        datePicker.setCancelable(false);
        datePicker.show();
    }

    DatePickerDialog.OnDateSetListener ondate = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
            calendar.set(year, monthOfYear, dayOfMonth);
            calendar.set(Calendar.HOUR_OF_DAY, 00);
            calendar.set(Calendar.MINUTE, 00);
            calendar.set(Calendar.SECOND, 00);
            Date setdate = calendar.getTime();
            SimpleDateFormat fmtOut = new SimpleDateFormat("dd MMM, yyyy");
            String dateString = fmtOut.format(setdate);
            fromdate.setText(dateString);
            SimpleDateFormat dateFrmOut = new SimpleDateFormat("yyyy-MM-dd");
            from_date = dateFrmOut.format(setdate);
        }
    };


    private void fetchClass() {
        commonProgress.show();
        HashMap<String, String> params = new HashMap<String, String>();
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.CLASSLIST_URL, classapiCallback,getActivity(), params);
    }


    ApiHandler.ApiCallback classapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            commonProgress.dismiss();
            if (error == null) {
                try {
                    classList = ClassBean.parseClassArray(jsonObject.getJSONArray(Constants.CLASSDATA));
                    ClassAdapter adapter = new ClassAdapter(getActivity(), classList);
                    spinnerClass.setAdapter(adapter);
                    spinnerClass.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if (i > 0) {
                                classid = classList.get(i - 1).getId();
                                fetchSection();
                                fetchSubject();
                            }

                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                    if (!classid_u.equalsIgnoreCase("")) {
                        int i = 0;
                        for (ClassBean obj : classList) {
                            if (obj.getId().equals(classid_u)) {
                                spinnerClass.setSelection(i + 1);
                                break;
                            }
                            i++;
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Toast.makeText(getActivity(),error.getMessage(),Toast.LENGTH_LONG).show();
                // Utils.showSnackBar(error.getMessage(), view.findViewById(R.id.parent));
            }
        }
    };

    private void fetchSubject() {
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("id", classid);
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.SubjectList, subapiCallback, getActivity(), params);

    }

    ApiHandler.ApiCallback subapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            /* */

            if (error == null) {
                try {

                    subjectList =   SubjectBeancls.parseClassArray(jsonObject.getJSONArray("sub_data"));

                    adaptersub = new AdapterOfSubjectList(getActivity(), subjectList);
                    spinnersubject.setAdapter(adaptersub);
                    spinnersubject.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if (i != 0) {
                                sub_name = subjectList.get(i - 1).getName();
                                sub_id = subjectList.get(i - 1).getId();
                            }

                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });
                    if (!sub_id_u.equalsIgnoreCase("")) {
                        int i = 0;
                        for (SubjectBeancls obj : subjectList) {
                            if (obj.getId().equals(sub_id_u)) {
                                spinnersubject.setSelection(i + 1);
                                break;
                            }
                            i++;
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    };
    private void fetchSection() {
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("class_id", classid);
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.SECTIONLIST_URL, sectionapiCallback, getActivity(), params);
    }

    ApiHandler.ApiCallback sectionapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            if (error == null) {
                try {
                    sectionList = SectionBean.parseCommonArray(jsonObject.getJSONArray("hostelroom_data"));
                    SectionnewAdapter adapter = new SectionnewAdapter(getActivity(), sectionList);
                    spinnersection.setAdapter(adapter);
                    spinnersection.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if (i > 0) {
                                sectionid = sectionList.get(i - 1).getSection_id();
                                //fetchSubject();
                                // fetchHomework();
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });
                    if (!sectionid_u.equalsIgnoreCase("")) {
                        int i = 0;
                        for (SectionBean obj : sectionList) {
                            if (obj.getSection_id().equals(sectionid_u)) {
                                spinnersection.setSelection(i + 1);
                                break;
                            }
                            i++;
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Utils.showSnackBar(error.getMessage(), parent);
            }
        }
    };

    private void editGroup(QueCateNameBean data, final int pos, String name) {
        commonProgress.show();
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("name", name);
        params.put(Constants.ID, data.getId());
        params.put("class", classid);
        params.put("sec_id", sectionid);
        params.put("subject", sub_id);
        params.put("total_marks", edtT_marks.getText().toString().trim());
        params.put("pass_marks", edtP_marks.getText().toString().trim());
        params.put("date", from_date);
        params.put("time", edtTime.getText().toString().trim());
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.EDIT_QUE_GROUP_URL, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                commonProgress.dismiss();
                if (error == null) {
                    try {
                        Toast.makeText(getActivity(), jsonObject.getString("msg"), Toast.LENGTH_SHORT).show();
//                        QueCateNameBean groupdata = QueCateNameBean.parseClassObject(jsonObject.getJSONObject(Constants.GROUP_DATA));
//                        dataList.set(pos, groupdata);
//                        adapter.notifyItemChanged(pos);
                        dialog.dismiss();
                        fetchGroupList();
                        updateInst();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    Log.e("error", error.getMessage() + "");
                    Utils.showSnackBar(error.getMessage(), parent);
                }
            }
        }, getActivity(), params);

    }

    private void addNewGroup(String name) {
        commonProgress.show();
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("name", name);
        params.put("class", classid);
        params.put("sec_id", sectionid);
        params.put("subject", sub_id);
        params.put("total_marks", edtT_marks.getText().toString().trim());
        params.put("pass_marks", edtP_marks.getText().toString().trim());
        params.put("date", from_date);
        params.put("time", edtTime.getText().toString().trim());
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.ADD_QUE_URL, apiCallback, getActivity(), params);
    }

    ApiHandler.ApiCallback apiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            commonProgress.dismiss();
            if (error == null) {
                try {
                    Toast.makeText(getActivity(), jsonObject.getString("msg"), Toast.LENGTH_SHORT).show();
//                    QueCateNameBean groupdata = QueCateNameBean.parseClassObject(jsonObject.getJSONObject(Constants.GROUP_DATA));
//                    dataList.add(groupdata);
//                    adapter.notifyDataSetChanged();
                    dialog.dismiss();
                    fetchGroupList();
                    updateInst();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("error", error.getMessage() + "");
                Utils.showSnackBar(error.getMessage(), parent);
            }
        }
    };

    @Override
    public void deleteGroup(final QueCateNameBean data, final int position) {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:
                        commonProgress.show();
                        HashMap<String, String> params = new HashMap<String, String>();
                        params.put(Constants.ID, data.getId());
                        params.put("check", "group");
                        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.DEL_QUE_URL, new ApiHandler.ApiCallback() {
                            @Override
                            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                                commonProgress.dismiss();
                                if (error == null) {
                                    Toast.makeText(getActivity(), "Deleted Successfully", Toast.LENGTH_SHORT).show();
//                                    dataList.remove(position);
//                                    adapter.notifyDataSetChanged();
//                                    updateInst();
                                    fetchGroupList();
                                } else {
                                    Utils.showSnackBar(error.getMessage(), parent);
                                }
                            }
                        }, getActivity(), params);
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        //No button clicked
                        break;
                }
            }
        };
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("Are you sure you want to delete?").setPositiveButton("Yes", dialogClickListener)
                .setNegativeButton("No", dialogClickListener).show();
    }
}