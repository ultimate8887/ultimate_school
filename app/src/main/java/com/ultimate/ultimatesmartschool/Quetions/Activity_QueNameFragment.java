package com.ultimate.ultimatesmartschool.Quetions;


import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.textfield.TextInputLayout;
import com.ultimate.ultimatesmartschool.BeanModule.CommonBean;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.CommonProgress;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Activity_QueNameFragment extends Fragment implements QueListAdapter.AddNewGroup {
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    BottomSheetDialog dialog;
    @BindView(R.id.parent)
    RelativeLayout parent;
    ArrayList<CommonBean> dataList = new ArrayList<>();
    private QueListAdapter adapter;
    //    @BindView(R.id.lytInst)
//    LinearLayout lytInst;
  //  private Spinner spinnerGroup;
    @BindView(R.id.spinnerGroup)
    Spinner spinnerGroup;
    private ArrayList<QueCateNameBean> groupList = new ArrayList<>();
    private String groupid = "";
    CommonProgress commonProgress;
    RadioGroup radioGroup;
    EditText edtVenue,edtTitle;
    TextInputLayout inpVenue;
    public String to_date,venue="In School";
    public String from_date,venue_address="";
    RadioButton radioone;
    RadioButton radiomulti;
    TextView todate;
    TextView fromdate;
    RelativeLayout one,multi;

    @BindView(R.id.top_lyt)
    LinearLayout top_lyt;
    @BindView(R.id.txt_sub)
    TextView txt_sub;
    @BindView(R.id.txt_date)
    TextView txt_date;
    @BindView(R.id.txt_time)
    TextView txt_time;
    @BindView(R.id.txt_t_mark)
    TextView txt_t_mark;
    @BindView(R.id.txt_p_mark)
    TextView txt_p_mark;
    @BindView(R.id.txt_que)
    TextView txt_que;
    @BindView(R.id.txt_class)
    TextView txt_class;
    @BindView(R.id.lytAddData)
    RelativeLayout lytAddData;
    @BindView(R.id.textNorecord)
    TextView textNorecord;
    Animation animation;
    public Activity_QueNameFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_que_name, container, false);
        ButterKnife.bind(this, view);
        commonProgress=new CommonProgress(getActivity());
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        adapter = new QueListAdapter(getActivity(), dataList, this,2);
        recyclerView.setAdapter(adapter);
        animation = AnimationUtils.loadAnimation(getActivity(), R.anim.btn_blink_animation);
        updateInst();
        fetchGroup(0);
      //  fetchClass();
        return view;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
//        if(getView() != null && isVisibleToUser){
//            Log.i("1st","1st");
//            fetchGroup(0);
//        }
        if(isVisibleToUser){
            Log.i("1st","1st");
            fetchGroup(0);
        }
    }

    private void fetchClass(String groupidjj) {
        commonProgress.show();
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("group_id",groupid);
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.QUE_URL, classapiCallback, getActivity(), params);
    }
    ApiHandler.ApiCallback classapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            commonProgress.dismiss();
            if (error == null) {
                try {
                    ArrayList<CommonBean> classList = CommonBean.parseCommonArray(jsonObject.getJSONArray(Constants.CLASSDATA));
                    dataList.clear();
                    dataList.addAll(classList);
                    adapter.notifyDataSetChanged();
                    txt_que.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.VISIBLE);
                    textNorecord.setVisibility(View.GONE);
                    updateInst();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                textNorecord.setVisibility(View.VISIBLE);
                recyclerView.setVisibility(View.GONE);
              //  Utility.showSnackBar(error.getMessage(), parent);
            }
        }
    };

    private void updateInst() {
        lytAddData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lytAddData.startAnimation(animation);
                openDialog(null,-1);
            }
        });
    }

    private void openDialog(final CommonBean data, final int pos) {
        dialog = new BottomSheetDialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);

        dialog.setContentView(R.layout.add_que_classes_dialog);
        edtVenue = (EditText) dialog.findViewById(R.id.edtVenue);
        edtTitle = (EditText) dialog.findViewById(R.id.edtTitle);
        Button btnSubmit = (Button) dialog.findViewById(R.id.btnSubmit);
        TextView txtSetup = (TextView) dialog.findViewById(R.id.txtSetup);

        if (pos != -1) {
            edtTitle.setText(data.getName());
            edtVenue.setText(data.getSerial_no());
            //groupid = data.getId();
            btnSubmit.setText("Update");
            txtSetup.setText("Update Question");
        }

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                if (!venue.isEmpty() && to_date != null && from_date != null && groupid != null && !groupid.isEmpty()
//                        && !edtTitle.getText().toString().isEmpty()) {
                if (pos == -1) {

                    if (edtTitle.getText().toString().isEmpty()) {
                        Toast.makeText(getActivity(), "Question name field is required", Toast.LENGTH_SHORT).show();

                    }else if (edtVenue.getText().toString().isEmpty()) {
                        Toast.makeText(getActivity(), "Serial no. field is required", Toast.LENGTH_SHORT).show();

                    }else{
                        // Utility.showSnackBar("Please enter venue address", parent);
                        addClassName(edtTitle.getText().toString(), groupid);
                    }

                } else {
                    editClass(data, pos, edtTitle.getText().toString());
                }

//                } else {
//                    Utility.showSnackBar("Please enter data", parent);
//                    Toast.makeText(getActivity(), "Please enter data", Toast.LENGTH_SHORT).show();
//                }
            }
        });
        dialog.show();
//        Window window = dialog.getWindow();
//        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    }

    @SuppressLint("NewApi")
    @Override
    public void addEditNewGroup(final CommonBean data, final int pos) {
        openDialog(data,pos);
    }


    public void todateset(){
        Calendar cal = Calendar.getInstance(TimeZone.getDefault()); // Get current date
        DatePickerDialog datePicker = new DatePickerDialog(getActivity(),
                R.style.MyDatePickerDialogTheme, datefrom,
                cal.get(Calendar.YEAR),
                cal.get(Calendar.MONTH),
                cal.get(Calendar.DAY_OF_MONTH));
        datePicker.getDatePicker().setMinDate(new Date().getTime());
        datePicker.setCancelable(false);
        datePicker.show();

    }

    DatePickerDialog.OnDateSetListener datefrom = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
            calendar.set(year, monthOfYear, dayOfMonth);
            calendar.set(Calendar.HOUR_OF_DAY, 00);
            calendar.set(Calendar.MINUTE, 00);
            calendar.set(Calendar.SECOND, 00);
            Date setdate = calendar.getTime();
            SimpleDateFormat fmtOut = new SimpleDateFormat("dd MMM, yyyy");
            String dateString = fmtOut.format(setdate);
            todate.setText(dateString);
            SimpleDateFormat dateFrmOut = new SimpleDateFormat("yyyy-MM-dd");
            to_date = dateFrmOut.format(setdate);
        }
    };

    private void editClass(CommonBean data, final int pos, String name) {
        commonProgress.show();
        HashMap<String, String> params = new HashMap<String, String>();
        params.put(Constants.ID, data.getId());
        params.put("question", edtTitle.getText().toString().trim());
        params.put("group_id", groupid);
        params.put("serial_no", edtVenue.getText().toString().trim());
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.EDIT_QUE_URL, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                commonProgress.dismiss();
                if (error == null) {
                    try {
                        Toast.makeText(getActivity(), jsonObject.getString("msg"), Toast.LENGTH_SHORT).show();
                        dialog.dismiss();
                        fetchClass(groupid);
//                        CommonBean groupdata = CommonBean.parseCommonObject(jsonObject.getJSONObject(Constants.CLASSDATA));
//                        dataList.set(pos, groupdata);
//                        adapter.notifyItemChanged(pos);
//                        recyclerView.scrollToPosition(pos);
//                        updateInst();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    Log.e("error", error.getMessage() + "");
                    Utils.showSnackBar(error.getMessage(), parent);
                }
            }
        }, getActivity(), params);
    }

    private void addClassName(String name, String group_id) {
        commonProgress.show();
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("question", edtTitle.getText().toString().trim());
        params.put("group_id", group_id);
        params.put("serial_no", edtVenue.getText().toString().trim());
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.ADD_QUE_GROUP_URL, apiCallback, getActivity(), params);
    }

    ApiHandler.ApiCallback apiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            commonProgress.dismiss();
            if (error == null) {
                try {
                    Toast.makeText(getActivity(), jsonObject.getString("msg"), Toast.LENGTH_SHORT).show();
//                    CommonBean classdata = CommonBean.parseCommonObject(jsonObject.getJSONObject(Constants.CLASSDATA));
//                    dataList.add(classdata);
//                    adapter.notifyDataSetChanged();
                    dialog.dismiss();
                    fetchClass(groupid);
                    updateInst();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("error", error.getMessage() + "");
                Utils.showSnackBar(error.getMessage(), parent);
            }
        }
    };

    private void fetchGroup(final int pos) {
        commonProgress.show();
        HashMap<String, String> params = new HashMap<String, String>();
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.QUE_LIST_URL, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                ErpProgress.cancelProgressBar();
                commonProgress.dismiss();
                if (error == null) {
                    try {
                        groupList.clear();
                        groupList = QueCateNameBean.parseClassArray(jsonObject.getJSONArray(Constants.GROUP_DATA));
                        QueSpinnerAdapter adapter = new QueSpinnerAdapter(getActivity(), groupList,2);
                        spinnerGroup.setAdapter(adapter);
                        spinnerGroup.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                if (i > 0) {
                                    groupid = groupList.get(i - 1).getId();
                                    setData(groupList.get(i - 1));
                                    fetchClass(groupid);
                                }else{
                                    top_lyt.setVisibility(View.GONE);
                                    lytAddData.setVisibility(View.GONE);
                                    groupid="";
                                    txt_que.setVisibility(View.GONE);
                                    recyclerView.setVisibility(View.GONE);
                                }
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> adapterView) {

                            }
                        });
                        if (pos != -1) {
                            int i = 0;
                            for (QueCateNameBean obj : groupList) {
                                if (obj.getId().equals(groupid)) {
                                    spinnerGroup.setSelection(i + 1);
                                    break;
                                }
                                i++;
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    Utils.showSnackBar(error.getMessage(), parent);
                }
            }
        }, getActivity(), params);
    }

    private void setData(QueCateNameBean queCateNameBean) {
        lytAddData.setVisibility(View.VISIBLE);
        top_lyt.setVisibility(View.VISIBLE);
        txt_que.setVisibility(View.GONE);
        recyclerView.setVisibility(View.GONE);
        if (queCateNameBean.getSection()!=null){
            txt_class.setText(queCateNameBean.getClass_name()+"-"+queCateNameBean.getSection());
        }else{
            txt_class.setText(queCateNameBean.getClass_name());
        }
        txt_sub.setText(queCateNameBean.getSubject());
        txt_date.setText(Utils.getDateFormated(queCateNameBean.getDate()));
        txt_time.setText(queCateNameBean.getTime());
        txt_t_mark.setText(queCateNameBean.getTotal_marks());
        txt_p_mark.setText(queCateNameBean.getPass_marks());
    }

    @Override
    public void deleteGroup(final CommonBean data, final int position) {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        commonProgress.show();
                        HashMap<String, String> params = new HashMap<String, String>();
                        params.put(Constants.ID, data.getId());
                        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.DEL_QUE_URL, new ApiHandler.ApiCallback() {
                            @Override
                            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                                commonProgress.dismiss();
                                if (error == null) {
                                    Toast.makeText(getActivity(), "Deleted Successfully", Toast.LENGTH_SHORT).show();
//                                    dataList.remove(position);
//                                    adapter.notifyDataSetChanged();
//                                    if (dataList.size() > 0) {
//                                        textNorecord.setVisibility(View.VISIBLE);
//                                    } else {
//                                        textNorecord.setVisibility(View.GONE);
//                                    }
                                    fetchClass(groupid);
                                } else {
                                    Utils.showSnackBar(error.getMessage(), parent);
                                }
                            }
                        }, getActivity(), params);
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        //No button clicked
                        break;
                }
            }
        };
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("Are you sure you want to delete?").setPositiveButton("Yes", dialogClickListener)
                .setNegativeButton("No", dialogClickListener).show();
    }
}