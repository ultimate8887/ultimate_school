package com.ultimate.ultimatesmartschool.Quetions;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

public class QueTabAdapter extends FragmentPagerAdapter {

    int tabCount;
    // tab titles
    private String[] tabTitles = new String[]{"SetUp Paper", "Add Questions"};

    public QueTabAdapter(FragmentManager fm) {
        super(fm);
        this.tabCount = 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabTitles[position];
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                Activity_QueGroupFragment tab1 = new Activity_QueGroupFragment();
                return tab1;
            case 1:
                Activity_QueNameFragment tab2 = new Activity_QueNameFragment();
                return tab2;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return tabCount;
    }
}
