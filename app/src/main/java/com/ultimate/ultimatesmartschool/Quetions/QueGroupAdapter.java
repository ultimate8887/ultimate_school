package com.ultimate.ultimatesmartschool.Quetions;

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class QueGroupAdapter extends RecyclerView.Adapter<QueGroupAdapter.MyViewHolder> {

    private final AddNewGroup mAddNewGroup;
    private Context mContext;
    ArrayList<QueCateNameBean> dataList;
    int value=0;
    public QueGroupAdapter(Context mContext, ArrayList<QueCateNameBean> dataList, AddNewGroup mAddNewGroup, int value) {
        this.mContext = mContext;
        this.mAddNewGroup = mAddNewGroup;
        this.dataList = dataList;
        this.value = value;
    }
    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        LayoutInflater inflater = LayoutInflater.from(
                parent.getContext());
        View v =
                inflater.inflate(R.layout.que_gcs_common_lyt, parent, false);
        // set the view's size, margins, paddings and layout parameters
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, @SuppressLint("RecyclerView") final int position) {

        holder.txtAddText.setText("Add Question Paper");
        Animation animation = AnimationUtils.loadAnimation(mContext, R.anim.btn_blink_animation);
//        if (dataList.size() == position) {   //suppose data size is 4 then when position is 4 we have show add UI as it start count from o  then 4th position means  index 5
//            holder.lytAll.setVisibility(View.GONE);
//            holder.lytAddData.setVisibility(View.VISIBLE);
//            holder.lytAddData.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    holder.lytAddData.startAnimation(animation);
//                    if (mAddNewGroup != null) {
//                        mAddNewGroup.addEditNewGroup(null, -1);
//                    }
//                }
//            });
//        }
//        else {
            holder.lytAll.setVisibility(View.VISIBLE);
            holder.lytAddData.setVisibility(View.GONE);

            if (dataList.get(position).getName() != null) {
                String title = getColoredSpanned("Paper Name : ", "#000000");
                String Name = getColoredSpanned(""+dataList.get(position).getName(), "#383737");
                holder.txtTitle.setText(Html.fromHtml(title + " " + Name));
            }else{
                holder.txtTitle.setText("Not Found");
            }

            if (dataList.get(position).getSection() != null) {
                String title = getColoredSpanned("Class & Sec : ", "#383737");
                String Name = getColoredSpanned(""+dataList.get(position).getClass_name()+"-"+dataList.get(position).getSection(), "#5A5C59");
                holder.txtSub.setText(Html.fromHtml(title + " " + Name));
            }else{
                String title = getColoredSpanned("Class : ", "#383737");
                String Name = getColoredSpanned(""+dataList.get(position).getClass_name(), "#5A5C59");
                holder.txtSub.setText(Html.fromHtml(title + " " + Name));
            }

            if (dataList.get(position).getDate() != null) {
                String title = getColoredSpanned("Date : ", "#383737");
                String Name = getColoredSpanned(""+ Utils.getDateFormated(dataList.get(position).getDate()), "#5A5C59");
                holder.txtDate.setText(Html.fromHtml(title + " " + Name));
            }else{
                holder.txtDate.setText("Not Found");
            }

            if (dataList.get(position).getSubject() != null) {
                String title = getColoredSpanned("Subject : ", "#383737");
                String Name = getColoredSpanned(""+dataList.get(position).getSubject(), "#5A5C59");
                holder.txtVenue.setText(Html.fromHtml(title + " " + Name));
            }else{
                holder.txtVenue.setText("Not Found");
            }

            if (dataList.get(position).getTotal_marks() != null) {
                String title = getColoredSpanned("Total Marks : ", "#383737");
                String Name = getColoredSpanned(""+dataList.get(position).getTotal_marks(), "#5A5C59");
                holder.txtVenueAdd.setText(Html.fromHtml(title + " " + Name));
            }else{
                holder.txtVenueAdd.setText("Not Found");
            }

            if (dataList.get(position).getPass_marks() != null) {
                String title = getColoredSpanned("Pass Marks : ", "#383737");
                String Name = getColoredSpanned(""+dataList.get(position).getPass_marks(), "#5A5C59");
                holder.txtVenueAdd1.setText(Html.fromHtml(title + " " + Name));
            }else{
                holder.txtVenueAdd1.setText("Not Found");
            }

            if (dataList.get(position).getTime() != null) {
                String title = getColoredSpanned("Time : ", "#383737");
                String Name = getColoredSpanned(""+dataList.get(position).getTime(), "#5A5C59");
                holder.txtVenueAdd2.setText(Html.fromHtml(title + " " + Name));
            }else{
                holder.txtVenueAdd2.setText("Not Found");
            }



            holder.update.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    holder.update.startAnimation(animation);
                    if (mAddNewGroup != null) {
                        mAddNewGroup.addEditNewGroup(dataList.get(position),position);
                    }
                }
            });
            holder.imgTrash.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    holder.imgTrash.startAnimation(animation);
                    if (mAddNewGroup != null) {
                        mAddNewGroup.deleteGroup(dataList.get(position),position);
                    }
                }
            });
      //  }
    }

    @Override
    public int getItemCount() {
//        return (dataList.size() + 1);
        return (dataList.size());
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txtTitle)
        TextView txtTitle;
        @BindView(R.id.txtSub)
        TextView txtSub;
        @BindView(R.id.txtBatch)
        TextView txtDate;
        @BindView(R.id.txtVenueAdd)
        TextView txtVenueAdd;
        @BindView(R.id.txtVenueAdd1)
        TextView txtVenueAdd1;
        @BindView(R.id.txtVenueAdd2)
        TextView txtVenueAdd2;
        @BindView(R.id.txtVenue)
        TextView txtVenue;
        @BindView(R.id.trash)
        ImageView imgTrash;
        @BindView(R.id.update)
        ImageView update;
        @BindView(R.id.lytAddData)
        RelativeLayout lytAddData;
        @BindView(R.id.lytAll)
        RelativeLayout lytAll;
        @BindView(R.id.txtAddText)
        TextView txtAddText;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
          //  txtSub.setVisibility(View.GONE);
        }
    }

    public interface AddNewGroup {
        public void addEditNewGroup(QueCateNameBean data, int i);

        public void deleteGroup(QueCateNameBean data, int position);
    }
}
