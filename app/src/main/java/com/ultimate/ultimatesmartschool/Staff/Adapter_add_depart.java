package com.ultimate.ultimatesmartschool.Staff;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ultimate.ultimatesmartschool.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Adapter_add_depart extends RecyclerView.Adapter<Adapter_add_depart.Viewholder> {
    private final AddNewDepartment maddNewDepartment;
    ArrayList<Deparment_bean> depart_list;
    Context context;

    public Adapter_add_depart(Context  context,ArrayList<Deparment_bean> depart_list, AddNewDepartment maddNewDepartment) {
        this.depart_list=depart_list;
        this.context=context;
        this.maddNewDepartment=maddNewDepartment;
    }

    @NonNull
    @Override
    public Adapter_add_depart.Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(
                parent.getContext());
        View v =
                inflater.inflate(R.layout.add_depart_adapt_lay, parent, false);
        Adapter_add_depart.Viewholder vh = new Adapter_add_depart.Viewholder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull Adapter_add_depart.Viewholder holder, final int position) {
        if (depart_list.size() == position) {   //suppose data size is 4 then when position is 4 we have show add UI as it start count from o  then 4th position means  index 5
            holder.lytAlldepart.setVisibility(View.GONE);
            holder.lytAddDepart.setVisibility(View.VISIBLE);
            holder.lytAddDepart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (maddNewDepartment != null) {
                        maddNewDepartment.addEditNewDepartmnnt(null,-1);
                    }

                }
            });
        }else{
            holder.lytAlldepart.setVisibility(View.VISIBLE);
            holder.lytAddDepart.setVisibility(View.GONE);
            holder.txtDepartmentname.setText(depart_list.get(position).getName());
            holder.lytAlldepart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (maddNewDepartment != null) {
                        maddNewDepartment.addEditNewDepartmnnt(depart_list.get(position),position);
                    }
                }
            });
            holder.imgTrash.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (maddNewDepartment != null) {
                        maddNewDepartment.deleteDeaprt(depart_list.get(position),position);
                    }
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return depart_list.size()+1;
    }

    public void setdeparmentList(ArrayList<Deparment_bean> depart_list) {
        this.depart_list = depart_list;
    }

    public class Viewholder extends RecyclerView.ViewHolder {
        //        @BindView(R.id.txtTitle)
//        TextView txtTitle;
        @BindView(R.id.txtdeparment)
        TextView txtDepartmentname;
        @BindView(R.id.trash)
        ImageView imgTrash;
        @BindView(R.id.lytAdddepart)
        RelativeLayout lytAddDepart;
        @BindView(R.id.lytAlldepart)
        RelativeLayout lytAlldepart;
        @BindView(R.id.txtAddTextdepart)
        TextView txtAdddeparment;
        public Viewholder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
            txtAdddeparment.setText("Add Department");
        }
    }


    public interface AddNewDepartment {
        public void addEditNewDepartmnnt(Deparment_bean data, int i);

        public void deleteDeaprt(Deparment_bean data, int position);
    }
}
