package com.ultimate.ultimatesmartschool.Staff;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.StuGatePass.ClassBeans;

import java.util.ArrayList;

public class InchargeSpinner  extends BaseAdapter {
    private final ArrayList<TeachingStaffBean> casteList;
    Context context;
    LayoutInflater inflter;

    public InchargeSpinner(Context applicationContext, ArrayList<TeachingStaffBean> casteList) {
        this.context = applicationContext;
        this.casteList = casteList;
        inflter = (LayoutInflater.from(applicationContext));
    }

    @Override
    public int getCount() {
        return casteList.size() + 1;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        view = inflter.inflate(R.layout.spinner_lyt, null);
        TextView label = (TextView) view.findViewById(R.id.txtText);
        if (i == 0) {
            // Default selected Spinner item
            label.setText("Select Staff");
        } else {
            TeachingStaffBean classObj = casteList.get(i - 1);
            label.setText(classObj.getName()+" \n("+classObj.getStaff_desg_name()+")");
        }

        return view;
    }
}
