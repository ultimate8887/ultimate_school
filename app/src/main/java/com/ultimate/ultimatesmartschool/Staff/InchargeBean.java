package com.ultimate.ultimatesmartschool.Staff;

import com.ultimate.ultimatesmartschool.Utility.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class InchargeBean {

    private static String ID="id";
    private static String PROFILE = "profile";
    private static String STAFF_DESG_NAME = "staff_desg_name";

    private String gender;

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getFather_name() {
        return father_name;
    }

    public void setFather_name(String father_name) {
        this.father_name = father_name;
    }

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }

    public String getStaff_desg_name() {
        return staff_desg_name;
    }

    public void setStaff_desg_name(String staff_desg_name) {
        this.staff_desg_name = staff_desg_name;
    }

    private String father_name;
    private String profile;
    private String staff_desg_name;

    private String id;
    private String c_id;
    private String c_name;
    private String s_id;
    private String s_name;
    private String created_on;
    private String section_id;
    private String section_name;

    public String getEs_groupname() {
        return es_groupname;
    }

    public void setEs_groupname(String es_groupname) {
        this.es_groupname = es_groupname;
    }

    public String getEs_groupsid() {
        return es_groupsid;
    }

    public void setEs_groupsid(String es_groupsid) {
        this.es_groupsid = es_groupsid;
    }

    private String es_groupname;
    private String es_groupsid;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getC_id() {
        return c_id;
    }

    public void setC_id(String c_id) {
        this.c_id = c_id;
    }

    public String getC_name() {
        return c_name;
    }

    public void setC_name(String c_name) {
        this.c_name = c_name;
    }

    public String getS_id() {
        return s_id;
    }

    public void setS_id(String s_id) {
        this.s_id = s_id;
    }

    public String getS_name() {
        return s_name;
    }

    public void setS_name(String s_name) {
        this.s_name = s_name;
    }

    public String getCreated_on() {
        return created_on;
    }

    public void setCreated_on(String created_on) {
        this.created_on = created_on;
    }

    public String getSection_id() {
        return section_id;
    }

    public void setSection_id(String section_id) {
        this.section_id = section_id;
    }

    public String getSection_name() {
        return section_name;
    }

    public void setSection_name(String section_name) {
        this.section_name = section_name;
    }


    public static ArrayList<InchargeBean> parseInchargeBeanArray(JSONArray jsonArray) {

        ArrayList<InchargeBean> list = new ArrayList<InchargeBean>();
        try {

            for (int i = 0; i < jsonArray.length(); i++) {
                InchargeBean p = parseInchargeBeanObject(jsonArray.getJSONObject(i));
                if (p != null) {
                    list.add(p);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();

        }
        return list;

    }

    private static InchargeBean parseInchargeBeanObject(JSONObject jsonObject) {
        InchargeBean casteObj = new InchargeBean();
        try {
            if (jsonObject.has(ID)) {
                casteObj.setId(jsonObject.getString(ID));
            }

            if (jsonObject.has("c_id") && !jsonObject.getString("c_id").isEmpty() && !jsonObject.getString("c_id").equalsIgnoreCase("null")) {
                casteObj.setC_id(jsonObject.getString("c_id"));
            }
            if (jsonObject.has("c_name") && !jsonObject.getString("c_name").isEmpty() && !jsonObject.getString("c_name").equalsIgnoreCase("null")) {
                casteObj.setC_name(jsonObject.getString("c_name"));
            }

            if (jsonObject.has("s_id") && !jsonObject.getString("s_id").isEmpty() && !jsonObject.getString("s_id").equalsIgnoreCase("null")) {
                casteObj.setS_id(jsonObject.getString("s_id"));
            }
            if (jsonObject.has("s_name") && !jsonObject.getString("s_name").isEmpty() && !jsonObject.getString("s_name").equalsIgnoreCase("null")) {
                casteObj.setS_name(jsonObject.getString("s_name"));
            }

            if (jsonObject.has("created_on") && !jsonObject.getString("created_on").isEmpty() && !jsonObject.getString("created_on").equalsIgnoreCase("null")) {
                casteObj.setCreated_on(jsonObject.getString("created_on"));
            }
            if (jsonObject.has("section_id") && !jsonObject.getString("section_id").isEmpty() && !jsonObject.getString("section_id").equalsIgnoreCase("null")) {
                casteObj.setSection_id(jsonObject.getString("section_id"));
            }

            if (jsonObject.has("section_name") && !jsonObject.getString("section_name").isEmpty() && !jsonObject.getString("section_name").equalsIgnoreCase("null")) {
                casteObj.setSection_name(jsonObject.getString("section_name"));
            }

            if (jsonObject.has(STAFF_DESG_NAME) && !jsonObject.getString(STAFF_DESG_NAME).isEmpty() && !jsonObject.getString(STAFF_DESG_NAME).equalsIgnoreCase("null")) {
                casteObj.setStaff_desg_name(jsonObject.getString(STAFF_DESG_NAME));
            }

            if (jsonObject.has("father_name") && !jsonObject.getString("father_name").isEmpty() && !jsonObject.getString("father_name").equalsIgnoreCase("null")) {
                casteObj.setFather_name(jsonObject.getString("father_name"));
            }


            if (jsonObject.has("gender") && !jsonObject.getString("gender").isEmpty() && !jsonObject.getString("gender").equalsIgnoreCase("null")) {
                casteObj.setGender(jsonObject.getString("gender"));
            }

            if (jsonObject.has(PROFILE) && !jsonObject.getString(PROFILE).isEmpty() && !jsonObject.getString(PROFILE).equalsIgnoreCase("null")) {
                casteObj.setProfile(Constants.getImageBaseURL()+jsonObject.getString(PROFILE));
            }


            if (jsonObject.has("es_groupname") && !jsonObject.getString("es_groupname").isEmpty() && !jsonObject.getString("es_groupname").equalsIgnoreCase("null")) {
                casteObj.setEs_groupname(jsonObject.getString("es_groupname"));
            }


            if (jsonObject.has("es_groupsid") && !jsonObject.getString("es_groupsid").isEmpty() && !jsonObject.getString("es_groupsid").equalsIgnoreCase("null")) {
                casteObj.setEs_groupsid(jsonObject.getString("es_groupsid"));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return casteObj;
    }

}
