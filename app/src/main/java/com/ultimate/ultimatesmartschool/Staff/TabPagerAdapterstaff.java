package com.ultimate.ultimatesmartschool.Staff;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

public class TabPagerAdapterstaff extends FragmentPagerAdapter {
    int tabCount;
    private String[] tabTitles = new String[]{"Department", "Post"};
    public TabPagerAdapterstaff(FragmentManager fm) {
        super(fm);
        this.tabCount =2;
    }
    @Override
    public CharSequence getPageTitle(int position) {
        return tabTitles[position];
    }
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                Add_drpat_frag add_drpat_frag = new Add_drpat_frag();
                return add_drpat_frag;
            case 1:
                Add_post_frag add_post_frag = new Add_post_frag();
                return add_post_frag;

            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return tabCount;
    }
}
