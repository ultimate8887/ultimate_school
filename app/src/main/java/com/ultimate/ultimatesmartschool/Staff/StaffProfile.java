package com.ultimate.ultimatesmartschool.Staff;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatTextView;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.siyamed.shapeimageview.RoundedImageView;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Utility.Utils;


import androidmads.library.qrgenearator.QRGContents;
import androidmads.library.qrgenearator.QRGEncoder;
import butterknife.BindView;
import butterknife.ButterKnife;

public class StaffProfile extends AppCompatActivity {
    @BindView(R.id.txtName)
    AppCompatTextView txtName;
    @BindView(R.id.imgPro)
    ImageView imgPro;
    @BindView(R.id.profile_bg)
    ImageView profile_bg;
    @BindView(R.id.txtdepart)
    AppCompatTextView txtdepart;
    @BindView(R.id.txtBG)
    AppCompatTextView txtBG;
    @BindView(R.id.txtDOB)
    AppCompatTextView txtDOB;
//    @BindView(R.id.txtAadhar)
//    AppCompatTextView txtAadhar;
    @BindView(R.id.txtGender)
    AppCompatTextView txtGender;
    @BindView(R.id.txtAddress)
    AppCompatTextView txtAddress;
    @BindView(R.id.txtContact)
    AppCompatTextView txtContact;
    @BindView(R.id.txtEmail)
    AppCompatTextView txtEmail;
    @BindView(R.id.txtTitle)
    AppCompatTextView txtTitle;
    @BindView(R.id.txtempid)AppCompatTextView txtempid;
    @BindView(R.id.txtusername)AppCompatTextView txtusername;
    @BindView(R.id.txtpassword)AppCompatTextView txtpassword;
    ImageView imgback;
    View_staff_bean user;
    private Bitmap bitmap;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_staff_profile);
        ButterKnife.bind(this);
        txtTitle.setText("Staff Detail");
        imgback = (ImageView) findViewById(R.id.imgBack);
        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        if(getIntent()!= null && getIntent().hasExtra("staffdet")){
            Gson gson = new Gson();
            user = gson.fromJson(getIntent().getStringExtra("staffdet"),View_staff_bean.class);
        }else {
            return;
        }
        txtName.setText("Name: "+user.getName());
        if(user.getUsername()!=null){
            txtusername.setText("Username: "+user.getUsername());
        }
        if(user.getPassword()!=null){
            txtpassword.setText("Password: "+user.getPassword());
        }
        txtempid.setText("Employee Id: "+user.getId());
        if (user.getDesignation()!=null && user.getPost()!=null ) {
            txtdepart.setText("Designation: "+user.getDesignation()+"("+user.getPost()+")");
        }
        if(user.getBloodgroup()!=null) {
            txtBG.setText("Blood Group" + user.getBloodgroup());
        }else {
            txtBG.setText("Not Mentioned");
        }
        if (user.getEmail()!=null){
            txtEmail.setText("Email: "+user.getEmail());
        }else {
            txtEmail.setText("Not Mentioned");
        }
        txtEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String email=user.getEmail();
                Intent intent = new Intent(Intent.ACTION_SENDTO); // it's not ACTION_SEND
                intent.setType("text/plain");
                intent.putExtra(Intent.EXTRA_SUBJECT, "Subject of email");
                intent.putExtra(Intent.EXTRA_TEXT, "Body of email");
                intent.setData(Uri.parse("mailto:"+email)); // or just "mailto:" for blank
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK); // this will make such that when user returns to your app, your app is displayed, instead of the email app.
                startActivity(intent);
            }
        });

        if (user.getPhonno()!=null){
            txtContact.setText("Mobile: "+user.getPhonno());
        }else {
            txtContact.setText("Not Mentioned");
        }
        txtContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String phoneNo=user.getPhonno();
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:"+ phoneNo));
                startActivity(intent);
            }
        });

        if (user.getGender()!=null){
            txtGender.setText(user.getGender());
    }else {
            txtGender.setText("Not Mentioned");
    }
        if (user.getAddress()!=null){
            txtAddress.setText("Address: "+user.getAddress());
        }else {
            txtAddress.setText("Not Mentioned");
        }
        if (user.getDateofbirth()!=null){
            txtDOB.setText("DOB: "+ Utils.getDateFormated(user.getDateofbirth()));
        }else {
            txtDOB.setText("Not Mentioned");
        }
        if (user.getImage() != null)
            Picasso.get().load(user.getImage()).placeholder(getResources().getDrawable(R.drawable.boy)).into(imgPro);
//        if (user.getImage() != null)
//            Picasso.with(this).load(user.getImage()).placeholder(getResources().getDrawable(R.drawable.boy)).into(profile_bg);
        createQR();
    }

    private void createQR() {
        String inputValue="";
        if (user.getId() !=null){
            inputValue=user.getId();
        }
        if (!inputValue.equalsIgnoreCase("")) {
            //Toast.makeText(getApplicationContext(),"hiiii "+inputValue,Toast.LENGTH_LONG).show();
            QRGEncoder qrgEncoder = new QRGEncoder(inputValue, null, QRGContents.Type.TEXT, 800);
//            qrgEncoder.setColorBlack(R.color.dark_black);
//            qrgEncoder.setColorWhite(R.color.white);
            // Getting QR-Code as Bitmap
            bitmap = qrgEncoder.getBitmap();

            // Setting Bitmap to ImageView
            profile_bg.setImageBitmap(bitmap);

        }
    }
}
