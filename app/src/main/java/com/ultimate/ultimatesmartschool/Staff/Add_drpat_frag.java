package com.ultimate.ultimatesmartschool.Staff;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.ultimate.ultimatesmartschool.BeanModule.User;
import com.ultimate.ultimatesmartschool.Login.LoginActivity;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Add_drpat_frag extends Fragment implements Adapter_add_depart.AddNewDepartment {
    EditText edtTitle;
    Dialog departdialog;
    @BindView(R.id.parent)
    LinearLayout parent;
    String name;
    @BindView(R.id.lytInsts)
    LinearLayout lytInst;
    @BindView(R.id.recyclerViewadddpart)
    RecyclerView recyclerView;
    private Adapter_add_depart adapter;
    ArrayList<Deparment_bean> depart_list = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_deparment, container, false);
        ButterKnife.bind(this, view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        adapter = new Adapter_add_depart(getActivity(), depart_list, this);
        recyclerView.setAdapter(adapter);
        updateInst();
        fetchDpartment();
        return view;
    }


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if(getView() != null && isVisibleToUser){
            Log.i("1st","1st");
            updateInst();
            fetchDpartment();
        }
    }

    private void updateInst() {
        if (depart_list.size() > 0) {
            lytInst.setVisibility(View.VISIBLE);
        } else {
            lytInst.setVisibility(View.GONE);
        }
    }

    private void fetchDpartment() {
        HashMap<String, String> params = new HashMap<>();
        ApiHandler.apiHit(Request.Method.GET, Constants.getBaseURL() + Constants.deparmrnt, apiCallback, getActivity(), params);
    }

    ApiHandler.ApiCallback apiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            // txtupload.setVisibility(View.VISIBLE);
            if (error == null) {
                try {
                    if (depart_list != null) {
                        depart_list.clear();
                    }
                    JSONArray jsonArray = jsonObject.getJSONArray("department_data");
                    depart_list = Deparment_bean.parseDepartmentArray(jsonArray);
                    //Log.d("TAG", "onDataFetched: " + examList.size());
                    //adapter.setHQList(obj);
                    adapter.setdeparmentList(depart_list);
                    adapter.notifyDataSetChanged();
                    updateInst();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Utils.showSnackBar(error.getMessage(), parent);
                if (error.getStatusCode() == 405) {
                    User.logout();
                    startActivity(new Intent(getActivity(), LoginActivity.class));
                    getActivity().finish();
                }
            }
        }
    };

    @Override
    public void addEditNewDepartmnnt(final Deparment_bean data, final int i) {
        departdialog = new Dialog(getActivity());
        departdialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        departdialog.setCancelable(true);
        departdialog.setContentView(R.layout.add_depart_dialog);
        edtTitle = (EditText) departdialog.findViewById(R.id.edtTitledepartname);
        Button btnSubmit = (Button) departdialog.findViewById(R.id.btnSubmitexm);

        if (i != -1) {
            edtTitle.setText(depart_list.get(i).getName());
            btnSubmit.setText("Update");
        }

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edtTitle.getText().toString().length() > 0) {
                if (i == -1) {
                    addNewdepart(edtTitle.getText().toString());
                } else {
                    editdepart(data, i, edtTitle.getText().toString());
                }
                adapter.notifyDataSetChanged();
                departdialog.dismiss();
                updateInst();
                }else {
                    Toast.makeText(getActivity(),"Enter Department Name",Toast.LENGTH_SHORT).show();

                }
            }
        });
        departdialog.show();
    }


    private void editdepart(final Deparment_bean data, final int i, String s) {
        HashMap<String, String> params = new HashMap<>();
        name = edtTitle.getText().toString();
        ApiHandler.apiHit(Request.Method.PUT, Constants.getBaseURL() + Constants.deparmrnt + Constants.dpartment_id + data.getId() + Constants.dpartment_name + name, apiCallback, getActivity(), params);
    }

    private void addNewdepart(String s) {
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("d_name", s);
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.deparmrnt, apiCallback, getActivity(), params);

    }

    @Override
    public void deleteDeaprt(final Deparment_bean data, final int position) {
        DialogInterface.OnClickListener dialogclicllistner = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        HashMap<String, String> params = new HashMap<String, String>();
                        //params.put("d_id", data.getId());
                        ApiHandler.apiHit(Request.Method.DELETE, Constants.getBaseURL() + Constants.deparmrnt + Constants.dpartment_id + data.getId(), apiCallback, getActivity(), params);
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        //No button clicked
                        break;
                }
            }
        };
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("Are you sure you want to delete?").setPositiveButton("Yes", dialogclicllistner)
                .setNegativeButton("No", dialogclicllistner).show();
    }

}
