package com.ultimate.ultimatesmartschool.Staff;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ultimate.ultimatesmartschool.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Adapter_post_name extends RecyclerView.Adapter<Adapter_post_name.Viewholder> {
    private final AddNewPost mAddnewpost;
    ArrayList<Postname_Bean> postname_list;
    Context context;


    public Adapter_post_name(Context context, ArrayList<Postname_Bean> postname_list, AddNewPost mAddnewpost) {
        this.context=context;
        this.postname_list=postname_list;
        this.mAddnewpost=mAddnewpost;
    }

    @NonNull
    @Override
    public Adapter_post_name.Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(
                parent.getContext());
        View v =
                inflater.inflate(R.layout.add_depart_adapt_lay, parent, false);
        // set the view's size, margins, paddings and layout parameters
        Adapter_post_name.Viewholder vh = new Adapter_post_name.Viewholder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull Adapter_post_name.Viewholder holder, final int position) {
        if (postname_list.size() == position) {   //suppose data size is 4 then when position is 4 we have show add UI as it start count from o  then 4th position means  index 5
            holder.lytAllpost.setVisibility(View.GONE);
            holder.lytAddpost.setVisibility(View.VISIBLE);
            holder.lytAddpost.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mAddnewpost != null) {
                        mAddnewpost.addEditNewPost(null,-1);
                    }

                }
            });
        }else{
            holder.lytAllpost.setVisibility(View.VISIBLE);
            holder.lytAddpost.setVisibility(View.GONE);
            holder.txtpostname.setText(postname_list.get(position).getName());
            holder.lytAllpost.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mAddnewpost != null) {
                        mAddnewpost.addEditNewPost(postname_list.get(position),position);
                    }
                }
            });
            holder.imgTrash.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mAddnewpost != null) {
                        mAddnewpost.deletePost(postname_list.get(position),position);
                    }
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return postname_list.size()+1;
    }

    public void setdpostList(ArrayList<Postname_Bean> postname_list) {
        this.postname_list = postname_list;
    }

    public class Viewholder extends RecyclerView.ViewHolder {
        @BindView(R.id.txtdeparment)
        TextView txtpostname;
        @BindView(R.id.trash)
        ImageView imgTrash;
        @BindView(R.id.lytAdddepart)
        RelativeLayout lytAddpost;
        @BindView(R.id.lytAlldepart)
        RelativeLayout lytAllpost;
        @BindView(R.id.txtAddTextdepart)
        TextView txtAddpost;
        public Viewholder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
            txtAddpost.setText("Add Post");
        }
    }
    public interface AddNewPost {
        public void addEditNewPost(Postname_Bean data, int i);

        public void deletePost(Postname_Bean data, int position);
    }
}
