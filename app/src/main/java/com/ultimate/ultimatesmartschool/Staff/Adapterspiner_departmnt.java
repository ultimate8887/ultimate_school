package com.ultimate.ultimatesmartschool.Staff;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.ultimate.ultimatesmartschool.R;

import java.util.ArrayList;

public class Adapterspiner_departmnt extends BaseAdapter {

    private final ArrayList<Deparment_bean> departmnt_list;
    Context context;
    LayoutInflater inflter;
    public Adapterspiner_departmnt(Context context, ArrayList<Deparment_bean> departmnt_list) {
        this.context = context;
        this.departmnt_list = departmnt_list;
        inflter = (LayoutInflater.from(context));
    }
    @Override
    public int getCount() {
        return departmnt_list.size()+1;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        convertView = inflter.inflate(R.layout.spinner_lyt, null);
        TextView label = (TextView) convertView.findViewById(R.id.txtText);
        if (position == 0) {
            // Default selected Spinner item
            label.setText("Select Department");
        } else {
            Deparment_bean classObj = departmnt_list.get(position - 1);
            label.setText(classObj.getName());
        }

        return convertView;

    }
}
