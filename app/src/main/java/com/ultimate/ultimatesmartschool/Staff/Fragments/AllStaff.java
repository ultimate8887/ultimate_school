package com.ultimate.ultimatesmartschool.Staff.Fragments;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Environment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetSequence;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;
import com.ultimate.ultimatesmartschool.BeanModule.User;
import com.ultimate.ultimatesmartschool.Home.Adapter_spinner_viewdepart;
import com.ultimate.ultimatesmartschool.Home.ViewStaffActivity;
import com.ultimate.ultimatesmartschool.Login.LoginActivity;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Staff.Deparment_bean;
import com.ultimate.ultimatesmartschool.Staff.StaffBirthdayList;
import com.ultimate.ultimatesmartschool.Staff.StaffProfile;
import com.ultimate.ultimatesmartschool.Staff.VAdapter_view_staff;
import com.ultimate.ultimatesmartschool.Staff.View_staff_bean;
import com.ultimate.ultimatesmartschool.Student.StudentList;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.CommonProgress;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.content.Context.MODE_PRIVATE;


public class AllStaff extends Fragment implements VAdapter_view_staff.StaffCallBack {


    @BindView(R.id.news_rv)
    RecyclerView recyclerView;
    @BindView(R.id.imgBack)
    ImageView back;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.totalRecord)
    TextView totalRecord;
    //private LinearLayoutManager layoutManager;
    RecyclerView.LayoutManager layoutManager;
    @BindView(R.id.root_layout)
    RelativeLayout parent;
    ArrayList<Deparment_bean> department_list=new ArrayList<>();
    Adapter_spinner_viewdepart adap;
    String departmentid="";
    private VAdapter_view_staff adapter;
    ArrayList<View_staff_bean> viewstafflist=new ArrayList<>();
    int loaded=0;
    boolean isDark = false;
    EditText searchInput ;
    CharSequence search="";
    CommonProgress commonProgress;
    @BindView(R.id.textNorecord)TextView txtNorecord;
    SharedPreferences sharedPreferences;
    @BindView(R.id.export)
    FloatingActionButton export;

    public AllStaff() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.activity_student_list_byroll, container, false);
        ButterKnife.bind(this,view);
        commonProgress=new CommonProgress(getActivity());
        searchInput = view.findViewById(R.id.search_input);
        txtTitle.setVisibility(View.GONE);
        back.setVisibility(View.GONE);
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        adapter = new VAdapter_view_staff(viewstafflist, getActivity(),this);
        recyclerView.setAdapter(adapter);
        fetchstaffdetail();
        isDark = !isDark ;

        searchInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if(s.length()==0)
                {
                    fetchstaffdetail();
                }else {
                    adapter.getFilter().filter(s);
                    search = s;
                    totalRecord.setText("Search Record:- " +String.valueOf(adapter.viewstafflist.size()));
                    if (adapter.viewstafflist.size()==0){
                        export.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });


        return view;
    }

    private void setShowcaseViewP() {
        new TapTargetSequence(getActivity())
                .targets(TapTarget.forView(export,"Export Excel!","Tap the Export button to export Staff list.")
                        .outerCircleColor(R.color.light)
                        .outerCircleAlpha(0.96f)
                        .targetCircleColor(R.color.white)
                        .titleTextSize(20)
                        .titleTextColor(R.color.white)
                        .descriptionTextSize(14)
                        .descriptionTextColor(R.color.black)
                        .textColor(R.color.black)
                        .textTypeface(Typeface.SANS_SERIF)
                        .dimColor(R.color.black)
                        .drawShadow(true)
                        .cancelable(false)
                        .tintTarget(true)
                        .transparentTarget(true)
                        .targetRadius(40)).listener(new TapTargetSequence.Listener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onSequenceFinish() {
                // Toast.makeText(getApplicationContext(),"Sequence Finished",Toast.LENGTH_SHORT).show();
                savePrefDataP();

            }

            @Override
            public void onSequenceStep(TapTarget lastTarget, boolean targetClicked) {
                // Toast.makeText(getActivity(),"GREAT!",Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSequenceCanceled(TapTarget lastTarget) {

            }
        }).start();
    }

    private void savePrefDataP(){
        sharedPreferences=getActivity().getSharedPreferences("boarding_pref_view_export",MODE_PRIVATE);
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putBoolean("IsFirstTimeVisit_view_export",true);
        editor.apply();
    }

    private boolean restorePrefDataP(){
        sharedPreferences = getActivity().getSharedPreferences("boarding_pref_view_export",MODE_PRIVATE);
        return sharedPreferences.getBoolean("IsFirstTimeVisit_view_export",false);
    }

    @SuppressLint("RestrictedApi")
    @OnClick(R.id.export)
    public void submit() {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.S) {
            importData();
        } else {
            if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {

                if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    Toast.makeText(getActivity(), "Please provide storage permission from app settings", Toast.LENGTH_LONG).show();
                } else {

                    ActivityCompat.requestPermissions(getActivity(),
                            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            1);
                }
            } else {
                importData();

            }
        }
    }

    @SuppressLint("RestrictedApi")
    private void importData() {
        // File filePath = new File(Environment.getExternalStorageDirectory() + "/Demo.xls");
        Workbook wb = new HSSFWorkbook();

        Cell cell = null;

        String ndate="",sub_staff="",sub_date="";

        sub_staff="All";

        Sheet sheet = null;
        sheet = wb.createSheet(sub_staff+" Staff List");
        //Now column and row
        Row row = sheet.createRow(0);

        cell = row.createCell(0);
        cell.setCellValue("S No");

        cell = row.createCell(1);
        cell.setCellValue("Staff Id");

        cell = row.createCell(2);
        cell.setCellValue("Department/Post");

        cell = row.createCell(3);
        cell.setCellValue("Staff Name");

        cell = row.createCell(4);
        cell.setCellValue("Father Name");

        cell = row.createCell(5);
        cell.setCellValue("Phone");


        //column width
        sheet.setColumnWidth(0, (20 * 100));
        sheet.setColumnWidth(1, (20 * 150));
        sheet.setColumnWidth(2, (30 * 250));
        sheet.setColumnWidth(3, (30 * 250));
        sheet.setColumnWidth(4, (30 * 200));
        sheet.setColumnWidth(5, (20 * 100));


        for (int i = 0; i < viewstafflist.size(); i++) {

            Row row1 = sheet.createRow(i + 1);

            cell = row1.createCell(0);
            cell.setCellValue(i+1);

            cell = row1.createCell(1);
            cell.setCellValue(viewstafflist.get(i).getId());

            cell = row1.createCell(2);
            cell.setCellValue(viewstafflist.get(i).getDesignation()+" ("+viewstafflist.get(i).getPost()+")");

            cell = row1.createCell(3);
            cell.setCellValue((viewstafflist.get(i).getName()));
            //  cell.setCellStyle(cellStyle);

            cell = row1.createCell(4);
            cell.setCellValue(viewstafflist.get(i).getFathername());

            cell = row1.createCell(5);
            cell.setCellValue(viewstafflist.get(i).getPhonno());

            sheet.setColumnWidth(0, (20 * 100));
            sheet.setColumnWidth(1, (20 * 150));
            sheet.setColumnWidth(2, (30 * 250));
            sheet.setColumnWidth(3, (30 * 250));
            sheet.setColumnWidth(4, (30 * 200));
            sheet.setColumnWidth(5, (20 * 100));

        }
        String fileName;

        fileName = sub_staff+"_staff_list_"+ System.currentTimeMillis() + ".xls";

        // File file = new File(getActivity().getExternalFilesDir(null), fileName);

        File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS), fileName);

        FileOutputStream fileOutputStream = null;

        try {
            fileOutputStream = new FileOutputStream(file);
            wb.write(fileOutputStream);
            Utils.openSuccessDialog("Success!","Export file in "+file,"",getActivity());
        } catch (IOException e) {
            Utils.openErrorDialog("Error writing Exception: "+ e,getActivity());
        } catch (Exception e) {
            Utils.openErrorDialog("Failed to save file due to Exception: "+ e,getActivity());
        } finally {
            try {
                if (null != fileOutputStream) {
                    fileOutputStream.close();
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

    }

    @SuppressLint("RestrictedApi")
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == 1 && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            importData();
        } else {
            Toast.makeText(getActivity(), "Permission Denied", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        fetchstaffdetail();
    }

    private void fetchstaffdetail() {
        commonProgress.show();
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("d_id", "0");
        params.put("all", "1");
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.STAFF_DETAIL, apiCallbackstaffdetail, getActivity(), params);

    }

    ApiHandler.ApiCallback apiCallbackstaffdetail = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            commonProgress.dismiss();
            //txtupload.setVisibility(View.VISIBLE);
            if (error == null) {
                try {
                    if (viewstafflist != null) {
                        viewstafflist.clear();
                    }
                    JSONArray jsonArray = jsonObject.getJSONArray("staff_detail");
                    viewstafflist = View_staff_bean.parseviewstaffArray(jsonArray);
                    if (viewstafflist.size() > 0) {
                        adapter.setstaffdetailList(viewstafflist);
                        adapter.notifyDataSetChanged();
                        totalRecord.setText("Total Entries:- "+String.valueOf(viewstafflist.size()));
                        export.setVisibility(View.VISIBLE);
                        if (!restorePrefDataP()){
                            setShowcaseViewP();
                        }
                    } else {
                        export.setVisibility(View.GONE);
                        adapter.setstaffdetailList(viewstafflist);
                        adapter.notifyDataSetChanged();
                        totalRecord.setText("Total Entries:- 0");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                export.setVisibility(View.GONE);
                txtNorecord.setVisibility(View.VISIBLE);
                viewstafflist.clear();
                adapter.setstaffdetailList(viewstafflist);
                totalRecord.setText("Total Entries:- 0");
                adapter.notifyDataSetChanged();
            }
        }
    };


    @Override
    public void staffCallBack(View_staff_bean viewStaffBean) {
        String phoneNo = viewStaffBean.getPhonno();
        Log.i("email",phoneNo);
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:"+ phoneNo));
        startActivity(intent);
    }

    @Override
    public void onUpdateCallback(View_staff_bean viewStaffBean) {
        commonProgress.show();
        HashMap<String, String> params = new HashMap<>();
        params.put("stu_id", viewStaffBean.getId());
        params.put("check", "staff");
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.BLOCKSTUDENTURL, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                commonProgress.dismiss();
                if (error == null) {
                    Toast.makeText(getActivity(),"Staff Blocked!",Toast.LENGTH_LONG).show();
                    //  Utils.showSnackBar(jsonObject.getString("msg"), parent);
                    getActivity().finish();

                } else {
                    Log.e("error", error.getMessage() + "");
                    Utils.showSnackBar(error.getMessage(), parent);
                    // Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_LONG).show();
                    if (error.getStatusCode() == 405) {
                        User.logout();
                        startActivity(new Intent(getActivity(), LoginActivity.class));
                        getActivity().finish();
                    }
                }
            }
        }, getActivity(), params);
    }

    @Override
    public void staffemailCallBack(View_staff_bean viewStaffBean) {
        String email = viewStaffBean.getEmail();
        Log.i("email",email);
        Intent intent = new Intent(Intent.ACTION_SENDTO); // it's not ACTION_SEND
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_SUBJECT, "Subject of email");
        intent.putExtra(Intent.EXTRA_TEXT, "Body of email");
        intent.setData(Uri.parse("mailto:"+email)); // or just "mailto:" for blank
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK); // this will make such that when user returns to your app, your app is displayed, instead of the email app.
        startActivity(intent);
    }

    @Override
    public void staffProfile(View_staff_bean viewStaffBean) {
        Gson gson  = new Gson();
        String staffdet = gson.toJson(viewStaffBean,View_staff_bean.class);
        Intent i = new Intent(getActivity(), StaffProfile.class);
        i.putExtra("staffdet",staffdet);
        startActivity(i);
    }
    @Override
    public void onUpdateProfile(View_staff_bean viewStaffBean) {
        Gson gson  = new Gson();
        String staffdet = gson.toJson(viewStaffBean,View_staff_bean.class);
        Intent i = new Intent(getActivity(), UpdateStaff.class);
        i.putExtra("staffdet",staffdet);
        startActivity(i);
    }


    @Override
    public void onDeleteCallback(View_staff_bean viewStaffBean) {
        commonProgress.show();
        HashMap<String, String> params = new HashMap<>();
        params.put("h_id", viewStaffBean.getId());
        params.put("check", "staff");
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.DELETEHW, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                ErpProgress.cancelProgressBar();
                commonProgress.dismiss();
                if (error == null) {

                    Utils.showSnackBar("Staff Deleted!", parent);
                    // getActivity().finish();
                    fetchstaffdetail();
                } else {
                    Log.e("error", error.getMessage() + "");
                    Utils.showSnackBar(error.getMessage(), parent);
                    if (error.getStatusCode() == 405) {
                        User.logout();
                        startActivity(new Intent(getActivity(), LoginActivity.class));
                        getActivity().finish();
                    }
                }
            }
        }, getActivity(), params);
    }

}