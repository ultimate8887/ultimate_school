package com.ultimate.ultimatesmartschool.Staff;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.ultimate.ultimatesmartschool.BeanModule.User;
import com.ultimate.ultimatesmartschool.Login.LoginActivity;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Add_post_frag extends Fragment implements Adapter_post_name.AddNewPost {
    @BindView(R.id.spinnerdepart)
    Spinner spinnerClass;
    @BindView(R.id.recyclerViewdeprt)
    RecyclerView recyclerView;
    @BindView(R.id.parents)
    RelativeLayout parent;
    private Adapterspiner_departmnt spinneradapt;
    private Adapter_post_name adapter;
    ArrayList<Postname_Bean> postlist=new ArrayList<>();
    ArrayList<Deparment_bean>department_list=new ArrayList<>();
    private String departmentid = "";
    private Dialog postdialog;
    EditText edtTitle;
    String name;
    String names;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_post, container, false);
        ButterKnife.bind(this, view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        adapter = new Adapter_post_name(getActivity(), postlist, this);
        recyclerView.setAdapter(adapter);
        return view;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if(getView() != null && isVisibleToUser){
            Log.i("1st","1st");
            fetchdepartlist();
        }
    }

    private void fetchdepartlist() {
        HashMap<String,String> params=new HashMap<>();
        ApiHandler.apiHit(Request.Method.GET, Constants.getBaseURL() + Constants.deparmrnt, apiCallback, getActivity(), params);
    }
    ApiHandler.ApiCallback apiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            // txtupload.setVisibility(View.VISIBLE);
            if (error == null) {
                try {
                    if (department_list != null) {
                        department_list.clear();
                    }
                    JSONArray jsonArray = jsonObject.getJSONArray("department_data");
                    department_list = Deparment_bean.parseDepartmentArray(jsonArray);
                    spinneradapt= new Adapterspiner_departmnt(getActivity(), department_list);
                    spinnerClass.setAdapter(spinneradapt);
                    spinnerClass.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if (i > 0) {
                                departmentid = department_list.get(i-1 ).getId();
                                //fetchSubject(classid);
                                fetchPostname(departmentid);
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });
                    //Log.d("TAG", "onDataFetched: " + examList.size());
                    //adapter.setHQList(obj);
//                    adapter.setdeparmentList(depart_list);
//                    adapter.notifyDataSetChanged();
//                    updateInst();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Utils.showSnackBar(error.getMessage(), parent);
                if (error.getStatusCode() == 405) {
                    User.logout();
                    startActivity(new Intent(getActivity(), LoginActivity.class));
                    getActivity().finish();
                }
            }
        }
    };

    private void fetchPostname(String departmentid) {
        HashMap<String,String> params=new HashMap<>();
        ApiHandler.apiHit(Request.Method.GET, Constants.getBaseURL() + Constants.Post+Constants.dpartment_id+departmentid, apiCallbackpost, getActivity(), params);
    }
    ApiHandler.ApiCallback apiCallbackpost = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            if (error == null) {
                try {
                    if (postlist != null) {
                        postlist.clear();
                    }
                    JSONArray jsonArray = jsonObject.getJSONArray("post_data");
                    postlist = Postname_Bean.parsePostnsmeArray(jsonArray);
                    //Log.d("TAG", "onDataFetched: " + examList.size());
                    //adapter.setHQList(obj);
                    adapter.setdpostList(postlist);
                    adapter.notifyDataSetChanged();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Utils.showSnackBar(error.getMessage(), parent);
            }
        }
    };
    @Override
    public void addEditNewPost(final Postname_Bean data, final int i) {

        if(checkValid()) {
            postdialog = new Dialog(getActivity());
            postdialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            postdialog.setCancelable(true);
            postdialog.setContentView(R.layout.add_post_dialog);
            edtTitle = (EditText) postdialog.findViewById(R.id.edtTitlepost_name);
            if (i != -1) {
                edtTitle.setText(postlist.get(i).getName());
            }
            Button btnSubmit = (Button) postdialog.findViewById(R.id.btnSubmitpost);
            btnSubmit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (edtTitle.getText().toString().length() > 0) {
                        if (i == -1) {
                            addNewpost(edtTitle.getText().toString());
                        } else {
                            editpost(data, i, edtTitle.getText().toString());
                        }
                        adapter.notifyDataSetChanged();
                        postdialog.dismiss();
                    }else {
                        Toast.makeText(getActivity(),"Enter Post Name",Toast.LENGTH_SHORT).show();

                    }

                }
            });
            postdialog.show();
        }

    }
    private void editpost(final Postname_Bean data, final int i, String s) {
        HashMap<String, String> params = new HashMap<>();
        name = edtTitle.getText().toString();
        ApiHandler.apiHit(Request.Method.PUT, Constants.getBaseURL() + Constants.Post + Constants.dpartment_id + departmentid + Constants.Postname_name + name+Constants.Postname_id+data.getId(), apiCallbackpost, getActivity(), params);
    }


    private void addNewpost(String s) {
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("d_id",departmentid);
        params.put("p_name", s);
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.Post, apiCallbackpostnn, getActivity(), params);

    }
    ApiHandler.ApiCallback apiCallbackpostnn = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            if (error == null) {
                try {
                    if (postlist != null) {
                        postlist.clear();
                    }
                    JSONArray jsonArray = jsonObject.getJSONArray("post_data");
                    postlist = Postname_Bean.parsePostnsmeArray(jsonArray);
                    adapter.setdpostList(postlist);
                    adapter.notifyDataSetChanged();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Utils.showSnackBar(error.getMessage(), parent);
            }
        }
    };
    @Override
    public void deletePost(final Postname_Bean data, int position) {
        DialogInterface.OnClickListener dialogclicllistner = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        HashMap<String, String> params = new HashMap<String, String>();
                        ApiHandler.apiHit(Request.Method.DELETE, Constants.getBaseURL() + Constants.Post +Constants.dpartment_id+departmentid+ Constants.Postname_id + data.getId(), apiCallbackpost, getActivity(), params);
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        //No button clicked
                        break;
                }
            }
        };
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("Are you sure you want to delete?").setPositiveButton("Yes", dialogclicllistner)
                .setNegativeButton("No", dialogclicllistner).show();
    }



    private boolean checkValid() {
        boolean valid = true;
        String errorMsg = null;
        if (departmentid.isEmpty() || departmentid == "") {
            valid = false;
            errorMsg = "Please select department";
        }
        if (!valid) {
            Utils.showSnackBar(errorMsg, parent);
        }
        return valid;
    }
}
