package com.ultimate.ultimatesmartschool.Staff;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.Html;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.daimajia.swipe.SimpleSwipeListener;
import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputLayout;
import com.squareup.picasso.Picasso;
import com.ultimate.ultimatesmartschool.BeanModule.Studentbean;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

import static java.util.Collections.addAll;
import static org.apache.commons.beanutils.WrapDynaClass.clear;

public class VAdapter_view_staff extends RecyclerSwipeAdapter<VAdapter_view_staff.Viewholder> implements Filterable {

    public ArrayList<View_staff_bean> viewstafflist;
    private ArrayList<View_staff_bean> filterModelClass;
    Context context;
    private StaffCallBack callBack;
    private ValueFilter valueFilter;

    public VAdapter_view_staff(ArrayList<View_staff_bean> viewstafflist, Context context, StaffCallBack callBack) {
        this.viewstafflist = viewstafflist;
        this.context = context;
        this.callBack = callBack;
        this.filterModelClass = viewstafflist;
    }

    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.new_viewstaffada_item, parent, false);
        VAdapter_view_staff.Viewholder viewholder = new VAdapter_view_staff.Viewholder(view);
        return viewholder;
    }

    @Override
    public void onBindViewHolder(@NonNull Viewholder holder, @SuppressLint("RecyclerView") final int position) {

//        holder.image.setAnimation(AnimationUtils.loadAnimation(context, R.anim.fade_transition_animation));
//        holder.parent.setAnimation(AnimationUtils.loadAnimation(context, R.anim.fade_scale_animation));
        View_staff_bean mData = viewstafflist.get(position);

        // holder.employee_id.setText("Emp.ID: " + " " + viewstafflist.get(position).getId());

        if (mData.getId() != null) {
            String title = getColoredSpanned("Emp.ID: ", "#000000");
            String Name = getColoredSpanned(mData.getId(), "#5A5C59");
            holder.employee_id.setText(Html.fromHtml(title + " " + Name));
        }

        // holder.employee_name.setText("Name: " + " " + viewstafflist.get(position).getName());
        if (mData.getName() != null) {
            String title = getColoredSpanned("Name: ", "#000000");
            String Name = getColoredSpanned(mData.getName(), "#5A5C59");
            holder.employee_name.setText(Html.fromHtml(title + " " + Name));
        } else {
            holder.employee_name.setText("Name Not Mentioned");
        }

        //  holder.employee_designation.setText("Designation: " + " " + viewstafflist.get(position).getDesignation());
        if (mData.getDesignation() != null) {
            String title = getColoredSpanned("", "#000000");
            String Name = getColoredSpanned(mData.getDesignation(), "#5A5C59");
            holder.employee_designation.setText(Html.fromHtml(title + " " + Name));
        } else {
            holder.employee_designation.setText("Designation Not Mentioned");
        }

        //  holder.post.setText("Post:" + " " + viewstafflist.get(position).getPost());
        if (mData.getPost() != null) {
            String title = getColoredSpanned("Post: ", "#000000");
            String Name = getColoredSpanned(mData.getPost(), "#5A5C59");
            holder.post.setText(Html.fromHtml(title + " " + Name));
        } else {
            holder.post.setText("Post Not Mentioned");
        }

        //  holder.employee_dob.setText("Date of Birth: " + " " + viewstafflist.get(position).getDateofbirth());
        if (mData.getDateofbirth() != null) {
            String title = getColoredSpanned("Date of Birth: ", "#000000");
            String Name = getColoredSpanned(Utils.getDateFormated(mData.getDateofbirth()), "#5A5C59");
            holder.employee_dob.setText(Html.fromHtml(title + " " + Name));
        } else {
            holder.employee_dob.setText("Date of Birth Not Mentioned");
        }

        // holder.employee_doj.setText("Date of Joining: " + " " + viewstafflist.get(position).getDateofjoining());
        if (mData.getDateofjoining() != null) {
            String title = getColoredSpanned("Date of Joining: ", "#000000");
            String Name = getColoredSpanned(Utils.getDateFormated(mData.getDateofjoining()), "#5A5C59");
            holder.employee_doj.setText(Html.fromHtml(title + " " + Name));
        } else {
            holder.employee_doj.setText("Date of Joining Not Mentioned");
        }

        //  holder.employee_salary.setText("Salary: " + " " + viewstafflist.get(position).getSalary());


        //    holder.phone.setText("Mobile: " + " " + viewstafflist.get(position).getPhonno());


//        if (mData.getPhonno() != null) {
//            String title = getColoredSpanned("Mobile: ", "#000000");
//            String Name = getColoredSpanned("</u>" +mData.getPhonno()+"</u>", "#5A5C59");
//            holder.phone.setText(Html.fromHtml(title + " " + Name));
//        }else {
//            holder.phone.setText("Mobile: Not Mentioned");
//        }

//        if (viewstafflist.get(position).getFatherphonno() != null) {
//            holder.fphone.setText("Father's Mobile: " + " " + viewstafflist.get(position).getFatherphonno());
//        } else {
//            holder.fphone.setText("Father's Mobile: " + "--- ");
//        }
//        if (viewstafflist.get(position).getOtherphonno() != null) {
//            holder.ophone.setText("Other's Mobile: " + " " + viewstafflist.get(position).getOtherphonno());
//        } else {
//            holder.ophone.setText("Other's Mobile: " + "---");
//        }


        //  holder.email.setText("Email: " + " " + viewstafflist.get(position).getEmail());


        if (mData.getEmail() != null) {

            String title = getColoredSpanned("Email: ", "#000000");
            String Name = getColoredSpanned("</u>" + mData.getEmail() + "</u>", "#5A5C59");
            holder.email.setText(Html.fromHtml(title + " " + Name));
        } else {
            holder.email.setText("Email: Not Mentioned");
        }

//        if (mData.getMobile_name() != null) {
//            holder.login_device.setText(mData.getMobile_brand()+"-"+mData.getMobile_name());
//        }else {
//            holder.login_device.setText("Not Detect");
//        }

        if (mData.getAge() != null) {
//            holder.login_device.setText(mData.getAge()+" years old");

            if (mData.getAge() != null) {
                String tit = getColoredSpanned("Age: ", "#000000");
                String title = getColoredSpanned("  " + mData.getAge(), "#F4212C");
                String Name = getColoredSpanned(" years old", "#5A5C59");
                holder.phone.setText(Html.fromHtml(tit + " " + title + " " + Name));
            } else {
                holder.phone.setText("Age: Not Found");
            }

            holder.inpSchoolId.setHint("Birthday Card Seen Status");
            if (mData.getDate_time() != null) {
                //holder.login_device.setText("Birthday Card Seen at " + Utils.getTimeHr(mData.getDate_time()));
                String title = getColoredSpanned("Seen at ", "#000000");
                String Name = getColoredSpanned("</u>" + Utils.getTimeHr(mData.getDate_time()) + "</u>", "#1C8B3B");
                holder.login_device.setText(Html.fromHtml(title + " " + Name));

            } else {

                String title = getColoredSpanned("Not Seen yet", "#F4212C");
                String Name = getColoredSpanned("", "#5A5C59");
                holder.login_device.setText(Html.fromHtml(title + " " + Name));

                // holder.login_device.setText("Not Seen yet");
            }
            holder.email.setVisibility(View.GONE);
//            holder.phone.setVisibility(View.GONE);
            holder.view5.setVisibility(View.GONE);
        } else {
            holder.inpSchoolId.setHint("Login Device");
            holder.email.setVisibility(View.VISIBLE);

            if (mData.getPhonno() != null) {
                String title = getColoredSpanned("Mobile: ", "#000000");
                String Name = getColoredSpanned("</u>" + mData.getPhonno() + "</u>", "#5A5C59");
                holder.phone.setText(Html.fromHtml(title + " " + Name));
            } else {
                holder.phone.setText("Mobile: Not Mentioned");
            }


            holder.phone.setVisibility(View.VISIBLE);
            holder.view5.setVisibility(View.VISIBLE);
            if (mData.getMobile_name() != null) {
                holder.login_device.setText(mData.getMobile_brand() + "-" + mData.getMobile_name());
            } else {
                holder.login_device.setText("Not Detect");
            }
        }


        if (mData.getGender().equalsIgnoreCase("male")) {
            if (viewstafflist.get(position).getImage() != null) {
                //  Picasso.with(context).load(viewstafflist.get(position).getImage()).placeholder(context.getResources().getDrawable(R.drawable.man)).into(holder.image);
                Utils.progressImg(viewstafflist.get(position).getImage(), holder.image, context);
            } else {
                // Picasso.with(context).load(String.valueOf(context.getResources().getDrawable(R.drawable.staffimg))).into(holder.image);
                Picasso.get().load(R.drawable.boy).into(holder.image);

            }
        } else {
            if (viewstafflist.get(position).getImage() != null) {
                //  Picasso.with(context).load(viewstafflist.get(position).getImage()).placeholder(context.getResources().getDrawable(R.drawable.man)).into(holder.image);
                Utils.progressImg(viewstafflist.get(position).getImage(), holder.image, context);
            } else {
                // Picasso.with(context).load(String.valueOf(context.getResources().getDrawable(R.drawable.staffimg))).into(holder.image);
                Picasso.get().load(R.drawable.female_teacher).into(holder.image);

            }
        }

        holder.phoneLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mData.getPhonno() != null) {
                    callBack.staffCallBack(viewstafflist.get(position));
                } else {
                    Toast.makeText(context, "Phone Number Not Found!", Toast.LENGTH_SHORT).show();
                }
            }
        });

        holder.swipeLayout.setShowMode(SwipeLayout.ShowMode.PullOut);
        holder.swipeLayout.addSwipeListener(new SimpleSwipeListener() {
            @Override
            public void onOpen(SwipeLayout layout) {

            }

            @Override
            public void onStartClose(SwipeLayout layout) {
                holder.swipeLayout.close();
            }
        });

        holder.swipeLayout.setOnDoubleClickListener(new SwipeLayout.DoubleClickListener() {
            @Override
            public void onDoubleClick(SwipeLayout layout, boolean surface) {
                // Toast.makeText(context, "DoubleClick", Toast.LENGTH_SHORT).show();
            }
        });


        mItemManger.bindView(holder.itemView, position);
        holder.update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                //   Toast.makeText(view.getContext(), "clicked " + holder.employee_name.getText().toString() + "!", Toast.LENGTH_SHORT).show();

                AlertDialog.Builder builder2 = new AlertDialog.Builder(context);
                builder2.setMessage("Do you want to Block? ");
                builder2.setCancelable(false);
                builder2.setPositiveButton(
                        "Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                if (callBack != null) {
                                    callBack.onUpdateCallback(viewstafflist.get(position));
                                }
                                viewstafflist.remove(position);
                                notifyItemRemoved(position);
                                notifyItemRangeChanged(position, viewstafflist.size());
                                mItemManger.closeAllItems();
                                Toast.makeText(view.getContext(), "Blocked " + holder.employee_name.getText().toString() + "!", Toast.LENGTH_SHORT).show();
                            }
                        });
                builder2.setNegativeButton(
                        "No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        });
                AlertDialog alert11 = builder2.create();
                alert11.show();

            }
        });

        holder.trash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                if (callBack != null) {
                    callBack.onUpdateProfile(viewstafflist.get(position));
                }
            }
        });

        holder.googleLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (mData.getEmail() != null) {
                    callBack.staffemailCallBack(viewstafflist.get(position));
                } else {
                    Toast.makeText(context, "Email ID Not Found!", Toast.LENGTH_SHORT).show();
                }

            }
        });

        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                //  Toast.makeText(view.getContext(), "clicked " + holder.employee_name.getText().toString() + "!", Toast.LENGTH_SHORT).show();

                AlertDialog.Builder builder2 = new AlertDialog.Builder(context);
                builder2.setMessage("Do you want to delete this staff permanently? ");
                builder2.setCancelable(false);
                builder2.setPositiveButton(
                        "Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                if (callBack != null) {
                                    callBack.onDeleteCallback(viewstafflist.get(position));

                                }
                                viewstafflist.remove(position);
                                notifyItemRemoved(position);
                                notifyItemRangeChanged(position, viewstafflist.size());
                                mItemManger.closeAllItems();
                                //      Toast.makeText(view.getContext(), "Deleted " + holder.employee_name.getText().toString() + "!", Toast.LENGTH_SHORT).show();
                            }
                        });
                builder2.setNegativeButton(
                        "No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        });
                AlertDialog alert11 = builder2.create();
                alert11.show();

            }
        });


        holder.parent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                callBack.staffProfile(viewstafflist.get(position));

            }
        });

        filterModelClass = viewstafflist;
    }

    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }


    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe;
    }


    @Override
    public int getItemCount() {
        return viewstafflist.size();
    }

    public void setstaffdetailList(ArrayList<View_staff_bean> viewstafflist) {
        this.viewstafflist = viewstafflist;
    }

    public class Viewholder extends RecyclerView.ViewHolder {
        @BindView(R.id.swipe)
        SwipeLayout swipeLayout;
        @BindView(R.id.id)
        TextView employee_id;
        @BindView(R.id.txtstuName)
        EditText employee_name;
        @BindView(R.id.txtDT)
        TextView employee_designation;
        @BindView(R.id.txtdob)
        TextView employee_dob;
        @BindView(R.id.txtdoj)
        TextView employee_doj;
        @BindView(R.id.txtReason)
        EditText login_device;
        @BindView(R.id.txtFatherName)
        TextView post;
        @BindView(R.id.profile)
        CircularImageView image;
        @BindView(R.id.txtPhone)
        TextView phone;
        @BindView(R.id.googleLogin)
        FloatingActionButton googleLogin;
        @BindView(R.id.phoneLogin)
        FloatingActionButton phoneLogin;
        @BindView(R.id.delete)
        ImageView delete;
        @BindView(R.id.update)
        ImageView update;
        @BindView(R.id.trash)
        ImageView trash;
        @BindView(R.id.email)
        TextView email;
        @BindView(R.id.parent)
        RelativeLayout parent;

        @BindView(R.id.inpUsername)
        TextInputLayout inpSchoolId;
        @BindView(R.id.view5)
        View view5;

        public Viewholder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface StaffCallBack {
        void staffCallBack(View_staff_bean viewStaffBean);
        void onUpdateCallback(View_staff_bean viewStaffBean);

        void onUpdateProfile(View_staff_bean viewStaffBean);
        void onDeleteCallback(View_staff_bean viewStaffBean);
        void staffemailCallBack(View_staff_bean viewStaffBean);

        void staffProfile(View_staff_bean viewStaffBean);
    }

    @Override
    public Filter getFilter() {
        if (valueFilter == null) {
            valueFilter = new ValueFilter();
        }
        return valueFilter;
    }

    private class ValueFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults filterResults = new FilterResults();
            if (constraint != null && constraint.length() > 0) {
                ArrayList<View_staff_bean> lstFiltered = new ArrayList<>();
                for (int i = 0; i < filterModelClass.size(); i++) {
                    if ((filterModelClass.get(i).getName().toUpperCase()).contains(constraint.toString().toUpperCase()) || (filterModelClass.get(i).getId().toUpperCase()).contains(constraint.toString().toUpperCase())) {
                        lstFiltered.add(filterModelClass.get(i));

                    }
                }

                filterResults.count = lstFiltered.size();
                filterResults.values = lstFiltered;

                Log.e("valuesssss", String.valueOf(lstFiltered));
            } else {
                filterModelClass.addAll(viewstafflist);
                filterResults.count = viewstafflist.size();
                filterResults.values = viewstafflist;

            }

            return filterResults;

        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {


            viewstafflist = (ArrayList<View_staff_bean>) results.values;
            clear();
            addAll(viewstafflist);
            Log.e("values", String.valueOf(viewstafflist));
            notifyDataSetChanged();

        }

//            @Override
//            protected void publishResults(CharSequence constraint, FilterResults results) {
//                ArrayList<View_staff_bean> tempList = (ArrayList<View_staff_bean>) results.values;
//                clear();
//                addAll(tempList);
//            }

//            @Override
//            protected FilterResults performFiltering(CharSequence constraint) {
//                FilterResults result = new FilterResults();
//
//                if (constraint == null || constraint.length() == 0) {
//                    result.values = viewstafflist;
//                    result.count = viewstafflist.size();
//                } else {
//                    final ArrayList<View_staff_bean> nlist = new ArrayList<View_staff_bean>();
//                    String lowerConstraint = constraint.toString().toLowerCase();
//                    for (int i = 0; i < filterModelClass.size(); i++) {
//                        if ((filterModelClass.get(i).getName().toUpperCase()).contains(constraint.toString().toUpperCase())||(filterModelClass.get(i).getId().toUpperCase()).contains(constraint.toString().toUpperCase())) {
//                            nlist.add(filterModelClass.get(i));
//                        }
//                    }
//                    // loop through originalItems which always contains all items
////                    for(View_staff_bean nightOut : viewstafflist) {
////                        final String value = nightOut.getName().toLowerCase();
////                        if (value.contains(lowerConstraint)) {
////                            nlist.add(nightOut);
////                        }
////                    }
//
//                    result.values = nlist;
//                    result.count = nlist.size();
//                }
//
//                return result;
//            }
    }

    ;


}