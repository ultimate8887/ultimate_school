package com.ultimate.ultimatesmartschool.Staff;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.ultimate.ultimatesmartschool.Homework.Fragments.StaffHW_DateWise;
import com.ultimate.ultimatesmartschool.Homework.Fragments.StaffHW_MonthWise;
import com.ultimate.ultimatesmartschool.Staff.Fragments.AllStaff;
import com.ultimate.ultimatesmartschool.Staff.Fragments.DepartmentStaff;

public class StaffTabPager extends FragmentStatePagerAdapter {

    public StaffTabPager(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new AllStaff();
            case 1:
                return new DepartmentStaff();
            default:
                break;
        }
        return null;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        // Generate title based on item position
        switch (position) {
            case 0:
                return "All Staff list";
            case 1:
                return "Department Wise list";
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return 2;
    }
}
