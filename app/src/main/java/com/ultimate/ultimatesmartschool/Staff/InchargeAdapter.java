package com.ultimate.ultimatesmartschool.Staff;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.daimajia.swipe.SimpleSwipeListener;
import com.daimajia.swipe.SwipeLayout;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.squareup.picasso.Picasso;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;

public class InchargeAdapter extends RecyclerView.Adapter<InchargeAdapter.Viewholder> implements Filterable {

    Context context;
    ArrayList<InchargeBean> securitylist;
    ArrayList<InchargeBean> filterModelClassa;
    // private ValueFilter valueFilter;
    IncMethodCallBack methodCallBack;

    public InchargeAdapter(ArrayList<InchargeBean> securitylist, Context context, IncMethodCallBack methodCallBack) {
        this.context = context;
        this.securitylist = securitylist;
        this.filterModelClassa = securitylist;
        this.methodCallBack = methodCallBack;
    }

    public interface IncMethodCallBack {
        void delete(InchargeBean inchargeBean);
    }


    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.common_layout, parent, false);
        Viewholder viewholder = new Viewholder(view);
        return viewholder;
    }

    @Override
    public void onBindViewHolder(@NonNull Viewholder holder, @SuppressLint("RecyclerView") final int position) {

        final InchargeBean mData = securitylist.get(position);

        holder.txtstuName.setEnabled(false);
        holder.txtReason.setEnabled(false);
        holder.txtClass.setVisibility(View.VISIBLE);

        String mclass = "";

        if (mData.getId() != null) {
            String title = getColoredSpanned("ID: ", "#000000");
            String Name = getColoredSpanned(mData.getId(), "#5A5C59");
            holder.id.setText(Html.fromHtml(title + " " + Name));
        }

//        if (mData.getGate_class() != null)
//            mclass = mData.getGate_class();
//

        if (mData.getProfile() != null) {
            Picasso.get().load(mData.getProfile()).placeholder(R.color.white).into(holder.profile);
        } else {
            holder.profile.setVisibility(View.GONE);
        }


        if (mData.getStaff_desg_name() != null) {
            String title = getColoredSpanned("Designation: ", "#5A5C59");
            String Name = getColoredSpanned(mData.getStaff_desg_name(), "#7D7D7D");
            holder.txtcheckOut.setText(Html.fromHtml(title + " " + Name));
        }

        if (mData.getGender() != null) {
            String title = getColoredSpanned("Gender: ", "#5A5C59");
            String Name = getColoredSpanned(mData.getGender(), "#7D7D7D");
            holder.txtFatherName.setText(Html.fromHtml(title + " " + Name));
        }


        if (mData.getFather_name() != null) {
            String title = getColoredSpanned("Father Name: ", "#5A5C59");
            String Name = getColoredSpanned(mData.getFather_name(), "#7D7D7D");
            holder.txtPhone.setText(Html.fromHtml(title + " " + Name));
        }


        //for Today
        java.util.Calendar calendar = java.util.Calendar.getInstance(TimeZone.getDefault());
        Date setdate = calendar.getTime();
        SimpleDateFormat fmtOut = new SimpleDateFormat("dd MMM, yyyy");
        String dateString = fmtOut.format(setdate);
        //for Yesterday
        DateFormat dateFormat = new SimpleDateFormat("dd MMM, yyyy");
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);
        String dateString1= dateFormat.format(cal.getTime());

        String check= Utils.getDateFormated(mData.getCreated_on());
        if (check.equalsIgnoreCase(dateString)){
            //  holder.lytLine.setVisibility(View.VISIBLE);
            // holder.txtDT.setText("Today");
            String title = getColoredSpanned("Today", "#e31e25");
            //String Name = getColoredSpanned(messagelist.get(position).getFrom_type(), "#7D7D7D");
            String l_Name = getColoredSpanned("", "#5A5C59");
            holder.txtDT.setText(Html.fromHtml(title + " " + l_Name));
            // holder.txtDT.setText("Today at "+time);
            //  holder.vieww.setBackground(mContext.getResources().getDrawable(R.drawable.a_card_lyt2));
        }else if (check.equalsIgnoreCase(dateString1)){

            String title = getColoredSpanned("Yesterday", "#1C8B3B");
            //String Name = getColoredSpanned(messagelist.get(position).getFrom_type(), "#7D7D7D");
            String l_Name = getColoredSpanned("", "#5A5C59");
            holder.txtDT.setText(Html.fromHtml(title + " " + l_Name));

            //  holder.lytLine.setVisibility(View.GONE);
            //  holder.vieww.setBackground(mContext.getResources().getDrawable(R.drawable.card_lyt2));
        }else {
            holder.txtDT.setText(Utils.getDateTimeFormatedWithAMPM(mData.getCreated_on()));
        }


//        if (mData.getCreated_on().equalsIgnoreCase("Morning")){
        //  holder.txtDT.setText(Utils.getDateTimeFormatedWithAMPM(mData.getCreated_on()));
//        }else {
//            holder.txtDT.setText(Utility.getDateTimeFormated(mData.getSec_time_in())+"PM");
//        }


        if (mData.getSection_name() != null) {
            // holder.txtReason.setText(mData.getC_name() + " Class" + " (" + mData.getSection_name() + ")");
            String title = getColoredSpanned(mData.getC_name() + " Class", "#F4212C");
            String Name = getColoredSpanned(" (" + mData.getSection_name() + ")", "#F9A602");
            holder.txtReason.setText(Html.fromHtml(title + " " + Name));
        } else {
            //  holder.txtReason.setText(mData.getC_name() + " Class" + " (" + mData.getEs_groupname() + ")");
            String title = getColoredSpanned(mData.getC_name() + " Class", "#F4212C");
            String Name = getColoredSpanned("", "#F9A602");
            holder.txtReason.setText(Html.fromHtml(title + " " + Name));
        }

        if (mData.getEs_groupname() != null) {
            String title = getColoredSpanned("Batch: ", "#5A5C59");
            String Name = "", Name1 = "";
            // Name = getColoredSpanned(mData.getC_name() + " Class", "#000000");
            Name1 = getColoredSpanned("(" + mData.getEs_groupname() + ")", "#000000");
            holder.txtClass.setText(Html.fromHtml(title + " " + Name + " " + Name1));
        }else{
            holder.txtClass.setText("Batch: Not Mentioned.");
        }

        if (mData.getS_name() != null) {
            String title = getColoredSpanned("Name: ", "#5A5C59");
            String Name = getColoredSpanned(mData.getS_name(), "#7D7D7D");
            holder.txtstuName.setText(Html.fromHtml(title + " " + Name));
        }
        holder.swipeLayout.setShowMode(SwipeLayout.ShowMode.PullOut);
        holder.swipeLayout.addSwipeListener(new SimpleSwipeListener() {
            @Override
            public void onOpen(SwipeLayout layout) {

            }

            @Override
            public void onStartClose(SwipeLayout layout) {
                holder.swipeLayout.close();
            }
        });

        holder.trash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
                builder1.setMessage("Do you want to delete? ");
                builder1.setCancelable(false);
                builder1.setPositiveButton(
                        "Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                                if (methodCallBack != null) {
                                    methodCallBack.delete(securitylist.get(position));
//                                    Toast.makeText(listner, "deleted", Toast.LENGTH_SHORT).show();
                                }
                                // mAdapterCallback.deleteNotification(notificationList.get(postion));
                            }
                        });
                builder1.setNegativeButton(
                        "No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        });
                AlertDialog alert11 = builder1.create();
                alert11.show();
            }
        });


    }

    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }


    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {

                String Key = constraint.toString();
                if (Key.isEmpty()) {

                    filterModelClassa = securitylist;

                } else {
                    ArrayList<InchargeBean> lstFiltered = new ArrayList<>();
                    for (InchargeBean row : securitylist) {

                        if (row.getS_name().toLowerCase().contains(Key.toLowerCase())) {
                            lstFiltered.add(row);
                        }

                    }

                    filterModelClassa = lstFiltered;

                }


                FilterResults filterResults = new FilterResults();
                filterResults.values = filterModelClassa;
                return filterResults;

            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {


                securitylist = (ArrayList<InchargeBean>) results.values;
                notifyDataSetChanged();

            }
        };
    }


    @Override
    public int getItemCount() {
        //return securitylist.size();
        if (securitylist != null)
            return securitylist.size();
        return 0;
    }

    public void setsecuList(ArrayList<InchargeBean> securitylist) {
        this.securitylist = securitylist;
    }

    public interface Mycallback {
        public void onApproveCallback(InchargeBean inchargeBean);

    }

    public class Viewholder extends RecyclerView.ViewHolder {

        @BindView(R.id.txtFatherName)
        TextView txtFatherName;
        @BindView(R.id.txtPhone)
        TextView txtPhone;
        @BindView(R.id.txtDT)
        TextView txtDT;
        @BindView(R.id.id)
        TextView id;

        @BindView(R.id.txtcheckOut)
        TextView txtcheckOut;
        @BindView(R.id.txtClass)
        TextView txtClass;
        @BindView(R.id.txtReason)
        EditText txtReason;
        @BindView(R.id.txtstuName)
        EditText txtstuName;

        @BindView(R.id.profile)
        CircularImageView profile;
        SwipeLayout swipeLayout;
        public ImageView trash, update;

        public Viewholder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            swipeLayout = (SwipeLayout) itemView.findViewById(R.id.swipe);
            trash = (ImageView) itemView.findViewById(R.id.trash);
            update = (ImageView) itemView.findViewById(R.id.update);
        }
    }


}
