package com.ultimate.ultimatesmartschool.Staff;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetSequence;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.ultimate.ultimatesmartschool.BeanModule.ClassBean;
import com.ultimate.ultimatesmartschool.BeanModule.CommonBean;
import com.ultimate.ultimatesmartschool.BeanModule.SectionBean;
import com.ultimate.ultimatesmartschool.BeanModule.SubjectBeancls;
import com.ultimate.ultimatesmartschool.BeanModule.User;
import com.ultimate.ultimatesmartschool.Examination.GroupSpinnerAdapter;
import com.ultimate.ultimatesmartschool.Homework.AdapterOfSubjectList;
import com.ultimate.ultimatesmartschool.Homework.SectionnewAdapter;
import com.ultimate.ultimatesmartschool.Login.LoginActivity;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.StudentAttendance.ClassAdapterAttend;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.CommonProgress;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;
import com.ultimate.ultimatesmartschool.Utility.ErpProgressVert;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AssignSubject extends AppCompatActivity implements SubjectAssignAdapter.SubMethodCallBack {


    RecyclerView NewsRecyclerview;
    //NewsAdapter newsAdapter;
    //List<NewsItem> mData;
    SharedPreferences sharedPreferences;
    boolean isDark = false;
    RelativeLayout rootLayout;
    EditText searchInput ;
    CharSequence search="";
    ArrayList<AssignSubjectBean> mData = new ArrayList<>();
    @BindView(R.id.textNorecord)
    TextView txtNorecord;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    Spinner spinnersection;
    String sectionid = "";
    ArrayList<SectionBean> sectionList = new ArrayList<>();
    SectionnewAdapter sectionnewAdapter;
    @BindView(R.id.totalRecord)
    TextView totalRecord;
    @BindView(R.id.contact_support)
    ImageView contact_support;
    RecyclerView.LayoutManager layoutManager;
    private SubjectAssignAdapter newsAdapter;
    public String date="";
    @BindView(R.id.root_layout)
    RelativeLayout parent;
    Spinner spinnerClass,spinnerstaff;
    ArrayList<ClassBean> classList = new ArrayList<>();
    ArrayList<ClassBean> classLists = new ArrayList<>();
    ArrayList<TeachingStaffBean> teachingStaffBeans = new ArrayList<>();
    String classid = "",g_id = "",b_id = "";
    int value;
    String staff_id="",sub_id = "";
    BottomSheetDialog mBottomSheetDialog;
    ArrayList<SubjectBeancls> subjectList = new ArrayList<>();
    AdapterOfSubjectList adaptersub;
    Spinner spinnerGroup;
    @BindView(R.id.spinnerGroups)
    Spinner spinnerGroups;
    Spinner spinnersubject;
    private ArrayList<CommonBean> groupList = new ArrayList<>();
    private ArrayList<CommonBean> groupLists = new ArrayList<>();
    ClassAdapterAttend adapter;
    TextView txtSetup;
    RelativeLayout classSubject;
    CommonProgress commonProgress;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_assign_subject);
        ButterKnife.bind(this);
        commonProgress =new CommonProgress(this);
        rootLayout = findViewById(R.id.root_layout);
        searchInput = findViewById(R.id.search_input);
        NewsRecyclerview = findViewById(R.id.recyclerView11);
        //  searchInput.setBackgroundResource(R.drawable.search_input_dark_style);
        rootLayout.setBackgroundColor(getResources().getColor(R.color.white));


        layoutManager = new LinearLayoutManager(AssignSubject.this);
        NewsRecyclerview.setLayoutManager(layoutManager);
        // newsAdapter = new NewsAdapter(this,mData);
        newsAdapter = new SubjectAssignAdapter(mData,AssignSubject.this,this);
        NewsRecyclerview.setAdapter(newsAdapter);
//        int i=NewsRecyclerview.getAdapter().getItemCount();
//        txtTitle.setText(i);
        if (!restorePrefData()){
            setShowcaseView();
        }

        isDark = !isDark ;
        if (isDark) {

            rootLayout.setBackgroundColor(getResources().getColor(R.color.white));
            //      searchInput.setBackgroundResource(R.drawable.search_input_dark_style);

        }
        else {
            rootLayout.setBackgroundColor(getResources().getColor(R.color.white));
            //          searchInput.setBackgroundResource(R.drawable.search_input_style);
        }
        if (!search.toString().isEmpty()){

            newsAdapter.getFilter().filter(search);

        }
//
//        searchInput.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//
//                if(s.length()==0)
//                {
//
//                    // Toast.makeText(getActivity(),"Module Under Working!",Toast.LENGTH_LONG).show();
//
//                    fetchStaff();
//
//
//                }else {
//                    newsAdapter.getFilter().filter(s);
//                    search = s;
//                    totalRecord.setText("Search Record:- " +String.valueOf(newsAdapter.securitylist.size()));
//                }
////                newsAdapter.getFilter().filter(s);
////                search = s;
//
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//
//            }
//        });
        // fetchBatch();
    }

    private void fetchgrouplist() {
        commonProgress.show();
        HashMap<String, String> params = new HashMap<String, String>();
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.GROUP_LIST_URL, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                ErpProgress.cancelProgressBar();
                commonProgress.dismiss();
                if (error == null) {
                    try {
                        groupList.clear();
                        groupList = CommonBean.parseCommonArray(jsonObject.getJSONArray(Constants.GROUP_DATA));
                        GroupSpinnerAdapter adapter = new GroupSpinnerAdapter(AssignSubject.this, groupList,0);
                        spinnerGroup.setAdapter(adapter);
                        spinnerGroup.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                if (i == 0) {
                                    if (adapter != null) {
                                        classList.clear();
                                        adapter.notifyDataSetChanged();
                                    }
                                }
                                if (i > 0) {
//                                    dataList.clear();
//                                    madapter.notifyDataSetChanged();
                                    g_id = groupList.get(i - 1).getId();
                                    classid = "";
                                    fetchclasslist(g_id);
                                }
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> adapterView) {

                            }
                        });
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    //Utils.showSnackBar(error.getMessage(), parent);
                }
            }
        }, this, params);
    }


    private void savePrefData(){
        sharedPreferences=AssignSubject.this.getSharedPreferences("boarding_pref2",MODE_PRIVATE);
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putBoolean("IsFirstTimeVisit2",true);
        editor.apply();
    }

    private boolean restorePrefData(){
        sharedPreferences = AssignSubject.this.getSharedPreferences("boarding_pref2",MODE_PRIVATE);
        return sharedPreferences.getBoolean("IsFirstTimeVisit2",false);
    }

    private void setShowcaseView() {
        new TapTargetSequence(this)
                .targets(TapTarget.forView(contact_support,"Assign Subject Button!","Tap the Assign button to assign Subject.")
                        .outerCircleColor(R.color.light)
                        .outerCircleAlpha(0.96f)
                        .targetCircleColor(R.color.white)
                        .titleTextSize(20)
                        .titleTextColor(R.color.white)
                        .descriptionTextSize(14)
                        .descriptionTextColor(R.color.black)
                        .textColor(R.color.black)
                        .textTypeface(Typeface.SANS_SERIF)
                        .dimColor(R.color.black)
                        .drawShadow(true)
                        .cancelable(false)
                        .tintTarget(true)
                        .transparentTarget(true)
                        .targetRadius(60)).listener(new TapTargetSequence.Listener() {
                    @Override
                    public void onSequenceFinish() {
                        // Toast.makeText(getApplicationContext(),"Sequence Finished",Toast.LENGTH_SHORT).show();
                        savePrefData();
                        openDialog();
                    }

                    @Override
                    public void onSequenceStep(TapTarget lastTarget, boolean targetClicked) {
                        // Toast.makeText(getActivity(),"GREAT!",Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onSequenceCanceled(TapTarget lastTarget) {

                    }
                }).start();


    }

    @SuppressLint("MissingInflatedId")
    private void openDialog() {

        mBottomSheetDialog = new BottomSheetDialog(this);
        final View sheetView = getLayoutInflater().inflate(R.layout.incharge_dialog, null);
        mBottomSheetDialog.setContentView(sheetView);
        mBottomSheetDialog.setCancelable(true);

        spinnerClass=(Spinner) sheetView.findViewById(R.id.spinnerClass);
        spinnerstaff=(Spinner) sheetView.findViewById(R.id.spinnerstaff);
        spinnerGroup=(Spinner) sheetView.findViewById(R.id.spinnerGroup);
        spinnersubject=(Spinner) sheetView.findViewById(R.id.spinnersubject);
        spinnersection = (Spinner) sheetView.findViewById(R.id.spinnersection);

        txtSetup=(TextView) sheetView.findViewById(R.id.txtSetup);
        txtSetup.setText("Assign Subject");
        classSubject=(RelativeLayout) sheetView.findViewById(R.id.classSubject);
        classSubject.setVisibility(View.VISIBLE);
//        fetchStafffffff();
//        fetchclasslist();

        fetchgrouplist();

        Button btnYes= (Button) sheetView.findViewById(R.id.btnYes);
        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                assignData();
            }
        });
        mBottomSheetDialog.show();

    }



    private void assignData() {

        if (g_id.equalsIgnoreCase("")) {
            Toast.makeText(getApplicationContext(),"Kindly Select Batch",Toast.LENGTH_LONG).show();

        } else if (classid.equalsIgnoreCase("")) {
            Toast.makeText(getApplicationContext(),"Kindly Select Class",Toast.LENGTH_LONG).show();

        } else if (sectionid.equalsIgnoreCase("")) {
            Toast.makeText(getApplicationContext(), "Kindly Select Class Section", Toast.LENGTH_LONG).show();

        }else if (sub_id.equalsIgnoreCase("")) {
            Toast.makeText(getApplicationContext(),"Kindly Select Class Subject",Toast.LENGTH_LONG).show();

        }else if (staff_id.equalsIgnoreCase("")) {
            Toast.makeText(getApplicationContext(),"Kindly Select Staff",Toast.LENGTH_LONG).show();

        } else{
            commonProgress.show();
            HashMap<String, String> params = new HashMap<>();
            params.put("class_id", classid);
            params.put("g_id", g_id);
            params.put("section_id", sectionid);
            params.put("s_id", staff_id);
            params.put("sub_id", sub_id);
            params.put("user_id", User.getCurrentUser().getId());

            ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.ADDSUB, apisendmsgCallback, this, params);
        }
    }
    ApiHandler.ApiCallback apisendmsgCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgressVert.cancelProgressBar();
            commonProgress.dismiss();
            if (error == null) {
                try {
                    // fetchStaff(value);
                    // groupLists.clear();
                    searchInput.setText("");
                    fetchBatch();

                    Toast.makeText(getApplicationContext(),jsonObject.getString("msg"),Toast.LENGTH_LONG).show();
                    mBottomSheetDialog.dismiss();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("error", error.getMessage() + "");
                // Utils.showSnackBar(error.getMessage(), parent);
                Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_LONG).show();

            }
        }
    };


    @OnClick(R.id.contact_support)
    public void contact_support() {
        openDialog();
    }

    @OnClick(R.id.imgBack)
    public void onback() {
        finish();
    }

    private void fetchStafffffff() {
        spinnerstaff.setVisibility(View.VISIBLE);
        HashMap<String, String> params = new HashMap<String, String>();
        //params.put("check","assign");
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.STAFF_LIST_TEACHER, classapiCallback1, this, params);
    }

    ApiHandler.ApiCallback classapiCallback1= new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            if (error == null) {
                try {
                    teachingStaffBeans = TeachingStaffBean.parseTeachingStaffBeanArray(jsonObject.getJSONArray("staff_data"));
                    InchargeSpinner adapter = new InchargeSpinner(AssignSubject.this, teachingStaffBeans);
                    spinnerstaff.setAdapter(adapter);
                    spinnerstaff.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                            if (i > 0) {
                                staff_id = teachingStaffBeans.get(i-1).getId();
                            }else{
                                staff_id="";
                            }

                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_LONG).show();
                //    Utils.showSnackBar(error.getMessage(), parent);
            }
        }
    };

    private void fetchclasslist(String g_id) {
        spinnerClass.setVisibility(View.VISIBLE);
        HashMap<String, String> params = new HashMap<String, String>();
//        params.put("check","subject");
        params.put("id",g_id);
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.GROUPCLASSLIST_URL, classapiCallback, this, params);
    }


    ApiHandler.ApiCallback classapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            if (error == null) {
                try {
                    classList = ClassBean.parseClassArray(jsonObject.getJSONArray(Constants.CLASSDATA));
                    adapter = new ClassAdapterAttend(AssignSubject.this, classList,0);
                    spinnerClass.setAdapter(adapter);
                    spinnerClass.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                            if (i > 0) {
                                classid = classList.get(i-1).getId();
                                // g_id = classList.get(i-1).getGroup_data().getId();
                            }else{
                                classid="";
                            }
                            fetchSection();
                            fetchSubject(classid);
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_LONG).show();
                //    Utils.showSnackBar(error.getMessage(), parent);
            }
        }
    };

    private void fetchSection() {
        spinnersection.setVisibility(View.VISIBLE);
        HashMap<String, String> params = new HashMap<String, String>();
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.SECTIONLIST_URL, sectionapiCallback, this, params);
    }

    ApiHandler.ApiCallback sectionapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            if (error == null) {
                try {
                    sectionList = SectionBean.parseCommonArray(jsonObject.getJSONArray("hostelroom_data"));
                    sectionnewAdapter = new SectionnewAdapter(AssignSubject.this, sectionList);
                    spinnersection.setAdapter(sectionnewAdapter);
                    spinnersection.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if (i > 0) {
                                sectionid = sectionList.get(i - 1).getSection_id();
                            } else {
                                sectionid = "";
                            }
                            fetchStafffffff();
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();

                if (error.getStatusCode() == 405) {
                    User.logout();
                    startActivity(new Intent(AssignSubject.this, LoginActivity.class));
                    finish();
                }
            }
        }
    };


    private void fetchSubject(String classid) {
        spinnersubject.setVisibility(View.VISIBLE);
        HashMap<String, String> params = new HashMap<String, String>();
        // params.put("check","assign");
        params.put("id", classid);
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.SubjectList, subapiCallback, this, params);

    }

    ApiHandler.ApiCallback subapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            /* */

            if (error == null) {
                try {

                    subjectList = SubjectBeancls.parseClassArray(jsonObject.getJSONArray("sub_data"));

                    adaptersub = new AdapterOfSubjectList(AssignSubject.this, subjectList);
                    spinnersubject.setAdapter(adaptersub);
                    spinnersubject.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if (i != 0) {
                                // sub_name = subjectList.get(i - 1).getName();
                                sub_id = subjectList.get(i - 1).getId();
                            }else {
                                sub_id="";
                            }
                            fetchStafffffff();
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    };

    private void saveThemeStatePref(boolean isDark) {

        SharedPreferences pref = getApplicationContext().getSharedPreferences("myPref",MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean("isDark",isDark);
        editor.commit();
    }

    private boolean getThemeStatePref () {

        SharedPreferences pref = getApplicationContext().getSharedPreferences("myPref",MODE_PRIVATE);
        boolean isDark = pref.getBoolean("isDark",false) ;
        return isDark;

    }



    @Override
    protected void onResume() {
        super.onResume();
//        if (User.getCurrentUser().getType().equalsIgnoreCase("watchmen")) {
//            // Toast.makeText(getActivity(),"Module Under Working!",Toast.LENGTH_LONG).show();
//            contact_support.setVisibility(View.GONE);
        txtTitle.setText("Assign Subject");
        // fetchStaff();
        fetchBatch();
        searchInput.setText("");
//        }else{
//            fetchStudent();
//            txtTitle.setText("Student List");
//        }

    }

    private void fetchBatch() {
        commonProgress.show();
        HashMap<String, String> params = new HashMap<String, String>();
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.CLASSLIST_URL, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                ErpProgress.cancelProgressBar();
                commonProgress.dismiss();
                if (error == null) {
                    try {
                        classLists.clear();
                        classLists = ClassBean.parseClassArray(jsonObject.getJSONArray(Constants.CLASSDATA));
                        adapter = new ClassAdapterAttend(AssignSubject.this, classLists,0);
                        spinnerGroups.setAdapter(adapter);
                        spinnerGroups.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                searchInput.setText("");

                                //value=i;

                                if (i != 0) {
//                                    dataList.clear();
//                                    madapter.notifyDataSetChanged();
                                    b_id = classLists.get(i - 1).getId();

                                }
                                fetchStaff(i);

                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> adapterView) {

                            }
                        });
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    //Utils.showSnackBar(error.getMessage(), parent);
                }
            }
        }, this, params);
    }


    private void fetchStaff(int i) {
        //  ErpProgress.showProgressBar(AssignSubject.this, "Please wait...");
        //   Toast.makeText(getApplicationContext(),"value "+ i,Toast.LENGTH_SHORT).show();

        HashMap<String, String> params = new HashMap<String, String>();
        if (i == 0){
            params.put("b_id", "");
        } else {
            params.put("b_id", b_id);
        }
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.ASSIGNSUBJECT, apiCallback, this, params);
    }

    ApiHandler.ApiCallback apiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            if (error == null) {
                ErpProgress.cancelProgressBar();
                try {
                    if (mData != null) {
                        mData.clear();
                    }

                    if (mData != null){
                        mData.clear();
                        mData=AssignSubjectBean.parseAssignSubjectBeanArray(jsonObject.getJSONArray("subject_data"));
                        Log.e("sizeeeeeeeeeeeeeeeeee", String.valueOf(mData.size()));
                        newsAdapter.setsecuList(mData);
                        newsAdapter.notifyDataSetChanged();
                        totalRecord.setVisibility(View.VISIBLE);
                        totalRecord.setText("Total Entries:- "+String.valueOf(mData.size()));
                        txtNorecord.setVisibility(View.GONE);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                ErpProgress.cancelProgressBar();
                totalRecord.setText("Total Entries:- 0");
                txtNorecord.setVisibility(View.VISIBLE);
                mData.clear();
                newsAdapter.setsecuList(mData);
                newsAdapter.notifyDataSetChanged();
            }
        }
    };

    @Override
    public void delete(AssignSubjectBean homeworkbean) {
        //Toast.makeText(getActivity(), homeworkbean.getEs_messagesid(), Toast.LENGTH_LONG).show();
        commonProgress.show();
        HashMap<String, String> params = new HashMap<>();
        params.put("h_id", homeworkbean.getId());
        params.put("check", "subject");
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.DELETEHW, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                ErpProgress.cancelProgressBar();
                commonProgress.dismiss();
                if (error == null) {
                    int pos = mData.indexOf(homeworkbean);
                    mData.remove(pos);
                    newsAdapter.setsecuList(mData);
                    newsAdapter.notifyDataSetChanged();
                    Utils.showSnackBar("Deleted Successfully!", parent);
                    totalRecord.setText("Total Entries:- "+String.valueOf(mData.size()));
                } else {
                    Log.e("error", error.getMessage() + "");
                    Utils.showSnackBar(error.getMessage(), parent);
                    //  Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_LONG).show();

                }
            }
        }, this, params);
    }
}