package com.ultimate.ultimatesmartschool.Staff;

import com.ultimate.ultimatesmartschool.Utility.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class View_staff_bean {


    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    private String age;


    public String getDate_time() {
        return date_time;
    }

    public void setDate_time(String date_time) {
        this.date_time = date_time;
    }

    private String date_time;


    private static String ID="id";
    private static String NAME="name";
    private static String DESIGNAMTION="designation";
    private static String DATEOFBIRTH="dateofbirth";
    private static String DATEOFJOINING="dateofjoining";
    private static String SALARY="salary";
    private static String POST="post";
    private static String IMAGE="image";
    /**
     * id : 3
     * name : Staff Gupta
     * designation : technical
     * dateofbirth : 1995-08-17
     * dateofjoining : 2018-03-10
     * salary : 15000
     */

    private String id;
    private String name;
    private String designation;
    private String dateofbirth;
    private String dateofjoining;
    private String salary;
    private String phonno;
    private String email;
    private String  bloodgroup;
    private String fathername;
    private String gender;
    private String address;

    public String getSt_prcity() {
        return st_prcity;
    }

    public void setSt_prcity(String st_prcity) {
        this.st_prcity = st_prcity;
    }

    private String st_prcity;

    public String getSt_prstate() {
        return st_prstate;
    }

    public void setSt_prstate(String st_prstate) {
        this.st_prstate = st_prstate;
    }

    public String getSt_prcountry() {
        return st_prcountry;
    }

    public void setSt_prcountry(String st_prcountry) {
        this.st_prcountry = st_prcountry;
    }

    public String getSt_prpincode() {
        return st_prpincode;
    }

    public void setSt_prpincode(String st_prpincode) {
        this.st_prpincode = st_prpincode;
    }

    public String getSt_dist() {
        return st_dist;
    }

    public void setSt_dist(String st_dist) {
        this.st_dist = st_dist;
    }

    public String getDepart_id() {
        return depart_id;
    }

    public void setDepart_id(String depart_id) {
        this.depart_id = depart_id;
    }

    public String getSt_post() {
        return st_post;
    }

    public void setSt_post(String st_post) {
        this.st_post = st_post;
    }

    public String getSt_category() {
        return st_category;
    }

    public void setSt_category(String st_category) {
        this.st_category = st_category;
    }

    public String getTeach_nonteach() {
        return teach_nonteach;
    }

    public void setTeach_nonteach(String teach_nonteach) {
        this.teach_nonteach = teach_nonteach;
    }

    private String st_prstate;
    private String st_prcountry;
    private String st_prpincode;
    private String st_dist;
    private String depart_id;
    private String st_post;
    private String st_category;

    private String teach_nonteach;
    // private String st_category;



    private String username;
    private String password;
    private String fatherphonno;
    private String otherphonno;

    public String getMobile_name() {
        return mobile_name;
    }

    public void setMobile_name(String mobile_name) {
        this.mobile_name = mobile_name;
    }

    private String mobile_name;

    public String getMobile_brand() {
        return mobile_brand;
    }

    public void setMobile_brand(String mobile_brand) {
        this.mobile_brand = mobile_brand;
    }

    private String mobile_brand;

    public String getFatherphonno() {
        return fatherphonno;
    }

    public void setFatherphonno(String fatherphonno) {
        this.fatherphonno = fatherphonno;
    }

    public String getOtherphonno() {
        return otherphonno;
    }

    public void setOtherphonno(String otherphonno) {
        this.otherphonno = otherphonno;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getBloodgroup() {
        return bloodgroup;
    }

    public void setBloodgroup(String bloodgroup) {
        this.bloodgroup = bloodgroup;
    }

    public String getFathername() {
        return fathername;
    }

    public void setFathername(String fathername) {
        this.fathername = fathername;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * post : FIELD MANAGER
     */

    private String post;
    /**
     * image : office_admin/images/user_06292018_050600.jpg
     */

    private String image;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getDateofbirth() {
        return dateofbirth;
    }

    public void setDateofbirth(String dateofbirth) {
        this.dateofbirth = dateofbirth;
    }

    public String getDateofjoining() {
        return dateofjoining;
    }

    public void setDateofjoining(String dateofjoining) {
        this.dateofjoining = dateofjoining;
    }

    public String getSalary() {
        return salary;
    }

    public void setSalary(String salary) {
        this.salary = salary;
    }

    public String getPhonno() {
        return phonno;
    }

    public void setPhonno(String phonno) {
        this.phonno = phonno;
    }

    public static ArrayList<View_staff_bean> parseviewstaffArray(JSONArray jsonArray) {

        ArrayList<View_staff_bean> list = new ArrayList<View_staff_bean>();
        try {

            for (int i = 0; i < jsonArray.length(); i++) {
                View_staff_bean p = parseHWObject(jsonArray.getJSONObject(i));
                if (p != null) {
                    list.add(p);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();

        }
        return list;

    }

    private static View_staff_bean parseHWObject(JSONObject jsonObject) {
        View_staff_bean casteObj = new View_staff_bean();
        try {
            if (jsonObject.has(ID)) {
                casteObj.setId(jsonObject.getString(ID));
            }
            if (jsonObject.has(NAME) && !jsonObject.getString(NAME).isEmpty() && !jsonObject.getString(NAME).equalsIgnoreCase("null")) {
                casteObj.setName(jsonObject.getString(NAME));
            }

            if (jsonObject.has(DESIGNAMTION) && !jsonObject.getString(DESIGNAMTION).isEmpty() && !jsonObject.getString(DESIGNAMTION).equalsIgnoreCase("null")) {
                casteObj.setDesignation(jsonObject.getString(DESIGNAMTION));
            }
            if (jsonObject.has(DATEOFBIRTH) && !jsonObject.getString(DATEOFBIRTH).isEmpty() && !jsonObject.getString(DATEOFBIRTH).equalsIgnoreCase("null")) {
                casteObj.setDateofbirth(jsonObject.getString(DATEOFBIRTH));
            }

            if (jsonObject.has("age") && !jsonObject.getString("age").isEmpty() && !jsonObject.getString("age").equalsIgnoreCase("null")) {
                casteObj.setAge(jsonObject.getString("age"));
            }

            if (jsonObject.has(DATEOFJOINING) && !jsonObject.getString(DATEOFJOINING).isEmpty() && !jsonObject.getString(DATEOFJOINING).equalsIgnoreCase("null")) {
                casteObj.setDateofjoining(jsonObject.getString(DATEOFJOINING));
            }
            if (jsonObject.has(SALARY) && !jsonObject.getString(SALARY).isEmpty() && !jsonObject.getString(SALARY).equalsIgnoreCase("null")) {
                casteObj.setSalary(jsonObject.getString(SALARY));
            }
            if (jsonObject.has(POST) && !jsonObject.getString(POST).isEmpty() && !jsonObject.getString(POST).equalsIgnoreCase("null")) {
                casteObj.setPost(jsonObject.getString(POST));
            }
            if (jsonObject.has(IMAGE) && !jsonObject.getString(IMAGE).isEmpty() && !jsonObject.getString(IMAGE).equalsIgnoreCase("null")) {
                casteObj.setImage(Constants.getImageBaseURL()+jsonObject.getString(IMAGE));
            }
            if (jsonObject.has("phonno") && !jsonObject.getString("phonno").isEmpty() && !jsonObject.getString("phonno").equalsIgnoreCase("null")) {
                casteObj.setPhonno(jsonObject.getString("phonno"));
            }
            if (jsonObject.has("email") && !jsonObject.getString("email").isEmpty() && !jsonObject.getString("email").equalsIgnoreCase("null")) {
                casteObj.setEmail(jsonObject.getString("email"));
            }

            if (jsonObject.has("bloodgroup") && !jsonObject.getString("bloodgroup").isEmpty() && !jsonObject.getString("bloodgroup").equalsIgnoreCase("null")) {
                casteObj.setBloodgroup(jsonObject.getString("bloodgroup"));
            }
            if (jsonObject.has("fathername") && !jsonObject.getString("fathername").isEmpty() && !jsonObject.getString("fathername").equalsIgnoreCase("null")) {
                casteObj.setFathername(jsonObject.getString("fathername"));
            }

            if (jsonObject.has("gender") && !jsonObject.getString("gender").isEmpty() && !jsonObject.getString("gender").equalsIgnoreCase("null")) {
                casteObj.setGender(jsonObject.getString("gender"));
            }
            if (jsonObject.has("address") && !jsonObject.getString("address").isEmpty() && !jsonObject.getString("address").equalsIgnoreCase("null")) {
                casteObj.setAddress(jsonObject.getString("address"));
            }

            if (jsonObject.has("username") && !jsonObject.getString("username").isEmpty() && !jsonObject.getString("username").equalsIgnoreCase("null")) {
                casteObj.setUsername(jsonObject.getString("username"));
            }
            if (jsonObject.has("password") && !jsonObject.getString("password").isEmpty() && !jsonObject.getString("password").equalsIgnoreCase("null")) {
                casteObj.setPassword(jsonObject.getString("password"));
            }

            if (jsonObject.has("fatherphonno") && !jsonObject.getString("fatherphonno").isEmpty() && !jsonObject.getString("fatherphonno").equalsIgnoreCase("null")) {
                casteObj.setFatherphonno(jsonObject.getString("fatherphonno"));
            }
            if (jsonObject.has("otherphonno") && !jsonObject.getString("otherphonno").isEmpty() && !jsonObject.getString("otherphonno").equalsIgnoreCase("null")) {
                casteObj.setOtherphonno(jsonObject.getString("otherphonno"));
            }

            if (jsonObject.has("mobile_name") && !jsonObject.getString("mobile_name").isEmpty() && !jsonObject.getString("mobile_name").equalsIgnoreCase("null")) {
                casteObj.setMobile_name(jsonObject.getString("mobile_name"));
            }

            if (jsonObject.has("mobile_brand") && !jsonObject.getString("mobile_brand").isEmpty() && !jsonObject.getString("mobile_brand").equalsIgnoreCase("null")) {
                casteObj.setMobile_brand(jsonObject.getString("mobile_brand"));
            }

            if (jsonObject.has("date_time") && !jsonObject.getString("date_time").isEmpty() && !jsonObject.getString("date_time").equalsIgnoreCase("null")) {
                casteObj.setDate_time(jsonObject.getString("date_time"));
            }

            if (jsonObject.has("teach_nonteach") && !jsonObject.getString("teach_nonteach").isEmpty() && !jsonObject.getString("teach_nonteach").equalsIgnoreCase("null")) {
                casteObj.setTeach_nonteach(jsonObject.getString("teach_nonteach"));
            }

            if (jsonObject.has("st_category") && !jsonObject.getString("st_category").isEmpty() && !jsonObject.getString("st_category").equalsIgnoreCase("null")) {
                casteObj.setSt_category(jsonObject.getString("st_category"));
            }

            if (jsonObject.has("st_post") && !jsonObject.getString("st_post").isEmpty() && !jsonObject.getString("st_post").equalsIgnoreCase("null")) {
                casteObj.setSt_post(jsonObject.getString("st_post"));
            }

            if (jsonObject.has("depart_id") && !jsonObject.getString("depart_id").isEmpty() && !jsonObject.getString("depart_id").equalsIgnoreCase("null")) {
                casteObj.setDepart_id(jsonObject.getString("depart_id"));
            }

            if (jsonObject.has("st_prpincode") && !jsonObject.getString("st_prpincode").isEmpty() && !jsonObject.getString("st_prpincode").equalsIgnoreCase("null")) {
                casteObj.setSt_prpincode(jsonObject.getString("st_prpincode"));
            }

            if (jsonObject.has("st_prcountry") && !jsonObject.getString("st_prcountry").isEmpty() && !jsonObject.getString("st_prcountry").equalsIgnoreCase("null")) {
                casteObj.setSt_prcountry(jsonObject.getString("st_prcountry"));
            }


            if (jsonObject.has("st_dist") && !jsonObject.getString("st_dist").isEmpty() && !jsonObject.getString("st_dist").equalsIgnoreCase("null")) {
                casteObj.setSt_dist(jsonObject.getString("st_dist"));
            }

            if (jsonObject.has("st_prstate") && !jsonObject.getString("st_prstate").isEmpty() && !jsonObject.getString("st_prstate").equalsIgnoreCase("null")) {
                casteObj.setSt_prstate(jsonObject.getString("st_prstate"));
            }

            if (jsonObject.has("st_prcity") && !jsonObject.getString("st_prcity").isEmpty() && !jsonObject.getString("st_prcity").equalsIgnoreCase("null")) {
                casteObj.setSt_prcity(jsonObject.getString("st_prcity"));
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }
        return casteObj;
    }

    public String getPost() {
        return post;
    }

    public void setPost(String post) {
        this.post = post;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
