package com.ultimate.ultimatesmartschool.Staff;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewSwitcher;

import com.android.volley.Request;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;
import com.ultimate.ultimatesmartschool.BeanModule.User;
import com.ultimate.ultimatesmartschool.Fee.AddFeesQRActivity;
import com.ultimate.ultimatesmartschool.Fee.FeeDetail;
import com.ultimate.ultimatesmartschool.Home.Adapter_spinner_viewdepart;
import com.ultimate.ultimatesmartschool.Home.ViewStaffActivity;
import com.ultimate.ultimatesmartschool.Login.LoginActivity;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Staff.Fragments.UpdateStaff;
import com.ultimate.ultimatesmartschool.Student.StudentBirthdayList;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.CommonProgress;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Objects;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import nl.dionsegijn.konfetti.KonfettiView;
import nl.dionsegijn.konfetti.models.Shape;
import nl.dionsegijn.konfetti.models.Size;

public class StaffBirthdayList extends AppCompatActivity implements VAdapter_view_staff.StaffCallBack {

    Dialog mBottomSheetDialog1;
    KonfettiView konfettiView;
    Button btnSetting;
    TextView bdayMsg;
    RelativeLayout layout0, layoutW;
    ViewSwitcher logo0, logo1, logo2, logo3, logow, logox, logoy, logoz;
    ImageButton export;
    ImageView close;

    @BindView(R.id.spinerVehicletype)
    Spinner spinnerdepart;
    @BindView(R.id.recyclerviewlist)
    RecyclerView recyclerView;
    @BindView(R.id.imgBack)
    ImageView back;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.totalRecord)
    TextView totalRecord;
    //private LinearLayoutManager layoutManager;
    RecyclerView.LayoutManager layoutManager;
    @BindView(R.id.parent)
    RelativeLayout parent;
    ArrayList<Deparment_bean> department_list=new ArrayList<>();
    Adapter_spinner_viewdepart adap;
    String departmentid="";
    public String date;
    private VAdapter_view_staff adapter;
    ArrayList<View_staff_bean> viewstafflist=new ArrayList<>();
    int loaded=0;
    @BindView(R.id.textNorecord)TextView txtNorecord;
    CommonProgress commonProgress;
    @BindView(R.id.textView8)
    TextView textView8;
    @BindView(R.id.dialog)
    ImageView dialog;
    Animation animation;
    boolean isDark = false;
    String tag="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_staff_leave_list);
        ButterKnife.bind(this);
        if (getIntent().getExtras() !=null){
            tag = getIntent().getExtras().getString("id");
        }else {
            tag="";
        }

        textView8.setText("Filter List");
        dialog.setVisibility(View.GONE);
        getTodayDate();
        if (tag.equalsIgnoreCase("weekly")){
            txtTitle.setText("Upcoming Birthday List");
        }else {
            txtTitle.setText("Today's Birthday List");
        }
        animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.btn_blink_animation);
        commonProgress=new CommonProgress(StaffBirthdayList.this);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        layoutManager = new LinearLayoutManager(StaffBirthdayList.this);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new VAdapter_view_staff(viewstafflist, StaffBirthdayList.this,this);
        recyclerView.setAdapter(adapter);
        fetchdepartment();isDark = !isDark ;
        if (isDark) {

            parent.setBackgroundColor(getResources().getColor(R.color.white));
            //  searchInput.setBackgroundResource(R.drawable.search_input_dark_style);

        }
        else {
            parent.setBackgroundColor(getResources().getColor(R.color.white));
        }
//        if (!search.toString().isEmpty()){
//
//            adapter.getFilter().filter(search);
//
//        }

    }

    @OnClick(R.id.dialog)
    public void dialog() {
        Intent intent = new Intent(StaffBirthdayList.this, AddFeesQRActivity.class);
        intent.putExtra("key", "card");
        startActivity(intent);
    }

    public void getTodayDate() {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.HOUR_OF_DAY, 00);
        c.set(Calendar.MINUTE, 00);
        c.set(Calendar.SECOND, 00);
        Date setdate = c.getTime();
        SimpleDateFormat fmtOut = new SimpleDateFormat("dd MMM, yyyy");
        String dateString = fmtOut.format(setdate);
        // adddate.setText(dateString);
        SimpleDateFormat dateFrmOut = new SimpleDateFormat("yyyy-MM-dd");
        date = dateFrmOut.format(setdate);
    }

    private void fetchdepartment() {
        if(loaded==0){
            commonProgress.show();
        }loaded++;
        HashMap<String,String> params=new HashMap<>();
        ApiHandler.apiHit(Request.Method.GET, Constants.getBaseURL() + Constants.deparmrnt, apiCallback, this, params);
    }
    ApiHandler.ApiCallback apiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            commonProgress.dismiss();
            // txtupload.setVisibility(View.VISIBLE);
            if (error == null) {
                try {
                    if (department_list != null) {
                        department_list.clear();
                    }
                    JSONArray jsonArray = jsonObject.getJSONArray("department_data");
                    department_list = Deparment_bean.parseDepartmentArray(jsonArray);
                    adap= new Adapter_spinner_viewdepart(StaffBirthdayList.this,department_list);
                    spinnerdepart.setAdapter(adap);
                    spinnerdepart.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if (i > 0) {
                                departmentid = department_list.get(i-1 ).getId();
                            }
                            fetchstaffdetail(i);
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Utils.showSnackBar(error.getMessage(), parent);
                if (error.getStatusCode() == 405) {
                    User.logout();
                    startActivity(new Intent(StaffBirthdayList.this, LoginActivity.class));
                    StaffBirthdayList.this.finish();
                }
            }
        }
    };

    private void fetchstaffdetail(int i) {

        commonProgress.show();
        HashMap<String, String> params = new HashMap<String, String>();
        // params.put("class_id", classid);
        if(i==0){
            params.put("departid", "");
        }else{
            params.put("departid", departmentid);
        }
        params.put("check", "staff");
        params.put("date", date);
        params.put("tag", tag);
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.birthdaylist, apiCallbackstaffdetail, this, params);

    }

    ApiHandler.ApiCallback apiCallbackstaffdetail = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            commonProgress.dismiss();
            //txtupload.setVisibility(View.VISIBLE);
            if (error == null) {
                try {
                    if (viewstafflist != null) {
                        viewstafflist.clear();
                    }
                    JSONArray jsonArray = jsonObject.getJSONArray("birthday_data");
                    viewstafflist = View_staff_bean.parseviewstaffArray(jsonArray);
                    if (viewstafflist.size() > 0) {
                        adapter.setstaffdetailList(viewstafflist);
                        adapter.notifyDataSetChanged();
                        totalRecord.setText("Total Entries:- "+String.valueOf(viewstafflist.size()));
                        txtNorecord.setVisibility(View.GONE);
                    } else {
                        txtNorecord.setVisibility(View.VISIBLE);
                        adapter.setstaffdetailList(viewstafflist);
                        adapter.notifyDataSetChanged();
                        totalRecord.setText("Total Entries:- 0");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                txtNorecord.setVisibility(View.VISIBLE);
                viewstafflist.clear();
                adapter.setstaffdetailList(viewstafflist);
                totalRecord.setText("Total Entries:- 0");
                adapter.notifyDataSetChanged();
            }
        }
    };


    @Override
    public void staffCallBack(View_staff_bean viewStaffBean) {
        String phoneNo = viewStaffBean.getPhonno();
        Log.i("email",phoneNo);
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:"+ phoneNo));
        startActivity(intent);
    }

    @Override
    public void onUpdateCallback(View_staff_bean viewStaffBean) {

    }

    @Override
    public void staffemailCallBack(View_staff_bean viewStaffBean) {
        String email = viewStaffBean.getEmail();
        Log.i("email",email);
        Intent intent = new Intent(Intent.ACTION_SENDTO); // it's not ACTION_SEND
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_SUBJECT, "Subject of email");
        intent.putExtra(Intent.EXTRA_TEXT, "Body of email");
        intent.setData(Uri.parse("mailto:"+email)); // or just "mailto:" for blank
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK); // this will make such that when user returns to your app, your app is displayed, instead of the email app.
        startActivity(intent);
    }

    @Override
    public void staffProfile(View_staff_bean std) {
//        Gson gson  = new Gson();
//        String staffdet = gson.toJson(viewStaffBean,View_staff_bean.class);
//        Intent i = new Intent(StaffBirthdayList.this, StaffProfile.class);
//        i.putExtra("staffdet",staffdet);
//        startActivity(i);
//        mBottomSheetDialog1 = new Dialog(this, android.R.style.Theme_Translucent_NoTitleBar);
//        final View sheetView = getLayoutInflater().inflate(R.layout.birthday_dialog, null);
//        mBottomSheetDialog1.setContentView(sheetView);
//                mBottomSheetDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

//        Window window = mBottomSheetDialog1.getWindow();
//        WindowManager.LayoutParams wlp = window.getAttributes();
//
//        wlp.gravity = Gravity.CENTER;
//        wlp.flags &= ~WindowManager.LayoutParams.FLAG_BLUR_BEHIND;
//        window.setAttributes(wlp);
        //  mBottomSheetDialog1.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);

        //  mBottomSheetDialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

//
//        mBottomSheetDialog1.setCancelable(false);
//
//
//        TextView bdayText = sheetView.findViewById(R.id.bdayText);
//
//        bdayText.setText("Happy Birthday, "+std.getName()+ "");
//
//        logo0 = sheetView.findViewById(R.id.logo0);
//        logo1 = sheetView.findViewById(R.id.logo1);
//        logo2 = sheetView.findViewById(R.id.logo2);
//        logo3 = sheetView.findViewById(R.id.logo3);
//        logow = sheetView.findViewById(R.id.logow);
//        logox = sheetView.findViewById(R.id.logox);
//        logoy = sheetView.findViewById(R.id.logoy);
//        logoz = sheetView.findViewById(R.id.logoz);
//
//
//        CircularImageView logow1 = sheetView.findViewById(R.id.logow1);
//        CircularImageView logow2 = sheetView.findViewById(R.id.logow2);
//
//        if (std.getImage() != null) {
//            //  Picasso.with(context).load(viewstafflist.get(position).getImage()).placeholder(context.getResources().getDrawable(R.drawable.man)).into(holder.image);
//            // Utils.progressImg(std.getProfile(),logow1,this);
//            Picasso.with(this).load(std.getImage()).into(logow1);
//            Picasso.with(this).load(std.getImage()).into(logow2);
//        } else {
//            // Picasso.with(context).load(String.valueOf(context.getResources().getDrawable(R.drawable.staffimg))).into(holder.image);
//            Picasso.with(this).load(R.drawable.boy).into(logow1);
//            Picasso.with(this).load(R.drawable.boy).into(logow2);
//        }
//
//        final MediaPlayer mp = MediaPlayer.create(this, R.raw.bdaytone);
//        mp.start();
//        mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
//            @Override
//            public void onCompletion(MediaPlayer mp) {
//                mp.release();
//            }
//        });
//        ImageView close = sheetView.findViewById(R.id.close);
//        close.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                mp.release();
//                //saveData();
//                mBottomSheetDialog1.dismiss();
//
//            }
//        });
//
//
//        bdayMsg = sheetView.findViewById(R.id.bdaywishText);
//        btnSetting = sheetView.findViewById(R.id.btnSettings);
//
//        btnSetting.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                // startActivity(new Intent(StudentBirthdayList.this, SettingsActivity.class));
//            }
//        });
//
//
//        layout0 = sheetView.findViewById(R.id.layout0);
//        layoutW = sheetView.findViewById(R.id.layoutW);
//
//        Animation rotation = AnimationUtils.loadAnimation(this, R.anim.anim_pic);
//        logo0.setAnimation(rotation);
//        logow.setAnimation(rotation);
//
//        layoutOneView();
//
//
//        konfettiView = sheetView.findViewById(R.id.viewKonfetti);
//        konfettiView.build()
//                .addColors(Color.YELLOW, Color.GREEN, Color.MAGENTA)
//                .setDirection(0.0, 359.0)
//                .setSpeed(1f, 5f)
//                .setFadeOutEnabled(true)
//                .setTimeToLive(2000L)
//                .addShapes(Shape.Square.INSTANCE, Shape.Circle.INSTANCE)
//                .addSizes(new Size(12, 5f))
//                .setPosition(-50f, konfettiView.getWidth() + 50f, -50f, -50f)
//                .streamFor(300, 5000000L);
//


//        ImageView close = (ImageView) sheetView.findViewById(R.id.close);
//        ImageView imageeee = (ImageView) sheetView.findViewById(R.id.circleimgrecepone);
//
//        RelativeLayout birth_card = (RelativeLayout) sheetView.findViewById(R.id.birth_card);
//        RelativeLayout default_card = (RelativeLayout) sheetView.findViewById(R.id.default_card);
//
//        if (!image_url_birth.equalsIgnoreCase("https://ultimatesolutiongroup.com/office_admin/images/school_logo/")) {
//            birth_card.setVisibility(View.VISIBLE);
//            default_card.setVisibility(View.GONE);
//            // Utility.progressImg(image_url_birth,imageeee,getActivity());
//            Picasso.with(getActivity()).load(image_url_birth).placeholder(R.color.transparent).into(imageeee);
//
//        } else {
//            birth_card.setVisibility(View.GONE);
//            default_card.setVisibility(View.VISIBLE);
//            // Picasso.with(AddFeesQRActivity.this).load(R.color.transparent).into(image);
//            // Toast.makeText(getActivity(), "Birthday Card not found, kindly select Birthday Card Image File to upload", Toast.LENGTH_SHORT).show();
//            /// default_card.setVisibility(View.VISIBLE);
//            // selectImg();
//        }
//
//        final MediaPlayer mp = MediaPlayer.create(getActivity(), R.raw.birth_sound);
//        mp.start();
//        mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
//            @Override
//            public void onCompletion(MediaPlayer mp) {
//                mp.release();
//            }
//        });
//
//        close.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                mp.release();
//                saveData();
//                // mBottomSheetDialog.dismiss();
//
//            }
//        });

        //   mBottomSheetDialog1.show();

        mBottomSheetDialog1 = new Dialog(this);
        final View sheetView = getLayoutInflater().inflate(R.layout.birthday_dialog_new_staff, null);
        mBottomSheetDialog1.setContentView(sheetView);

        mBottomSheetDialog1.setCancelable(false);


        RelativeLayout scrollView = (RelativeLayout) sheetView.findViewById(R.id.parent);

        CircularImageView profile = (CircularImageView) sheetView.findViewById(R.id.profile);
        CircularImageView school_profile = (CircularImageView) sheetView.findViewById(R.id.school_profile);

        TextView birth_name = (TextView) sheetView.findViewById(R.id.birth_name);
        TextView school_name = (TextView) sheetView.findViewById(R.id.school_name);
        TextView std_name = (TextView) sheetView.findViewById(R.id.std_name);
        export = (ImageButton) sheetView.findViewById(R.id.export);
        close = (ImageView) sheetView.findViewById(R.id.close);

        // Define an array of strings
        String[] strings = {String.valueOf(R.string.bday1),String.valueOf(R.string.bday2),String.valueOf(R.string.bday3), String.valueOf(R.string.bday4),
                String.valueOf(R.string.bday5),String.valueOf(R.string.bday6),String.valueOf(R.string.bday7),String.valueOf(R.string.bday8),String.valueOf(R.string.bday9)};
        // Generate a random index
        Random random = new Random();
        int randomIndex = random.nextInt(strings.length);
        // Select the string at the random index
        String selectedString = strings[randomIndex];
        // Do something with the selected string (e.g., print it)
        // System.out.println(selectedString);
        birth_name.setText(getResources().getText(Integer.parseInt(selectedString)));

        std_name.setText(std.getName());
        school_name.setText(User.getCurrentUser().getSchoolData().getName());
        //school_name.setText(User.getCurrentUser().getSchoolData().getName()+"\n"+User.getCurrentUser().getSchoolData().getName());

        if (User.getCurrentUser().getSchoolData().getLogo()!=null) {
            Picasso.get().load(User.getCurrentUser().getSchoolData().getLogo()).placeholder(R.color.transparent).into(school_profile);
        } else {
            Picasso.get().load(R.color.transparent).into(school_profile);
        }


        if (std.getImage()!=null) {
            Picasso.get().load(std.getImage()).placeholder(R.color.transparent).into(profile);
        } else {
            Picasso.get().load(R.color.transparent).into(profile);
        }

        final MediaPlayer mp = MediaPlayer.create(this, R.raw.bdaytone);
        mp.start();
        mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                mp.release();
            }
        });

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                close.startAnimation(animation);
                mp.release();
                // saveData();
                mBottomSheetDialog1.dismiss();

            }
        });

        export.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                export.startAnimation(animation);
                export.setVisibility(View.INVISIBLE);
                close.setVisibility(View.INVISIBLE);
//                viewed.setVisibility(View.VISIBLE);

                Bitmap bitmap = getBitmapFromView(scrollView, scrollView.getChildAt(0).getHeight(), scrollView.getChildAt(0).getWidth());
                // Bitmap bitmap = Bitmap.createBitmap(v1.getDrawingCache());
                // v1.setDrawingCacheEnabled(false);
                saveImageToGallery(bitmap,std.getName());

            }
        });

        mBottomSheetDialog1.show();
        Window window = mBottomSheetDialog1.getWindow();
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    }

    private void saveImageToGallery(Bitmap bitmap,String name){

        OutputStream fos;
        String school=User.getCurrentUser().getSchoolData().getName();
        long time= System.currentTimeMillis();
        try{

            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q){

                ContentResolver resolver = getContentResolver();
                ContentValues contentValues =  new ContentValues();
                contentValues.put(MediaStore.MediaColumns.DISPLAY_NAME, name+"_BirthdayCard"+time+ ".jpg");
                contentValues.put(MediaStore.MediaColumns.MIME_TYPE, "image/jpeg");
                contentValues.put(MediaStore.MediaColumns.RELATIVE_PATH, Environment.DIRECTORY_PICTURES + File.separator + school);
                Uri imageUri = resolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, contentValues);

                fos = resolver.openOutputStream(Objects.requireNonNull(imageUri));

                Toast.makeText(this, "Image Saved", Toast.LENGTH_SHORT).show();

            }
            else{
                String imagesDir = Environment.getExternalStoragePublicDirectory(
                        Environment.DIRECTORY_PICTURES).toString() + File.separator + school;

                File file = new File(imagesDir);

                if (!file.exists()) {
                    file.mkdir();
                }

                File image = new File(imagesDir, name+"_BirthdayCard"+time+ ".jpg");
                fos = new FileOutputStream(image);

                Toast.makeText(this, "Image saved to internal!!", Toast.LENGTH_SHORT).show();
                //resetOpTimes();

            }

            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            Objects.requireNonNull(fos);


        }catch(Exception e){

            Toast.makeText(this, "Image not saved \n" + e.toString(), Toast.LENGTH_SHORT).show();
        }
        export.setVisibility(View.VISIBLE);
        close.setVisibility(View.VISIBLE);
    }

    private Bitmap getBitmapFromView(View view, int height, int width) {
        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        Drawable bgDrawable = view.getBackground();
        if (bgDrawable != null)
            bgDrawable.draw(canvas);
        else
            canvas.drawColor(Color.WHITE);
        view.draw(canvas);
        return bitmap;
    }

    private void layoutOneView() {

        // we didnt initialized the layout0 and layoutW



        layout0.setVisibility(View.VISIBLE);
        layoutW.setVisibility(View.GONE);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (logo0.getDisplayedChild() == 0) {
                    logo0.showNext();
                    bdayMsg.setText(getResources().getText(R.string.bday2));
                } else
                    logo0.showPrevious();
            }
        }, 10000) ;  // 10000 = 10 seconds

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (logo1.getDisplayedChild() == 0) {
                    logo1.showNext();
                    bdayMsg.setText(getResources().getText(R.string.bday3));
                } else
                    logo1.showPrevious();
            }
        }, 20000) ;  // we will increase seconds in every viewswitcher... you will understand it in the output


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (logo2.getDisplayedChild() == 0) {
                    logo2.showNext();
                    bdayMsg.setText(getResources().getText(R.string.bday4));
                } else
                    logo2.showPrevious();
            }
        }, 30000) ;  // 10000 = 10 seconds


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (logo3.getDisplayedChild() == 0) {
                    logo3.showNext();
                    bdayMsg.setText(getResources().getText(R.string.bday5));
                } else
                    logo3.showPrevious();
            }
        }, 40000) ;  // 10000 = 10 seconds

        // one layout is not visible in first 40 seconds... now its term to visible to layoutW

        layoutTwoView();
    }

    private void layoutTwoView() {

        layout0.setVisibility(View.GONE);
        layoutW.setVisibility(View.VISIBLE);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (logow.getDisplayedChild() == 0 ){
                    logow.showNext();
                    bdayMsg.setText(getResources().getText(R.string.bday6));
                } else
                    logow.showPrevious();
            }
        }, 10000);
        // we will write 10000, not 50000 becuase the another layoutW is in begin state

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (logox.getDisplayedChild() == 0 ){
                    logox.showNext();
                    bdayMsg.setText(getResources().getText(R.string.bday7));
                } else
                    logox.showPrevious();
            }
        }, 20000);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (logoy.getDisplayedChild() == 0 ){
                    logoy.showNext();
                    bdayMsg.setText(getResources().getText(R.string.bday8));
                } else
                    logoy.showPrevious();
            }
        }, 30000);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (logoz.getDisplayedChild() == 0 ){
                    logoz.showNext();
                    bdayMsg.setText(getResources().getText(R.string.bday9));
                } else
                    logoz.showPrevious();
            }
        }, 40000);

    }

    @Override
    public void onUpdateProfile(View_staff_bean viewStaffBean) {
//        Gson gson  = new Gson();
//        String staffdet = gson.toJson(viewStaffBean,View_staff_bean.class);
//        Intent i = new Intent(StaffBirthdayList.this, UpdateStaff.class);
//        i.putExtra("staffdet",staffdet);
//        startActivity(i);
    }

    @Override
    public void onDeleteCallback(View_staff_bean viewStaffBean) {

    }

}