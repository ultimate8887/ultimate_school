package com.ultimate.ultimatesmartschool.Staff.Fragments;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import com.ultimate.ultimatesmartschool.BeanModule.CasteBean;
import com.ultimate.ultimatesmartschool.BeanModule.ClassBean;
import com.ultimate.ultimatesmartschool.BeanModule.User;
import com.ultimate.ultimatesmartschool.ForCamera.IPickResult;
import com.ultimate.ultimatesmartschool.ForCamera.PickImageDialog;
import com.ultimate.ultimatesmartschool.ForCamera.PickResult;
import com.ultimate.ultimatesmartschool.ForCamera.PickSetup;
import com.ultimate.ultimatesmartschool.Login.LoginActivity;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Staff.Adapteraddstaf_post;
import com.ultimate.ultimatesmartschool.Staff.Adapteraddstff_departmnt;
import com.ultimate.ultimatesmartschool.Staff.CategoryAdapter;
import com.ultimate.ultimatesmartschool.Staff.Deparment_bean;
import com.ultimate.ultimatesmartschool.Staff.Postname_Bean;
import com.ultimate.ultimatesmartschool.Staff.Subjectspin_adapter;
import com.ultimate.ultimatesmartschool.Staff.View_staff_bean;
import com.ultimate.ultimatesmartschool.Student.UpdateStudentDetails;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class UpdateStaff extends AppCompatActivity implements IPickResult {
    @BindView(R.id.parent)
    RelativeLayout parent;
    @BindView(R.id.imgUser)
    ImageView setimage;

    @BindView(R.id.quali_detail)
    TextView qualifction_detail;
    @BindView(R.id.sedtFirstName)
    EditText edtFirstName;
    @BindView(R.id.sedtlastName)
    EditText edtlastName;
    @BindView(R.id.sedtDob)
    EditText edtDob;
    // @BindView(R.id.edtfathernm)EditText fathername;
    @BindView(R.id.edtNationality)
    EditText edtNationality;
    @BindView(R.id.edtBloodgrp)
    EditText edtBloodGroup;
    //@BindView(R.id.edtemailid)EditText emailid;
    @BindView(R.id.edtUserName)
    EditText edtUserName;
    @BindView(R.id.edtPassword)
    EditText edtPassword;
    @BindView(R.id.edtAddress)
    EditText edtAddress;
    @BindView(R.id.experirncee)EditText experience;


    @BindView(R.id.edtCity)
    EditText edtCity;
    @BindView(R.id.edtDoj)EditText eddoj;
    @BindView(R.id.edtpinCode)EditText edtpinCode;
    @BindView(R.id.edtemailid)EditText edtemailid;
    @BindView(R.id.edtdistrct)EditText edtdistrct;
    @BindView(R.id.edtfathernm)EditText edtfathernm;
    @BindView(R.id.edtfather_num)EditText edtfather_num;
    @BindView(R.id.edtState)EditText edtState;
    @BindView(R.id.edtcountry)EditText edtcountry;
    @BindView(R.id.edtEmailpersonalmob)EditText prsnlmobilenum;
    // @BindView(R.id.edtInstitutionsec)EditText institutionsec;
    //@BindView(R.id.edtoffice_num)EditText edtoffice_num;
    @BindView(R.id.edtprv_pckg)EditText edtprv_pckg;
    @BindView(R.id.edtbasic)EditText edtbasic;
    @BindView(R.id.edtremark)EditText edtremark;
//    @BindView(R.id.edtInstitution)EditText edtInstitution;
//    @BindView(R.id.edtposition)EditText edtposition;
//    @BindView(R.id.edtfunc_depart)EditText edtfunction_depart;
//    @BindView(R.id.edtperiod)EditText edtperiod;
//
//    @BindView(R.id.edtInstitutionsec)EditText edtInstitutionsec;
//    @BindView(R.id.edtpositionsec)EditText edtpositionsec;
//    @BindView(R.id.edtfunc_departsec)EditText edtfunction_departsec;
//    @BindView(R.id.edtperiodsec)EditText edtperiodsec;

    @BindView(R.id.txtempid)
    TextView txstaffID;
    @BindView(R.id.spinnerCategory)
    Spinner spinnerCategory;
    //    @BindView(R.id.spinnerClass)
//    Spinner spinnerClass;
    @BindView(R.id.spinnerGender)
    Spinner spinnerGender;
    @BindView(R.id.spinnerDepartment)Spinner spinnerdepartment;
    @BindView(R.id.spinner_post)Spinner spinnerpost;

    @BindView(R.id.spinner_pickup)Spinner spinnerpickup;
    @BindView(R.id.spinner_route_board)Spinner spinnerrouteboard;
    @BindView(R.id.imgBack)ImageView back;
    //    @BindView(R.id.spinner_qualifica)Spinner spinnerqualification;
    @BindView(R.id.checkBoxtrnsport)
    CheckBox checkTransport;

    ArrayList<CasteBean> categorylist = new ArrayList<>();
    ArrayList<ClassBean> classList = new ArrayList<>();
    //    private ArrayList<PickupPointBean> pickupList = new ArrayList<>();
//    private ArrayList<RouteBean> routelist = new ArrayList<>();
//    private ArrayList<Qualification_bean> dataqualification = new ArrayList<>();
    ArrayList<Deparment_bean>department_list=new ArrayList<>();
    ArrayList<Postname_Bean> postlist=new ArrayList<>();
    private Adapteraddstff_departmnt spinneradapt;
    private Adapteraddstaf_post spinerpost;
    private Subjectspin_adapter madapter;
    String[] genderList = {
            "Male",
            "Female",
    };
    private Bitmap userBitmap;
    String selected_value_qualification="";
    View selectedView;
    String gender = "";
    String casteid = "";
    String classid = "";
    String subid=" ";
    String pickupid = "", routeid = "";
    public String birthdate = "";
    public String joiningdate="";
    private String departmentid = "";
    private String post_id = "";
    // SpinnerAdapterqwalfc adapterss;

    EditText specialization;
    EditText exam_passed;
    EditText marks_obtain;
    EditText board_univer;
    EditText year_session;
    int flag=0;
    RadioGroup martial_status;
    RadioGroup proffsional_status;
    String radiobuttonvalue;
    String professional_detail;

    View_staff_bean user;
    String checkboxvalue;
    CheckBox uploadassign_prepare,mark_attendance,uploadexam,obtainmrks;
    //StringBuffer stringBuffer;
    Dialog staffDialog;
    String all="all";
    String abcd="";
    @BindView(R.id.edtExamPasses)EditText edtExamPasses;
    @BindView(R.id.edtSepecialization)EditText edtSepecialization;
    @BindView(R.id.edtmarksobtain)EditText edtmarksobtain;
    @BindView(R.id.edtuniversityboard)EditText edtuniversityboard;
    @BindView(R.id.edtyearsession)EditText edtyearsession;

    @SuppressLint("MissingInflatedId")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_staff);
        ButterKnife.bind(this);

        if(getIntent()!= null && getIntent().hasExtra("staffdet")){
            Gson gson = new Gson();
            user = gson.fromJson(getIntent().getStringExtra("staffdet"),View_staff_bean.class);
            setUpUserData();
        }else {
            return;
        }

        fetchdepartment();
        fetchCategorylistt();
        fetchstaffid();

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        martial_status = (RadioGroup) findViewById(R.id.radimartialstts);
        proffsional_status=(RadioGroup)findViewById(R.id.radioprofess);

        martial_status.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch(checkedId){
                    case R.id.radiomarried:
                        radiobuttonvalue="married";
                        break;
                    case R.id.radiounmarried:

                        radiobuttonvalue="unmarried";

                        break;

                }
            }
        });



        proffsional_status.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                switch(checkedId){

                    case R.id.radioteach:
                        professional_detail = "teaching";
                        break;

                    case R.id.radiononteach:
                        professional_detail = "nonteaching";

                        break;
                    case R.id.radioriception:
                        professional_detail = "reception";

                        // do operations specific to this selection
                        break;
                    case R.id.radiosecurity:
                        professional_detail = "watchmen";

                        // do operations specific to this selection
                        break;

                    case R.id.radiowarden:
                        professional_detail = "warden";
                        break;


                }
                Log.e("myresult", String.valueOf(professional_detail));
            }
        });


        ArrayAdapter<String> genderAdapter = new ArrayAdapter<String>(this, android.
                R.layout.simple_spinner_dropdown_item, genderList);
        spinnerGender.setAdapter(genderAdapter);
        spinnerGender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                gender = genderList[i];
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void setUpUserData() {

        // txtName.setText("Name: "+user.getName());
        birthdate=user.getDateofbirth();
        professional_detail=user.getTeach_nonteach();
        post_id=user.getSt_post();
        departmentid=user.getDepart_id();
        gender=user.getGender();
        casteid=user.getSt_category();

        if(user.getName()!=null){
            edtFirstName.setText(user.getName());
        }
//
//
//        params.put("gender", gender);

        if (birthdate!=null){
            edtDob.setText(birthdate);
        }

//
//        if (casteid != null && !casteid.isEmpty())
//            params.put("category", casteid);

        if (user.getEmail()!=null){
            edtemailid.setText(user.getEmail());
        }


        if (user.getBloodgroup()!=null){
            edtBloodGroup.setText(user.getBloodgroup());
        }

        if (user.getAddress()!=null){
            edtAddress.setText(user.getAddress());
        }
//
//        params.put("country", edtcountry.getText().toString());
//        params.put("city", edtCity.getText().toString());

        if (user.getSt_prcity()!=null){
            edtCity.setText(user.getSt_prcity());
        }

        if (user.getSt_prcountry()!=null){
            edtcountry.setText(user.getSt_prcountry());
        }

        if (user.getSt_prstate()!=null){
            edtState.setText(user.getSt_prstate());
        }

        if (user.getSt_prpincode()!=null){
            edtpinCode.setText(user.getSt_prpincode());
        }

        if (user.getSt_dist()!=null){
            edtdistrct.setText(user.getSt_dist());
        }

        if (user.getFatherphonno()!=null){
            edtfather_num.setText(user.getFatherphonno());
        }

//
        if(user.getFathername()!=null){
            edtfathernm.setText(user.getFathername());
        }

        if(user.getPhonno()!=null){
            prsnlmobilenum.setText(user.getPhonno());
        }


        switch (professional_detail) {
            case "teaching":
                RadioButton radioButtonTeach = findViewById(R.id.radioteach);
                radioButtonTeach.setChecked(true);
                break;
            case "nonteaching":
                RadioButton radioButtonNonTeach = findViewById(R.id.radiononteach);
                radioButtonNonTeach.setChecked(true);
                break;
            case "reception":
                RadioButton radioButtonReceptionist = findViewById(R.id.radioriception);
                radioButtonReceptionist.setChecked(true);
                break;
            case "watchmen":
                RadioButton radioButtonWatchmen = findViewById(R.id.radiosecurity);
                radioButtonWatchmen.setChecked(true);
                break;
            case "warden":
                RadioButton radioButtonWarden = findViewById(R.id.radiowarden);
                radioButtonWarden.setChecked(true);
                break;
        }


//        txtempid.setText("Employee Id: "+user.getId());
//        if (user.getDesignation()!=null && user.getPost()!=null ) {
//            txtdepart.setText("Designation: "+user.getDesignation()+"("+user.getPost()+")");
//        }
//        if(user.getBloodgroup()!=null) {
//            txtBG.setText("Blood Group" + user.getBloodgroup());
//        }else {
//            txtBG.setText("Not Mentioned");
//        }
//        if (user.getEmail()!=null){
//            txtEmail.setText("Email: "+user.getEmail());
//        }else {
//            txtEmail.setText("Not Mentioned");
//        }
//        txtEmail.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                String email=user.getEmail();
//                Intent intent = new Intent(Intent.ACTION_SENDTO); // it's not ACTION_SEND
//                intent.setType("text/plain");
//                intent.putExtra(Intent.EXTRA_SUBJECT, "Subject of email");
//                intent.putExtra(Intent.EXTRA_TEXT, "Body of email");
//                intent.setData(Uri.parse("mailto:"+email)); // or just "mailto:" for blank
//                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK); // this will make such that when user returns to your app, your app is displayed, instead of the email app.
//                startActivity(intent);
//            }
//        });
//
//        if (user.getPhonno()!=null){
//            txtContact.setText("Mobile: "+user.getPhonno());
//        }else {
//            txtContact.setText("Not Mentioned");
//        }
//        txtContact.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                String phoneNo=user.getPhonno();
//                Intent intent = new Intent(Intent.ACTION_DIAL);
//                intent.setData(Uri.parse("tel:"+ phoneNo));
//                startActivity(intent);
//            }
//        });
//
//        if (user.getGender()!=null){
//            txtGender.setText(user.getGender());
//        }else {
//            txtGender.setText("Not Mentioned");
//        }
//        if (user.getAddress()!=null){
//            txtAddress.setText("Address: "+user.getAddress());
//        }else {
//            txtAddress.setText("Not Mentioned");
//        }
//        if (user.getDateofbirth()!=null){
//            txtDOB.setText("DOB: "+ Utils.getDateFormated(user.getDateofbirth()));
//        }else {
//            txtDOB.setText("Not Mentioned");
//        }
//        if (user.getImage() != null)
//            Picasso.with(this).load(user.getImage()).placeholder(getResources().getDrawable(R.drawable.boy)).into(setimage);

        if (user.getGender().equalsIgnoreCase("male")) {
            if (user.getImage() != null) {
                //  Picasso.with(context).load(viewstafflist.get(position).getImage()).placeholder(context.getResources().getDrawable(R.drawable.man)).into(holder.image);
                Utils.progressImg(user.getImage(), setimage, this);
            } else {
                // Picasso.with(context).load(String.valueOf(context.getResources().getDrawable(R.drawable.staffimg))).into(holder.image);
                Picasso.get().load(R.drawable.boy).into(setimage);

            }
        } else {
            if (user.getImage() != null) {
                //  Picasso.with(context).load(viewstafflist.get(position).getImage()).placeholder(context.getResources().getDrawable(R.drawable.man)).into(holder.image);
                Utils.progressImg(user.getImage(), setimage, this);
            } else {
                // Picasso.with(context).load(String.valueOf(context.getResources().getDrawable(R.drawable.staffimg))).into(holder.image);
                Picasso.get().load(R.drawable.female_teacher).into(setimage);

            }
        }
    }


    private void fetchCategorylistt() {

        HashMap<String, String> params = new HashMap<String, String>();
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.CASTELIST_URL, castesapiCallback, this, params);
    }
    ApiHandler.ApiCallback castesapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            if (error == null) {
                try {
                    categorylist = CasteBean.parseCasteArray(jsonObject.getJSONArray(Constants.CASTEDATA));
                    CategoryAdapter adapter = new CategoryAdapter(UpdateStaff.this, categorylist);
                    spinnerCategory.setAdapter(adapter);
                    spinnerCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if (i > 0) {
                                casteid = categorylist.get(i - 1).getCaste_id();

                                Log.e("castid",String.valueOf(casteid));
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                    int i = 0;
                    String id=user.getSt_category();
                    for ( CasteBean obj : categorylist) {
                        if (obj.getCaste_id().equalsIgnoreCase(id)) {
                            spinnerCategory.setSelection(i + 1);
                            break;
                        }
                        i++;
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Utils.showSnackBar(error.getMessage(), parent);
            }
        }
    };
    private void fetchstaffid() {

        HashMap<String, String> params = new HashMap<String, String>();
        ApiHandler.apiHit(Request.Method.GET, Constants.getBaseURL() + Constants.STAFF_ID, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                if (error == null) {
                    try {
                        int staffid = jsonObject.getInt("staff_id");
                        txstaffID.setText("Staff id: " + staffid);
                        //txtRegNo.setText(User.getCurrentUser().getClass_id());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    Utils.showSnackBar(error.getMessage(), parent);
                }
            }
        }, this, params);

    }

    private boolean checkValid() {
        boolean valid = true;
        String errorMsg = null;
        if (exam_passed.getText().toString().trim().length() <= 0) {
            valid = false;
            errorMsg = "Please enter exam passed";
        } else if (specialization.getText().toString().trim().length() <= 0) {
            valid = false;
            errorMsg = "Please enter specelization";
        }else if (marks_obtain.getText().toString().trim().length() <= 0) {
            valid = false;
            errorMsg = "Please enter marks obtained";
        } else if (board_univer.getText().toString().trim().length() <= 0) {
            valid = false;
            errorMsg = "Please enter Board/University";
        }else if (year_session.getText().toString().trim().length() <= 0) {
            valid = false;
            errorMsg = "Please enter year/session";
        }

        if (!valid) {
            Utils.showSnackBar(errorMsg, parent);
        }
        return valid;
    }
    @RequiresApi(api = Build.VERSION_CODES.M)
    @OnClick(R.id.expandedImage)
    public void fetchUserImage() {
        pick_img();
    }
    @RequiresApi(api = Build.VERSION_CODES.M)
    @SuppressLint("ResourceType")
    private void pick_img() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.UPSIDE_DOWN_CAKE) { // Android 14+ (API 34)
            onImageViewClick();

        } else if (Build.VERSION.SDK_INT > Build.VERSION_CODES.S) {
                if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_MEDIA_IMAGES)
                        != PackageManager.PERMISSION_GRANTED) {

                    if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_MEDIA_IMAGES)) {
                        Toast.makeText(this, "Please provide storage permission from app settings", Toast.LENGTH_LONG).show();
                    } else {

                        ActivityCompat.requestPermissions(this,
                                new String[]{Manifest.permission.READ_MEDIA_IMAGES},
                                1);
                    }
                } else {

                    onImageViewClick();
                }

            } else {
                if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {

                    if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                        Toast.makeText(this, "Please provide storage permission from app settings", Toast.LENGTH_LONG).show();
                    } else {

                        ActivityCompat.requestPermissions(this,
                                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                                1);
                    }
                } else {
                    onImageViewClick();

                }
        }
    }

    private void onImageViewClick() {
        //type = 1;
        PickSetup setup = new PickSetup();
        // super.customize(setup);
        PickImageDialog.build(setup)
                //.setOnClick(this)
                .show(this);
    }

    @Override
    public void onPickResult(PickResult r) {
        if (r.getError() == null) {
            //If you want the Bitmap.
            setimage.setImageBitmap(r.getBitmap());
            userBitmap = r.getBitmap();
            setimage.setVisibility(View.VISIBLE);
            //  encoded = Utility.encodeToBase64(profileBitmap, Bitmap.CompressFormat.JPEG, 50);
            //r.getPath();
        } else {
            //Handle possible errors
            //TODO: do what you have to do with r.getError();
            Toast.makeText(this, r.getError().getMessage(), Toast.LENGTH_LONG).show();
        }


    }


//    @RequiresApi(api = Build.VERSION_CODES.M)
//    @Override
//    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        if (resultCode == Activity.RESULT_OK) {
//            switch (requestCode) {
//                case CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE:
//                    Uri imageUri = CropImage.getPickImageResultUri(UpdateStaff.this, data);
//                    CropImage.activity(imageUri).setCropShape(CropImageView.CropShape.RECTANGLE).setAspectRatio(1, 1)
//                            .start(UpdateStaff.this);
//                    break;
//
//                case CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE:
//                    CropImage.ActivityResult result = CropImage.getActivityResult(data);
//                    Uri resultUri = result.getUri();
//                    userBitmap = Utils.getBitmapFromUri(UpdateStaff.this, resultUri, 2048);
//                    setimage.setImageBitmap(userBitmap);
//
//            }
//        }
//    }


    @OnClick(R.id.imgCal)
    public void fetchDateOfBirth() {

        java.util.Calendar cal = java.util.Calendar.getInstance(TimeZone.getDefault()); // Get current date
        DatePickerDialog datePicker = new DatePickerDialog(this,
                R.style.MyDatePickerDialogTheme, ondate,
                cal.get(java.util.Calendar.YEAR),
                cal.get(java.util.Calendar.MONTH),
                cal.get(java.util.Calendar.DAY_OF_MONTH));
        datePicker.getDatePicker().setMaxDate(new Date().getTime());
        datePicker.setCancelable(false);
        datePicker.show();
    }

    DatePickerDialog.OnDateSetListener ondate = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

            java.util.Calendar calendar = java.util.Calendar.getInstance(TimeZone.getDefault());
            calendar.set(year, monthOfYear, dayOfMonth);
            Date dbirthdate = calendar.getTime();
            Calendar value_datePicker = calendar;
            SimpleDateFormat fmtOut = new SimpleDateFormat("yyyy-MM-dd");
            String dateString = fmtOut.format(dbirthdate);
            birthdate=dateString;
            edtDob.setText(dateString);
        }
    };
    //
//
    @OnClick(R.id.imgCalen)
    public void fetchDateOfJoining() {
        java.util.Calendar cal = java.util.Calendar.getInstance(TimeZone.getDefault()); // Get current date
        DatePickerDialog datePicker = new DatePickerDialog(this,
                R.style.MyDatePickerDialogTheme, ondates,
                cal.get(java.util.Calendar.YEAR),
                cal.get(java.util.Calendar.MONTH),
                cal.get(java.util.Calendar.DAY_OF_MONTH));
        datePicker.getDatePicker().setMaxDate(new Date().getTime());
        datePicker.setCancelable(false);
        datePicker.show();
    }
    //
    DatePickerDialog.OnDateSetListener ondates = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

            java.util.Calendar calendar = java.util.Calendar.getInstance(TimeZone.getDefault());
            calendar.set(year, monthOfYear, dayOfMonth);
            Date dbofjoin = calendar.getTime();
            Calendar value_datePicker = calendar;
            SimpleDateFormat fmtOut = new SimpleDateFormat("yyyy-MM-dd");
            String dateString = fmtOut.format(dbofjoin);
            joiningdate=dateString;
            eddoj.setText(dateString);
        }
    };




    @OnClick(R.id.btnSubmtstaff)
    public void clickSubmit() {
        if (checkValidsec()) {


            ErpProgress.showProgressBar(this, "Please wait");
            HashMap<String, String> params = new HashMap<String, String>();
            params.put("s_id", user.getId());
            params.put("first_name", edtFirstName.getText().toString());
            params.put("dob", String.valueOf(birthdate));
            params.put("nationality", edtNationality.getText().toString());
            if (casteid != null && !casteid.isEmpty())
                params.put("category", casteid);
            params.put("department",departmentid);
            params.put("post",post_id);
            params.put("bg", edtBloodGroup.getText().toString());
            params.put("teach_nonteach",professional_detail);
            params.put("address", edtAddress.getText().toString());
            params.put("country", edtcountry.getText().toString());
            params.put("city", edtCity.getText().toString());
            params.put("state", edtState.getText().toString());
            params.put("pincode", edtpinCode.getText().toString());
            params.put("district", edtdistrct.getText().toString());
            params.put("email", edtemailid.getText().toString());
            params.put("mobno", prsnlmobilenum.getText().toString());
            params.put("father_name", edtfathernm.getText().toString());
            params.put("father_mob_number", edtfather_num.getText().toString());

            if (userBitmap != null) {
                String encoded = Utils.encodeToBase64(userBitmap, Bitmap.CompressFormat.JPEG, 90);
                encoded = String.format("data:image/jpeg;base64,%s", encoded);
                params.put("image", encoded);
            }
            ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.UPDATE_STAFFURL, apiCallbackaddstaff, this, params);
        }
    }
    ApiHandler.ApiCallback apiCallbackaddstaff = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            if (error == null) {
                try {
                    Toast.makeText(UpdateStaff.this, jsonObject.getString("msg"), Toast.LENGTH_SHORT).show();
                    finish();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                // Toast.makeText(Add_staff.this, jsonObject.getString("msg"), Toast.LENGTH_SHORT).show();
                Log.e("errorss", error.getMessage() + "");
                Toast.makeText(UpdateStaff.this,"wrong",Toast.LENGTH_LONG).show();
                // showSnackBar(error.getMessage());
                Utils.showSnackBar(error.getMessage(), parent);
            }
        }
    };

    public void showSnackBar(String errorMsg) {


        Toast.makeText(getApplicationContext(),errorMsg,Toast.LENGTH_LONG).show();

    }
    private void fetchdepartment() {
        HashMap<String,String> params=new HashMap<>();
        ApiHandler.apiHit(Request.Method.GET, Constants.getBaseURL() + Constants.deparmrnt, apiCallback, this, params);
    }
    ApiHandler.ApiCallback apiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            // txtupload.setVisibility(View.VISIBLE);
            if (error == null) {
                try {
                    if (department_list != null) {
                        department_list.clear();
                    }
                    JSONArray jsonArray = jsonObject.getJSONArray("department_data");
                    department_list = Deparment_bean.parseDepartmentArray(jsonArray);
                    spinneradapt= new Adapteraddstff_departmnt(UpdateStaff.this,department_list);
                    spinnerdepartment.setAdapter(spinneradapt);
                    spinnerdepartment.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if (i > 0) {
                                departmentid = department_list.get(i-1 ).getId();
                                //fetchSubject(classid);
                                fetchPostname(departmentid);
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                    int i = 0;
                    String id=user.getDepart_id();
                    for ( Deparment_bean obj : department_list) {
                        if (obj.getId().equalsIgnoreCase(id)) {
                            spinnerdepartment.setSelection(i + 1);
                            break;
                        }
                        i++;
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Utils.showSnackBar(error.getMessage(), parent);
                if (error.getStatusCode() == 405) {
                    User.logout();
                    startActivity(new Intent(UpdateStaff.this, LoginActivity.class));
                    finish();
                }
            }
        }
    };

    private void fetchPostname(String departmentid) {

        HashMap<String,String> params=new HashMap<>();
        ApiHandler.apiHit(Request.Method.GET, Constants.getBaseURL() + Constants.Post+Constants.dpartment_id+departmentid, apiCallbackpost, this, params);

    }
    ApiHandler.ApiCallback apiCallbackpost = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            if (error == null) {
                try {
                    if (postlist != null) {
                        postlist.clear();
                    }
                    JSONArray jsonArray = jsonObject.getJSONArray("post_data");
                    postlist = Postname_Bean.parsePostnsmeArray(jsonArray);
                    spinerpost = new Adapteraddstaf_post(UpdateStaff.this,postlist);
                    spinnerpost.setAdapter(spinerpost);
                    spinnerpost.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if (i > 0) {
                                post_id = postlist.get(i-1 ).getId();
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                    int i = 0;
                    String id=user.getSt_post();
                    for ( Postname_Bean obj : postlist) {
                        if (obj.getId().equalsIgnoreCase(id)) {
                            spinnerpost.setSelection(i + 1);
                            break;
                        }
                        i++;
                    }

                } catch (JSONException e) {

                    e.printStackTrace();

                }
            } else {

                Utils.showSnackBar(error.getMessage(), parent);
            }
        }
    };




    private boolean checkValidsec() {
        boolean valid = true;
        String errorMsg = null;
        if (edtFirstName.getText().toString().trim().length() <= 0) {
            valid = false;
            errorMsg = "Please enter firstname";
        } else if (gender.isEmpty()) {
            valid = false;
            errorMsg = "Please select gender";
        }
        else if (birthdate == null) {
            valid = false;
            errorMsg = "Please enter date of birth";
        }

//        else if (edtUserName.getText().toString().trim().length() <= 0) {
//            valid = false;
//            errorMsg = "Please enter username";
//        }else if (edtPassword.getText().toString().trim().length() <= 0) {
//            valid = false;
//            errorMsg = "Please enter password";
//        }
        else if (proffsional_status.getCheckedRadioButtonId()==-1) {
            valid = false;
            errorMsg = "Please select your professional";
        }
        else if (departmentid.isEmpty()||departmentid=="") {
            valid = false;
            errorMsg = "Please enter department";
        }
        else if (post_id.isEmpty()||post_id==" ") {
            valid = false;
            errorMsg = "Please enter post";
        }

        else if (prsnlmobilenum.getText().toString().trim().length() <= 0) {
            valid = false;
            errorMsg = "Please enter personal mobile number";
        }


        if (!valid) {
            Utils.showSnackBar(errorMsg, parent);
        }
        return valid;
    }



}