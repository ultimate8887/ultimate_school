package com.ultimate.ultimatesmartschool.Staff;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;
import android.widget.TextView;

import com.google.android.material.tabs.TabLayout;
import com.ultimate.ultimatesmartschool.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class StaffDepartment extends AppCompatActivity {
    @BindView(R.id.tab_layout)
    TabLayout tabLayout;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_staff_department);
        ButterKnife.bind(this);
        final ViewPager viewPager = (ViewPager) findViewById(R.id.pager);
        tabLayout.setupWithViewPager(viewPager);
        final PagerAdapter adapter = new TabPagerAdapterstaff
                (getSupportFragmentManager());
        viewPager.setAdapter(adapter);
        txtTitle.setText("Department/Post");
    }

    @OnClick(R.id.imgBack)
    public void onBackImgPress() {
        finish();
    }
}
