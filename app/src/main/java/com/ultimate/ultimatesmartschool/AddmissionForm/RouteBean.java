package com.ultimate.ultimatesmartschool.AddmissionForm;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class RouteBean {

    private static String ID = "id";
    private static String NAME = "route_title";
    /**
     * id : 2
     * name : XYZ
     */

    private String id;
    private String name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static ArrayList<RouteBean> parseRouteArray(JSONArray arrayObj) {
        ArrayList list = new ArrayList<RouteBean>();
        try {
            for (int i = 0; i < arrayObj.length(); i++) {
                RouteBean p = parseRouteObject(arrayObj.getJSONObject(i));
                if (p != null) {
                    list.add(p);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public static RouteBean parseRouteObject(JSONObject jsonObject) {
        RouteBean casteObj = new RouteBean();
        try {
            if (jsonObject.has(ID)) {
                casteObj.setId(jsonObject.getString(ID));
            }
            if (jsonObject.has(NAME) && !jsonObject.getString(NAME).isEmpty() && !jsonObject.getString(NAME).equalsIgnoreCase("null")) {
                casteObj.setName(jsonObject.getString(NAME));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return casteObj;
    }
}
