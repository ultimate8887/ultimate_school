package com.ultimate.ultimatesmartschool.AddmissionForm;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.ultimate.ultimatesmartschool.R;

import java.util.ArrayList;

public class FeeAdapter extends BaseAdapter {
    private final ArrayList<FeeCatBean> sessionList;
    Context context;
    LayoutInflater inflter;

    public FeeAdapter(Context applicationContext, ArrayList<FeeCatBean> sessionList) {
        this.context = applicationContext;
        this.sessionList = sessionList;
        inflter = (LayoutInflater.from(applicationContext));
    }

    @Override
    public int getCount() {
        return sessionList.size() + 1;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = inflter.inflate(R.layout.spinner_lyt_msg, null);
        TextView label = (TextView) view.findViewById(R.id.txtText);
        if (i == 0) {
            // Default selected Spinner item
            label.setText("Select Fee Category");
        } else {
            FeeCatBean sessionObj = sessionList.get(i - 1);
            label.setText(sessionObj.getFeecategory());
        }
        return view;
    }
}
