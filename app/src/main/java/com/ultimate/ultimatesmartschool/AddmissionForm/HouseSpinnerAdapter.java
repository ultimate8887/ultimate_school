package com.ultimate.ultimatesmartschool.AddmissionForm;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.ultimate.ultimatesmartschool.BeanModule.CommonBean;
import com.ultimate.ultimatesmartschool.R;

import java.util.ArrayList;

public class HouseSpinnerAdapter extends BaseAdapter {
    private final ArrayList<CommonBean> casteList;
    Context context;
    LayoutInflater inflter;
    int val=0;

    public HouseSpinnerAdapter(Context applicationContext, ArrayList<CommonBean> casteList,int val) {
        this.context = applicationContext;
        this.casteList = casteList;
        this.val = val;
        inflter = (LayoutInflater.from(applicationContext));
    }

    @Override
    public int getCount() {
        return casteList.size() + 1;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        view = inflter.inflate(R.layout.spinner_lyt_group, null);
        TextView label = (TextView) view.findViewById(R.id.txtText);
        if (i == 0) {
            // Default selected Spinner item
            if (val==1) {
                label.setText("All");
            }else {
                label.setText("Select House");
            }
        } else {
            CommonBean classObj = casteList.get(i - 1);
            label.setText(classObj.getName());
        }

        return view;
    }
}
