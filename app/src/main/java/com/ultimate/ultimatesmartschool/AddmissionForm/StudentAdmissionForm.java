package com.ultimate.ultimatesmartschool.AddmissionForm;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputLayout;
import com.ultimate.ultimatesmartschool.BeanModule.CasteBean;
import com.ultimate.ultimatesmartschool.BeanModule.ClassBean;
import com.ultimate.ultimatesmartschool.BeanModule.CommonBean;
import com.ultimate.ultimatesmartschool.BeanModule.SectionBean;
import com.ultimate.ultimatesmartschool.BeanModule.User;
import com.ultimate.ultimatesmartschool.ForCamera.IPickResult;
import com.ultimate.ultimatesmartschool.ForCamera.PickImageDialog;
import com.ultimate.ultimatesmartschool.ForCamera.PickResult;
import com.ultimate.ultimatesmartschool.ForCamera.PickSetup;
import com.ultimate.ultimatesmartschool.Homework.SectionnewAdapter;
import com.ultimate.ultimatesmartschool.Login.LoginActivity;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Staff.CategoryAdapter;
import com.ultimate.ultimatesmartschool.StudentAttendance.ClassAdapterAttend;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;
import com.ultimate.ultimatesmartschool.Utility.FileUtils;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class StudentAdmissionForm extends AppCompatActivity implements IPickResult {
    @BindView(R.id.spinnerFeeCategory)
    Spinner spinnerFeeCategory;
    @BindView(R.id.spinnerPickUpPoint)
    Spinner spinnerPickUpPoint;

    @BindView(R.id.spinnerHouse)
    Spinner spinnerHouse;
    @BindView(R.id.edtRoute)
    EditText edtRoute;
    @BindView(R.id.spinnerCategory)
    Spinner spinnerCategory;
    @BindView(R.id.spinnerClass)
    Spinner spinnerClass;
    @BindView(R.id.spinnerGender)
    Spinner spinnerGender;
    @BindView(R.id.spinnerSession)
    Spinner spinnerSession;
    @BindView(R.id.spinnerTitle)
    Spinner spinnerTitle;

    @BindView(R.id.edtFirstName)
    EditText edtFirstName;
    @BindView(R.id.edtlastName)
    EditText edtlastName;
    @BindView(R.id.edtDob)
    EditText edtDob;
    @BindView(R.id.edtReligion)
    EditText edtReligion;
    @BindView(R.id.edtNationality)
    EditText edtNationality;
    @BindView(R.id.edtBloodGroup)
    EditText edtBloodGroup;
    @BindView(R.id.edtAadharNo)
    EditText edtAadharNo;
    @BindView(R.id.edtUserName)
    EditText edtUserName;
    @BindView(R.id.edtPassword)
    EditText edtPassword;
    @BindView(R.id.edtAddress)
    EditText edtAddress;
    @BindView(R.id.edtArea)
    EditText edtArea;
    @BindView(R.id.edtCity)
    EditText edtCity;
    @BindView(R.id.edtState)
    EditText edtState;
    @BindView(R.id.edtpinCode)
    EditText edtpinCode;
    @BindView(R.id.edtEmail)
    EditText edtEmail;
    @BindView(R.id.edtPhone)
    EditText edtPhone;
    @BindView(R.id.edtFatherName)
    EditText edtFatherName;
    @BindView(R.id.edtFatherAge)
    EditText edtFatherAge;
    @BindView(R.id.edtFatherEduQuali)
    EditText edtFatherEduQuali;
    @BindView(R.id.edtFatherOccupation)
    EditText edtFatherOccupation;
    @BindView(R.id.edtFatherPhone)
    EditText edtFatherPhone;
    @BindView(R.id.edtMotherName)
    EditText edtMotherName;
    @BindView(R.id.edtMotherAge)
    EditText edtMotherAge;
    @BindView(R.id.edtMotherEduQuali)
    EditText edtMotherEduQuali;
    @BindView(R.id.edtMotherOccupation)
    EditText edtMotherOccupation;
    @BindView(R.id.edtMotherPhone)
    EditText edtMotherPhone;
    @BindView(R.id.edt1stChildAdmiNo)
    EditText edt1stChildAdmiNo;
    @BindView(R.id.edt2ndChildAdmiNo)
    EditText edt2ndChildAdmiNo;
    @BindView(R.id.parent)
    RelativeLayout parent;
    @BindView(R.id.edtOtherTransport)
    EditText edtOtherTransport;
    @BindView(R.id.edtMedium)
    EditText edtMedium;

    private ArrayList<CommonBean> groupList = new ArrayList<>();
    @BindView(R.id.checkTransport)
    CheckBox checkTransport;
    @BindView(R.id.txtRegNo)
    EditText txtRegNo;
    @BindView(R.id.inpOtherTransport)
    TextInputLayout inpOtherTransport;
    @BindView(R.id.lytSchoolTransport)
    LinearLayout lytSchoolTransport;
    @BindView(R.id.imgUser)
    ImageView imgUser;
    String[] list = {
            "Master",
            "Miss.",
    };

    String[] genderList = {
            "Male",
            "Female",
    };
    public String feeid = "",houseid="";
    private SessionBean session = null;
    String classid = "";
    public String birthdate = "";
    String gender = "";
    String title = "";
    String casteid = "";
    String pickupid = "", routeid = "";
    ArrayList<CasteBean> categorylist = new ArrayList<>();
    ArrayList<ClassBean> classList = new ArrayList<>();
    private ArrayList<PickupPointBean> pickupList = new ArrayList<>();
    private ArrayList<RouteBean> routelist = new ArrayList<>();
    private ArrayList<SessionBean> sessionlist = new ArrayList<>();
    private ArrayList<FeeCatBean> feeList = new ArrayList<>();
    private Bitmap userBitmap;
    ArrayList<VehicleBean> arrayListveh=new ArrayList<>();
    @BindView(R.id.spinevehiclemorn)Spinner spinevehiclemorn;
    @BindView(R.id.spinevehicleeveni)Spinner spinevehicleeveni;
    String vehicle_id_morning,vehicle_id_evening;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.spinnerSection)
    Spinner spinnersection;
    ArrayList<SectionBean> sectionList = new ArrayList<>();

    private int type;
    private Bitmap hwbitmap,hwbitmap10,hwbitmap12,hwbitmapschlcert,hwbitmapschltrans,hwbitmapgrad;
    @BindView(R.id.imageView6)ImageView imageView6;
    @BindView(R.id.imageView610th)ImageView imageView610th;
    @BindView(R.id.imageView612th)ImageView imageView612th;
    @BindView(R.id.imageView6scholcert)ImageView imageView6scholcert;
    @BindView(R.id.imageView6scoltc)ImageView imageView6scoltc;
    @BindView(R.id.imageView6grad)ImageView imageView6grad;
    @BindView(R.id.doc_lyt)LinearLayout doc_lyt;
    String sectionid="";
    String sectionname="";
    private final int LOAD_FILE_RESULTS = 1000;
    private final int LOAD_FILE_RESULTS10 = 1010;
    private final int LOAD_FILE_RESULTS12 = 1012;
    private final int LOAD_FILE_RESULTSsc = 1013;
    private final int LOAD_FILE_RESULTSstc = 1014;
    private final int LOAD_FILE_RESULTSgc = 1015;
    String fileBase64 = null;
    String fileBase6410th = null;
    String fileBase6413th = null;
    String fileBase64scth = null;
    String fileBase64tcth = null;
    String fileBase64grad = null;
    String fileBase65 = null;
    @BindView(R.id.textnote)TextView textnote;
    @BindView(R.id.textnote10th)TextView textnote10th;
    @BindView(R.id.textnote12th)TextView textnote12th;
    @BindView(R.id.textnotescholcert)TextView textnotescholcert;
    @BindView(R.id.textnotescoltc)TextView textnotescoltc;
    @BindView(R.id.textnotegrad)TextView textnotegrad;
    @BindView(R.id.addpdf)TextView addpdf;
    @BindView(R.id.addimage)TextView addimage;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_admission_form);
        ButterKnife.bind(this);
        txtTitle.setText("Admission Form");
        //Spinner View
        fetchHouse();
        fetchCategory();
        fetchClass();
        fetchPickupPoint();
        fetchRoute();
        fetchVicledetailsMorn();
        fetchVicledetailsEven();
        fetchSession();
        fetchRegNo();
        fetchFeeList();
        fetchSection();

        if (User.getCurrentUser().getSchoolData().getFi_school_id().equalsIgnoreCase("XYZGGSMT")){
            doc_lyt.setVisibility(View.VISIBLE);
        }else{
            doc_lyt.setVisibility(View.GONE);
        }

        ArrayAdapter<String> genderAdapter = new ArrayAdapter<String>(this, android.
                R.layout.simple_spinner_dropdown_item, genderList);
        spinnerGender.setAdapter(genderAdapter);
        spinnerGender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                gender = genderList[i];
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.
                R.layout.simple_spinner_dropdown_item, list);
        spinnerTitle.setAdapter(adapter);
        spinnerTitle.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                title = list[i];
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        spinnerSession.setAdapter(adapter);
        //Transport check
        checkTransport.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    inpOtherTransport.setVisibility(View.GONE);
                    lytSchoolTransport.setVisibility(View.VISIBLE);
                } else {
                    lytSchoolTransport.setVisibility(View.GONE);
                    inpOtherTransport.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    private void fetchHouse() {
        ErpProgress.showProgressBar(this, "Please wait...");
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("check","house");
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.GROUP_LIST_URL, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                ErpProgress.cancelProgressBar();
                if (error == null) {

                    try {
                        groupList.clear();
                        groupList = CommonBean.parseCommonArray(jsonObject.getJSONArray(Constants.GROUP_DATA));
                        final HouseSpinnerAdapter adapter = new HouseSpinnerAdapter(StudentAdmissionForm.this, groupList,0);
                        spinnerHouse.setAdapter(adapter);
                        spinnerHouse.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                if (position > 0) {
                                    houseid = groupList.get(position - 1).getId();
                                } else {
                                    houseid="";
                                }
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    Utils.showSnackBar(error.getMessage(), parent);
                }
            }
        }, this, params);
    }


    private void fetchSection() {
        HashMap<String, String> params = new HashMap<String, String>();
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.SECTIONLIST_URL, sectionapiCallback, this, params);
    }

    ApiHandler.ApiCallback sectionapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            if (error == null) {
                try {
                    sectionList = SectionBean.parseCommonArray(jsonObject.getJSONArray("hostelroom_data"));
                    SectionnewAdapter adapter = new SectionnewAdapter(StudentAdmissionForm.this, sectionList);
                    spinnersection.setAdapter(adapter);
                    spinnersection.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if (i > 0) {
                                sectionid = sectionList.get(i - 1).getSection_id();
                                sectionname = sectionList.get(i - 1).getSection_name();
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Utils.showSnackBar(error.getMessage(), parent);
                if (error.getStatusCode() == 405) {
                    User.logout();
                    startActivity(new Intent(StudentAdmissionForm.this, LoginActivity.class));
                    finish();
                }
            }
        }
    };



    @OnClick(R.id.imgBack)
    public void onback() {
        finish();
    }

    private void fetchVicledetailsMorn() {
        HashMap<String,String> params=new HashMap<>();
        ApiHandler.apiHit(Request.Method.GET, Constants.getBaseURL() + Constants.VEHICLE_URL, apiCallBackvrh, this,null);
    }

    ApiHandler.ApiCallback apiCallBackvrh=new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            if (error==null){
                try{


                    arrayListveh= VehicleBean.parseVehicleArray(jsonObject.getJSONArray("vehical_data"));
                    VehicledetailSpinerAdap adap1=new VehicledetailSpinerAdap(StudentAdmissionForm.this,arrayListveh);
                    spinevehiclemorn.setAdapter(adap1);
                    spinevehiclemorn.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if (i > 0) {

                                vehicle_id_morning=arrayListveh.get(i-1).getEs_transportid();

                            }
                        }
                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {
                        }
                    });



                } catch (JSONException e) {
                    e.printStackTrace();
                }

            } else {
                Toast.makeText(StudentAdmissionForm.this,error.getMessage(),Toast.LENGTH_SHORT).show();
            }
        }
    };

    private void fetchVicledetailsEven() {
        HashMap<String,String> params=new HashMap<>();
        ApiHandler.apiHit(Request.Method.GET, Constants.getBaseURL() + Constants.VEHICLE_URL, apiCallBackvrheven, this,null);
    }

    ApiHandler.ApiCallback apiCallBackvrheven=new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            if (error==null){
                try{


                    arrayListveh= VehicleBean.parseVehicleArray(jsonObject.getJSONArray("vehical_data"));
                    VehicledetailSpinerAdap adap1=new VehicledetailSpinerAdap(StudentAdmissionForm.this,arrayListveh);
                    spinevehicleeveni.setAdapter(adap1);
                    spinevehicleeveni.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if (i > 0) {

                                vehicle_id_evening=arrayListveh.get(i-1).getEs_transportid();

                            }
                        }
                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {
                        }
                    });



                } catch (JSONException e) {
                    e.printStackTrace();
                }

            } else {
                Toast.makeText(StudentAdmissionForm.this,error.getMessage(),Toast.LENGTH_SHORT).show();
            }
        }
    };

    private void fetchFeeList() {
        HashMap<String, String> params = new HashMap<String, String>();
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.FEE_LIST_URL, feeapiCallback, this, params);

    }

    ApiHandler.ApiCallback feeapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            if (error == null) {
                try {
                    feeList = FeeCatBean.parseFeeArray(jsonObject.getJSONArray(Constants.FEE_DATA));
                    FeeAdapter adapter = new FeeAdapter(StudentAdmissionForm.this, feeList);
                    spinnerFeeCategory.setAdapter(adapter);
                    spinnerFeeCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if (i > 0) {
                                feeid = feeList.get(i - 1).getFeecategory_id();
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Utils.showSnackBar(error.getMessage(), parent);
            }
        }
    };

    private void fetchRegNo() {
        HashMap<String, String> params = new HashMap<String, String>();
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.REGNO_URL, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                if (error == null) {
                    try {
                        int regNo = jsonObject.getInt("reg_no");
                        txtRegNo.setText(String.valueOf(regNo));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    Utils.showSnackBar(error.getMessage(), parent);
                }
            }
        }, this, params);

    }

    private void fetchSession() {
        HashMap<String, String> params = new HashMap<String, String>();
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.SESSION_URL, sessionapiCallback, this, params);
    }

    ApiHandler.ApiCallback sessionapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            if (error == null) {
                try {
                    sessionlist = SessionBean.parseSessionArray(jsonObject.getJSONArray(Constants.SESSIONDATA));
                    SessionAdapter adapter = new SessionAdapter(StudentAdmissionForm.this, sessionlist);
                    spinnerSession.setAdapter(adapter);
                    spinnerSession.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            session = sessionlist.get(i);
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Utils.showSnackBar(error.getMessage(), parent);
            }
        }
    };


    private void fetchRoute() {
        HashMap<String, String> params = new HashMap<String, String>();
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.ROUTE_URL, transapiCallback, this, params);

    }

    ApiHandler.ApiCallback transapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            if (error == null) {
                try {
                    routelist = RouteBean.parseRouteArray(jsonObject.getJSONArray(Constants.ROUTEDATA));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Utils.showSnackBar(error.getMessage(), parent);
            }
        }
    };

    private void fetchCategory() {
        HashMap<String, String> params = new HashMap<String, String>();
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.CASTELIST_URL, casteapiCallback, this, params);
    }

    ApiHandler.ApiCallback casteapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            if (error == null) {
                try {
                    categorylist = CasteBean.parseCasteArray(jsonObject.getJSONArray(Constants.CASTEDATA));
                    CategoryAdapter adapter = new CategoryAdapter(StudentAdmissionForm.this, categorylist);
                    spinnerCategory.setAdapter(adapter);
                    spinnerCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if (i > 0) {
                                casteid = categorylist.get(i - 1).getCaste_id();

                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Utils.showSnackBar(error.getMessage(), parent);
            }
        }
    };

    private void fetchClass() {
        HashMap<String, String> params = new HashMap<String, String>();
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.CLASSLIST_URL, classapiCallback, this, params);
    }

    ApiHandler.ApiCallback classapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            if (error == null) {
                try {
                    classList = ClassBean.parseClassArray(jsonObject.getJSONArray(Constants.CLASSDATA));
                    ClassAdapterAttend adapter = new ClassAdapterAttend(StudentAdmissionForm.this, classList,0);
                    spinnerClass.setAdapter(adapter);
                    spinnerClass.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if (i > 0) {
                                classid = classList.get(i - 1).getId();
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Utils.showSnackBar(error.getMessage(), parent);
            }
        }
    };

    private void fetchPickupPoint() {
        HashMap<String, String> params = new HashMap<String, String>();
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.PICKPOINTLIST_URL, pickupapiCallback, this, params);
    }

    ApiHandler.ApiCallback pickupapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            if (error == null) {
                try {
                    pickupList = PickupPointBean.parsePickupArray(jsonObject.getJSONArray(Constants.PICKUP_POINT));
                    PickupPointAdapter adapter = new PickupPointAdapter(StudentAdmissionForm.this, pickupList);
                    spinnerPickUpPoint.setAdapter(adapter);
                    spinnerPickUpPoint.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if (i > 0) {
                                if (routelist.size() <= 0) {
                                    fetchRoute();
                                }
                                pickupid = pickupList.get(i - 1).getId();
                                for (RouteBean obj : routelist) {
                                    if (obj.getId().equals(pickupList.get(i - 1).getRoute_id())) {
                                        edtRoute.setText(obj.getName());
                                        routeid = routelist.get(i - 1).getId();
                                    }
                                }
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Utils.showSnackBar(error.getMessage(), parent);
            }
        }
    };

    @OnClick(R.id.imgBack)
    public void onBackImgPress() {
        onBackPressed();
    }

    @OnClick(R.id.imgCal)
    public void fetchDateOfBirth() {
        java.util.Calendar cal = java.util.Calendar.getInstance(TimeZone.getDefault()); // Get current date
        DatePickerDialog datePicker = new DatePickerDialog(this,
                R.style.MyDatePickerDialogTheme, ondate,
                cal.get(java.util.Calendar.YEAR),
                cal.get(java.util.Calendar.MONTH),
                cal.get(java.util.Calendar.DAY_OF_MONTH));
        datePicker.getDatePicker().setMaxDate(new Date().getTime());
        datePicker.setCancelable(false);
        datePicker.show();
    }

    DatePickerDialog.OnDateSetListener ondate = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

            java.util.Calendar calendar = java.util.Calendar.getInstance(TimeZone.getDefault());
            calendar.set(year, monthOfYear, dayOfMonth);
            Date dbirthdate = calendar.getTime();
            Calendar value_datePicker = calendar;
            SimpleDateFormat fmtOut = new SimpleDateFormat("yyyy-MM-dd");
            String dateString = fmtOut.format(dbirthdate);
            birthdate=dateString;
            edtDob.setText(dateString);
        }
    };

    @RequiresApi(api = Build.VERSION_CODES.M)
    @OnClick(R.id.expandedImage)
    public void fetchUserImage() {
//        if (CropImage.isExplicitCameraPermissionRequired(StudentAdmissionForm.this)) {
//            requestPermissions(new String[]{Manifest.permission.CAMERA}, CropImage.CAMERA_CAPTURE_PERMISSIONS_REQUEST_CODE);
//        } else {
//            if (!Utils.checkPermission(StudentAdmissionForm.this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
//                StudentAdmissionForm.this.requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, Constants.READ_PERMISSION);
//            } else {
//                type=5;
//                CropImage.startPickImageActivity(StudentAdmissionForm.this);
//            }
//        }
        pick_img();
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @SuppressLint("ResourceType")
    private void pick_img() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.UPSIDE_DOWN_CAKE) { // Android 14+ (API 34)
          onImageViewClick();

        } else if (Build.VERSION.SDK_INT > Build.VERSION_CODES.S) {
                if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_MEDIA_IMAGES)
                        != PackageManager.PERMISSION_GRANTED) {

                    if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_MEDIA_IMAGES)) {
                        Toast.makeText(this, "Please provide storage permission from app settings", Toast.LENGTH_LONG).show();
                    } else {

                        ActivityCompat.requestPermissions(this,
                                new String[]{Manifest.permission.READ_MEDIA_IMAGES},
                                1);
                    }
                } else {

                    onImageViewClick();
                }

            } else {
                if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {

                    if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                        Toast.makeText(this, "Please provide storage permission from app settings", Toast.LENGTH_LONG).show();
                    } else {

                        ActivityCompat.requestPermissions(this,
                                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                                1);
                    }
                } else {
                    onImageViewClick();

                }
        }
    }

    private void onImageViewClick() {
        // type = 1;
        PickSetup setup = new PickSetup();
        // super.customize(setup);

        PickImageDialog.build(setup)
                //.setOnClick(this)
                .show(this);
    }

    @Override
    public void onPickResult(PickResult r) {
        if (r.getError() == null) {
            //If you want the Bitmap.
            imgUser.setImageBitmap(r.getBitmap());
            userBitmap= r.getBitmap();

        } else {
            //Handle possible errors
            //TODO: do what you have to do with r.getError();
            Toast.makeText(this, r.getError().getMessage(), Toast.LENGTH_LONG).show();
        }

    }


    @OnClick(R.id.btnSubmit)
    public void clickSubmit() {
        if (checkValid()) {
            ErpProgress.showProgressBar(this, "Please wait, form submission takes few seconds.....");
            HashMap<String, String> params = new HashMap<String, String>();
            params.put("rollNo", txtRegNo.getText().toString());
            params.put("section", sectionid);
            params.put("house", houseid);
            params.put(Constants.USER_NAME, edtUserName.getText().toString());
            params.put(Constants.PASSWORD, edtPassword.getText().toString());
            params.put(Constants.TITLE, title);
            params.put(Constants.FIRST_NAME, edtFirstName.getText().toString());
            params.put(Constants.LAST_NAME, edtlastName.getText().toString());
            params.put(Constants.FROM_DATE, session.getStart_date());
            params.put(Constants.TO_DATE, session.getEnd_date());
            params.put(Constants.GENDER, gender);
            params.put(Constants.DOB, String.valueOf(birthdate));
            if (edtReligion.getText().toString().length() > 0)
                params.put(Constants.RELIGION, edtReligion.getText().toString());
            if (edtNationality.getText().toString().length() > 0)
                params.put(Constants.NATIONALITY, edtNationality.getText().toString());
            if (edtMedium.getText().toString().length() > 0)
                params.put(Constants.MEDIUM, edtMedium.getText().toString());
            if (casteid != null && !casteid.isEmpty())
                params.put(Constants.CATEGORY, casteid);
            params.put(Constants.CLASSID, classid);
            if (edtBloodGroup.getText().toString().length() > 0)
                params.put(Constants.BLOOD_GRP, edtBloodGroup.getText().toString());
            if (!feeid.isEmpty())
                params.put(Constants.FEE_CAT, feeid);
            params.put(Constants.AADHAR, edtAadharNo.getText().toString());
            params.put(Constants.PRE_ADDRESS, edtAddress.getText().toString());
            if (edtCity.getText().toString().length() > 0)
                params.put(Constants.PRE_CITY, edtCity.getText().toString());
            if (edtState.getText().toString().length() > 0)
                params.put(Constants.PRE_SATTE, edtState.getText().toString());
            if (edtpinCode.getText().toString().length() > 0)
                params.put(Constants.PRE_PIN_CODE, edtpinCode.getText().toString());
            params.put(Constants.PRE_EMAIL, edtEmail.getText().toString());
            params.put(Constants.SMS_CONTACT, edtPhone.getText().toString());
            params.put(Constants.FATHER_NAME, edtFatherName.getText().toString());
            if (edtFatherAge.getText().toString().length() > 0)
                params.put(Constants.FATHER_AGE, edtFatherAge.getText().toString());
            if (edtFatherEduQuali.getText().toString().length() > 0)
                params.put(Constants.FATHER_QUALI, edtFatherEduQuali.getText().toString());
            if (edtFatherOccupation.getText().toString().length() > 0)
                params.put(Constants.FATHER_OCC, edtFatherOccupation.getText().toString());
            params.put(Constants.FATHER_CONTACT, edtFatherPhone.getText().toString());
            if (edtMotherName.getText().toString().length() > 0)
                params.put(Constants.MOTHER_NAME, edtMotherName.getText().toString());
            if (edtMotherAge.getText().toString().length() > 0)
                params.put(Constants.MOTH_AGE, edtMotherAge.getText().toString());
            if (edtMotherEduQuali.getText().toString().length() > 0)
                params.put(Constants.MOTHER_QUALI, edtMotherEduQuali.getText().toString());
            if (edtMotherOccupation.getText().toString().length() > 0)
                params.put(Constants.MOTHER_OCC, edtMotherOccupation.getText().toString());
            params.put(Constants.MOTHER_CONTACT, edtMotherPhone.getText().toString());
            if (checkTransport.isChecked()) {
                if (!routeid.isEmpty() && !pickupid.isEmpty()) {
                    params.put(Constants.PICKUPPOINT, pickupid);
                    params.put(Constants.ROUTE, routeid);
                    params.put("morn_vehicle",vehicle_id_morning);
                    params.put("even_vehicle",vehicle_id_evening);
                }
            } else {
                if (edtOtherTransport.getText().toString().length() > 0)
                    params.put(Constants.OTHER, edtOtherTransport.getText().toString());
            }
            if (userBitmap != null) {
                String encoded = Utils.encodeToBase64(userBitmap, Bitmap.CompressFormat.JPEG, 90);
                encoded = String.format("data:image/jpeg;base64,%s", encoded);
                params.put("image", encoded);
            }

            if (hwbitmap != null) {
                String encoded = Utils.encodeToBase64(hwbitmap, Bitmap.CompressFormat.JPEG, 50);
                encoded = String.format("data:image/jpeg;base64,%s", encoded);
                params.put("imageaadhar", encoded);
                Log.e("imagedd", encoded);
            }
            if (fileBase64 != null) {
                params.put("fileaadhar", fileBase64);
                Log.e("file", fileBase64);
            }

            if (hwbitmap10 != null) {
                String encoded10 = Utils.encodeToBase64(hwbitmap10, Bitmap.CompressFormat.JPEG, 50);
                encoded10 = String.format("data:image/jpeg;base64,%s", encoded10);
                params.put("image10th", encoded10);
                Log.e("image10th", encoded10);
            }
            if (fileBase6410th != null) {
                params.put("file10th", fileBase6410th);
                Log.e("file", fileBase64);
            }

            if (hwbitmap12 != null) {
                String encoded12 = Utils.encodeToBase64(hwbitmap12, Bitmap.CompressFormat.JPEG, 50);
                encoded12 = String.format("data:image/jpeg;base64,%s", encoded12);
                params.put("image12th", encoded12);
                Log.e("image12th", encoded12);
            }
            if (fileBase6413th != null) {
                params.put("file12th", fileBase6413th);
                //  Log.e("file12th", fileBase64);
            }


            if (hwbitmapschlcert != null) {
                String encodedschlcert = Utils.encodeToBase64(hwbitmapschlcert, Bitmap.CompressFormat.JPEG, 50);
                encodedschlcert = String.format("data:image/jpeg;base64,%s", encodedschlcert);
                params.put("imageschcert", encodedschlcert);
                Log.e("imageschcert", encodedschlcert);
            }
            if (fileBase64scth != null) {
                params.put("fileschcert", fileBase64scth);
                Log.e("fileschcert", fileBase64scth);
            }


            if (hwbitmapschltrans != null) {
                String encodedtrans = Utils.encodeToBase64(hwbitmapschltrans, Bitmap.CompressFormat.JPEG, 50);
                encodedtrans = String.format("data:image/jpeg;base64,%s", encodedtrans);
                params.put("imageschtrans", encodedtrans);
                Log.e("imageschtrans", encodedtrans);
            }
            if (fileBase64tcth != null) {
                params.put("fileschtrans", fileBase64tcth);
                Log.e("fileschtrans", fileBase64tcth);
            }

            if (hwbitmapgrad != null) {
                String encodedgrad = Utils.encodeToBase64(hwbitmapgrad, Bitmap.CompressFormat.JPEG, 50);
                encodedgrad = String.format("data:image/jpeg;base64,%s", encodedgrad);
                params.put("imagegrad", encodedgrad);
                Log.e("imagegrad", encodedgrad);
            }
            if (fileBase64grad != null) {
                params.put("filegrad", fileBase64grad);
                Log.e("filegrad", fileBase64grad);
            }
            ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.ADMISSION_URL, apiCallback, this, params);

        }
    }

    ApiHandler.ApiCallback apiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            if (error == null) {
                try {
                    showSnackBar(jsonObject.getString("msg"));
                    Toast.makeText(getApplicationContext(), "Form submitted successfully.", Toast.LENGTH_SHORT).show();
                    finish();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("error", error.getMessage() + "");
                showSnackBar(error.getMessage());
            }
        }
    };

    private boolean checkValid() {
        boolean valid = true;
        String errorMsg = null;
        if (txtRegNo.getText().toString().trim().length() <= 0) {
            valid = false;
            errorMsg = "Please enter Registration No.";
        }else if (edtFirstName.getText().toString().trim().length() <= 0) {
            valid = false;
            errorMsg = "Please enter firstname";
        } else if (edtlastName.getText().toString().trim().length() <= 0) {
            valid = false;
            errorMsg = "Please enter lastname";
        } else if (session == null) {
            valid = false;
            errorMsg = "Please select session";
        }
//        else if (birthdate == null) {
//            valid = false;
//            errorMsg = "Please enter date of birth";
//        }
        else if (classid.isEmpty()) {
            valid = false;
            errorMsg = "Please select class";
        }
//        else if (edtAadharNo.getText().toString().trim().length() <= 0) {
//            valid = false;
//            errorMsg = "Please enter aadhar no.";
//        }
//        else if (edtAddress.getText().toString().trim().length() <= 0) {
//            valid = false;
//            errorMsg = "Please enter present address";
//        }
        else if (edtPhone.getText().toString().trim().length() <= 0) {
            valid = false;
            errorMsg = "Please enter sms mobile no.";
        }
//        else if (edtFatherPhone.getText().toString().trim().length() <= 0) {
//            valid = false;
//            errorMsg = "Please enter father contact no.";
//        } else if (edtEmail.getText().toString().trim().length() <= 0) {
//            valid = false;
//            errorMsg = "Please enter present email address";
//        }
        else if (edtFatherName.getText().toString().trim().length() <= 0) {
            valid = false;
            errorMsg = "Please enter father name";
        } else if (edtUserName.getText().toString().trim().length() <= 0) {
            valid = false;
            errorMsg = "Please enter Username";
        } else if (edtPassword.getText().toString().trim().length() <= 0) {
            valid = false;
            errorMsg = "Please enter password";
        }
//        else if(checkTransport.isChecked()){
//            if (vehicle_id_morning == null || vehicle_id_morning.isEmpty()) {
//                errorMsg = "Please select Morning vehicle";
//                valid = false;
//            }else
//
//            if (vehicle_id_evening == null || vehicle_id_evening.isEmpty()) {
//                errorMsg = "Please select Evening vehicle";
//                valid = false;
//            }
//
//        }



        if (!valid) {
            showSnackBar(errorMsg);
        }
        return valid;
    }

    public void showSnackBar(String errorMsg) {
        Snackbar snack = Snackbar.make(parent, errorMsg, Snackbar.LENGTH_LONG);
        View view = snack.getView();
//        TextView tv = (TextView) view.findViewById(R.id.snackbar_text);
//        tv.setTextColor(Color.WHITE);
        snack.show();
    }

//    @Override
//    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        if (resultCode == Activity.RESULT_OK) {
//            switch (requestCode) {
//                case CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE:
//                    Uri imageUri = CropImage.getPickImageResultUri(StudentAdmissionForm.this, data);
//                    CropImage.activity(imageUri).setCropShape(CropImageView.CropShape.RECTANGLE).setAspectRatio(1, 1)
//                            .start(StudentAdmissionForm.this);
//                    break;
//
//                case CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE:
//                    CropImage.ActivityResult result = CropImage.getActivityResult(data);
//                    Uri resultUri = result.getUri();
//                    userBitmap = Utils.getBitmapFromUri(StudentAdmissionForm.this, resultUri, 2048);
//                    imgUser.setImageBitmap(userBitmap);
//
//            }
//        }
//    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    @OnClick(R.id.addimage)
    public void click() {
        type = 1;
        CallCropAct();
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @OnClick(R.id. addimage10th)
    public void profclick() {
        type = 6;
        CallCropAct();
    }
    @RequiresApi(api = Build.VERSION_CODES.M)
    @OnClick(R.id. addimage12th)
    public void prof12click() {
        type = 7;
        CallCropAct();
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @OnClick(R.id. addimagescholcert)
    public void profschcerticlick() {
        type = 8;
        CallCropAct();
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @OnClick(R.id. addimagescoltc)
    public void profschtciclick() {
        type = 9;
        CallCropAct();
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @OnClick(R.id. addimagegrad)
    public void profgradclick() {
        type = 10;
        CallCropAct();
    }
    @RequiresApi(api = Build.VERSION_CODES.M)
    public void CallCropAct() {
//        if (CropImage.isExplicitCameraPermissionRequired(StudentAdmissionForm.this)) {
//            requestPermissions(new String[]{Manifest.permission.CAMERA}, CropImage.CAMERA_CAPTURE_PERMISSIONS_REQUEST_CODE);
//        } else {
//            if (!checkPermission(StudentAdmissionForm.this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
//                StudentAdmissionForm.this.requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
//            } else {
//                CropImage.startPickImageActivity(StudentAdmissionForm.this);
//            }
//        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {


//                case CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE:
//                    Uri imageUri2 = CropImage.getPickImageResultUri(StudentAdmissionForm.this, data);
//
//                    if (type == 1) {
//                        CropImage.activity(imageUri2).start(StudentAdmissionForm.this);
//                    }else if(type==5){
//                        Uri imageUri = CropImage.getPickImageResultUri(StudentAdmissionForm.this, data);
//                        CropImage.activity(imageUri).setCropShape(CropImageView.CropShape.RECTANGLE).setAspectRatio(1, 1)
//                                .start(StudentAdmissionForm.this);
//                        break;
//                    }else if( type == 6){//10th marksheet
//
//                        Uri imageUri10 = CropImage.getPickImageResultUri(StudentAdmissionForm.this, data);
//                        CropImage.activity(imageUri10).setCropShape(CropImageView.CropShape.RECTANGLE).setAspectRatio(1, 1)
//                                .start(StudentAdmissionForm.this);
//                        break;
//                    }else if( type ==7){//12th marksheet
//
//                        Uri imageUri12 = CropImage.getPickImageResultUri(StudentAdmissionForm.this, data);
//                        CropImage.activity(imageUri12).setCropShape(CropImageView.CropShape.RECTANGLE).setAspectRatio(1, 1)
//                                .start(StudentAdmissionForm.this);
//                        break;
//                    }else if( type ==8){//school certificate
//
//                        Uri imageUri13 = CropImage.getPickImageResultUri(StudentAdmissionForm.this, data);
//                        CropImage.activity(imageUri13).setCropShape(CropImageView.CropShape.RECTANGLE).setAspectRatio(1, 1)
//                                .start(StudentAdmissionForm.this);
//                        break;
//                    }else if( type ==9){//school tc
//
//                        Uri imageUri14 = CropImage.getPickImageResultUri(StudentAdmissionForm.this, data);
//                        CropImage.activity(imageUri14).setCropShape(CropImageView.CropShape.RECTANGLE).setAspectRatio(1, 1)
//                                .start(StudentAdmissionForm.this);
//                        break;
//                    }else if( type ==10){//graduation
//
//                        Uri imageUri15 = CropImage.getPickImageResultUri(StudentAdmissionForm.this, data);
//                        CropImage.activity(imageUri15).setCropShape(CropImageView.CropShape.RECTANGLE).setAspectRatio(1, 1)
//                                .start(StudentAdmissionForm.this);
//                        break;
//                    }
//
//                    break;
//
//
//
//                case CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE:
//                    if(type==1) {//aadhar card
//                        CropImage.ActivityResult result2 = CropImage.getActivityResult(data);
//                        Uri resultUri2 = result2.getUri();
//                        hwbitmap = Utils.getBitmapFromUri(StudentAdmissionForm.this, resultUri2, 2048);
//                        imageView6.setImageBitmap(hwbitmap);
////                        addpdf.setClickable(false);
////                        addpdf.setFocusable(false);
//                        textnote.setText("Image: Your selected images shown above.");
//
//                        Toast.makeText(getApplicationContext(), "error.getMessage()", Toast.LENGTH_SHORT).show();
//                    }
////                    else if(type==5){//profile image
////
////                        CropImage.ActivityResult result = CropImage.getActivityResult(data);
////                        Uri resultUri = result.getUri();
////                        userBitmap = Utils.getBitmapFromUri(StudentAdmissionForm.this, resultUri, 2048);
////                        imgUser.setImageBitmap(userBitmap);
////                    }
//                    else if(type==6){//10th image
//
//                        CropImage.ActivityResult result3 = CropImage.getActivityResult(data);
//                        Uri resultUri3 = result3.getUri();
//                        hwbitmap10 = Utils.getBitmapFromUri(StudentAdmissionForm.this, resultUri3, 2048);
//                        imageView610th.setImageBitmap(hwbitmap10);
//                    }else if(type==7){//12th image
//
//                        CropImage.ActivityResult result4 = CropImage.getActivityResult(data);
//                        Uri resultUri4 = result4.getUri();
//                        hwbitmap12 = Utils.getBitmapFromUri(StudentAdmissionForm.this, resultUri4, 2048);
//                        imageView612th.setImageBitmap(hwbitmap12);
//                    }else if(type==8){//schoolcertificate
//
//                        CropImage.ActivityResult result5 = CropImage.getActivityResult(data);
//                        Uri resultUri5 = result5.getUri();
//                        hwbitmapschlcert = Utils.getBitmapFromUri(StudentAdmissionForm.this, resultUri5, 2048);
//                        imageView6scholcert.setImageBitmap(hwbitmapschlcert);
//                    }else if(type==9){//schooltc
//
//                        CropImage.ActivityResult result6 = CropImage.getActivityResult(data);
//                        Uri resultUri6 = result6.getUri();
//                        hwbitmapschltrans = Utils.getBitmapFromUri(StudentAdmissionForm.this, resultUri6, 2048);
//                        imageView6scoltc.setImageBitmap(hwbitmapschltrans);
//                    }else if(type==10){//grad
//
//                        CropImage.ActivityResult result7 = CropImage.getActivityResult(data);
//                        Uri resultUri7 = result7.getUri();
//                        hwbitmapgrad = Utils.getBitmapFromUri(StudentAdmissionForm.this, resultUri7, 2048);
//                        imageView6grad.setImageBitmap(hwbitmapgrad);
//                    }

                case LOAD_FILE_RESULTS:
                    try {
                        Uri uri = data.getData();
                        // String path = Utils.getPath(this, uri);
                        if(uri!=null) {
                            String path = FileUtils.getRealPath(this, uri);
                            fileBase64 = Utils.encodeFileToBase64Binary(path);
                            textnote.setText("File Name: " + path);
//                            addimage.setClickable(false);
//                            addimage.setFocusable(false);
                            imageView6.setImageResource(R.drawable.pdfbig);
                            Toast.makeText(this, "File Picked...", Toast.LENGTH_SHORT).show();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                case LOAD_FILE_RESULTS10:
                    try {
                        Uri uri = data.getData();
                        // String path = Utils.getPath(this, uri);
                        if(uri!=null) {
                            String path = FileUtils.getRealPath(this, uri);
                            fileBase6410th = Utils.encodeFileToBase64Binary(path);
                            textnote10th.setText("File Name: " + path);
//                            addimage.setClickable(false);
//                            addimage.setFocusable(false);
                            imageView610th.setImageResource(R.drawable.pdfbig);
                            Toast.makeText(this, "File Picked...", Toast.LENGTH_SHORT).show();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                case LOAD_FILE_RESULTS12:
                    try {
                        Uri uri = data.getData();
                        // String path = Utils.getPath(this, uri);
                        if(uri!=null) {
                            String path = FileUtils.getRealPath(this, uri);
                            fileBase6413th = Utils.encodeFileToBase64Binary(path);
                            textnote12th.setText("File Name: " + path);
//                            addimage.setClickable(false);
//                            addimage.setFocusable(false);
                            imageView612th.setImageResource(R.drawable.pdfbig);
                            Toast.makeText(this, "File Picked...", Toast.LENGTH_SHORT).show();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }


                case LOAD_FILE_RESULTSsc:
                    try {
                        Uri uri = data.getData();
                        // String path = Utils.getPath(this, uri);
                        if(uri!=null) {
                            String path = FileUtils.getRealPath(this, uri);
                            fileBase64scth = Utils.encodeFileToBase64Binary(path);
                            textnotescholcert.setText("File Name: " + path);
//                            addimage.setClickable(false);
//                            addimage.setFocusable(false);
                            imageView6scholcert.setImageResource(R.drawable.pdfbig);
                            Toast.makeText(this, "File Picked...", Toast.LENGTH_SHORT).show();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                case LOAD_FILE_RESULTSstc:
                    try {
                        Uri uri = data.getData();
                        // String path = Utils.getPath(this, uri);
                        if(uri!=null) {
                            String path = FileUtils.getRealPath(this, uri);
                            fileBase64tcth = Utils.encodeFileToBase64Binary(path);
                            textnotescoltc.setText("File Name: " + path);
//                            addimage.setClickable(false);
//                            addimage.setFocusable(false);
                            imageView6scoltc.setImageResource(R.drawable.pdfbig);
                            Toast.makeText(this, "File Picked...", Toast.LENGTH_SHORT).show();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                case LOAD_FILE_RESULTSgc:
                    try {
                        Uri uri = data.getData();
                        // String path = Utils.getPath(this, uri);
                        if(uri!=null) {
                            String path = FileUtils.getRealPath(this, uri);
                            fileBase64grad = Utils.encodeFileToBase64Binary(path);
                            textnotegrad.setText("File Name: " + path);
//                            addimage.setClickable(false);
//                            addimage.setFocusable(false);
                            imageView6grad.setImageResource(R.drawable.pdfbig);
                            Toast.makeText(this, "File Picked...", Toast.LENGTH_SHORT).show();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
            }
        }
    }

    public static boolean checkPermission(Context mContext, String Permission) {
        int result = ContextCompat.checkSelfPermission(mContext, Permission);
        if (result == PackageManager.PERMISSION_GRANTED) {

            return true;
        } else {
            return false;
        }
    }
    @OnClick(R.id.imageView6)
    public void imagecheck() {

        if (hwbitmap != null) {
            final Dialog EventDialog = new Dialog(this);
            EventDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            EventDialog.setCancelable(true);
            EventDialog.setContentView(R.layout.image_lyt);
            EventDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            ImageView imgShow = (ImageView) EventDialog.findViewById(R.id.imgShow);
            ImageView imgDownload = (ImageView) EventDialog.findViewById(R.id.imgDownload);
            imgDownload.setVisibility(View.GONE);
            imgShow.setImageBitmap(hwbitmap);
            // Picasso.with(this).load(String.valueOf(hwbitmap)).placeholder(getResources().getDrawable(R.drawable.home)).into(imgShow);

            EventDialog.show();
            Window window = EventDialog.getWindow();
            window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        } else{
            Toast.makeText(getApplicationContext(),"Please Select HomeWork Image",Toast.LENGTH_LONG).show();
        }
    }



    @OnClick(R.id.addpdf)
    public void browseDocuments() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                Toast.makeText(this,"Please provide storage permission from app settings",Toast.LENGTH_LONG).show();
            } else {

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        1);

            }
        }else{
            callPicker();
            // showFileChooser();
        }


    }

    @OnClick(R.id.addpdf10th)
    public void browsepdf10thDocuments() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                Toast.makeText(this,"Please provide storage permission from app settings",Toast.LENGTH_LONG).show();
            } else {

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        1);

            }
        }else{
            callPicker10();
            // showFileChooser();
        }


    }
    public void callPicker(){
        Toast.makeText(getApplicationContext(),"hiiiimonu",Toast.LENGTH_LONG).show();
        type = 2;

        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("application/pdf");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        //startActivityForResult(intent, SAVE_REQUEST_CODE);
        startActivityForResult(Intent.createChooser(intent, "ChooseFile"), LOAD_FILE_RESULTS);
    }
    public void callPicker10(){
        Toast.makeText(getApplicationContext(),"hiiiimonu",Toast.LENGTH_LONG).show();
        type = 2;

        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("application/pdf");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        //startActivityForResult(intent, SAVE_REQUEST_CODE);
        startActivityForResult(Intent.createChooser(intent, "ChooseFile"), LOAD_FILE_RESULTS10);
    }

    @OnClick(R.id.addpdf12th)
    public void browsepdf12thDocuments() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                Toast.makeText(this,"Please provide storage permission from app settings",Toast.LENGTH_LONG).show();
            } else {

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        1);

            }
        }else{
            callPicker12();
            // showFileChooser();
        }


    }

    public void callPicker12(){
        Toast.makeText(getApplicationContext(),"hiiiimonu",Toast.LENGTH_LONG).show();
        type = 2;

        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("application/pdf");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        //startActivityForResult(intent, SAVE_REQUEST_CODE);
        startActivityForResult(Intent.createChooser(intent, "ChooseFile"), LOAD_FILE_RESULTS12);
    }


    @OnClick(R.id.addpdfscholcert)
    public void browsepdfscholcertDocuments() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                Toast.makeText(this,"Please provide storage permission from app settings",Toast.LENGTH_LONG).show();
            } else {

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        1);

            }
        }else{
            callPickerscholcert();
            // showFileChooser();
        }


    }

    public void callPickerscholcert(){
        Toast.makeText(getApplicationContext(),"hiiiimonu",Toast.LENGTH_LONG).show();
        type = 2;

        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("application/pdf");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        //startActivityForResult(intent, SAVE_REQUEST_CODE);
        startActivityForResult(Intent.createChooser(intent, "ChooseFile"), LOAD_FILE_RESULTSsc);
    }

    @OnClick(R.id.addpdfscoltc)
    public void browsepdfscoltcDocuments() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                Toast.makeText(this,"Please provide storage permission from app settings",Toast.LENGTH_LONG).show();
            } else {

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        1);

            }
        }else{
            callPickerscoltc();
            // showFileChooser();
        }


    }

    public void callPickerscoltc(){

        type = 2;

        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("application/pdf");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        //startActivityForResult(intent, SAVE_REQUEST_CODE);
        startActivityForResult(Intent.createChooser(intent, "ChooseFile"), LOAD_FILE_RESULTSstc);
    }

    @OnClick(R.id.addpdfgrad)
    public void browsepdfgradDocuments() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                Toast.makeText(this,"Please provide storage permission from app settings",Toast.LENGTH_LONG).show();
            } else {

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        1);

            }
        }else{
            callPickergrad();
            // showFileChooser();
        }


    }

    public void callPickergrad(){
        Toast.makeText(getApplicationContext(),"hiiiimonu",Toast.LENGTH_LONG).show();
        type = 2;

        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("application/pdf");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        //startActivityForResult(intent, SAVE_REQUEST_CODE);
        startActivityForResult(Intent.createChooser(intent, "ChooseFile"), LOAD_FILE_RESULTSgc);
    }
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 1: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    callPicker();

                } else {
                    // permission denied, boo! Disable the
                    Toast.makeText(this, "Please provide storage permission from app settings, as it is mandatory to access your files!", Toast.LENGTH_LONG).show();
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request.
        }
    }
}
