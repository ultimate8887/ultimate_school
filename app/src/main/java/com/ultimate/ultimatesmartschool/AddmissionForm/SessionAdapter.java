package com.ultimate.ultimatesmartschool.AddmissionForm;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.ultimate.ultimatesmartschool.R;

import java.util.ArrayList;

public class SessionAdapter extends BaseAdapter {
    private final ArrayList<SessionBean> sessionList;
    Context context;
    LayoutInflater inflter;

    public SessionAdapter(Context applicationContext, ArrayList<SessionBean> sessionList) {
        this.context = applicationContext;
        this.sessionList = sessionList;
        inflter = (LayoutInflater.from(applicationContext));
    }

    @Override
    public int getCount() {
        return sessionList.size() ;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = inflter.inflate(R.layout.spinner_lyt, null);
        TextView label = (TextView) view.findViewById(R.id.txtText);
        SessionBean sessionObj = sessionList.get(i);
        label.setText(sessionObj.getStart_date() + " to " + sessionObj.getEnd_date());
        return view;
    }
}
