package com.ultimate.ultimatesmartschool.AddmissionForm;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class FeeCatBean {



    private static String ID = "feecategory_id";
    private static String NAME = "feecategory";

    private String feecategory_id;
    private String feecategory;

    public String getFeecategory_id() {
        return feecategory_id;
    }

    public void setFeecategory_id(String feecategory_id) {
        this.feecategory_id = feecategory_id;
    }

    public String getFeecategory() {
        return feecategory;
    }

    public void setFeecategory(String feecategory) {
        this.feecategory = feecategory;
    }

    public static ArrayList<FeeCatBean> parseFeeArray(JSONArray arrayObj) {
        ArrayList list = new ArrayList<FeeCatBean>();
        try {
            for (int i = 0; i < arrayObj.length(); i++) {
                FeeCatBean p = parseFeeObject(arrayObj.getJSONObject(i));
                if (p != null) {
                    list.add(p);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public static FeeCatBean parseFeeObject(JSONObject jsonObject) {
        FeeCatBean casteObj = new FeeCatBean();
        try {
            if (jsonObject.has(ID)) {
                casteObj.setFeecategory_id(jsonObject.getString(ID));
            }
            if (jsonObject.has(NAME) && !jsonObject.getString(NAME).isEmpty() && !jsonObject.getString(NAME).equalsIgnoreCase("null")) {
                casteObj.setFeecategory(jsonObject.getString(NAME));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return casteObj;
    }
}
