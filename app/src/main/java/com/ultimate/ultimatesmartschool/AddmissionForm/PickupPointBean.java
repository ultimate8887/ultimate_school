package com.ultimate.ultimatesmartschool.AddmissionForm;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

class PickupPointBean {

    private static String ID = "id";
    private static String NAME = "name";
    private static String ROUTE_ID = "route_id";
    /**
     * id : 1
     * route_id : 1
     * name : bhucho
     * amount : 0
     * status : Active
     */

    private String id;
    private String route_id;
    private String name;
    private String amount;
    private String status;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRoute_id() {
        return route_id;
    }

    public void setRoute_id(String route_id) {
        this.route_id = route_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public static ArrayList<PickupPointBean> parsePickupArray(JSONArray arrayObj) {
        ArrayList list = new ArrayList<PickupPointBean>();
        try {
            for (int i = 0; i < arrayObj.length(); i++) {
                PickupPointBean p = parsePickupObject(arrayObj.getJSONObject(i));
                if (p != null) {
                    list.add(p);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public static PickupPointBean parsePickupObject(JSONObject jsonObject) {
        PickupPointBean casteObj = new PickupPointBean();
        try {
            if (jsonObject.has(ID)) {
                casteObj.setId(jsonObject.getString(ID));
            }
            if (jsonObject.has(NAME) && !jsonObject.getString(NAME).isEmpty() && !jsonObject.getString(NAME).equalsIgnoreCase("null")) {
                casteObj.setName(jsonObject.getString(NAME));
            }
            if (jsonObject.has(ROUTE_ID) && !jsonObject.getString(ROUTE_ID).isEmpty() && !jsonObject.getString(ROUTE_ID).equalsIgnoreCase("null")) {
                casteObj.setRoute_id(jsonObject.getString(ROUTE_ID));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return casteObj;
    }

}
