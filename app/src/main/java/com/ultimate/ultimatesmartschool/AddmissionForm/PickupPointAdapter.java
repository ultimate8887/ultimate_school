package com.ultimate.ultimatesmartschool.AddmissionForm;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.ultimate.ultimatesmartschool.R;

import java.util.ArrayList;

public class PickupPointAdapter extends BaseAdapter {
    private final ArrayList<PickupPointBean> pickupPointList;
    Context context;
    LayoutInflater inflter;

    public PickupPointAdapter(Context applicationContext, ArrayList<PickupPointBean> pickupPointList) {
        this.context = applicationContext;
        this.pickupPointList = pickupPointList;
        inflter = (LayoutInflater.from(applicationContext));
    }

    @Override
    public int getCount() {
        return pickupPointList.size() + 1;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        view = inflter.inflate(R.layout.spinner_prod, null);
        TextView label = (TextView) view.findViewById(R.id.txtText);
        if (i == 0) {
            // Default selected Spinner item
            label.setText("Pickup Point");
        } else {
            PickupPointBean classObj = pickupPointList.get(i - 1);
            label.setText(classObj.getName());
            Log.e("pickuppoint",classObj.getName());
        }

        return view;
    }
}
