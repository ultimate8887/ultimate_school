package com.ultimate.ultimatesmartschool.Home;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;


import com.andremion.floatingnavigationview.FloatingNavigationView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetSequence;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;

import com.squareup.picasso.Picasso;
import com.ultimate.ultimatesmartschool.Activity.B_SliderAdapter;
import com.ultimate.ultimatesmartschool.Activity.NewsData;
import com.ultimate.ultimatesmartschool.Activity.UpdateProfileActivity;
import com.ultimate.ultimatesmartschool.Activity.UserPermissions;
import com.ultimate.ultimatesmartschool.AddThought.Thought_bean;
import com.ultimate.ultimatesmartschool.Banner.BannerBean;
import com.ultimate.ultimatesmartschool.Banner.CustomVolleyRequest;
import com.ultimate.ultimatesmartschool.Banner.ViewPagerAdapter;
import com.ultimate.ultimatesmartschool.BeanModule.ClassBean;
import com.ultimate.ultimatesmartschool.BeanModule.User;


import com.ultimate.ultimatesmartschool.Holiday.AddHolidayActivity;
import com.ultimate.ultimatesmartschool.Login.LoginActivity;

import com.ultimate.ultimatesmartschool.MSG_NEW.SendMSG;
import com.ultimate.ultimatesmartschool.NoticeMod.NoticeBean;
import com.ultimate.ultimatesmartschool.NoticeMod.ViewNoticeInfo;
import com.ultimate.ultimatesmartschool.NotificationMod.NotificationAct;
import com.ultimate.ultimatesmartschool.NotificationMod.NotificationAdapter;
import com.ultimate.ultimatesmartschool.NotificationMod.NotificationBean;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Staff.StaffBirthdayList;
import com.ultimate.ultimatesmartschool.StaffAttendance.QRAttendance.StaffAttendanceReport;
import com.ultimate.ultimatesmartschool.Student.StudentBirthdayList;
import com.ultimate.ultimatesmartschool.StudentAttendance.SubjectWiseAttendeance.AttendanceReport;

import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.CommonProgress;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.relex.circleindicator.CircleIndicator;

import static android.content.Context.MODE_PRIVATE;


public class Mainfragment extends Fragment implements TextToSpeech.OnInitListener, NotificationAdapter.AdapterCallback,Holidayadapter.Mycallback {

    String today_txtMsg_s = "", today_a_s = "", weekly_txtMsg_s = "", weekly_a_s = "", monthly_txtMsg_s = "", monthly_a_s = "", yearly_txtMsg_s = "",
            yearly_a_s = "",staff_birth_c,std_birth_c;

    @BindView(R.id.today_txtMsg)
    TextView today_txtMsg;
    @BindView(R.id.today_a)
    TextView today_a;

    @BindView(R.id.Weekly_txtMsg)
    TextView weekly_txtMsg;
    @BindView(R.id.Weekly_a)
    TextView weekly_a;

    @BindView(R.id.a_count)
    TextView a_count;
    @BindView(R.id.p_count)
    TextView p_count;
    @BindView(R.id.l_count)
    TextView l_count;

    @BindView(R.id.total_students)
    TextView total_students;

    @BindView(R.id.total_students_m)
    TextView total_students_m;


    @BindView(R.id.a_count_staff)
    TextView a_count_staff;
    @BindView(R.id.p_count_staff)
    TextView p_count_staff;
    @BindView(R.id.l_count_staff)
    TextView l_count_staff;

    @BindView(R.id.total_staff)
    TextView total_staff;

    @BindView(R.id.total_staff_m)
    TextView total_staff_m;

    @BindView(R.id.monthly_txtMsg)
    TextView monthly_txtMsg;
    @BindView(R.id.monthly_a)
    TextView monthly_a;

    @BindView(R.id.std_birth)
    TextView std_birth;
    @BindView(R.id.staff_birth)
    TextView staff_birth;

    @BindView(R.id.std_txtMsg)
    TextView std_txtMsg;
    @BindView(R.id.staff_txtMsg)
    TextView staff_txtMsg;

    @BindView(R.id.staff_birth_list)
    RelativeLayout staff_birth_list;
    @BindView(R.id.std_birth_list)
    RelativeLayout std_birth_list;

    @BindView(R.id.staff_att)
    LinearLayout staff_att;
    @BindView(R.id.std_att)
    LinearLayout std_att;

    @BindView(R.id.banner_card1)
    CardView banner_card1;


    int load = 0;
    private FloatingNavigationView mFloatingNavigationView;
    TextView bday, txtSetup, txtNB;
    Animation animation1, animation2, animation3, animation4;
    @BindView(R.id.imgStud)
    CircularImageView imgStud;
    SharedPreferences sharedPreferences;
    @BindView(R.id.txtStudName)
    TextView txtStudName;
    @BindView(R.id.class_name)
    TextView class_name;
    @BindView(R.id.view_notification)
    TextView view_notification;
    @BindView(R.id.roll_no)
    TextView roll_no;
    @BindView(R.id.imgProfile)
    ImageView imgProfile;
    @BindView(R.id.view_noticeBoard)
    ImageView view_noticeBoard;
    @BindView(R.id.view_holidayList)
    ImageView view_holidayList;
    TextView stu_name, class_namessss;
    ImageView imgMoreUser;
    CircularImageView navi_profile;
    String password = "", c_password = "", image_url = "";
    @BindView(R.id.thought_shimmer)
    ShimmerFrameLayout thought_shimmer;
    @BindView(R.id.holiday_shimmer)
    ShimmerFrameLayout holiday_shimmer;
    @BindView(R.id.shimmer)
    ShimmerFrameLayout shimmer;
    @BindView(R.id.noti_shimmer)
    ShimmerFrameLayout noti_shimmer;

    @BindView(R.id.notification_shimmer)
    ShimmerFrameLayout notification_shimmer;
    CommonProgress commonProgress;
    ArrayList<NewsData> new_sliderDataArrayList = new ArrayList<>();

    int speech = 0;
    TextToSpeech textToSpeech;
    String textholder = "", title = "";

    @BindView(R.id.f_view1)
    LinearLayout f_view1;

    @BindView(R.id.lyt1)
    RelativeLayout lyt1;


    /*Notice board section*/
    @BindView(R.id.nbRV)
    RecyclerView nbRV;
    ArrayList<NoticeBean> noticeList;
    private NoticeBoardAdapter nbAdapter;
    @BindView(R.id.noNoticeData)
    TextView noNoticeData;

    /*Notification section*/
    @BindView(R.id.notiRV)
    RecyclerView notiRV;
    ArrayList<NotificationBean> notificationBeans;
    private NotificationAdapter notificationAdapter;
    @BindView(R.id.noNotificationData)
    TextView noNotificationData;

    /* holiday section*/
    @BindView(R.id.holiRV)
    RecyclerView holiRV;
    ArrayList<HolidayBean> holidayList;
    private Holidayadapter hAdapter;
    @BindView(R.id.noHolidayData)
    TextView noHolidayData;
    ViewPager viewPager;
    LinearLayout sliderDotspanel;
    private int dotscount;
    private ImageView[] dots;
    private int current_pos = 0;
    int count = 0;
    RequestQueue rq;
    List<BannerBean> sliderImg;
    ViewPagerAdapter viewPagerAdapter;
    //    String request_url = "https://ultimatesolutiongroup.com/includes/studentapi/sliderBanner.php";
//    String request_url1= "https://ultimatesolutiongroup.com/office_admin/images/banner/";
    String request_url = "http://ultimatesolutiongroup.com/includes/studentapi/sliderBanner.php";
    String request_url1 = "http://ultimatesolutiongroup.com/office_admin/images/banner/";
    Handler handler = new Handler();
    Runnable runnable;
    Timer timer = new Timer();
    @BindView(R.id.lytHolidy)
    CardView lytHolidy;
    @BindView(R.id.lytNB)
    CardView lytNB;
    BottomSheetDialog mBottomSheetDialog;
    TextView admin_name, post;
    @BindView(R.id.headtext)
    TextView headtext;
    ArrayList<Thought_bean> eventList = new ArrayList<>();
    @BindView(R.id.thought)
    TextView thought;
    // private SliderAdapterExample adapter;
    @BindView(R.id.indicator)
    CircleIndicator indicator;
    List<BannerBean> sliderItemList = new ArrayList<>();

    B_SliderAdapter b_adapter;
    //SliderView Code


    ImageView edit_profile;

    String url1 = "http://ultimatesolutiongroup.com/office_admin/images/photo1.jpg";
    String url2 = "http://ultimatesolutiongroup.com/office_admin/images/photo2.jpg";
    String url3 = "http://ultimatesolutiongroup.com/office_admin/images/photo3.jpg";
    String url4 = "http://ultimatesolutiongroup.com/office_admin/images/photo4.jpg";


    String url6 = "http://ultimatesolutiongroup.com/office_admin/images/photo6.jpeg";
    String url7 = "http://ultimatesolutiongroup.com/office_admin/images/photo7.jpeg";
    String url8 = "http://ultimatesolutiongroup.com/office_admin/images/photo8.jpeg";
    String url9 = "http://ultimatesolutiongroup.com/office_admin/images/photo9.jpeg";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.mainfrag_layout_new, container, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        view_noticeBoard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //  startActivity(new Intent(getContext(), ViewNoticeInfo.class));
                // getActivity().finish();
            }
        });

        if (User.getCurrentUser().getType().equalsIgnoreCase("admin") ||
                User.getCurrentUser().getType().equalsIgnoreCase("super")){
            lytNB.setVisibility(View.VISIBLE);
        }else {
            lytNB.setVisibility(View.GONE);
        }

        std_att.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), AttendanceReport.class));
                // getActivity().finish();
            }
        });

        staff_att.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), StaffAttendanceReport.class));
                // getActivity().finish();
            }
        });

        textToSpeech = new TextToSpeech(getActivity(), this);
        //this is for url based banner....


        // b_adapter = new B_SliderAdapter(new_sliderDataArrayList, getActivity());
        commonProgress = new CommonProgress(getActivity());

        if (User.getCurrentUser().getSchoolData().getName() != null) {
            headtext.setText(User.getCurrentUser().getSchoolData().getName());
        }
        rq = CustomVolleyRequest.getInstance(getActivity()).getRequestQueue();
        sliderImg = new ArrayList<>();
        viewPager = (ViewPager) view.findViewById(R.id.viewPager);
        sliderDotspanel = (LinearLayout) view.findViewById(R.id.SliderDots);
        //Toast.makeText(getActivity(),"Count "+count,Toast.LENGTH_LONG).show();
        handler1 = new Handler();

        if (load == 0) {
            load++;
            commonProgress.show();
            // ErpProgress.showProgressBar(getActivity(), "Please Wait........!");
        }
        startAnimation();
        //sendRequest();
        //  checkView();

        userprofile_deatils();
        mFloatingNavigationView = (FloatingNavigationView) view.findViewById(R.id.floating_navigation_view);
        View headerView = mFloatingNavigationView.getHeaderView(0);
        navi_profile = (CircularImageView) headerView.findViewById(R.id.navi_profile);
        admin_name = (TextView) headerView.findViewById(R.id.stu_name);
        post = (TextView) headerView.findViewById(R.id.class_namessss);
        admin_name.setText(User.getCurrentUser().getFirstname() + " " + User.getCurrentUser().getLastname());
        edit_profile = (ImageView) headerView.findViewById(R.id.edit_profile);
        if (!restorePrefData()) {
            setShowcaseView();
        }

        view_notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                view_notification.startAnimation(animation4);
                startActivity(new Intent(getActivity(), NotificationAct.class));
                // Animatoo.animateShrink(getActivity());
            }
        });

        std_txtMsg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                std_txtMsg.startAnimation(animation4);

                Intent intent= new Intent(getActivity(), StudentBirthdayList.class);
                intent.putExtra("id","weekly");
                startActivity(intent);

                //  startActivity(new Intent(getActivity(), StudentBirthdayList.class));
                // Animatoo.animateShrink(getActivity());
            }
        });

        staff_txtMsg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                staff_txtMsg.startAnimation(animation4);
                //   startActivity(new Intent(getActivity(), StaffBirthdayList.class));
                Intent intent= new Intent(getActivity(), StaffBirthdayList.class);
                intent.putExtra("id","weekly");
                startActivity(intent);
                // Animatoo.animateShrink(getActivity());
            }
        });

        std_birth_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                std_txtMsg.startAnimation(animation4);

                Intent intent= new Intent(getActivity(), StudentBirthdayList.class);
                intent.putExtra("id","weekly");
                startActivity(intent);
                // Animatoo.animateShrink(getActivity());
            }
        });

        staff_birth_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                staff_txtMsg.startAnimation(animation4);
                Intent intent= new Intent(getActivity(), StaffBirthdayList.class);
                intent.putExtra("id","weekly");
                startActivity(intent);
                // Animatoo.animateShrink(getActivity());
            }
        });

        edit_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                edit_profile.startAnimation(animation4);
                startActivity(new Intent(getActivity(), UpdateProfileActivity.class));
                // Animatoo.animateShrink(getActivity());
            }
        });
        setUserData();
        fetchthoughtlist();

        userprofile();
//        thought.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                startActivity(new Intent(getContext(), TestingActivity.class));
//            }
//        });
//        if(User.getCurrentUser().getProfile()!=null) {
//            Picasso.with(getActivity()).load(User.getCurrentUser().getProfile()).placeholder(R.drawable.boy).into(navi_profile);
//        }

        mFloatingNavigationView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mFloatingNavigationView.open();
            }
        });
        mFloatingNavigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem item) {

                if (item.getTitle().equals("Change Password")) {
                    // Toast.makeText(getActivity(),"Change Password",Toast.LENGTH_SHORT).show();
                    // reset_password();
                } else if (item.getTitle().equals("Feedback")) {
                    Toast.makeText(getActivity(), "Feedback", Toast.LENGTH_SHORT).show();
                    //  open_changePassword();
                } else if (item.getTitle().equals("Rate Us")) {
                    Uri uri = Uri.parse("market://details?id=" + getActivity().getPackageName());
                    Intent myAppLinkToMarket = new Intent(Intent.ACTION_VIEW, uri);
                    try {
                        startActivity(myAppLinkToMarket);
                    } catch (ActivityNotFoundException e) {

                    }
                } else if (item.getTitle().equals("Share Us")) {
                    try {
                        Intent i = new Intent(Intent.ACTION_SEND);
                        i.setType("text/plain");
                        i.putExtra(Intent.EXTRA_SUBJECT, "XYZ Ultimate School");
                        String sAux = "\nLet me recommend you this application\n\n";
                        sAux = sAux + "https://play.google.com/store/apps/details?id=" + getActivity().getPackageName() + "\n\n";
                        i.putExtra(Intent.EXTRA_TEXT, sAux);
                        startActivity(Intent.createChooser(i, "choose one"));
                    } catch (Exception e) {
                        //e.toString();
                    }
                } else if (item.getTitle().equals("Privacy Policy")) {
                    Toast.makeText(getActivity(), "Privacy Policy", Toast.LENGTH_SHORT).show();
                    BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(getActivity());
                    final View sheetView = getLayoutInflater().inflate(R.layout.privacy_policy_dialog, null);
                    mBottomSheetDialog.setContentView(sheetView);
                    mBottomSheetDialog.setCancelable(false);
                    final boolean[] loadingFinished = {true};
                    final boolean[] redirect = {false};
                    WebView webView = (WebView) sheetView.findViewById(R.id.privacy);

                    ErpProgress.showProgressBar(getActivity(), "Please Wait.......!");
                    if (!loadingFinished[0]) {
                        redirect[0] = true;
                    }
                    loadingFinished[0] = false;
                    webView.getSettings().setJavaScriptEnabled(true);
                    String url = "http://ultimatesolutiongroup.com/privacy_policy.html";
                    webView.loadUrl(url);
                    webView.setWebViewClient(new WebViewClient() {
                        @Override
                        public void onPageFinished(WebView view, String url) {
                            if (!redirect[0]) {
                                loadingFinished[0] = true;
                            }
                            if (loadingFinished[0] && !redirect[0]) {
                                ErpProgress.cancelProgressBar();
                            } else {
                                redirect[0] = false;
                            }
                        }
                    });

                    RelativeLayout btnNo = (RelativeLayout) sheetView.findViewById(R.id.btnNo);
                    btnNo.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            btnNo.startAnimation(animation4);
                            mBottomSheetDialog.dismiss();
                        }
                    });
                    mBottomSheetDialog.show();
                } else if (item.getTitle().equals("App Info")) {
                    Toast.makeText(getActivity(), "App Info", Toast.LENGTH_SHORT).show();
                    oenInfoDialog();
                } else {
                    // Snackbar.make((View) mFloatingNavigationView.getParent(), item.getTitle() + " Selected!", Snackbar.LENGTH_SHORT).show();
                    //    Toast.makeText(getActivity(),"Logout!",Toast.LENGTH_SHORT).show();

                    final Dialog logoutDialog = new Dialog(getActivity());
                    logoutDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    logoutDialog.setCancelable(true);
                    logoutDialog.setContentView(R.layout.logout_dialog);
                    logoutDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                    TextView txt2 = (TextView) logoutDialog.findViewById(R.id.txt2);

                    String logout_user = User.getCurrentUser().getFirstname();
                    txt2.setText("Are you sure" + " " + logout_user + "!" + " " + "want to logout?");

//                    CircularImageView img = (CircularImageView) logoutDialog.findViewById(R.id.img);
//
//                    if (User.getCurrentUser().getProfile() != null) {
//                        Picasso.with(getActivity()).load(User.getCurrentUser().getProfile()).placeholder(R.drawable.boy).into(navi_profile);
//                    } else {
//                        Picasso.with(getActivity()).load(R.drawable.boy).into(navi_profile);
//                    }

                    Button btnNo = (Button) logoutDialog.findViewById(R.id.btnNo);
                    btnNo.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            logoutDialog.dismiss();
                        }
                    });
                    Button btnYes = (Button) logoutDialog.findViewById(R.id.btnYes);
                    btnYes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            btnYes.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    logoutDialog.dismiss();
                                    ErpProgress.showProgressBar(getContext(), "Please wait...");
                                    User.getCurrentUser().logout();
                                    Toast.makeText(getContext(), "Logged out successfully", Toast.LENGTH_SHORT).show();
                                    Intent loginpage = new Intent(getContext(), LoginActivity.class);
                                    loginpage.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                                            Intent.FLAG_ACTIVITY_CLEAR_TASK |
                                            Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(loginpage);
                                    ErpProgress.cancelProgressBar();
                                    getActivity().finish();
                                }
                            });
                        }
                    });
                    logoutDialog.show();

                }

                //  Snackbar.make((View) mFloatingNavigationView.getParent(), item.getTitle() + " Selected!", Snackbar.LENGTH_SHORT).show();
                mFloatingNavigationView.close();


                Snackbar.make((View) mFloatingNavigationView.getParent(), item.getTitle() + " Selected!", Snackbar.LENGTH_SHORT).show();
                mFloatingNavigationView.close();
                return true;
            }
        });



        /* holiday section*/
        holidayList = new ArrayList<>();
        hAdapter = new Holidayadapter(holidayList, getActivity(), 5,this);
        holiRV.setAdapter(hAdapter);


        /* Notification section*/
        notificationBeans = new ArrayList<>();
        LinearLayoutManager layoutManager1 = new LinearLayoutManager(getActivity());
        notiRV.setLayoutManager(layoutManager1);
//        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(nbRV.getContext(),
//                layoutManager.getOrientation());
//        nbRV.addItemDecoration(dividerItemDecoration);
        notificationAdapter = new NotificationAdapter(notificationBeans, this, getActivity());
        notiRV.setAdapter(notificationAdapter);


        /* Notice board section*/
        noticeList = new ArrayList<>();
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        nbRV.setLayoutManager(layoutManager);
//        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(nbRV.getContext(),
//                layoutManager.getOrientation());
//        nbRV.addItemDecoration(dividerItemDecoration);
        nbAdapter = new NoticeBoardAdapter(noticeList, getActivity());
        nbRV.setAdapter(nbAdapter);
        //sendRequest2();
    }

    private static int currentPage = 0;
    private static int NUM_PAGES = 0;
    Timer swipeTimer;
    Handler handler1;

    private void setUpSliderShow(ArrayList<NewsData> newSliderDataArrayList) {

        NUM_PAGES =newSliderDataArrayList.size();
        b_adapter = new B_SliderAdapter(getContext(), new_sliderDataArrayList);
        viewPager.setAdapter(b_adapter);
        indicator.setViewPager(viewPager);
        // Auto start of viewpager
        // Ensure handler1 and Update Runnable are not null
        final Runnable Update = new Runnable() {
            public void run() {
                if (viewPager == null || b_adapter == null) {
                    return; // Exit if viewPager or b_adapter is null
                }
                if (currentPage == NUM_PAGES) {
                    currentPage = 0;
                }
                viewPager.setCurrentItem(currentPage++, true);
            }
        };

        // Stop the previous timer if it's running
        if (swipeTimer != null) {
            swipeTimer.cancel();
            swipeTimer.purge();
        }

        // Create a new timer
        swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                if (handler1 != null) {
                    handler1.post(Update);
                }
            }
        }, 2000, 2000);

        // Pager listener over indicator
        indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                currentPage = position;
            }
            @Override
            public void onPageScrolled(int pos, float arg1, int arg2) {
            }
            @Override
            public void onPageScrollStateChanged(int pos) {

            }
        });


    }

    private void openBirth() {
        final BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(getContext());
        final View sheetView = getLayoutInflater().inflate(R.layout.noticeboard_dialog, null);
        mBottomSheetDialog.setContentView(sheetView);

        Button add= (Button) sheetView.findViewById(R.id.add);
        Button vieww= (Button) sheetView.findViewById(R.id.view);
        ImageView img= (ImageView) sheetView.findViewById(R.id.img);
        TextView txtTitle= (TextView) sheetView.findViewById(R.id.txtTitle);

        add.setText("Student");
        vieww.setText("Staff");
        txtTitle.setText("Birthday List");
        img.setImageDrawable(getResources().getDrawable(R.drawable.birthday_cake));

        sheetView.findViewById(R.id.add).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), StudentBirthdayList.class));
            }
        });
        sheetView.findViewById(R.id.view).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), StaffBirthdayList.class));
            }
        });
        RelativeLayout btnNo= (RelativeLayout) sheetView.findViewById(R.id.btnNo);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnNo.startAnimation(animation4);
                mBottomSheetDialog.dismiss();
            }
        });


        mBottomSheetDialog.show();
    }



    private void userprofile_deatils() {
        commonProgress.show();
        HashMap<String, String> params = new HashMap<>();
        params.put("user_id", User.getCurrentUser().getId());
        params.put("check", "fetch");

        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.UPDATE_SCHOOL_INFO, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                commonProgress.dismiss();
                ErpProgress.cancelProgressBar();
                if (error == null) {
                    try {

                        today_a_s = jsonObject.getJSONObject(Constants.USERDATA).getString("paid_amount_datewise");
                        today_txtMsg_s = jsonObject.getJSONObject(Constants.USERDATA).getString("paid_amount_datewise_rc");
                        weekly_a_s = jsonObject.getJSONObject(Constants.USERDATA).getString("paid_amount_weekwise");
                        weekly_txtMsg_s = jsonObject.getJSONObject(Constants.USERDATA).getString("paid_amount_weekwise_rc");
                        monthly_a_s = jsonObject.getJSONObject(Constants.USERDATA).getString("paid_amount_monthwise");
                        monthly_txtMsg_s = jsonObject.getJSONObject(Constants.USERDATA).getString("paid_amount_monthwise_rc");

                        staff_birth_c = jsonObject.getJSONObject(Constants.USERDATA).getString("staff_count");
                        std_birth_c = jsonObject.getJSONObject(Constants.USERDATA).getString("std_count");

                        yearly_a_s = jsonObject.getJSONObject(Constants.USERDATA).getString("paid_amount_yearwise");
                        yearly_txtMsg_s = jsonObject.getJSONObject(Constants.USERDATA).getString("paid_amount_yearwise_rc");


                        if (staff_birth_c != null) {
                            staff_birth.setText(staff_birth_c);
                        } else {
                            staff_birth.setText("0");
                        }

                        if (std_birth_c != null) {
                            std_birth.setText(std_birth_c);
                        } else {
                            std_birth.setText("0");
                        }


                        if (today_a_s != null) {
                            today_a.setText(today_a_s + "/-");
                        } else {
                            today_a.setText("0.00");
                        }

                        if (today_txtMsg_s != null) {
                            today_txtMsg.setText(today_txtMsg_s + "-Entries");
                        } else {
                            today_txtMsg.setText("");
                        }

//                        if (weekly_a_s != null) {
//                            weekly_a.setText(weekly_a_s + "/-");
//                        } else {
//                            weekly_a.setText("0.00");
//                        }
//
//                        if (weekly_txtMsg_s != null) {
//                            weekly_txtMsg.setText(weekly_txtMsg_s + "-Entries");
//                        } else {
//                            weekly_txtMsg.setText("");
//                        }

                        if (monthly_a_s != null) {
                            weekly_a.setText(monthly_a_s + "/-");
                        } else {
                            weekly_a.setText("0.00");
                        }

                        if (monthly_txtMsg_s != null) {
                            weekly_txtMsg.setText(monthly_txtMsg_s + "-Entries");
                        } else {
                            weekly_txtMsg.setText("");
                        }

                        if (yearly_a_s != null) {
                            monthly_a.setText(yearly_a_s + "/-");
                        } else {
                            monthly_a.setText("0.00");
                        }

                        if (yearly_txtMsg_s != null) {
                            monthly_txtMsg.setText(yearly_txtMsg_s + "-Entries");
                        } else {
                            monthly_txtMsg.setText("");
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }, getActivity(), params);
    }

    private void newslist() {
        // i++;
        // cancelProgressBar.show();
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.NEWS_URL, apiCallback5, getActivity(), null);

    }

    ApiHandler.ApiCallback apiCallback5 = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            //  cancelProgressBar.dismiss();

            if (error == null) {
                try {
                    if (new_sliderDataArrayList != null) {
                        new_sliderDataArrayList.clear();
                    }
                    JSONArray jsonArray = jsonObject.getJSONArray("news_data");
                    Log.e("jsonArray", String.valueOf(jsonArray));
                    new_sliderDataArrayList = NewsData.parseNewsDataArrayyy(jsonArray);

                    Log.e("image2", String.valueOf(new_sliderDataArrayList.get(0).getImage()));
                    if (new_sliderDataArrayList.get(0).getDate() != null) {
                        Log.e("image", new_sliderDataArrayList.get(0).getDate());
                    } else {
                        Log.e("image", "empty");
                    }
//                    //Top SliderView
                    setUpSliderShow(new_sliderDataArrayList);

                    shimmer.stopShimmer();
                    shimmer.setVisibility(View.GONE);
                    f_view1.setVisibility(View.GONE);
                    lyt1.setVisibility(View.VISIBLE);
                    //setanimation on adapter...
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                shimmer.stopShimmer();
                shimmer.setVisibility(View.GONE);
                f_view1.setVisibility(View.GONE);
                lyt1.setVisibility(View.VISIBLE);
                //      Toast.makeText(getActivity(),error.getMessage(),Toast.LENGTH_SHORT).show();
            }
        }
    };

    private void setShowcaseView() {

        new TapTargetSequence(getActivity())
                .targets(
                        TapTarget.forView(mFloatingNavigationView, "Option Menu Button", "Tap the Option Menu Button for using multiple options")
                                .outerCircleColor(R.color.light)
                                .outerCircleAlpha(0.96f)
                                .targetCircleColor(R.color.white)
                                .titleTextSize(20)
                                .titleTextColor(R.color.white)
                                .descriptionTextSize(14)
                                .descriptionTextColor(R.color.dark_black)
                                .textColor(R.color.dark_black)
                                .textTypeface(Typeface.SANS_SERIF)
                                .dimColor(R.color.dark_black)
                                .drawShadow(true)
                                .cancelable(false)
                                .tintTarget(true)
                                .transparentTarget(true)
                                .targetRadius(60)).listener(new TapTargetSequence.Listener() {
                    @Override
                    public void onSequenceFinish() {
                        //Toast.makeText(getActivity(),"Sequence Finished",Toast.LENGTH_SHORT).show();
                        savePrefData();
                    }

                    @Override
                    public void onSequenceStep(TapTarget lastTarget, boolean targetClicked) {

                        // Toast.makeText(getActivity(),"GREAT!",Toast.LENGTH_SHORT).show();

                    }

                    @Override
                    public void onSequenceCanceled(TapTarget lastTarget) {

                    }
                }).start();
    }

//    public void sendRequest2() {
//      //  Toast.makeText(getActivity(), "sendRequest", Toast.LENGTH_SHORT).show();
//        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, request_url, null, new Response.Listener<JSONArray>() {
//
//            @Override
//            public void onResponse(JSONArray response) {
//
//                Log.e("Response", String.valueOf(response));
//                Log.e("Length", String.valueOf(response.length()));
//                for (int i = 0; i < response.length(); i++) {
//
//                    BannerBean sliderUtils = new BannerBean();
//
//                    try {
//                        JSONObject jsonObject = response.getJSONObject(i);
//                        // comment is original code...
//                        //sliderUtils.setCateImage(request_url1+jsonObject.getString("CateImage"));
//                        sliderUtils.setBaner_image(request_url1+jsonObject.getString("baner_image"));
//                        sliderUtils.setTitle(jsonObject.getString("title"));
//                        Log.e("Image_url",sliderUtils.getBaner_image());
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }
//
//                    sliderItemList.add(sliderUtils);
//
//                }
//
//                adapter.renewItems(sliderItemList);
//
//
//            }
//        }, new Response.ErrorListener() {
//
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show();
//
//                Log.e("whatserror",error.getMessage());
//            }
//        });
//
//        CustomVolleyRequest.getInstance(getActivity()).addToRequestQueue(jsonArrayRequest);
//
//    }


    private void savePrefData() {
        sharedPreferences = getActivity().getSharedPreferences("boarding_pref", MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean("IsFirstTimeVisit", true);
        editor.apply();
    }

    private boolean restorePrefData() {
        sharedPreferences = getActivity().getSharedPreferences("boarding_pref", MODE_PRIVATE);
        return sharedPreferences.getBoolean("IsFirstTimeVisit", false);
    }


    @SuppressLint("RestrictedApi")
    private void oenInfoDialog() {


        mBottomSheetDialog = new BottomSheetDialog(getActivity());
        final View sheetView = getLayoutInflater().inflate(R.layout.app_info_dialog, null);
        mBottomSheetDialog.setContentView(sheetView);

        TextView txtVersion = (TextView) sheetView.findViewById(R.id.txtVersion);
        FloatingActionButton whatsapp = (FloatingActionButton) sheetView.findViewById(R.id.whatsapp);
        FloatingActionButton email = (FloatingActionButton) sheetView.findViewById(R.id.email);
        FloatingActionButton phone = (FloatingActionButton) sheetView.findViewById(R.id.phone);
        String number = "7009091606";
        String versionName = "";
        int code = 0;
        try {
            versionName = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0).versionName;
            code = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0).versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        txtVersion.setText("App Version Code: " + code + "\n" + "App Version Name: " + String.valueOf(versionName));

        email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //  mBottomSheetDialog.dismiss();
                ErpProgress.showProgressBar(getActivity(), "Please Wait.....!");
                Intent intent = new Intent(Intent.ACTION_SENDTO); // it's not ACTION_SEND
                intent.setType("text/plain");
                intent.putExtra(Intent.EXTRA_SUBJECT, "Subject of email");
                intent.putExtra(Intent.EXTRA_TEXT, "Body of email");
                //simplydirect email for setting email,,,
                intent.setData(Uri.parse("mailto:dealingkings112@gmail.com")); // or just "mailto:" for blank
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK); // this will make such that when user returns to your app, your app is displayed, instead of the email app.
                startActivity(intent);
                ErpProgress.cancelProgressBar();

            }
        });
        whatsapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //  mBottomSheetDialog.dismiss();
                Uri uri = Uri.parse("smsto:" + number);
                Intent i = new Intent(Intent.ACTION_SENDTO, uri);
                i.setPackage("com.whatsapp");
                startActivity(Intent.createChooser(i, "Chat Now!"));
            }
        });
        phone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // mBottomSheetDialog.dismiss();
                // for permission granted....
                if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CALL_PHONE}, 1);
                } else {
                    Intent intent = new Intent(Intent.ACTION_CALL);
                    intent.setData(Uri.parse("tel:" + number));
                    startActivity(intent);
                    ErpProgress.cancelProgressBar();
                }
            }
        });


        RelativeLayout btnNo = (RelativeLayout) sheetView.findViewById(R.id.btnNo);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnNo.startAnimation(animation4);
                mBottomSheetDialog.dismiss();
            }
        });
        mBottomSheetDialog.show();


    }

    public void logoutDialog() {
        final Dialog logoutDialog = new Dialog(getActivity());
        logoutDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        logoutDialog.setCancelable(true);
        logoutDialog.setContentView(R.layout.logout_dialog);
        logoutDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        Button btnNo = logoutDialog.findViewById(R.id.btnNo);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                logoutDialog.dismiss();
            }
        });
        Button btnYes = logoutDialog.findViewById(R.id.btnYes);
        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                logoutDialog.dismiss();
                ErpProgress.showProgressBar(getContext(), "Please wait...");
                User.getCurrentUser().logout();
                Toast.makeText(getContext(), "Logged out successfully", Toast.LENGTH_SHORT).show();
                Intent loginpage = new Intent(getContext(), LoginActivity.class);
                loginpage.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                        Intent.FLAG_ACTIVITY_CLEAR_TASK |
                        Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(loginpage);
                ErpProgress.cancelProgressBar();
                getActivity().finish();
            }
        });
        logoutDialog.show();
    }

    @Override
    public void onResume() {
        super.onResume();
        fetchNotification();
        newslist();
        fetchNoticeBoardList();
        fetchHolidayList();
        fetchthoughtlist();
        userprofile();
        checkUserpermission();
        fetchtFeeslist();
        fetchtStaffAttendlist();
//        fetchNotification();
//        fetchWeekAttend();

    }

    private void checkUserpermission() {
        HashMap<String, String> params = new HashMap<>();
        params.put("user_id", User.getCurrentUser().getId());
        params.put("check", "");
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.USER_PERMISSION, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                if (error == null) {
                    try {
                        Log.e("USERDATA", String.valueOf(jsonObject.getJSONObject("admin_data")));
//
//                        if (UserPermissions.getCurrentUserPermissions()!=null){
//                            UserPermissions.logoutUserPermissions();
//                            user_obj = jsonObject.getJSONObject("admin_data");
//                            UserPermissions.setCurrentUserPermissions(user_obj);
//                        }else{

                        JSONObject user_obj = jsonObject.getJSONObject("admin_data");
                        UserPermissions.setCurrentUserPermissions(user_obj);
                        // }


                        //  setup= jsonObject.getJSONObject("admin_data").getString("setup");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    // fees_count.setText(jsonObject.getJSONObject("doc_data").getString("noti_count"));

                } else {
                    // msg_count.setText("0");
                    // Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_LONG).show();
                    //Utils.showSnackBar(error.getMessage(), parent);
                }
            }
        }, getActivity(), params);
    }


    public void fetchNotification() {
        HashMap<String, String> params = new HashMap<>();
        params.put("id", User.getCurrentUser().getId());
        params.put("type", "15");
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.NOTIFICATIONURL, notiapiCallback, getActivity(), params);
    }

    ApiHandler.ApiCallback notiapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            if (error == null) {
                try {
                    JSONArray noticeArray = jsonObject.getJSONArray("notice_data");
                    notificationBeans = NotificationBean.parseNotificationArray(noticeArray);
                    if (notificationBeans.size() > 0) {
                        notificationAdapter.setNotificationList(notificationBeans);
                        notificationAdapter.notifyDataSetChanged();
                        notiRV.setVisibility(View.VISIBLE);
                        notification_shimmer.stopShimmer();
                        notification_shimmer.setVisibility(View.GONE);
                        noNotificationData.setVisibility(View.GONE);
                    } else {
                        notification_shimmer.stopShimmer();
                        notification_shimmer.setVisibility(View.GONE);
                        //lytNB.setVisibility(View.GONE);
                        notificationAdapter.setNotificationList(notificationBeans);
                        notificationAdapter.notifyDataSetChanged();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                notification_shimmer.stopShimmer();
                notification_shimmer.setVisibility(View.GONE);
                // lytNB.setVisibility(View.GONE);
                notificationBeans.clear();
                notificationAdapter.setNotificationList(notificationBeans);
                notificationAdapter.notifyDataSetChanged();
                notiRV.setVisibility(View.GONE);
                noNotificationData.setVisibility(View.VISIBLE);


            }
        }
    };

    private void userprofile() {
        // UltimateProgress.showProgressBar(getActivity(),"Please Wait........!");
        HashMap<String, String> params = new HashMap<>();
        params.put("user_id", User.getCurrentUser().getId());
        params.put("image", "");


        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.UPDATE_IMG, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                commonProgress.dismiss();
                ErpProgress.cancelProgressBar();
                if (error == null) {
                    try {
                        image_url = jsonObject.getJSONObject(Constants.USERDATA).getString("profile");
                        Log.i("USERDATA-Home", image_url);

                        // Utils.setNaviHeaderDatas(stu_name,class_namessss,navi_profile,image_url,getActivity());
                        Utils.setNaviHeaderDatas(admin_name, post, navi_profile, image_url, getActivity());

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }, getActivity(), params);
    }


    private void fetchNoticeBoardList() {
        HashMap<String, String> params = new HashMap<>();
        params.put("type", "1");
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.NOTICE_URL, napiCallback, getActivity(), params);

    }

    ApiHandler.ApiCallback napiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            //  ErpProgress.cancelProgressBar();
            if (error == null) {
                try {
                    JSONArray noticeArray = jsonObject.getJSONArray("notice_data");
                    noticeList = NoticeBean.parseNoticetArray(noticeArray);
                    if (noticeList.size() > 0) {
                        nbAdapter.setNList(noticeList);
                        nbAdapter.notifyDataSetChanged();
                        nbRV.setVisibility(View.VISIBLE);
                        noti_shimmer.stopShimmer();
                        noti_shimmer.setVisibility(View.GONE);
                        noNoticeData.setVisibility(View.GONE);
                    } else {
                        noti_shimmer.stopShimmer();
                        noti_shimmer.setVisibility(View.GONE);
                        //lytNB.setVisibility(View.GONE);
                        nbAdapter.setNList(noticeList);
                        nbAdapter.notifyDataSetChanged();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                noti_shimmer.stopShimmer();
                noti_shimmer.setVisibility(View.GONE);
                // lytNB.setVisibility(View.GONE);
                noticeList.clear();
                nbAdapter.setNList(noticeList);
                nbAdapter.notifyDataSetChanged();
                nbRV.setVisibility(View.GONE);
                noNoticeData.setVisibility(View.VISIBLE);
                if (error.getStatusCode() == 405) {
                    User.logout();
                    startActivity(new Intent(getActivity(), LoginActivity.class));
                    getActivity().finish();
                }

            }
        }
    };


    private void fetchtStaffAttendlist() {
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("check", "main");
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.STAFF_ATTEND_COUNT_URL, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                if (error == null) {
                    try {
                        ArrayList<ClassBean> classList = ClassBean.parseClassArray(jsonObject.getJSONArray(Constants.CLASSDATA));
                        Log.e("A_staff",classList.get(0).getA_student());
                        Log.e("L_staff",classList.get(0).getL_student());
                        Log.e("P_staff",classList.get(0).getP_student());
                        int l = 0, a = 0, p = 0, sum = 0;
                        l = Integer.parseInt(classList.get(0).getL_student());
                        a = Integer.parseInt(classList.get(0).getA_student());
                        p = Integer.parseInt(classList.get(0).getP_student());
                        sum = l + a + p;
                        l_count_staff.setText(String.valueOf(l));
                        a_count_staff.setText(String.valueOf(a));
                        p_count_staff.setText(String.valueOf(p));
                        total_staff_m.setText("Mark Attendance: " + String.valueOf(sum));

                        total_staff.setText("Total Staff: " + String.valueOf(classList.get(0).getTotal_std()));

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    // Utils.showSnackBar(error.getMessage(), parent);
                }
            }
        }, getActivity(), params);

    }

    private void fetchtFeeslist() {
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("check", "main");
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.CLASSLIST_FEES_URL, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                if (error == null) {
                    try {
                        ArrayList<ClassBean> classList = ClassBean.parseClassArray(jsonObject.getJSONArray(Constants.CLASSDATA));
//                        Log.e("A_student",classList.get(0).getA_student());
//                        Log.e("L_student",classList.get(0).getL_student());
//                        Log.e("P_student",classList.get(0).getP_student());
                        int l = 0, a = 0, p = 0, sum = 0;
                        l = Integer.parseInt(classList.get(0).getL_student());
                        a = Integer.parseInt(classList.get(0).getA_student());
                        p = Integer.parseInt(classList.get(0).getP_student());
                        sum = l + a + p;
                        l_count.setText(String.valueOf(l));
                        a_count.setText(String.valueOf(a));
                        p_count.setText(String.valueOf(p));
                        total_students_m.setText("Mark Attendance: " + String.valueOf(sum));

                        total_students.setText("Total Students: " + String.valueOf(classList.get(0).getTotal_std()));

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    // Utils.showSnackBar(error.getMessage(), parent);
                }
            }
        }, getActivity(), params);

    }


    private void fetchthoughtlist() {
        // ErpProgress.showProgressBar(getActivity(),"Please wait...");
        HashMap<String, String> params = new HashMap<String, String>();
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.VIEWTHOUGHT, apiCallbackgggg, getActivity(), params);

    }

    ApiHandler.ApiCallback apiCallbackgggg = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            commonProgress.dismiss();

            if (error == null) {
                try {
                    if (eventList != null) {
                        eventList.clear();
                    }
                    JSONArray jsonArray = jsonObject.getJSONArray("gallery_data");
                    eventList = Thought_bean.parseALBMlistarray(jsonArray);

                    thought_shimmer.stopShimmer();
                    thought_shimmer.setVisibility(View.GONE);
                    thought.setVisibility(View.VISIBLE);
                    thought.setText(eventList.get(0).getMessage());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                thought_shimmer.stopShimmer();
                thought_shimmer.setVisibility(View.GONE);
                thought.setVisibility(View.GONE);
                eventList.clear();

            }
        }
    };

    private void fetchHolidayList() {
        HashMap<String, String> params = new HashMap<>();
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.HOLIDAY_URL, apiCallback, getActivity(), params);
    }

    ApiHandler.ApiCallback apiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            if (error == null) {
                try {
                    JSONArray hArray = jsonObject.getJSONArray("holiday_data");
                    holidayList = HolidayBean.parseArray(hArray);
                    if (holidayList.size() > 0) {
                        hAdapter.setHList(holidayList);
                        hAdapter.notifyDataSetChanged();
                        holiRV.setVisibility(View.VISIBLE);
                        holiday_shimmer.stopShimmer();
                        holiday_shimmer.setVisibility(View.GONE);
                        noHolidayData.setVisibility(View.GONE);
                    } else {
                        holiday_shimmer.stopShimmer();
                        holiday_shimmer.setVisibility(View.GONE);
                        // lytHolidy.setVisibility(View.GONE);
                        noHolidayData.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                //  lytHolidy.setVisibility(View.GONE);
                holiday_shimmer.stopShimmer();
                holiday_shimmer.setVisibility(View.GONE);
                holidayList.clear();
                hAdapter.setHList(holidayList);
                hAdapter.notifyDataSetChanged();
                holiRV.setVisibility(View.GONE);
                noHolidayData.setVisibility(View.VISIBLE);
                if (error.getStatusCode() == 405) {
                    User.logout();
                    startActivity(new Intent(getActivity(), LoginActivity.class));
                    getActivity().finish();
                }
            }
        }
    };

    private void startAnimation() {
        animation1 = AnimationUtils.loadAnimation(getActivity(), R.anim.logo_animation);
        animation2 = AnimationUtils.loadAnimation(getActivity(), R.anim.enter_from_left_animation);
        animation3 = AnimationUtils.loadAnimation(getActivity(), R.anim.fade_animation);
        animation4 = AnimationUtils.loadAnimation(getActivity(), R.anim.btn_blink_animation);
        noHolidayData.startAnimation(animation1);
        noNoticeData.startAnimation(animation1);
        imgStud.startAnimation(animation2);
        txtStudName.startAnimation(animation3);
        class_name.startAnimation(animation3);
        roll_no.startAnimation(animation3);
    }

    private void checkView() {
//        Toast.makeText(getActivity(), "checkView", Toast.LENGTH_SHORT).show();
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                for (int i = 0; i < dotscount; i++) {
                    dots[i].setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.unactive_dots));
                }
                dots[position].setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.active_dots));
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

//        if (dotscount == 0){
//            Toast.makeText(getActivity(),"Next "+dotscount,Toast.LENGTH_SHORT).show();
//            createSlideShow();
//        } else {
//            Toast.makeText(getActivity(),"Stop ",Toast.LENGTH_SHORT).show();
//        }

    }

    private void createSlideShow() {
        //  handler= new Handler();
        runnable = new Runnable() {
            @Override
            public void run() {
                if (current_pos == dotscount) {
                    current_pos = 0;
                }
                viewPager.setCurrentItem(current_pos++, true);
            }
        };

        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(runnable);
            }

        }, 2500, 2500);

    }

    public void sendRequest() {
        //  Toast.makeText(getActivity(), "sendRequest", Toast.LENGTH_SHORT).show();
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, request_url, null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                ErpProgress.cancelProgressBar();
                commonProgress.dismiss();
                Log.e("Response", String.valueOf(response));
                Log.e("Length", String.valueOf(response.length()));
                for (int i = 0; i < response.length(); i++) {

                    BannerBean sliderUtils = new BannerBean();

                    try {
                        JSONObject jsonObject = response.getJSONObject(i);
                        // comment is original code...
                        //sliderUtils.setCateImage(request_url1+jsonObject.getString("CateImage"));
                        sliderUtils.setBaner_image(request_url1 + jsonObject.getString("baner_image"));
                        sliderUtils.setTitle(jsonObject.getString("title"));
                        Log.e("Image_url", sliderUtils.getBaner_image());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    sliderImg.add(sliderUtils);

                }

                viewPagerAdapter = new ViewPagerAdapter(sliderImg, getActivity());

                viewPager.setAdapter(viewPagerAdapter);

                dotscount = viewPagerAdapter.getCount();
                // new code
                if (sliderDotspanel.getChildCount() > 0) {
                    sliderDotspanel.removeAllViews();
                }
                // new code
                dots = new ImageView[dotscount];

                for (int i = 0; i < dotscount; i++) {

                    dots[i] = new ImageView(getActivity());
                    dots[i].setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.unactive_dots));

                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);

                    params.setMargins(8, 0, 8, 0);

                    sliderDotspanel.addView(dots[i], params);

                }

                dots[0].setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.active_dots));

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //     Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show();

                // Log.e("whatserror",error.getMessage());
            }
        });

        CustomVolleyRequest.getInstance(getActivity()).addToRequestQueue(jsonArrayRequest);

    }


    private void setUserData() {

        Utils.setNaviHeaderData(txtStudName, class_name, imgStud, getActivity());

        if (User.getCurrentUser().getId() != null) {
            roll_no.setText("Email: " + User.getCurrentUser().getEmail());
        } else {
            roll_no.setText("");
        }

    }

    @OnClick(R.id.imgProfile)
    public void imgProfilesss() {
        imgProfile.startAnimation(animation4);
        Toast.makeText(getActivity(), "View Profile Details", Toast.LENGTH_SHORT).show();
    }

    @OnClick(R.id.view_noticeBoard)
    public void view_noticeBoardss() {
        view_noticeBoard.startAnimation(animation4);
        Toast.makeText(getActivity(), "View Notice Board List", Toast.LENGTH_SHORT).show();
    }

    @OnClick(R.id.view_holidayList)
    public void view_holidayListss() {
        view_holidayList.startAnimation(animation4);
        //Toast.makeText(getActivity(), "View Holiday List", Toast.LENGTH_SHORT).show();
        startActivity(new Intent(getActivity(), AddHolidayActivity.class));
    }

    @Override
    public void deleteNotification(final NotificationBean notiList) {
        speech = 1;
        textholder = notiList.getN_title() + " ! " + notiList.getN_msg();
        TextToSpeechFunction();
    }

    //text to speech programmatically...
    public void TextToSpeechFunction() {
        textToSpeech.speak(textholder, TextToSpeech.QUEUE_FLUSH, null);
    }

    @Override
    public void onInit(int status) {
        if (status == TextToSpeech.SUCCESS) {
            textToSpeech.setLanguage(Locale.US);
            if (speech == 0) {
                TextToSpeechFunction();
            }
        }
    }

    @Override
    public void onDestroy() {
        if (textToSpeech != null) {
            textToSpeech.shutdown();
        }
        super.onDestroy();
    }

    @Override
    public void onDelecallback(HolidayBean homeworkbean) {

    }


}

