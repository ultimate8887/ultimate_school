package com.ultimate.ultimatesmartschool.Home;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.Request;
import com.ultimate.ultimatesmartschool.BeanModule.User;
import com.ultimate.ultimatesmartschool.Login.LoginActivity;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import org.json.JSONObject;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ResetPassActivity extends AppCompatActivity {
    @BindView(R.id.edtPassword)
    EditText edtPassword;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_pass);
        ButterKnife.bind(this);
        txtTitle.setText("Reset Password");
    }

    @OnClick(R.id.imgBack)
    public void callBackF() {
        finish();
    }

    @OnClick(R.id.btnReset)
    public void resetPass() {
        Utils.hideKeyboard(this);
        if (edtPassword.getText().length() >= 5) {
            HashMap<String, String> params = new HashMap<String, String>();
            params.put("user_id", User.getCurrentUser().getId());
            params.put("pass", edtPassword.getText().toString());
            ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.PASS_RESET_URL, apiCallback, this, params);
        } else {
            Utils.showSnackBar("Please enter password atleast 5 letter!", findViewById(R.id.parent));
        }
    }

    ApiHandler.ApiCallback apiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            if (error == null) {
                Utils.showSnackBar("Success!", findViewById(R.id.parent));
                finish();
            } else {
                Utils.showSnackBar("Something went wrong!", findViewById(R.id.parent));
                if (error.getStatusCode() == 405) {
                    User.logout();
                    startActivity(new Intent(ResetPassActivity.this, LoginActivity.class));
                    finish();
                }
            }
        }

    };
}
