package com.ultimate.ultimatesmartschool.Home;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatTextView;

import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.squareup.picasso.Picasso;
import com.ultimate.ultimatesmartschool.BeanModule.User;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AdminProfile extends AppCompatActivity {
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.name)
    AppCompatTextView name;

    @BindView(R.id.admin_email)
    AppCompatTextView admin_email;
    @BindView(R.id.phone)
    AppCompatTextView phone;
    @BindView(R.id.admin_address)
    AppCompatTextView admin_address;
    @BindView(R.id.profile_img)
    ImageView profile_img;
    @BindView(R.id.profile_bg)ImageView profile_bg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_profile);
        ButterKnife.bind(this);
        txtTitle.setText("Admin Detail");
        name.setText(User.getCurrentUser().getFirstname()+"("+User.getCurrentUser().getId()+")");
        admin_email.setText(User.getCurrentUser().getEmail());
        phone.setText(User.getCurrentUser().getPhoneno());
        admin_address.setText(User.getCurrentUser().getAddress());
        if(User.getCurrentUser().getProfile()!=null) {
            Picasso.get().load(User.getCurrentUser().getProfile()).placeholder(R.drawable.stud).into(profile_img);
            Picasso.get().load(User.getCurrentUser().getSchoolData().getLogo()).placeholder(R.drawable.stud).into(profile_bg);
        }
        userprofile();
    }

    private void userprofile() {
        // UltimateProgress.showProgressBar(getActivity(),"Please Wait........!");
        HashMap<String, String> params = new HashMap<>();
        params.put("user_id", User.getCurrentUser().getId());
        params.put("image", "");


        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.UPDATE_IMG, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error ) {
                ErpProgress.cancelProgressBar();
                if (error == null) {
                    try {
                       String image_url=jsonObject.getJSONObject(Constants.USERDATA).getString("profile");
                        Log.i("USERDATA-Home", image_url);

                        // Utils.setNaviHeaderDatas(stu_name,class_namessss,navi_profile,image_url,getActivity());

                        if (!image_url.equalsIgnoreCase("https://ultimatesolutiongroup.com/office_admin/images/admin/")) {
                            Picasso.get().load(image_url).placeholder(R.drawable.boy).into(profile_img);
                        } else {
                            Picasso.get().load(R.drawable.boy).into(profile_img);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }, this, params);
    }

    @OnClick(R.id.imgBack)
    public void onback() {
        finish();
    }
}
