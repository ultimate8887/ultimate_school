package com.ultimate.ultimatesmartschool.Home;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.google.gson.Gson;
import com.ultimate.ultimatesmartschool.BeanModule.User;
import com.ultimate.ultimatesmartschool.Login.LoginActivity;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Staff.Deparment_bean;
import com.ultimate.ultimatesmartschool.Staff.Fragments.UpdateStaff;
import com.ultimate.ultimatesmartschool.Staff.StaffBirthdayList;
import com.ultimate.ultimatesmartschool.Staff.StaffProfile;
import com.ultimate.ultimatesmartschool.Staff.VAdapter_view_staff;
import com.ultimate.ultimatesmartschool.Staff.View_staff_bean;
import com.ultimate.ultimatesmartschool.StaffAttendance.ViewStaffAttendance;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.CommonProgress;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ViewStaffActivity extends AppCompatActivity implements VAdapter_view_staff.StaffCallBack {
    @BindView(R.id.spinnerviewdepartment)
    Spinner spinnerdepart;
    @BindView(R.id.recyclerview_viewstaff)
    RecyclerView recyclerView;
    @BindView(R.id.imgBack)
    ImageView back;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.totalRecord)
    TextView totalRecord;
    //private LinearLayoutManager layoutManager;
    RecyclerView.LayoutManager layoutManager;
    @BindView(R.id.parent)
    RelativeLayout parent;
    ArrayList<Deparment_bean> department_list=new ArrayList<>();
    Adapter_spinner_viewdepart adap;
    String departmentid="";
    private VAdapter_view_staff adapter;
    ArrayList<View_staff_bean> viewstafflist=new ArrayList<>();
    int loaded=0;
    boolean isDark = false;
    EditText searchInput ;
    CharSequence search="";
    CommonProgress commonProgress;
    @BindView(R.id.textNorecord)TextView txtNorecord;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_staff);
        ButterKnife.bind(this);
        txtTitle.setText("Complete Staff List");
        commonProgress=new CommonProgress(ViewStaffActivity.this);
        searchInput = findViewById(R.id.search_input);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        layoutManager = new LinearLayoutManager(ViewStaffActivity.this);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new VAdapter_view_staff(viewstafflist, ViewStaffActivity.this,this);
        recyclerView.setAdapter(adapter);
        fetchdepartment();
        isDark = !isDark ;
        if (isDark) {

            parent.setBackgroundColor(getResources().getColor(R.color.white));
          //  searchInput.setBackgroundResource(R.drawable.search_input_dark_style);

        }
        else {
            parent.setBackgroundColor(getResources().getColor(R.color.white));
            searchInput.setBackgroundResource(R.drawable.search_input_style);
        }
//        if (!search.toString().isEmpty()){
//
//            adapter.getFilter().filter(search);
//
//        }



        searchInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if(s.length()==0)
                {
                    fetchdepartment();
                }else {
                    adapter.getFilter().filter(s);
                    search = s;
                    totalRecord.setText("Search Record:- " +String.valueOf(adapter.viewstafflist.size()));
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
//                if(s.length()==0)
//                {
//                    fetchdepartment();
//                }else {
//                    adapter.getFilter().filter(s);
//                    search = s;
//                    Toast.makeText(getApplicationContext(),s,Toast.LENGTH_LONG).show();
//                }
            }
        });



    }

    private void fetchdepartment() {
        if(loaded==0){
            commonProgress.show();
        }loaded++;
        HashMap<String,String> params=new HashMap<>();
        ApiHandler.apiHit(Request.Method.GET, Constants.getBaseURL() + Constants.deparmrnt, apiCallback, this, params);
    }
    ApiHandler.ApiCallback apiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            commonProgress.dismiss();
            // txtupload.setVisibility(View.VISIBLE);
            if (error == null) {
                try {
                    if (department_list != null) {
                        department_list.clear();
                    }
                    JSONArray jsonArray = jsonObject.getJSONArray("department_data");
                    department_list = Deparment_bean.parseDepartmentArray(jsonArray);
                    adap= new Adapter_spinner_viewdepart(ViewStaffActivity.this,department_list);
                    spinnerdepart.setAdapter(adap);
                    spinnerdepart.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if (i > 0) {
                                departmentid = department_list.get(i-1 ).getId();
                            }
                            fetchstaffdetail(i);
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Utils.showSnackBar(error.getMessage(), parent);
                if (error.getStatusCode() == 405) {
                    User.logout();
                    startActivity(new Intent(ViewStaffActivity.this, LoginActivity.class));
                    ViewStaffActivity.this.finish();
                }
            }
        }
    };

    private void fetchstaffdetail(int i) {
        commonProgress.show();
        HashMap<String, String> params = new HashMap<String, String>();
        if(departmentid.isEmpty()){
            params.put("d_id", "0");
        }else {
            params.put("d_id", departmentid);
        }
        // params.put("class_id", classid);
        if(i==0){
            params.put("all", "1");
        }
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.STAFF_DETAIL, apiCallbackstaffdetail, this, params);

    }

    ApiHandler.ApiCallback apiCallbackstaffdetail = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            commonProgress.dismiss();
            //txtupload.setVisibility(View.VISIBLE);
            if (error == null) {
                try {
                    if (viewstafflist != null) {
                        viewstafflist.clear();
                    }
                    JSONArray jsonArray = jsonObject.getJSONArray("staff_detail");
                    viewstafflist = View_staff_bean.parseviewstaffArray(jsonArray);
                    if (viewstafflist.size() > 0) {
                        adapter.setstaffdetailList(viewstafflist);
                        adapter.notifyDataSetChanged();
                        totalRecord.setText("Total Entries:- "+String.valueOf(viewstafflist.size()));

                    } else {
                        adapter.setstaffdetailList(viewstafflist);
                        adapter.notifyDataSetChanged();
                        totalRecord.setText("Total Entries:- 0");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                txtNorecord.setVisibility(View.VISIBLE);
                viewstafflist.clear();
                adapter.setstaffdetailList(viewstafflist);
                totalRecord.setText("Total Entries:- 0");
                adapter.notifyDataSetChanged();
            }
        }
    };


    @Override
    public void staffCallBack(View_staff_bean viewStaffBean) {
        String phoneNo = viewStaffBean.getPhonno();
        Log.i("email",phoneNo);
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:"+ phoneNo));
        startActivity(intent);
    }

    @Override
    public void onUpdateCallback(View_staff_bean viewStaffBean) {

    }

    @Override
    public void staffemailCallBack(View_staff_bean viewStaffBean) {
        String email = viewStaffBean.getEmail();
        Log.i("email",email);
        Intent intent = new Intent(Intent.ACTION_SENDTO); // it's not ACTION_SEND
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_SUBJECT, "Subject of email");
        intent.putExtra(Intent.EXTRA_TEXT, "Body of email");
        intent.setData(Uri.parse("mailto:"+email)); // or just "mailto:" for blank
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK); // this will make such that when user returns to your app, your app is displayed, instead of the email app.
        startActivity(intent);
    }

    @Override
    public void staffProfile(View_staff_bean viewStaffBean) {
        Gson gson  = new Gson();
        String staffdet = gson.toJson(viewStaffBean,View_staff_bean.class);
        Intent i = new Intent(ViewStaffActivity.this, StaffProfile.class);
        i.putExtra("staffdet",staffdet);
        startActivity(i);
    }
    @Override
    public void onUpdateProfile(View_staff_bean viewStaffBean) {
        Gson gson  = new Gson();
        String staffdet = gson.toJson(viewStaffBean,View_staff_bean.class);
        Intent i = new Intent(ViewStaffActivity.this, UpdateStaff.class);
        i.putExtra("staffdet",staffdet);
        startActivity(i);
    }

    @Override
    public void onDeleteCallback(View_staff_bean viewStaffBean) {

    }
}
