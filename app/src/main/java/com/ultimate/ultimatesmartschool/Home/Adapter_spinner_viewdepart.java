package com.ultimate.ultimatesmartschool.Home;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Staff.Deparment_bean;

import java.util.ArrayList;

public class Adapter_spinner_viewdepart extends BaseAdapter {
    private final ArrayList<Deparment_bean> departlist;
    Context context;
    LayoutInflater inflter;

    public Adapter_spinner_viewdepart(Context applicationContext, ArrayList<Deparment_bean> departlist) {
        this.context = applicationContext;
        this.departlist = departlist;
        inflter = (LayoutInflater.from(applicationContext));
    }

    @Override
    public int getCount() {
        return departlist.size() + 1;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        view = inflter.inflate(R.layout.spinner_lyt, null);
        TextView label = (TextView) view.findViewById(R.id.txtText);
        if (i == 0) {
            // Default selected Spinner item
            label.setText("All");
        } else {
            Deparment_bean classObj = departlist.get(i - 1);
            label.setText(classObj.getName());
        }

        return view;
    }
}
