package com.ultimate.ultimatesmartschool.Home;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.speech.tts.TextToSpeech;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;

import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.andremion.floatingnavigationview.FloatingNavigationView;
import com.android.volley.Request;
import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetSequence;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;
import com.squareup.picasso.Picasso;
import com.ultimate.ultimatesmartschool.Activity.SchoolInfo;
import com.ultimate.ultimatesmartschool.Activity.UserPermissions;
import com.ultimate.ultimatesmartschool.AddmissionForm.SessionAdapter;
import com.ultimate.ultimatesmartschool.AddmissionForm.SessionBean;
import com.ultimate.ultimatesmartschool.AddmissionForm.StudentAdmissionForm;
import com.ultimate.ultimatesmartschool.Assignment.AddAssignment;
import com.ultimate.ultimatesmartschool.Assignment.ReplyAssignmentList;
import com.ultimate.ultimatesmartschool.Assignment.ViewAssignment;
import com.ultimate.ultimatesmartschool.BeanModule.ClassBean;
import com.ultimate.ultimatesmartschool.BeanModule.CommonBean;
import com.ultimate.ultimatesmartschool.BeanModule.Studentbean;
import com.ultimate.ultimatesmartschool.BeanModule.User;
import com.ultimate.ultimatesmartschool.BlockStudent.MarkBlockStutent;
import com.ultimate.ultimatesmartschool.ClassTest.ViewClassWiseTest;
import com.ultimate.ultimatesmartschool.ClassTest.ViewStaffWiseTest;
import com.ultimate.ultimatesmartschool.ClassWork.Classwork_Report;
import com.ultimate.ultimatesmartschool.ClassWork.StaffWiseCWActivity;
import com.ultimate.ultimatesmartschool.ClassWork.ViewClassWork;
import com.ultimate.ultimatesmartschool.DateSheet.AddDateSheetNew;
import com.ultimate.ultimatesmartschool.DateSheet.ViewDatesheet;
import com.ultimate.ultimatesmartschool.DietChart.DietChartActivity;
import com.ultimate.ultimatesmartschool.Ebook.Ebook;
import com.ultimate.ultimatesmartschool.Ebook.ViewEbook;
import com.ultimate.ultimatesmartschool.Examination.Add_Exam;
import com.ultimate.ultimatesmartschool.Examination.CheckReportCard;
import com.ultimate.ultimatesmartschool.Examination.ExamWebviewActivity;
import com.ultimate.ultimatesmartschool.Examination.Exam_Detail_Activity;
import com.ultimate.ultimatesmartschool.Examination.GDSUploadExammrksSubjectWise;
import com.ultimate.ultimatesmartschool.Examination.GazetteWebviewActivity;
import com.ultimate.ultimatesmartschool.Examination.GroupSpinnerAdapter;
import com.ultimate.ultimatesmartschool.Examination.ResultsPermissionsActivity;
import com.ultimate.ultimatesmartschool.Examination.ScheduleActivity;
import com.ultimate.ultimatesmartschool.Fee.AddBankDetails;
import com.ultimate.ultimatesmartschool.Fee.CommonFeesListActivity;
import com.ultimate.ultimatesmartschool.Fee.FeeDetail;
import com.ultimate.ultimatesmartschool.Fee.FeePaidList;
import com.ultimate.ultimatesmartschool.Fee.New_Fees.CommonPaidFeesListActivity;
import com.ultimate.ultimatesmartschool.Fee.New_Fees.CommonPendingFeesListActivity;
import com.ultimate.ultimatesmartschool.Fee.New_Fees.CommonSTDPendingFeesListActivity;
import com.ultimate.ultimatesmartschool.Fee.New_Fees.FeePaymentHistory;
import com.ultimate.ultimatesmartschool.Fee.New_Fees.PayFee_New;
import com.ultimate.ultimatesmartschool.Fee.QRScanner;
import com.ultimate.ultimatesmartschool.Fee.WebViewFeeStat;
import com.ultimate.ultimatesmartschool.Health_Module.Add_Health;
import com.ultimate.ultimatesmartschool.Health_Module.Add_Health_Activity;
import com.ultimate.ultimatesmartschool.Health_Module.View_Health;
import com.ultimate.ultimatesmartschool.Homework.GETHomeworkActivity;
import com.ultimate.ultimatesmartschool.Homework.Homework_Report;
import com.ultimate.ultimatesmartschool.Homework.Hw_Cw_Pending_Staff.Hw_Cw_Staff_Activity;
import com.ultimate.ultimatesmartschool.Homework.StaffWiseHWActivity;
import com.ultimate.ultimatesmartschool.Leave.StudentLeaveList;
import com.ultimate.ultimatesmartschool.Login.LoginActivity;
import com.ultimate.ultimatesmartschool.MSG_NEW.Fragments.Stu_adapterGds;
import com.ultimate.ultimatesmartschool.MSG_NEW.SendMSG;
import com.ultimate.ultimatesmartschool.NewOnlineClass.OnlineClassNew;
import com.ultimate.ultimatesmartschool.NewOnlineClass.ViewOnlineClassTeacher;
import com.ultimate.ultimatesmartschool.NotificationMod.NotificationAdapter;
import com.ultimate.ultimatesmartschool.NotificationMod.NotificationBean;
import com.ultimate.ultimatesmartschool.OnlineClass.AddOnlineClassLink;
import com.ultimate.ultimatesmartschool.OnlineClass.ViewOnlineClass;
import com.ultimate.ultimatesmartschool.Performance_Report.AddCommunication;
import com.ultimate.ultimatesmartschool.Performance_Report.Add_Cleanliness;
import com.ultimate.ultimatesmartschool.Performance_Report.CleanlinessList;
import com.ultimate.ultimatesmartschool.Performance_Report.CommunicationList;
import com.ultimate.ultimatesmartschool.QRScanner.QRScannerActivity;
import com.ultimate.ultimatesmartschool.Query.Querry;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.StuGatePass.AddGatePersonDetails;
import com.ultimate.ultimatesmartschool.StuGatePass.AddStuGatePass;
import com.ultimate.ultimatesmartschool.StuGatePass.ViewStudentGatePass;
import com.ultimate.ultimatesmartschool.Student.STD_Fragment.Male_FemaleAnalysis;
import com.ultimate.ultimatesmartschool.Student.STD_Fragment.STDListActivity;
import com.ultimate.ultimatesmartschool.Student.StudentDetailByScanner;
import com.ultimate.ultimatesmartschool.Student.StudentList;
import com.ultimate.ultimatesmartschool.Student.StudentListBySection;
import com.ultimate.ultimatesmartschool.StudentAttendance.AddAttendance;
import com.ultimate.ultimatesmartschool.StudentAttendance.AttendanceByScanner;
import com.ultimate.ultimatesmartschool.StudentAttendance.ClassAdapterAttend;
import com.ultimate.ultimatesmartschool.StudentAttendance.ClassReportGDS;
import com.ultimate.ultimatesmartschool.StudentAttendance.Monthwisereport;
import com.ultimate.ultimatesmartschool.StudentAttendance.StudentGateAttendance;
import com.ultimate.ultimatesmartschool.StudentAttendance.SubjectWiseAttendeance.Class_SubReportGDS;
import com.ultimate.ultimatesmartschool.StudentAttendance.SubjectWiseAttendeance.Sub_Monthwisereport;
import com.ultimate.ultimatesmartschool.StudentAttendance.SubjectWiseAttendeance.View_SubAttendance;
import com.ultimate.ultimatesmartschool.StudentAttendance.ViewAttendance;
import com.ultimate.ultimatesmartschool.StudentAttendance.YearWiseReport;
import com.ultimate.ultimatesmartschool.Sylabus.AddSyllabus;
import com.ultimate.ultimatesmartschool.Sylabus.ViewSyllabus;
import com.ultimate.ultimatesmartschool.TimeTable.AddTimeTable;
import com.ultimate.ultimatesmartschool.TimeTable.ViewTimeTable;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.CommonProgress;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;
import com.ultimate.ultimatesmartschool.Utility.Utils;
import com.ultimate.ultimatesmartschool.VideoPlayer.VideoUpload;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.content.Context.MODE_PRIVATE;


public class StudentFragment extends Fragment implements DashBoardAdapter.OpenDahsboardActivity, TextToSpeech.OnInitListener, NotificationAdapter.AdapterCallback {
    SharedPreferences sharedPreferences;

    @BindView(R.id.msg_count)
    TextView msg_count;
    @BindView(R.id.lytttt)
    RelativeLayout lytttt;
    @BindView(R.id.totalRecord)
    TextView totalRecord;
    @BindView(R.id.fees_count)
    TextView fees_count;
    @BindView(R.id.lytttt_school)
    RelativeLayout lytttt_school;
    @BindView(R.id.lytttt_fees)
    RelativeLayout lytttt_fees;
    int loaded = 0;

    // map view data
    private GoogleMap map;
    private SupportMapFragment mapFragment;
    LatLng latLng;
    String image_url = "", school = "", phone = "", address1 = "", website = "", email1 = "", latitude = "", longitude = "";
    double lat, lng;
    Handler handler = new Handler();
    Geocoder geo;

    int count = 0, speech = 0;
    TextToSpeech textToSpeech;
    String textholder = "", title = "";
    RecyclerView notiRV;
    ArrayList<NotificationBean> notiList = new ArrayList<>();
    private NotificationAdapter notiAdapter;

    @BindView(R.id.schName)
    TextView headtext;
    @BindView(R.id.imgLogo)
    CircularImageView navi_profiles;
    BottomSheetDialog mBottomSheetDialog;
    ImageView close, qr_code, dp;
    LinearLayout student_lyt;
    //    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    private DashBoardAdapter adapter;
    private ArrayList<DashboardBean> dataList;
    private Dialog timetbledialog;
    private RecyclerView timeselct;
    @BindView(R.id.searchView)
    SearchView searchView;
    private LinearLayoutManager layoutManager;
    @BindView(R.id.textNorecord)
    TextView txtNorecord;
    private ArrayList<SessionBean> sessionlist;
    private SessionBean session;
    ArrayList<ClassBean> classList = new ArrayList<>();
    private ArrayList<CommonBean> groupList = new ArrayList<>();
    private ArrayList<Studentbean> stuList;
    String jan_m = "", feb_m = "", mar_m = "", april_m = "", may_m = "", june_m = "", july_m = "", aug_m = "", sep_m = "", oct_m = "", nov_m = "", dec_m = "";
    String sturoll = "", className = "";
    String stuname = "";
    String classid = "", g_id = "", std_dp = "", stu_id = "", gender = "", regist = "", fee_cate_id = "", place_id = "", route_id = "", selectMonth = "";
    Spinner spinnerClass, spinnerstaff, spinnerFinancialYear, spinnerGroup;

    EditText edtName, edtFather, edtRegistration, edtMobile;
    CommonProgress commonProgress;
    ArrayList<String> monthList = new ArrayList<>();
    Spinner spinnerMonth;
    RelativeLayout month_lyt, stdlayout, classlayout3, classlayout;

    public StudentFragment() {
        // Required empty public constructor
    }

    // ArrayList<NotificationBean> notiList;
    String formattedDate;
    String abcd;
    private FloatingNavigationView mFloatingNavigationView;
    Animation animation1, animation2, animation3, animation4;
    TextView admin_name;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.stufrag_layout, container, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        commonProgress = new CommonProgress(getActivity());

        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);

        mFloatingNavigationView = (FloatingNavigationView) view.findViewById(R.id.floating_navigation_view);
        dataList = new ArrayList<>();
        dataList = addList();
        loaded = dataList.size();
        if (User.getCurrentUser().getSchoolData().getName() != null) {
            headtext.setText(User.getCurrentUser().getSchoolData().getName());
        }

        textToSpeech = new TextToSpeech(getActivity(), this);
        lytttt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                msg_count.startAnimation(animation4);
                Intent intent = new Intent(getActivity(), SendMSG.class);
                intent.putExtra("id", "inbox");
                startActivity(intent);
            }
        });

        lytttt_school.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                lytttt_school.startAnimation(animation4);
                Intent intent = new Intent(getActivity(), SchoolInfo.class);
                startActivity(intent);
            }
        });

        lytttt_fees.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fees_count.startAnimation(animation4);

                if (!fees_count.getText().toString().equalsIgnoreCase("0")) {
                    openDialoggg();
                } else {
                    Toast.makeText(getContext(), "No New Notification Found!", Toast.LENGTH_SHORT).show();
                }
                // startActivity(new Intent(getActivity(), FeePaidListActivity.class));
            }

        });

        startAnimation();
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 3));
        adapter = new DashBoardAdapter(getActivity(), dataList, this);
        recyclerView.setAdapter(adapter);
        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("dd MMM, yyyy");
        formattedDate = df.format(c);
        View headerView = mFloatingNavigationView.getHeaderView(0);
        //navi_profile = (CircularImageView) headerView.findViewById(R.id.navi_profile);
        admin_name = (TextView) headerView.findViewById(R.id.stu_name);
        admin_name.setText(User.getCurrentUser().getFirstname());


        totalRecord.setText("Total Module:- " + String.valueOf(loaded));
        checkLoaded(loaded);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                //FILTER AS YOU TYPE

                if (query.length() == 0) {
                    totalRecord.setText("Total Module:- " + String.valueOf(loaded));
                    checkLoaded(loaded);
                } else {
                    totalRecord.setText("Search Module:- " + String.valueOf(adapter.dataList.size()));
                    checkLoaded(adapter.dataList.size());
                }
                adapter.getFilter().filter(query);

                return false;
            }
        });


        if (User.getCurrentUser().getSchoolData().getLogo() != null) {
            Picasso.get().load(User.getCurrentUser().getSchoolData().getLogo()).placeholder(R.drawable.logo).into(navi_profiles);
        } else {
            Picasso.get().load(R.drawable.logo).into(navi_profiles);
        }

        mFloatingNavigationView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mFloatingNavigationView.open();
            }
        });
        mFloatingNavigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem item) {

                if (item.getTitle().equals("Change Password")) {
                    startActivity(new Intent(getContext(), ResetPassActivity.class));
                    // Toast.makeText(getActivity(),"Change Password",Toast.LENGTH_SHORT).show();
                    // reset_password();
                } else if (item.getTitle().equals("Feedback")) {
                    Toast.makeText(getActivity(), "Feedback", Toast.LENGTH_SHORT).show();
                    //  open_changePassword();
                } else if (item.getTitle().equals("Rate Us")) {
                    Uri uri = Uri.parse("market://details?id=" + getActivity().getPackageName());
                    Intent myAppLinkToMarket = new Intent(Intent.ACTION_VIEW, uri);
                    try {
                        startActivity(myAppLinkToMarket);
                    } catch (ActivityNotFoundException e) {
                        Toast.makeText(getContext(), " unable to find market app", Toast.LENGTH_LONG).show();
                    }
                    Toast.makeText(getActivity(), "Rate Us", Toast.LENGTH_SHORT).show();
                } else if (item.getTitle().equals("Share Us")) {
                    try {
                        Intent i = new Intent(Intent.ACTION_SEND);
                        i.setType("text/plain");
                        i.putExtra(Intent.EXTRA_SUBJECT, "XYZ Ultimate School");
                        String sAux = "\nLet me recommend you this application\n\n";
                        sAux = sAux + "https://play.google.com/store/apps/details?id=" + getActivity().getPackageName() + "\n\n";
                        i.putExtra(Intent.EXTRA_TEXT, sAux);
                        startActivity(Intent.createChooser(i, "choose one"));
                    } catch (Exception e) {
                        //e.toString();
                    }


                    //Toast.makeText(getActivity(),"Share Us",Toast.LENGTH_SHORT).show();
                } else if (item.getTitle().equals("Privacy Policy")) {
                    Toast.makeText(getActivity(), "Privacy Policy", Toast.LENGTH_SHORT).show();

                    BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(getActivity());
                    final View sheetView = getLayoutInflater().inflate(R.layout.privacy_policy_dialog, null);
                    mBottomSheetDialog.setContentView(sheetView);
                    mBottomSheetDialog.setCancelable(false);
                    final boolean[] loadingFinished = {true};
                    final boolean[] redirect = {false};
                    WebView webView = (WebView) sheetView.findViewById(R.id.privacy);

                    ErpProgress.showProgressBar(getActivity(), "Please Wait.......!");
                    if (!loadingFinished[0]) {
                        redirect[0] = true;
                    }
                    loadingFinished[0] = false;
                    webView.getSettings().setJavaScriptEnabled(true);
                    String url = "http://ultimatesolutiongroup.com/privacy_policy.html";
                    webView.loadUrl(url);
                    webView.setWebViewClient(new WebViewClient() {
                        @Override
                        public void onPageFinished(WebView view, String url) {
                            if (!redirect[0]) {
                                loadingFinished[0] = true;
                            }
                            if (loadingFinished[0] && !redirect[0]) {
                                ErpProgress.cancelProgressBar();
                            } else {
                                redirect[0] = false;
                            }
                        }
                    });

                    RelativeLayout btnNo = (RelativeLayout) sheetView.findViewById(R.id.btnNo);
                    btnNo.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            btnNo.startAnimation(animation4);
                            mBottomSheetDialog.dismiss();
                        }
                    });
                    mBottomSheetDialog.show();
                } else if (item.getTitle().equals("App Info")) {
                    Toast.makeText(getActivity(), "App Info", Toast.LENGTH_SHORT).show();
                    oenInfoDialog();
                } else {
                    // Snackbar.make((View) mFloatingNavigationView.getParent(), item.getTitle() + " Selected!", Snackbar.LENGTH_SHORT).show();
                    //    Toast.makeText(getActivity(),"Logout!",Toast.LENGTH_SHORT).show();

                    final Dialog logoutDialog = new Dialog(getActivity());
                    logoutDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    logoutDialog.setCancelable(true);
                    logoutDialog.setContentView(R.layout.logout_dialog);
                    logoutDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                    TextView txt2 = (TextView) logoutDialog.findViewById(R.id.txt2);

                    String logout_user = User.getCurrentUser().getFirstname();
                    txt2.setText("Are you sure" + " " + logout_user + "!" + " " + "want to logout?");

//                    CircularImageView img = (CircularImageView) logoutDialog.findViewById(R.id.img);
//
//                    if (User.getCurrentUser().getProfile() != null) {
//                        Picasso.with(getActivity()).load(User.getCurrentUser().getProfile()).placeholder(R.drawable.boy).into(navi_profile);
//                    } else {
//                        Picasso.with(getActivity()).load(R.drawable.boy).into(navi_profile);
//                    }

                    Button btnNo = (Button) logoutDialog.findViewById(R.id.btnNo);
                    btnNo.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            logoutDialog.dismiss();
                        }
                    });
                    Button btnYes = (Button) logoutDialog.findViewById(R.id.btnYes);
                    btnYes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            btnYes.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    logoutDialog.dismiss();
                                    ErpProgress.showProgressBar(getContext(), "Please wait...");
                                    User.getCurrentUser().logout();
                                    Toast.makeText(getContext(), "Logged out successfully", Toast.LENGTH_SHORT).show();
                                    Intent loginpage = new Intent(getContext(), LoginActivity.class);
                                    loginpage.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                                            Intent.FLAG_ACTIVITY_CLEAR_TASK |
                                            Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(loginpage);
                                    ErpProgress.cancelProgressBar();
                                    getActivity().finish();
                                }
                            });
                        }
                    });
                    logoutDialog.show();

                }

                //  Snackbar.make((View) mFloatingNavigationView.getParent(), item.getTitle() + " Selected!", Snackbar.LENGTH_SHORT).show();
                mFloatingNavigationView.close();


                Snackbar.make((View) mFloatingNavigationView.getParent(), item.getTitle() + " Selected!", Snackbar.LENGTH_SHORT).show();
                mFloatingNavigationView.close();
                return true;
            }
        });

    }

    private void checkLoaded(int loaded) {
        if (loaded == 0) {
            txtNorecord.setVisibility(View.VISIBLE);
        } else {
            txtNorecord.setVisibility(View.GONE);
        }
    }

    private void startAnimation() {
        animation1 = AnimationUtils.loadAnimation(getActivity(), R.anim.logo_animation);
        animation2 = AnimationUtils.loadAnimation(getActivity(), R.anim.enter_from_left_animation);
        animation3 = AnimationUtils.loadAnimation(getActivity(), R.anim.fade_animation);
        animation4 = AnimationUtils.loadAnimation(getActivity(), R.anim.btn_blink_animation);

    }

    private void openDialoggg() {
        BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(getActivity());
        final View sheetView = getLayoutInflater().inflate(R.layout.noti_msgimage_lyt, null);
        mBottomSheetDialog.setContentView(sheetView);
        mBottomSheetDialog.setCancelable(true);
        notiRV = (RecyclerView) sheetView.findViewById(R.id.recyclerView);

        TextView tittle = (TextView) sheetView.findViewById(R.id.tittle);
        String count = fees_count.getText().toString();

        if (!count.equalsIgnoreCase("0")) {
            String title = getColoredSpanned(count, "#e31e25");
            String Name = getColoredSpanned(" New Notification", "#000000");
            tittle.setText(Html.fromHtml(title + " " + Name));
        } else {
            tittle.setText("Today Notification");
        }

        notiList = new ArrayList<>();
        LinearLayoutManager layoutManager1 = new LinearLayoutManager(getActivity());
        notiRV.setLayoutManager(layoutManager1);
        notiAdapter = new NotificationAdapter(notiList, this, getActivity());
        notiRV.setAdapter(notiAdapter);
        fetchNotification();

        RelativeLayout btnNo = (RelativeLayout) sheetView.findViewById(R.id.btnNo);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                mBottomSheetDialog.dismiss();
            }
        });

        mBottomSheetDialog.show();
    }

    public void fetchNotification() {
        HashMap<String, String> params = new HashMap<>();
        params.put("id", User.getCurrentUser().getId());
        params.put("type", "10");
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.NOTIFICATIONURL, notiapiCallback, getActivity(), params);
    }

    ApiHandler.ApiCallback notiapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            if (error == null) {
                try {
                    JSONArray noticeArray = jsonObject.getJSONArray("notice_data");
                    notiList = NotificationBean.parseNotificationArray(noticeArray);
                    if (notiList.size() > 0) {
                        notiAdapter.setNotificationList(notiList);
                        //setanimation on adapter...
                        notiRV.getAdapter().notifyDataSetChanged();
                        notiRV.scheduleLayoutAnimation();
                        //-----------end------------
                    } else {
                        notiList.clear();
                        notiAdapter.setNotificationList(notiList);
                        notiAdapter.notifyDataSetChanged();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                notiList.clear();
                notiAdapter.setNotificationList(notiList);
                notiAdapter.notifyDataSetChanged();

            }
        }
    };


    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }


    @SuppressLint("RestrictedApi")
    private void oenInfoDialog() {
        mBottomSheetDialog = new BottomSheetDialog(getActivity());
        final View sheetView = getLayoutInflater().inflate(R.layout.app_info_dialog, null);
        mBottomSheetDialog.setContentView(sheetView);

        TextView txtVersion = (TextView) sheetView.findViewById(R.id.txtVersion);
        FloatingActionButton whatsapp = (FloatingActionButton) sheetView.findViewById(R.id.whatsapp);
        FloatingActionButton email = (FloatingActionButton) sheetView.findViewById(R.id.email);
        FloatingActionButton phone = (FloatingActionButton) sheetView.findViewById(R.id.phone);
        String number = "7009091606";
        String versionName = "";
        int code = 0;
        try {
            versionName = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0).versionName;
            code = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0).versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        txtVersion.setText("App Version Code: " + code + "\n" + "App Version Name: " + String.valueOf(versionName));

        email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //  mBottomSheetDialog.dismiss();
                ErpProgress.showProgressBar(getActivity(), "Please Wait.....!");
                Intent intent = new Intent(Intent.ACTION_SENDTO); // it's not ACTION_SEND
                intent.setType("text/plain");
                intent.putExtra(Intent.EXTRA_SUBJECT, "Subject of email");
                intent.putExtra(Intent.EXTRA_TEXT, "Body of email");
                //simplydirect email for setting email,,,
                intent.setData(Uri.parse("mailto:dealingkings112@gmail.com")); // or just "mailto:" for blank
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK); // this will make such that when user returns to your app, your app is displayed, instead of the email app.
                startActivity(intent);
                ErpProgress.cancelProgressBar();

            }
        });
        whatsapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //  mBottomSheetDialog.dismiss();
                Uri uri = Uri.parse("smsto:" + number);
                Intent i = new Intent(Intent.ACTION_SENDTO, uri);
                i.setPackage("com.whatsapp");
                startActivity(Intent.createChooser(i, "Chat Now!"));
            }
        });
        phone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // mBottomSheetDialog.dismiss();
                // for permission granted....
                if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CALL_PHONE}, 1);
                } else {
                    Intent intent = new Intent(Intent.ACTION_CALL);
                    intent.setData(Uri.parse("tel:" + number));
                    startActivity(intent);
                    ErpProgress.cancelProgressBar();
                }
            }
        });


        RelativeLayout btnNo = (RelativeLayout) sheetView.findViewById(R.id.btnNo);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnNo.startAnimation(animation4);
                mBottomSheetDialog.dismiss();
            }
        });
        mBottomSheetDialog.show();


    }


    public ArrayList<DashboardBean> addList() {
        ArrayList<DashboardBean> list = new ArrayList<>();


//        if(User.getCurrentUser().getSchoolData().getName().equalsIgnoreCase("Guru Nanak Dev Dental College and Research Institute")) {
//            DashboardBean addcolgstuObj = new DashboardBean();
//            addcolgstuObj.setTitle(Constants.colgadmission_form);
//            addcolgstuObj.setMIcon(getResources().getDrawable(R.drawable.add_form));
//            list.add(addcolgstuObj);
//        }else if(User.getCurrentUser().getSchoolData().getName().equalsIgnoreCase("GENESIS INSTITUTE OF DENTAL SCIENCES & RESEARCH")){
//
//            DashboardBean addcolgstuObj = new DashboardBean();
//            addcolgstuObj.setTitle(Constants.gensisadmission_form);
//            addcolgstuObj.setMIcon(getResources().getDrawable(R.drawable.add_form));
//            list.add(addcolgstuObj);
//
//        }
//
//        else{
//
//            DashboardBean addObj = new DashboardBean();
//            addObj.setTitle(Constants.admission_form);
//            addObj.setMIcon(getResources().getDrawable(R.drawable.add_form));
//            list.add(addObj);
//        }


        if (User.getCurrentUser().getType().equalsIgnoreCase("user")) {


            if (UserPermissions.getCurrentUserPermissions().getAdmission_form().equalsIgnoreCase("yes")) {

                DashboardBean addcolgstuObj = new DashboardBean();
                addcolgstuObj.setTitle(Constants.admission_form);
                addcolgstuObj.setMIcon(getResources().getDrawable(R.drawable.attendances));
                list.add(addcolgstuObj);

            }

            if (UserPermissions.getCurrentUserPermissions().getStudent_panel().equalsIgnoreCase("yes")) {
                DashboardBean studentobj = new DashboardBean();
                studentobj.setTitle(Constants.student);
                studentobj.setMIcon(getResources().getDrawable(R.drawable.students));
                list.add(studentobj);
            }


            if (UserPermissions.getCurrentUserPermissions().getStd_gate_attend().equalsIgnoreCase("yes")) {
                DashboardBean gdsattendanceObj1 = new DashboardBean();
                gdsattendanceObj1.setTitle(Constants.gateattendance);
                gdsattendanceObj1.setMIcon(getResources().getDrawable(R.drawable.stuicon));
                list.add(gdsattendanceObj1);
            }


            if (UserPermissions.getCurrentUserPermissions().getStud_attend().equalsIgnoreCase("yes")) {
                DashboardBean gdsattendanceObj = new DashboardBean();
                gdsattendanceObj.setTitle(Constants.attendance);
                gdsattendanceObj.setMIcon(getResources().getDrawable(R.drawable.attendance));
                list.add(gdsattendanceObj);

            }


            if (UserPermissions.getCurrentUserPermissions().getTimetable().equalsIgnoreCase("yes")) {
                DashboardBean timetableObj = new DashboardBean();
                timetableObj.setTitle(Constants.timetable);
                timetableObj.setMIcon(getResources().getDrawable(R.drawable.timetable));
                list.add(timetableObj);

            }


            if (UserPermissions.getCurrentUserPermissions().getHomework().equalsIgnoreCase("yes")) {

                DashboardBean homeworkObj = new DashboardBean();
                homeworkObj.setTitle(Constants.homework);
                homeworkObj.setMIcon(getResources().getDrawable(R.drawable.homework));
                list.add(homeworkObj);
            }


            if (UserPermissions.getCurrentUserPermissions().getClasswork().equalsIgnoreCase("yes")) {

                DashboardBean classworkobj = new DashboardBean();
                classworkobj.setTitle(Constants.classwork);
                classworkobj.setMIcon(getResources().getDrawable(R.drawable.classwork));
                list.add(classworkobj);

            }

            if (UserPermissions.getCurrentUserPermissions().getAssignment().equalsIgnoreCase("yes")) {
                DashboardBean assignobj = new DashboardBean();
                assignobj.setTitle(Constants.assign);
                assignobj.setMIcon(getResources().getDrawable(R.drawable.assignment));
                list.add(assignobj);

            }


            if (UserPermissions.getCurrentUserPermissions().getExamination().equalsIgnoreCase("yes")) {
                DashboardBean examObj = new DashboardBean();
                examObj.setTitle(Constants.exam);
                examObj.setMIcon(getResources().getDrawable(R.drawable.exam));
                list.add(examObj);

            }




            if (UserPermissions.getCurrentUserPermissions().getSea().equalsIgnoreCase("yes")) {
                DashboardBean testObj1 = new DashboardBean();
                testObj1.setTitle(Constants.sea);
                testObj1.setMIcon(getResources().getDrawable(R.drawable.exam_sea));
                list.add(testObj1);

            }


            if (UserPermissions.getCurrentUserPermissions().getClasstest().equalsIgnoreCase("yes")) {
                DashboardBean testObj = new DashboardBean();
                testObj.setTitle(Constants.test);
                testObj.setMIcon(getResources().getDrawable(R.drawable.test));
                list.add(testObj);

            }


            if (UserPermissions.getCurrentUserPermissions().getDatesheet().equalsIgnoreCase("yes")) {
                DashboardBean dsObj = new DashboardBean();
                dsObj.setTitle(Constants.ds);
                dsObj.setMIcon(getResources().getDrawable(R.drawable.datesheet));
                list.add(dsObj);

            }


            if (UserPermissions.getCurrentUserPermissions().getStd_leave().equalsIgnoreCase("yes")) {
                DashboardBean leaveObj = new DashboardBean();
                leaveObj.setTitle(Constants.leave);
                leaveObj.setMIcon(getResources().getDrawable(R.drawable.leave));
                leaveObj.setTxtColor("1");
                list.add(leaveObj);

            }


            if (UserPermissions.getCurrentUserPermissions().getSyllabus().equalsIgnoreCase("yes")) {
                DashboardBean syllabusObj = new DashboardBean();
                syllabusObj.setTitle(Constants.syllabus);
                syllabusObj.setMIcon(getResources().getDrawable(R.drawable.syllabus));
                list.add(syllabusObj);


            }

            if (UserPermissions.getCurrentUserPermissions().getEs_planner().equalsIgnoreCase("yes")) {

                DashboardBean syllabusObj1 = new DashboardBean();
                syllabusObj1.setTitle(Constants.month_plan);
                syllabusObj1.setMIcon(getResources().getDrawable(R.drawable.planner));
                list.add(syllabusObj1);


            }


            if (UserPermissions.getCurrentUserPermissions().getFeepay().equalsIgnoreCase("yes")) {

                DashboardBean colgfeeObj = new DashboardBean();
                colgfeeObj.setTitle(Constants.CollegeFee_manage);
                colgfeeObj.setMIcon(getResources().getDrawable(R.drawable.fees));
                list.add(colgfeeObj);
            }

            if (UserPermissions.getCurrentUserPermissions().getEs_health().equalsIgnoreCase("yes")) {
                DashboardBean QuerryObj = new DashboardBean();
                QuerryObj.setTitle(Constants.health);
                QuerryObj.setMIcon(getResources().getDrawable(R.drawable.health));
                list.add(QuerryObj);
            }

            if (UserPermissions.getCurrentUserPermissions().getEs_performance().equalsIgnoreCase("yes")) {
                DashboardBean QuerryObj1 = new DashboardBean();
                QuerryObj1.setTitle(Constants.performance);
                QuerryObj1.setMIcon(getResources().getDrawable(R.drawable.performance));
                list.add(QuerryObj1);
            }


            if (UserPermissions.getCurrentUserPermissions().getStd_gatepass().equalsIgnoreCase("yes")) {

                DashboardBean colggateObj = new DashboardBean();
                colggateObj.setTitle(Constants.collegegatepass);
                colggateObj.setMIcon(getResources().getDrawable(R.drawable.gatepass));
                list.add(colggateObj);
            }


            if (UserPermissions.getCurrentUserPermissions().getEbook().equalsIgnoreCase("yes")) {

                DashboardBean ebookObj = new DashboardBean();
                ebookObj.setTitle(Constants.ebook);
                ebookObj.setMIcon(getResources().getDrawable(R.drawable.ebook));
                list.add(ebookObj);
            }


            if (UserPermissions.getCurrentUserPermissions().getElearning().equalsIgnoreCase("yes")) {
                DashboardBean onlineclsobj = new DashboardBean();
                onlineclsobj.setTitle(Constants.onlineclass);
                onlineclsobj.setMIcon(getResources().getDrawable(R.drawable.elearning));
                list.add(onlineclsobj);
            }

        } else if (User.getCurrentUser().getType().equalsIgnoreCase("admin") || User.getCurrentUser().getType().equalsIgnoreCase("super")) {

            DashboardBean addcolgstuObj = new DashboardBean();
            addcolgstuObj.setTitle(Constants.admission_form);
            addcolgstuObj.setMIcon(getResources().getDrawable(R.drawable.attendances));
            list.add(addcolgstuObj);


            DashboardBean gdsattendanceObj1 = new DashboardBean();
            gdsattendanceObj1.setTitle(Constants.gateattendance);
            gdsattendanceObj1.setMIcon(getResources().getDrawable(R.drawable.stuicon));
            list.add(gdsattendanceObj1);

            DashboardBean studentobj = new DashboardBean();
            studentobj.setTitle(Constants.student);
            studentobj.setMIcon(getResources().getDrawable(R.drawable.students));
            list.add(studentobj);


            DashboardBean gdsattendanceObj = new DashboardBean();
            gdsattendanceObj.setTitle(Constants.attendance);
            gdsattendanceObj.setMIcon(getResources().getDrawable(R.drawable.attendance));
            list.add(gdsattendanceObj);


            DashboardBean timetableObj = new DashboardBean();
            timetableObj.setTitle(Constants.timetable);
            timetableObj.setMIcon(getResources().getDrawable(R.drawable.timetable));
            list.add(timetableObj);

//        DashboardBean sattendanceObj = new DashboardBean();
//        sattendanceObj.setTitle(Constants.attendance);
//        sattendanceObj.setMIcon(getResources().getDrawable(R.drawable.attendance));
//        list.add(sattendanceObj);

//        Log.e("whstisgetid",User.getCurrentUser().getSchoolData().getId());
//        Log.e("whatisgetid",User.getCurrentUser().getSchoolData().getName());
//       Log.e("whatisid",User.getCurrentUser().getSchoolData().getSchool_id());


//        if(User.getCurrentUser().getSchoolData().getName().equalsIgnoreCase("Guru Nanak Dev Dental College and Research Institute")){
//            DashboardBean colgattendanceObj = new DashboardBean();
//            colgattendanceObj.setTitle(Constants.collegeattendance);
//            colgattendanceObj.setMIcon(getResources().getDrawable(R.drawable.attendance));
//            list.add(colgattendanceObj);
//        }else{
//
//            DashboardBean sattendanceObj = new DashboardBean();
//            sattendanceObj.setTitle(Constants.attendance);
//            sattendanceObj.setMIcon(getResources().getDrawable(R.drawable.attendance));
//            list.add(sattendanceObj);
//        }


            DashboardBean homeworkObj = new DashboardBean();
            homeworkObj.setTitle(Constants.homework);
            homeworkObj.setMIcon(getResources().getDrawable(R.drawable.homework));
            list.add(homeworkObj);


            DashboardBean classworkobj = new DashboardBean();
            classworkobj.setTitle(Constants.classwork);
            classworkobj.setMIcon(getResources().getDrawable(R.drawable.classwork));
            list.add(classworkobj);

            DashboardBean assignobj = new DashboardBean();
            assignobj.setTitle(Constants.assign);
            assignobj.setMIcon(getResources().getDrawable(R.drawable.assignment));
            list.add(assignobj);

            DashboardBean examObj = new DashboardBean();
            examObj.setTitle(Constants.exam);
            examObj.setMIcon(getResources().getDrawable(R.drawable.exam));
            list.add(examObj);

            DashboardBean testObj = new DashboardBean();
            testObj.setTitle(Constants.test);
            testObj.setMIcon(getResources().getDrawable(R.drawable.test));
            list.add(testObj);

            DashboardBean testObj1 = new DashboardBean();
            testObj1.setTitle(Constants.sea);
            testObj1.setMIcon(getResources().getDrawable(R.drawable.exam_sea));
            list.add(testObj1);

            DashboardBean dsObj = new DashboardBean();
            dsObj.setTitle(Constants.ds);
            dsObj.setMIcon(getResources().getDrawable(R.drawable.datesheet));
            list.add(dsObj);

            DashboardBean leaveObj = new DashboardBean();
            leaveObj.setTitle(Constants.leave);
            leaveObj.setMIcon(getResources().getDrawable(R.drawable.leave));
            leaveObj.setTxtColor("1");
            list.add(leaveObj);

            DashboardBean syllabusObj = new DashboardBean();
            syllabusObj.setTitle(Constants.syllabus);
            syllabusObj.setMIcon(getResources().getDrawable(R.drawable.syllabus));
            list.add(syllabusObj);

            DashboardBean syllabusObj1 = new DashboardBean();
            syllabusObj1.setTitle(Constants.month_plan);
            syllabusObj1.setMIcon(getResources().getDrawable(R.drawable.planner));
            list.add(syllabusObj1);


//        if(User.getCurrentUser().getSchoolData().getName().equalsIgnoreCase("Guru Nanak Dev Dental College and Research Institute")) {
//            DashboardBean colgfeeObj = new DashboardBean();
//            colgfeeObj.setTitle(Constants.CollegeFee_manage);
//            colgfeeObj.setMIcon(getResources().getDrawable(R.drawable.fees));
//            list.add(colgfeeObj);
//        }else if(User.getCurrentUser().getSchoolData().getName().equalsIgnoreCase("City Heart School")){
//            DashboardBean feeObj = new DashboardBean();
//            feeObj.setTitle(Constants.ChssclFee_manage);
//            feeObj.setMIcon(getResources().getDrawable(R.drawable.fee));
//            list.add(feeObj);
//        }
//        else{
//
//            DashboardBean feeObj = new DashboardBean();
//            feeObj.setTitle(Constants.Fee_manage);
//            feeObj.setMIcon(getResources().getDrawable(R.drawable.fees));
//            list.add(feeObj);
//        }

//        DashboardBean chsfeeObj = new DashboardBean();
//        chsfeeObj.setTitle(Constants.ChssclFee_manage);
//        chsfeeObj.setMIcon(getResources().getDrawable(R.drawable.fees));
//        list.add(chsfeeObj);

            DashboardBean colgfeeObj = new DashboardBean();
            colgfeeObj.setTitle(Constants.CollegeFee_manage);
            colgfeeObj.setMIcon(getResources().getDrawable(R.drawable.fees));
            list.add(colgfeeObj);


            DashboardBean QuerryObj = new DashboardBean();
            QuerryObj.setTitle(Constants.health);
            QuerryObj.setMIcon(getResources().getDrawable(R.drawable.health));
            list.add(QuerryObj);

            DashboardBean QuerryObj1 = new DashboardBean();
            QuerryObj1.setTitle(Constants.performance);
            QuerryObj1.setMIcon(getResources().getDrawable(R.drawable.performance));
            list.add(QuerryObj1);

//        DashboardBean QuerryObj = new DashboardBean();
//        QuerryObj.setTitle(Constants.querry);
//        QuerryObj.setMIcon(getResources().getDrawable(R.drawable.query));
//        list.add(QuerryObj);


//        if(User.getCurrentUser().getSchoolData().getName().equalsIgnoreCase("Guru Nanak Dev Dental College and Research Institute")) {
//            DashboardBean colggateObj = new DashboardBean();
//            colggateObj.setTitle(Constants.collegegatepass);
//            colggateObj.setMIcon(getResources().getDrawable(R.drawable.gatepass));
//            list.add(colggateObj);
//        }else{
//            DashboardBean gateObj = new DashboardBean();
//            gateObj.setTitle(Constants.gatepass);
//            gateObj.setMIcon(getResources().getDrawable(R.drawable.gatepass));
//            list.add(gateObj);
//        }

            DashboardBean colggateObj = new DashboardBean();
            colggateObj.setTitle(Constants.collegegatepass);
            colggateObj.setMIcon(getResources().getDrawable(R.drawable.gatepass));
            list.add(colggateObj);


//        DashboardBean hostlObj = new DashboardBean();
//        hostlObj.setTitle(Constants.HOSTEL);
//        hostlObj.setMIcon(getResources().getDrawable(R.drawable.hostel));
//        list.add(hostlObj);


            DashboardBean ebookObj = new DashboardBean();
            ebookObj.setTitle(Constants.ebook);
            ebookObj.setMIcon(getResources().getDrawable(R.drawable.ebook));
            list.add(ebookObj);

//            DashboardBean dietchrtObj = new DashboardBean();
//            dietchrtObj.setTitle(Constants.dietchart);
//            dietchrtObj.setMIcon(getResources().getDrawable(R.drawable.dietchart));
//            list.add(dietchrtObj);


//        DashboardBean elibraryObj = new DashboardBean();
//        elibraryObj.setTitle(Constants.elibrary);
//        elibraryObj.setMIcon(getResources().getDrawable(R.drawable.library));
//        list.add(elibraryObj);


//        DashboardBean sturepoObj = new DashboardBean();
//        sturepoObj.setTitle(Constants.stureport);
//        sturepoObj.setMIcon(getResources().getDrawable(R.drawable.laptop));
//        list.add(sturepoObj);
//
//        DashboardBean front_desk = new DashboardBean();
//        front_desk.setTitle(Constants.frontoffice);
//        front_desk.setMIcon(getResources().getDrawable(R.drawable.receptionist));
//        list.add(front_desk);
//
//        DashboardBean sportobj = new DashboardBean();
//        sportobj.setTitle(Constants.sports);
//        sportobj.setMIcon(getResources().getDrawable(R.drawable.football));
//        list.add(sportobj);
//
//        DashboardBean childobj = new DashboardBean();
//        childobj.setTitle(Constants.childcare);
//        childobj.setMIcon(getResources().getDrawable(R.drawable.childcare));
//        list.add(childobj);
//
//        DashboardBean smrcmpobj = new DashboardBean();
//        smrcmpobj.setTitle(Constants.summercamp);
//        smrcmpobj.setMIcon(getResources().getDrawable(R.drawable.caravan));
//        list.add(smrcmpobj);
//
//        DashboardBean trainingobj = new DashboardBean();
//        trainingobj.setTitle(Constants.trainingmgt);
//        trainingobj.setMIcon(getResources().getDrawable(R.drawable.presentation));
//        list.add(trainingobj);

//
//
            DashboardBean onlineclsobj = new DashboardBean();
            onlineclsobj.setTitle(Constants.onlineclass);
            onlineclsobj.setMIcon(getResources().getDrawable(R.drawable.elearning));
            list.add(onlineclsobj);


            DashboardBean trainingobj = new DashboardBean();
            trainingobj.setTitle(Constants.OnlineQuiz);
            trainingobj.setMIcon(getResources().getDrawable(R.drawable.onlinetest));
            list.add(trainingobj);


//            DashboardBean qrscanobj = new DashboardBean();
//            qrscanobj.setTitle(Constants.QRSCANNNER);
//            qrscanobj.setMIcon(getResources().getDrawable(R.drawable.qr_scan));
//            list.add(qrscanobj);

//            DashboardBean onlineclassobj = new DashboardBean();
//            onlineclassobj.setTitle(Constants.OnlineClass);
//            onlineclassobj.setMIcon(getResources().getDrawable(R.drawable.videoconference));
//            list.add(onlineclassobj);


        } else {

        }


//        DashboardBean dietchrtObj = new DashboardBean();
//        dietchrtObj.setTitle(Constants.dietchart);
//        dietchrtObj.setMIcon(getResources().getDrawable(R.drawable.dietchart));
//        list.add(dietchrtObj);


        return list;
    }

    @Override
    public void openDahsboardActivity(DashboardBean dashboard) {

        if (dashboard.getTitle().equals(Constants.OnlineClass)) {

            openonlineclassdialog();


        }

        if (dashboard.getTitle().equals(Constants.gateattendance)) {

            startActivity(new Intent(getContext(), StudentGateAttendance.class));

        }
//
//        if (dashboard.getTitle().equals(Constants.QRSCANNNER)) {
//            startActivity(new Intent(getActivity(), QRScannerActivity.class));
//
//        }

        if (dashboard.getTitle().equals(Constants.CollegeFee_manage)) {
            opengfeedialogsec();

        }
        if (dashboard.getTitle().equals(Constants.OnlineQuiz)) {
            opengOnlineQuizdialogsec();

        }

        if (dashboard.getTitle().equals(Constants.admission_form)) {

            //openAdmissionformdialog();
            startActivity(new Intent(getActivity(), StudentAdmissionForm.class));
        }
        if (dashboard.getTitle().equals(Constants.querry)) {
            startActivity(new Intent(getActivity(), Querry.class));
        }
        if (dashboard.getTitle().equals(Constants.dietchart)) {
            startActivity(new Intent(getActivity(), DietChartActivity.class));
        }
        if (dashboard.getTitle().equals(Constants.assign)) {
            // openassigndialogsec();
            startActivity(new Intent(getContext(), ViewAssignment.class));
        }
        if (dashboard.getTitle().equals(Constants.classwork)) {
            opencwdialogsec();
        }
        if (dashboard.getTitle().equals(Constants.student)) {
            openstudentdialogsec();
        }

        if (dashboard.getTitle().equals(Constants.homework)) {
            openhwgdsdialogsec();
        }
        if (dashboard.getTitle().equals(Constants.onlineclass)) {
            openvideoclasdialogsec();
        }

        if (dashboard.getTitle().equals(Constants.timetable)) {
            opentimetabledsdialogsec();
        }

        if (dashboard.getTitle().equals(Constants.ebook)) {
            openebookdsdialogsec();
        }

        if (dashboard.getTitle().equals(Constants.syllabus)) {
            openesyllabusdsdialogsec(dashboard.getTitle());
        }

        if (dashboard.getTitle().equals(Constants.month_plan)) {
            openesyllabusdsdialogsec(dashboard.getTitle());
        }

        if (dashboard.getTitle().equals(Constants.attendance)) {
            // openeattenddialogsec();
            if (User.getCurrentUser().getSchoolData().getFi_school_id().equalsIgnoreCase("XYZGGSMT")) {
                openeattenddialogsec("college");
            } else if (User.getCurrentUser().getSchoolData().getFi_school_id().equalsIgnoreCase("XYZ")) {
                openeattenddialogsec("school");
            } else {
                openeattenddialogsec("school");
            }

        }

        if (dashboard.getTitle().equals(Constants.leave)) {
            startActivity(new Intent(getContext(), StudentLeaveList.class));
        }


        if (dashboard.getTitle().equals(Constants.ds)) {
            opendatesheetdialogsec();
        }


        if (dashboard.getTitle().equals(Constants.collegegatepass)) {
            opengatepassdialogsec();
            // startActivity(new Intent(getContext(), ViewStudentGatePass.class));
        }

        if (dashboard.getTitle().equals(Constants.test)) {
            opengclasstestdialogsec(Constants.test);
        }
        if (dashboard.getTitle().equals(Constants.sea)) {
            opengclasstestdialogsec(Constants.sea);
        }


        if (dashboard.getTitle().equals(Constants.exam)) {
            openexamdialogsec();
        }


        if (dashboard.getTitle().equals(Constants.performance)) {
            openAdmissionformdialog(dashboard.getTitle());
        }

        if (dashboard.getTitle().equals(Constants.health)) {
            openAdmissionformdialog(dashboard.getTitle());

        }

        if (dashboard.getTitle().equals(Constants.post)) {
            openAdmissionformdialog(dashboard.getTitle());

        }

    }

    private void openAdmissionformdialog(String title) {

        final BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(getContext());
        final View sheetView = getLayoutInflater().inflate(R.layout.admission_dialog, null);
        mBottomSheetDialog.setContentView(sheetView);

        TextView tittle = sheetView.findViewById(R.id.txtTitle);
        TextView add = sheetView.findViewById(R.id.add);
        TextView view = sheetView.findViewById(R.id.view);
        ImageView img = sheetView.findViewById(R.id.img);

        TextView add1 = sheetView.findViewById(R.id.add1);
        TextView view1 = sheetView.findViewById(R.id.view1);

        LinearLayout firstlay1 = sheetView.findViewById(R.id.firstlay1);

        if (title.equalsIgnoreCase(Constants.health)) {
            tittle.setText(title);
            add.setText("Health & Activity");
            view.setText("View List");
            add1.setText("Health & Medicine");
            view1.setText("View List");
            img.setImageDrawable(getResources().getDrawable(R.drawable.health));
            firstlay1.setVisibility(View.VISIBLE);
        } else {
            tittle.setText(title);
            add.setText("Add Cleanliness");
            view.setText("Cleanliness List");
            img.setImageDrawable(getResources().getDrawable(R.drawable.performance));
            firstlay1.setVisibility(View.VISIBLE);
        }

        sheetView.findViewById(R.id.add).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (title.equalsIgnoreCase("Health")) {
                    startActivity(new Intent(getContext(), Add_Health_Activity.class));
                } else {
                    startActivity(new Intent(getContext(), Add_Cleanliness.class));
                }

            }
        });
        sheetView.findViewById(R.id.view).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (title.equalsIgnoreCase("Health")) {
                    Intent intent = new Intent(getActivity(), View_Health.class);
                    intent.putExtra("view", "activity");
                    startActivity(intent);
                } else {
                    startActivity(new Intent(getContext(), CleanlinessList.class));
                }

            }
        });

        sheetView.findViewById(R.id.add1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (title.equalsIgnoreCase("Health")) {
                    startActivity(new Intent(getContext(), Add_Health.class));
                } else {
                    startActivity(new Intent(getContext(), AddCommunication.class));
                }

            }
        });
        sheetView.findViewById(R.id.view1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (title.equalsIgnoreCase("Health")) {
                    Intent intent = new Intent(getActivity(), View_Health.class);
                    intent.putExtra("view", "medicine");
                    startActivity(intent);
                } else {
                    startActivity(new Intent(getContext(), CommunicationList.class));
                }

            }
        });


        RelativeLayout btnNo = (RelativeLayout) sheetView.findViewById(R.id.btnNo);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnNo.startAnimation(animation4);
                mBottomSheetDialog.dismiss();
            }
        });

        mBottomSheetDialog.show();

    }

    @SuppressLint("MissingInflatedId")
    private void opengfeedialogsec() {

        final BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(getContext());
        final View sheetView = getLayoutInflater().inflate(R.layout.fee_dialog, null);
        mBottomSheetDialog.setContentView(sheetView);


        sheetView.findViewById(R.id.addBank).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), AddBankDetails.class));

                //startActivity(new Intent(getContext(), PayFee_New.class));
            }
        });

        sheetView.findViewById(R.id.add).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), FeeDetail.class));
            }
        });


        sheetView.findViewById(R.id.view).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), FeePaidList.class));

            }
        });

        sheetView.findViewById(R.id.view2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //  startActivity(new Intent(getContext(), ClassWiseFeeStatus.class));
                startActivity(new Intent(getContext(), WebViewFeeStat.class));
            }
        });


        Button stu_wise = (Button) sheetView.findViewById(R.id.stu_wise);
        Button class_wise = (Button) sheetView.findViewById(R.id.class_wise);
        Button pay = (Button) sheetView.findViewById(R.id.Pay);


        if (User.getCurrentUser().getSchoolData().getFi_school_id().equalsIgnoreCase("XYZGGSMT")) {
            pay.setVisibility(View.GONE);
        } else if (User.getCurrentUser().getSchoolData().getFi_school_id().equalsIgnoreCase("XYZJDS")) {
            pay.setVisibility(View.GONE);
            stu_wise.setVisibility(View.GONE);
            class_wise.setVisibility(View.GONE);
        } else {
            pay.setVisibility(View.VISIBLE);
            stu_wise.setVisibility(View.VISIBLE);
            class_wise.setVisibility(View.VISIBLE);
        }

        sheetView.findViewById(R.id.payment_history).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    openDialog("payment_history");
                }
            }
        });

        sheetView.findViewById(R.id.pending_stu_wise).setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onClick(View view) {
                //startActivity(new Intent(getContext(), AddFee.class));

                final BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(getContext());
                final View sheetView = getLayoutInflater().inflate(R.layout.attend_dialog2, null);
                mBottomSheetDialog.setContentView(sheetView);
                Button fees_list = (Button) sheetView.findViewById(R.id.add);
                Button fees_receipt = (Button) sheetView.findViewById(R.id.view);
                TextView txtTitle = (TextView) sheetView.findViewById(R.id.txtTitle);
                fees_receipt.setText("Student Wise");
                fees_list.setText("Class Wise");
                txtTitle.setText("Fees Pending List");

                Button all = (Button) sheetView.findViewById(R.id.all);
                all.setVisibility(View.VISIBLE);

                sheetView.findViewById(R.id.all).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        openDialog("pending_fees_list_all");
                    }
                });


                sheetView.findViewById(R.id.view).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        openDialog("pending_stu_wise");
                    }
                });
                sheetView.findViewById(R.id.add).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        openDialog("pending_class_wise");
                    }
                });
                RelativeLayout btnNo = (RelativeLayout) sheetView.findViewById(R.id.btnNo);
                btnNo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        btnNo.startAnimation(animation4);
                        mBottomSheetDialog.dismiss();
                    }
                });

                mBottomSheetDialog.show();


            }
        });

        sheetView.findViewById(R.id.stu_wise).setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onClick(View view) {
                //startActivity(new Intent(getContext(), AddFee.class));

                final BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(getContext());
                final View sheetView = getLayoutInflater().inflate(R.layout.attend_dialog2, null);
                mBottomSheetDialog.setContentView(sheetView);
                Button fees_list = (Button) sheetView.findViewById(R.id.add);
                Button fees_receipt = (Button) sheetView.findViewById(R.id.view);
                TextView txtTitle = (TextView) sheetView.findViewById(R.id.txtTitle);
                fees_receipt.setText("Student Wise");
                fees_list.setText("Class Wise");
                txtTitle.setText("Fees Paid List");

                Button all = (Button) sheetView.findViewById(R.id.all);
                all.setVisibility(View.VISIBLE);

                sheetView.findViewById(R.id.all).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        openDialog("fees_list_all");
                    }
                });


                sheetView.findViewById(R.id.view).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        openDialog("stu_wise");
                    }
                });
                sheetView.findViewById(R.id.add).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        openDialog("class_wise");
                    }
                });
                RelativeLayout btnNo = (RelativeLayout) sheetView.findViewById(R.id.btnNo);
                btnNo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        btnNo.startAnimation(animation4);
                        mBottomSheetDialog.dismiss();
                    }
                });

                mBottomSheetDialog.show();


            }
        });


        sheetView.findViewById(R.id.class_wise).setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onClick(View view) {
                //startActivity(new Intent(getContext(), AddFee.class));

                final BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(getContext());
                final View sheetView = getLayoutInflater().inflate(R.layout.attend_dialog2, null);
                mBottomSheetDialog.setContentView(sheetView);

                Button fees_list = (Button) sheetView.findViewById(R.id.add);
                Button fees_receipt = (Button) sheetView.findViewById(R.id.view);
                TextView txtTitle = (TextView) sheetView.findViewById(R.id.txtTitle);

                Button all = (Button) sheetView.findViewById(R.id.all);
                all.setVisibility(View.VISIBLE);

                fees_receipt.setText("Student Wise");
                fees_list.setText("Class Wise");

                txtTitle.setText("Online Fees Paid List");

                sheetView.findViewById(R.id.all).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        openDialog("fees_receipt_all");
                    }
                });

                sheetView.findViewById(R.id.view).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        openDialog("stu_wise_rec");
                    }
                });

                sheetView.findViewById(R.id.add).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        openDialog("class_wise_rec");
                    }
                });
                RelativeLayout btnNo = (RelativeLayout) sheetView.findViewById(R.id.btnNo);
                btnNo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        btnNo.startAnimation(animation4);
                        mBottomSheetDialog.dismiss();
                    }
                });

                mBottomSheetDialog.show();

            }
        });


        sheetView.findViewById(R.id.Pay).setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onClick(View view) {
                //startActivity(new Intent(getContext(), AddFee.class));
                openDialog("Pay");
            }
        });

        RelativeLayout btnNo = (RelativeLayout) sheetView.findViewById(R.id.btnNo);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnNo.startAnimation(animation4);
                mBottomSheetDialog.dismiss();
            }
        });

        mBottomSheetDialog.show();


    }


    @RequiresApi(api = Build.VERSION_CODES.N)
    private void openDialog(String tag) {

        BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(getActivity());
        final View sheetView = getLayoutInflater().inflate(R.layout.pay_fee_dialog, null);
        mBottomSheetDialog.setContentView(sheetView);
        mBottomSheetDialog.setCancelable(false);

        spinnerClass = (Spinner) sheetView.findViewById(R.id.spinnerClass);
        spinnerstaff = (Spinner) sheetView.findViewById(R.id.spinnerstaff);
        spinnerMonth = (Spinner) sheetView.findViewById(R.id.spinnerMonth);
        spinnerGroup = (Spinner) sheetView.findViewById(R.id.spinnerGroup);
        spinnerFinancialYear = (Spinner) sheetView.findViewById(R.id.spinnerFinancialYear);

        edtName = (EditText) sheetView.findViewById(R.id.edtName);
        edtFather = (EditText) sheetView.findViewById(R.id.edtFather);
        edtRegistration = (EditText) sheetView.findViewById(R.id.edtRegistration);
        edtMobile = (EditText) sheetView.findViewById(R.id.edtMobile);
        qr_code = (ImageView) sheetView.findViewById(R.id.qr);
        close = (ImageView) sheetView.findViewById(R.id.close);
        dp = (ImageView) sheetView.findViewById(R.id.dp);
        TextView txtSetup = (TextView) sheetView.findViewById(R.id.txtSetup);

        student_lyt = (LinearLayout) sheetView.findViewById(R.id.student_lyt);

        classlayout3 = (RelativeLayout) sheetView.findViewById(R.id.classlayout3);
        classlayout = (RelativeLayout) sheetView.findViewById(R.id.classlayout);
        month_lyt = (RelativeLayout) sheetView.findViewById(R.id.month_lyt);
        stdlayout = (RelativeLayout) sheetView.findViewById(R.id.stdlayout);

        if (tag.equalsIgnoreCase("stu_wise")) {
            txtSetup.setText("Filter Fees Paid List \nStudent Wise");
            fetchSession();
            fetchgrouplist();
            month_lyt.setVisibility(View.GONE);
            stdlayout.setVisibility(View.VISIBLE);
            qr_code.setVisibility(View.GONE);
        } else if (tag.equalsIgnoreCase("class_wise")) {
            fetchSession();
            fetchgrouplist();
            month_lyt.setVisibility(View.GONE);
            stdlayout.setVisibility(View.GONE);
            qr_code.setVisibility(View.GONE);
            txtSetup.setText("Filter Fees Paid List \nClass Wise");
        } else if (tag.equalsIgnoreCase("fees_list_all")) {
            fetchSession();
            month_lyt.setVisibility(View.GONE);
            stdlayout.setVisibility(View.GONE);
            qr_code.setVisibility(View.GONE);
            classlayout.setVisibility(View.GONE);
            classlayout3.setVisibility(View.GONE);
            txtSetup.setText("Fees Paid List");
        } else if (tag.equalsIgnoreCase("pending_stu_wise")) {
            txtSetup.setText("Filter Fees Pending List \nStudent Wise");
            fetchSession();
            fetchgrouplist();
            month_lyt.setVisibility(View.GONE);
            stdlayout.setVisibility(View.VISIBLE);
            qr_code.setVisibility(View.GONE);
        } else if (tag.equalsIgnoreCase("pending_class_wise")) {
            fetchSession();
            fetchgrouplist();
            month_lyt.setVisibility(View.GONE);
            stdlayout.setVisibility(View.GONE);
            qr_code.setVisibility(View.GONE);
            txtSetup.setText("Filter Fees Pending List \nClass Wise");
        } else if (tag.equalsIgnoreCase("pending_fees_list_all")) {
            fetchSession();
            month_lyt.setVisibility(View.GONE);
            stdlayout.setVisibility(View.GONE);
            qr_code.setVisibility(View.GONE);
            classlayout.setVisibility(View.GONE);
            classlayout3.setVisibility(View.GONE);
            txtSetup.setText("Fees Pending List");
        } else if (tag.equalsIgnoreCase("stu_wise_rec")) {
            txtSetup.setText("Filter Online Fees Paid \nList Student Wise");
            fetchSession();
            fetchgrouplist();
            month_lyt.setVisibility(View.GONE);
            stdlayout.setVisibility(View.VISIBLE);
            qr_code.setVisibility(View.GONE);
        } else if (tag.equalsIgnoreCase("fees_receipt_all")) {
            fetchSession();
            month_lyt.setVisibility(View.GONE);
            stdlayout.setVisibility(View.GONE);
            qr_code.setVisibility(View.GONE);
            classlayout.setVisibility(View.GONE);
            classlayout3.setVisibility(View.GONE);
            txtSetup.setText("Online Fees Paid List");
        } else if (tag.equalsIgnoreCase("payment_history")) {
            fetchSession();
            month_lyt.setVisibility(View.GONE);
            stdlayout.setVisibility(View.GONE);
            qr_code.setVisibility(View.GONE);
            classlayout.setVisibility(View.GONE);
            classlayout3.setVisibility(View.GONE);
            txtSetup.setText("Fees Payment Report");
        } else if (tag.equalsIgnoreCase("class_wise_rec")) {
            fetchSession();
            fetchgrouplist();
            month_lyt.setVisibility(View.GONE);
            stdlayout.setVisibility(View.GONE);
            qr_code.setVisibility(View.GONE);

            txtSetup.setText("Filter Online Fees Paid \nList Class Wise");
        } else {
            fetchSession();
            fetchgrouplist();
            qr_code.setVisibility(View.GONE);
            month_lyt.setVisibility(View.GONE);
            stdlayout.setVisibility(View.VISIBLE);
            txtSetup.setText("Pay Fees");
        }

        edtName.setEnabled(false);
        edtFather.setEnabled(false);
        edtRegistration.setEnabled(false);
        edtMobile.setEnabled(false);

//        if (!restorePrefData()){
//            setShowcaseView();
//        }

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //assignData();
                mBottomSheetDialog.dismiss();
            }
        });

        qr_code.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //assignData();
//                mBottomSheetDialog.dismiss();
                if (tag.equalsIgnoreCase("stu_wise")) {
                    Intent intent = new Intent(getActivity(), QRScanner.class);
                    intent.putExtra("tag", "stu_wise");
                    startActivity(intent);
                } else if (tag.equalsIgnoreCase("pending_stu_wise")) {
                    Intent intent = new Intent(getActivity(), QRScanner.class);
                    intent.putExtra("tag", "pending_stu_wise");
                    startActivity(intent);
                } else {
                    Intent intent = new Intent(getActivity(), QRScanner.class);
                    intent.putExtra("tag", "details");
                    startActivity(intent);
                }


            }
        });


        Button btnYes = (Button) sheetView.findViewById(R.id.btnYes);


        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //assignData();

                if (tag.equalsIgnoreCase("class_wise")) {
                    commonCode(tag);
                } else if (tag.equalsIgnoreCase("pending_class_wise")) {
                    commonCode(tag);
                } else if (tag.equalsIgnoreCase("class_wise_rec")) {
                    commonCode(tag);
                } else if (tag.equalsIgnoreCase("fees_receipt_all")) {
                    commonCodeAll(tag);
                } else if (tag.equalsIgnoreCase("fees_list_all")) {
                    commonCodeAll(tag);
                } else if (tag.equalsIgnoreCase("payment_history")) {
                    commonCodeAll(tag);
                } else if (tag.equalsIgnoreCase("pending_fees_list_all")) {
                    commonCodeAll(tag);
                } else {
                    if (session == null) {
                        Toast.makeText(getActivity(), "Kindly Select Session", Toast.LENGTH_SHORT).show();
                    } else if (g_id.equalsIgnoreCase("")) {
                        Toast.makeText(getActivity(), "Kindly Select Batch", Toast.LENGTH_SHORT).show();
                    } else if (classid.equalsIgnoreCase("")) {
                        Toast.makeText(getActivity(), "Kindly Select Class", Toast.LENGTH_SHORT).show();
                    } else if (stu_id.equalsIgnoreCase("")) {
                        Toast.makeText(getActivity(), "Kindly Select Student", Toast.LENGTH_SHORT).show();
                    } else {
                        Intent intent = null;

                        if (tag.equalsIgnoreCase("stu_wise")) {
                            intent = new Intent(getActivity(), CommonFeesListActivity.class);
                            // intent = new Intent(getActivity(), PayFeeSActivity.class);
                            intent.putExtra("tag", "student_wise");

                        } else if (tag.equalsIgnoreCase("pending_stu_wise")) {
                            intent = new Intent(getActivity(), CommonSTDPendingFeesListActivity.class);
                            intent.putExtra("tag", "student_wise");
                        } else if (tag.equalsIgnoreCase("stu_wise_rec")) {
//                            intent = new Intent(getActivity(), FeesRecipt_STD_Class.class);
//                            intent.putExtra("tag", "stu_wise_rec");

                            intent = new Intent(getActivity(), CommonFeesListActivity.class);
                            // intent = new Intent(getActivity(), PayFeeSActivity.class);
                            intent.putExtra("tag", "student_wise");
                            intent.putExtra("list_view", "pending");

                        } else {
                            intent = new Intent(getActivity(), PayFee_New.class);
                        }

                        intent.putExtra("name", edtName.getText().toString());
                        intent.putExtra("f_name", edtFather.getText().toString());
                        intent.putExtra("roll", edtRegistration.getText().toString());
                        intent.putExtra("dp", std_dp);
                        intent.putExtra("regist", regist);
                        intent.putExtra("class", classid);
                        intent.putExtra("class_name", className);
                        intent.putExtra("gender", gender);
                        intent.putExtra("fee_cate_id", fee_cate_id);
                        intent.putExtra("route_id", route_id);
                        intent.putExtra("place_id", place_id);
                        intent.putExtra("fromdate", session.getStart_date());
                        intent.putExtra("todate", session.getEnd_date());

                        intent.putExtra("jan_m", jan_m);
                        intent.putExtra("feb_m", feb_m);
                        intent.putExtra("mar_m", mar_m);
                        intent.putExtra("april_m", april_m);
                        intent.putExtra("may_m", may_m);
                        intent.putExtra("june_m", june_m);

                        intent.putExtra("july_m", july_m);
                        intent.putExtra("aug_m", aug_m);
                        intent.putExtra("sep_m", sep_m);
                        intent.putExtra("oct_m", oct_m);
                        intent.putExtra("nov_m", nov_m);
                        intent.putExtra("dec_m", dec_m);


                        startActivity(intent);
                        if (tag.equalsIgnoreCase("pay")) {
                            mBottomSheetDialog.dismiss();
                        }
                    }

                }


//                startActivity(new Intent(getContext(), PayFeeSActivity.class));
            }
        });
        mBottomSheetDialog.show();

    }

    private void commonCodeAll(String tag) {
        if (session == null) {
            Toast.makeText(getActivity(), "Kindly Select Session", Toast.LENGTH_SHORT).show();
        } else {

            Intent intent = null;

            if (tag.equalsIgnoreCase("fees_list_all")) {
                intent = new Intent(getActivity(), CommonPaidFeesListActivity.class);
                intent.putExtra("tag", tag);
            } else if (tag.equalsIgnoreCase("pending_fees_list_all")) {
                intent = new Intent(getActivity(), CommonPendingFeesListActivity.class);
                intent.putExtra("tag", tag);
            } else if (tag.equalsIgnoreCase("payment_history")) {
                intent = new Intent(getActivity(), FeePaymentHistory.class);
                intent.putExtra("tag", "fees_list_all");
            } else {
                // intent = new Intent(getActivity(), FeesRecipt_STD_Class.class);
                intent = new Intent(getActivity(), CommonPaidFeesListActivity.class);
                intent.putExtra("tag", tag);
            }
            intent.putExtra("name", edtName.getText().toString());
            intent.putExtra("f_name", edtFather.getText().toString());
            intent.putExtra("roll", edtRegistration.getText().toString());
            intent.putExtra("dp", std_dp);
            intent.putExtra("regist", regist);
            intent.putExtra("class", classid);
            intent.putExtra("class_name", className);
            intent.putExtra("gender", gender);
            intent.putExtra("fee_cate_id", fee_cate_id);
            intent.putExtra("route_id", route_id);
            intent.putExtra("place_id", place_id);
            intent.putExtra("fromdate", session.getStart_date());
            intent.putExtra("todate", session.getEnd_date());
            startActivity(intent);
        }
    }

    private void commonCode(String tag) {
        if (session == null) {
            Toast.makeText(getActivity(), "Kindly Select Session", Toast.LENGTH_SHORT).show();
        } else if (g_id.equalsIgnoreCase("")) {
            Toast.makeText(getActivity(), "Kindly Select Batch", Toast.LENGTH_SHORT).show();
        } else if (classid.equalsIgnoreCase("")) {
            Toast.makeText(getActivity(), "Kindly Select Class", Toast.LENGTH_SHORT).show();
        } else {

            Intent intent = null;

            if (tag.equalsIgnoreCase("class_wise")) {
                intent = new Intent(getActivity(), CommonPaidFeesListActivity.class);
                intent.putExtra("tag", tag);
            } else if (tag.equalsIgnoreCase("pending_class_wise")) {
                intent = new Intent(getActivity(), CommonPendingFeesListActivity.class);
                intent.putExtra("tag", "class_wise");
            } else {
                // intent = new Intent(getActivity(), FeesRecipt_STD_Class.class);
                intent = new Intent(getActivity(), CommonPaidFeesListActivity.class);
                intent.putExtra("tag", tag);
            }

            intent.putExtra("name", edtName.getText().toString());
            intent.putExtra("f_name", edtFather.getText().toString());
            intent.putExtra("roll", edtRegistration.getText().toString());
            intent.putExtra("dp", std_dp);
            intent.putExtra("regist", regist);
            intent.putExtra("class", classid);
            intent.putExtra("class_name", className);
            intent.putExtra("gender", gender);
            intent.putExtra("fee_cate_id", fee_cate_id);
            intent.putExtra("route_id", route_id);
            intent.putExtra("place_id", place_id);
            intent.putExtra("fromdate", session.getStart_date());
            intent.putExtra("todate", session.getEnd_date());
            startActivity(intent);
        }
    }


    @RequiresApi(api = Build.VERSION_CODES.N)
    private void fetchmonthList() {

        monthList.add("April");
        monthList.add("May");
        monthList.add("June");
        monthList.add("July");
        monthList.add("August");
        monthList.add("September");
        monthList.add("October");
        monthList.add("November");
        monthList.add("December");
        monthList.add("January");
        monthList.add("February");
        monthList.add("March");

        //   Toast.makeText(getApplicationContext(),month_name,Toast.LENGTH_SHORT).show();

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_item, monthList);
        //set the view for the Drop down list
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //set the ArrayAdapter to the spinner
        spinnerMonth.setAdapter(dataAdapter);

        android.icu.util.Calendar cal = android.icu.util.Calendar.getInstance();
        android.icu.text.SimpleDateFormat month_date = new android.icu.text.SimpleDateFormat("MMMM");
        String month_name = month_date.format(cal.getTime());

        // setting here spinner item selected with position..
        for (int i = 0; i < monthList.size(); i++) {
            if (monthList.get(i).equalsIgnoreCase(month_name)) {
                selectMonth = monthList.get(i);
                spinnerMonth.setSelection(i);

            }
        }

        //attach the listener to the spinner
        spinnerMonth.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long l) {
                selectMonth = monthList.get(pos);

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }


    private void fetchSession() {
        HashMap<String, String> params = new HashMap<String, String>();
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.SESSION_URL, sessionapiCallback, getActivity(), params);
    }


    ApiHandler.ApiCallback sessionapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            if (error == null) {
                try {
                    if (sessionlist != null) {
                        sessionlist.clear();
                    }
                    sessionlist = SessionBean.parseSessionArray(jsonObject.getJSONArray(Constants.SESSIONDATA));
                    SessionAdapter adapter = new SessionAdapter(getActivity(), sessionlist);
                    spinnerFinancialYear.setAdapter(adapter);
                    spinnerFinancialYear.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            session = sessionlist.get(i);
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                //   Utils.showSnackBar(error.getMessage(), parent);
            }
        }
    };

    private void fetchgrouplist() {
        commonProgress.show();
        HashMap<String, String> params = new HashMap<String, String>();
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.GROUP_LIST_URL, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                ErpProgress.cancelProgressBar();
                commonProgress.dismiss();
                if (error == null) {
                    try {
                        if (groupList != null) {
                            groupList.clear();
                        }

                        groupList = CommonBean.parseCommonArray(jsonObject.getJSONArray(Constants.GROUP_DATA));
                        GroupSpinnerAdapter adapter = new GroupSpinnerAdapter(getActivity(), groupList, 0);
                        spinnerGroup.setAdapter(adapter);
                        spinnerGroup.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                g_id = "";
                                classid = "";

                                if (i == 0) {
                                    if (adapter != null) {
                                        classList.clear();
                                        adapter.notifyDataSetChanged();
                                    }
                                }
                                if (i > 0) {
//                                    dataList.clear();
//                                    madapter.notifyDataSetChanged();
                                    g_id = groupList.get(i - 1).getId();
                                    classid = "";
                                    fetchclasslist(g_id);
                                }
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> adapterView) {

                            }
                        });
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    //Utils.showSnackBar(error.getMessage(), parent);
                }
            }
        }, getActivity(), params);
    }

    private void fetchclasslist(String g_id) {
        spinnerClass.setVisibility(View.VISIBLE);
        commonProgress.show();
        HashMap<String, String> params = new HashMap<String, String>();
//        params.put("check","subject");
        params.put("id", g_id);
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.GROUPCLASSLIST_URL, classapiCallback, getActivity(), params);
    }


    ApiHandler.ApiCallback classapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            commonProgress.dismiss();
            if (error == null) {
                try {
                    if (classList != null) {
                        classList.clear();
                    }

                    classList = ClassBean.parseClassArray(jsonObject.getJSONArray(Constants.CLASSDATA));
                    ClassAdapterAttend adapter = new ClassAdapterAttend(getActivity(), classList, 0);
                    spinnerClass.setAdapter(adapter);
                    spinnerClass.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                            if (i > 0) {
                                classid = classList.get(i - 1).getId();
                                className = classList.get(i - 1).getName();
                                fetchStudent(classid);
                                // g_id = classList.get(i-1).getGroup_data().getId();
                            } else {
                                classid = "";
                            }

                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_LONG).show();
                //    Utils.showSnackBar(error.getMessage(), parent);
            }
        }
    };

    private void fetchStudent(String classid) {
        spinnerstaff.setVisibility(View.VISIBLE);
        commonProgress.show();
        HashMap<String, String> params = new HashMap<String, String>();
        //  params.put("class_id", String.valueOf(classidsel));
        params.put("class_id", classid);
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.studentlistclass, apiCallback, getActivity(), params);

    }

    ApiHandler.ApiCallback apiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            commonProgress.dismiss();
            if (error == null) {
                try {
                    if (stuList != null) {
                        stuList.clear();
                    }

                    stuList = Studentbean.parseHWArray(jsonObject.getJSONArray("student_list"));
                    Stu_adapterGds adapterstu = new Stu_adapterGds(getActivity(), stuList);
                    spinnerstaff.setAdapter(adapterstu);
                    spinnerstaff.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                            edtFather.setText("");
                            edtName.setText("");
                            edtRegistration.setText("");
                            edtMobile.setText("");
                            Picasso.get().load(R.drawable.stud).into(dp);

                            if (i > 0) {
                                stu_id = stuList.get(i - 1).getId();
                                stuname = stuList.get(i - 1).getName();
                                sturoll = stuList.get(i - 1).getRoll_no();

                                student_lyt.setVisibility(View.VISIBLE);

                                if (stuList.get(i - 1).getGender().equalsIgnoreCase("Male")) {
                                    if (stuList.get(i - 1).getProfile() != null) {
                                        Utils.progressImg(stuList.get(i - 1).getProfile(), dp, getActivity());
                                        // Picasso.with(getActivity()).load(stuList.get(i-1).getProfile()).placeholder(R.drawable.stud).into(dp);
                                    } else {
                                        Picasso.get().load(R.drawable.stud).into(dp);
                                    }
                                } else {
                                    if (stuList.get(i - 1).getProfile() != null) {
                                        Utils.progressImg(stuList.get(i - 1).getProfile(), dp, getActivity());
                                        //  Picasso.with(getActivity()).load(stuList.get(i-1).getProfile()).placeholder(R.drawable.f_student).into(dp);
                                    } else {
                                        Picasso.get().load(R.drawable.f_student).into(dp);
                                    }
                                }
                                fee_cate_id = stuList.get(i - 1).getFee_cat_id();
                                regist = stuList.get(i - 1).getId();
                                gender = stuList.get(i - 1).getGender();
                                std_dp = stuList.get(i - 1).getProfile();

                                jan_m = stuList.get(i - 1).getJan_month();
                                feb_m = stuList.get(i - 1).getFeb_month();
                                mar_m = stuList.get(i - 1).getMar_month();
                                april_m = stuList.get(i - 1).getApr_month();
                                may_m = stuList.get(i - 1).getMay_month();
                                june_m = stuList.get(i - 1).getJune_month();
                                july_m = stuList.get(i - 1).getJuly_month();
                                aug_m = stuList.get(i - 1).getAug_month();
                                sep_m = stuList.get(i - 1).getSep_month();
                                oct_m = stuList.get(i - 1).getOct_month();
                                nov_m = stuList.get(i - 1).getNov_month();
                                dec_m = stuList.get(i - 1).getDec_month();

                                place_id = stuList.get(i - 1).getPlace_id();
                                route_id = stuList.get(i - 1).getRoute_id();
                                edtFather.setText(stuList.get(i - 1).getFather_name());
                                edtName.setText(stuList.get(i - 1).getName());
                                edtRegistration.setText(stuList.get(i - 1).getClass_name() + "(" + stuList.get(i - 1).getId() + ")");
                                edtMobile.setText(stuList.get(i - 1).getPhoneno());
                                // edtFather.setText(stuList.get(i-1).getF_name());

                            } else {

                                stu_id = "";
                            }

                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {

            }
        }
    };


    private void openonlineclassdialog() {

        final BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(getContext());
        final View sheetView = getLayoutInflater().inflate(R.layout.onlineclasss_dialog, null);
        mBottomSheetDialog.setContentView(sheetView);

        sheetView.findViewById(R.id.add).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(getActivity(), ViewOnlineClassTeacher.class));
            }
        });
        sheetView.findViewById(R.id.view).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), OnlineClassNew.class));
            }
        });


        RelativeLayout btnNo = (RelativeLayout) sheetView.findViewById(R.id.btnNo);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnNo.startAnimation(animation4);
                mBottomSheetDialog.dismiss();
            }
        });

        mBottomSheetDialog.show();

    }

    private void opengOnlineQuizdialogsec() {
        final BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(getContext());
        final View sheetView = getLayoutInflater().inflate(R.layout.stuquiz_dialog, null);
        mBottomSheetDialog.setContentView(sheetView);

        sheetView.findViewById(R.id.add).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String link = "http://test.ultimatesolutiongroup.com/adminResult.php?s_key=" + User.getCurrentUser().getSchoolData().getFi_school_id() + "";
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(link)));
                // startActivity(new Intent(getContext(), OnlineQuiz.class));
            }
        });
        sheetView.findViewById(R.id.view).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String link = "http://test.ultimatesolutiongroup.com/adminResult.php?s_key=" + User.getCurrentUser().getSchoolData().getFi_school_id() + "";
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(link)));
                //startActivity(new Intent(getContext(), ViewOnlineQuiz.class));
            }
        });


        RelativeLayout btnNo = (RelativeLayout) sheetView.findViewById(R.id.btnNo);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnNo.startAnimation(animation4);
                mBottomSheetDialog.dismiss();
            }
        });

        mBottomSheetDialog.show();
    }

    private void openassigndialogsec() {
        final BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(getContext());
        final View sheetView = getLayoutInflater().inflate(R.layout.assignment_dialog, null);
        mBottomSheetDialog.setContentView(sheetView);

        sheetView.findViewById(R.id.add).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), AddAssignment.class));
            }
        });
        sheetView.findViewById(R.id.view).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), ViewAssignment.class));
            }
        });
        sheetView.findViewById(R.id.reply).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), ReplyAssignmentList.class));
            }
        });


        RelativeLayout btnNo = (RelativeLayout) sheetView.findViewById(R.id.btnNo);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnNo.startAnimation(animation4);
                mBottomSheetDialog.dismiss();
            }
        });

        mBottomSheetDialog.show();
    }

    private void openexamdialogsec() {
        final BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(getContext());
        final View sheetView = getLayoutInflater().inflate(R.layout.exam_dialog, null);
        mBottomSheetDialog.setContentView(sheetView);
        sheetView.findViewById(R.id.add).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), Add_Exam.class));
            }
        });
        sheetView.findViewById(R.id.viewReport).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(getContext());
                final View sheetView = getLayoutInflater().inflate(R.layout.exam_dialog3, null);
                mBottomSheetDialog.setContentView(sheetView);


                sheetView.findViewById(R.id.add).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(getContext(), CheckReportCard.class);
                        intent.putExtra("name", "student");
                        startActivity(intent);

                    }
                });
                sheetView.findViewById(R.id.view).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                       // startActivity(new Intent(getContext(), ExamWebviewActivity.class));
                        Intent intent = new Intent(getContext(), ExamWebviewActivity.class);
                        intent.putExtra("name", "half");
                        startActivity(intent);
                    }
                });


                sheetView.findViewById(R.id.viewDatewise).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                     //   Toast.makeText(getActivity(), "Annually result not available", Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(getContext(), ExamWebviewActivity.class);
                        intent.putExtra("name", "full");
                        startActivity(intent);
                        //startActivity(new Intent(getContext(), Exam_Detail_Activity.class));
                    }
                });

                RelativeLayout btnNo = (RelativeLayout) sheetView.findViewById(R.id.btnNo);
                btnNo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        btnNo.startAnimation(animation4);
                        mBottomSheetDialog.dismiss();
                    }
                });

                mBottomSheetDialog.show();


                // startActivity(new Intent(getContext(), ReportCardNew.class));
            }
        });

        sheetView.findViewById(R.id.report_card).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), CheckReportCard.class);
                intent.putExtra("name", "student");
                startActivity(intent);
            }
        });

        sheetView.findViewById(R.id.btnYes).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (User.getCurrentUser().getSchoolData().getFi_school_id().equalsIgnoreCase("BFPS")) {
                    Intent intent = new Intent(getContext(), GazetteWebviewActivity.class);
                    intent.putExtra("name", "BFPS");
                    startActivity(intent);
                } else {
                    Intent intent = new Intent(getContext(), CheckReportCard.class);
                    intent.putExtra("name", "gazette");
                    startActivity(intent);
                }

            }
        });


        sheetView.findViewById(R.id.admit_card).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), GDSUploadExammrksSubjectWise.class);
                intent.putExtra("view", "admit_card");
                startActivity(intent);
            }
        });

        sheetView.findViewById(R.id.view).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(getContext());
                final View sheetView = getLayoutInflater().inflate(R.layout.exam_dialog2, null);
                mBottomSheetDialog.setContentView(sheetView);


                sheetView.findViewById(R.id.add).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        startActivity(new Intent(getContext(), ScheduleActivity.class));
                    }
                });
                sheetView.findViewById(R.id.view).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        startActivity(new Intent(getContext(), Exam_Detail_Activity.class));
                    }
                });
                RelativeLayout btnNo = (RelativeLayout) sheetView.findViewById(R.id.btnNo);
                btnNo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        btnNo.startAnimation(animation4);
                        mBottomSheetDialog.dismiss();
                    }
                });

                mBottomSheetDialog.show();


            }
        });
        sheetView.findViewById(R.id.viewDatewise).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), ResultsPermissionsActivity.class));
//                final BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(getContext());
//                final View sheetView = getLayoutInflater().inflate(R.layout.exam_dialog2, null);
//                mBottomSheetDialog.setContentView(sheetView);
//                final ViewGroup transitions_container = (ViewGroup) sheetView.findViewById(R.id.transitions_container);
//            TextView one=(TextView)sheetView.findViewById(R.id.add);
//            one.setText("Upload Marks StudentWise");
//                TextView two=(TextView)sheetView.findViewById(R.id.view);
//                two.setText("Upload Marks SubjectWise");
//                sheetView.findViewById(R.id.add).setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        startActivity(new Intent(getContext(), GDSUploadExamMrks.class));
//                    }
//                });
//                sheetView.findViewById(R.id.view).setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        startActivity(new Intent(getContext(), GDSUploadExammrksSubjectWise.class));
//                    }
//                });
//                RelativeLayout btnNo= (RelativeLayout) sheetView.findViewById(R.id.btnNo);
//                btnNo.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        btnNo.startAnimation(animation4);
//                        mBottomSheetDialog.dismiss();
//                    }
//                });
//
//                mBottomSheetDialog.show();
//
//                //startActivity(new Intent(getContext(), Homeworkbydate.class));
            }
        });

        RelativeLayout btnNo = (RelativeLayout) sheetView.findViewById(R.id.btnNo);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnNo.startAnimation(animation4);
                mBottomSheetDialog.dismiss();
            }
        });

        mBottomSheetDialog.show();
    }

    private void opengclasstestdialogsec(String tag) {
        final BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(getContext());
        final View sheetView = getLayoutInflater().inflate(R.layout.clstest_dialog, null);
        mBottomSheetDialog.setContentView(sheetView);

        TextView title = (TextView) sheetView.findViewById(R.id.title);
        ImageView image = (ImageView) sheetView.findViewById(R.id.image);
        Button add = (Button) sheetView.findViewById(R.id.add);
        Button view = (Button) sheetView.findViewById(R.id.view);

        if (tag.equalsIgnoreCase(Constants.sea)){
            title.setText(Constants.sea_d);
            add.setText("Class-Wise SEA");
            view.setText("Staff-Wise SEA");
            Picasso.get().load(R.drawable.exam_sea).into(image);
        }else{
            title.setText(Constants.test);
            add.setText("Class-Wise Test");
            view.setText("Staff-Wise Test");
            Picasso.get().load(R.drawable.test).into(image);
        }

        sheetView.findViewById(R.id.add).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (tag.equalsIgnoreCase(Constants.sea)){
                    Intent intent = new Intent(getContext(), ViewClassWiseTest.class);
                    intent.putExtra("name", "sea");
                    startActivity(intent);
                }else{
                    Intent intent = new Intent(getContext(), ViewClassWiseTest.class);
                    intent.putExtra("name", "test");
                    startActivity(intent);
                }
              //  startActivity(new Intent(getContext(), ViewClassWiseTest.class));
            }
        });
        sheetView.findViewById(R.id.view).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (tag.equalsIgnoreCase(Constants.sea)){
                    Intent intent = new Intent(getContext(), ViewStaffWiseTest.class);
                    intent.putExtra("name", "sea");
                    startActivity(intent);
                }else{
                    Intent intent = new Intent(getContext(), ViewStaffWiseTest.class);
                    intent.putExtra("name", "test");
                    startActivity(intent);
                }
           //     startActivity(new Intent(getContext(), ViewStaffWiseTest.class));
            }
        });


        RelativeLayout btnNo = (RelativeLayout) sheetView.findViewById(R.id.btnNo);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnNo.startAnimation(animation4);
                mBottomSheetDialog.dismiss();
            }
        });

        mBottomSheetDialog.show();
    }

    private void opengatepassdialogsec() {
        final BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(getContext());
        final View sheetView = getLayoutInflater().inflate(R.layout.stugatepass_dialog, null);
        mBottomSheetDialog.setContentView(sheetView);
        TextView btnYes = (TextView) sheetView.findViewById(R.id.btnYes);
        btnYes.setVisibility(View.VISIBLE);
        TextView view = (TextView) sheetView.findViewById(R.id.view);
        //  view.setText("Verified!");
        sheetView.findViewById(R.id.add).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), AddStuGatePass.class));
            }
        });
        sheetView.findViewById(R.id.btnYes).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), AddGatePersonDetails.class));
            }
        });
        sheetView.findViewById(R.id.view).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), ViewStudentGatePass.class));
            }
        });


        RelativeLayout btnNo = (RelativeLayout) sheetView.findViewById(R.id.btnNo);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnNo.startAnimation(animation4);
                mBottomSheetDialog.dismiss();
            }
        });

        mBottomSheetDialog.show();
    }

    private void opendatesheetdialogsec() {
        final BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(getContext());
        final View sheetView = getLayoutInflater().inflate(R.layout.ds_dialog, null);
        mBottomSheetDialog.setContentView(sheetView);

        sheetView.findViewById(R.id.add).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), AddDateSheetNew.class));
            }
        });
        sheetView.findViewById(R.id.view).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), ViewDatesheet.class));
            }
        });


        RelativeLayout btnNo = (RelativeLayout) sheetView.findViewById(R.id.btnNo);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnNo.startAnimation(animation4);
                mBottomSheetDialog.dismiss();
            }
        });

        mBottomSheetDialog.show();
    }


    private void openstudentdialogsec() {
        final BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(getContext());
        final View sheetView = getLayoutInflater().inflate(R.layout.stu_dialog, null);
        mBottomSheetDialog.setContentView(sheetView);

        sheetView.findViewById(R.id.add).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //  startActivity(new Intent(getContext(), StudentListByroll.class));
                startActivity(new Intent(getContext(), STDListActivity.class));
            }
        });
        sheetView.findViewById(R.id.view).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //  startActivity(new Intent(getContext(), StudentList.class));
                Intent intent = new Intent(getActivity(), StudentList.class);
                intent.putExtra("classid", "");
                intent.putExtra("check", "class");
                startActivity(intent);
            }
        });
        sheetView.findViewById(R.id.stubyHouse).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //  startActivity(new Intent(getContext(), StudentList.class));
                Intent intent = new Intent(getActivity(), StudentList.class);
                intent.putExtra("classid", "");
                intent.putExtra("check", "house");
                startActivity(intent);
            }
        });

        sheetView.findViewById(R.id.viewDatewise).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), StudentListBySection.class));
            }
        });
        sheetView.findViewById(R.id.male_female).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), Male_FemaleAnalysis.class));
            }
        });

        sheetView.findViewById(R.id.blocklist).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), MarkBlockStutent.class));
            }
        });
        sheetView.findViewById(R.id.stubyscan).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), StudentDetailByScanner.class));
            }
        });


        RelativeLayout btnNo = (RelativeLayout) sheetView.findViewById(R.id.btnNo);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnNo.startAnimation(animation4);
                mBottomSheetDialog.dismiss();
            }
        });

        mBottomSheetDialog.show();
    }

    private void openeattenddialogsec(String tag) {
        final BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(getContext());
        final View sheetView = getLayoutInflater().inflate(R.layout.attend_dialog, null);
        mBottomSheetDialog.setContentView(sheetView);

        Button add = (Button) sheetView.findViewById(R.id.add);
        Button viewwww = (Button) sheetView.findViewById(R.id.view);
        LinearLayout firstlaysec = (LinearLayout) sheetView.findViewById(R.id.firstlaysec);
        if (tag.equalsIgnoreCase("college")) {
            firstlaysec.setVisibility(View.GONE);
            add.setVisibility(View.GONE);
        } else {
            firstlaysec.setVisibility(View.GONE);
            add.setVisibility(View.GONE);
        }

        sheetView.findViewById(R.id.add).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                if (tag.equalsIgnoreCase("college")){
//                    startActivity(new Intent(getContext(), Add_SubAttendance.class));
//                }else{
                startActivity(new Intent(getContext(), AddAttendance.class));
                //  }

            }
        });
        viewwww.setText("View Class-wise");
        sheetView.findViewById(R.id.view).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (tag.equalsIgnoreCase("college")) {
                    startActivity(new Intent(getContext(), View_SubAttendance.class));
                } else {
                    startActivity(new Intent(getContext(), ViewAttendance.class));
                }
            }
        });

        sheetView.findViewById(R.id.qrattend).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), AttendanceByScanner.class));
            }
        });

        sheetView.findViewById(R.id.viewDatewise).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(getContext());
                final View sheetView = getLayoutInflater().inflate(R.layout.attend_dialog2, null);
                mBottomSheetDialog.setContentView(sheetView);


                Button viewDatewise = (Button) sheetView.findViewById(R.id.viewDatewise);
                viewDatewise.setVisibility(View.VISIBLE);
                viewDatewise.setText("YearWise");

                sheetView.findViewById(R.id.viewDatewise).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (tag.equalsIgnoreCase("college")) {
                            // startActivity(new Intent(getContext(), ClassReportGDS.class));
                            startActivity(new Intent(getContext(), Class_SubReportGDS.class));
                        } else {
                            startActivity(new Intent(getContext(), YearWiseReport.class));
                        }
                    }
                });

                sheetView.findViewById(R.id.add).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (tag.equalsIgnoreCase("college")) {
                            // startActivity(new Intent(getContext(), ClassReportGDS.class));
                            startActivity(new Intent(getContext(), Class_SubReportGDS.class));
                        } else {
                            startActivity(new Intent(getContext(), ClassReportGDS.class));
                        }
                    }
                });
                sheetView.findViewById(R.id.view).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (tag.equalsIgnoreCase("college")) {
                            startActivity(new Intent(getContext(), Sub_Monthwisereport.class));
                            // startActivity(new Intent(getContext(), Monthwisereport.class));
                        } else {
                            startActivity(new Intent(getContext(), Monthwisereport.class));
                        }

                    }
                });
                RelativeLayout btnNo = (RelativeLayout) sheetView.findViewById(R.id.btnNo);
                btnNo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        btnNo.startAnimation(animation4);
                        mBottomSheetDialog.dismiss();
                    }
                });

                mBottomSheetDialog.show();

                //startActivity(new Intent(getContext(), Homeworkbydate.class));
            }
        });
        RelativeLayout btnNo = (RelativeLayout) sheetView.findViewById(R.id.btnNo);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnNo.startAnimation(animation4);
                mBottomSheetDialog.dismiss();
            }
        });

        mBottomSheetDialog.show();
    }

    private void openvideoclasdialogsec() {
        final BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(getContext());
        final View sheetView = getLayoutInflater().inflate(R.layout.onlinevideo_dialog, null);
        mBottomSheetDialog.setContentView(sheetView);

        sheetView.findViewById(R.id.add).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), AddOnlineClassLink.class));
            }
        });
        sheetView.findViewById(R.id.view).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), ViewOnlineClass.class));
            }
        });
        RelativeLayout btnNo = (RelativeLayout) sheetView.findViewById(R.id.btnNo);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnNo.startAnimation(animation4);
                mBottomSheetDialog.dismiss();
            }
        });
        sheetView.findViewById(R.id.uploadvideo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), VideoUpload.class));
            }
        });

        mBottomSheetDialog.show();
    }

    private void openesyllabusdsdialogsec(String tag) {
        final BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(getContext());
        final View sheetView = getLayoutInflater().inflate(R.layout.syllabus_dialog, null);
        mBottomSheetDialog.setContentView(sheetView);

        TextView title = (TextView) sheetView.findViewById(R.id.txtTitle);
        ImageView image = (ImageView) sheetView.findViewById(R.id.image);

        if (tag.equalsIgnoreCase("Monthly Planner")) {
            Picasso.get().load(R.drawable.planner).into(image);
            title.setText(Constants.month_plan);
        } else {
            title.setText(Constants.syllabus);
            Picasso.get().load(R.drawable.syllabus).into(image);
        }

        sheetView.findViewById(R.id.add).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (tag.equalsIgnoreCase("Monthly Planner")) {
                    Intent i = new Intent(getActivity(), AddSyllabus.class);
                    i.putExtra("check", "month");
                    startActivity(i);
                } else {
                    Intent i = new Intent(getActivity(), AddSyllabus.class);
                    i.putExtra("check", "syllabus");
                    startActivity(i);
                }
            }
        });
        sheetView.findViewById(R.id.view).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (tag.equalsIgnoreCase("Monthly Planner")) {
                    Intent i = new Intent(getActivity(), ViewSyllabus.class);
                    i.putExtra("check", "month");
                    startActivity(i);
                } else {
                    Intent i = new Intent(getActivity(), ViewSyllabus.class);
                    i.putExtra("check", "syllabus");
                    startActivity(i);
                }
            }
        });

        RelativeLayout btnNo = (RelativeLayout) sheetView.findViewById(R.id.btnNo);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnNo.startAnimation(animation4);
                mBottomSheetDialog.dismiss();
            }
        });

        mBottomSheetDialog.show();
    }

    private void openebookdsdialogsec() {
        final BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(getContext());
        final View sheetView = getLayoutInflater().inflate(R.layout.ebooktable_dialog, null);
        mBottomSheetDialog.setContentView(sheetView);
        sheetView.findViewById(R.id.add).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), Ebook.class));
            }
        });
        sheetView.findViewById(R.id.view).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), ViewEbook.class));
            }
        });
        RelativeLayout btnNo = (RelativeLayout) sheetView.findViewById(R.id.btnNo);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnNo.startAnimation(animation4);
                mBottomSheetDialog.dismiss();
            }
        });


        mBottomSheetDialog.show();
    }

    private void opentimetabledsdialogsec() {
        final BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(getContext());
        final View sheetView = getLayoutInflater().inflate(R.layout.timetable_dialog, null);
        mBottomSheetDialog.setContentView(sheetView);

        sheetView.findViewById(R.id.add).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), AddTimeTable.class));
            }
        });
        sheetView.findViewById(R.id.view).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), ViewTimeTable.class));
            }
        });
        RelativeLayout btnNo = (RelativeLayout) sheetView.findViewById(R.id.btnNo);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnNo.startAnimation(animation4);
                mBottomSheetDialog.dismiss();
            }
        });


        mBottomSheetDialog.show();
    }

    private void opencwdialogsec() {
        final BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(getContext());
        final View sheetView = getLayoutInflater().inflate(R.layout.cw_dialog, null);
        mBottomSheetDialog.setContentView(sheetView);

        sheetView.findViewById(R.id.add).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), ViewClassWork.class));
            }
        });
        sheetView.findViewById(R.id.view).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), StaffWiseCWActivity.class));
            }
        });

        sheetView.findViewById(R.id.viewDatewise).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), Classwork_Report.class));
            }
        });

        sheetView.findViewById(R.id.viewDatewisePend).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getActivity(), Hw_Cw_Staff_Activity.class);
                i.putExtra("check", "cw");
                startActivity(i);
            }
        });

        RelativeLayout btnNo = (RelativeLayout) sheetView.findViewById(R.id.btnNo);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnNo.startAnimation(animation4);
                mBottomSheetDialog.dismiss();
            }
        });

        mBottomSheetDialog.show();
    }

    private void openhwgdsdialogsec() {
        final BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(getContext());
        final View sheetView = getLayoutInflater().inflate(R.layout.feedback_dialog, null);
        mBottomSheetDialog.setContentView(sheetView);

        sheetView.findViewById(R.id.add).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), GETHomeworkActivity.class);
                intent.putExtra("view", "normal");
                startActivity(intent);
            }
        });
        sheetView.findViewById(R.id.view).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), StaffWiseHWActivity.class));
            }
        });

        sheetView.findViewById(R.id.viewDatewise).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), Homework_Report.class));
            }
        });

        sheetView.findViewById(R.id.viewDatewisePend).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getActivity(), Hw_Cw_Staff_Activity.class);
                i.putExtra("check", "hw");
                startActivity(i);
            }
        });

        sheetView.findViewById(R.id.holidayHW).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), GETHomeworkActivity.class);
                intent.putExtra("view", "holiday");
                startActivity(intent);
            }
        });

        RelativeLayout btnNo = (RelativeLayout) sheetView.findViewById(R.id.btnNo);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnNo.startAnimation(animation4);
                mBottomSheetDialog.dismiss();
            }
        });

        mBottomSheetDialog.show();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (getView() != null && isVisibleToUser) {
            Log.i("1st", "1st");
//            if (!restorePrefData()){
//                setShowcaseView();
//            }
        }
    }

    private void setShowcaseView() {
        new TapTargetSequence(getActivity())
                .targets(
                        TapTarget.forView(lytttt_school, "School Icon", "Tap the School Icon for view School Details.")
                                .outerCircleColor(R.color.light)
                                .outerCircleAlpha(0.96f)
                                .targetCircleColor(R.color.white)
                                .titleTextSize(20)
                                .titleTextColor(R.color.white)
                                .descriptionTextSize(14)
                                .descriptionTextColor(R.color.black)
                                .textColor(R.color.black)
                                .textTypeface(Typeface.SANS_SERIF)
                                .dimColor(R.color.black)
                                .drawShadow(true)
                                .cancelable(false)
                                .tintTarget(true)
                                .transparentTarget(true)
                                .targetRadius(40),
                        TapTarget.forView(lytttt, "Message Icon", "Tap the Message Icon to view latest Message.")
                                .outerCircleColor(R.color.light)
                                .outerCircleAlpha(0.96f)
                                .targetCircleColor(R.color.white)
                                .titleTextSize(20)
                                .titleTextColor(R.color.white)
                                .descriptionTextSize(14)
                                .descriptionTextColor(R.color.black)
                                .textColor(R.color.black)
                                .textTypeface(Typeface.SANS_SERIF)
                                .dimColor(R.color.black)
                                .drawShadow(true)
                                .cancelable(false)
                                .tintTarget(true)
                                .transparentTarget(true)
                                .targetRadius(40),
                        TapTarget.forView(lytttt_fees, "Notification Icon", "Tap the Notification Icon to view today Notification list.")
                                .outerCircleColor(R.color.light)
                                .outerCircleAlpha(0.96f)
                                .targetCircleColor(R.color.white)
                                .titleTextSize(20)
                                .titleTextColor(R.color.white)
                                .descriptionTextSize(14)
                                .descriptionTextColor(R.color.black)
                                .textColor(R.color.black)
                                .textTypeface(Typeface.SANS_SERIF)
                                .dimColor(R.color.black)
                                .drawShadow(true)
                                .cancelable(false)
                                .tintTarget(true)
                                .transparentTarget(true)
                                .targetRadius(40)).listener(new TapTargetSequence.Listener() {
                    @Override
                    public void onSequenceFinish() {
                        //Toast.makeText(getActivity(),"Sequence Finished",Toast.LENGTH_SHORT).show();
                        savePrefData();
                    }

                    @Override
                    public void onSequenceStep(TapTarget lastTarget, boolean targetClicked) {

                        // Toast.makeText(getActivity(),"GREAT!",Toast.LENGTH_SHORT).show();

                    }

                    @Override
                    public void onSequenceCanceled(TapTarget lastTarget) {

                    }
                }).start();
    }

    private void savePrefData() {
        sharedPreferences = getActivity().getSharedPreferences("boarding_pref_tool", MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean("IsFirstTimeVisit_tool", true);
        editor.apply();
    }

    private boolean restorePrefData() {
        sharedPreferences = getActivity().getSharedPreferences("boarding_pref_tool", MODE_PRIVATE);
        return sharedPreferences.getBoolean("IsFirstTimeVisit_tool", false);
    }

    @Override
    public void onResume() {
        super.onResume();
        fetnewmsg();
        fetnewmsgNoti();
        userprofile();
    }

    private void userprofile() {
        // UltimateProgress.showProgressBar(getActivity(),"Please Wait........!");
        HashMap<String, String> params = new HashMap<>();
        params.put("user_id", User.getCurrentUser().getId());
        params.put("check", "fetch");


        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.UPDATE_SCHOOL_INFO, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                if (error == null) {
                    try {

                        image_url = jsonObject.getJSONObject(Constants.USERDATA).getString("logo");
                        school = jsonObject.getJSONObject(Constants.USERDATA).getString("name");
                        phone = jsonObject.getJSONObject(Constants.USERDATA).getString("phoneno");
                        address1 = jsonObject.getJSONObject(Constants.USERDATA).getString("address");
                        email1 = jsonObject.getJSONObject(Constants.USERDATA).getString("email");
                        website = jsonObject.getJSONObject(Constants.USERDATA).getString("website");

                        latitude = jsonObject.getJSONObject(Constants.USERDATA).getString("latitude");
                        longitude = jsonObject.getJSONObject(Constants.USERDATA).getString("longitude");

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    //
                }
            }
        }, getActivity(), params);
    }

    private void fetnewmsgNoti() {
        HashMap<String, String> params = new HashMap<>();
        params.put("user_id", User.getCurrentUser().getId());
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.NEW_MSG_NOTI, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                if (error == null) {

                    try {
                        Log.e("USERDATA", jsonObject.getJSONObject("doc_data").toString());

                        fees_count.setText(jsonObject.getJSONObject("doc_data").getString("noti_count"));

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                } else {
                    fees_count.setText("0");
                    // Toast.makeText(getActivity(),error.getMessage(),Toast.LENGTH_LONG).show();
                    //Utils.showSnackBar(error.getMessage(), parent);
                }
            }
        }, getActivity(), params);
    }


    private void fetnewmsg() {
        HashMap<String, String> params = new HashMap<>();
        params.put("user_id", User.getCurrentUser().getId());
        params.put("check", "list");
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.NEW_MSG, apicallback5, getActivity(), params);
    }

    ApiHandler.ApiCallback apicallback5 = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();

            if (error == null) {

                try {
                    Log.e("USERDATA", jsonObject.getJSONObject("doc_data").toString());

                    msg_count.setText(jsonObject.getJSONObject("doc_data").getString("count"));

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            } else {
                msg_count.setText("0");
                // Toast.makeText(getActivity(),error.getMessage(),Toast.LENGTH_LONG).show();
                //Utils.showSnackBar(error.getMessage(), parent);
            }
        }
    };

    //text to speech programmatically...
    public void TextToSpeechFunction() {
        textToSpeech.speak(textholder, TextToSpeech.QUEUE_FLUSH, null);
    }

    @Override
    public void onInit(int status) {
        if (status == TextToSpeech.SUCCESS) {
            textToSpeech.setLanguage(Locale.US);
            if (speech == 0) {
                TextToSpeechFunction();
            }
        }
    }

    @Override
    public void onDestroy() {
        if (textToSpeech != null) {
            textToSpeech.shutdown();
        }
        super.onDestroy();
    }


    @Override
    public void deleteNotification(NotificationBean notiList) {
        speech = 1;
        textholder = notiList.getN_title() + " ! " + notiList.getN_msg();
        TextToSpeechFunction();
    }


}
