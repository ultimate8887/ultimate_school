package com.ultimate.ultimatesmartschool.Home;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;

import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.andremion.floatingnavigationview.FloatingNavigationView;
import com.android.volley.Request;
import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetSequence;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;
import com.squareup.picasso.Picasso;
import com.ultimate.ultimatesmartschool.Activity.AddSignatureActivity;
import com.ultimate.ultimatesmartschool.Activity.LocationActivity;
import com.ultimate.ultimatesmartschool.Activity.SchoolInfoActivity;
import com.ultimate.ultimatesmartschool.Activity.UserActivity;
import com.ultimate.ultimatesmartschool.Activity.UserPermissions;
import com.ultimate.ultimatesmartschool.Activity.User_Permission;
import com.ultimate.ultimatesmartschool.AddThought.ViewThought;
import com.ultimate.ultimatesmartschool.Banner.BannerActivity;
import com.ultimate.ultimatesmartschool.Banner.BannerActivityNew;
import com.ultimate.ultimatesmartschool.BeanModule.CommonBean;
import com.ultimate.ultimatesmartschool.BeanModule.User;

import androidx.multidex.BuildConfig;

import com.ultimate.ultimatesmartschool.DietChart.UploadmealplanActivity;
import com.ultimate.ultimatesmartschool.Examination.GDSUploadExamMrks;
import com.ultimate.ultimatesmartschool.Examination.GDSUploadExammrksSubjectWise;
import com.ultimate.ultimatesmartschool.Fee.New_Fees.CommonPaidFeesListActivity;
import com.ultimate.ultimatesmartschool.Gallery.Add_album;
import com.ultimate.ultimatesmartschool.Gallery.Add_photo;
import com.ultimate.ultimatesmartschool.Gallery.NewGallery.ViewGalleryGrid;
import com.ultimate.ultimatesmartschool.Gallery.ViewGallery;
import com.ultimate.ultimatesmartschool.Health_Module.Add_Health;
import com.ultimate.ultimatesmartschool.Health_Module.View_Health;
import com.ultimate.ultimatesmartschool.Holiday.AddHolidayActivity;
import com.ultimate.ultimatesmartschool.Homework.StaffListAdapter;
import com.ultimate.ultimatesmartschool.ImageToPDFConvert.ImageToPdfConvert;
import com.ultimate.ultimatesmartschool.Leave.StaffLeaveList;
import com.ultimate.ultimatesmartschool.Login.LoginActivity;
import com.ultimate.ultimatesmartschool.MSG_NEW.SendMSG;
import com.ultimate.ultimatesmartschool.Message.AdminComposeMsgActivity;
import com.ultimate.ultimatesmartschool.Message.CompStuMsg;
import com.ultimate.ultimatesmartschool.Message.ComposeStudentMsg;
import com.ultimate.ultimatesmartschool.Message.InboxActivity;
import com.ultimate.ultimatesmartschool.Message.MultiClassComposeActivity;
import com.ultimate.ultimatesmartschool.Message.MultiStaffCompose_msgActivity;
import com.ultimate.ultimatesmartschool.Message.Sent_MessageActivity;
import com.ultimate.ultimatesmartschool.Message.StaffCompose_msgActivity;
import com.ultimate.ultimatesmartschool.Message.StafftostuchatActivity;
import com.ultimate.ultimatesmartschool.Message.StdToStaffWiseMsg;
import com.ultimate.ultimatesmartschool.NoticeMod.AddNoticeBoard;
import com.ultimate.ultimatesmartschool.NoticeMod.ViewNoticeInfo;
import com.ultimate.ultimatesmartschool.NotificationMod.NotificationAct;
import com.ultimate.ultimatesmartschool.NotificationMod.NotificationAdapter;
import com.ultimate.ultimatesmartschool.NotificationMod.NotificationBean;
import com.ultimate.ultimatesmartschool.OCRMode.OcrCaptureActivity;
import com.ultimate.ultimatesmartschool.OnlineClass.Addvideogal;
import com.ultimate.ultimatesmartschool.OnlineClass.ViewVideogal;
import com.ultimate.ultimatesmartschool.PTMFeedBack.PTMFeedbackList;
import com.ultimate.ultimatesmartschool.PTMFeedBack.PTMList;
import com.ultimate.ultimatesmartschool.Quetions.QuestionsActivities;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.SchoolActivity.SchoolActivityMSGComposeActivity;
import com.ultimate.ultimatesmartschool.SchoolActivity.ViewStudents;
import com.ultimate.ultimatesmartschool.SchoolSetup.GSCActivity;
import com.ultimate.ultimatesmartschool.Security.SecurityActivity;
import com.ultimate.ultimatesmartschool.Security.SecurityReport;
import com.ultimate.ultimatesmartschool.Social_Post.AddSocialLinks;
import com.ultimate.ultimatesmartschool.Social_Post.SocialLinkList;
import com.ultimate.ultimatesmartschool.Staff.AssignIncharge;
import com.ultimate.ultimatesmartschool.Staff.AssignSubject;
import com.ultimate.ultimatesmartschool.Staff.StaffBirthdayList;
import com.ultimate.ultimatesmartschool.Staff.View_staff_bean;
import com.ultimate.ultimatesmartschool.StaffAttendance.DepartmentAdapter;
import com.ultimate.ultimatesmartschool.StaffAttendance.ViewStaffAttendance;
import com.ultimate.ultimatesmartschool.StaffAttendance.ViewStaffQRAttendence;
import com.ultimate.ultimatesmartschool.Student.STD_Fragment.STDListActivity_Enquiry;
import com.ultimate.ultimatesmartschool.Student.StudentBirthdayList;
import com.ultimate.ultimatesmartschool.Student.StudentListByroll;
import com.ultimate.ultimatesmartschool.Transport.AddFuelExpense;
import com.ultimate.ultimatesmartschool.Transport.AllotVicleToRouteMod.VicleToRouteActivity;
import com.ultimate.ultimatesmartschool.Transport.AlotDrivrToVicleMod.DrivrToVicleActivity;
import com.ultimate.ultimatesmartschool.Transport.DriverListMod.DriverListActivity;
import com.ultimate.ultimatesmartschool.Transport.DriverNotice.SentDriverNotice;
import com.ultimate.ultimatesmartschool.Transport.FuelExpenseList;
import com.ultimate.ultimatesmartschool.Transport.PickUpMod.PickUpActivity;
import com.ultimate.ultimatesmartschool.Transport.RouteMod.RouteActivity;
import com.ultimate.ultimatesmartschool.Transport.VehicleListMod.VehicleListActivity;
import com.ultimate.ultimatesmartschool.Transport.VehicleWiseComposeActivity;
import com.ultimate.ultimatesmartschool.Transport.VehicleWiseStudentListActivity;
import com.ultimate.ultimatesmartschool.Transport.driverlocation.DriverLocActivity;
import com.ultimate.ultimatesmartschool.TransportAttendane.CheckOutTransAttenActivity;
import com.ultimate.ultimatesmartschool.TransportAttendane.EvenCheckOutTransAttenActivity;
import com.ultimate.ultimatesmartschool.TransportAttendane.EvenTransAttendListActivity;
import com.ultimate.ultimatesmartschool.TransportAttendane.EveningDefaulterList;
import com.ultimate.ultimatesmartschool.TransportAttendane.EveningScanMarkAttendance;
import com.ultimate.ultimatesmartschool.TransportAttendane.MorningDefaulterList;
import com.ultimate.ultimatesmartschool.TransportAttendane.TransAttendListActivity;
import com.ultimate.ultimatesmartschool.TransportAttendane.TransScanMarkAttendance;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.CommonProgress;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.content.Context.MODE_PRIVATE;


public class AdminFragment extends Fragment implements DashBoardAdapter.OpenDahsboardActivity, TextToSpeech.OnInitListener, NotificationAdapter.AdapterCallback {
    List<User_Permission> datalist = new ArrayList<>();
    User_Permission user_permissions;
    SharedPreferences sharedPreferences;
    String password = "", c_password = "", image_url = "";
    @BindView(R.id.msg_count)
    TextView msg_count;
    @BindView(R.id.lytttt)
    RelativeLayout lytttt;
    @BindView(R.id.totalRecord)
    TextView totalRecord;
    @BindView(R.id.fees_count)
    TextView fees_count;
    @BindView(R.id.lytttt_school)
    RelativeLayout lytttt_school;
    @BindView(R.id.lytttt_fees)
    RelativeLayout lytttt_fees;
    int loaded = 0;
    @BindView(R.id.searchView)
    SearchView searchView;
    CommonProgress commonProgress;

    String staffid = "", staffname = "", staffimg = "";
    Spinner spinnerDepartment;
    //    @BindView(R.id.spinnerstudnt)
    Spinner spinnerstaff;
    private ArrayList<CommonBean> departmentList;
    private String departmentid = "", departmentname = "";

    private ArrayList<View_staff_bean> stafflist;
    private StaffListAdapter adapterstafflist;


    String setup, student_panel, staff_panel, feepay, stud_attend, staff_attend, examination, syllabus, timetable, assignment, homework, classwork, message, notice,
            thought, photo, ebook, transport, idcard, library, visitor, elearning, gatepass, classtest;

    int count = 0, speech = 0;
    TextToSpeech textToSpeech;
    String textholder = "", title = "";
    RecyclerView notiRV;
    ArrayList<NotificationBean> notiList = new ArrayList<>();
    private NotificationAdapter notiAdapter;
    @BindView(R.id.textNorecord)
    TextView txtNorecord;
    RecyclerView recyclerView;
    private DashBoardAdapter adapter;
    private ArrayList<DashboardBean> dataList;

    private FloatingNavigationView mFloatingNavigationView;
    Animation animation1, animation2, animation3, animation4;
    BottomSheetDialog mBottomSheetDialog;

    AppCompatTextView admin_address, phone1, admin_email, name, staff;
    ImageView profile_img, profile_bg;

    @BindView(R.id.schName)
    TextView headtext;
    @BindView(R.id.imgLogo)
    CircularImageView navi_profile;

    TextView admin_name;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.adminfrag_layout, container, false);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        commonProgress = new CommonProgress(getActivity());
        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        count++;
        // Toast.makeText(getActivity(), String.valueOf(count), Toast.LENGTH_SHORT).show();
        dataList = new ArrayList<>();
        dataList = addList();
        loaded = dataList.size();
        totalRecord.setText("Total Module:- " + String.valueOf(loaded));
        checkLoaded(loaded);
        startAnimation();
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 3));
        adapter = new DashBoardAdapter(getActivity(), dataList, this);
        recyclerView.setAdapter(adapter);
        textToSpeech = new TextToSpeech(getActivity(), this);

        lytttt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                msg_count.startAnimation(animation4);
//                Intent intent= new Intent(getActivity(), InboxActivity.class);
//                intent.putExtra("Today","Today");
//                startActivity(intent);
                Intent intent = new Intent(getActivity(), SendMSG.class);
                intent.putExtra("id", "inbox");
                startActivity(intent);
            }
        });

//        Toast.makeText(getContext(), user_permissions.getSetup(), Toast.LENGTH_SHORT).show();

        lytttt_fees.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fees_count.startAnimation(animation4);

                if (!fees_count.getText().toString().equalsIgnoreCase("0")) {
                    openDialog();
                } else {
                    Toast.makeText(getContext(), "No New Notification Found!", Toast.LENGTH_SHORT).show();
                }
                // startActivity(new Intent(getActivity(), FeePaidListActivity.class));
            }

        });


        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                //FILTER AS YOU TYPE

                if (query.length() == 0) {
                    totalRecord.setText("Total Module:- " + String.valueOf(loaded));
                    checkLoaded(loaded);
                } else {
                    totalRecord.setText("Search Module:- " + String.valueOf(adapter.dataList.size()));
                    checkLoaded(adapter.dataList.size());
                }
                adapter.getFilter().filter(query);

                return false;
            }
        });

        if (User.getCurrentUser().getSchoolData().getName() != null) {
            headtext.setText(User.getCurrentUser().getSchoolData().getName());
        }
        setUserData();

        mFloatingNavigationView = (FloatingNavigationView) view.findViewById(R.id.floating_navigation_view);
        startAnimation();
        mFloatingNavigationView = (FloatingNavigationView) view.findViewById(R.id.floating_navigation_view);
        View headerView = mFloatingNavigationView.getHeaderView(0);
        //navi_profile = (CircularImageView) headerView.findViewById(R.id.navi_profile);
        admin_name = (TextView) headerView.findViewById(R.id.stu_name);
        admin_name.setText(User.getCurrentUser().getFirstname());

        mFloatingNavigationView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mFloatingNavigationView.open();
            }
        });
        mFloatingNavigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem item) {

                if (item.getTitle().equals("Change Password")) {
                    // Toast.makeText(getActivity(),"Change Password",Toast.LENGTH_SHORT).show();
                    // reset_password();
                } else if (item.getTitle().equals("Feedback")) {
                    Toast.makeText(getActivity(), "Feedback", Toast.LENGTH_SHORT).show();
                    //  open_changePassword();
                } else if (item.getTitle().equals("Rate Us")) {
                    Uri uri = Uri.parse("market://details?id=" + getActivity().getPackageName());
                    Intent myAppLinkToMarket = new Intent(Intent.ACTION_VIEW, uri);
                    try {
                        startActivity(myAppLinkToMarket);
                    } catch (ActivityNotFoundException e) {

                    }
                } else if (item.getTitle().equals("Share Us")) {
                    try {
                        Intent i = new Intent(Intent.ACTION_SEND);
                        i.setType("text/plain");
                        i.putExtra(Intent.EXTRA_SUBJECT, "XYZ Ultimate School");
                        String sAux = "\nLet me recommend you this application\n\n";
                        sAux = sAux + "https://play.google.com/store/apps/details?id=" + getActivity().getPackageName() + "\n\n";
                        i.putExtra(Intent.EXTRA_TEXT, sAux);
                        startActivity(Intent.createChooser(i, "choose one"));
                    } catch (Exception e) {
                        //e.toString();
                    }
                } else if (item.getTitle().equals("Privacy Policy")) {
                    Toast.makeText(getActivity(), "Privacy Policy", Toast.LENGTH_SHORT).show();

                    BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(getActivity());
                    final View sheetView = getLayoutInflater().inflate(R.layout.privacy_policy_dialog, null);
                    mBottomSheetDialog.setContentView(sheetView);
                    mBottomSheetDialog.setCancelable(false);
                    final boolean[] loadingFinished = {true};
                    final boolean[] redirect = {false};
                    WebView webView = (WebView) sheetView.findViewById(R.id.privacy);

                    ErpProgress.showProgressBar(getActivity(), "Please Wait.......!");
                    if (!loadingFinished[0]) {
                        redirect[0] = true;
                    }
                    loadingFinished[0] = false;
                    webView.getSettings().setJavaScriptEnabled(true);
                    String url = "http://ultimatesolutiongroup.com/privacy_policy.html";
                    webView.loadUrl(url);
                    webView.setWebViewClient(new WebViewClient() {
                        @Override
                        public void onPageFinished(WebView view, String url) {
                            if (!redirect[0]) {
                                loadingFinished[0] = true;
                            }
                            if (loadingFinished[0] && !redirect[0]) {
                                ErpProgress.cancelProgressBar();
                            } else {
                                redirect[0] = false;
                            }
                        }
                    });

                    RelativeLayout btnNo = (RelativeLayout) sheetView.findViewById(R.id.btnNo);
                    btnNo.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            btnNo.startAnimation(animation4);
                            mBottomSheetDialog.dismiss();
                        }
                    });
                    mBottomSheetDialog.show();
                } else if (item.getTitle().equals("App Info")) {
                    Toast.makeText(getActivity(), "App Info", Toast.LENGTH_SHORT).show();
                    oenInfoDialog();
                } else {
                    // Snackbar.make((View) mFloatingNavigationView.getParent(), item.getTitle() + " Selected!", Snackbar.LENGTH_SHORT).show();
                    //    Toast.makeText(getActivity(),"Logout!",Toast.LENGTH_SHORT).show();

                    final Dialog logoutDialog = new Dialog(getActivity());
                    logoutDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    logoutDialog.setCancelable(true);
                    logoutDialog.setContentView(R.layout.logout_dialog);
                    logoutDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                    TextView txt2 = (TextView) logoutDialog.findViewById(R.id.txt2);

                    String logout_user = User.getCurrentUser().getFirstname();
                    txt2.setText("Are you sure" + " " + logout_user + "!" + " " + "want to logout?");

//                    CircularImageView img = (CircularImageView) logoutDialog.findViewById(R.id.img);
//
//                    if (User.getCurrentUser().getProfile() != null) {
//                        Picasso.with(getActivity()).load(User.getCurrentUser().getProfile()).placeholder(R.drawable.boy).into(navi_profile);
//                    } else {
//                        Picasso.with(getActivity()).load(R.drawable.boy).into(navi_profile);
//                    }

                    Button btnNo = (Button) logoutDialog.findViewById(R.id.btnNo);
                    btnNo.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            logoutDialog.dismiss();
                        }
                    });
                    Button btnYes = (Button) logoutDialog.findViewById(R.id.btnYes);
                    btnYes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            btnYes.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    logoutDialog.dismiss();
                                    ErpProgress.showProgressBar(getContext(), "Please wait...");
                                    User.getCurrentUser().logout();
                                    Toast.makeText(getContext(), "Logged out successfully", Toast.LENGTH_SHORT).show();
                                    Intent loginpage = new Intent(getContext(), LoginActivity.class);
                                    loginpage.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                                            Intent.FLAG_ACTIVITY_CLEAR_TASK |
                                            Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(loginpage);
                                    ErpProgress.cancelProgressBar();
                                    getActivity().finish();
                                }
                            });
                        }
                    });
                    logoutDialog.show();

                }

                //  Snackbar.make((View) mFloatingNavigationView.getParent(), item.getTitle() + " Selected!", Snackbar.LENGTH_SHORT).show();
                mFloatingNavigationView.close();


                Snackbar.make((View) mFloatingNavigationView.getParent(), item.getTitle() + " Selected!", Snackbar.LENGTH_SHORT).show();
                mFloatingNavigationView.close();
                return true;
            }
        });
    }


    private void checkLoaded(int loaded) {
        if (loaded == 0) {
            txtNorecord.setVisibility(View.VISIBLE);
        } else {
            txtNorecord.setVisibility(View.GONE);
        }
    }

    private void openDialog() {
        BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(getActivity());
        final View sheetView = getLayoutInflater().inflate(R.layout.noti_msgimage_lyt, null);
        mBottomSheetDialog.setContentView(sheetView);
        mBottomSheetDialog.setCancelable(true);
        notiRV = (RecyclerView) sheetView.findViewById(R.id.recyclerView);

        TextView tittle = (TextView) sheetView.findViewById(R.id.tittle);
        String count = fees_count.getText().toString();

        if (!count.equalsIgnoreCase("0")) {
            String title = getColoredSpanned(count, "#e31e25");
            String Name = getColoredSpanned(" New Notification", "#000000");
            tittle.setText(Html.fromHtml(title + " " + Name));
        } else {
            tittle.setText("Today Notification");
        }

        notiList = new ArrayList<>();
        LinearLayoutManager layoutManager1 = new LinearLayoutManager(getActivity());
        notiRV.setLayoutManager(layoutManager1);
        notiAdapter = new NotificationAdapter(notiList, this, getActivity());
        notiRV.setAdapter(notiAdapter);
        fetchNotification();

        RelativeLayout btnNo = (RelativeLayout) sheetView.findViewById(R.id.btnNo);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                mBottomSheetDialog.dismiss();
            }
        });

        mBottomSheetDialog.show();
    }

    public void fetchNotification() {
        HashMap<String, String> params = new HashMap<>();
        params.put("id", User.getCurrentUser().getId());
        params.put("type", "10");
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.NOTIFICATIONURL, notiapiCallback, getActivity(), params);
    }

    ApiHandler.ApiCallback notiapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            if (error == null) {
                try {
                    JSONArray noticeArray = jsonObject.getJSONArray("notice_data");
                    notiList = NotificationBean.parseNotificationArray(noticeArray);
                    if (notiList.size() > 0) {
                        notiAdapter.setNotificationList(notiList);
                        //setanimation on adapter...
                        notiRV.getAdapter().notifyDataSetChanged();
                        notiRV.scheduleLayoutAnimation();
                        //-----------end------------
                    } else {
                        notiList.clear();
                        notiAdapter.setNotificationList(notiList);
                        notiAdapter.notifyDataSetChanged();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                notiList.clear();
                notiAdapter.setNotificationList(notiList);
                notiAdapter.notifyDataSetChanged();

            }
        }
    };


    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }

    @OnClick(R.id.imgLogo)
    public void imgProfilesss() {
        navi_profile.startAnimation(animation4);
        //   startActivity(new Intent(getActivity(), VideoActivity.class));
        //  Animatoo.animateShrink(getActivity());
        Toast.makeText(getActivity(), "View Profile Details", Toast.LENGTH_SHORT).show();

        mBottomSheetDialog = new BottomSheetDialog(getActivity());
        final View sheetView = getLayoutInflater().inflate(R.layout.profile_dialog, null);
        mBottomSheetDialog.setContentView(sheetView);
        mBottomSheetDialog.setCancelable(true);

        profile_img = (ImageView) sheetView.findViewById(R.id.profile_img);
        profile_bg = (ImageView) sheetView.findViewById(R.id.profile_bg);
        admin_address = (AppCompatTextView) sheetView.findViewById(R.id.admin_address);
        phone1 = (AppCompatTextView) sheetView.findViewById(R.id.phone);
        admin_email = (AppCompatTextView) sheetView.findViewById(R.id.admin_email);
        name = (AppCompatTextView) sheetView.findViewById(R.id.name);
        staff = (AppCompatTextView) sheetView.findViewById(R.id.staff);
        setProfileData();
        RelativeLayout btnNo = (RelativeLayout) sheetView.findViewById(R.id.btnNo);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnNo.startAnimation(animation4);
                mBottomSheetDialog.dismiss();
            }
        });

        mBottomSheetDialog.show();


    }

    private void userprofile() {
        // UltimateProgress.showProgressBar(getActivity(),"Please Wait........!");
        HashMap<String, String> params = new HashMap<>();
        params.put("user_id", User.getCurrentUser().getId());
        params.put("image", "");


        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.UPDATE_IMG, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                ErpProgress.cancelProgressBar();
                if (error == null) {
                    try {
                        image_url = jsonObject.getJSONObject(Constants.USERDATA).getString("profile");
                        Log.i("USERDATA-Home", image_url);

                        // Utils.setNaviHeaderDatas(stu_name,class_namessss,navi_profile,image_url,getActivity());
//
                        if (!image_url.equalsIgnoreCase("https://ultimatesolutiongroup.com/office_admin/images/admin/")) {
                            Picasso.get().load(image_url).placeholder(R.drawable.boy).into(navi_profile);
                        } else {
                            Picasso.get().load(R.drawable.boy).into(navi_profile);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    //  Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }, getActivity(), params);
    }

    @Override
    public void onResume() {
        super.onResume();
        userprofile();
        fetnewmsg();
        fetnewmsgNoti();
        checkUserpermission();
    }

    private void checkUserpermission() {
        HashMap<String, String> params = new HashMap<>();
        params.put("user_id", User.getCurrentUser().getId());
        params.put("check", "");
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.USER_PERMISSION, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                if (error == null) {
                    try {
                        Log.e("USERDATA", String.valueOf(jsonObject.getJSONObject("admin_data")));

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    // fees_count.setText(jsonObject.getJSONObject("doc_data").getString("noti_count"));

                } else {
                    // msg_count.setText("0");
                    // Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_LONG).show();
                    //Utils.showSnackBar(error.getMessage(), parent);
                }
            }
        }, getActivity(), params);
    }

    private void fetnewmsgNoti() {
        HashMap<String, String> params = new HashMap<>();
        params.put("user_id", User.getCurrentUser().getId());
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.NEW_MSG_NOTI, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                if (error == null) {

                    try {
                        Log.e("USERDATA", jsonObject.getJSONObject("doc_data").toString());

                        fees_count.setText(jsonObject.getJSONObject("doc_data").getString("noti_count"));

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                } else {
                    fees_count.setText("0");
                    // Toast.makeText(getActivity(),error.getMessage(),Toast.LENGTH_LONG).show();
                    //Utils.showSnackBar(error.getMessage(), parent);
                }
            }
        }, getActivity(), params);
    }


    private void fetnewmsg() {
        HashMap<String, String> params = new HashMap<>();
        params.put("user_id", User.getCurrentUser().getId());
        params.put("check", "list");
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.NEW_MSG, apicallback5, getActivity(), params);
    }

    ApiHandler.ApiCallback apicallback5 = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();

            if (error == null) {

                try {
                    Log.e("USERDATA", jsonObject.getJSONObject("doc_data").toString());

                    msg_count.setText(jsonObject.getJSONObject("doc_data").getString("count"));

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            } else {
                msg_count.setText("0");
                // Toast.makeText(getActivity(),error.getMessage(),Toast.LENGTH_LONG).show();
                //Utils.showSnackBar(error.getMessage(), parent);
            }
        }
    };


    private void setProfileData() {

        name.setText(User.getCurrentUser().getFirstname() + "(" + User.getCurrentUser().getId() + ")");

        //   Log.e("getAddress", User.getCurrentUser().getAddress() );
//           Toast.makeText(getActivity(),User.getCurrentUser().getAddress(),Toast.LENGTH_SHORT).show();
        if (User.getCurrentUser().getEmail() == null) {
            admin_email.setText("Not Mentioned");
        } else {
            admin_email.setText(User.getCurrentUser().getEmail());
        }
        if (User.getCurrentUser().getPhoneno() != null) {
            phone1.setText(User.getCurrentUser().getPhoneno());
        } else {
            phone1.setText("Not Mentioned");
        }
        if (User.getCurrentUser().getAddress() != null) {
            admin_address.setText(User.getCurrentUser().getAddress());
        } else {
            admin_address.setText("Not Mentioned");
        }
//
//        if (User.getCurrentUser().getDepartment() != null && User.getCurrentUser().getPost() != null){
//            staff.setText(User.getCurrentUser().getDepartment()+" ("+User.getCurrentUser().getPost()+")");
//        } else{
//            staff.setText("Staff");
//        }


        if (!image_url.equalsIgnoreCase("https://ultimatesolutiongroup.com/office_admin/images/staff/")) {
            //    Picasso.with(getActivity()).load(image_url).placeholder(R.drawable.boy).into(profile_bg);
            Picasso.get().load(image_url).placeholder(R.drawable.boy).into(profile_img);
        } else {
            //    Picasso.with(getActivity()).load(R.drawable.boy).into(profile_bg);
            Picasso.get().load(R.drawable.boy).into(profile_img);
        }


        if (User.getCurrentUser().getProfile() != null) {
            // Picasso.with(getActivity()).load(User.getCurrentUser().getProfile()).placeholder(R.drawable.stud).into(profile_img);
            Picasso.get().load(User.getCurrentUser().getSchoolData().getLogo()).placeholder(R.drawable.stud).into(profile_bg);
        }

    }

    private void opentransportdialogsec() {


        final BottomSheetDialog transportDialog = new BottomSheetDialog(getActivity());
        final View sheetView = getLayoutInflater().inflate(R.layout.transport_dialog, null);
        transportDialog.setContentView(sheetView);
        RelativeLayout btnNo = (RelativeLayout) sheetView.findViewById(R.id.btnNo);
        Button txtLoc = (Button) transportDialog.findViewById(R.id.txtLoc);

        txtLoc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getActivity(), "Module Under Working!", Toast.LENGTH_SHORT).show();

//              Intent intent = new Intent(getActivity(), DriverLocActivity.class);
//              startActivity(intent);
                //   transportDialog.dismiss();
            }
        });
        Button txtRoute = (Button) transportDialog.findViewById(R.id.txtRoute);
        txtRoute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), RouteActivity.class);
                startActivity(intent);
                //  transportDialog.dismiss();
            }
        });
        Button txtPickUpPoint = (Button) transportDialog.findViewById(R.id.txtPickUpPoint);
        txtPickUpPoint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(getActivity(), PickUpActivity.class);
                startActivity(intent);
                //  transportDialog.dismiss();
            }
        });
        Button txtVehicleList = (Button) transportDialog.findViewById(R.id.txtVehicleList);
        txtVehicleList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(getActivity(), VehicleListActivity.class);
                startActivity(intent);
                //   transportDialog.dismiss();
            }
        });
        Button txtDriverList = (Button) transportDialog.findViewById(R.id.txtDriverList);
        txtDriverList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(getActivity(), DriverListActivity.class);
                startActivity(intent);
                // transportDialog.dismiss();
            }
        });
        Button txtDrivrToVehicle = (Button) transportDialog.findViewById(R.id.txtDrivrToVehicle);
        txtDrivrToVehicle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(getActivity(), DrivrToVicleActivity.class);
                startActivity(intent);
                //   transportDialog.dismiss();
            }
        });

        Button txtVehiToRoute = (Button) transportDialog.findViewById(R.id.txtVehiToRoute);
        txtVehiToRoute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(getActivity(), VicleToRouteActivity.class);
                startActivity(intent);
                //   transportDialog.dismiss();
            }
        });

        Button txtnoticedriver = (Button) transportDialog.findViewById(R.id.txtnoticedriver);
        txtnoticedriver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getActivity(), "Module Under Working!", Toast.LENGTH_SHORT).show();
//               Intent intent = new Intent(getActivity(), SentDriverNotice.class);
//               startActivity(intent);
                //   transportDialog.dismiss();
            }
        });


        Button txtMarkattendance = (Button) transportDialog.findViewById(R.id.txtMarkattendance);
        txtMarkattendance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(getActivity(), TransScanMarkAttendance.class);
                startActivity(intent);
                //   transportDialog.dismiss();
            }
        });

        Button txtStdVehList = (Button) transportDialog.findViewById(R.id.txtStdVehList);
        txtStdVehList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), VehicleWiseStudentListActivity.class);
                startActivity(intent);
                transportDialog.dismiss();
            }
        });

        Button txtStdVehCompMSG = (Button) transportDialog.findViewById(R.id.txtStdVehCompMSG);
        txtStdVehCompMSG.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(getActivity(), VehicleWiseComposeActivity.class);
                startActivity(intent);
                transportDialog.dismiss();
            }
        });

//        txtstudentattend.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                Intent intent = new Intent(getActivity(), TransAttendListActivity.class);
//                startActivity(intent);
//                transportDialog.dismiss();
//            }
//        });

        sheetView.findViewById(R.id.txtstudentattend).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(getContext());
                final View sheetView = getLayoutInflater().inflate(R.layout.checkoutransdialog, null);
                mBottomSheetDialog.setContentView(sheetView);
                TextView one = (TextView) sheetView.findViewById(R.id.add);

                TextView two = (TextView) sheetView.findViewById(R.id.view);

                TextView checkin = (TextView) sheetView.findViewById(R.id.view);

                sheetView.findViewById(R.id.checkin).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(getActivity(), TransScanMarkAttendance.class);
                        startActivity(intent);

                    }
                });
                sheetView.findViewById(R.id.add).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(getActivity(), TransAttendListActivity.class);
                        startActivity(intent);

                    }
                });
                sheetView.findViewById(R.id.view).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(getActivity(), CheckOutTransAttenActivity.class);
                        startActivity(intent);

                    }
                });

                sheetView.findViewById(R.id.mordefin).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(getActivity(), MorningDefaulterList.class);
                        startActivity(intent);

                    }
                });


                RelativeLayout btnNo = (RelativeLayout) sheetView.findViewById(R.id.btnNo);
                btnNo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        btnNo.startAnimation(animation4);
                        mBottomSheetDialog.dismiss();
                    }
                });

                mBottomSheetDialog.show();

                //startActivity(new Intent(getContext(), Homeworkbydate.class));
            }
        });


        sheetView.findViewById(R.id.txtstudentattendeve).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(getContext());
                final View sheetView = getLayoutInflater().inflate(R.layout.checkoutransdialogeven, null);
                mBottomSheetDialog.setContentView(sheetView);
                TextView one = (TextView) sheetView.findViewById(R.id.add);

                TextView two = (TextView) sheetView.findViewById(R.id.view);
                TextView checkin = (TextView) sheetView.findViewById(R.id.view);
                sheetView.findViewById(R.id.checkin).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(getActivity(), EveningScanMarkAttendance.class);
                        startActivity(intent);

                    }
                });
                sheetView.findViewById(R.id.add).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        Intent intent = new Intent(getActivity(), EvenTransAttendListActivity.class);
                        startActivity(intent);

                    }
                });
                sheetView.findViewById(R.id.view).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(getActivity(), EvenCheckOutTransAttenActivity.class);
                        startActivity(intent);

                    }
                });

                sheetView.findViewById(R.id.evedefin).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(getActivity(), EveningDefaulterList.class);
                        startActivity(intent);

                    }
                });
                RelativeLayout btnNo = (RelativeLayout) sheetView.findViewById(R.id.btnNo);
                btnNo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        btnNo.startAnimation(animation4);
                        mBottomSheetDialog.dismiss();
                    }
                });

                mBottomSheetDialog.show();

                //startActivity(new Intent(getContext(), Homeworkbydate.class));
            }
        });
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnNo.startAnimation(animation4);
                transportDialog.dismiss();
            }
        });
        transportDialog.show();
    }

    private void setUserData() {

        //  Utils.setNaviHeaderData(txtStudName,class_name,imgStud,getActivity());

        // Log.i("logoooooooooo",User.getCurrentUser().getSchoolData().getLogo());

//        if(User.getCurrentUser().getSchoolData().getLogo()!=null){
//            Picasso.with(getActivity()).load(User.getCurrentUser().getSchoolData().getLogo()).placeholder(R.drawable.logo).into(navi_profile);
//        }else{
//            Picasso.with(getActivity()).load(R.drawable.logo).into(navi_profile);
//        }
//
//        if (User.getCurrentUser().getId() != null) {
//            roll_no.setText("Email: " + User.getCurrentUser().getEmail());
//        } else {
//            roll_no.setText("");
//        }

    }

    @SuppressLint("RestrictedApi")
    private void oenInfoDialog() {


        mBottomSheetDialog = new BottomSheetDialog(getActivity());
        final View sheetView = getLayoutInflater().inflate(R.layout.app_info_dialog, null);
        mBottomSheetDialog.setContentView(sheetView);

        TextView txtVersion = (TextView) sheetView.findViewById(R.id.txtVersion);
        FloatingActionButton whatsapp = (FloatingActionButton) sheetView.findViewById(R.id.whatsapp);
        FloatingActionButton email = (FloatingActionButton) sheetView.findViewById(R.id.email);
        FloatingActionButton phone = (FloatingActionButton) sheetView.findViewById(R.id.phone);
        String number = "7009091606";
        String versionName = "";
        int code = 0;
        try {
            versionName = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0).versionName;
            code = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0).versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        txtVersion.setText("App Version Code: " + code + "\n" + "App Version Name: " + String.valueOf(versionName));

        email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //  mBottomSheetDialog.dismiss();
                ErpProgress.showProgressBar(getActivity(), "Please Wait.....!");
                Intent intent = new Intent(Intent.ACTION_SENDTO); // it's not ACTION_SEND
                intent.setType("text/plain");
                intent.putExtra(Intent.EXTRA_SUBJECT, "Subject of email");
                intent.putExtra(Intent.EXTRA_TEXT, "Body of email");
                //simplydirect email for setting email,,,
                intent.setData(Uri.parse("mailto:dealingkings112@gmail.com")); // or just "mailto:" for blank
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK); // this will make such that when user returns to your app, your app is displayed, instead of the email app.
                startActivity(intent);
                ErpProgress.cancelProgressBar();

            }
        });
        whatsapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //  mBottomSheetDialog.dismiss();
                Uri uri = Uri.parse("smsto:" + number);
                Intent i = new Intent(Intent.ACTION_SENDTO, uri);
                i.setPackage("com.whatsapp");
                startActivity(Intent.createChooser(i, "Chat Now!"));
            }
        });
        phone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // mBottomSheetDialog.dismiss();
                // for permission granted....
                if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CALL_PHONE}, 1);
                } else {
                    Intent intent = new Intent(Intent.ACTION_CALL);
                    intent.setData(Uri.parse("tel:" + number));
                    startActivity(intent);
                    ErpProgress.cancelProgressBar();
                }
            }
        });


        RelativeLayout btnNo = (RelativeLayout) sheetView.findViewById(R.id.btnNo);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnNo.startAnimation(animation4);
                mBottomSheetDialog.dismiss();
            }
        });
        mBottomSheetDialog.show();

    }

    private void openmsgdialogsec() {
        final BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(getContext());
        final View sheetView = getLayoutInflater().inflate(R.layout.message_dialog, null);
        mBottomSheetDialog.setContentView(sheetView);
        // LinearLayout sec = (LinearLayout) sheetView.findViewById(R.id.seclay);

        Button sec = (Button) sheetView.findViewById(R.id.stfstumsg);

        if (User.getCurrentUser().getType().equalsIgnoreCase("user")) {
            sec.setVisibility(View.GONE);
        } else {
            sec.setVisibility(View.VISIBLE);
        }
        sheetView.findViewById(R.id.add).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(getContext());
                final View sheetView = getLayoutInflater().inflate(R.layout.message_dialog2, null);
                mBottomSheetDialog.setContentView(sheetView);

                sheetView.findViewById(R.id.add).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        startActivity(new Intent(getContext(), ComposeStudentMsg.class));
                    }
                });
                sheetView.findViewById(R.id.view).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        startActivity(new Intent(getContext(), MultiClassComposeActivity.class));
                    }
                });
                sheetView.findViewById(R.id.staff).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        startActivity(new Intent(getContext(), StaffCompose_msgActivity.class));
                    }
                });
                sheetView.findViewById(R.id.multstaff).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        startActivity(new Intent(getContext(), MultiStaffCompose_msgActivity.class));
                    }
                });
                sheetView.findViewById(R.id.admin).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        startActivity(new Intent(getContext(), AdminComposeMsgActivity.class));
                    }
                });

                RelativeLayout btnNo = (RelativeLayout) sheetView.findViewById(R.id.btnNo);
                btnNo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        btnNo.startAnimation(animation4);
                        mBottomSheetDialog.dismiss();
                    }
                });

                mBottomSheetDialog.show();


            }
        });
        sheetView.findViewById(R.id.view).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // startActivity(new Intent(getContext(), InboxActivity.class));
                Intent intent = new Intent(getActivity(), SendMSG.class);
                intent.putExtra("id", "inbox");
                startActivity(intent);
            }
        });
        sheetView.findViewById(R.id.sent).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), SendMSG.class);
                intent.putExtra("id", "send");
                startActivity(intent);
                //  startActivity(new Intent(getContext(), Sent_MessageActivity.class));
                //  startActivity(new Intent(getContext(), SendMSG.class));
            }
        });
        sheetView.findViewById(R.id.stfstfmsg).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), StafftostuchatActivity.class));
            }
        });
        sheetView.findViewById(R.id.stfstumsg).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), StafftostuchatActivity.class));
            }
        });

        sheetView.findViewById(R.id.stdtostafffmsg).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), StdToStaffWiseMsg.class));
            }
        });


        RelativeLayout btnNo = (RelativeLayout) sheetView.findViewById(R.id.btnNo);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnNo.startAnimation(animation4);
                mBottomSheetDialog.dismiss();
            }
        });


        mBottomSheetDialog.show();
    }

    private void opengallerydialogsec() {
        final BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(getContext());
        final View sheetView = getLayoutInflater().inflate(R.layout.gallery_dialog, null);
        mBottomSheetDialog.setContentView(sheetView);

        sheetView.findViewById(R.id.add).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), Add_album.class));
            }
        });
        sheetView.findViewById(R.id.view).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), Add_photo.class));
            }
        });

        sheetView.findViewById(R.id.viewphoto).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), ViewGalleryGrid.class));
            }
        });

        sheetView.findViewById(R.id.addvideo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), Addvideogal.class));
            }
        });

        sheetView.findViewById(R.id.viewvideo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), ViewVideogal.class));
            }
        });
        RelativeLayout btnNo = (RelativeLayout) sheetView.findViewById(R.id.btnNo);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnNo.startAnimation(animation4);
                mBottomSheetDialog.dismiss();
            }
        });


        mBottomSheetDialog.show();
    }

    private void opentimetabledsdialogsecccc() {
        final BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(getContext());
        final View sheetView = getLayoutInflater().inflate(R.layout.noticeboard_dialog, null);
        mBottomSheetDialog.setContentView(sheetView);

        Button add = (Button) sheetView.findViewById(R.id.add);
        Button vieww = (Button) sheetView.findViewById(R.id.view);
        ImageView img = (ImageView) sheetView.findViewById(R.id.img);
        TextView txtTitle = (TextView) sheetView.findViewById(R.id.txtTitle);

        add.setText("Student");
        vieww.setText("Staff");
        txtTitle.setText("Birthday List");
        img.setImageDrawable(getResources().getDrawable(R.drawable.birthday_cake));

        sheetView.findViewById(R.id.add).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), StudentBirthdayList.class));
            }
        });
        sheetView.findViewById(R.id.view).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), StaffBirthdayList.class));
            }
        });
        RelativeLayout btnNo = (RelativeLayout) sheetView.findViewById(R.id.btnNo);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnNo.startAnimation(animation4);
                mBottomSheetDialog.dismiss();
            }
        });


        mBottomSheetDialog.show();
    }


    private void opentimetabledsdialogsec(String tag) {
        final BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(getContext());
        final View sheetView = getLayoutInflater().inflate(R.layout.noticeboard_dialog, null);
        mBottomSheetDialog.setContentView(sheetView);
        TextView tittle = sheetView.findViewById(R.id.txtTitle);
        ImageView img = sheetView.findViewById(R.id.img);

        Button add_btn = sheetView.findViewById(R.id.add);
        Button view_btn = sheetView.findViewById(R.id.view);


        if (tag.equalsIgnoreCase(Constants.fuel_expense)) {
            tittle.setText(tag);
            img.setImageDrawable(getResources().getDrawable(R.drawable.fuel));
        } else if (tag.equalsIgnoreCase(Constants.feedback)) {
            tittle.setText(tag);
            add_btn.setText("Add/View PTM");
            view_btn.setText("View Feedback");
            img.setImageDrawable(getResources().getDrawable(R.drawable.feedback_icon));
        } else {
            tittle.setText(tag);
            img.setImageDrawable(getResources().getDrawable(R.drawable.chalk_board_icon));
        }

        sheetView.findViewById(R.id.add).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (tag.equalsIgnoreCase(Constants.fuel_expense)) {
                    startActivity(new Intent(getContext(), AddFuelExpense.class));
                }else if (tag.equalsIgnoreCase(Constants.feedback)) {
                    startActivity(new Intent(getContext(), PTMList.class));
                }  else {
                    startActivity(new Intent(getContext(), AddNoticeBoard.class));
                }

            }
        });
        sheetView.findViewById(R.id.view).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (tag.equalsIgnoreCase(Constants.fuel_expense)) {
                    startActivity(new Intent(getContext(), FuelExpenseList.class));
                }else if (tag.equalsIgnoreCase(Constants.feedback)) {
                    startActivity(new Intent(getContext(), PTMFeedbackList.class));
                }  else {
                    startActivity(new Intent(getContext(), ViewNoticeInfo.class));
                }

            }
        });
        RelativeLayout btnNo = (RelativeLayout) sheetView.findViewById(R.id.btnNo);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnNo.startAnimation(animation4);
                mBottomSheetDialog.dismiss();
            }
        });


        mBottomSheetDialog.show();
    }

    private void startAnimation() {
        animation1 = AnimationUtils.loadAnimation(getActivity(), R.anim.logo_animation);
        animation2 = AnimationUtils.loadAnimation(getActivity(), R.anim.enter_from_left_animation);
        animation3 = AnimationUtils.loadAnimation(getActivity(), R.anim.fade_animation);
        animation4 = AnimationUtils.loadAnimation(getActivity(), R.anim.btn_blink_animation);

    }

    @SuppressLint("UseCompatLoadingForDrawables")
    private ArrayList<DashboardBean> addList() {

        ArrayList<DashboardBean> list = new ArrayList<>();
        // Toast.makeText(getActivity(), UserPermissions.getCurrentUserPermissions().getSetup(), Toast.LENGTH_SHORT).show();


        if (User.getCurrentUser().getType().equalsIgnoreCase("user")) {

            if (UserPermissions.getCurrentUserPermissions().getSetup().equalsIgnoreCase("yes")) {
                DashboardBean staffObj = new DashboardBean();
                staffObj.setTitle(Constants.schoolsetup);
                staffObj.setMIcon(getResources().getDrawable(R.drawable.systems));
                list.add(staffObj);
            }
            if (UserPermissions.getCurrentUserPermissions().getEs_enquiry().equalsIgnoreCase("yes")) {
                DashboardBean staffObjAdd = new DashboardBean();
                staffObjAdd.setTitle(Constants.admission_enq);
                staffObjAdd.setMIcon(getResources().getDrawable(R.drawable.addmission_list));
                list.add(staffObjAdd);
            }

            if (UserPermissions.getCurrentUserPermissions().getThought().equalsIgnoreCase("yes")) {
                DashboardBean staffObj1 = new DashboardBean();
                staffObj1.setTitle(Constants.thought);
                staffObj1.setMIcon(getResources().getDrawable(R.drawable.bulb));
                list.add(staffObj1);
            }

            if (UserPermissions.getCurrentUserPermissions().getEs_holiday().equalsIgnoreCase("yes")) {
                DashboardBean staffObj17 = new DashboardBean();
                staffObj17.setTitle(Constants.holiday);
                staffObj17.setMIcon(getResources().getDrawable(R.drawable.holiday_calendar));
                list.add(staffObj17);
            }

            if (UserPermissions.getCurrentUserPermissions().getEs_school_activity().equalsIgnoreCase("yes")) {
                DashboardBean hostlObj = new DashboardBean();
                hostlObj.setTitle(Constants.ACTIVITY);
                hostlObj.setMIcon(getResources().getDrawable(R.drawable.school_activty));
                list.add(hostlObj);
            }


            if (UserPermissions.getCurrentUserPermissions().getBanner().equalsIgnoreCase("yes")) {

                DashboardBean staffObj3 = new DashboardBean();
                staffObj3.setTitle(Constants.banner);
                staffObj3.setMIcon(getResources().getDrawable(R.drawable.poster));
                list.add(staffObj3);

            }

            if (UserPermissions.getCurrentUserPermissions().getEs_social_post().equalsIgnoreCase("yes")) {

                DashboardBean staffObj31 = new DashboardBean();
                staffObj31.setTitle(Constants.post);
                staffObj31.setMIcon(getResources().getDrawable(R.drawable.social_media));
                list.add(staffObj31);

            }

//            if (UserPermissions.getCurrentUserPermissions().getNotice().equalsIgnoreCase("yes")){
            DashboardBean gateObj1 = new DashboardBean();
            gateObj1.setTitle(Constants.NoticeBoard);
            gateObj1.setMIcon(getResources().getDrawable(R.drawable.chalk_board_icon));
            list.add(gateObj1);
//            }


            //  if (UserPermissions.getCurrentUserPermissions().getMessage().equalsIgnoreCase("yes")){
            DashboardBean sattendanceObj = new DashboardBean();
            sattendanceObj.setTitle(Constants.Message);
            sattendanceObj.setMIcon(getResources().getDrawable(R.drawable.new_message_icon));
            list.add(sattendanceObj);
            // }


            if (UserPermissions.getCurrentUserPermissions().getTransport().equalsIgnoreCase("yes")) {
                DashboardBean gateObj = new DashboardBean();
                gateObj.setTitle(Constants.Transport);
                gateObj.setMIcon(getResources().getDrawable(R.drawable.bus_school));
                list.add(gateObj);
            }

            if (UserPermissions.getCurrentUserPermissions().getEs_expense().equalsIgnoreCase("yes")) {
                DashboardBean gdsattendanceObj1 = new DashboardBean();
                gdsattendanceObj1.setTitle(Constants.fuel_expense);
                gdsattendanceObj1.setMIcon(getResources().getDrawable(R.drawable.fuel));
                list.add(gdsattendanceObj1);
            }

            if (UserPermissions.getCurrentUserPermissions().getPhoto().equalsIgnoreCase("yes")) {

                DashboardBean leaveObj = new DashboardBean();
                leaveObj.setTitle(Constants.Gallery);
                leaveObj.setMIcon(getResources().getDrawable(R.drawable.document_photo_icon));
                list.add(leaveObj);
            }


            if (UserPermissions.getCurrentUserPermissions().getEs_visitor().equalsIgnoreCase("yes")) {
                DashboardBean ebookObj = new DashboardBean();
                ebookObj.setTitle(Constants.Visitor);
                ebookObj.setMIcon(getResources().getDrawable(R.drawable.guard));
                list.add(ebookObj);
            }

//            if (UserPermissions.getCurrentUserPermissions().getEs_feedback().equalsIgnoreCase("yes")){
//                DashboardBean ebookObj = new DashboardBean();
//                ebookObj.setTitle(Constants.feedback);
//                ebookObj.setMIcon(getResources().getDrawable(R.drawable.feedback));
//                list.add(ebookObj);
//            }
//
//            if (UserPermissions.getCurrentUserPermissions().getThought().equalsIgnoreCase("yes")){
//
//            }


        } else if (User.getCurrentUser().getType().equalsIgnoreCase("admin")
                || User.getCurrentUser().getType().equalsIgnoreCase("super")) {

            DashboardBean userObjs = new DashboardBean();
            userObjs.setTitle(Constants.school);
            userObjs.setMIcon(getResources().getDrawable(R.drawable.school));
            list.add(userObjs);


            DashboardBean staffObj11 = new DashboardBean();
            staffObj11.setTitle(Constants.signature);
            staffObj11.setMIcon(getResources().getDrawable(R.drawable.digital_signature));
            list.add(staffObj11);

            DashboardBean userObj = new DashboardBean();
            userObj.setTitle(Constants.userlist);
            userObj.setMIcon(getResources().getDrawable(R.drawable.evaluation));
            list.add(userObj);


            DashboardBean staffObj = new DashboardBean();
            staffObj.setTitle(Constants.schoolsetup);
            staffObj.setMIcon(getResources().getDrawable(R.drawable.systems));
            list.add(staffObj);

            DashboardBean staffObjAdd = new DashboardBean();
            staffObjAdd.setTitle(Constants.admission_enq);
            staffObjAdd.setMIcon(getResources().getDrawable(R.drawable.addmission_list));
            list.add(staffObjAdd);


            DashboardBean staffObj1 = new DashboardBean();
            staffObj1.setTitle(Constants.thought);
            staffObj1.setMIcon(getResources().getDrawable(R.drawable.bulb));
            list.add(staffObj1);

            DashboardBean staffObj17 = new DashboardBean();
            staffObj17.setTitle(Constants.holiday);
            staffObj17.setMIcon(getResources().getDrawable(R.drawable.holiday_calendar));
            list.add(staffObj17);


            DashboardBean hostlObj = new DashboardBean();
            hostlObj.setTitle(Constants.ACTIVITY);
            hostlObj.setMIcon(getResources().getDrawable(R.drawable.school_activty));
            list.add(hostlObj);

            DashboardBean staffObj3 = new DashboardBean();
            staffObj3.setTitle(Constants.banner);
            staffObj3.setMIcon(getResources().getDrawable(R.drawable.poster));
            list.add(staffObj3);


            DashboardBean staffObj31 = new DashboardBean();
            staffObj31.setTitle(Constants.post);
            staffObj31.setMIcon(getResources().getDrawable(R.drawable.social_media));
            list.add(staffObj31);

            DashboardBean gateObjquestions = new DashboardBean();
            gateObjquestions.setTitle(Constants.questions);
            gateObjquestions.setMIcon(getResources().getDrawable(R.drawable.questions));
            list.add(gateObjquestions);

            DashboardBean gateObj1 = new DashboardBean();
            gateObj1.setTitle(Constants.NoticeBoard);
            gateObj1.setMIcon(getResources().getDrawable(R.drawable.chalk_board_icon));
            list.add(gateObj1);

            DashboardBean sattendanceObj = new DashboardBean();
            sattendanceObj.setTitle(Constants.Message);
            sattendanceObj.setMIcon(getResources().getDrawable(R.drawable.new_message_icon));
            list.add(sattendanceObj);

//        DashboardBean timetableObj = new DashboardBean();
//        timetableObj.setTitle(Constants.timetable);
//        timetableObj.setMIcon(getResources().getDrawable(R.drawable.timetable));
//        list.add(timetableObj);


            DashboardBean gateObj = new DashboardBean();
            gateObj.setTitle(Constants.Transport);
            gateObj.setMIcon(getResources().getDrawable(R.drawable.bus_school));
            list.add(gateObj);


            DashboardBean gdsattendanceObj1 = new DashboardBean();
            gdsattendanceObj1.setTitle(Constants.fuel_expense);
            gdsattendanceObj1.setMIcon(getResources().getDrawable(R.drawable.fuel));
            list.add(gdsattendanceObj1);


            DashboardBean leaveObj = new DashboardBean();
            leaveObj.setTitle(Constants.Gallery);
            leaveObj.setMIcon(getResources().getDrawable(R.drawable.document_photo_icon));
            list.add(leaveObj);

            DashboardBean feedBackObj = new DashboardBean();
            feedBackObj.setTitle(Constants.feedback);
            feedBackObj.setMIcon(getResources().getDrawable(R.drawable.feedback_icon));
            list.add(feedBackObj);

            DashboardBean birthObj = new DashboardBean();
            birthObj.setTitle(Constants.birthday);
            birthObj.setMIcon(getResources().getDrawable(R.drawable.birthday_cake));
            list.add(birthObj);

            DashboardBean ebookObj = new DashboardBean();
            ebookObj.setTitle(Constants.Visitor);
            ebookObj.setMIcon(getResources().getDrawable(R.drawable.guard));
            list.add(ebookObj);


        } else {

        }


        DashboardBean elibraryObj = new DashboardBean();
        elibraryObj.setTitle(Constants.Notification);
        elibraryObj.setMIcon(getResources().getDrawable(R.drawable.notifications));
        list.add(elibraryObj);

//        DashboardBean staffaprsl = new DashboardBean();
//        staffaprsl.setTitle(Constants.ImageToPdf);
//        staffaprsl.setMIcon(getResources().getDrawable(R.drawable.jpgtopdfpng));
//        list.add(staffaprsl);

        DashboardBean staffaprsOCR = new DashboardBean();
        staffaprsOCR.setTitle(Constants.OCR);
        staffaprsOCR.setMIcon(getResources().getDrawable(R.drawable.qrcodeicon));
        list.add(staffaprsOCR);

        return list;
    }

    @Override
    public void openDahsboardActivity(DashboardBean dashboard) {

        if (dashboard.getTitle().equals(Constants.questions)) {
            startActivity(new Intent(getContext(), QuestionsActivities.class));
        }


        if (dashboard.getTitle().equals(Constants.schoolsetup)) {
            Intent mintent = new Intent(getActivity(), GSCActivity.class);
            startActivity(mintent);

        }

        if (dashboard.getTitle().equals(Constants.fuel_expense)) {
            opentimetabledsdialogsec(dashboard.getTitle());

        }

        if (dashboard.getTitle().equals(Constants.feedback)) {
            opentimetabledsdialogsec(dashboard.getTitle());

        }

        if (dashboard.getTitle().equals(Constants.birthday)) {
//            Intent mintent = new Intent(getActivity(), StudentBirthdayList.class);
//            startActivity(mintent);

            opentimetabledsdialogsecccc();

        }


        if (dashboard.getTitle().equals(Constants.school)) {
            // Intent mintent = new Intent(getActivity(), GSCActivity.class);
            Intent mintent = new Intent(getActivity(), SchoolInfoActivity.class);
            startActivity(mintent);

        }

        if (dashboard.getTitle().equals(Constants.holiday)) {
            Intent mintent = new Intent(getActivity(), AddHolidayActivity.class);
            startActivity(mintent);
        }

        if (dashboard.getTitle().equals(Constants.userlist)) {
            Intent mintent = new Intent(getActivity(), UserActivity.class);
            startActivity(mintent);

        }
        if (dashboard.getTitle().equals(Constants.signature)) {
            startActivity(new Intent(getContext(), AddSignatureActivity.class));


        }

        if (dashboard.getTitle().equals(Constants.thought)) {
            startActivity(new Intent(getContext(), ViewThought.class));


        }

        if (dashboard.getTitle().equals(Constants.ACTIVITY)) {
            openstudentdialogsec_activity();

        }

        if (dashboard.getTitle().equals(Constants.banner)) {

            startActivity(new Intent(getContext(), BannerActivityNew.class));

        }

        if (dashboard.getTitle().equals(Constants.admission_enq)) {
//            startActivity(new Intent(getContext(), StudentListByroll.class));
            startActivity(new Intent(getContext(), STDListActivity_Enquiry.class));
        }

        if (dashboard.getTitle().equals(Constants.Notification)) {
            startActivity(new Intent(getContext(), NotificationAct.class));

        }

        if (dashboard.getTitle().equals(Constants.NoticeBoard)) {

            if (UserPermissions.getCurrentUserPermissions().getNotice().equalsIgnoreCase("no")) {
                startActivity(new Intent(getContext(), ViewNoticeInfo.class));
            } else {
                opentimetabledsdialogsec(dashboard.getTitle());
            }
        }

        if (dashboard.getTitle().equals(Constants.Message)) {
            openmsgdialogsec();
        }
        if (dashboard.getTitle().equals(Constants.Gallery)) {
            opengallerydialogsec();
        }

        if (dashboard.getTitle().equals(Constants.Visitor)) {
            startActivity(new Intent(getContext(), SecurityActivity.class));
        }
        if (dashboard.getTitle().equals(Constants.ImageToPdf)) {
            startActivity(new Intent(getActivity(), ImageToPdfConvert.class));
        }
        if (dashboard.getTitle().equals(Constants.OCR)) {
            startActivity(new Intent(getContext(), OcrCaptureActivity.class));
        }

        if (dashboard.getTitle().equals(Constants.Transport)) {
            opentransportdialogsec();
        }

        if (dashboard.getTitle().equals(Constants.post)) {
            openAdmissionformdialog(dashboard.getTitle());

        }


    }

    private void openstudentdialogsec_activity() {

        mBottomSheetDialog = new BottomSheetDialog(getActivity());
        final View sheetView = getLayoutInflater().inflate(R.layout.search_dialog, null);
        mBottomSheetDialog.setContentView(sheetView);
        mBottomSheetDialog.setCancelable(false);
        spinnerstaff = (Spinner) sheetView.findViewById(R.id.spinnersection);
        spinnerDepartment = (Spinner) sheetView.findViewById(R.id.spinnerClass);
        RelativeLayout spinnersection = (RelativeLayout) sheetView.findViewById(R.id.sectionlayout);

        TextView textView7 = (TextView) sheetView.findViewById(R.id.textView7);
        textView7.setText("Department");

        TextView textsection = (TextView) sheetView.findViewById(R.id.textsection);
        textsection.setText("Staff");

        TextView txtSetup = (TextView) sheetView.findViewById(R.id.txtSetup);
        txtSetup.setText("School Activity List");

        ImageView close = (ImageView) sheetView.findViewById(R.id.close);
        //txtTitle.setText("Mark Attendance");
        fetchDepartment();

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //assignData();
                mBottomSheetDialog.dismiss();
            }
        });


        Button btnYes = (Button) sheetView.findViewById(R.id.btnYes);


        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (departmentid.equalsIgnoreCase("")) {
                    Toast.makeText(getActivity(), "Kindly Select Department!", Toast.LENGTH_SHORT).show();
                } else if (departmentid.equalsIgnoreCase("")) {
                    Toast.makeText(getActivity(), "Kindly Select Department!", Toast.LENGTH_SHORT).show();
                } else {


                    final BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(getContext());
                    final View sheetView = getLayoutInflater().inflate(R.layout.sa_dialog, null);
                    mBottomSheetDialog.setContentView(sheetView);

                    sheetView.findViewById(R.id.add).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent intent = new Intent(getActivity(), ViewStudents.class);
                            intent.putExtra("staffid", staffid);
                            intent.putExtra("staffname", staffname);
                            intent.putExtra("staffimg", staffimg);
                            startActivity(intent);

                        }
                    });
                    sheetView.findViewById(R.id.view).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent intent = new Intent(getActivity(), SchoolActivityMSGComposeActivity.class);
                            intent.putExtra("staffid", staffid);
                            intent.putExtra("staffname", staffname);
                            intent.putExtra("staffimg", staffimg);
                            startActivity(intent);
                        }
                    });


                    RelativeLayout btnNo = (RelativeLayout) sheetView.findViewById(R.id.btnNo);
                    btnNo.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            btnNo.startAnimation(animation4);
                            mBottomSheetDialog.dismiss();
                        }
                    });

                    mBottomSheetDialog.show();


                }

            }
        });
        mBottomSheetDialog.show();
    }

    private void fetchDepartment() {
        commonProgress.show();
        HashMap<String, String> params = new HashMap<String, String>();
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.DEPARTMENT_LIST_URL, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                commonProgress.dismiss();
                if (error == null) {
                    try {
                        departmentList = CommonBean.parseCommonArray(jsonObject.getJSONArray("department_data"));
                        DepartmentAdapter adapter = new DepartmentAdapter(getActivity(), departmentList);
                        spinnerDepartment.setAdapter(adapter);
                        spinnerDepartment.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                if (i > 0) {
                                    departmentid = "";
                                    departmentname = "";
                                    departmentid = departmentList.get(i - 1).getId();
                                    departmentname = departmentList.get(i - 1).getName();
                                    fetchstaff();
                                } else {
                                    departmentid = "";
                                    departmentname = "";
                                }
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> adapterView) {

                            }
                        });

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_LONG).show();
                    //   Utils.showSnackBar(error.getMessage(), view.findViewById(R.id.parent));
                }
            }
        }, getActivity(), params);
    }

    public void fetchstaff() {
        commonProgress.show();
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("d_id", departmentid);
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.STAFF_DETAIL, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                commonProgress.dismiss();
                if (error == null) {
                    try {
                        JSONArray jsonArray = jsonObject.getJSONArray("staff_detail");
                        stafflist = View_staff_bean.parseviewstaffArray(jsonArray);
                        adapterstafflist = new StaffListAdapter(getActivity(), stafflist);
                        spinnerstaff.setAdapter(adapterstafflist);
                        spinnerstaff.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                if (i > 0) {
                                    staffid = stafflist.get(i - 1).getId();
                                    staffname = stafflist.get(i - 1).getName() + " (" + stafflist.get(i - 1).getPost() + ")";
                                    staffimg = stafflist.get(i - 1).getImage();
                                } else {
                                    staffid = "";
                                    staffname = "";
                                    staffimg = "";
                                }
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> adapterView) {

                            }
                        });

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        }, getActivity(), params);
    }


    @SuppressLint("MissingInflatedId")
    private void openAdmissionformdialog(String title) {

        final BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(getContext());
        final View sheetView = getLayoutInflater().inflate(R.layout.admission_dialog, null);
        mBottomSheetDialog.setContentView(sheetView);

        TextView tittle = sheetView.findViewById(R.id.txtTitle);
        ImageView img = sheetView.findViewById(R.id.img);

        if (title.equalsIgnoreCase(Constants.health)) {
            tittle.setText(title);
            img.setImageDrawable(getResources().getDrawable(R.drawable.health));
        } else {
            tittle.setText(title);
            img.setImageDrawable(getResources().getDrawable(R.drawable.social_media));
        }

        sheetView.findViewById(R.id.add).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (title.equalsIgnoreCase("Health")) {
                    startActivity(new Intent(getContext(), Add_Health.class));
                } else {
                    startActivity(new Intent(getContext(), AddSocialLinks.class));
                }

            }
        });
        sheetView.findViewById(R.id.view).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (title.equalsIgnoreCase("Health")) {
                    startActivity(new Intent(getContext(), View_Health.class));
                } else {
                    startActivity(new Intent(getContext(), SocialLinkList.class));
                }

            }
        });


        RelativeLayout btnNo = (RelativeLayout) sheetView.findViewById(R.id.btnNo);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnNo.startAnimation(animation4);
                mBottomSheetDialog.dismiss();
            }
        });

        mBottomSheetDialog.show();

    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (getView() != null && isVisibleToUser) {
            Log.i("1st", "1st");
            if (!restorePrefData()) {
                setShowcaseView();
            }
        }
    }

    private void setShowcaseView() {
        new TapTargetSequence(getActivity())
                .targets(
                        TapTarget.forView(lytttt_school, "Profile Icon", "Tap the Profile Icon for view Profile Details.")
                                .outerCircleColor(R.color.light)
                                .outerCircleAlpha(0.96f)
                                .targetCircleColor(R.color.white)
                                .titleTextSize(20)
                                .titleTextColor(R.color.white)
                                .descriptionTextSize(14)
                                .descriptionTextColor(R.color.black)
                                .textColor(R.color.black)
                                .textTypeface(Typeface.SANS_SERIF)
                                .dimColor(R.color.black)
                                .drawShadow(true)
                                .cancelable(false)
                                .tintTarget(true)
                                .transparentTarget(true)
                                .targetRadius(40),
                        TapTarget.forView(lytttt, "Message Icon", "Tap the Message Icon to view latest Message.")
                                .outerCircleColor(R.color.light)
                                .outerCircleAlpha(0.96f)
                                .targetCircleColor(R.color.white)
                                .titleTextSize(20)
                                .titleTextColor(R.color.white)
                                .descriptionTextSize(14)
                                .descriptionTextColor(R.color.black)
                                .textColor(R.color.black)
                                .textTypeface(Typeface.SANS_SERIF)
                                .dimColor(R.color.black)
                                .drawShadow(true)
                                .cancelable(false)
                                .tintTarget(true)
                                .transparentTarget(true)
                                .targetRadius(40),
                        TapTarget.forView(lytttt_fees, "Notification Icon", "Tap the Notification Icon to view today Notification list.")
                                .outerCircleColor(R.color.light)
                                .outerCircleAlpha(0.96f)
                                .targetCircleColor(R.color.white)
                                .titleTextSize(20)
                                .titleTextColor(R.color.white)
                                .descriptionTextSize(14)
                                .descriptionTextColor(R.color.black)
                                .textColor(R.color.black)
                                .textTypeface(Typeface.SANS_SERIF)
                                .dimColor(R.color.black)
                                .drawShadow(true)
                                .cancelable(false)
                                .tintTarget(true)
                                .transparentTarget(true)
                                .targetRadius(40)).listener(new TapTargetSequence.Listener() {
                    @Override
                    public void onSequenceFinish() {
                        //Toast.makeText(getActivity(),"Sequence Finished",Toast.LENGTH_SHORT).show();
                        savePrefData();
                    }

                    @Override
                    public void onSequenceStep(TapTarget lastTarget, boolean targetClicked) {

                        // Toast.makeText(getActivity(),"GREAT!",Toast.LENGTH_SHORT).show();

                    }

                    @Override
                    public void onSequenceCanceled(TapTarget lastTarget) {

                    }
                }).start();
    }

    private void savePrefData() {
        sharedPreferences = getActivity().getSharedPreferences("boarding_pref_tool", MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean("IsFirstTimeVisit_tool", true);
        editor.apply();
    }

    private boolean restorePrefData() {
        sharedPreferences = getActivity().getSharedPreferences("boarding_pref_tool", MODE_PRIVATE);
        return sharedPreferences.getBoolean("IsFirstTimeVisit_tool", false);
    }


    //text to speech programmatically...
    public void TextToSpeechFunction() {
        textToSpeech.speak(textholder, TextToSpeech.QUEUE_FLUSH, null);
    }

    @Override
    public void onInit(int status) {
        if (status == TextToSpeech.SUCCESS) {
            textToSpeech.setLanguage(Locale.US);
            if (speech == 0) {
                TextToSpeechFunction();
            }
        }
    }

    @Override
    public void onDestroy() {
        if (textToSpeech != null) {
            textToSpeech.shutdown();
        }
        super.onDestroy();
    }


    @Override
    public void deleteNotification(NotificationBean notiList) {
        speech = 1;
        textholder = notiList.getN_title() + " ! " + notiList.getN_msg();
        TextToSpeechFunction();
    }
}
