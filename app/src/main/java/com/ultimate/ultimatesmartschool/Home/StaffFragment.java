package com.ultimate.ultimatesmartschool.Home;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.andremion.floatingnavigationview.FloatingNavigationView;
import com.android.volley.Request;
import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetSequence;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;
import com.ultimate.ultimatesmartschool.Activity.SchoolInfo;
import com.ultimate.ultimatesmartschool.Activity.UserPermissions;
import com.ultimate.ultimatesmartschool.BeanModule.User;
import com.ultimate.ultimatesmartschool.Leave.StaffLeaveList;
import com.ultimate.ultimatesmartschool.Login.LoginActivity;
import com.ultimate.ultimatesmartschool.MSG_NEW.SendMSG;
import com.ultimate.ultimatesmartschool.NotificationMod.NotificationAdapter;
import com.ultimate.ultimatesmartschool.NotificationMod.NotificationBean;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Staff.AddNewStaff;
import com.ultimate.ultimatesmartschool.Staff.AssignIncharge;
import com.ultimate.ultimatesmartschool.Staff.AssignSubject;
import com.ultimate.ultimatesmartschool.Staff.StaffDepartment;
import com.ultimate.ultimatesmartschool.Staff.StaffList;
import com.ultimate.ultimatesmartschool.StaffAttendance.QRAttendance.StafAttendanceByScanner;
import com.ultimate.ultimatesmartschool.StaffAttendance.StaffAttendance;
import com.ultimate.ultimatesmartschool.StaffAttendance.StaffDayReort;
import com.ultimate.ultimatesmartschool.StaffAttendance.StaffMonthReport;
import com.ultimate.ultimatesmartschool.StaffAttendance.StaffYearWiseReport;
import com.ultimate.ultimatesmartschool.StaffAttendance.ViewStaffAttendance;
import com.ultimate.ultimatesmartschool.StaffAttendance.ViewStaffQRAttendence;
import com.ultimate.ultimatesmartschool.StaffGatePass.AddStaffGatePass;
import com.ultimate.ultimatesmartschool.StaffGatePass.ViewStaffGatepass;
import com.ultimate.ultimatesmartschool.Utility.ApiHandler;
import com.ultimate.ultimatesmartschool.Utility.ApiHandlerError;
import com.ultimate.ultimatesmartschool.Utility.Constants;
import com.ultimate.ultimatesmartschool.Utility.ErpProgress;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.content.Context.MODE_PRIVATE;


public class StaffFragment extends Fragment implements DashBoardAdapter.OpenDahsboardActivity , TextToSpeech.OnInitListener, NotificationAdapter.AdapterCallback{
    SharedPreferences sharedPreferences;
    @BindView(R.id.msg_count)
    TextView msg_count;
    @BindView(R.id.lytttt)
    RelativeLayout lytttt;
    @BindView(R.id.totalRecord)
    TextView totalRecord;
    @BindView(R.id.fees_count)
    TextView fees_count;
    @BindView(R.id.lytttt_school)
    RelativeLayout lytttt_school;
    @BindView(R.id.lytttt_fees)
    RelativeLayout lytttt_fees;
    int loaded=0;
    @BindView(R.id.searchView)
    SearchView searchView;

    int count=0,speech=0;
    TextToSpeech textToSpeech;
    String textholder="",title="";
    RecyclerView notiRV;
    ArrayList<NotificationBean> notiList = new ArrayList<>();
    private NotificationAdapter notiAdapter;
    @BindView(R.id.textNorecord)
    TextView txtNorecord;

    RecyclerView recyclerView;
    private DashBoardAdapter adapter;
    private ArrayList<DashboardBean> dataList;
    private FloatingNavigationView mFloatingNavigationView;
    Animation animation1,animation2,animation3,animation4;
    BottomSheetDialog mBottomSheetDialog;
   // @BindView(R.id.imgLogo) CircularImageView navi_profiles;
    TextView admin_name;
    @BindView(R.id.schName)TextView headtext;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.staffrag_layout, container, false);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        recyclerView=(RecyclerView)view.findViewById(R.id.recyclerView) ;
        dataList = new ArrayList<>();
        dataList = addList();
        loaded=dataList.size();
        startAnimation();
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 3));
        adapter = new DashBoardAdapter(getActivity(), dataList, this);
        recyclerView.setAdapter(adapter);
        mFloatingNavigationView = (FloatingNavigationView) view.findViewById(R.id.floating_navigation_view);
        if(User.getCurrentUser().getSchoolData().getName()!=null){
            headtext.setText(User.getCurrentUser().getSchoolData().getName());
        }
        textToSpeech = new TextToSpeech(getActivity(), this);
        lytttt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                msg_count.startAnimation(animation4);
                Intent intent= new Intent(getActivity(), SendMSG.class);
                intent.putExtra("id","inbox");
                startActivity(intent);
            }
        });
        lytttt_school.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                lytttt_school.startAnimation(animation4);
                Intent intent= new Intent(getActivity(), SchoolInfo.class);
                startActivity(intent);
            }
        });

        lytttt_fees.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fees_count.startAnimation(animation4);

                if (!fees_count.getText().toString().equalsIgnoreCase("0")) {
                    openDialog();
                }else {
                    Toast.makeText(getContext(), "No New Notification Found!", Toast.LENGTH_SHORT).show();
                }
                // startActivity(new Intent(getActivity(), FeePaidListActivity.class));
            }

        });


//        if(User.getCurrentUser().getSchoolData().getLogo()!=null){
//            Picasso.with(getActivity()).load(User.getCurrentUser().getSchoolData().getLogo()).placeholder(R.drawable.logo).into(navi_profiles);
//        }else{
//            Picasso.with(getActivity()).load(R.drawable.logo).into(navi_profiles);
//        }

        View headerView = mFloatingNavigationView.getHeaderView(0);
        //navi_profile = (CircularImageView) headerView.findViewById(R.id.navi_profile);
        admin_name = (TextView) headerView.findViewById(R.id.stu_name);
        admin_name.setText(User.getCurrentUser().getFirstname());
//        if(User.getCurrentUser().getProfile()!=null) {
//            Picasso.with(getActivity()).load(User.getCurrentUser().getProfile()).placeholder(R.drawable.stud).into(navi_profile);
//        }
        mFloatingNavigationView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mFloatingNavigationView.open();
            }
        });




        totalRecord.setText("Total Module:- "+String.valueOf(loaded));
        checkLoaded(loaded);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }
            @Override
            public boolean onQueryTextChange(String query) {
                //FILTER AS YOU TYPE

                if (query.length()==0){
                    totalRecord.setText("Total Module:- "+String.valueOf(loaded));
                    checkLoaded(loaded);
                }else {
                    totalRecord.setText("Search Module:- " +String.valueOf(adapter.dataList.size()));
                    checkLoaded(adapter.dataList.size());
                }
                adapter.getFilter().filter(query);

                return false;
            }
        });

        mFloatingNavigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem item) {

                if (item.getTitle().equals("Change Password")){
                    // Toast.makeText(getActivity(),"Change Password",Toast.LENGTH_SHORT).show();
                    // reset_password();
                }
                else if (item.getTitle().equals("Feedback")){
                    Toast.makeText(getActivity(),"Feedback",Toast.LENGTH_SHORT).show();
                    //  open_changePassword();
                } else if (item.getTitle().equals("Rate Us")){
                    Uri uri = Uri.parse("market://details?id=" + getActivity().getPackageName());
                    Intent myAppLinkToMarket = new Intent(Intent.ACTION_VIEW, uri);
                    try {
                        startActivity(myAppLinkToMarket);
                    } catch (ActivityNotFoundException e) {

                    }
                } else if (item.getTitle().equals("Share Us")){
                    try {
                        Intent i = new Intent(Intent.ACTION_SEND);
                        i.setType("text/plain");
                        i.putExtra(Intent.EXTRA_SUBJECT, "XYZ Ultimate School");
                        String sAux = "\nLet me recommend you this application\n\n";
                        sAux = sAux + "https://play.google.com/store/apps/details?id=" +getActivity().getPackageName() + "\n\n";
                        i.putExtra(Intent.EXTRA_TEXT, sAux);
                        startActivity(Intent.createChooser(i, "choose one"));
                    } catch (Exception e) {
                        //e.toString();
                    }
                } else if (item.getTitle().equals("Privacy Policy")){
                    Toast.makeText(getActivity(),"Privacy Policy",Toast.LENGTH_SHORT).show();

                    BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(getActivity());
                    final View sheetView = getLayoutInflater().inflate(R.layout.privacy_policy_dialog, null);
                    mBottomSheetDialog.setContentView(sheetView);
                    mBottomSheetDialog.setCancelable(false);
                    final boolean[] loadingFinished = {true};
                    final boolean[] redirect = {false};
                    WebView webView = (WebView) sheetView.findViewById(R.id.privacy);

                    ErpProgress.showProgressBar(getActivity(),"Please Wait.......!");
                    if (!loadingFinished[0]) {
                        redirect[0] = true;
                    }
                    loadingFinished[0] = false;
                    webView.getSettings().setJavaScriptEnabled(true);
                    String url = "http://ultimatesolutiongroup.com/privacy_policy.html";
                    webView.loadUrl(url);
                    webView.setWebViewClient(new WebViewClient(){
                        @Override
                        public void onPageFinished(WebView view, String url) {
                            if(!redirect[0]){
                                loadingFinished[0] = true;
                            }
                            if(loadingFinished[0] && !redirect[0]){
                                ErpProgress.cancelProgressBar();
                            } else{
                                redirect[0] = false;
                            }
                        }
                    });

                    RelativeLayout btnNo= (RelativeLayout) sheetView.findViewById(R.id.btnNo);
                    btnNo.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            btnNo.startAnimation(animation4);
                            mBottomSheetDialog.dismiss();
                        }
                    });
                    mBottomSheetDialog.show();
                }
                else if (item.getTitle().equals("App Info")){
                    Toast.makeText(getActivity(),"App Info",Toast.LENGTH_SHORT).show();
                    oenInfoDialog();
                } else{
                    // Snackbar.make((View) mFloatingNavigationView.getParent(), item.getTitle() + " Selected!", Snackbar.LENGTH_SHORT).show();
                    //    Toast.makeText(getActivity(),"Logout!",Toast.LENGTH_SHORT).show();

                    final Dialog logoutDialog = new Dialog(getActivity());
                    logoutDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    logoutDialog.setCancelable(true);
                    logoutDialog.setContentView(R.layout.logout_dialog);
                    logoutDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                    TextView txt2 = (TextView) logoutDialog.findViewById(R.id.txt2);

                    String logout_user= User.getCurrentUser().getFirstname();
                    txt2.setText("Are you sure" + " " + logout_user+ "!" + " " + "want to logout?");

//                    CircularImageView img = (CircularImageView) logoutDialog.findViewById(R.id.img);
//
//                    if (User.getCurrentUser().getProfile() != null) {
//                        Picasso.with(getActivity()).load(User.getCurrentUser().getProfile()).placeholder(R.drawable.boy).into(navi_profile);
//                    } else {
//                        Picasso.with(getActivity()).load(R.drawable.boy).into(navi_profile);
//                    }

                    Button btnNo = (Button) logoutDialog.findViewById(R.id.btnNo);
                    btnNo.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            logoutDialog.dismiss();
                        }
                    });
                    Button btnYes = (Button) logoutDialog.findViewById(R.id.btnYes);
                    btnYes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            btnYes.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    logoutDialog.dismiss();
                                    ErpProgress.showProgressBar(getContext(), "Please wait...");
                                    User.getCurrentUser().logout();
                                    Toast.makeText(getContext(), "Logged out successfully", Toast.LENGTH_SHORT).show();
                                    Intent loginpage = new Intent(getContext(), LoginActivity.class);
                                    loginpage.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                                            Intent.FLAG_ACTIVITY_CLEAR_TASK |
                                            Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(loginpage);
                                    ErpProgress.cancelProgressBar();
                                    getActivity().finish();
                                }
                            });
                        }
                    });
                    logoutDialog.show();

                }

                //  Snackbar.make((View) mFloatingNavigationView.getParent(), item.getTitle() + " Selected!", Snackbar.LENGTH_SHORT).show();
                mFloatingNavigationView.close();


                Snackbar.make((View) mFloatingNavigationView.getParent(), item.getTitle() + " Selected!", Snackbar.LENGTH_SHORT).show();
                mFloatingNavigationView.close();
                return true;
            }
        });


    }

//    private void viewSchoolInfo() {
//        BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(getActivity());
//        final View sheetView = getLayoutInflater().inflate(R.layout.school_info_dialog, null);
//        mBottomSheetDialog.setContentView(sheetView);
//        final ViewGroup transitions_container = (ViewGroup) sheetView.findViewById(R.id.transitions_container);
//
//        TextView txtHead = (TextView) sheetView.findViewById(R.id.txtHead);
//        TextView c_name = (TextView) sheetView.findViewById(R.id.c_name);
//        TextView txtAdd = (TextView) sheetView.findViewById(R.id.txtAdd);
//
//
//        CircularImageView circleimgrecepone = (CircularImageView) sheetView.findViewById(R.id.circleimgrecepone);
//
//        FloatingActionButton email = (FloatingActionButton) sheetView.findViewById(R.id.email);
//        FloatingActionButton phone = (FloatingActionButton) sheetView.findViewById(R.id.phone);
//        String number = User.getCurrentUser().getSchoolData().getPhoneno();
//        String mail = User.getCurrentUser().getSchoolData().getEmail();
//
//        if (User.getCurrentUser().getSchoolData().getFi_school_id().equalsIgnoreCase("XYZGGSMT")){
//            txtHead.setText("College Details");
//        }else{
//            txtHead.setText("School Details");
//        }
//
//
//        if (User.getCurrentUser().getSchoolData().getLogo() != null) {
//            Picasso.with(getActivity()).load(User.getCurrentUser().getSchoolData().getLogo()).placeholder(R.drawable.logo).into(circleimgrecepone);
//        } else {
//            Picasso.with(getActivity()).load(R.drawable.logo).into(circleimgrecepone);
//        }
//        if (User.getCurrentUser().getSchoolData().getName() != null){
//            c_name.setText(User.getCurrentUser().getSchoolData().getName());
//        }
//
//        if (User.getCurrentUser().getSchoolData().getName() != null){
//            txtAdd.setText(User.getCurrentUser().getSchoolData().getAddress());
//        }
//
//        if (mail!=null || mail.equalsIgnoreCase("")) {
//            email.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    //  mBottomSheetDialog.dismiss();
//                    //  UltimateProgress.showProgressBar(getActivity(), "Please Wait.....!");
//                    Intent intent = new Intent(Intent.ACTION_SENDTO); // it's not ACTION_SEND
//                    intent.setType("text/plain");
//                    intent.putExtra(Intent.EXTRA_SUBJECT, "Subject of email");
//                    intent.putExtra(Intent.EXTRA_TEXT, "Body of email");
//                    //simplydirect email for setting email,,,
//                    intent.setData(Uri.parse("mailto:" + mail)); // or just "mailto:" for blank
//                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK); // this will make such that when user returns to your app, your app is displayed, instead of the email app.
//                    startActivity(intent);
//                 //   UltimateProgress.cancelProgressBar();
//
//                }
//            });
//        }else{
//            Toast.makeText(getActivity(),"Email Not Found!",Toast.LENGTH_LONG).show();
//        }
//
//
//        if (number!=null || number.equalsIgnoreCase("")) {
//            phone.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    // mBottomSheetDialog.dismiss();
//                    // for permission granted....
//                    if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
//                        ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CALL_PHONE},1);
//                    } else {
//                        Intent intent = new Intent(Intent.ACTION_CALL);
//                        intent.setData(Uri.parse("tel:" + number));
//                        startActivity(intent);
//                     //   UltimateProgress.cancelProgressBar();
//                    }
//                }
//            });
//        }else{
//            Toast.makeText(getActivity(),"Number Not Found!",Toast.LENGTH_LONG).show();
//        }
//
//
//        RelativeLayout btnNo= (RelativeLayout) sheetView.findViewById(R.id.btnNo);
//        btnNo.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                mBottomSheetDialog.dismiss();
//            }
//        });
//        mBottomSheetDialog.show();
//    }

    private void checkLoaded(int loaded) {
        if (loaded==0){
            txtNorecord.setVisibility(View.VISIBLE);
        }else {
            txtNorecord.setVisibility(View.GONE);
        }
    }

    private void openDialog() {
        BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(getActivity());
        final View sheetView = getLayoutInflater().inflate(R.layout.noti_msgimage_lyt, null);
        mBottomSheetDialog.setContentView(sheetView);
        mBottomSheetDialog.setCancelable(true);
        notiRV=(RecyclerView) sheetView.findViewById(R.id.recyclerView);

        TextView tittle = (TextView) sheetView.findViewById(R.id.tittle);
        String count=fees_count.getText().toString();

        if (!count.equalsIgnoreCase("0")) {
            String title = getColoredSpanned(count, "#e31e25");
            String Name = getColoredSpanned(" New Notification", "#000000");
            tittle.setText(Html.fromHtml(title + " " + Name));
        }else {
            tittle.setText("Today Notification");
        }

        notiList = new ArrayList<>();
        LinearLayoutManager layoutManager1 = new LinearLayoutManager(getActivity());
        notiRV.setLayoutManager(layoutManager1);
        notiAdapter = new NotificationAdapter(notiList, this, getActivity());
        notiRV.setAdapter(notiAdapter);
        fetchNotification();

        RelativeLayout btnNo= (RelativeLayout) sheetView.findViewById(R.id.btnNo);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                mBottomSheetDialog.dismiss();
            }
        });

        mBottomSheetDialog.show();
    }

    public void fetchNotification() {
        HashMap<String, String> params = new HashMap<>();
        params.put("id", User.getCurrentUser().getId());
        params.put("type", "10");
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.NOTIFICATIONURL, notiapiCallback, getActivity(), params);
    }

    ApiHandler.ApiCallback notiapiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();
            if (error == null) {
                try {
                    JSONArray noticeArray = jsonObject.getJSONArray("notice_data");
                    notiList = NotificationBean.parseNotificationArray(noticeArray);
                    if (notiList.size() > 0) {
                        notiAdapter.setNotificationList(notiList);
                        //setanimation on adapter...
                        notiRV.getAdapter().notifyDataSetChanged();
                        notiRV.scheduleLayoutAnimation();
                        //-----------end------------
                    } else {
                        notiList.clear();
                        notiAdapter.setNotificationList(notiList);
                        notiAdapter.notifyDataSetChanged();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                notiList.clear();
                notiAdapter.setNotificationList(notiList);
                notiAdapter.notifyDataSetChanged();

            }
        }
    };


    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }


    @SuppressLint("RestrictedApi")
    private void oenInfoDialog() {
        mBottomSheetDialog = new BottomSheetDialog(getActivity());
        final View sheetView = getLayoutInflater().inflate(R.layout.app_info_dialog, null);
        mBottomSheetDialog.setContentView(sheetView);

        TextView txtVersion = (TextView) sheetView.findViewById(R.id.txtVersion);
        FloatingActionButton whatsapp = (FloatingActionButton) sheetView.findViewById(R.id.whatsapp);
        FloatingActionButton email = (FloatingActionButton) sheetView.findViewById(R.id.email);
        FloatingActionButton phone = (FloatingActionButton) sheetView.findViewById(R.id.phone);
        String number="7009091606";
        String versionName ="";
        int code=0;
        try {
            versionName = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0).versionName;
            code= getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0).versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        txtVersion.setText("App Version Code: "+code+"\n"+"App Version Name: "+String.valueOf(versionName));

        email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //  mBottomSheetDialog.dismiss();
                ErpProgress.showProgressBar(getActivity(), "Please Wait.....!");
                Intent intent = new Intent(Intent.ACTION_SENDTO); // it's not ACTION_SEND
                intent.setType("text/plain");
                intent.putExtra(Intent.EXTRA_SUBJECT, "Subject of email");
                intent.putExtra(Intent.EXTRA_TEXT, "Body of email");
                //simplydirect email for setting email,,,
                intent.setData(Uri.parse("mailto:dealingkings112@gmail.com")); // or just "mailto:" for blank
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK); // this will make such that when user returns to your app, your app is displayed, instead of the email app.
                startActivity(intent);
                ErpProgress.cancelProgressBar();

            }
        });
        whatsapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //  mBottomSheetDialog.dismiss();
                Uri uri = Uri.parse("smsto:" + number);
                Intent i = new Intent(Intent.ACTION_SENDTO, uri);
                i.setPackage("com.whatsapp");
                startActivity(Intent.createChooser(i, "Chat Now!"));
            }
        });
        phone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // mBottomSheetDialog.dismiss();
                // for permission granted....
                if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CALL_PHONE},1);
                } else {
                    Intent intent = new Intent(Intent.ACTION_CALL);
                    intent.setData(Uri.parse("tel:" + number));
                    startActivity(intent);
                    ErpProgress.cancelProgressBar();
                }
            }
        });


        RelativeLayout btnNo= (RelativeLayout) sheetView.findViewById(R.id.btnNo);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnNo.startAnimation(animation4);
                mBottomSheetDialog.dismiss();
            }
        });
        mBottomSheetDialog.show();


    }

    private void startAnimation() {
        animation1 = AnimationUtils.loadAnimation(getActivity(), R.anim.logo_animation);
        animation2 = AnimationUtils.loadAnimation(getActivity(), R.anim.enter_from_left_animation);
        animation3 = AnimationUtils.loadAnimation(getActivity(), R.anim.fade_animation);
        animation4 = AnimationUtils.loadAnimation(getActivity(), R.anim.btn_blink_animation);

    }

    @SuppressLint("UseCompatLoadingForDrawables")
    private ArrayList<DashboardBean> addList() {

        ArrayList<DashboardBean> list = new ArrayList<>();


        if (User.getCurrentUser().getType().equalsIgnoreCase("user")) {
//
//            if (UserPermissions.getCurrentUserPermissions().getSetup().equalsIgnoreCase("yes")) {
//                DashboardBean staffObj = new DashboardBean();
//                staffObj.setTitle(Constants.schoolsetup);
//                staffObj.setMIcon(getResources().getDrawable(R.drawable.systems));
//                list.add(staffObj);
//            }

            if (UserPermissions.getCurrentUserPermissions().getStaff_panel().equalsIgnoreCase("yes")) {
                DashboardBean staffObj = new DashboardBean();
                staffObj.setTitle(Constants.staff);
                staffObj.setMIcon(getResources().getDrawable(R.drawable.boy));
                list.add(staffObj);
            }
//
            if (UserPermissions.getCurrentUserPermissions().getAssign_incharge().equalsIgnoreCase("yes")) {
                DashboardBean staffObj1 = new DashboardBean();
                staffObj1.setTitle(Constants.inchage);
                staffObj1.setMIcon(getResources().getDrawable(R.drawable.presentation));
                list.add(staffObj1);
            }
//

            if (UserPermissions.getCurrentUserPermissions().getAssign_subject().equalsIgnoreCase("yes")) {

                DashboardBean staffObj3 = new DashboardBean();
                staffObj3.setTitle(Constants.subject);
                staffObj3.setMIcon(getResources().getDrawable(R.drawable.open_book));
                list.add(staffObj3);
            }

            if (UserPermissions.getCurrentUserPermissions().getStaff_gate_attend().equalsIgnoreCase("yes")) {

                DashboardBean gateObj1 = new DashboardBean();
                gateObj1.setTitle(Constants.gateattendance);
                gateObj1.setMIcon(getResources().getDrawable(R.drawable.team));
                list.add(gateObj1);
            }

            if (UserPermissions.getCurrentUserPermissions().getStaff_attend().equalsIgnoreCase("yes")) {

                DashboardBean sattendanceObj = new DashboardBean();
                sattendanceObj.setTitle(Constants.sattendance);
                sattendanceObj.setMIcon(getResources().getDrawable(R.drawable.attendance));
                list.add(sattendanceObj);
            }

            if (UserPermissions.getCurrentUserPermissions().getStaff_leave().equalsIgnoreCase("yes")) {

                DashboardBean leaveObj = new DashboardBean();
                leaveObj.setTitle(Constants.leave);
                leaveObj.setMIcon(getResources().getDrawable(R.drawable.leave));
                list.add(leaveObj);
            }


            if (UserPermissions.getCurrentUserPermissions().getGatepass().equalsIgnoreCase("yes")) {

                DashboardBean gateObj = new DashboardBean();
                gateObj.setTitle(Constants.gatepass);
                gateObj.setMIcon(getResources().getDrawable(R.drawable.gatepass));
                list.add(gateObj);
            }

        }else if (User.getCurrentUser().getType().equalsIgnoreCase("admin") || User.getCurrentUser().getType().equalsIgnoreCase("super")) {
            DashboardBean staffObj = new DashboardBean();
            staffObj.setTitle(Constants.staff);
            staffObj.setMIcon(getResources().getDrawable(R.drawable.boy));
            list.add(staffObj);


            DashboardBean staffObj1 = new DashboardBean();
            staffObj1.setTitle(Constants.inchage);
            staffObj1.setMIcon(getResources().getDrawable(R.drawable.presentation));
            list.add(staffObj1);


            DashboardBean staffObj3 = new DashboardBean();
            staffObj3.setTitle(Constants.subject);
            staffObj3.setMIcon(getResources().getDrawable(R.drawable.open_book));
            list.add(staffObj3);

            DashboardBean gateObj1 = new DashboardBean();
            gateObj1.setTitle(Constants.gateattendance);
            gateObj1.setMIcon(getResources().getDrawable(R.drawable.team));
            list.add(gateObj1);

            DashboardBean sattendanceObj = new DashboardBean();
            sattendanceObj.setTitle(Constants.sattendance);
            sattendanceObj.setMIcon(getResources().getDrawable(R.drawable.attendance));
            list.add(sattendanceObj);



            DashboardBean leaveObj = new DashboardBean();
            leaveObj.setTitle(Constants.leave);
            leaveObj.setMIcon(getResources().getDrawable(R.drawable.leave));
            list.add(leaveObj);

            DashboardBean gateObj = new DashboardBean();
            gateObj.setTitle(Constants.gatepass);
            gateObj.setMIcon(getResources().getDrawable(R.drawable.gatepass));
            list.add(gateObj);


        }else{

        }



//        DashboardBean timetableObj = new DashboardBean();
//        timetableObj.setTitle(Constants.timetable);
//        timetableObj.setMIcon(getResources().getDrawable(R.drawable.timetable));
//        list.add(timetableObj);




//        DashboardBean ebookObj = new DashboardBean();
//        ebookObj.setTitle(Constants.ebook);
//        ebookObj.setMIcon(getResources().getDrawable(R.drawable.ebook));
//        list.add(ebookObj);

//        DashboardBean elibraryObj = new DashboardBean();
//        elibraryObj.setTitle(Constants.elibrary);
//        elibraryObj.setMIcon(getResources().getDrawable(R.drawable.library));
//        list.add(elibraryObj);



//        DashboardBean staffaprsl = new DashboardBean();
//        staffaprsl.setTitle(Constants.staffappraisal);
//        staffaprsl.setMIcon(getResources().getDrawable(R.drawable.appraisal));
//        list.add(staffaprsl);
        return list;
    }
    @Override
    public void openDahsboardActivity(DashboardBean dashboard) {


        if (dashboard.getTitle().equals(Constants.gatepass)) {
            openstaffgatepssdialog();
           // startActivity(new Intent(getContext(), ViewStaffGatepass.class));
        }

        if (dashboard.getTitle().equals(Constants.gateattendance)) {

            startActivity(new Intent(getContext(), ViewStaffQRAttendence.class));

        }

        if (dashboard.getTitle().equals(Constants.subject)) {

            startActivity(new Intent(getContext(), AssignSubject.class));

        }

        if (dashboard.getTitle().equals(Constants.inchage)) {

            startActivity(new Intent(getContext(), AssignIncharge.class));

        }

        if (dashboard.getTitle().equals(Constants.staff)) {
            openstaffdialogsec();
        }
        if (dashboard.getTitle().equals(Constants.sattendance)) {
            openeattenddialogsec();
        }

        if (dashboard.getTitle().equals(Constants.leave)) {
            startActivity(new Intent(getContext(), StaffLeaveList.class));
        }
    }

    private void  openstaffgatepssdialog(){
        final BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(getContext());
        final View sheetView = getLayoutInflater().inflate(R.layout.stugatepass_dialog, null);
        mBottomSheetDialog.setContentView(sheetView);
        TextView add= (TextView) sheetView.findViewById(R.id.add);
      //  add.setText("Pending!");
        TextView view= (TextView) sheetView.findViewById(R.id.view);
      //  view.setText("Verified!");
        sheetView.findViewById(R.id.add).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), AddStaffGatePass.class));
            }
        });
        sheetView.findViewById(R.id.view).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), ViewStaffGatepass.class));
            }
        });



        RelativeLayout btnNo= (RelativeLayout) sheetView.findViewById(R.id.btnNo);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnNo.startAnimation(animation4);
                mBottomSheetDialog.dismiss();
            }
        });

        mBottomSheetDialog.show();
    }
    private void openeattenddialogsec() {
        final BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(getContext());
        final View sheetView = getLayoutInflater().inflate(R.layout.sattend_dialog, null);
        mBottomSheetDialog.setContentView(sheetView);

        sheetView.findViewById(R.id.qr).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), StafAttendanceByScanner.class));
            }
        });
        sheetView.findViewById(R.id.add).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), StaffAttendance.class));
            }
        });
        sheetView.findViewById(R.id.view).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), ViewStaffAttendance.class));
            }
        });
        sheetView.findViewById(R.id.viewDatewise).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(getContext());
                final View sheetView = getLayoutInflater().inflate(R.layout.attend_dialog2, null);
                mBottomSheetDialog.setContentView(sheetView);

                Button viewDatewise = (Button) sheetView.findViewById(R.id.viewDatewise);
                viewDatewise.setVisibility(View.VISIBLE);
                viewDatewise.setText("YearWise");

                sheetView.findViewById(R.id.viewDatewise).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        startActivity(new Intent(getContext(), StaffYearWiseReport.class));

                    }
                });

                sheetView.findViewById(R.id.add).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        startActivity(new Intent(getContext(), StaffDayReort.class));
                    }
                });
                sheetView.findViewById(R.id.view).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        startActivity(new Intent(getContext(), StaffMonthReport.class));
                    }
                });
                RelativeLayout btnNo= (RelativeLayout) sheetView.findViewById(R.id.btnNo);
                btnNo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        btnNo.startAnimation(animation4);
                        mBottomSheetDialog.dismiss();
                    }
                });

                mBottomSheetDialog.show();

                //startActivity(new Intent(getContext(), Homeworkbydate.class));
            }
        });
        RelativeLayout btnNo= (RelativeLayout) sheetView.findViewById(R.id.btnNo);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnNo.startAnimation(animation4);
                mBottomSheetDialog.dismiss();
            }
        });

        mBottomSheetDialog.show();
    }
    private void  openstaffdialogsec(){
        final BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(getContext());
        final View sheetView = getLayoutInflater().inflate(R.layout.staff_dialog, null);
        mBottomSheetDialog.setContentView(sheetView);

        LinearLayout firstlay = (LinearLayout) sheetView.findViewById(R.id.firstlay);
        LinearLayout sec = (LinearLayout) sheetView.findViewById(R.id.firstlaysss);

//        if (User.getCurrentUser().getType().equalsIgnoreCase("user")){
//
//            if (UserPermissions.getCurrentUserPermissions().getAdd_staff().equalsIgnoreCase("no")){
//                firstlay.setVisibility(View.GONE);
//            }else {
//                firstlay.setVisibility(View.VISIBLE);
//            }
//
//            if (UserPermissions.getCurrentUserPermissions().getView_staff().equalsIgnoreCase("no")){
//                sec.setVisibility(View.GONE);
//            }else {
//                sec.setVisibility(View.VISIBLE);
//            }
//
//        }

        sheetView.findViewById(R.id.add).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), StaffDepartment.class));
            }
        });
        sheetView.findViewById(R.id.view).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), AddNewStaff.class));
            }
        });

        sheetView.findViewById(R.id.viewDatewise).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), StaffList.class));
            }
        });

        RelativeLayout btnNo= (RelativeLayout) sheetView.findViewById(R.id.btnNo);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnNo.startAnimation(animation4);
                mBottomSheetDialog.dismiss();
            }
        });

        mBottomSheetDialog.show();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if(getView() != null && isVisibleToUser){
            Log.i("1st","1st");
//            if (!restorePrefData()){
//                setShowcaseView();
//            }
        }
    }

    private void setShowcaseView() {
        new TapTargetSequence(getActivity())
                .targets(
                        TapTarget.forView(lytttt_school,"School Icon","Tap the School Icon for view School Details.")
                                .outerCircleColor(R.color.light)
                                .outerCircleAlpha(0.96f)
                                .targetCircleColor(R.color.white)
                                .titleTextSize(20)
                                .titleTextColor(R.color.white)
                                .descriptionTextSize(14)
                                .descriptionTextColor(R.color.black)
                                .textColor(R.color.black)
                                .textTypeface(Typeface.SANS_SERIF)
                                .dimColor(R.color.black)
                                .drawShadow(true)
                                .cancelable(false)
                                .tintTarget(true)
                                .transparentTarget(true)
                                .targetRadius(40),
                        TapTarget.forView(lytttt,"Message Icon","Tap the Message Icon to view latest Message.")
                                .outerCircleColor(R.color.light)
                                .outerCircleAlpha(0.96f)
                                .targetCircleColor(R.color.white)
                                .titleTextSize(20)
                                .titleTextColor(R.color.white)
                                .descriptionTextSize(14)
                                .descriptionTextColor(R.color.black)
                                .textColor(R.color.black)
                                .textTypeface(Typeface.SANS_SERIF)
                                .dimColor(R.color.black)
                                .drawShadow(true)
                                .cancelable(false)
                                .tintTarget(true)
                                .transparentTarget(true)
                                .targetRadius(40),
                        TapTarget.forView(lytttt_fees,"Notification Icon","Tap the Notification Icon to view today Notification list.")
                                .outerCircleColor(R.color.light)
                                .outerCircleAlpha(0.96f)
                                .targetCircleColor(R.color.white)
                                .titleTextSize(20)
                                .titleTextColor(R.color.white)
                                .descriptionTextSize(14)
                                .descriptionTextColor(R.color.black)
                                .textColor(R.color.black)
                                .textTypeface(Typeface.SANS_SERIF)
                                .dimColor(R.color.black)
                                .drawShadow(true)
                                .cancelable(false)
                                .tintTarget(true)
                                .transparentTarget(true)
                                .targetRadius(40)).listener(new TapTargetSequence.Listener() {
            @Override
            public void onSequenceFinish() {
                //Toast.makeText(getActivity(),"Sequence Finished",Toast.LENGTH_SHORT).show();
                savePrefData();
            }

            @Override
            public void onSequenceStep(TapTarget lastTarget, boolean targetClicked) {

                // Toast.makeText(getActivity(),"GREAT!",Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onSequenceCanceled(TapTarget lastTarget) {

            }
        }).start();
    }

    private void savePrefData(){
        sharedPreferences=getActivity().getSharedPreferences("boarding_pref_tool",MODE_PRIVATE);
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putBoolean("IsFirstTimeVisit_tool",true);
        editor.apply();
    }

    private boolean restorePrefData(){
        sharedPreferences = getActivity().getSharedPreferences("boarding_pref_tool",MODE_PRIVATE);
        return sharedPreferences.getBoolean("IsFirstTimeVisit_tool",false);
    }

    @Override
    public void onResume() {
        super.onResume();
        fetnewmsg();
        fetnewmsgNoti();
    }

    private void fetnewmsgNoti() {
        HashMap<String, String> params = new HashMap<>();
        params.put("user_id",User.getCurrentUser().getId());
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.NEW_MSG_NOTI, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                if (error == null) {

                    try {
                        Log.e("USERDATA", jsonObject.getJSONObject("doc_data").toString());

                        fees_count.setText(jsonObject.getJSONObject("doc_data").getString("noti_count"));

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                } else {
                    fees_count.setText("0");
                    // Toast.makeText(getActivity(),error.getMessage(),Toast.LENGTH_LONG).show();
                    //Utils.showSnackBar(error.getMessage(), parent);
                }
            }
        }, getActivity(), params);
    }


    private void fetnewmsg() {
        HashMap<String, String> params = new HashMap<>();
        params.put("user_id",User.getCurrentUser().getId());
        params.put("check","list");
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.NEW_MSG, apicallback5, getActivity(), params);
    }

    ApiHandler.ApiCallback apicallback5 = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            ErpProgress.cancelProgressBar();

            if (error == null) {

                try {
                    Log.e("USERDATA", jsonObject.getJSONObject("doc_data").toString());

                    msg_count.setText(jsonObject.getJSONObject("doc_data").getString("count"));

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            } else {
                msg_count.setText("0");
                // Toast.makeText(getActivity(),error.getMessage(),Toast.LENGTH_LONG).show();
                //Utils.showSnackBar(error.getMessage(), parent);
            }
        }
    };


    //text to speech programmatically...
    public void TextToSpeechFunction() {
        textToSpeech.speak(textholder, TextToSpeech.QUEUE_FLUSH, null);
    }

    @Override
    public void onInit(int status) {
        if (status == TextToSpeech.SUCCESS) {
            textToSpeech.setLanguage(Locale.US);
            if (speech==0) {
                TextToSpeechFunction();
            }
        }
    }

    @Override
    public void onDestroy() {
        if (textToSpeech != null) {
            textToSpeech.shutdown();
        }
        super.onDestroy();
    }


    @Override
    public void deleteNotification(NotificationBean notiList) {
        speech=1;
        textholder = notiList.getN_title()+" ! "+ notiList.getN_msg();
        TextToSpeechFunction();
    }
}
