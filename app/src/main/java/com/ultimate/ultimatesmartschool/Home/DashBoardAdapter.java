package com.ultimate.ultimatesmartschool.Home;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.ultimate.ultimatesmartschool.Activity.ModuleFilter;
import com.ultimate.ultimatesmartschool.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DashBoardAdapter extends RecyclerView.Adapter<DashBoardAdapter.MyViewHolder> implements Filterable {
    ModuleFilter filter;
    private Animation animation;
    private final OpenDahsboardActivity mOpenDahsboardActivity;
    private Context mContext;
    public ArrayList<DashboardBean> dataList;

    public DashBoardAdapter(Context mContext, ArrayList<DashboardBean> dataList, OpenDahsboardActivity mOpenDahsboardActivity) {
        this.mContext = mContext;
        this.mOpenDahsboardActivity = mOpenDahsboardActivity;
        this.dataList = dataList;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        LayoutInflater inflater = LayoutInflater.from(
                parent.getContext());
        View v =
                inflater.inflate(R.layout.dashboard_list_lyt, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        holder.parent.setAnimation(AnimationUtils.loadAnimation(mContext, R.anim.up_to_down));
        final DashboardBean beanObj = dataList.get(position);
        holder.txtTitle.setText(beanObj.getTitle());
        holder.icon.setImageDrawable(beanObj.getMIcon());

        holder.lytAdmissionForm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                animation = AnimationUtils.loadAnimation(mContext, R.anim.btn_blink_animation);
                holder.lytAdmissionForm.startAnimation(animation);
                if (mOpenDahsboardActivity != null) {
                    mOpenDahsboardActivity.openDahsboardActivity(beanObj);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return (dataList.size());
    }

    @Override
    public Filter getFilter() {
        if (filter == null) {
            filter = new ModuleFilter(dataList, this);
        }
        return filter;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView icon;

        TextView txtTitle;
        TextView badge;

        @BindView(R.id.parent)
        RelativeLayout parent;
        RelativeLayout lytAdmissionForm;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            icon = (ImageView) itemView.findViewById(R.id.icon);
            txtTitle = (TextView) itemView.findViewById(R.id.txtTitle);
            lytAdmissionForm = (RelativeLayout) itemView.findViewById(R.id.lytAdmissionForm);
            badge = (TextView) itemView.findViewById(R.id.badge);

        }
    }

    public interface OpenDahsboardActivity {
        public void openDahsboardActivity(DashboardBean dashboard);
    }
}
