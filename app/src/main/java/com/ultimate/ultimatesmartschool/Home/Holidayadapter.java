package com.ultimate.ultimatesmartschool.Home;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.daimajia.swipe.SimpleSwipeListener;
import com.daimajia.swipe.SwipeLayout;
import com.ultimate.ultimatesmartschool.Homework.HomeworkadapterNew;
import com.ultimate.ultimatesmartschool.Homework.Homeworkbean;
import com.ultimate.ultimatesmartschool.R;
import com.ultimate.ultimatesmartschool.Utility.Utils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Holidayadapter  extends RecyclerView.Adapter<Holidayadapter.Viewholder> {
    Context mContext;
    ArrayList<HolidayBean> holidayList;

    private final Mycallback mAdaptercall;

    int value=0;
    public Holidayadapter(ArrayList<HolidayBean> holidayList, Context mContext,int value,Mycallback mAdaptercall) {
        this.holidayList = holidayList;
        this.mContext = mContext;
        this.value = value;
        this.mAdaptercall=mAdaptercall;
    }


    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adpt_holiday_list, parent, false);
        Holidayadapter.Viewholder viewholder = new Holidayadapter.Viewholder(view);
        return viewholder;
    }

    @Override
    public void onBindViewHolder(@NonNull Holidayadapter.Viewholder holder, @SuppressLint("RecyclerView") int position) {

        if (value==1){
            holder.swipeLayout.setVisibility(View.VISIBLE);
            holder.home.setVisibility(View.GONE);
            if (holidayList.get(position).getId() != null) {
                String title = getColoredSpanned("ID: ", "#000000");
                String Name = getColoredSpanned("hd-"+holidayList.get(position).getId(), "#5A5C59");
                holder.albm_id.setText(Html.fromHtml(title + " " + Name));
            }

            if (holidayList.get(position).getTitle() != null) {
                String title = getColoredSpanned("Title: ", "#F4212C");
                String Name = getColoredSpanned(""+holidayList.get(position).getTitle(), "#5A5C59");
                holder.title.setText(Html.fromHtml(title + " " + Name));
            }

            if (holidayList.get(position).getHoliday_date() != null) {
                String title = getColoredSpanned("Holiday Date: ", "#000000");
                String Name = getColoredSpanned(""+Utils.getDateFormated(holidayList.get(position).getHoliday_date()), "#5A5C59");
                holder.h_date.setText(Html.fromHtml(title + " " + Name));
            }

            if (holidayList.get(position).getCreated_on() != null) {
                String title = getColoredSpanned("Created-On: ", "#000000");
                String Name = getColoredSpanned(""+Utils.getDateFormated(holidayList.get(position).getCreated_on()), "#5A5C59");
                holder.createdon.setText(Html.fromHtml(title + " " + Name));
            }



            holder.swipeLayout.setShowMode(SwipeLayout.ShowMode.PullOut);
            holder.swipeLayout.addSwipeListener(new SimpleSwipeListener() {
                @Override
                public void onOpen(SwipeLayout layout) {

                }

                @Override
                public void onStartClose(SwipeLayout layout) {
                    holder.swipeLayout.close();
                }
            });


            holder.trash.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AlertDialog.Builder builder1 = new AlertDialog.Builder(mContext);
                    builder1.setMessage("Do you want to delete? ");
                    builder1.setCancelable(false);
                    builder1.setPositiveButton(
                            "Yes", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {

                                    if (mAdaptercall != null) {
                                        mAdaptercall.onDelecallback(holidayList.get(position));
//                                    Toast.makeText(listner, "deleted", Toast.LENGTH_SHORT).show();
                                    }
                                    // mAdapterCallback.deleteNotification(notificationList.get(postion));
                                }
                            });
                    builder1.setNegativeButton(
                            "No", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();
                                }
                            });
                    AlertDialog alert11 = builder1.create();
                    alert11.show();
                }
            });

            //  holder.albm_id.setText("Album Id: "+hq_list.get(position).getAlbum_id());
           // holder.title.setText(holidayList.get(position).getTitle());
           // holder.createdon.setText("Created-On: "+ Utils.getDateFormated(holidayList.get(position).getCreated_on()));
        }else{
            holder.swipeLayout.setVisibility(View.GONE);
            holder.home.setVisibility(View.VISIBLE);
            holder.txtHoliday.setText(holidayList.get(position).getDate() + "\n" + holidayList.get(position).getMon());
        }


    }

    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }

    public interface Mycallback{
        public void onDelecallback(HolidayBean homeworkbean);
    }

    @Override
    public int getItemCount() {
        return holidayList.size();
    }

    public void setHList(ArrayList<HolidayBean> holidayList) {
        this.holidayList = holidayList;
    }

    public class Viewholder extends RecyclerView.ViewHolder {

        @BindView(R.id.hol)
        RelativeLayout holiday;

        @BindView(R.id.home)
        RelativeLayout home;

        @BindView(R.id.txtHoliday)
        TextView txtHoliday;

        @BindView(R.id.albm_id)
        TextView albm_id;

        @BindView(R.id.title)
        TextView title;

        @BindView(R.id.albm_name)
        TextView h_date;

        @BindView(R.id.status)
        TextView createdon;

        SwipeLayout swipeLayout;
        public ImageView trash, update;


        public Viewholder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            swipeLayout = (SwipeLayout) itemView.findViewById(R.id.swipe);
            trash = (ImageView) itemView.findViewById(R.id.trash);
            update = (ImageView) itemView.findViewById(R.id.update);
        }
    }


}
