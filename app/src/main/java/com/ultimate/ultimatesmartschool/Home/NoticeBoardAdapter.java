package com.ultimate.ultimatesmartschool.Home;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.squareup.picasso.Picasso;
import com.ultimate.ultimatesmartschool.NoticeMod.NoticeBean;
import com.ultimate.ultimatesmartschool.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NoticeBoardAdapter extends RecyclerView.Adapter<NoticeBoardAdapter.Viewholder> {
    Context mContext;
    ArrayList<NoticeBean> noticeList;

    public NoticeBoardAdapter(ArrayList<NoticeBean> noticeList, Context mContext) {
        this.noticeList = noticeList;
        this.mContext = mContext;
    }


    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adpt_notice_list, parent, false);
        NoticeBoardAdapter.Viewholder viewholder = new NoticeBoardAdapter.Viewholder(view);
        return viewholder;
    }

    @Override
    public void onBindViewHolder(@NonNull NoticeBoardAdapter.Viewholder holder, int position) {
        holder.txtNTitle.setText(noticeList.get(position).getTitle());
        holder.txtMsg.setText(Html.fromHtml(noticeList.get(position).getMsg()));
        holder.txtTime.setText(noticeList.get(position).getDate());
        if((position % 2) == 0){ //Even section
           Picasso.get().load(R.drawable.books).into(holder.imgNInfo);
        }else{
            Picasso.get().load(R.drawable.books).into(holder.imgNInfo);
        }
    }

    @Override
    public int getItemCount() {
        return noticeList.size();
    }

    public void setNList(ArrayList<NoticeBean> noticeList) {
        this.noticeList = noticeList;
    }

    public class Viewholder extends RecyclerView.ViewHolder {
        @BindView(R.id.txtNTitle)
        TextView txtNTitle;
        @BindView(R.id.txtMsg)
        TextView txtMsg;
        @BindView(R.id.txtTime)
        TextView txtTime;
        @BindView(R.id.imgNInfo)
        ImageView imgNInfo;

        public Viewholder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
